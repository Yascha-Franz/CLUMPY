# This file has been changed to be used with CLUMPY, v3.0 (June, 2018)

HEALPIX_TARGET?=auto
#ifndef HEALPIX_TARGET
#  HEALPIX_TARGET:=$(error HEALPIX_TARGET undefined. Please see README.compilation for help)UNDEFINED
#endif


##################################################################################################
# READ local_env.txt to retrieve correct variables
HEALPIX_TARGET          := $(shell awk '$$1=="HEALPIX_TARGET"         {print $$2}' local_env.txt)
EXTERNAL_CFITSIO        := $(shell awk '$$1=="EXTERNAL_CFITSIO"       {print $$2}' local_env.txt)
CFITSIO_EXT_LIB         := $(shell awk '$$1=="CFITSIO_EXT_LIB"        {print $$2}' local_env.txt)
CFITSIO_EXT_INC         := $(shell awk '$$1=="CFITSIO_EXT_INC"        {print $$2}' local_env.txt)
OPENMPLINK              := $(shell awk '$$1=="OPENMPLINK"             {print $$2}' local_env.txt)
ifeq ($(OPENMPLINK), NOTFOUND)
	OPENMPLINK          :=
endif
HEALPIXCPPINCLUDE_PATH  := $(shell awk '$$1=="HEALPIXCPPINCLUDE_PATH" {print $$2}' local_env.txt)

# $(info $(HEALPIX_TARGET))
# $(info $(EXTERNAL_CFITSIO))
# $(info $(CFITSIO_EXT_LIB))
# $(info $(CFITSIO_EXT_INC))
# $(info $(OPENMPLINK))
##################################################################################################

default: compile_all
SRCROOT:=$(shell pwd)
include $(SRCROOT)/config/config.$(HEALPIX_TARGET)
include $(SRCROOT)/config/rules.common

.PHONY: test

all_hdr:=
all_lib:=
all_cbin:=
all_cxxbin:=

FULL_INCLUDE:=

include libcfitsio/planck.make
include c_utils/planck.make
include libfftpack/planck.make
include libsharp/planck.make
include cxxsupport/planck.make
include Healpix_cxx/planck.make
include alice/planck.make
include docsrc/planck.make

$(all_lib): %: | $(LIBDIR)_mkdir
	@echo "#  creating library $*"
	$(ARCREATE) $@ $^

$(all_cxxbin): %: | $(BINDIR)_mkdir
	@echo "#  linking C++ binary $*"
	$(CXXL) $(CXXLFLAGS) -o $@ $^ $(CXX_EXTRALIBS)

$(all_cbin): %: | $(BINDIR)_mkdir
	@echo "#  linking C binary $*"
	$(CXXL) $(CXXLFLAGS) -o $@ $^ $(CXX_EXTRALIBS)

compile_all: $(all_cbin) $(all_cxxbin) hdrcopy

compile_libs: $(all_lib) $(LIB_libcfitsio) hdrcopy

hdrclean:
	@if [ -d $(INCDIR) ]; then rm -rf $(INCDIR)/* ; fi

hdrcopy: | $(INCDIR)_mkdir
	@if [ "$(all_hdr)" ]; then cp -p $(all_hdr) $(INCDIR); fi

$(notdir $(all_cbin) $(all_cxxbin)) : % : $(BINDIR)/%
$(notdir $(all_lib)) libcfitsio.a : % : $(LIBDIR)/%

test: compile_all
	cd test; HEALPIX_TARGET=$(HEALPIX_TARGET) ./runtest.sh
