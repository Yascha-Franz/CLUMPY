#ifndef _CLUMPY_CLUMPS_H_
#define _CLUMPY_CLUMPS_H_

// C++ std libraries
using namespace std;
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <string>

struct structTriaxiality {                    //!< Structure to store DM halo profile definition
   double a                          = -1;    //!< Major axis (if no rotation angle below, along x_los)
   double b                          = -1;    //!< Second axis (if no rotation angle below, along y_los)
   double c                          = -1;    //!< Minor axis (if no rotation angle below, along z_los)
   bool   Is                         = false; //!< Whether the halo is triaxial or not
   double rotalpha                   = -1;    //!< Euler \f$\alpha\f$ rotation angle (xyz_los coordinates, see \c geometry.h)
   double rotbeta                    = -1;    //!< Euler \f$\beta\f$ rotation angle (xyz_los coordinates, see \c geometry.h)
   double rotgamma                   = -1;    //!< Euler \f$\gamma\f$ rotation angle (xyz_los coordinates, see \c geometry.h)
};

struct structHaloProfile {                    //!< Structure to store DM halo profile definition
   int    profileName                = -1;    //!< Chosen among values in gENUM_PROFILE (from \c params.h)
   double rhos                       = -1;    //!< Normalisation \f$[M_{\odot} \cdot kpc^{-3}]\f$
   double rscale                     = -1;    //!< Scale radius \f$[kpc]\f$
   double rdelta                     = -1;    //!< Radius of the halo \f$[kpc]\f$
   double shapeParam1                = -1;    //!< \f$\alpha\f$ (kZHAO or kEINASTO) or \f$n\f$ (kEINASTO)
   double shapeParam2                = -1;    //!< \f$\beta\f$ (kZHAO) or unused (kEINASTO and kEINASTO_N)
   double shapeParam3                = -1;    //!< \f$\gamma\f$ (kZHAO) or unused (kEINASTO and kEINASTO_N)
   structTriaxiality triaxial;                //!<
};

struct c_params_for_rootfinding {
   double fac;
};

struct Rdelta_params_for_rootfinding {
   double rho_s;        //!< Clump density normalisation [Msol/kpc^{3}]
   double r_s;          //!< Clump scale radius, based on mdelta_to_rscale() [kpc]
   double par1;         //!< Shape parameter #1
   double par2;         //!< Shape parameter #2
   double par3;         //!< Shape parameter #3
   int    card_profile; //!< card_profile
   double Delta_c;
   double z;
};

struct rbias_params_for_rootfinding {
   double mdelta;             //!< Mass of host halo [Msol]
   double rdelta;             //!< Outer halo bound [kpc] of host
   double rho_s;              //!< scale density [Msol/kpc^{3}] of host
   double r_s;                //!< scale radius  [kpc] of host
   double par1;               //!< Shape parameter #1 of host
   double par2;               //!< Shape parameter #2 of host
   double par3;               //!< Shape parameter #3 of host
   int    card_profile;       //!< DM halo profile of host
   double fsub;               //!< substructure mass fraction in host
};

struct Mdelta_params_for_rootfinding {
   double mdelta;             //!< Mass mdelta1 or mdelta_ref, depending on bool find_m2_given_m1
   double Delta1;             //!< Overdensity of mdelta1
   int    card_cdelta;        //!< cdelta_Ref - mdelta_ref relation
   double par1;               //!< Shape parameter #1
   double par2;               //!< Shape parameter #2
   double par3;               //!< Shape parameter #3
   int    card_profile;       //!< DM halo profile
   double z;                  //!< redshift of the halo
   double xpos;                  //!< distance r/rhost from the host halo center in case of a distance-dependent c(M) choice
   bool   find_mref_given_m1; //!< specifies whether the function is solved for mdelta_ref or mdelta1.
};

double card_cdelta_to_Delta(int card_cdelta, double const &z);
double card_mf_to_Delta(int card_mf);

double delta_x_to_delta_crit(double const &Delta_x0, int const card_delta_ref, double const &z);
double delta_crit_to_delta_x(double const &Delta_c, int const card_delta_ref, double const &z);

double dpdc(double *c, double par_dpdc[3]);
void   dpdc(double &c, double par_dpdc[3], double &res);
void   dpdc_lum(double &c, double par_subs[25], double &res);
void   dpdc_lum2(double &c, double par_subs[25], double &res);
double dpdlogc(double *log_c, double par_dpdc[3]);
void   dpdlogc(double &log_c, double par_dpdc[3], double &res);

double dpdlogm(double *log_m, double par_dpdm[2]);
void   dpdlogm(double &log_m, double par_dpdm[2], double &res);
double dpdm(double *m, double par_dpdm[2]);
void   dpdm(double &m, double par_dpdm[2], double &res);
void   dpdm_lum_r_m(double &m, double par_subs[25], double &res);
void   dpdm_lum2_r_m(double &m, double par_subs[25], double &res);
void   dpdm_m(double &m, double par_dpdm[2], double &res);
void   dpdm_setnormprob(double par_dpdm[2], double &m1, double &m2, double const &eps);

void   dpdv_lum_r(double &r, double par[28], double &res);
void   dpdv_lum2_r(double &r, double par[28], double &res);
void   r2dpdv_lum_r(double &r, double par[28], double &res);
void   r2dpdv_lum2_r(double &r, double par[28], double &res);
void   r2dpdv_c(double &r, double par_dpdv_c[19], double &res);
double dpdv_lcosalphabeta(double *x, double par_dpdvmod[10]);
double dpdv_lthetapsi(double *x, double par_dpdv[8]);
double dpdv_rthetapsi(double *x, double par_dpdv[8]);
double dpdv_xyz(double *x, double par_dpdv[8]);
void   dpdv_setnormprob(double par_dpdv[7], double const &eps);

void   find_fracjpointlike_dist(double par_tot[7], double const &frac, double &dist, int iter = 0);
void   find_fracjtot_alphaint(double par_tot[10], double const &frac, double &alphaint_rad, int iter = 0);
void   find_fracjtot_alphaint(double par_tot[10], double const &mtot, double const &f_dm, double par_dpdv[10], double par_subs[25], double const &ntot_subs, double const &jtot, double const &frac, double &alphaint_rad, int iter = 0);
void   find_fraclumref_radius(double par_tot[6], double const &r_ref, double const &lum_ref, double const &frac, double &radius, int iter = 0);
double find_halo_physicalsize(double &r, double par_size[18]);
double frac_nsubs_in_foi(double par_dpdv[10], double const &psi_los, double const &theta_los, double const &l1, double const &l2, double const &eps);
double frac_nsubs_in_m1m2(double par_dpdm[2], double &m1, double &m2, double const &eps);

double jcrossprod_continuum(double const &mtot, double par_tot[10], double const &psi_los, double const &theta_los, double const &eps, double const &f_dm, double par_dpdv[10]);
double jcrossprod_continuum(double const &mtot, double par_tot[10], double const &psi_los, double const &theta_los, double const &l1, double const &l2, double const &eps, double const &f_dm, double par_dpdv[10]);
double jsmooth(double par_tot[10], double const &psi_los, double const &theta_los, double const &eps);
double jsmooth(double par_tot[10], double const &psi_los, double const &theta_los, double const &l1, double const &l2, double const &eps);
double jsmooth_mix(double const &mtot, double par_tot[10], double const &psi_los, double const &theta_los, double const &eps, double const &f_dm, double par_dpdv[10]);
double jsmooth_mix(double const &mtot, double par_tot[10], double const &psi_los, double const &theta_los, double const &l1, double const &l2, double const &eps, double const &f_dm, double par_dpdv[10]);
double jsub_continuum(double const &ntot_subs, double par_dpdv[10], double const &psi_los, double const &theta_los, double const &l1, double const &l2, double par_subs[25], double &m1, double &m2);



double lum_singlehalo_nosubs(double par_tot[7], double const &eps, bool is_force_int = false, bool is_verbose = false);
double lum_singlehalo_nosubs_mix(double par_mix[21], double const &eps, bool is_force_int = false, bool is_verbose = false);
double lum_singlehalo_subs(double par_host[26], bool is_force_int = false, bool is_verbose = false);

double mass_singlehalo(double par_tot[7], double const &eps, bool is_force_int = false, bool is_verbose = false);
double mass_triaxialhalo(double par_tot[7], double axes[3], double const &eps, bool is_verbose = false);

double mdelta_to_cdelta(double const &mdelta, double const &Delta_c, int card_cdelta, double par_prof[4], double const &z, double const &xpos = -1);
void   mdelta_to_innerslope(double const &mdelta, double const Delta_c, int const card_cdelta, double par_prof[4], double const &z, double const &xpos = -1);

void   mdelta_to_par(double par_tot[7], double const &mdelta, double const &Delta_c, int const card_cdelta, double par_prof[4], double const &eps, double const &z, double const &cdelta = -1, double const &xpos = -1);

double mdelta_to_Rdelta(double const &mdelta, double const &Delta_c, double const &z);
double mdelta_to_rscale(double const &mdelta, double const &Delta_c, int const card_cdelta, double par_prof[4], double const &z, double const &xpos = -1);
double mdelta1_to_mdelta2(double const &mdelta1, double const &Delta1, int const  card_cdelta, double par_prof[4], double const &z,  double Delta2 = -1., double const &xpos = -1);

double nsubtot_from_msubtot(double const &mtot_subs, double &mmin_subs,  double &mmax_subs, double par_dpdm[2], double const &eps);
double nsubtot_from_nsubm1m2(double par_dpdm[2], double &m1, double &m2, double const &nsub_in_m1m2, double const &eps);

void   print_pardpdv(double par_dpdv[10]);
void   print_parhost(double par_host[26]);
void   print_parmix(double par_mix[21]);
void   print_parsubs(double par_subs[25]);
void   print_partot(double par_tot[10]);

double Rdelta_to_mdelta(double const &Rdelta, double const &Delta_c, double const &z);
double shapeparams_to_Rdelta(double par_tot[6], double const &Delta_c, double const &z);
double shapeparams_to_Delta(double par_tot[6], double const &Rdelta, double const &z);

double solve_c_SIDM_Burkert(double c, void *p);
double solve_Rdelta(double Rdelta, void *p);
double solve_Mdelta(double Mdelta, void *p);
double solve_cdelta_celine(double const &Mdelta, double const &Delta_c, int const  card_cdelta, double par_prof[4], double const &z, double const &xpos);
double solve_rbias(double rbias, void *p);

void   stref17_load_cmin200_vs_rgal(string const &filename, vector <double> &rgal_kpc, vector <double> &cmin_200);
void   stref17_load_lcrit(string const &filename, vector <double> &stref17_mmin, vector <double> &stref17_mmax, vector <double> &stref17_lcrit);
void   stref17_load_rt2rs_vs_rgal_c200(string const &filename, vector <double> &rgal_kpc, vector <double> &c200, vector< vector<double> > &rt2rs);
#endif

/*! \file clumps.h
  \brief Clump functions: concentration, spatial and mass distribution, ....
*/
