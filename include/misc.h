#ifndef _CLUMPY_MISC_H_
#define _CLUMPY_MISC_H_

// C++ std libraries
#include <string>
#include <vector>
using namespace std;

// CLUMPY includes
#include <healpix_fits.h>
#include <inlines.h>

// GSL includes
#include <gsl/gsl_roots.h>

// ROOT includes
#if IS_ROOT
#include <TText.h>
#endif

bool      execute_test(const string &mode, const vector<string> &filenames_test, const string &exe_dir,
                       const string &test_dir, const string &compare_dir, const string &special_command = "none");

double    find_max_value(vector<double> &array, int i_start = 0, int i_end = -1);
double    find_min_value(vector<double> &array, int i_start = 0, int i_end = -1);
void      find_x_logstep(double const &y_ref, double &res, double x1, double x2, void fn(double &, double *, double &), double *par, double const &tol, double y = 0., double y2 = 0., int n_iter = 0);

/*! \brief Sets the global variables gPATH_TO_CLUMPY_EXE (the absolute path of the executed binary)  and gPATH_TO_CLUMPY_HOME */
void      get_clumpy_dirs(string const &argv0                                  //!< [in] argv[0] string
                         );
string    get_path(string const &str, bool is_ensure_trailing_dash = true);
double    goldenmin_logstep(double const &ax, double const &bx, double const &cx, void fn(double &, double *, double &), double *par, double const &tol, bool is_min_or_max, double &xmin_or_xmax);

double    interp1D(double const &x_eval, const vector<double> &x_base, const vector<double> &y_base, const int card_interp, bool no_error = false);
double    interp2D(double const &x_eval, double const &y_eval, const vector<double> &x_base, const vector<double> &y_base, const vector< vector<double> > &z_base, const int card_interp, bool no_error = false);

/*! \brief Returns true if a character is not ASCII, false otherwise */
bool      isNotASCII(const char s                                              //!< [in] character to check
                    );
bool      isNumeric(const char *s);

double    linlin_interp(double const &x_eval, double const &x1, double const &x2, double const &y1, double const &y2);
double    linlog_interp(double const &x_eval, double const &x1, double const &x2, double const &y1, double const &y2);
double    loglin_interp(double const &x_eval, double const &x1, double const &x2, double const &y1, double const &y2);
double    loglog_interp(double const &x_eval, double const &x1, double const &x2, double const &y1, double const &y2);
double    spline_interp(double const &x_eval, const vector<double> &x_base, const vector<double> &y_base);

double    linlin_interp2D(double const &x_eval, double const &y_eval, fix_arr<double, 2> &x_base, fix_arr<double, 2> &y_base, fix_arr<double, 4> &z_base);
double    linlog_interp2D(double const &x_eval, double const &y_eval, fix_arr<double, 2> &x_base, fix_arr<double, 2> &y_base, fix_arr<double, 4> &z_base);
double    loglin_interp2D(double const &x_eval, double const &y_eval, fix_arr<double, 2> &x_base, fix_arr<double, 2> &y_base, fix_arr<double, 4> &z_base);
double    loglog_interp2D(double const &x_eval, double const &y_eval, fix_arr<double, 2> &x_base, fix_arr<double, 2> &y_base, fix_arr<double, 4> &z_base);

vector<double> make_1D_grid(double &x_min, const double &x_max, const int n_x, const bool is_x_log);
bool      makedir(const char *dir);
int       name_to_indexinlist(string const &name, vector<string> const &names);

#if IS_ROOT
TText    *orphanCLUMPYadonplots();
#endif

void      print_error(string library, string function, string message, bool is_abort = true);
void      print_warning(string library, string function, string message);

vector< vector<double> > read_ascii(string const &filename, int const ncolumns = -1, bool const is_monotonous = false, int const column_start = 0);

string    removeblanks_from_startstop(string const &str);
string    removeslashes_from_end(string const &str);
string    removechars(string const &str, string const &delimiters);
void      replace_substring(string &s, const string &search, const string &replace);
void      resolve_envvar(string &str);

void      root_trimname4object(string &s);
#if IS_ROOT
int       rootcolor(int i);
int       rootmarker(int i);
void      rootstyle_set2CLUMPY();
#endif

double    rootsolver_brent(double fn(double &, double *), double par[], double x1, double x2, double tol, bool is_softfail = false);
double    rootsolver_gsl(const gsl_root_fsolver_type *T, gsl_function Fx, double &x_lo, double &x_hi, double &precision, int &return_status);

double    round(const double &x, const int digits);

double    sigmoid_window(double const &x, double const &x0, double const &sigma);

void      string2list(string const &str, string const &delimiters, vector<string> &list);
void      string2list(string const &str, string const &delimiters, vector<float> &list);
void      string2list(string const &str, string const &delimiters, vector<double> &list);
void      string2list(string const &str, string const &delimiters, vector<int> &list);
string    string_fixlength(string const &str, size_t string_out_length);

/*! \brief Wraps a string according to a given line length. Additionally, the number of leading spaces can be specified (leading spaces will be added to the specified line length). */
string    string_wrap(string const &str,                                       //!< [in] String to wrap
                      size_t line_length = 72,                                 //!< [in] Line length around which to wrap
                      size_t leading_spaces = 0                                //!< [in] Number of additional leading spaces
                     );

void      sys_safe_execution(string const &command);
string    sys_command(string const &command);

string    upper_case(string const &s);

//______________________________________________________________________________
template<typename T>
void      pushback_array2vector(vector< vector<T> > &vec, const T *array, const int size)
{
   vector<T> tmp;
   tmp.assign(array, array + size / sizeof array[0]);
   vec.push_back(tmp);
   return;
};

//______________________________________________________________________________
template<typename T>
int binary_search(const vector<T> &vec, const T value)
{
   //--- Wrapper around C++'s lower_bound() function, drawing an error
   //     when value not vector's range.
   //     Also, comprises [first, last) range of vector,
   //     and allows for some tolerance below first and above last.
   // INPUTS:
   //  vec     Sorted vector to search for the element
   //  value   Value to search in the vector.
   // OUTPUT:
   //   index of the element in vec smaller or equal to value.
   int index = lower_bound(vec.begin(), vec.end(), value) - vec.begin();

   int sign_lo = sign(vec[0]);
   int sign_up = sign(vec.back());

   if (index == int(vec.size())) {
      if (value < vec[index - 1] * (1 + sign_up * SMALL_NUMBER)) index = index - 2; // go back two to not crash when accessing index + 1
      else index = -1;
   } else if (value < vec[0]) {
      if (value > vec[0] * (1 - sign_lo * SMALL_NUMBER)) index = 0;
      else index = -1;
   } else if (vec[index] != value) index--; // choose the lower index

   if (index == -1) {
      char tmp[256];
      for (int i = 1; i < int(vec.size()); ++i) {
         if (vec[i] < vec[i - 1]) {
            print_error("misc.h", "binary_search()", "Input vector must be sorted - it is not!");
         }
      }
      sprintf(tmp, "Value %.8e exceeds range [%.8e,%.8e) contained in vector.", double(value), double(vec[0]), double(vec[vec.size() - 1]));
      print_error("misc.h", "binary_search()", string(tmp));
   }
   return index;
}

#endif

/*! \file misc.h
  \brief Miscellaneous simple functions used in several places

 <CENTER>________________________________________</CENTER>\n

The functions defined here can be broadly listed as follows:

   -# <i>Finds and returns</i> \f$ x_{\rm ref}\f$ for which \f$ fn(x_{\rm ref})=y_{\rm ref} \f$ in a given interval \f$[x_{1},x_{2}]\f$ (adapted from <a href=http://nr.com/ target="_blank">Numerical Recipes</a>)
      - \c find_max_value() and find_min_value()
           \param[in]  array      Vector of double
           \returns    Minimum (or maximum) value found from array
   \n\n
      - \c find_x_logstep()
           \param[in]  y_ref      Reference value
           \param[out] res        Value for which \f$|fn(x_{\rm ref})-y_{\rm ref}|<tol\times y_{\rm ref}\f$ in the interval \f$[x_{1},x_{2}]\f$
           \param[in]  x1         Lower value of interval
           \param[in]  x2         Upper value of interval
           \param[in]  fn         Function to use
           \param[in]  par[]      Parameters of \c fn
           \param[in]  tol        Relative precision to search for y_ref
           \param[in]  y1         fn(x1)
           \param[in]  y2         fn(x2)
           \param[in]  n_iter     Number of iteration in recursion
   \n\n
   -# <i>Search for minimum/maximum of a function</i> (adapted from <a href=http://nr.com/ target="_blank">Numerical Recipes</a>)
      - \c goldenmin_logstep()
           \param[in]  ax         1st point (ax<bx<cx) at which to evaluate \c fn
           \param[in]  bx         2nd point (ax<bx<cx) at which to evaluate \c fn
           \param[in]  cx         3rd point (ax<bx<cx) at which to evaluate \c fn
           \param[in]  fn         Function to use
           \param[in]  par[]      Parameters of \c fn
           \param[in]  tol        Tolerance (relative precision) for which the search stops
           \param[in]  is_min_or_max Whether to search for the minimum or the maximum
           \param[in]  xmin_or_xmax  Minimum/maximum x value for which the function is min/max
           \returns    Minimum/maximum of the function \c fn within the user-defined relative precision \c tol.
   \n\n
   -# <i>Search zero of a function</i> (adapted from <a href=http://nr.com/ target="_blank">Numerical Recipes</a>)
      - \c rootsolver_brent()
           \param[in]  fn         Function to use
           \param[in]  par[]      Parameters of \c fn
           \param[in]  x1         Lower value of interval
           \param[in]  x2         Upper value of interval
           \param[in]  tol        Tolerance (relative precision) for which the search stops
           \returns    Zero of the function \c fn within the user-defined relative precision \c tol.
   \n\n
   -# <i>Interpolation functions</i>
           \param[in]  x          Position at which to interpolate
           \param[in]  x1         Lower position for which y is known
           \param[in]  x2         Upper position for which y is known
           \param[in]  y1         Value of \f$y(x_1)\f$
           \param[in]  y2         Value of \f$y(x_2)\f$
           \returns
                          - \c linlin_interp(): \f$\displaystyle y_{interp} = y_1 + (y_2-y_1)\cdot\frac{(x-x_1)}{(x_2-x_1)}\f$
                          - \c linlog_interp(): \f$\displaystyle y_{interp} = y_1 \cdot \left(\frac{y_2}{y_1}\right)^{[(x-x_1)/(x_2-x_1)]}\f$
                          - \c loglin_interp(): \f$\displaystyle y_{interp} = y_1 + (y_2-y_1)\cdot \frac{\log(x/x_1)}{\log(x_2/x_1)}\f$
                          - \c loglog_interp(): \f$\displaystyle y_{interp} = y_1 \cdot \left(\frac{x}{x_1}\right)^{\log(y_2/y_1)/\log(x_2/x_1)}\f$
                          .
   \n\n\n
   -# <i>Manipulation of strings and vectors</i>
      - \c makedir()
           \param[in]  dir        Relative or absolute directory or directory tree.
           \returns    True if dir had to be created, false if it was already existing
      \n\n
      - \c name_to_indexinlist()
           \param[in]  name       String to locate in a list
           \param[in]  names      Vector of strings on which to search for
           \returns    Index for which names[i]=name (returns -1 if no match)
      \n\n
      - \c removeblanks_from_startstop()
           \param[in]  str        String to analyse
           \returns    String from which spaces (' ' or tab) are removed if before or after characters
      \n\n
      - \c removeblanks_from_startstop_char()
           \param[in]  char_in    Character to analyse
           \param[in]  len        Length of input character (char_in)
           \param[out] char_out   Trimmed character
           \returns    Length of the trimmed output character \c char_out
      \n\n
      - \c removechars()
           \param[in]  str        String to analyse
           \param[in]  delimiters Characters to drop in str
           \returns    String from which characters present in \c delimiters are removed
      \n\n
      - \c string2list()
           \param[in]  str        String to parse
           \param[in]  delimiters List of \c char to use to parse str
           \param[out] list       Vector of \c int (or float, double, string) to parse \c str into
                                  (ex. \c str = "1,2,3" with \c delimiters = "," gives list[0]="1", list[1]="2", list[3]="2")
      \n\n
      - \c string_fixlength()
           \param[in]  str        String to fix
           \param[in]  string_out_length Total fixed length of output string
           \returns    \c string of length \c string_out_length (if \c str shorter, spaces
                       are added, if longer, \c str is truncated)
      \n\n
      - \c upper_case()
           \param[in]  s          Input string
           \returns    Upper-case version of the string
      \n\n\n
   -# <i>ROOT related functions (for displays)</i>
      - \c orphanCLUMPYadonplots(): return TText to add in plots (CLUMPY link advertisement)
      \n\n
      - \c root_trimname4object(): discards  special caracter "{}*~#$ -:?!,;/+=[]()." and replaces
               '(' and ')' by '_' in s to be ROOT compliant, i.e., so that the trimmed string can be
               used as a root object name.
           \param[in,out]  str        String before and after trimming
      \n\n
      - \c rootstyle_set2CLUMPY(): sets style for plots in CLUMPY (ROOT style). Note that when called,
             this function leads to a memory leak (but it is unimportant, as it is only at the display stage).
      \n\n
      - Useful whenever multiple graphs are plotted on the same plot (to allocate color and marke style)
           \param[in]  i          Index
           \returns
                       - \c rootcolor(): ROOT color
                       - \c rootmarker(): ROOOT marker (useful when multiple graphs are plotted on the same plot)
                       .
      \n\n

*/
