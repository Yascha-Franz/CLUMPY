#ifndef _CLUMPY_PARAMS_H_
#define _CLUMPY_PARAMS_H_

// ROOT includes
#if IS_ROOT
#include <TApplication.h>
#include <TText.h>
#endif

// CLUMPY includes
#include "../include/healpix_fits.h"

// c++ std libraries
#include <string>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
using namespace std;


//------------------------------------------------------------------------------
// Global variables for available enumerators (except gN_SHAPE_PARAMS and gN_XYZ):
//   -> number (gN_XXX), list (gENUM_XXX), and names (gNAMES_XXX)
const int            gN_ANISOTROPYPROFILE = 3;                                  //!< Number of anisotropy profiles for the Jeans analysis
const int            gN_LIGHTPROFILE = 6;                                       //!< Number of light profiles for the Jeans analysis
const int            gN_CVIR_DIST = 2;                                          //!< Number of \f$c_{\Delta}\f$ distributions (corresponding names defined in \c gNAMES_CVIR_DIST)
const int            gN_CDELTAMDELTA = 17;                                      //!< Number of \f$c_{\Delta}-M_{\Delta}\f$ parametrisations (corresponding names defined in \c gNAMES_CDELTAMDELTA)
const int            gN_FINALSTATE = 5;                                         //!< Number of final state defined (corresponding names defined in \c gNAMES_FINALSTATES)
const int            gN_MASSFUNCTION = 9;                                       //!< Number of final state defined (corresponding names defined in \c gNAMES_MASSFUNCTION)
const int            gN_NUFLAVOUR = 3;                                          //!< Number of \f$\nu\f$ flavours (corresponding names defined in \c gNAMES_NUFLAVOUR)
const int            gN_PP_BR = 28;                                             //!< Number of branching ratios (for gamma-ray and neutrino production)
const int            gN_PP_SPECTRUMMODEL = 5;                                   //!< Number of particle physics spectra parametrisations (corresponding names defined in \c gNAMES_PP_SPECTRUMMODEL)
const int            gN_PROFILE = 11;                                           //!< Number of DM profile families parametrisations (corresponding names defined in \c gNAMES_PROFILE)
const int            gN_ABSORPTIONPROFILE = 12;                                 //!< Number of ABSORPTION profiles parametrisations
const int            gN_SHAPE_PARAMS = 3;                                       //!< Number of shape parameters for DM profiles (\c gENUM_PROFILE)
const int            gN_TYPEHALOES = 4;                                         //!< Number of halo types (corresponding names defined in \c gNAMES_TYPEHALOES)
const int            gN_XYZ = 3;                                                //!< Dummy variable for number of dimensions
const int            gN_INPUTPARAMS = 215;                                      //!< Number of total input parameters which must be provided in the input file
const int            gN_SIMUMODES = 44;                                         //!< Number of simulation modes with clumpy.
const int            gN_WINDOWFUNC = 3;                                         //!< Number of available window functions.
const int            gN_DELTA_REF = 3;                                          //!< Number of available Reference models for the overdensity factor Delta.
const int            gN_GROWTHFACTOR = 3;                                       //!< Number of models for the growth factor to compute sigma^2 at redshift z>0

//! Models of the \f$\gamma\f$-ray extinction \f$\tau(E_{\gamma},\,z)\f$ on the extragalactic background light
enum gENUM_ABSORPTIONPROFILE {
   kNOEBL,
   kFRANCESCHINI08,                                                             //!< <a href=../enumerators_tauEBL.html>See user documentation</a>
   kFRANCESCHINI17,                                                             //!< <a href=../enumerators_tauEBL.html>See user documentation</a>
   kFINKE10,                                                                    //!< <a href=../enumerators_tauEBL.html>See user documentation</a>
   kDOMINGUEZ11_REF,                                                            //!< <a href=../enumerators_tauEBL.html>See user documentation</a>
   kDOMINGUEZ11_LO,                                                             //!< <a href=../enumerators_tauEBL.html>See user documentation</a>
   kDOMINGUEZ11_UP,                                                             //!< <a href=../enumerators_tauEBL.html>See user documentation</a>
   kGILMORE12_FIDUCIAL,                                                         //!< <a href=../enumerators_tauEBL.html>See user documentation</a>
   kGILMORE12_FIXED,                                                            //!< <a href=../enumerators_tauEBL.html>See user documentation</a>
   kINOUE13_REF,                                                                //!< <a href=../enumerators_tauEBL.html>See user documentation</a>
   kINOUE13_LO,                                                                 //!< <a href=../enumerators_tauEBL.html>See user documentation</a>
   kINOUE13_UP                                                                  //!< <a href=../enumerators_tauEBL.html>See user documentation</a>
};

//! Anisotropy profiles \f$\beta_{\rm ani}(r)\equiv 1-\bar{v_{\theta}^2}(r)/\bar{v_r^2}(r)\f$ for the Jeans analysis
enum gENUM_ANISOTROPYPROFILE {
   kCONSTANT,                                                                   //!< <a href=../enumerators_anisotropyprofiles.html>See user documentation</a>
   kBAES,                                                                       //!< <a href=../enumerators_anisotropyprofiles.html>See user documentation</a>
   kOSIPKOV                                                                     //!< <a href=../enumerators_anisotropyprofiles.html>See user documentation</a>
};

//! Concentration distributions (PDF) \f$d{\cal P}_c(c,M)/dc\f$ for DM clumps of mass \f$M\f$
enum gENUM_CDELTA_DIST {
   kDIRAC,                                                                      //!< No longer connected to an input parameter
   kLOGNORM                                                                     //!< No longer connected to an input parameter
};

//! Concentration-mass relationships \f$c_\Delta-M_\Delta\f$ (with \f$\Delta=200,\,{\rm vir},\dots\f$) to link \f$c_\Delta=R_\Delta/r_{-2}\f$ to \f$M\f$ for DM halos
enum gENUM_CDELTAMDELTA {
   kB01_VIR,                                                                    //!< <a href=../enumerators_cdelta.html>See user documentation</a>
   kB01_VIR_RAD,                                                                //!< <a href=../enumerators_cdelta.html>See user documentation</a>
   kENS01_VIR,                                                                  //!< <a href=../enumerators_cdelta.html>See user documentation</a>
   kNETO07_200,                                                                 //!< <a href=../enumerators_cdelta.html>See user documentation</a>
   kDUFFY08F_VIR,                                                               //!< <a href=../enumerators_cdelta.html>See user documentation</a>
   kDUFFY08F_200,                                                               //!< <a href=../enumerators_cdelta.html>See user documentation</a>
   kDUFFY08F_MEAN,                                                              //!< <a href=../enumerators_cdelta.html>See user documentation</a>
   kETTORI10_200,                                                               //!< <a href=../enumerators_cdelta.html>See user documentation</a>
   kPRADA12_200,                                                                //!< <a href=../enumerators_cdelta.html>See user documentation</a>
   kGIOCOLI12_VIR,                                                              //!< <a href=../enumerators_cdelta.html>See user documentation</a>
   kPIERI11_VIALACTEA,                                                          //!< <a href=../enumerators_cdelta.html>See user documentation</a>
   kPIERI11_AQUARIUS,                                                           //!< <a href=../enumerators_cdelta.html>See user documentation</a>
   kSANCHEZ14_200,                                                              //!< <a href=../enumerators_cdelta.html>See user documentation</a>
   kMOLINE17_200,                                                               //!< <a href=../enumerators_cdelta.html>See user documentation</a>
   kLUDLOW16_200,                                                               //!< <a href=../enumerators_cdelta.html>See user documentation</a>
   kCORREA15_PLANCK_200,                                                        //!< <a href=../enumerators_cdelta.html>See user documentation</a>
   kROCHA13_SIDM_VIR                                                            //!< <a href=../enumerators_cdelta.html>See user documentation</a>
};

//! Final states available for DM fluxes (only \c kGAMMA and \c kNEUTRINO for \f$J\f$ and \f$D\f$ calculations)
enum gENUM_FINALSTATE {
   kGAMMA,                                                                      //!< <a href=../enumerators_finalstates.html>See user documentation</a>
   kNEUTRINO,                                                                   //!< <a href=../enumerators_finalstates.html>See user documentation</a>
   kANTIPROTON,                                                                 //!< <a href=../enumerators_finalstates.html>See user documentation</a>
   kPOSITRON,                                                                   //!< <a href=../enumerators_finalstates.html>See user documentation</a>
   kELECTRON                                                                    //!< <a href=../enumerators_finalstates.html>See user documentation</a>
};

//! Surface brightness \f$\Sigma(R)\equiv I(R)\f$ in 2D (\f$R\f$ the projected radius) or density \f$\rho(r) \equiv\nu(r)\f$ in 3D used to describe the light profile (stars) in the Jeans analaysis
enum gENUM_LIGHTPROFILE {
   kEXP2D,                                                                      //!< <a href=../enumerators_lightprofiles.html>See user documentation</a>
   kEXP3D,                                                                      //!< <a href=../enumerators_lightprofiles.html>See user documentation</a>
   kKING2D,                                                                     //!< <a href=../enumerators_lightprofiles.html>See user documentation</a>
   kPLUMMER2D,                                                                  //!< <a href=../enumerators_lightprofiles.html>See user documentation</a>
   kSERSIC2D,                                                                   //!< <a href=../enumerators_lightprofiles.html>See user documentation</a>
   kZHAO3D                                                                      //!< <a href=../enumerators_lightprofiles.html>See user documentation</a>
};

//! Choice of mass function for extragalactic halo population
enum gENUM_MASSFUNCTION {
   kTINKER08,                                                                   //!<  <a href=../enumerators_massfunction.html>See user documentation</a>
   kTINKER08_N,                                                                 //!<  <a href=../enumerators_massfunction.html>See user documentation</a>
   kTINKER10,                                                                   //!<  <a href=../enumerators_massfunction.html>See user documentation</a>
   kBOCQUET16_HYDRO,                                                            //!<  <a href=../enumerators_massfunction.html>See user documentation</a>
   kBOCQUET16_DMONLY,                                                           //!<  <a href=../enumerators_massfunction.html>See user documentation</a>
   kJENKINS01,                                                                  //!<  <a href=../enumerators_massfunction.html>See user documentation</a>
   kSHETHTORMEN99,                                                              //!<  <a href=../enumerators_massfunction.html>See user documentation</a>
   kPRESSSCHECHTER74,                                                           //!<  <a href=../enumerators_massfunction.html>See user documentation</a>
   kRODRIGUEZPUEBLA16_PLANCK                                                    //!<  <a href=../enumerators_massfunction.html>See user documentation</a>
};

//! Neutrino flavours selections (if \c kNEUTRINO selected as a final state)
enum gENUM_NUFLAVOUR {
   kNUE,                                                                        //!< <a href=../enumerators_nuflavours.html>See user documentation</a>
   kNUMU,                                                                       //!< <a href=../enumerators_nuflavours.html>See user documentation</a>
   kNUTAU                                                                       //!< <a href=../enumerators_nuflavours.html>See user documentation</a>
};

//! Particle physics elementary spectra for DM annihilation and/or decay
enum gENUM_PP_SPECTRUMMODEL {                                                   //!< <a href=../enumerators_spectra.html>See user documentation</a>
   kBERGSTROM98,                                                                //!< <a href=../enumerators_spectra.html>See user documentation</a>
   kTASITSIOMI02,                                                               //!< <a href=../enumerators_spectra.html>See user documentation</a>
   kBRINGMANN08,                                                                //!< <a href=../enumerators_spectra.html>See user documentation</a>
   kCIRELLI11_EW,                                                               //!< <a href=../enumerators_spectra.html>See user documentation</a>
   kCIRELLI11_NOEW                                                              //!< <a href=../enumerators_spectra.html>See user documentation</a>
};

//! Dark matter profiles and/or spatial distribution. The suffix \c _SUB indicates a profile that should only be used for the spatial distribution of clumps \f$d{\cal P}_V/dV\f$, while the suffix \c _INNERSUB applies only for the inner profile of a population of subhalos (cannot be used for the host halo)
enum gENUM_PROFILE {
   kHOST = -1,                                                                  //!< <a href=../enumerators_dm_profiles.html>See user documentation</a>
   kEINASTO,                                                                    //!< <a href=../enumerators_dm_profiles.html>See user documentation</a>
   kEINASTO_N,                                                                  //!< <a href=../enumerators_dm_profiles.html>See user documentation</a>
   kZHAO,                                                                       //!< <a href=../enumerators_dm_profiles.html>See user documentation</a>
   kBURKERT,                                                                    //!< <a href=../enumerators_dm_profiles.html>See user documentation</a>
   kDPDV_GAO04,                                                                 //!< <a href=../enumerators_dm_profiles.html>See user documentation</a>
   kDPDV_SIGMOID_EINASTO,                                                       //!< <a href=../enumerators_dm_profiles.html>See user documentation</a>
   kDPDV_SPRINGEL08_ANTIBIASED,                                                 //!< <a href=../enumerators_dm_profiles.html>See user documentation</a>
   kDPDV_SPRINGEL08_FIT,                                                        //!< <a href=../enumerators_dm_profiles.html>See user documentation</a>
   kDPDV_PIERI11,                                                               //!< <a href=../enumerators_cdelta.html>See user documentation</a>
   kISHIYAMA14,                                                                 //!< <a href=../enumerators_dm_profiles.html>See user documentation</a>
   kNODES                                                                       //!< <a href=../enumerators_dm_profiles.html>See user documentation</a>
};

//! Types of DM halos for which specific (but for all objects of this type) parametrisations can be set
enum gENUM_TYPEHALOES {
   kDSPH,                                                                       //!< Dwarf spheroidal galaxies (inside or outside the Milky Way)
   kGALAXY,                                                                     //!< Galaxies (but not the Milky Way)
   kCLUSTER,                                                                    //!< Remote clusters of galaxies
   kEXTRAGAL                                                                    //!< Generic cosmic halo object with same properties on all scales from cluster to mictrohalo size
};

//! Types of window functions for the cosmic 3D matter power spectrum
enum gENUM_WINDOWFUNC {
   kTOP_HAT,                                                                    //!< <a href=../enumerators_windowfunc.html>See user documentation</a>
   kGAUSS,                                                                      //!< <a href=../enumerators_windowfunc.html>See user documentation</a>
   kSHARP_K,                                                                    //!< <a href=../enumerators_windowfunc.html>See user documentation</a>
};

//! Reference choices for the overdensity defining the size of a halo
enum gENUM_DELTA_REF {
   kRHO_CRIT,                                                                   //!< <a href=../enumerators_delta.html>See user documentation</a>
   kRHO_MEAN,                                                                   //!< <a href=../enumerators_delta.html>See user documentation</a>
   kBRYANNORMAN98,                                                              //!< <a href=../enumerators_delta.html>See user documentation</a>
};

//! Interpolation types
enum gENUM_INTERPOLTYPE {
   kLINLIN,                                                                     //!< x-y linear interpolation
   kLINLOG,                                                                     //!< x-log(y) linear interpolation
   kLOGLIN,                                                                     //!< log(x)-y linear interpolation
   kLOGLOG,                                                                     //!< log(x)-log(y) linear interpolation
   kSPLINE,                                                                     //!< Spline interpolation
};

//! Calculation of the growth of cosmological perturbations
enum gENUM_GROWTHFACTOR {
   kPKZ_FROMFILE,                                                               //!< <a href=../enumerators_growthfactor.html>See user documentation</a>
   kCARROLL92,                                                                  //!< <a href=../enumerators_growthfactor.html>See user documentation</a>
   kHEATH77,                                                                    //!< <a href=../enumerators_growthfactor.html>See user documentation</a>
};

extern const char    gNAMES_ABSORPTIONPROFILE[gN_ABSORPTIONPROFILE][50];        //!< Extragalactic gamma-ray absorption models: FRANCESCHINI08, DOMINGUEZ11, ...
extern const char    gNAMES_ANISOTROPYPROFILE[gN_ANISOTROPYPROFILE][50];        //!< Anisotropy profiles names are CONSTANT, BAES, OSIPKOV, ... (<a href=../enumerators_anisotropyprofiles.html>See user documentation</a>)
extern const char    gNAMES_LIGHTPROFILE[gN_LIGHTPROFILE][50];                  //!< Light profiles names are EXP2D, EXP3D, KING2D, ... ( <a href=../enumerators_lightprofiles.html>See user documentation</a>)
extern const char    gNAMES_CVIR_DIST[gN_CVIR_DIST][50];                        //!< \f$c_{\Delta}\f$ distributions are DIRAC, LOGNORM
extern const char    gNAMES_CDELTAMDELTA[gN_CDELTAMDELTA][50];                  //!< \f$c_{\Delta}-M_{\Delta}\f$ relationship names are <a href=http://cdsads.u-strasbg.fr/abs/2001MNRAS.321..559B target="_blank">B01_VIR</a>, <a href=http://cdsads.u-strasbg.fr/abs/2001ApJ...554..114E target="_blank">ENS01_VIR</a>, <a href =http://cdsads.u-strasbg.fr/abs/2000ApJ...529L..69J target="_blank">JS00</a> etc. (<a href=../enumerators_cdelta.html>See the user documentation</a>)
extern const char    gNAMES_FINALSTATE[gN_FINALSTATE][50];                      //!< Final states used in CLUMPY are GAMMA and NEUTRINO (ANTIPROTON, POSITRON, ELECTRON not used in this version)
extern const char    gNAMES_GROWTHFACTOR[gN_GROWTHFACTOR][50];                  //!< Growth factor computation used. Default: PKZ
extern const char    gNAMES_MASSFUNCTION[gN_MASSFUNCTION][50];                  //!< Available mass halo mass functions are TINKER08, SHETHTORMEN99, JENKINS01 and PRESSSCHECHTER74
extern const char    gNAMES_NUFLAVOUR[gN_NUFLAVOUR][50];                        //!< The 3 neutrino flavour names are NUE (\f$\nu_{e}\f$), NUMU (\f$\nu_{\mu}\f$), and NUTAU (\f$\nu_{\tau}\f$)
extern const char    gNAMES_PP_BR[gN_PP_BR][50];                                //!< Names of branching ratios (for gamma-ray and neutrino production)
extern const char    gNAMES_PP_SPECTRUMMODEL[gN_PP_SPECTRUMMODEL][50];          //!< Particle physics parametrisation names for spectra: <a href=http://adsabs.harvard.edu/abs/2011JCAP...03..051C target="_blank"> CIRELLI11</a>, ... (see \c spectra.h)
extern const char    gNAMES_PROFILE[gN_PROFILE][50];                            //!< DM profiles names are ZHAO, EINASTO, BURKERT... (<a href=../enumerators_dm_profiles.html>See the user documentation</a>)
extern const char    gNAMES_TYPEHALOES[gN_TYPEHALOES][50];                      //!< Host halo types are DSPH, GALAXY, and CLUSTER
extern const char    gNAMES_SIMUMODES[gN_SIMUMODES][50];                        //!< Names of the simulation modes with clumpy.
extern const char    gNAMES_WINDOWFUNC[gN_WINDOWFUNC][50];                      //!< Names of available window functions.


//------------------------------------------------------------------------------
// Global variables for the selection of parametrisations and key parameters
extern string        gCLUMPY_VERSION;                                           //!< Which Clumpy version/revision do we have here?

// Cosmology related
extern double        gCOSMO_HUBBLE;                                             //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
extern double        gCOSMO_OMEGA0_M;                                           //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
extern double        gCOSMO_OMEGA0_B;                                           //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
extern double        gCOSMO_OMEGA0_K;                                           //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
extern double        gCOSMO_SIGMA8;                                             //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
extern double        gCOSMO_N_S;                                                //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
extern double        gCOSMO_TAU_REIO;                                           //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
extern double        gCOSMO_WDE;                                                //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
extern double        gCOSMO_T0;                                                 //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
extern double        gCOSMO_DELTA0;                                             //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
extern int           gCOSMO_FLAG_DELTA_REF;                                     //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>

// - no input parameter
extern double        gCOSMO_RHO0_C;                                             //!< Present day \f$(z=0)\f$ critical density of the universe \f$\rho_c(z=0)=144\f$ in unit of \f$M_\odot\,\,kpc^{-3}\f$ (<A href="http://pdg.lbl.gov/" target="_blank">PDG</A>)
extern double        gCOSMO_OMEGA0_R;                                           //!< Present day \f$(z=0)\f$ radiation density of the Universe \f$ \Omega_r = \rho_r/\rho_c \approx 0.04\f$ (<A href="http://pdg.lbl.gov/" target="_blank">PDG</A>)
extern double        gCOSMO_OMEGA0_LAMBDA;                                      //!< Present day \f$(z=0)\f$ dark energy density of the \f$\Lambda\f$-CDM Universe \f$ \Omega_{\Lambda} \approx 0.74\f$ (<A href="http://pdg.lbl.gov/" target="_blank">PDG</A>)
extern double        gCOSMO_OMEGA0_CDM;                                         //!< Present day \f$(z=0)\f$ dark matter density of the Universe.
extern double        gCOSMO_M_to_MH;                                            //!< \f$h/\Omega_{m,0}\f$ conversion.
extern vector<double>gCOSMO_MH_GRID;
extern vector<double>gCOSMO_Z_GRID;                                             //!< Grid of redshift values for global tabulated values
extern vector<double>gCOSMO_D_TRANS_GRID;                                       //!< Tabulated values for the comoving transverse distance used throughout the code, calculated in init_extragal() from the cosmological parameters.
extern vector<double>gCOSMO_D_LUM_GRID;                                         //!< Tabulated values for the luminosity distance used throughout the code, calculated in init_extragal() from the cosmological parameters.
extern vector<double>gCOSMO_D_ANG_GRID;                                         //!< Tabulated values for the angular diameter distance used throughout the code, calculated in init_extragal() from the cosmological parameters.
extern vector<vector<double> > gCOSMO_DNDVHDLNMH_Z;                             //!< 2d grid of the mass function over tabulated masses and redshifts

// Dark matter related
extern double        gDM_LOGCDELTA_STDDEV;                                      //!< <a href=../parameters.html#dark-matter-global-parameters>See the user documentation</a>
extern double        gDM_SUBS_MMIN;                                             //!< <a href=../parameters.html#dark-matter-global-parameters>See the user documentation</a>
extern double        gDM_SUBS_MMAXFRAC;                                         //!< <a href=../parameters.html#dark-matter-global-parameters>See the user documentation</a>
extern int           gDM_SUBS_NUMBEROFLEVELS;                                   //!< <a href=../parameters.html#dark-matter-global-parameters>See the user documentation</a>
extern double        gDM_RHOSAT;                                                //!< <a href=../parameters.html#dark-matter-global-parameters>See the user documentation</a>
extern double        gDM_RHOHALOES_TO_RHOMEAN;                                  //!< <a href=../parameters.html#dark-matter-global-parameters>See the user documentation</a>
extern bool          gDM_IS_IDM;                                                //!< <a href=../parameters.html#dark-matter-global-parameters>See the user documentation</a>
extern double        gDM_KMAX;                                                  //!< <a href=../parameters.html#dark-matter-global-parameters>See the user documentation</a>
extern double        gDM_KMAX_SIGMA_CUTOFF;                                     //!< <a href=../parameters.html#dark-matter-global-parameters>See the user documentation</a>
// - no input parameter
extern int           gDM_FLAG_CDELTA_DIST;                                      //!< Flag for selected \f$c_{\Delta}\f$ distribution [\c gENUM_CDELTA_DIST]

// Galaxy (Milky-way) related
extern double        gMW_RHOSOL;                                                //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gMW_RSOL;                                                  //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gMW_RMAX;                                                  //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern int           gMW_SUBS_FLAG_CDELTAMDELTA;                                //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern int           gMW_SUBS_FLAG_PROFILE;                                     //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gMW_SUBS_SHAPE_PARAMS[gN_SHAPE_PARAMS];                    //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gMW_SUBS_DPDM_SLOPE;                                       //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern int           gMW_SUBS_DPDV_FLAG_PROFILE;                                //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gMW_SUBS_DPDV_RSCALE_TO_RS_HOST;                           //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gMW_SUBS_DPDV_SHAPE_PARAMS[gN_SHAPE_PARAMS];               //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gMW_SUBS_M1;                                               //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gMW_SUBS_M2;                                               //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern int           gMW_SUBS_N_INM1M2;                                         //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern bool          gMW_SUBS_TABULATED_IS;                                     //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern string        gMW_SUBS_TABULATED_CMIN_OF_R;                              //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern string        gMW_SUBS_TABULATED_LCRIT;                                  //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern string        gMW_SUBS_TABULATED_RTIDAL_TO_RS;                           //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern int           gMW_TOT_FLAG_PROFILE;                                      //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gMW_TOT_RSCALE;                                            //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gMW_TOT_SHAPE_PARAMS[gN_SHAPE_PARAMS];                     //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gMW_TRIAXIAL_AXES[gN_XYZ];                                 //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern bool          gMW_TRIAXIAL_IS;                                           //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gMW_TRIAXIAL_ROTANGLES[gN_XYZ];                            //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>

// Any other halo related (gN_TYPEHALOES = kDSPH, kGALAXY, kCLUSTER)
extern int           gHALO_SUBS_FLAG_CDELTAMDELTA[gN_TYPEHALOES];               //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
extern int           gHALO_SUBS_FLAG_PROFILE[gN_TYPEHALOES];                    //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
extern double        gHALO_SUBS_SHAPE_PARAMS[gN_TYPEHALOES][gN_SHAPE_PARAMS];   //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
extern double        gHALO_SUBS_DPDM_SLOPE[gN_TYPEHALOES];                      //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
extern int           gHALO_SUBS_DPDV_FLAG_PROFILE[gN_TYPEHALOES];               //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
extern double        gHALO_SUBS_DPDV_RSCALE_TO_RS_HOST[gN_TYPEHALOES];          //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
extern double        gHALO_SUBS_DPDV_SHAPE_PARAMS[gN_TYPEHALOES][gN_SHAPE_PARAMS]; //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
extern double        gHALO_SUBS_MASSFRACTION[gN_TYPEHALOES];                    //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>

// - no input parameter
extern double        gHALO_TRIAXIAL_AXES[gN_XYZ];                               //!< Triaxial major/second/minor axes (if no rotation angle, major/minor along x_los/z_los) [-], Triaxiality specified in halo list file is saved in these global parameters
extern bool          gHALO_TRIAXIAL_IS;                                         //!< Whether the halo is triaxial or not [-], Triaxiality specified in halo list file is saved in these global parameters
extern double        gHALO_TRIAXIAL_ROTANGLES[gN_XYZ];                          //!< Rotation angle of triaxial shalo around (x,y,z)_los axis (see \c geometry.h) in the range ([-180,180],[-90,90],[-180,180]) in [deg], Triaxiality specified in halo list file is saved in these global parameters
extern vector<vector<double>>gHALO_NODES_X_GRID;                                //!< Nodes on radius axis for numeric/tabulated halo profile in units of x = r/r_0
extern vector<vector<double>>gHALO_NODES_Y_GRID;                                //!< Values ("densities") on nodes gHALO_NODES_X_GRID, such that y(x=1) = 1
extern vector<int>   gHALO_NODES_INTERPOLTYPE;                                  //!< Interpolation type (lin-lin, lin-log,...) of node profile
extern vector<double>gHALO_NODES_RATIO_R_2_RSCALE;                              //!< r_2 of the numeric profile (determined in the code)
extern vector<double>gHALO_NODES_INPUT_RSCALE;                                  //!< Store absolute (unnormalized) input values (x-axis, kpc) if user wants to use them
extern vector<double>gHALO_NODES_INPUT_RHOSCALE;                                //!< Store absolute (unnormalized) input values (y-axis, Msol/kpc^3) if user wants to use them

// Extragalactic related
extern string        gEXTRAGAL_SUBS_DPDM_SLOPE_LIST;                            //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
extern int           gEXTRAGAL_FLAG_CDELTAMDELTA;                               //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
extern string        gEXTRAGAL_FLAG_CDELTAMDELTA_LIST;                          //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
extern int           gEXTRAGAL_FLAG_MASSFUNCTION;                               //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
extern int           gEXTRAGAL_FLAG_ABSORPTIONPROFILE;                          //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
extern string        gEXTRAGAL_SUBS_DPDM_SLOPE_LIST;                            //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gEXTRAGAL_IDM_MHALFMODE;                                   //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gEXTRAGAL_IDM_ALPHA;                                       //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gEXTRAGAL_IDM_BETA;                                        //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gEXTRAGAL_IDM_GAMMA;                                       //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gEXTRAGAL_IDM_DELTA;                                       //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE;                       //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
extern double        gEXTRAGAL_HMF_SMALLSCALE_DPDM_MLIM;                        //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>

// Sunyaev-Zel'dovich clusters
extern double        gSZ_REF_C500;                                              //!< Concentration parameter for \f$\Delta=500\f$ for SZ clusters. Default value taken from Arnaud et al. (2010)
extern double        gSZ_REF_PRESSURE_NORM;                                     //!< Gas pressure normalisation for SZ clusters. \f$ P_0 = 8.403 h_{70}^{-3/2}\f$ (default value from Arnaud et al. (2010)).
extern double        gSZ_REF_SHAPE_PARAMS[gN_SHAPE_PARAMS];                     //!< Universal pressure profile shape parameters. Default values taken from Arnaud et al. (2010)
extern double        gSZ_REF_ALPHA_MYX;                                         //!< Slope of conversion between \f$ M_{500} \propto Y_X^{\alpha_{M_{YX}}}\f$.
extern int           gSZ_FLAG_MASSFUNCTION;                                     //!< Extragalactic halo mass function for SZ studies

// Statistical analysis related
extern string        gSTAT_FILES;                                               //!< <a href=../parameters.html#statistical-analysis-of-single-halo>See the user documentation</a>
extern double        gSTAT_CL;                                                  //!< <a href=../parameters.html#statistical-analysis-of-single-halo>See the user documentation</a>
extern string        gSTAT_CL_LIST;                                             //!< <a href=../parameters.html#statistical-analysis-of-single-halo>See the user documentation</a>
extern string        gSTAT_ID_LIST;                                             //!< <a href=../parameters.html#statistical-analysis-of-single-halo>See the user documentation</a>
extern int           gSTAT_MODE;                                                //!< <a href=../parameters.html#statistical-analysis-of-single-halo>See the user documentation</a>
extern bool          gSTAT_IS_LOGL_OR_CHI2;                                     //!< <a href=../parameters.html#statistical-analysis-of-single-halo>See the user documentation</a>
extern double        gSTAT_RKPC_FOR_MR;                                         //!< <a href=../parameters.html#statistical-analysis-of-single-halo>See the user documentation</a>
extern string        gSTAT_DATAFILES;                                           //!< <a href=../parameters.html#statistical-analysis-of-single-halo>See the user documentation</a>
extern int           gSTAT_N_REALIZATIONS;                                      //!< <a href=../parameters.html#statistical-analysis-of-single-halo>See the user documentation</a>

// Particle physics model related
extern double        gPP_BR[gN_PP_BR];                                          //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
extern int           gPP_DM_ANNIHIL_DELTA;                                      //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
extern double        gPP_DM_ANNIHIL_SIGMAV_CM3PERS;                             //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
extern double        gPP_DM_DECAY_LIFETIME_S;                                   //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
extern bool          gPP_DM_IS_ANNIHIL_OR_DECAY;                                //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
extern double        gPP_DM_MASS_GEV;                                           //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
extern int           gPP_FLAG_SPECTRUMMODEL;                                    //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
extern double        gPP_NUMIXING_THETA12_DEG;                                  //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
extern double        gPP_NUMIXING_THETA13_DEG;                                  //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
extern double        gPP_NUMIXING_THETA23_DEG;                                  //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
// - no input parameter
extern double        gPP_NUOSCILLATIONMATRIX[gN_NUFLAVOUR][gN_NUFLAVOUR];       //!< Neutrino oscillation matrix (filled from \c gPP_NUMIXING_THETA12_DEG... mixing angles)

// List of halos used in CLUMPY run
extern string        gLIST_HALOES;                                              //!< <a href=../parameters.html#external-lists-with-specified-objects>See the user documentation</a>
extern string        gLIST_HALOES_JEANS;                                        //!< <a href=../parameters.html#external-lists-with-specified-objects>See the user documentation</a>
extern string        gLIST_HALONAME;                                            //!< <a href=../parameters.html#external-lists-with-specified-objects>See the user documentation</a>
extern string        gLIST_HALOES_NODES;                                        //!< <a href=../parameters.html#external-lists-with-specified-objects>See the user documentation</a>
extern string        gLIST_HALOES_NODES_RSCALE;                                 //!< <a href=../parameters.html#external-lists-with-specified-objects>See the user documentation</a>

// Dummy input parameters: these are defined in the halo list, but are needed here to be saved in FITS output.
extern double        gLIST_HALO_DISTANCE;                                       //!< Distance of halo (set in halo definition file)
extern double        gLIST_HALO_RSCALE;                                         //!< Scale radius of halo (set in halo definition file)
extern double        gLIST_HALO_RHOSCALE;                                       //!< Density at scale radius of halo (set in halo definition file)
extern double        gLIST_HALO_RDELTA;                                         //!< Virial radius of halo (set in halo definition file)

// CLUMPY run related
extern double        gSIM_EPS;                                                  //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_EPS_DRAWN;                                            //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>

extern double        gSIM_USER_RSE;                                             //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern int           gSIM_SEED;                                                 //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern string        gSIM_OUTPUT_DIR;                                           //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>

// parameters for 1D runs + alpha_int
extern double        gSIM_R_MIN;                                                //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_R_MAX;                                                //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern int           gSIM_NX;                                                   //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern bool          gSIM_IS_XLOG;                                              //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_ALPHAINT;                                             //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_ALPHAINT_MIN;                                         //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_ALPHAINT_MAX;                                         //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_THETA_MIN;                                            //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_THETA_MAX;                                            //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern bool          gSIM_SORT_CONTRAST_THRESH;                                 //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_PHI_CUT;                                              //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>

// parameters for 2D runs
extern double        gSIM_PSI_OBS;                                              //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern string        gSIM_THETA_OBS_DEG;                                        //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern string        gSIM_THETA_ORTH_SIZE_DEG;                                  //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_THETA_SIZE;                                           //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>

extern bool          gSIM_IS_ASTRO_OR_PP_UNITS;                                 //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>

// 2D-Resolution and pixelization related stuff
extern int           gSIM_HEALPIX_ITER;                                         //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_HEALPIX_NLMAX_FAC;                                    //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern int           gSIM_HEALPIX_NSIDE;                                        //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern string        gSIM_HEALPIX_RING_WEIGHTS_DIR;                             //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern PDT           gSIM_HEALPIX_FITS_DATATYPE;                                //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern Healpix_Ordering_Scheme gSIM_HEALPIX_SCHEME;                             //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>

extern bool          gSIM_IS_WRITE_FLUXMAPS;                                    //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern bool          gSIM_FLUX_IS_INTEG_OR_DIFF;                                //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_FLUX_AT_E_GEV;                                        //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_XPOWER;                                               //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_FLUX_EMIN_GEV;                                        //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_FLUX_EMAX_GEV;                                        //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern int           gSIM_FLUX_FLAG_NUFLAVOUR;                                  //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern int           gSIM_FLUX_FLAG_FINALSTATE;                                 //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_JFACTOR;                                              //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_JFRACTION;                                            //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>

extern double        gSIM_GAUSSBEAM_GAMMA_FWHM;                                 //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_GAUSSBEAM_NEUTRINO_FWHM;                              //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_GAUSSBEAM_SZ_FWHM;                                    //!< 2D Gaussian Beam FWHM for SZ maps : If set to value > 0, output map is smoothed with this beam and the result is saved in third fits-extension.

// Cosmology/extragalactic parameters
extern double        gSIM_REDSHIFT;                                             //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_EXTRAGAL_ZMIN;                                        //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_EXTRAGAL_ZMAX;                                        //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern int           gSIM_EXTRAGAL_NZ;                                          //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern bool          gSIM_EXTRAGAL_IS_ZLOG;                                     //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_EXTRAGAL_DELTAZ_PRECOMP;                              //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_EXTRAGAL_MMIN;                                        //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_EXTRAGAL_MMAX;                                        //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern int           gSIM_EXTRAGAL_NM;                                          //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern int           gSIM_EXTRAGAL_NM_PRECOMP;                                  //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern bool          gSIM_EXTRAGAL_IS_MLOG;                                     //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern int           gSIM_EXTRAGAL_FLAG_WINDOWFUNC;                             //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_EXTRAGAL_MF_SIGMA_CUTOFF;                             //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_EXTRAGAL_KMAX_PRECOMP;                                //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern bool          gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE;                   //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern double        gSIM_EXTRAGAL_EBL_UNCERTAINTY;                             //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
extern int           gSIM_EXTRAGAL_FLAG_GROWTHFACTOR;                           //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>

// no input variables:
extern double        gSIM_EXTRAGAL_KMIN_PRECOMP;                                //!< k_min/h [Mpc] of computed CDM power spectrum
extern bool          gSIM_EXTRAGAL_M_PRECOMP_ISLOG;                             //!< Precompute halo mass values in log-space
extern bool          gSIM_EXTRAGAL_DELTAZ_PRECOMP_ISLOG;                        //!< Plot/integrate mass values in log-space

extern bool          gSIM_IS_DISPLAY;                                           //!< Enables or disables displays (Root CERN graphs) in CLUMPY
extern bool          gSIM_IS_PRINT;                                             //!< Enables or disables print on screen in CLUMPY
extern bool          gSIM_IS_WRITE_GALPOWERSPECTRUM;                            //!< Choose to compute and write the (fullsky) angular power spectrum [-]
extern bool          gSIM_IS_SZ;                                                //!< Are we dealing with SZ map?

extern bool          gSIM_IS_WRITE_ROOTFILES;                                   //!< Choose to compute and append to the output FITS file the 2D fluxmaps for the particle physics model specified in the parameter file.

// "special" input parameter
extern int           gSIM_FLAG_MODE;                                            //!< Specifies the enumerator for the type of simulation performed in this run (g0, g1, g2,...)

// - no input parameters
extern int           gSIM_SIGDIGITS;                                            //!< Significant digits for ASCII and print output derived from gSIM_EPS
extern double        gSIM_HEALPIX_DELTAOMEGA;                                   //!< Integration solid angle \f$\Delta\Omega\f$ [sr] (set from gSIM_HEALPIX_NSIDE choice for 2D maps only)
extern double        gSIM_RESOLUTION;                                           //!< Approximate measure of the map resolution in [rad]. It is the radius of circular assumed pixel and calculated from the constant pixel area by \f$resolution = \arccos \left(1-\frac{\Delta\Omega}{2\pi}\right)\f$.
extern bool          gSIM_IS_CIRCULAR_SYMMETRY;                                 //!< global variable used for switching between 1D-calculations for circular (spherical) symmetry of the problem and full 2D-calculation when circular symmetry is not present.
extern double        gSIM_JEANS_RMAX;                                           //!< Maximum integration radius for Jeans analysis
extern int           gSIM_PARAMLENGTH_MAX;                                      //!< maximum  string length of a input parameter value
#if IS_ROOT
extern TApplication *gSIM_ROOTAPP;                                              //!< ROOT application (mandatory to enable displays) in CLUMPY
extern TText        *gSIM_CLUMPYAD;                                             //<! CLUMPY advertisment in ROOT plots (text)
#endif
extern bool          gSIM_MDELTA_TO_PAR_METHOD;                                 //!< Internal hardcoded switch: Determines which method is used to calculate the structural clump parameters from given mass, Delta, and cdelta-mdelta relationship.

extern vector<string>gSIM_INPUTPARAM_VALUESTRING;                               //!< Saves the values of all input parameters given by the user in a string

extern vector<string> gSIM_STANDARD_INPUTPARAMS;                                //!< Default input parameters for the chosen Clumpy run.
extern vector< vector<int> > gPARAMS_REQUIRED;                                  //!< Matrix to be filled at run time to define all required input parameters for all run modes.
extern vector< vector<int> > gPARAMS_HIDDEN;                                    //!< Matrix to be filled at run time to define all hidden (optional) input parameters for all run modes.
extern const char  gSIM_INPUTPARAMS[gN_INPUTPARAMS][5][50];                     //!< Matrix containing the names and info about all input parameters

extern struct option gSIM_COMMANDLINE_OPTIONS[];                                //!< Structure used by get_opt() to read all input parameters from the command line.

extern string        gPATH_TO_CLUMPY_EXE;                                       //!< Absolute path to CLUMPY executable
extern string        gPATH_TO_CLUMPY_HOME;                                      //!< Absolute path to CLUMPY home directory
extern string        gPATH_TO_CLASS;                                            //!< Absolute path to CLASS executable
extern string        gPATH_TO_USER_EXE;                                         //!< Absolute path to directory from where CLUMPY is run
extern bool          gSIM_IS_TEST;                                              //!< Indicates if CLUMPY is run in testing mode

//------------------------------------------------------------------------------
// Define corresponding keywords to all input and hidden variables
// They slightly differ from the global (gXX) variables:

//  - gPP_NUOSCILLATIONMATRIX
//  - gSIM_HEALPIX_DELTAOMEGA
//  - gSIM_RESOLUTION,
//  - gSIM_IS_CIRCULAR_SYMMETRY,
//  - gCLUMPY_VERSION,
//  - gSIM_JEANS_RMAX (defined in gLIST_HALOES_JEANS)
//  - gSIM_ROOTAPP are no input variables.

// check    kSIM_IS_DISPLAY, kSIM_IS_PRINT,   kSIM_INTFLUX_SUPPRESS_PRINTOUT,

// Order must match the gSIM_INPUTPARAMS definitions in params.cc

//! Each input parameter (which is also a global parameter, beginning with gXXX) has a corresponding keyword kXXX. <a href=../parameters.html>See the user documentation</a>  for the global input parameters.
enum gENUM_INPUTPARAMS {
   kCOSMO_DELTA0,                                                               //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
   kCOSMO_FLAG_DELTA_REF,                                                       //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
   kCOSMO_HUBBLE,                                                               //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
   kCOSMO_N_S,                                                                  //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
   kCOSMO_OMEGA0_M,                                                             //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
   kCOSMO_OMEGA0_B,                                                             //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
   kCOSMO_OMEGA0_K,                                                             //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
   kCOSMO_SIGMA8,                                                               //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
   kCOSMO_T0,                                                                   //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
   kCOSMO_TAU_REIO,                                                             //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>
   kCOSMO_WDE,                                                                  //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>

   kDM_IS_IDM,                                                                  //!< <a href=../parameters.html#dark-matter-global-parameters>See the user documentation</a>
   kDM_KMAX,                                                                    //!< <a href=../parameters.html#dark-matter-global-parameters>See the user documentation</a>
   kDM_KMAX_SIGMA_CUTOFF,                                                       //!< <a href=../parameters.html#dark-matter-global-parameters>See the user documentation</a>
   kDM_LOGCDELTA_STDDEV,                                                        //!< <a href=../parameters.html#dark-matter-global-parameters>See the user documentation</a>
   kDM_RHOHALOES_TO_RHOMEAN,                                                   //!< <a href=../parameters.html#dark-matter-global-parameters>See the user documentation</a>
   kDM_RHOSAT,                                                                  //!< <a href=../parameters.html#dark-matter-global-parameters>See the user documentation</a>
   kDM_SUBS_MMIN,                                                               //!< <a href=../parameters.html#dark-matter-global-parameters>See the user documentation</a>
   kDM_SUBS_MMAXFRAC,                                                           //!< <a href=../parameters.html#dark-matter-global-parameters>See the user documentation</a>
   kDM_SUBS_NUMBEROFLEVELS,                                                     //!< <a href=../parameters.html#dark-matter-global-parameters>See the user documentation</a>

   kMW_TOT_FLAG_PROFILE,                                                        //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_TOT_SHAPE_PARAMS_0,                                                      //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_TOT_SHAPE_PARAMS_1,                                                      //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_TOT_SHAPE_PARAMS_2,                                                      //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_TOT_RSCALE,                                                              //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_RMAX,                                                                    //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_RSOL,                                                                    //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_RHOSOL,                                                                  //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>

   kMW_TRIAXIAL_IS,                                                             //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_TRIAXIAL_AXES_0,                                                         //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_TRIAXIAL_AXES_1,                                                         //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_TRIAXIAL_AXES_2,                                                         //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_TRIAXIAL_ROTANGLES_0,                                                    //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_TRIAXIAL_ROTANGLES_1,                                                    //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_TRIAXIAL_ROTANGLES_2,                                                    //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>

   kMW_SUBS_FLAG_PROFILE,                                                       //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_SUBS_SHAPE_PARAMS_0,                                                     //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_SUBS_SHAPE_PARAMS_1,                                                     //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_SUBS_SHAPE_PARAMS_2,                                                     //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_SUBS_FLAG_CDELTAMDELTA,                                                  //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_SUBS_DPDV_FLAG_PROFILE,                                                  //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_SUBS_DPDV_SHAPE_PARAMS_0,                                                //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_SUBS_DPDV_SHAPE_PARAMS_1,                                                //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_SUBS_DPDV_SHAPE_PARAMS_2,                                                //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_SUBS_DPDV_RSCALE_TO_RS_HOST,                                             //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_SUBS_DPDM_SLOPE,                                                         //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_SUBS_M1,                                                                 //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_SUBS_M2,                                                                 //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_SUBS_N_INM1M2,                                                           //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>

   kMW_SUBS_TABULATED_IS,                                                       //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_SUBS_TABULATED_CMIN_OF_R,                                                //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_SUBS_TABULATED_LCRIT,                                                    //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>
   kMW_SUBS_TABULATED_RTIDAL_TO_RS,                                             //!< <a href=../parameters.html#milky-way-dm-total-and-clump-parameters>See the user documentation</a>

   kEXTRAGAL_FLAG_PROFILE,                                                      //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_SHAPE_PARAMS_0,                                                    //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_SHAPE_PARAMS_1,                                                    //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_SHAPE_PARAMS_2,                                                    //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_FLAG_CDELTAMDELTA,                                                 //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_FLAG_CDELTAMDELTA_LIST,                                            //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>

   kEXTRAGAL_FLAG_MASSFUNCTION,                                                 //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_IDM_MHALFMODE,                                                     //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_IDM_ALPHA,                                                         //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_IDM_BETA,                                                          //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_IDM_GAMMA,                                                         //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_IDM_DELTA,                                                         //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE,                                         //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_HMF_SMALLSCALE_DPDM_MLIM,                                          //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>

   kEXTRAGAL_TRIAXIAL_IS,                                                       //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_TRIAXIAL_AXES_0,                                                   //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_TRIAXIAL_AXES_1,                                                   //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_TRIAXIAL_AXES_2,                                                   //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_TRIAXIAL_ROTANGLES_0,                                              //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_TRIAXIAL_ROTANGLES_1,                                              //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_TRIAXIAL_ROTANGLES_2,                                              //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>

   kEXTRAGAL_SUBS_FLAG_CDELTAMDELTA,                                            //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_SUBS_DPDV_FLAG_PROFILE,                                            //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_0,                                          //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_1,                                          //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_2,                                          //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_SUBS_DPDV_RSCALE_TO_RS_HOST,                                       //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_SUBS_DPDM_SLOPE,                                                   //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_SUBS_DPDM_SLOPE_LIST,                                              //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>
   kEXTRAGAL_SUBS_MASSFRACTION,                                                 //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>

   kEXTRAGAL_FLAG_ABSORPTIONPROFILE,                                            //!< <a href=../parameters.html#extragalactic-dm-total-and-clump-parameters>See the user documentation</a>

   kLIST_HALOES,                                                                //!< <a href=../parameters.html#external-lists-with-specified-objects>See the user documentation</a>
   kLIST_HALOES_JEANS,                                                          //!< <a href=../parameters.html#external-lists-with-specified-objects>See the user documentation</a>
   kLIST_HALONAME,                                                              //!< <a href=../parameters.html#external-lists-with-specified-objects>See the user documentation</a>
   kLIST_HALOES_NODES,                                                          //!< <a href=../parameters.html#external-lists-with-specified-objects>See the user documentation</a>
   kLIST_HALOES_NODES_RSCALE,                                                   //!< <a href=../parameters.html#external-lists-with-specified-objects>See the user documentation</a>
   kLIST_HALO_DISTANCE,                                                         //!< Not an input parameter
   kLIST_HALO_RSCALE,                                                           //!< Not an input parameter
   kLIST_HALO_RHOSCALE,                                                         //!< Not an input parameter
   kLIST_HALO_RDELTA,                                                           //!< Not an input parameter

   kDSPH_SUBS_FLAG_PROFILE,                                                     //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kDSPH_SUBS_SHAPE_PARAMS_0,                                                   //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kDSPH_SUBS_SHAPE_PARAMS_1,                                                   //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kDSPH_SUBS_SHAPE_PARAMS_2,                                                   //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kDSPH_SUBS_FLAG_CDELTAMDELTA,                                                //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kDSPH_SUBS_DPDV_FLAG_PROFILE,                                                //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kDSPH_SUBS_DPDV_SHAPE_PARAMS_0,                                              //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kDSPH_SUBS_DPDV_SHAPE_PARAMS_1,                                              //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kDSPH_SUBS_DPDV_SHAPE_PARAMS_2,                                              //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kDSPH_SUBS_DPDV_RSCALE_TO_RS_HOST,                                           //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kDSPH_SUBS_DPDM_SLOPE,                                                       //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kDSPH_SUBS_MASSFRACTION,                                                     //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>

   kGALAXY_SUBS_FLAG_PROFILE,                                                   //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kGALAXY_SUBS_SHAPE_PARAMS_0,                                                 //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kGALAXY_SUBS_SHAPE_PARAMS_1,                                                 //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kGALAXY_SUBS_SHAPE_PARAMS_2,                                                 //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kGALAXY_SUBS_FLAG_CDELTAMDELTA,                                              //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kGALAXY_SUBS_DPDV_FLAG_PROFILE,                                              //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kGALAXY_SUBS_DPDV_SHAPE_PARAMS_0,                                            //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kGALAXY_SUBS_DPDV_SHAPE_PARAMS_1,                                            //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kGALAXY_SUBS_DPDV_SHAPE_PARAMS_2,                                            //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kGALAXY_SUBS_DPDV_RSCALE_TO_RS_HOST,                                         //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kGALAXY_SUBS_DPDM_SLOPE,                                                     //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kGALAXY_SUBS_MASSFRACTION,                                                   //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>

   kCLUSTER_SUBS_FLAG_PROFILE,                                                  //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kCLUSTER_SUBS_SHAPE_PARAMS_0,                                                //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kCLUSTER_SUBS_SHAPE_PARAMS_1,                                                //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kCLUSTER_SUBS_SHAPE_PARAMS_2,                                                //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kCLUSTER_SUBS_FLAG_CDELTAMDELTA,                                             //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kCLUSTER_SUBS_DPDV_FLAG_PROFILE,                                             //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_0,                                           //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_1,                                           //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_2,                                           //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kCLUSTER_SUBS_DPDV_RSCALE_TO_RS_HOST,                                        //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kCLUSTER_SUBS_DPDM_SLOPE,                                                    //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>
   kCLUSTER_SUBS_MASSFRACTION,                                                  //!< <a href=../parameters.html#universal-sub-clustering-properties>See the user documentation</a>

   kPP_BR,                                                                      //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
   kPP_FLAG_SPECTRUMMODEL,                                                      //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
   kPP_DM_ANNIHIL_DELTA,                                                        //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
   kPP_DM_ANNIHIL_SIGMAV_CM3PERS,                                               //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
   kPP_DM_DECAY_LIFETIME_S,                                                     //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
   kPP_DM_IS_ANNIHIL_OR_DECAY,                                                  //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
   kPP_DM_MASS_GEV,                                                             //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
   kPP_NUMIXING_THETA12_DEG,                                                    //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
   kPP_NUMIXING_THETA13_DEG,                                                    //!< <a href=../parameters.html#particle-physics-parameters>See the user documentation</a>
   kPP_NUMIXING_THETA23_DEG,                                                    //!< <a href=../parameters.html#cosmological-parameters>See the user documentation</a>

   kSZ_REF_C500,                                                                //!< Not part of the current release
   kSZ_REF_PRESSURE_NORM,                                                       //!< Not part of the current release
   kSZ_REF_SHAPE_PARAMS_0,                                                      //!< Not part of the current release
   kSZ_REF_SHAPE_PARAMS_1,                                                      //!< Not part of the current release
   kSZ_REF_SHAPE_PARAMS_2,                                                      //!< Not part of the current release
   kSZ_REF_ALPHA_MYX,                                                           //!< Not part of the current release
   kSZ_FLAG_MASSFUNCTION,                                                       //!< Not part of the current release

   kSTAT_CL,                                                                    //!< <a href=../parameters.html#statistical-analysis-of-single-halo>See the user documentation</a>
   kSTAT_CL_LIST,                                                               //!< <a href=../parameters.html#statistical-analysis-of-single-halo>See the user documentation</a>
   kSTAT_DATAFILES,                                                             //!< <a href=../parameters.html#statistical-analysis-of-single-halo>See the user documentation</a>
   kSTAT_FILES,                                                                 //!< <a href=../parameters.html#statistical-analysis-of-single-halo>See the user documentation</a>
   kSTAT_ID_LIST,                                                               //!< <a href=../parameters.html#statistical-analysis-of-single-halo>See the user documentation</a>
   kSTAT_IS_LOGL_OR_CHI2,                                                       //!< <a href=../parameters.html#statistical-analysis-of-single-halo>See the user documentation</a>
   kSTAT_MODE,                                                                  //!< <a href=../parameters.html#statistical-analysis-of-single-halo>See the user documentation</a>
   kSTAT_N_REALIZATIONS,                                                        //!< <a href=../parameters.html#statistical-analysis-of-single-halo>See the user documentation</a>
   kSTAT_RKPC_FOR_MR,                                                           //!< <a href=../parameters.html#statistical-analysis-of-single-halo>See the user documentation</a>

   kSIM_FLAG_MODE,                                                              //!< Simulation module. Not an input parameter.

   kSIM_ALPHAINT,                                                               //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_ALPHAINT_MIN,                                                           //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_ALPHAINT_MAX,                                                           //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>

   kSIM_IS_WRITE_FLUXMAPS,                                                      //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_FLUX_IS_INTEG_OR_DIFF,                                                  //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_FLUX_AT_E_GEV,                                                          //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_FLUX_EMIN_GEV,                                                          //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_FLUX_EMAX_GEV,                                                          //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_FLUX_FLAG_NUFLAVOUR,                                                    //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_FLUX_FLAG_FINALSTATE,                                                   //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_GAUSSBEAM_GAMMA_FWHM,                                                   //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_GAUSSBEAM_NEUTRINO_FWHM,                                                //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>

   kSIM_HEALPIX_FITS_DATATYPE,                                                  //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_HEALPIX_ITER,                                                           //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_HEALPIX_NLMAX_FAC,                                                      //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_HEALPIX_NSIDE,                                                          //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_HEALPIX_RING_WEIGHTS_DIR,                                               //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_HEALPIX_SCHEME,                                                         //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>

   kSIM_IS_XLOG,                                                                //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_NX,                                                                     //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_XPOWER,                                                                 //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>

   kSIM_IS_ASTRO_OR_PP_UNITS,                                                   //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_JFACTOR,                                                                //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_JFRACTION,                                                              //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_PHI_CUT,                                                                //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_R_MIN,                                                                  //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_R_MAX,                                                                  //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_REDSHIFT,                                                               //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_SORT_CONTRAST_THRESH,                                                   //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_THETA_MIN,                                                              //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_THETA_MAX,                                                              //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>

   kSIM_PSI_OBS,                                                                //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_THETA_OBS,                                                              //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_THETA_ORTH_SIZE,                                                        //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_THETA_SIZE,                                                             //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>

   kSIM_EXTRAGAL_EBL_UNCERTAINTY,                                               //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_EXTRAGAL_FLAG_GROWTHFACTOR,                                             //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_EXTRAGAL_FLAG_WINDOWFUNC,                                               //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_EXTRAGAL_KMAX_PRECOMP,                                                  //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE,                                     //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_EXTRAGAL_MF_SIGMA_CUTOFF,                                               //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_EXTRAGAL_MMIN,                                                          //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_EXTRAGAL_MMAX,                                                          //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_EXTRAGAL_NM,                                                            //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_EXTRAGAL_IS_MLOG,                                                       //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_EXTRAGAL_NM_PRECOMP,                                                    //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_EXTRAGAL_ZMIN,                                                          //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_EXTRAGAL_ZMAX,                                                          //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_EXTRAGAL_NZ,                                                            //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_EXTRAGAL_IS_ZLOG,                                                       //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_EXTRAGAL_DELTAZ_PRECOMP,                                                //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>

   kSIM_IS_SZ,                                                                  //!< Not part of the current release

   kSIM_EPS,                                                                    //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_EPS_DRAWN,                                                              //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_IS_WRITE_GALPOWERSPECTRUM,                                              //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_IS_WRITE_ROOTFILES,                                                     //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_OUTPUT_DIR,                                                             //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_SEED,                                                                   //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>
   kSIM_USER_RSE,                                                               //!< <a href=../parameters.html#simulation-parameters>See the user documentation</a>

};


//! Define keywords for columns in gSIM_INPUTPARAMS
enum gENUM_INPUTPARAMS_ROWS {
   kSIM_INPUTPARAM_VARNAME,                                                     //!< Name of the input parameter (as in the code, the input parameter file, or passed from the command line)
   kSIM_INPUTPARAM_FITSNAME,                                                    //!< Name of the input parameter in a FITS file header (different, because it is limited to eight characters).
   kSIM_INPUTPARAM_UNIT,                                                        //!< Physical unit of the input parameter
   kSIM_INPUTPARAM_DESCRIPTION,                                                 //!< Short description of the input parameter (printed in the input file and on screen), may slightly differ from those descriptions in this documentation.
   kSIM_INPUTPARAM_DATATYPE,                                                    //!< Data type of the input parameter (boolean, float, integer, string).
};

//------------------------------------------------------------------------------
//! Keywords of all run modes in CLUMPY
enum gENUM_SIMUMODES {
   kg0, kg1, kg2, kg3, kg4, kg5, kg6, kg7, kg8,
   kh0, kh1, kh2, kh3, kh4, kh5, kh6, kh7, kh8, kh9, kh10,
   ke0, ke1, ke2, ke3, ke4, ke5, ke6,
   ks0, ks1, ks2, ks3, ks4, ks5, ks6, ks7, ks8, ks9, ks10, ks11, ks12,
   ko1, ko2,
   kz,
   kf
};

//------------------------------------------------------------------------------
// Functions
void           get_required_params();                                           //!< Function which contains the hard-coded required parameters for each run modes and stores them in a vector of vectors at run time.

void           get_dependent_required_params(const int card_param,
      const vector<string> &inputvalues,
      const int card_simumode,
      vector<bool> &is_needed
                                            );
void           load_parameters(const string &file_name,
                               const vector<string> &commandline_values,
                               const int enum_simumode,
                               bool is_verbose = true
                              );                                                //!< Loads global parameters \c gXXX from the parameter file \c file_name
void           inputparameters_string2globalparams(const vector<string> &input_params
                                                  );
vector<string> inputparameters_globalparams2string(int card_simumode
                                                  );
void           inputparameters_write_paramfile(const string &filename,
      int simumode, bool is_write_hidden,
      const string &inputfile = ""
                                              );
int            string_to_enum(string flag_type,
                              string card_param
                             );                                                 //!< Function to get the index (in the \c enumerator list) from a user-selection name

#endif

/*! \file params.h
  \brief Global parameters declaration/initialisation (COSMO, DM, PP, Galaxy, CLUMPY run).

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><A href="#param_def">I. CLUMPY parameters</A></b>\n
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><A href="#param_fn">II. Functions</A></b>\n


 <CENTER>________________________________________</CENTER>\n

<b><A NAME="param_def">I. CLUMPY parameters</A></b>\n

This library contains the definition and description of many parameters mandatory
for a \c CLUMPY run. Note that some of the parameters presented below can be related
to (or depend on) one another (e.g., simulation resolution). Most of the parameters
of this library are read from <A href="../clumpy_params.txt" target="_blank">clumpy_params.txt</A>,
although some of them are only initialised in params.cc.

Broadly speaking, we can sort these parameters into three main categories:
    -# <b>Constant parameters</b>: must be changed only if new
       measurements provide different prescriptions.
        - <em>Cosmology parameters</em> (\c gCOSMO_XXX): for instance, \f$\rho_c(z=0)\f$, \f$\rho_m(z=0)\f$, \f$\rho_\Lambda(z=0)\f$;
        - <em>Particle physics parameters</em> (\c gPP_XXX): for instance, neutrino oscillation parameters (\c gPP_NUMIXING_THETA12_DEG);
        - <em>Solar system position in Galaxy</em>  (\c gMW_RSOL).
    \n\n
    -# <b>CLUMPY enumerators</b>: they are related to the following quantities.
        - <em>Base enumerator for DM and object type</em>
           - \c gENUM_PROFILE: DM spatial distribution, for host halo or \f$d{\cal P}_V/dV\f$;
           - \c gENUM_TYPEHALOES: specific to an object type only (galaxy, cluster, dsph).
        \n\n
        - <em>DM Sub-halos related</em>;
           - \c gENUM_CDELTA_DIST : concentration distributions \f$d{\cal P}_c(c,M)/dc\f$;
           - \c gENUM_CDELTAMDELTA: concentration-mass relationships \f$c_\Delta-M_\Delta\f$ (<a href=../enumerators_cdelta.html>See user documentation</a>).
        \n\n
        - <em>Particle physics related to final state simulated with \c CLUMPY</em>
           - \c gENUM_FINALSTATE: final states available for DM fluxes;
           - \c gENUM_NUFLAVOUR: neutrino flavours selections;
           - \c gENUM_PP_SPECTRUMMODEL: particle physics elementary spectra for DM annihilation and/or decay.
        \n\n
        - <em>Jeans analysis related</em>
           - \c ENUM_ANISOTROPYPROFILE: anisotropy profile
              \f$\beta_{\rm ani}(r)\equiv 1-\bar{v_{\theta}^2}(r)/\bar{v_r^2}(r)\f$;
           - \c gENUM_LIGHTPROFILE: surface brightness \f$\Sigma(R)\equiv I(R)\f$ or density \f$\rho(r)\equiv\nu(r)\f$
             light profile.
        .
       \n
       The advantage of enumerators/keyword lists (as described by their number <b>\c gN_XXX</b> and
       names <b>\c gNAMES_XXX</b> for each of the above category) is that the expressions
       of these lists (global variables <b>\c kXXX</b>) can be used directly in the code.
       Moreover, they have \c int type.
    \n\n
    -# <b>Free (user-defined) parameters</b>
       -# <em>Dark matter parameters</em> (\c gDM_XXX)
          - \c gDM_FLAG_CDELTA_DIST: global flag for \f$d{\cal P}/dc\f$ distribution;
          - \c gDM_LOGCDELTA_STDDEV: if \c gDM_FLAG_CDELTA_DIST=kLOGNORM, standard deviation \f$\log(c_{\Delta})\f$;
          - \c gDM_SUBS_MMIN: minimal mass \f$M_{min}^{subs}\f$ of DM structures;
          - \c gDM_MMAXFRAC_SUBS: fraction of host halo mass to define mass of most massive subhalo in hos;
          - \c gDM_RHOSAT: saturation density (high densities overcome by DM annihilation).
        \n\n
       -# <em>Galaxy (or other halo type) parameters</em> (\c gMW_XXX or \c gTYPE_XXX): we illustrate
       the parameters for the Galaxy (similar parameters for other types, with some changes).
          - \c gMW_CLUMPS_XXX: universal properties of Galactic clumps (spatial distribution + \f$c_{vir}-M_{vir}\f$ + ...);
          - \c gHALO_DPDM_SLOPE: universal mass distribution of subhalos in Galaxy (slope of \f$d{\cal P}/dM\f$);
          - \c gMW_DPDV_XXX: universal spatial distribution of subhalos in Galaxy (norm + \f$r_{s}\f$ + shape parameters);
          - \c gMW_TRIAXIAL_XXX: to enable and describe triaxial halos;
          - \c gMW_SUBS_XXX: parameters controlling the DM fraction in clumps (calculation depends on object type);
          - Description of total DM halo (norm, \f$r_{s}\f$, shape parameters):
              - \c gMW_TOT_XXX: exists only for Galaxy;
              - \c gLIST_HALOES: directly described in this file of halos provided.
        \n\n
       -# <em>Particle physics parameters</em> (\c gPP_XXX)
          - \c gPP_BR [gN_PP_BR]: vector of \c gN_PP_BR branching ratio values (for all channels);
          - \c gPP_DM_ANNIHIL_DELTA: whether the DM candidate is a Majorana or not;
          - \c gPP_DM_MASS_GEV: dark matter candidate mass;
          - \c gPP_DM_ANNIHIL_SIGMAV_CM3PERS: \f$\sigma v\f$ for the annihilating DM candidate;
          - \c gPP_FLAG_SPECTRUMMODEL: flag for selected particle physics parametrisation (used for gamma and nu spectra);
          - \c gPP_DM_IS_ANNIHIL_OR_DECAY: flag for annihilating or decaying DM.
        \n\n
       -# <em>Simulation related</em> (\c gSIM_XXX)
          - <em>Resolution</em> (\c gSIM_HEALPIX_NSIDE, \c gSIM_RESOLUTION, \c gSIM_ALPHAINT);
          - <em>Smoothing</em> (\c gSIM_GAUSSBEAM_GAMMA_FWHM, \c gSIM_GAUSSBEAM_NEUTRINO_FWHM,
            \c gSIM_HEALPIX_RING_WEIGHTS_DIR, \c gSIM_HEALPIX_ITER, \c gSIM_HEALPIX_NLMAX_FAC);
          - <em>Accuracy and seed</em> (\c gSIM_EPS, \c gSIM_EPS_DRAWN, \c gSIM_SEED);
          - <em> Outputs</em> (\c gSIM_FLUX_FLAG_NUFLAVOUR, \c gSIM_IS_WRITE_GALPOWERSPECTRUM, \c gSIM_IS_DISPLAY,
          \c gSIM_IS_PRINT, \c gSIM_OUTPUT_DIR).


*/
