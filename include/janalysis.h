#ifndef _CLUMPY_JANALYSIS_H_
#define _CLUMPY_JANALYSIS_H_

// ROOT includes
#if IS_ROOT
#include <TCanvas.h>
#include <TGraphAsymmErrors.h>
#include <TH1D.h>
#endif

// C++ std libraries
using namespace std;
#include <string>
#include <vector>

struct gStructJeansAnalysis {    //! Structure gathering parameters of a Jeans analysis minimization.
   bool   IsFreeParam[9];        //!< Whether or not log10(Rhos)/log10(Rscale)/ShapeParam1/ShapeParam2/ShapeParam3/Beta_Aniso_0/Beta_Aniso_Inf/log10(RAniso)/AnisoShapeParam is a free (true) or fixed (false) parameter
   double LowerRange[9];         //!< Lower range on log10(Rhos)/log10(Rscale)/ShapeParam1/ShapeParam2/ShapeParam3/Beta_Aniso_0/Beta_Aniso_Inf/log10(RAniso)/AnisoShapeParam
   double UpperRange[9];         //!< Upper range on log10(Rhos)/log10(Rscale)/ShapeParam1/ShapeParam2/ShapeParam3/Beta_Aniso_0/Beta_Aniso_Inf/log10(RAniso)/AnisoShapeParam
   double Value[9];              //!< (Initial) value of log10(Rhos)/log10(Rscale)/ShapeParam1/ShapeParam2/ShapeParam3/Beta_Aniso_0/Beta_Aniso_Inf/log10(RAniso)/AnisoShapeParam
   int    HaloProfile;           //!< Dark matter density profile
   int    LightProfile;          //!< Stellar light profile
   int    AnisoProfile;          //!< Stellar anisotropy profile
   double LightParam[5];         //!< Light profile parameters L/RLight/LightShapeParam1/LightShapeParam2/LightShapeParam3
   string Name;                  //!< Name of the object
   double Distance;              //!< Distance to the object [kpc]
   double Longitude;             //!< Longitude of the object [deg]
   double Latitude;              //!< Latitude of the object [deg]
   double HaloSize;              //!< Size of the dark matter halo [kpc]
};

struct gStructJeansData {        //! Structure storing data used for a Jeans analysis
   string         FileName;      //!< Name of Data file
   string         TypeData;      //!< Type of Data: Vel for velocity \f$[km/s]\f$, Sigmap for velocity dispersion \f$[km/s]\f$, Sigmap2 for velocity dispersion square \f$[km^{2}/s^{2}]\f$
   int            Ndata;         //!< Number of data points used for the analysis
   vector<double> Dec;           //!< Declination of the data point \f$[deg]\f$
   vector<double> RA;            //!< Right ascension of the data point \f$[deg]\f$
   double         Dec_center;    //!< Declination of the center of the object \f$[deg]\f$
   double         RA_center;     //!< Right ascension of the center of the object \f$[deg]\f$
   double         dist;          //!< Distance to the center of the object \f$[kpc]\f$
   vector<double> Radius;        //!< Radius of the data point \f$[kpc]\f$
   vector<double> ErrRadius;     //!< Error on the radius of the data point \f$[kpc]\f$
   vector<double> VelocData;     //!< Velocity data of the data point: velocity \f$[km/s]\f$, velocity dispersion \f$[km/s]\f$, velocity dispersion square \f$[km^{2}/s^{2}]\f$
   vector<double> ErrVelocData;  //!< Error on the velocity data of the data point (\f$[km/s]\f$ or \f$[km^{2}/s^{2}]\f$)
   vector<double> MembershipProb;//!< Membership probability of the data point. Only used for unbinned analysis
   vector<double> LightData;     //!< Light data of the data point: surface brightness \f$[kpc^{-2}]\f$
   vector<double> ErrLightData;  //!< Error on the light data of the data point \f$[/kpc^{-2}]\f$
};

struct gStructHalo {             //! Structure to store DM halo properties (including its Jeans data) and lists of possible halos
   string Name;                  //!< Name of the object
   string Type;                  //!< Type of the object (e.g., dSphs, clusters...)
   double PsiDeg;                //!< Galactic longitude [deg]
   double ThetaDeg;              //!< Galactic latitude [deg]
   double l;                     //!< Distance from Earth \f$[kpc]\f$
   double z;                     //!< Redshift
   struct gStructJeansData JeansData;//!< Kinematic and surface brightness data for this object
   int    HaloProfile;           //!< Chosen among values in gENUM_PROFILE (from \c params.h)
   double Rhos;                  //!< Normalisation \f$[M_{\odot} \cdot kpc^{-3}]\f$
   double Rscale;                //!< Scale radius \f$[kpc]\f$
   double Rvir;                  //!< Radius of the halo \f$[kpc]\f$
   double ShapeParam1;           //!< \f$\alpha\f$ (kZHAO or kEINASTO) or \f$n\f$ (kEINASTO)
   double ShapeParam2;           //!< \f$\beta\f$ (kZHAO) or unused (kEINASTO and kEINASTO_N)
   double ShapeParam3;           //!< \f$\gamma\f$ (kZHAO) or unused (kEINASTO and kEINASTO_N)
   int    LightProfile;          //!< Light profiles (chosen among gENUM_LIGHTPROFILE from \c params.h)
   double L   ;                  //!< Light profile normalization \f$[L_{\odot} ]\f$
   double RLight;                //!< Light profile scale radii \f$[kpc]\f$
   double LightShapeParam1;      //!< \f$\alpha_{light}\f$ (kZHAOLIGHT) or unused (kPLUMMER)
   double LightShapeParam2;      //!< \f$\beta_{light}\f$ (kZHAOLIGHT) or unused (kPLUMMER)
   double LightShapeParam3;      //!< \f$\gamma_{light}\f$ (kZHAOLIGHT) or unused (kPLUMMER)

   double Beta_Aniso_0;          //!< \f$\beta_0\f$
   double Beta_Aniso_Inf;        //!< \f$\beta_{\infty}\f$ (kBAES) or unused (kCONSTANT)
   int    AnisoProfile;          //!< Anisotropy profiles (chosen among gENUM_ANISOTROPYPROFILE from \c params.h)
   double RAniso;                //!< Anisotropy profile scale radii \f$[kpc]\f$ (kBAES) or unused (kCONSTANT)
   double AnisoShapeParam;       //!< \f$\eta\f$ (kBAES) or unused (kCONSTANT)

   double Mtot;                  //!< Total mass of the halo (calculated from the halo parameters)
   double Subs_mfrac;            //!< Mass fraction in subclumps
   double Subs_Mmax;             //!< Maximal mass of subclumps in the host halo \f$[M_{\odot}\f$
   double Subs_dPdM_Slope;       //!< Mass distribution (dPdM) index alphaM of subclumps
   double Subs_dPdV_Rscale;      //!< dPdV (spatial distribution of subclumps) scale radius \f$[kpc]\f$
   double Subs_dPdV_Rs_to_Rshost;//!< ratio of dPdV scale radius to total halo scale radius.
   int    Subs_dPdV_Profile;     //!< dPdV profile family (gENUM_PROFILE)
   double Subs_dPdV_ShapeParam1; //!< dPdV shape parameter #1
   double Subs_dPdV_ShapeParam2; //!< dPdV shape parameter #2
   double Subs_dPdV_ShapeParam3; //!< dPdV shape parameter #3
   int    Subs_Inner_CDELTAMDELTA;   //!< \f$c_{vir}-M_{vir}\f$ relationship (from gENUM_CDELTAMDELTA)
   int    Subs_Inner_Profile;    //!< Sub-clumps inner profile family (for all clumps)
   double Subs_Inner_ShapeParam1;//!< Sub-clumps shape parameter #1
   double Subs_Inner_ShapeParam2;//!< Sub-clumps shape parameter #2
   double Subs_Inner_ShapeParam3;//!< Sub-clumps shape parameter #3
   double Triaxial_a;            //!< Major axis (if no rotation angle below, along x_los)
   double Triaxial_b;            //!< Second axis (if no rotation angle below, along y_los)
   double Triaxial_c;            //!< Minor axis (if no rotation angle below, along z_los)
   bool   Triaxial_Is;           //!< Whether the halo is triaxial or not
   double Triaxial_rotalpha;     //!< Euler \f$\alpha\f$ rotation angle (xyz_los coordinates, see \c geometry.h)
   double Triaxial_rotbeta;      //!< Euler \f$\beta\f$ rotation angle (xyz_los coordinates, see \c geometry.h)
   double Triaxial_rotgamma;     //!< Euler \f$\gamma\f$ rotation angle (xyz_los coordinates, see \c geometry.h)
   int    Ndata;                 //!< Number of data points used for the MCMC fit
   int    Npar;                  //!< Number of free parameters for the MCMC fit

   vector<double> StatChi2;      //!< Vector of chi2 values (for all entries from the statistical analysis)
   bool StatIsFreeParam[17];     //!< Whether or not Rhos/Rscale/Rvir/ShapeParam1/ShapeParam2/ShapeParam3/L/RLight/LightShapeParam1/LightShapeParam2/LightShapeParam3/Beta_Aniso_0/Beta_Aniso_Inf/RAniso/AnisoShapeParam/RA_cnt/Dec_cnt is a free(true) or fixed (false) parameter
   vector<double> StatParam[17]; //!< Array of vectors of stat parameters. These parameters are, in this order: Rhos/Rscale/Rvir/ShapeParam1/ShapeParam2/ShapeParam3/L/RLight/LightShapeParam1/LightShapeParam2/LightShapeParam3/Beta_Aniso_0/Beta_Aniso_Inf/RAniso/AnisoShapeParam/RA_cnt/Dec_cnt
   vector<int> StatHaloProfile;  //!< Vector of profiles (chosen among gENUM_PROFILE from \c params.h)
   vector<int> StatLightProfile; //!< Vector of Light profiles (chosen among gENUM_LIGHTPROFILE from \c params.h)
   vector<int> StatAnisoProfile; //!< Vector of Anisotropy profiles (chosen among gENUM_ANISOTROPYPROFILE from \c params.h)
};

void   convert_to_PP_units(int switch_qty, double &val_y);
void   convert_to_PP_units(int switch_qty, int n_y, double val_y[]);

void   gal_j1D(vector<double> const &x, int switch_y, bool is_list_halos = false, double psi_los = 0, double theta_los = 0, double const &contrast_thresh = 1., double const &phi_cut_deg = 0.);
void   gal_j2D(double &psi, string const &theta_deg_str, string const &theta_orth_size_deg_str, double &dtheta, int switch_j, double const &user_rse = 0.);
void   gal_set_pardpdv(double par_dpdv[10]);
void   gal_set_parsmooth(double par_smooth[21]);
void   gal_set_parsubs(double par_subs[25]);
void   gal_set_partot(double par_tot[10]);

void   halo_files2files_data(string const &data_file, vector<string> &data_files);
double halo_findfracjtot_alphaint(int i, vector<struct gStructHalo> &list_halos, double const &frac);
void   halo_fracjpointlike_dist(string const &file_halos, double const &alphaint, double const &frac, bool is_annihil, double const &rho_sat);
void   halo_fracjtot_alphaint(string const &file_halos, double const &frac, bool is_annihil, double const &rho_sat);
int    halo_get_index(string const &name, string const &type, vector<struct gStructHalo> &list_halos);
string halo_get_name(int i, vector<struct gStructHalo> &list_halos);
string halo_get_type(int i, vector<struct gStructHalo> &list_halos);
void   halo_jeans(vector<double> const &x, int switch_y);
void   halo_j1D(vector<double> const &x, int switch_y);
void   halo_j2D(string const &name_halo, double const &fov_diam, bool is_subs_drawn = false, double const &user_rse = 0.);
double halo_jtot(int i, vector<struct gStructHalo> &list_halos, double const &psi_los, double const &theta_los, double const &eps, bool is_halo_set_to_0_0);
double halo_jtot(int i, vector<struct gStructHalo> &list_halos, double const &psi_los, double const &theta_los, double const &eps, bool is_halo_set_to_0_0, double &jsm, double &jsubs, double &jcrossprod);
void   halo_load_data4jeans(string const &data_file, struct gStructJeansData &data_halo, bool is_verbose);
void   halo_load_data4jeans(string const &data_file, struct gStructHalo &stat_halo, bool is_verbose);
void   halo_load_list(string const &file_halos, vector<struct gStructHalo> &list_halos, bool is_clear = true);
void   halo_load_list4jeans(string const &file_halos, vector<struct gStructHalo> &list_halos, int switch_y, bool is_clear = true);
void   halo_set_pardpdv(int i, double par_dpdv[10], vector<struct gStructHalo> &list_halos, bool is_halo_set_to_0_0);
void   halo_set_parsmooth(int i, double par_smooth[21], vector<struct gStructHalo> &list_halos, bool is_halo_set_to_0_0);
void   halo_set_parsubs(int i, double par_subs[25], vector<struct gStructHalo> &list_halos);
void   halo_set_partot(int i, double par_tot[11], vector<struct gStructHalo> &list_halos, bool is_halo_set_to_0_0);
void   halo_set_parjeans(int i, double par_jeans[20], vector<struct gStructHalo> &list_halos);
void   halo_set_triaxiality(int i, vector<struct gStructHalo> &list_halos);

void   plot_c_lum_boost_vs_m(double mmin, double mmax, int n_m, string const &list_cvir, double mmin_pp, double frac_max, double f_dm, string const &list_alpham, int nlevel, double sigma_cvir);
#if IS_ROOT
void   pop_study_plots(vector<struct gStructHalo> &list_halos, vector<pair<double, int> > &list_j, vector<pair<double, int> > &list_contrast, vector<pair<double, int> > &list_boost, TCanvas **c_pop, TH1D **h_pop, TGraphAsymmErrors **gr_pop, double const &phi_cut_deg = 0., string const &pop_name = "", string const &filename_root_str = "NULL");
#endif
void   pop_study_sort_j(vector<struct gStructHalo> &list_halos, vector<pair<double, int> > &list_j, vector<pair<double, int> > &list_contrast, vector<pair<double, int> > &list_boost, double const &contrast_thresh, bool is_print = true);

void   stat_CLs(vector<double> const &x, string const &stat_files, vector<double> const &cls, int switch_y, double const &rho_sat, double const &eps = 1.e-3, double const &alphaint = 0., int switch_stat = 0, string const &data_files = "unused");
void   stat_draw_chi2(string const &stat_files, bool is_logl_or_chi2);
void   stat_draw_correlations(vector<int> const &ids, string const &stat_file, double const &rkpc_for_mr = 0., double const &rho_sat = 1.e19);
void   stat_file2files(string const &stat_file, vector<string> &stat_files);
int    stat_find_bestmodel(string const &stat_files, int switch_stat);
void   stat_find_CLs(string const &stat_files, double const &cl, int switch_stat = 0);
void   stat_load_list(string const &stat_file, struct gStructHalo &stat_halo, bool is_verbose = true);
void   stat_set_paranis(int i, double par_anis[5], struct gStructHalo &stat_halo);
void   stat_set_parjeans(int i, double par_jeans[20], struct gStructHalo &stat_halo);
void   stat_set_parlight(int i, double par_light[8], struct gStructHalo &stat_halo);
void   stat_set_partot(int i, double par_tot[10], struct gStructHalo &stat_halo, bool is_halo_set_to_0_0);

string units_or_canvasname_1D(int switch_y, int switch_name, bool is_norm = false);

#endif

/*! \file janalysis.h
  \brief \f$J\f$-factor (Gal. w/o sub-halo mean/drawn w/o list) for annihil. \f$[M_\odot^2\,\,kpc^{-5}]\f$ or decay \f$[M_\odot\,\, kpc^{-2}]\f$

   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><A href="#load_all">I. Load DM haloes, Jeans-related data and set \c CLUMPY parameters for \c ./bin/clumpy calculations</A></b>\n
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><A href="#gal_fun">II. Analysis for Galactic halo (w/wo sub-clumps and list of halos)</A>: <tt>./bin/clumpy -g</tt></b>\n
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><A href="#halo_loaded">III. Analysis of halos (not the Galactic halo)</A>: <tt>./bin/clumpy -h</tt></b>\n
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><A href="#halo_stat">IV. Statistical analysis for a single halo (PDF and CLs)</A>: <tt>./bin/clumpy -s</tt></b>\n
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><A href="#analysis_other">V. Other functions</A></b>

\n

 <CENTER>________________________________________</CENTER>\n

<b><A NAME="load_all">I. Load DM haloes, Jeans-related data and set \c CLUMPY parameters for \c ./bin/clumpy calculations</A></b>\n

Note that for all calculations, the DM halo properties (Galaxy or not) must be set first.

<b>&nbsp;&nbsp;&nbsp; 1. Load and set parameters for Galaxy</b>: fills parameters Galactic halo (total, smooth, etc.),
always after \c load_parameters() in \c params.h [to load \c clumpy_params.txt]
   - \c gal_set_pardpdv(): spatial distribution \f$d{\cal P}/dV\f$ of clumps
   - \c gal_set_parsmooth(): smooth DM halo \f$\rho_{\rm sm}\f$
   - \c gal_set_parsubs(): mass distribution, number and profiles for the Galactic clumps (any level)
   - \c gal_set_partot(): total DM halo \f$\rho_{\rm tot}\f$ of the Galaxy

\n\n

<b>&nbsp;&nbsp;&nbsp; 2. Load and set parameters for haloes (not Galaxy)</b>: fills parameters for DM halo (total, smooth, etc.)
from the i-th DM halo in the list \c list_halos loaded with \c halo_load_list(), \c halo_load_list4jeans(), \c halo_load_data4jeans()
   - <em>Load list of files (data for jeans analysis) or single file</em>: must always be called first, to stores
       a list of halo properties in a vector.
      - \c halo_load_list(): must always be called first, to stores a list of halo properties in a vector.
          \param[in]  file_halos           Path to the list-formatted file to be read (\c string).
          \param[out] list_halos           Fill <c> vector<struct gStructHalo> list_halos</c> to store halo properties
          \param[in]  is_clear[opt=true]   If multiple calls, keep only last file read (\c true), or merge all halos found (\c false)
      - \c halo_load_list4jeans(): must always be called first in case of a Jeans analysis, to store the list of halo properties (for Jeans analysis, e.g. <A href="../data/list_generic_jeans.txt" target="_blank">list_generic_jeans.txt</A>).
          \param[in]  file_halos          Path to the list-formatted file to be read (\c string).
          \param[out] list_halos          Fill <c> vector<struct gStructJeansAnalysis> list_halos</c> to store halo properties
          \param[in]  switch_y            If data files are present, switch to determine which data to select:
                                             - 0 \f$\Rightarrow\f$ Kinematic data (e.g. \f$\sigma_{p}\f$)
                                             - 1 \f$\Rightarrow\f$ Surface brightness data \f$I(R)\f$
          \param[in]  is_clear[opt=true]     If multiple calls, keep only last file read (\c true), or merge all halos found (\c false)
      - \c halo_load_data4jeans(): loads a Jeans analysis data file (Kinematics or Surface Brightness), stored either in a \c gStructJeansData or a \c gStructHalo structure.
           \param[in]  data_file          Jeans analysis data file to be read
           \param[in,out]  data_halo      Structure to store the data. It can be either a \c gStructJeansData or a \c gStructHalo structure.
           \param[in]  is_verbose         Chatter or not...
      - \c halo_files2files_data(): from single 'data' file or list of such files, returns a \c vector of files.
           \param[in]  data_file          Single 'data' file (e.g., <A href="../data/data_vel.txt" target="_blank">data/data_vel.txt</A>) or list of such files
           \param[out] data_files         Vector of all 'data' file names
   - <em>Set DM or Jeans parameters from loaded files</em>
      - \c halo_set_pardpdv(): spatial distribution \f$d{\cal P}/dV\f$ of clumps
      - \c halo_set_parjeans(): Jeans analysis parameters (light, velocity dispersion, and DM profiles)
      - \c halo_set_parsmooth(): smooth DM halo \f$\rho_{\rm sm}\f$
      - \c halo_set_parsubs(): mass distribution, number and profiles for the halo clumps (any level)
      - \c halo_set_partot(): total DM halo \f$\rho_{\rm tot}\f$ of the halo
      - \c halo_set_triaxiality(): sets global parameters \c gHALO_TRIAXIALITY_XXX

\n\n

<b>&nbsp;&nbsp;&nbsp; 3. Load and set parameters for statistical properties of haloes</b>: fills vector of parameters for DM halo
(total, smooth, etc.) from the i-th DM halo in the list \c list_halos loaded with \c stat_load_list()
   - <em>Load list of files or single file:</em> must always be called first, to stores a list of 'statistical' halo properties in a vector.
      - \c stat_file2files(): from single 'statistical' file or list of such files, returns a \c vector of files.
          \param[in]  stat_file             Single 'statistical' file (e.g., <A href="../data/stat_example.dat" target="_blank">data/stat_example.dat</A>) or list of such files
          \param[out] stat_files            Vector of all 'statistical' file names
      - \c stat_load_list():
          \param[in]  stat_file             Path to the file to be read (\c string)
          \param[out] stat_halo             Fill <c> vector<struct gStructHalo> list_halos</c> from file
          \param[in]  is_verbose[true]      Print info or not on the loaded file
   - <em>Set DM or Jeans 'statistical' parameters from loaded files</em>
      - \c stat_set_paranis(): anisotropy profile parameters (Jeans analysis), see \c jeans_analysis.h
      - \c stat_set_parjeans(): Jeans analysis parameters (light, velocity dispersion, and DM profiles)
      - \c stat_set_parlight(): light profile parameters (Jeans analysis), see \c jeans_analysis.h
      - \c stat_set_partot(): total DM halo \f$\rho_{\rm tot}\f$


\n


 <CENTER>________________________________________</CENTER>\n

<b><A NAME="gal_fun">II. Analysis for Galactic halo (w/wo sub-clumps and list of halos)</A></b>: <tt>./bin/clumpy -g</tt>\n

All functions starting with \c gal_XXX are used to calculate Galactic J-factors (for decay or annihilation).
The Galactic DM profiles and parameters are loaded from a CLUMPY parameter-formatted file
(e.g. <A href="../clumpy_params.txt" target="_blank">clumpy_params.txt</A>), to calculate/print/plot
\f$M(r)\f$, \f$\rho(r)\f$, \f$J(\theta)\f$... and 1D and 2D maps for \f$J(\alpha_{\rm int})\f$.
Galactic subclumps can also be drawn, and a list of known halos (e.g. dSphs) can also be taken into account
(\c gLIST_HALOES in the parameter file read, e.g. <A href="../data/list_generic.txt" target="_blank">data/list_generic.txt</A>
or <A href="../data/list_generic_triaxial.txt" target="_blank">data/list_generic_triaxial.txt</A>).


The main functions called in \c clumpy.cc are \c gal_j1D() and \c gal_j2D().
While both functions output plots, the second one also generates files containing the
result of the calculation. Note that in the 2D mode, drawn subclumps of the host halo is enabled.

   - \c gal_j1D(): 1D profiles like \f$\rho(r)\f$, \f$J(\theta)\f$ and \f$J(\alpha_{\rm int})\f$
            w/wo \f$<J_{\rm sub}>\f$.
       \param[in]  x                     Vector of x-axis values (see \c switch_f)
       \param[in]  param_file            CLUMPY parameter file (e.g. <A href="../clumpy_params.txt" target="_blank">clumpy_params.txt</A>)
       \param[in]  switch_y              Switch to select y(x):
                                            - 0 \f$\Rightarrow y(x)=\rho(r)\f$ with \f$[x]=[kpc]\f$ and \f$[y]=[M_\odot\,\,kpc^{-3}]\f$
                                            - 1 \f$\Rightarrow y(x)=J(\alpha_{\rm int})\f$
                                            - 2 \f$\Rightarrow y(x)=J(\theta)\f$
       \param[in]  is_list_halos         Flag to add (or not) on the plot the \f$J\f$-factor from halos in \c gLIST_HALOES
       \param[in]  psi_los               [if \c switch_y=1] \f$\psi_{los}\f$ - longitude of the line of sight [rad]
       \param[in]  theta_los             [if \c switch_y=1] \f$\theta_{los}\f$ - latitude of the line of sight [rad]
       \param[in]  contrast_thresh       [if \c switch_y=2 and \c is_list_halos=true] Print/sort on screen halos
                                         with \f$J_{\rm halo}/J_{\rm  Gal.bkd}> {\rm contrast\_thresh}\f$
       \param[in]  phi_cut_deg           [if \c switch_y=2 and \c is_list_halos=true], discard objects with phi (w.r.t. Gal. centre) < phi_cut_deg
   \n\n
   - \c gal_j2D(): 2D skymap with drawn clumps in the \f$(\psi,\,\theta)\f$ direction
           for a FOV of \c psi_size_deg \f$\times \f$ \c theta_size_deg.
       \param[in]  param_file            CLUMPY parameter file (e.g. <A href="../clumpy_params.txt" target="_blank">clumpy_params.txt</A>)
       \param[in]  psi_deg               \f$\psi_{los}\f$ - l.o.s. Galactic longitude [deg]
       \param[in]  theta_deg             \f$\theta_{los}\f$ - l.o.s. Galactic latitude [deg]
       \param[in]  psi_size_deg          \f$\Delta\psi\f$ - skymap for \f$[\psi-\Delta\psi/2, \psi+\Delta\psi/2]\f$ [deg]
       \param[in]  theta_size_deg        \f$\Delta\theta\f$ - skymap for \f$[\theta-\Delta\theta/2, \theta+\Delta\theta/2]\f$ [deg]
       \param[in]  switch_j              Switch for the contributions to consider in the calculation
                                            - 0 \f$\Rightarrow J_{\rm sm}+J_{<{\rm sub}>} \f$
                                            - 1 \f$\Rightarrow J_{\rm sm}+J_{<{\rm sub}>}+J_{\rm list} \f$
                                            - 2 \f$\Rightarrow J_{\rm sm}+J_{<{\rm sub}>(lcrit)}+J_{\rm drawn} \f$
                                            - 3 \f$\Rightarrow J_{\rm sm}+J_{<{\rm sub}>(lcrit)}+J_{\rm drawn}+J_{\rm list} \f$
       \param[in]  user_rse              [if \c switch_j=2 or \c 3] relative standard error allowed (for subclumps to be drawn)
       \param[in]  is_subs_list          Whether to add or not the contribution of \f$J_{<subs>}^{\rm halo}\f$ when \f$J_{\rm list}\f$ is demanded


\n


 <CENTER>________________________________________</CENTER>\n

<b><A NAME="halo_loaded">III. Analysis of halos (not the Galactic halo)</A></b>: <tt>./bin/clumpy -h</tt>\n
All functions starting with \c halo_XXX are used on a list-formatted file
(see, e.g., <A href="../data/list_generic.txt" target="_blank">data/list_generic.txt</A>
or <A href="../data/list_generic_triaxial.txt" target="_blank">data/list_generic_triaxial.txt</A>)
to calculate/print/plot \f$M(r)\f$,  \f$\rho(r)\f$, \f$J(\theta)\f$... and 1D and 2D maps for \f$J(\alpha_{\rm int})\f$.

The main functions called in \c clumpy.cc are \c halo_j1D(), \c halo_j2D(). As with the <tt> -g </tt> option, both functions
produce plots but the second one only generates output files as well. Note that in the 2D mode, drawn
subclumps of the host halo is enabled.

   - \c halo_j1D(): 1D profiles like \f$\rho(r)\f$, \f$J(\theta)\f$ and \f$J(\alpha_{\rm int})\f$ w/wo \f$<J_{subcl}>\f$.
       \param[in]  x                      Vector of x-axis values (see \c switch_y)
       \param[in]  param_file             CLUMPY parameter file (e.g. <A href="../clumpy_params.txt" target="_blank">clumpy_params.txt</A>): then loads \c gLIST_HALOES file
       \param[in]  switch_y               Switch to select y(x)
                                             - 0 \f$\Rightarrow y(x)=\rho(r)\f$ with \f$[x]=[kpc]\f$ and \f$[y]=[M_\odot\,\,kpc^{-3}]\f$
                                             - 1 \f$\Rightarrow y(x)=J(\alpha_{\rm int})\f$
                                             - 2 \f$\Rightarrow y(x)=J(\theta)\f$
       \param[in]  is_jshell              If \c true, also plot J in shells (if \c switch_y=1)
   \n\n\n
   - \c halo_j2D(): 2D skymap (with drawn or mean Jhalo_subcl) around a specific halo.
       \param[in]  param_file             CLUMPY parameter file (e.g. <A href="../clumpy_params.txt" target="_blank">clumpy_params.txt</A>): then loads \c gLIST_HALOES file
       \param[in]  name_in_list           Name (case insensitive) of the halo for which a 2D map is demanded
       \param[in]  fov_deg                Field of view of the skymap [deg]
       \param[in]  is_subs_drawn          Switch to draw subclumps (if \c false, only calculate mean value)
       \param[in]  user_rse               [if \c is_subs_drawn=true] relative standard error allowed (for subclumps to be drawn)
   \n\n
   - \c halo_jeans(): 1D Jeans analysis-related profiles \f$\sigma_p(R)\f$, \f$I(R)\f$ and \f$\beta_{ani}(r)\f$.
       \param[in]  x                      Vector of x-axis values (see \c switch_y)
       \param[in]  param_file             CLUMPY parameter file (e.g. <A href="../clumpy_params.txt" target="_blank">clumpy_params.txt</A>): then loads \c gLIST_HALOES_JEANS file
       \param[in]  switch_y               Switch to select y(x)
                                             - 0 \f$\Rightarrow y(x)=\sigma_p(R)\f$ with \f$[x]=[kpc]\f$ and \f$[y]=[km\,\,s^{-1}]\f$
                                             - 1 \f$\Rightarrow y(x)=I(R)\f$ with \f$[x]=[kpc]\f$ and \f$[y]=[L_\odot\,\,kpc^{-2}]\f$
                                             - 2 \f$\Rightarrow y(x)=\beta_{ani}(r)\f$ with \f$[x]=[kpc]\f$
   \n\n
   - \c halo_jtot(): returns the sum (or detail) of all contributions (smooth, sub-halos and cross-product) for the i-th halo.
        \param[in]  i                      i-th halo of \c list_halos
        \param[in]  list_halos            <c> vector<struct gStructHalo></c> of halos loaded from \c halo_load_list()
        \param[in]  psi_los               \f$\psi_{los}\f$ - longitude of the line of sight [rad]
        \param[in]  theta_los             \f$\theta_{los}\f$ - latitude of the line of sight [rad]
        \param[in]  eps                   \f$\epsilon\f$ - relative precision sought for integrations
        \param[out] jsm                   \f$J_{smooth}(\psi_{los},\theta_{los})\f$ [optional]
        \param[out] jsubs                 \f$J_{<subs>}(\psi_{los},\theta_{los})\f$ [optional]
        \param[out] jcrossprod            \f$J_{<cross-prod>}(\psi_{los},\theta_{los})\f$ [optional]
   \n\n
   - Recursively find some quantity in the halo
      - \c halo_findfracjtot_alphaint(): finds and returns the integration angle \f$\alpha_f\f$ [rad] for which \f$J\f$ is a fraction of \f$J_{\rm max}\f$
                                   (when the integration encompasses the whole halo) for the i-th halo in list
      - \c halo_fracjpointlike_dist(): finds and prints the distance \f$d_f\f$for which \f$J(d_f) = f \cdot J_{\rm point-like} = f \cdot {\cal L}/d_f^2\f$.
      - \c halo_fracjtot_alphaint(): finds and prints the integration angle\f$\alpha_f\f$  for which \f$J(\alpha_f) = f \cdot J_{\rm tot}\f$.
   \n\n
   - Various low-level functions:
      - \c halo_get_index(): index of the halo corresponding to 'name' and 'type' (case insensitive) in \c list_halos
      - \c halo_get_name(): ROOT-compliant (no '+-'...) name of the object for ROOT canvas names
      - \c halo_get_type(): ROOT-compliant (i.e., no '+-'...) type name of the object


\n


 <CENTER>________________________________________</CENTER>\n

<b><A NAME="halo_stat">IV. Statistical analysis for a single halo (PDF and CLs)</A>: <tt>./bin/clumpy -s</tt></b>
In all functions starting with \c stat_XXX (called when <tt> ./bin/clumpy -s</tt> is executed, see \c clumpy.cc), 'stat' stands
for statistical analysis. Actually, these functions apply on profiles to calculate/print/plot \f$\rho(r)\f$,
\f$J(\alpha_{\rm int})\f$, and \f$J(\theta)\f$. By statistical analysis, we mean that a first analysis (not performed
with CLUMPY) provided a list of 'acceptable' profiles for a given halo (e.g. for the DM halo of a dwarf spheroidal
Galaxies). An example of a statistical-formatted file is <A href="../data/stat_example.dat" target="_blank">data/stat_example.dat</A>.
CLUMPY functions read all these possible profiles and calculate the mean, median... and CLs for
\f$\rho(r)\f$, \f$J(\alpha_{\rm int})\f$, and \f$J(\theta)\f$.

   - \c stat_CLs(): 1D CLs on \f$\rho(r)\f$, \f$J(\theta)\f$ and \f$J(\alpha_{\rm int})\f$.
        \param[in]  x                     Vector of x-axis values (see \c switch_y)
        \param[in]  stat_files            Single 'statistical' file (e.g., <A href="../data/stat_example.dat" target="_blank">data/stat_example.dat</A>) or list of such files
        \param[in]  cls                   Vector of CLs to use [%]
        \param[in]  switch_y              Switch to select y(x)
                                             - 0 \f$\Rightarrow y(x)=\rho(r)\f$ with \f$[x]=[kpc]\f$ and \f$[y]=[M_\odot\,\,kpc^{-3}]\f$
                                             - 1 \f$\Rightarrow y(x)=J(\alpha_{\rm int})\f$
                                             - 2 \f$\Rightarrow y(x)=J(\theta)\f$
                                             - 3 \f$\Rightarrow y(x)=\sigma_p(R)\f$
                                             - 4 \f$\Rightarrow y(x)=\nu(r) \times v_r^2(r)\f$
                                             - 5 \f$\Rightarrow y(x)=\beta_{ani}(r)\f$
                                             - 6 \f$\Rightarrow y(x)=\nu(r)\f$
                                             - 7 \f$\Rightarrow y(x)=I(R)     \f$
        \param[in]  rho_sat[=1.e19]       Saturation density \f$M_\odot~kpc^{-3}\f$
        \param[in]  eps                   \f$\epsilon\f$ - relative precision sought (non-applicable for \c switch_y=0)
        \param[in]  alphaint_deg          \f$\alpha_{\rm int}\f$ - integration angle [deg] (applicable only for \c switch_y=2)
        \param[in]  switch_stat           Switch method to calculate CLs (see App. F of
                                          <A href="http://adsabs.harvard.edu/abs/2011MNRAS.418.1526C" target="_blank">Charbonnier et al. 2011</A>)
                                             - 0 \f$\Rightarrow\f$ PDF method [recommanded]
                                             - 1 \f$\Rightarrow \chi^2\f$ method
                                             - 2 \f$\Rightarrow \f$ both methods
                                             - 3 \f$\Rightarrow\f$ mean and dispersion (e.g., for boostrap method)
   \n\n
   - \c stat_draw_chi2(): \f$\chi^2\f$ and \f$\chi^2/d.o.f.\f$ distributions (not always meaningful!).
        \param[in] stat_files            Single 'statistical' file (e.g., <A href="../data/stat_example.dat" target="_blank">data/stat_example.dat</A>) or list of such files
   \n\n
   - \c stat_draw_correlations(): draw PDF and correlation between the parameters of an MCMC file.
        \param[in]  stat_file             Single 'statistical' file (e.g., <A href="../data/stat_example.dat" target="_blank">data/stat_example.dat</A>)
        \param[in]  n_par[<=5]            Parameters of the analysis (\f$\rho_s\f$,\f$r_s\f$, \f$\alpha\f$, \f$\beta\f$, \f$\gamma\f$)
        \param[in]  rkpc_for_mr[=0]       If positive, plots/uses \f$M_{r}~[M_\odot]\f$ instead of \f$\rho_s\f$
        \param[in]  rho_sat[=1.e19]       Saturation density \f$[M_\odot~kpc^{-3}]\f$
   \n\n
   - \c stat_find_bestmodel(): finds and prints parameters for best-models (best \f$\chi^2\f$ or (log) likelihood) from the 'statistical' files.
        \param[in]  stat_files            Single 'statistical' file (e.g., <A href="../data/stat_example.dat" target="_blank">data/stat_example.dat</A>) or list of such files
        \param[in]  switch_stat           Switch method to calculate best-fit
                                             - 0 \f$\Rightarrow\f$ (log) Likelihood
                                             - 1 \f$\Rightarrow \chi^2\f$
   \n\n
   - \c stat_find_CLs(): calculates CLs for any profile parameters and/or for \f$M(r_0)\f$, \f$\rho(r_0)\f$,\f$J(\alpha_0)\f$, ... from a 'stat' file.
        \param[in]  stat_files            Single 'statistical' file (e.g., <A href="../data/stat_example.dat" target="_blank">data/stat_example.dat</A>) or list of such files
        \param[in]  cl                    Confidence level to use [%]
        \param[in]  switch_stat    Switch method to calculate CLs (see App. F of
                                          <A href="http://adsabs.harvard.edu/abs/2011MNRAS.418.1526C" target="_blank">Charbonnier et al. 2011</A>)
                                             - 0 \f$\Rightarrow\f$ PDF method [recommanded]
                                             - 1 \f$\Rightarrow \chi^2\f$ method
                                             - 2 \f$\Rightarrow \f$ both methods
                                             - 3 \f$\Rightarrow\f$ mean and dispersion (e.g., for boostrap method)

\n


 <CENTER>________________________________________</CENTER>\n

<b><A NAME="analysis_other"> V. Other functions</A></b>

   - \c convert_to_PP_units(): convert units('astrophysics' or 'particle physics') depending on \c gSIM_IS_ASTRO_OR_PP_UNITS.
        \param[in]  switch_qty              Quantity to convert
                                               - 0 is DM density \f$\rho\f$ (\f$[M_{\odot}~kpc^{-3}]\f$ or \f$[GeV~cm^{-2}]\f$)
                                               - 1 is annihilation \f$J\f$ (\f$[M_{\odot}^{2}~kpc^{-5}]\f$ or decay \f$[GeV^{2}~cm^{-5}]\f$) or \f$D\f$ (\f$[M_{\odot}~kpc^{-2}]\f$ or \f$[GeV~cm^{-2}]\f$)
                                               - 2 is mass  (\f$[M_{\odot}]\f$ or \f$[GeV]\f$)
        \param[in,out]  n_y                 Size of vector of values to convert
        \param[in,out]  val_y               To convert/converted (single value or vector)
   \n\n
   - \c plot_c_lum_boost_vs_m() is used to display a comparison of \f$c_\Delta-M_\Delta\f$ relationship and the
                          resulting \f${\cal L}(M_{\rm vir})\f$ for various DM profiles.
        \param[in]  param_file           Name of the CLUMPY parameter file to load
        \param[in]  mmin                 Minimal mass to plot
        \param[in]  mmax                 Maximal mass to plot
        \param[in]  n_m                  Number of mass bins to plot
        \param[in]  list_cvir            Comma-separated list of \f$c_{\rm vir}-m_{\rm vir}\f$ to plot (ALL to display all)
        \param[in]  mmin_pp              Minimal mass of the DM substructures
        \param[in]  frac_mmax            Mass fraction of the heaviest halo in host (e.g., 0.1)
        \param[in]  f_dm                 Fraction of DM in substructures at each level (e.g., 0.2)
        \param[in]  list_alpham          Comma-separated list of DM slopes (\f$dP/dM\sim M^{-\alpha_{m}}\f$ in [1.8-2])
        \param[in]  nlevel               Number of level of sub-sub-sub... to consider (1= no substructures)
        \param[in]  sigma_cvir           If non-null, use log-norm distribution of c around \f$<c>\f$ of width sigma_cvir (e.g., 0.14)
   \n\n
   - \c pop_study_plots() is used to display graphs related to population studies
        \param[in,out]  list_halos        <c> vector<struct gStructHalo></c> of halos loaded from \c halo_load_list()
        \param[in,out]  list_j            Vector of pairs of \f$<J_{\rm halo}(\alpha_{\rm int}),{\rm index}>\f$ values (for each halo in \c list_halos)
        \param[in,out]  list_contrast     Vector of pairs of \f$<J_{\rm halo}(\alpha_{\rm int})/J_{\rm Gal.bkg}(\alpha_{\rm int}),{\rm index}>\f$ values (for each halo in \c list_halos)
        \param[in,out]  list_boost        Vector of pairs of \f$<Boost,{\rm index}>\f$ values if aplicable (for each halo in \c list_halos)
        \param[in,out]  c_pop             Root canvas
        \param[in,out]  h_pop             Root histos for displays
        \param[in,out]  gr_pop            Root graph for displays
        \param[in]  phi_cut_deg           If angle of the object (w.r.t. Gal. centre) < phi_cut_deg, the object is discarded
        \param[in]  pop_name              Name added to graphs & canvas names (to differentiate pop if function called several times)
   \n\n
   - \c pop_study_sort_j() prints on screen the brightest halos sorted according to their \f$J_{\rm halo}(\alpha_{\rm int})\f$ values
                    and also to their contrast w.r.t. the Gal. background taken at the same position (i.e.
                    \f$J_{\rm halo}/J_{\rm  Gal.bkd}>{\rm contrast\_thresh}\f$). Returns these values sorted
                    (by J or by contrast).
        \param[in]  list_halos            <c> vector<struct gStructHalo></c> of halos loaded from \c halo_load_list()
        \param[in,out]  list_j            Vector of pairs of \f$<J_{\rm halo}(\alpha_{\rm int}),{\rm index}>\f$ values (for each halo in \c list_halos)
        \param[in,out]  list_contrast     Vector of pairs of \f$<J_{\rm halo}(\alpha_{\rm int})/J_{\rm Gal.bkg}(\alpha_{\rm int}),{\rm index}>\f$ values (for each halo in \c list_halos)
        \param[in,out]  list_boost        Vector of pairs of \f$<Boost,{\rm index}>\f$ values if aplicable (for each halo in \c list_halos)
        \param[in]  contrast_thresh       Sort and print (if \c is_print==true) on screen halos for which \f$J_{\rm halo}/J_{\rm  Gal.bkd}> {\rm contrast\_thresh}\f$
        \param[in]  is_print              Switch to print {default] on screen
   \n\n

*/
