#ifndef _CLUMPY_HEALPIXFITS_H_
#define _CLUMPY_HEALPIXFITS_H_

// HEALPIX includes
#include <healpix_map.h>
#include <powspec.h>
#include <healpix_map_fitsio.h>

// C++ std libraries
using namespace std;
#include <string>

extern Healpix_Ordering_Scheme gSIM_HEALPIX_SCHEME;  //!< Healpix map ordering scheme, either RING or NEST.

/*! \brief Adds \f$J_{sm}\f$ (and \f$J_{<\rm subs>} + J_{<\rm cross-prod>}\f$ optional) pixel by pixel to a skymap.
\details The calculation is optimised: it evaluates only pixels that outshine \f$\epsilon\cdot J_{bkd}\f$.
This function outputs an updated array \c j_map[FOV] that includes the added halo
(\f$[M_\odot^2\,\,kpc^{-5}]\f$ annihil., \f$[M_\odot\,\,kpc^{-2}]\f$ decay)
*
*/
void          add_halo_in_map(double par_tot[11],           //!< [in] [0-6]: \f$\rho_{tot}^{halo}\f$: norm \f$[M_\odot\,\,kpc^{-3}]\f$ + \f$r_{s}~[kpc]\f$ + shape #(1,2,3) + \c card_profile (\c gENUM_PROFILE in \c params.h) + \f$R_{max}~[kpc]\f$ <br> [7-10]:    \f$l_{HC},\psi_{HC},\theta_{HC}\f$: distance, longitude, and latitude to halo centre [kpc,rad,rad], redshift
                              double const &eps,            //!< [in] \f$\epsilon\f$: relative precision sought for integrations
                              vector<double> &j_map,        //!< [in/out] 2D skymap updated with J values for the added halo
                              double const j_bkd,           //!< [in] Value of continuum background towards halo direction (e.g. \f$J_{Gal}\f$...)
                              rangeset<int> &rs_pix_fov,    //!< [in] Rangeset containing HEALPix pixel numbers
                              bool is_subs = false,         //!< [in] Add also contribution from \f$J_{<\rm subs>}^{halo}\f$ if \c true
                              double const &mtot = 0.,      //!< [in] Total mass of halo \f$[M_\odot]\f$
                              double const &f_dm = 0.,      //!< [in] Fraction of the halo mass in subs
                              double par_dpdv[10] = NULL,   //!< [in] \f$d{\cal P}/dV_{\rm halo}\f$ and \f$l_{HC},\psi_{HC},\theta_{HC}\f$ DM halo related parameters
                              double par_subs[25] = NULL,   //!< [in] \f$\rho_{\rm cl}(r)\f$, \f$d{\cal P}/dM\f$, \f$d{\cal P}/dc\f$, and \f$d{\cal P}/dV\f$ description for subhalos
                              double ntot_subs = 0,         //!< [in] Total number of subs in halo
                              int is_sz = 0                 //!< [in] Is SZ map?
                             );

void          add_szhalo_in_map(double par_tot[36], double const &eps, double *j_map, double const j_bkd,
                                rangeset<int> &rs_pix_fov);
void          do_safegridextension(const double &dtheta_orth, const double &dtheta, const double &extend_dist, string &grid_mode, double &dtheta_orth_extended, double &dtheta_extended);

void          fits_append_fluxextension(string &filename);
int           fits_read_nextensions(fitshandle &fh_file);
int           fits_read_ncolumns(fitshandle &fh_file , const int i_extension, int &n_side, Healpix_Ordering_Scheme &scheme, int &n_pix);
void          fits_read_fieldinfo(fitshandle &fh_file , const int i_extension, const int i_field, string &extname, string &fieldname, string &phys_unit);
void          fits_read_clumpymaps(fitshandle &fh_file , const int i_extension, const int i_field, string &extname, string &fieldname, string &phys_unit, int &n_pix_fov_read, vector<int> &pixnums_fov_read, vector<double> &data_fov_read);
void          fits_set_key_from_inputparams(fitshandle &fh_file, const string &keyname, const string &value, const string &type, const  string &unit, const string &comment);
void          fits_printerror(const int status);
void          fits_print_info(string &filename);
void          fits_to_ascii(string &filename, int &i_extension);
void          fits_to_simplefits(string &filename, int &i_extension, int &i_field);
void          fits_write_clumpymaps(fitshandle &fh_file, vector<int> &pixnums_fov, vector<double> &data1);
void          fits_write_clumpymaps(fitshandle &fh_file, vector<int> &pixnums_fov, vector<double> &data1, vector<double> &data2);
void          fits_write_clumpymaps(fitshandle &fh_file, vector<int> &pixnums_fov, vector<double> &data1, vector<double> &data2, vector<double> &data3);
void          fits_write_clumpymaps(fitshandle &fh_file, vector<int> &pixnums_fov, vector<double> &data1, vector<double> &data2, vector<double> &data3, vector<double> &data4);
void          fits_write_clumpymaps(fitshandle &fh_file, vector<int> &pixnums_fov, vector<double> &data1, vector<double> &data2, vector<double> &data3, vector<double> &data4, vector<double> &data5);
void          fits_write_clumpymaps(fitshandle &fh_file, vector<int> &pixnums_fov, vector<double> &data1, vector<double> &data2, vector<double> &data3, vector<double> &data4, vector<double> &data5, vector<double> &data6, int nmaps = 6);
void          fits_write_fluxmaps(vector<int> &arr_pix_fov, vector<double> &j, fitshandle &fh_file);
void          fits_write_fluxmaps(vector<int> &arr_pix_fov, vector<double> &j, vector<double> &j_smoothed, int smooth_gamma_or_nu_or_both, fitshandle &fh_file);
void          fits_write_fluxmaps(vector<int> arr_pix_fov, vector<double> &j, vector<double> &j_gamma_smoothed, vector<double> &j_nu_smoothed, int smooth_gamma_or_nu_or_both, fitshandle &fh_file);
vector<string>fits_read_params2string(const string &filename, const int i_ext);
void          fits_write_key_from_inputparams(fitshandle &fh_file, const string &keyname, const string &value, const string &type, const  string &unit, const string &comment);
void          fits_params2reproduction(string &filename, int &i_extension);
void          fits_printerror(int status);
void          hp_get_powspec(Healpix_Map<double> &map, const string &weights_dir, PowSpec &spectrum);
double        hp_nside2resol(const int n_side);
int           hp_resol2nside(const double alphaint);
int           hp_ring_above(const double z, const int n_side);
void          hp_set_resolution(int &n_pix, double &delta_omega, int &is_gamma_or_neutrino_or_both, bool is_sz = false);
rangeset<int> hp_set_pix_fov(const string &grid_mode, const double &psi, const double &theta, const double &dtheta_orth, const double &dtheta, const int &n_side, const Healpix_Ordering_Scheme &hp_scheme);
void          hp_smooth_map(Healpix_Map<double> &map, double &fwhm, string const &weights_dir);
void          hp_smooth_map_and_get_powspec(Healpix_Map<double> &map, double &fwhm, string const &weights_dir, PowSpec &spectrum);
void          hp_smooth_or_powspec(Healpix_Map<double> &map, double &fwhm, string const &weights_dir, bool is_get_powspec, PowSpec &spectrum);

/*! \brief      Optimise the calculation of any J-factor term (see above) on a 2D skymap if many pixels required.
 */
void          j_interpolation(double *j_interp,           //!< [out] 2D skymap of interpolated j values
                              rangeset<int> &rs_pix_fov,  //!< [in]
                              string &grid_mode,          //!< [in]
                              const double &psi,          //!< [in]
                              const double &theta,        //!< [in]
                              const double &dtheta_orth,  //!< [in]
                              const double &dtheta,       //!< [in]
                              int switch_j,               //!< [in] Switch to select which J to interpolate <br> - \f$ 0 \Rightarrow J_{\rm smooth}\f$ <br> - \f$ 1 \Rightarrow J_{<\rm subs>}\f$ <br> - \f$ 2 \Rightarrow J_{<\rm cross-prod>}\f$
                              double const &mtot,         //!< [in] Total mass of halo \f$[M_\odot]\f$
                              double par_tot[10],         //!< [in] \f$\rho_{tot}^{halo}\f$ parameters
                              double const &eps,          //!< [in] \f$\epsilon\f$: relative precision sought for integrations
                              double const &f_dm,         //!< [in] Fraction of the halo mass in subs
                              double par_dpdv[10],        //!< [in] \f$d{\cal P}/dV_{\rm halo}\f$ and \f$l_{HC},\psi_{HC},\theta_{HC}\f$ DM halo related parameters
                              double par_subs[25],        //!< [in] \f$\rho_{\rm cl}(r)\f$, \f$d{\cal P}/dM\f$, \f$d{\cal P}/dc\f$, and \f$d{\cal P}/dV\f$ description for subhalos
                              double ntot_subs = 0,       //!< [in]  Ntot
                              int n_mass = 0,             //!< [in] Number of mass ranges (used if \c switch_j=1 or \c switch_j=2)
                              double *l_crit = NULL,      //!< [in] \f$l_{\rm crit}[n_{mass}]\f$: array of critical distances [kpc]
                              double *m1 = NULL,          //!< [in] \f$m_1[n_{mass}]\f$: array of lower value for the subs mass range \f$[M_\odot]\f$
                              double *m2 = NULL           //!< [in] \f$m_2[n_{mass}]\f$: array of upper value for the subs mass range \f$[M_\odot]\f$
                             );

double        hp_log_interp2D(fix_arr<double, 4> &base_values, fix_arr<double, 4> &wgt);
double        hp_angular_dist(const vec3_t<double> vec3_a, const vec3_t<double> vec3_b);

#endif

/*! \file healpix_fits.h
  \brief HEALPix and fits-related function (CLUMPY skymaps, smoothing, power spectrum)
  */
