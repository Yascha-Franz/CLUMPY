#ifndef _CLUMPY_EXTRAGAL_H_
#define _CLUMPY_EXTRAGAL_H_


// C++ std libraries
#include <string>
#include <vector>
using namespace std;

void   analyse_intensity_mean(double e1_gev, double e2_gev, const int n_e);     //!<
void   analyse_distances(const vector<double> &z_vec);                          //!<
void   analyse_intensitymultiplier(const vector<double> &z_vec,
                                   const vector<double> &M_vec);                //!<
void   analyse_ebl(const vector<double> &e_vec, const vector<double> &z_vec,
                   const int card_ebl);                                         //!<
void   analyse_intensity_mass_z_decades(const vector<double> &z_vec,
                                        const vector<double> &M_vec,
                                        const double &e_gev);                   //!<
void   analyse_massfunction(const vector<double> &z_vec,
                            const vector<double> &M_vec, const int card_mf);    //!<

void   d_intensitymultiplier_dlnMh(double &lnM, double par_subs[25],
                                   double &res);   //!<
double dPhidOmega(double  &e_gev_min, double &e_gev_max,  double par[37]);      //!<
void   dPhidOmegadE(double  &e_gev, double par[38], double &res);               //!<
void   dPhidOmegadEdlnMh(double &lnMh, double par[37], double &res);            //!<
void   dPhidOmegadEdz(double &z, double par[35], double &res);                  //!<
void   dPhidOmegadEdlnMhdz(double &lnMh, double par[35], double &res);          //!<

void   extragal_set_par(double const &Mdelta, double const &z, double par[26]); //!<

void   init_extragal(const vector<double> &z_vec, const vector<double> &M_vec,  //!<
                     const int card_mf, const int card_window);                 //!<
void   init_extragal_manipulatedHMF(const vector<double> &z_vec,
                                    const vector<double> &M_vec,
                                    const int card_mf, const int card_window);  //!<
void   init_gCOSMO_MH_GRID(double &Mmin, const double &Mmax);                   //!<
void   init_gCOSMO_Z_GRID(const double &zmin, const double &zmax, bool is_log); //!<
#endif

/*! \file extragal.h
  \brief Compute extragalactic gamma-ray or neutrino emission from DM annihilation/decay

*/
