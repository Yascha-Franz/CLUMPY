#ifndef _CLUMPY_STAT_H_
#define _CLUMPY_STAT_H_

void   find_lcritgal_los(double const &nsubs_m1m2, double par_dpdv[10], double const &psi_los,
                         double const &theta_los, double const &lmin_start, double const &lmax,
                         double par_subs[25], double &m1, double &m2, double const &user_rse,
                         double const &tol, double const &jgal_bkd, double &l_crit, int iter = 0);//!< Finds recursively the critical distance below which Galactic clumps must be drawn.
double find_mthresh_fov(double par_tot[10], double const &frac, double par_dpdv[10],
                        double const &fov_deg, double const &l1,
                        double const &l2, double par_subs[25], double &mmin_subs,
                        double &mmax_subs, double const &user_rse, double const &tol,
                        double const &jgal_bkd); //!< Finds, for a given host halo (which is not the Galactic halo), the threshold mass above which the subclumps in the host halo have to be drawn.
void   find_mthresh_los(double const &ntot_subs, double par_dpdv[10], double const &psi_los,
                        double const &theta_los, double const &l1, double const &l2, double par_subs[25],
                        double &mmin_subs, double &mmax_subs, double const &user_rse,
                        double const &tol, double const &jhost_bkd, double const &jgal_bkd,
                        double &m_thresh, int iter = 0); //!< Finds recursively the threshold mass above which subclumps in a host halo (not the Gal. halo) must be drawn.

void   integrand_lum_variance(double &m, double par_subsvar[25], double &res); //!< Integrand for the calculation of the variance of the luminosity distribution.
void   integrand_mass_variance(double &m, double par_dpdmvar[3], double &res);//!< Integrand for the calculation of the variance of the mass distribution.

double mean1cl_jn(double par_dpdv[10], double const &psi_los, double const &theta_los, double const &l1, double const &l2, double par_subs[25], double &m1, double &m2, int n_pow_j, bool is_normalize_dpdm = true, bool is_verbose = false);//!< Returns the means J-factor of the clumps, given the clumps mass and spatial distributions.

double mean1cl_ln(double par_dpdv[10], double const &psi_los, double const &theta_los, double const &l1, double const &l2, double const &eps, int n_pow_l = 1); //!< Returns the mean distance of the clumps, given the clumps spatial distributions.
double mean1cl_lumn_fullhalo(double par_subs[25], double &m1, double &m2, int n_pow_lum, bool is_normalize_dpdm = true);

double mean1cl_lumn_r(double par_subs[25], double &m1, double &m2, int n_pow_lum, bool is_normalize_dpdm = true);//!< Returns the mean luminosity of the clumps, given the clumps mass, spatial, and concentration distributions.
double mean1cl_mass(double par_dpdm[2], double &m1, double &m2, double const &eps);//!< Returns the mean mass of the clumps, given the clumps mass distributions.
double mean1cl_lumn_r_m(double par_subs[25], int n_pow_lum); //!< Same as mean1cl_lumn_r, but for a fixed mass

double var1cl_j(double par_dpdv[10], double const &psi_los, double const &theta_los,  double const &l1, double const &l2, double par_subs[25], double &m1, double &m2);//!< Returns the variance of the J-factor, given the clumps mass, spatial, and concentration distributions.

//double var1cl_l(double par_dpdv[10], double const &psi_los, double const &theta_los, double const &l1, double const &l2, double const &eps);//!< Returns the variance of the distance of the clumps, given the clumps spatial distributions.
double var1cl_lum(double par_subs[25], double &m1, double &m2);//!< Returns the variance of the luminosity of the clumps, given the clump smass, spatial, and concentration distributions.
double var1cl_mass(double par_dpdm[2], double &m1, double &m2, double const &eps);//!< Returns the variance of the luminosity of the clumps, given the clump mass distributions.

#endif

/*! \file stat.h
  \brief Statistical functions: mean and variance of several quantities.

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><A href="#stat_intro">I. Motivation </A></b>\n
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><A href="#mean">II. Averaging </A></b>\n
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><A href="#variance">III. Variance </A></b>\n
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><A href="#critical">IV. Critical distance or mass for drawing clumps </A></b>\n


 <CENTER>________________________________________</CENTER>\n

<b><A NAME="stat_intro"> I. Motivation </A> </b>\n

Given the probability density functions describing the clumps, several basic statistical operations are coded in this library. These include the
mean and the variance of several clump quantities, such as their mass, distance,
intrinsic luminosity or flux. The statistical variables considered are the
mass, the position, and the concentration (which depends on the mass).

A key quantity used below is the luminosity \f${\cal L}(M,c)\f$.
It corresponds to the point-like approximation for a given clump of mass \f$M\f$ and concentration
\f$c\f$. A complication comes from the fact that any halo can host sub-halos (that may contain
sub-sub-halos, etc.). If several levels of substructures are enabled (see \c gDM_SUBS_NUMBEROFLEVELS
in \c params.h), many parameters must then be passed to all functions involving \f${\cal L}(M,c)\f$,
in order to take into account the multi-level contributions to the signal (note that it matters only
for annihilating DM). Moreover, not only do we have a mass distribution of the halos, but a possible
concentration distribution for any given mass. This means that the average or variance can be be
taken over these two distributions.

The functions in this library  (as for clumps.h) mostly rely on the following parameters:
   - <i>For total DM density in a halo</i>
      \param[in]  par_tot[0-6]     \f$\rho_{tot}\f$: norm \f$[M_\odot\,\,kpc^{-3}]\f$ + \f$r_s~[kpc]\f$ + shape #(1,2,3) + \c card_profile (\c gENUM_PROFILE in \c params.h) + \f$R_{max}~[kpc]\f$
      \param[in]  par_tot[7-9]     \f$l_{HC},\psi_{HC},\theta_{HC}\f$: distance, longitude, and latitude to halo centre [kpc,rad,rad]
   \n\n
   - <i>For spatial distribution of substructures in a halo</i>
      \param[in]  par_dpdv[0-6]    \f$d{\cal P}/dV\f$: PDF norm \f$[kpc^{-3}]\f$ + \f$r_s~[kpc]\f$ + shape #(1,2,3) + \c card_profile (\c gENUM_PROFILE in \c params.h) + \f$R_{max}~[kpc]\f$
      \param[in]  par_dpdv[7-9]    \f$l_{HC},\psi_{HC},\theta_{HC}\f$: distance, longitude, and latitude to halo centre [kpc,rad,rad]
   \n\n
   - <i>For sub-structures at all levels (withing a given halo)</i>
    \param[in]  par_subs[0-4]     \f$\rho_{\rm cl}(r)\f$: shape #(1,2,3) + \c card_profile (\c gENUM_PROFILE in \c params.h) + \f$R_{max}\f$ [kpc]
    \param[in]  par_subs[5]       \f$\epsilon\f$ - relative precision sought for \f${\cal L}\f$ calculations
    \param[in]  par_subs[6]       \f$z\f$ - redshift of the halo
    \param[in]  par_subs[7]       \f$M_{\rm vir}\f$ - mass of \f$\rho_{cl}(r)\f$
    \param[in]  par_subs[8-9]     \f$d{\cal P}/dM\f$ PDF norm \f$[M_\odot^{-1}]\f$ + slope \f$\alpha_M\f$
    \param[in]  par_subs[10-11]   \f$d{\cal P}/dc\f$:  \c gENUM_CDELTAMDELTA (cdelta-mdelta) + \f$\langle c_{\rm vir}\rangle\f$
    \param[in]  par_subs[12-13]   \f$d{\cal P}/dc\f$: \f$\sigma_{\log c_{\rm vir}}\f$ (if \c gENUM_CDELTA_DIST=kLOGNORM) + \c card_dist \c gENUM_CDELTA_DIST
    \param[in]  par_subs[14]      \f$f_{\rm DM}\f$ - mass fraction of substructures in \f$\rho_{\rm cl}\f$
    \param[in]  par_subs[15]      \f$N_{\rm level}\f$ - umber of levels of sub-sub...halos (1=no substructures)
    \param[in]  par_subs[16-22]   \f$d{\cal P}/dV\f$: PDF norm \f$[kpc^{-3}]\f$ + \f$r_s~[kpc]\f$ + shape #(1,2,3) + \c card_profile (\c gENUM_PROFILE in \c params.h) + \f$R_{max}~[kpc]\f$
    \param[in]  par_subs[23]      ratio \f$r_{s,\,d{\cal P}/dV}/r_{s,\,host}\f$: Constant value used to update par_subs[17]
    \param[in]  par_subs[24]      \f$R_{host}\f$: Place in a host halo where clumps are residing (for distant-dependent concentrations)


\n


 <CENTER>________________________________________</CENTER>\n

<b><A NAME="mean"> \anchor mean II. Averaging </A> </b>\n

   - \c mean1cl_mass(): \f$ \displaystyle  \langle M \rangle =\int_{M_{\rm min}}^{M_{\rm max}} M \frac{d{\cal P}_M}{dM} dM\;.\f$
        \param[in]  par_dpdm[0]       UNUSED
        \param[in]  par_dpdm[1]       \f$d{\cal P}/dM\f$ slope \f$\alpha_M\f$
        \param[in]  m1,m2             Minimal/Maximal mass on which the mean is calculated \f$[M_\odot]\f$
        \param[in]  eps               Relative precision \f$\epsilon\f$ sought for the integration
   \n\n\n\n
   - \c mean1cl_lumn_r_m():
     \f$ \displaystyle
     \langle {\cal L}^i \rangle_{c} = \int_{c_{\rm min}(M)}^{c_{\rm max}(M)}{\cal L}^i(M,c(M)) \frac{d{\cal P}_c}{dc}\, dc\;\;\f$
     (with \f$i=1\f$ and \f$i=2\f$ respectively)\n
     N.B.: \f$\langle{\cal L}^i \rangle_{c} = {\cal L}^i(M,\langle c_{\rm vir}\rangle)\f$ if  \c gENUM\_CVIR\_DIST=kDIRAC.
        \param[in]  par_subs[0-23]    \f$\rho_{\rm cl}(r)\f$, \f$d{\cal P}/dM\f$, \f$d{\cal P}/dc\f$, and \f$d{\cal P}/dV\f$ description for subhalos
   \n\n\n\n
   - \c mean1cl_lumn_r():
     \f$ \displaystyle
     \langle {\cal L}^i \rangle_{M,c} =
      \int_{M_{\rm min}}^{M_{\rm max}} \frac{d{\cal P}_M}{dM} \times \langle {\cal L}^i \rangle_{c} \,dM\;\;\f$
       (with \f$i=1\f$ and \f$i=2\f$ respectively).
        \param[in]  par_subs[0-23]    \f$\rho_{\rm cl}(r)\f$, \f$d{\cal P}/dM\f$, \f$d{\cal P}/dc\f$, and \f$d{\cal P}/dV\f$ description for subhalos
        \param[in]  m1,m2             Minimal/Maximal mass on which calculation performed \f$[M_\odot]\f$
   \n\n\n\n
   - \c mean1cl_ln(): \f$\displaystyle
     \langle l^n \rangle = \int_{0}^{2\pi} \int_0^{\alpha_{\rm int}} \sin\alpha\int_{l_{\rm min}}^{l_{\rm max}}
        \,l^{n+2} \,\,\,\frac{d{\cal P}_V}{dV}(l,\alpha,\beta | \psi,\theta) \,dl \,d\alpha \,d\beta \,.
     \f$\n
     This calculates the average of distance (or more generally \f$l^n\f$) of a clump along a line of sight
     \f$(\psi,\theta)\f$ where the \f$l^2\f$ comes for the volume element in spherical coordinates.
        \param[in]  par_dpdv[0-9]     \f$d{\cal P}/dV_{\rm Gal.}\f$ and \f$l_{GC},\psi_{GC},\theta_{GC}\f$ Galaxy-related parameters
        \param[in]  psi_los,theta_los \f$\psi_{los},\theta_{los}\f$ - longitude and latitude of the line of sight [rad]
        \param[in]  l1,l2             Lower/Upper value for the l.o.s. integration boundary [kpc]
        \param[in]  eps               \f$\epsilon\f$ - relative precision sought for integrations
        \param[in]  n_pow_l           \f$n\f$ - power of \f$ <l^n>\f$ to calculate (\f$\!n=1,\,2,\,-2,\,-4\f$)
   \n\n\n\n
   - \c mean1cl_jn(): \f$\displaystyle
     \langle J _{1cl}\rangle = \langle{\cal L} \rangle_{M,c} \left< \frac{1}{l^2}\right>\,.
    \f$\n
    This is the average J-factor of a clump in the point like approximation (the J-factor of a single clump
    is \f${\cal L}(M)/l^2\f$), assuming the mass and spatial distributions of the clumps to be independent.
        \param[in]  par_dpdv[0-9]     \f$d{\cal P}/dV_{\rm Gal.}\f$ and \f$l_{GC},\psi_{GC},\theta_{GC}\f$ Galaxy-related parameters
        \param[in]  psi_los,theta_los \f$\psi_{los},\theta_{los}\f$ - longitude and latitude of the line of sight [rad]
        \param[in]  l1,l2             Lower/Upper value for the l.o.s. integration boundary [kpc]
        \param[in]  par_subs[0-23]    \f$\rho_{\rm cl}(r)\f$, \f$d{\cal P}/dM\f$, \f$d{\cal P}/dc\f$, and \f$d{\cal P}/dV\f$ description for subhalos
        \param[in]  m1,m2             Minimal/Maximal mass on which the mean is calculated \f$[M_\odot]\f$


\n


<CENTER>________________________________________</CENTER>\n

<b><A NAME="variance"> \anchor var III. Variance </A></b>\n

It is useful to estimate the variance of these quantities. In particular, the variance of the
flux allows to test the validity of an averaged description and is a corner stone of the code. We compute all the variances for 1 clump using the formula
  \f[
  \sigma^2_X= \left<X^2\right> - \left<X\right>^2\,,
  \f]
where the second term is readily obtained from the averaging functions above.
The first term must be evaluated in a similar fashion, integrating over the
distribution functions:
   - \c var1cl_mass() and integrand_mass_variance():
       \f$\displaystyle
       \sigma^2_M=\int_{M_{\rm min}}^{M_{\rm max}} M^2 \frac{d{\cal P}_M}{dM} dM - \left<M\right>^2\;.
       \f$
        \param[in]  par_dpdm[0]       UNUSED
        \param[in]  par_dpdm[1]       \f$d{\cal P}/dM\f$ slope \f$\alpha_M\f$
        \param[in]  m1,m2             Minimal/Maximal mass on which the mean is calculated \f$[M_\odot]\f$
        \param[in]  eps               Relative precision sought for the integration
    \n\n\n\n
   - \c var1cl_lum() and integrand_lum_variance():
       \f$\displaystyle
       \sigma^2_{\cal L}=\int_{M_{\rm min}}^{M_{\rm max}} \frac{d{\cal P}_M}{dM} \int_{c_{\rm min}(M)}^{c_{\rm max}(M)}{\cal L}^2(M, c(M)) \frac{d{\cal P}_c}{dc}\, dc\, dM - \left<{\cal L}_{M,c}\right>^2\;.
       \f$
        \param[in]  par_subs[0-23]    \f$\rho_{\rm cl}(r)\f$, \f$d{\cal P}/dM\f$, \f$d{\cal P}/dc\f$, and \f$d{\cal P}/dV\f$ description for subhalos
        \param[in]  m1,m2             Minimal/Maximal mass on which the mean is calculated \f$[M_\odot]\f$
   \n\n\n\n
   - \c var1cl_l():
       \f$\displaystyle
       \sigma^2_l=\int_{0}^{2\pi} \int_0^{\alpha_{\rm int}} \sin\alpha\int_{l_{\rm min}}^{l_{\rm max}}
           l^4 \frac{d{\cal P}_V}{dV}(l,\alpha,\beta | \psi,\theta) \,dl \,d\alpha \,d\beta \,\,- \,\,\left<l\right>^2\,.
       \f$
        \param[in]  par_dpdv[0-9]     \f$d{\cal P}/dV_{\rm Gal.}\f$ and \f$l_{GC},\psi_{GC},\theta_{GC}\f$ Galaxy-related parameters
        \param[in]  psi_los,theta_los \f$\psi_{los},\theta_{los}\f$ - longitude and latitude of the line of sight [rad]
        \param[in]  l1,l2             Lower/Upper value for the l.o.s. integration boundary [kpc]
        \param[in]  eps               \f$\epsilon\f$ - relative precision sought for integrations
   \n\n\n\n
   - \c var1cl_j():
       \f$\displaystyle
       \sigma^2_{J_{1cl}} = \left<{\cal L}^2\right>_{M,c}\left<\frac{1}{l^4}\right>
                            - \left<J_{1cl}\right>^2\;.
       \f$\n
       The variance on the flux of \f$N_{cl}\f$ clumps - which is the key quantity to estimate the
       validity of an average description for the total flux from the clumps - is simply given by
       \f$ \displaystyle
       \sigma^2_{J_{Ncl}} = N_{cl} \times \sigma^2_{J_{1cl}}.
       \f$
        \param[in]  par_dpdv[0-9]     \f$d{\cal P}/dV_{\rm Gal.}\f$ and \f$l_{GC},\psi_{GC},\theta_{GC}\f$ Galaxy-related parameters
        \param[in]  psi_los,theta_los \f$\psi_{los},\theta_{los}\f$ - longitude and latitude of the line of sight [rad]
        \param[in]  l1,l2             Lower/Upper value for the l.o.s. integration boundary [kpc]
        \param[in]  par_subs[0-23]    \f$\rho_{\rm cl}(r)\f$, \f$d{\cal P}/dM\f$, \f$d{\cal P}/dc\f$, and \f$d{\cal P}/dV\f$ description for subhalos
        \param[in]  m1,m2             Minimal/Maximal mass on which the mean is calculated \f$[M_\odot]\f$


\n


 <CENTER>________________________________________</CENTER>\n

<b><A NAME="critical"> \anchor critical IV. Critical distance (or mass) for drawing clumps </A> </b>\n

   - \c find_lcritgal_los() recursively finds the critical distance below which Galactic clumps need to be drawn,
        for a given mass interval and l.o.s. direction.
      \param[in]  nsubs_m1m2        \f$N_{[m_{1}-m_{2}]}^{sub}\f$ - number of clumps in Galaxy in a given mass range
      \param[in]  par_dpdv[0-9]     \f$d{\cal P}/dV_{\rm Gal.}\f$ and \f$l_{GC},\psi_{GC},\theta_{GC}\f$ Galaxy-related parameters
      \param[in]  psi_los,theta_los \f$\psi_{los},\theta_{los}\f$ - longitude and latitude of the line of sight [rad]
      \param[in]  lmin_start        First \f$l_{crit}\f$ value tested [kpc]
      \param[in]  lmax              Upper value of the l range [kpc]
      \param[in]  par_subs[0-22]    \f$\rho_{\rm cl}(r)\f$, \f$d{\cal P}/dM\f$, \f$d{\cal P}/dc\f$, and \f$d{\cal P}/dV\f$ description for subhalos
      \param[in]  m1,m2             Lower/Upper value for mass range of subs considered \f$[M_\odot]\f$
      \param[in]  user_rse          Targetted RSE [%]
      \param[in]  tol               Tolerance for matching \c user_rse
      \param[in]  jgal_bkd          Galaxy background in l.o.s.: \f$[M_\odot^2\,\,kpc^{-3}]\f$ annihil., \f$[M_\odot\,\,kpc^{-2}]\f$ decay
      \param[out] l_crit            Critical distance to find [kpc]
      \param[out] iter              Number of recursive calls [must be 0 for first call]

     The number of DM clumps in the Galaxy (with masses ranging from \f$M_{\rm
     min}=10^{-6}\f$ to \f$M_{\rm max}=10^{10}\;M_\odot\f$) amounts to \f$\sim 10^{14}\f$.
     This is too large a number to actually draw all of them. The smallest clumps are so
     numerous that they can be seen as a continuum and described by an average
     description. Conversely, the largest clumps are very rare and must be statistically
     drawn and computed independently. One way to quantify that effect is to calculate for
     each mass decade the distance below which the average description fails. This is
     readily obtained from the variance on the flux of N clumps described above. We
     define the "relative standard error" on the flux of N clumps as
     \f[
     RSE = \frac{\sqrt{\sigma_{\rm Ncl}^2}}{N_{\rm cl}\langle J_{\rm 1cl}\rangle + J_{\rm smooth}}\;.
     \f]
     Note that we need to include the smooth flux in this estimation as what matters in
     the deviation of the "drawn" flux with respect to the total flux measured (i.e.
     smooth and clumpy components). This quantity can be computed for any integration
     angle, DM profile, direction in the sky, mass decade...
     \n\n
     The result is plotted as a function of the distance lower bound of the l.o.s
     integration \f$l_{\rm min}\f$ (see \c integr_los.h) in the graph below.
       \image html DocImages/RSE.jpg
     For any direction in the sky, the RSE decreases with \f$l_{\rm min}\f$. For a user
     specified tolerated error on the flux (read on the y-axis), one can read from the
     graph the corresponding \f$l_{\rm min}\f$, below which the average description fails
     and clumps must be drawn. In practice, \f$l_{\rm min}\f$ is found by dichotomy by the
     function \c find_lcritgal_los() and is only used in \c gal_j2D().
     \n\n\n\n
   - <i>Critical mass</i>: we can now reverse the argument and ask, for a given distance integration interval
     \f$[l_{\rm min} - l_{\rm max}]\f$, what is the threshold mass above which clumps are
     not numerous enough to be treated with the average description. Similarly to the
     critical distance, the function \c find_mthresh_los() determines this threshold mass by
     dichotomy. The function \c find_mthresh_fov() calls \c find_mthresh_los() for all directions
     in the field of view in order to find the minimum of the threshold mass for the
     selected FOV. This is used in \c halo_j2D() which generates a skymap of the continuum
     and substructures in any halo.
      - \c find_mthresh_los() finds recursively the threshold mass above which sub-halos need to be drawn,
        for a given distance interval and l.o.s. direction in a host halo.
          \param[in]  ntot_subs         \f$N_{tot}^{sub}\f$ - total number of clumps in the host halo
          \param[in]  par_dpdv[0-9]     \f$d{\cal P}/dV_{\rm Host}\f$ and \f$l_{HC},\psi_{HC},\theta_{HC}\f$ DM host-related parameters
          \param[in]  psi_los,theta_los \f$\psi_{los},\theta_{los}\f$ - longitude and latitude of the line of sight [rad]
          \param[in]  l1,l2              Lower/Upper value for the l.o.s. integration boundary [kpc]
          \param[in]  par_subs[0-22]    \f$\rho_{\rm cl}(r)\f$, \f$d{\cal P}/dM\f$, \f$d{\cal P}/dc\f$, and \f$d{\cal P}/dV\f$ description for subhalos
          \param[in]  mmin_sub,mmax_sub \f$M_{min}^{sub},M_{max}^{sub}\f$ - lower/upper mass of halos populating this halo \f$[M_\odot]\f$
          \param[in]  user_rse          Targetted RSE [%]
          \param[in]  tol               Tolerance for matching \c user_rse
          \param[in]  jhost_bkd         Host background: \f$[M_\odot^2\,\,kpc^{-3}]\f$ annihil., \f$[M_\odot\,\,kpc^{-2}]\f$ decay
          \param[in]  jgal_bkd          Galaxy background: \f$[M_\odot^2\,\,kpc^{-3}]\f$ annihil., \f$[M_\odot\,\,kpc^{-2}]\f$ decay
          \param[out] m_thresh          Threshold mass to find \f$[M_\odot]\f$
          \param[out] iter              Number of recursive calls [must be 0 for first call]
      \n\n
      - \c find_mthresh_fov() returns the minimum threshold mass for a field of view (fov) by sampling
        over 10 directions in the f.o.v, and keeping the most conservative number. It is based on
        \c find_mthresh_los(), and it can also take into account the Galactic continuum contribution
        \c jgal_bkd (in addition to \c jhost_bkd) to increase \c m_thresh.
          \param[in]  par_tot[0-9]      \f$\rho_{tot}\f$ description and \f$l_{HC},\psi_{HC},\theta_{HC}\f$
          \param[in]  frac              \f$f_{\rm DM}\f$ - fraction of the host halo mass in clumps
          \param[in]  par_dpdv[0-9]     \f$d{\cal P}/dV_{\rm Host}\f$ and \f$l_{HC},\psi_{HC},\theta_{HC}\f$ DM host-related parameters
          \param[in]  fov_deg           F.O.V region centered on host [deg]
          \param[in]  l1,l2              Lower/Upper value for the l.o.s. integration boundary [kpc]
          \param[in]  par_subs[0-22]    \f$\rho_{\rm cl}(r)\f$, \f$d{\cal P}/dM\f$, \f$d{\cal P}/dc\f$, and \f$d{\cal P}/dV\f$ description for subhalos
          \param[in]  mmin_sub,mmax_sub \f$M_{min}^{sub},M_{max}^{sub}\f$ - lower/upper mass of halos populating this halo \f$[M_\odot]\f$
          \param[in]  user_rse          Targetted RSE [%]
          \param[in]  tol               Tolerance for matching \c user_rse
          \param[in]  jgal_bkd          Galaxy background in FOV: \f$[M_\odot^2\,\,kpc^{-3}]\f$ annihil., \f$[M_\odot\,\,kpc^{-2}]\f$ decay
          \returns \f$M_{thresh}^{f.o.v.}\f$ \f$[M_\odot]\f$ - mass threshold above which clumps need to be drawn in the field of view.

*/
