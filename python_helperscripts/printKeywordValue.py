#!/usr/bin/python
# -*- coding: utf-8 -*-

###############################################################################
#    Authors:
#    This file belongs to the Clumpy code, http://lpsc.in2p3.fr/clumpy
#    For any questions, please contact moritz.huetten@desy.de
###############################################################################
#
# You need to have installed 
#   - astropy.io
# to run this script.

###############################################################################
#    import modules:
###############################################################################

import sys
import getopt
from astropy.io import fits

###############################################################################
##    main part: 
###############################################################################

def main(argv):
    
    ###########################################################################
    #  read input variables:
    
    infile   = 'empty'
    keyword  = 'empty'
    
    try:
        opts, args = getopt.getopt(sys.argv[1:],'i:k:',['infile=','keyword='])
    except getopt.GetoptError:
        print('Wrong input. The input options are:')
        print('-i or --infile for reading the input FITS file')
        print('-k or --keyword for the desired keyword to know')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            # help option
            print('The input options are:')
            print('-i or --infile for reading the input FITS file')
            print('-k or --keyword for the desired keyword to know')
            sys.exit()
        elif opt in ('-i', '--infile'):
            infile = str(arg)
        elif opt in ('-k', '--keyword'):
            keyword = str(arg)
    
    if infile == 'empty':
        print('  Please parse an input FITS file with -i file.fits or --infile file.fits')
        sys.exit()
    if keyword == 'empty':
        print('  Please parse an keyword name with -k FITS_KEY or --keyword FITS_KEY')
        sys.exit()

    ###########################################################################
    

    
    #  read header:
    hdulist_in = fits.open(infile)
    try:
        value = hdulist_in[0].header[keyword]
    except:
        try:
            value = hdulist_in[1].header[keyword]
        except:
            message = 'Keyword "' + keyword + '" not found.'
            raise IOError(message)
    
    hdulist_in.close()


    print(value)
    
if __name__ == '__main__':
     
    main(sys.argv[1:])

##    end of file    ##########################################################
############################################################################### 
