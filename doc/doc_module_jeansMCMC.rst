.. _rst_doc_module_jeansMCMC:

:math:`\tt jeansMCMC` executable
--------------------------------

Jeans analysis with GreAT MCMC engine (only if GreAT is installed, see :ref:`rst_install`).

For example, to run MCMCs analysis on :math:`\sigma_{\rm p}` data from :file:`$CLUMPY/data/data_sigmap.txt`, execute:

.. code-block:: console

    $ clumpy_jeansMCMC -r $CLUMPY/data/data_sigmap.txt output/stat_example.root output/stat_exampleAna.root 10000 8 $CLUMPY/data/params_jeans.txt 0.05

using a numeric accuracy of 5%:

.. figure:: DocImages/jeansMCMC_rD.png
   :align: center
   :figclass: align-center
   :scale: 65%


The result can be  visualised with the :option:`-s8` module of the :math:`\tt clumpy` executable (see :numref:`rst_doc_module_s`):

.. code-block:: console

   $ clumpy -s8 -D
   $ clumpy -s8 -i clumpy_params_s8.txt --gSTAT_FILES=output/stat_example.dat

.. figure:: DocImages/jeansMCMC_rD-a.png
   :align: center
   :figclass: align-center
   :scale: 54%

   MCMC results  output :program:`ROOT` figures prcessed with :option:`-s8` option