.. _rst_enumerators_nuflavours:

.. role::  raw-html(raw)
    :format: html

Neutrino flavours
--------------------------------------------------------------

Possible keyword choices for the :option:`gSIM_FLUX_FLAG_NUFLAVOUR` parameter. 

.. csv-table::
   :file: tables/gENUM_NUFLAVOUR.csv_table
   :widths: 10, 70
   :align: left

:raw-html:`<br>&nbsp;<br>`

.. seealso::
   :numref:`rst_doc_finalstates`.
   
   