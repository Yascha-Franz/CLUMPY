.. Clumpy documentation master file, adapted from healpy documentation.

.. role::  raw-html(raw)
    :format: html

.. image:: DocImages/clumpy_snapshots.png

-----------------------

:program:`CLUMPY` user documentation
=========================================================================================

**A code for γ-ray and ν signals from dark matter structures**

We hope you will enjoy using :program:`CLUMPY` whether you are:

* *an experimental astroparticle physicist* looking for *J*-factors or synthetic  2D :math:`\gamma`-ray or :math:`\nu` skymaps from   dark matter decay or annihilation, to calculate your instrumental sensitivity or to use in model/template analyses;
* *a theoretical astroparticle physicist* wishing to explore the  :math:`\gamma`-ray or :math:`\nu` flux in the Galaxy, dSphs, or galaxy clusters for your preferred particle physics model;
* *an astrophysicist* working on the DM content of dSphs and wishing to perform a Jeans analysis on your kinematic data;
* *a cosmologist* wishing to compute halo mass functions for any cosmology, redshift, and overdensity definition :math:`\Delta`.

If you want to have a quick overview whether :program:`CLUMPY` serves for your purposes, have a look at the :ref:`rst_intro` and browse the :ref:`rst_doc_modules` section or the :ref:`rst_gallery`. If you have decided to use :program:`CLUMPY`, download it from the `GitLab repository <https://gitlab.com/clumpy/CLUMPY>`__ and consult the :ref:`rst_install` section. Start using it from our :ref:`rst_quick_start` and learn more about of the various modules of :program:`CLUMPY` in the :ref:`rst_physics` section. An automatically generated PDF version of this documentation can be retrieved  :download:`here <_build/latex/clumpy_doc.pdf>`.

If you use :program:`CLUMPY`, please cite the :program:`CLUMPY` publications by `Charbonnier et al. (2012) <http://adsabs.harvard.edu/abs/2012CoPhC.183..656C/>`__,  `Bonnivard et al. (2016) <http://adsabs.harvard.edu/abs/2016CoPhC.200..336B/>`__, and  `Hütten et al. (2018) <http://adsabs.harvard.edu/abs/2018arXiv180608639H>`__.

.. toctree::
   :maxdepth: 1
   :hidden:
   :numbered:

   intro
   gallery
   public_data
   install
   quick_start
   physics
   doc_modules
   doc_modules_jeans
   parameters
   enumerators
   license
   Doxygen (for developers) <doxygen/index.html#http://>
   CLUMPY goodies <http://shop.spreadshirt.net/clumpy>
