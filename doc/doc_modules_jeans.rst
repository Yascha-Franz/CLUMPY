.. _rst_doc_modules_jeans:

clumpy_jeans* executables: options
-----------------------------------------------

There are the two separate Jeans-analysis modules:

.. toctree::
   :maxdepth: 1

   doc_module_jeansChi2
   doc_module_jeansMCMC


The output of these commands are interactive :program:`ROOT` displays (:program:`ROOT` canvas and objects can be straightforwardly saved in many formats, e.g., eps, jpeg, etc.), ASCII, :program:`ROOT`, or :program:`FITS` files.
