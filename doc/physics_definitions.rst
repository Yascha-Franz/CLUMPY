.. _rst_physics_definitions:

.. sectionauthor:: David Maurin <dmaurin@lpsc.in2p3.fr>

.. role::  raw-html(raw)
    :format: html

γ-ray and neutrino fluxes
-----------------------------------------

Definition: flux & intensity
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The :math:`\gamma`-ray or :math:`\nu` **flux** at energy :math:`E` from dark matter annihilating/decaying particles in the Universe, :math:`\mathrm{d}\Phi_{\gamma,\nu}/\mathrm{d}E`, depends on:

   - a **particle physics term** :math:`\frac{\mathrm{d}\Phi^{PP}_{\gamma,\nu}}{\mathrm{d}E}(E,z)`, related to the DM candidate properties and couplings, and which is described in :numref:`rst_physics_spectra`; :raw-html:`<br><br>`
   - an **astrophysical term**, related to the DM distribution (along the line-of-sight), and whose expression depends on the target studied (galactic vs extragalactic, single vs population of haloes), as discussed below. :raw-html:`<br>`

.. note:: In the :math:`\Lambda\mathrm{CDM}` cosmological model, structures form in a bottom-up manner: micro-haloes form first, larger ones collapse later, and this process, accompanied with merger events, lead to the global picture of clumps within clumps within clumps, etc. It is convenient to separate, for a given halo, the main distribution (called smooth halo) from the contributions of each clump, which must all be accounted for in the calculation.


The **intensity** is related to the **flux** by:

   .. math::

      I_{\gamma,\nu}(E)= \frac{\mathrm{d}\Phi{\gamma,\nu}}{\mathrm{d}E\,\mathrm{d}\Omega}

with :math:`\mathrm{d}\Omega=\mathrm{d}\beta\, \sin\alpha \,\mathrm{d}\alpha` is the elementary solid angle along the line of sight (l.o.s.) direction.

.. note::

   - :math:`{\rm d}\Phi_{\gamma,\nu}/{\rm d}E` is a **flux**, in units of :math:`{\rm cm}^{-2}{\rm ~s}^{-1} {\rm ~GeV}^{-1}`;
   - :math:`{\rm d}\Phi_{\gamma,\nu}/{\rm d}E/{\rm d}\Omega` is an **intensity**, in units of :math:`{\rm cm}^{-2}{\rm ~s}^{-1} {\rm ~GeV}^{-1} {\rm ~sr}^{-1}`.


Definition: *J*- & *D*-factors
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Formally, the astrophysical *J* (annihilation) or *D* (decay) factor is defined to be the integration of some power of the DM density :math:`\rho(\psi,\theta, l,\alpha,\beta)`, see :numref:`rst_physics_profiles`, over the solid angle :math:`\Delta \Omega=2\pi\,(1-\cos\,\alpha_{\rm int})` at coordinate :math:`(l,\alpha,\beta)` in the l.o.s. direction :math:`(\psi, \theta)`:

     .. math:: J(\psi,\theta, \Delta \Omega) &= \int_{0}^{\Delta \Omega}\int_{\rm{l.o.s}} \mathrm{d}l \, \mathrm{d}\Omega \times \,\rho^2 {\quad \rm (for~annihilation)}\\
        D(\psi,\theta, \Delta \Omega) &= \int_{0}^{\Delta \Omega}\int_{\rm{l.o.s}} \mathrm{d}l \, \mathrm{d}\Omega \times  \,\rho  {\quad \rm (for~decay)}\;.
      :label: JDdefinitions

For the *J*-factors, the user may choose between:

   - **astrophysical** input/output units:

      .. math::

         \left[J^{\rm annihil.}\right]\; (=[J]) =  {\rm M}_\odot^2{\rm ~kpc}^{-5}\,, \quad \left[J^{\rm decay}\right]\; (=[D]) =  {\rm M}_\odot{\rm ~kpc}^{-2}\,.

   - **particle physics** units:

      .. math::

         \left[J^{\rm annihil.}\right]\; (=[J]) =  {\rm GeV}^2{\rm ~cm}^{-5}\,, \quad \left[J^{\rm decay}\right]\; (=[D]) =  {\rm GeV}{\rm ~cm}^{-2}\,.

.. note::

   - The switch between astrophysical and particle physics units is indicated by the :option:`gSIM_IS_ASTRO_OR_PP_UNITS` input parameter, with :option:`gSIM_IS_ASTRO_OR_PP_UNITS = False` to have the **particle physics** units.

   - The expressions in Eq. :eq:`JDdefinitions` do not contain a factor :math:`1/4\pi` factor originating from
   
      .. math:: \int_{0}^{\Delta \Omega}\int_{\rm{l.o.s}} \frac{1}{4\pi\,\mathrm{sr}\,l^2}  \times \,\rho^{(2)}\,\mathrm{d}l  \,l^2\, \mathrm{d}\Omega\,.
   
     We follow :raw-html:`<A href="http://adsabs.harvard.edu/abs/2009A&A...496..351P" target="_blank">Pieri et al. (2009)</A>` and include it in the particle physics factor, introduced in :numref:`rst_physics_spectra`. In other works, a factor :math:`1/4\pi` or :math:`1/(4\pi\,\mathrm{sr})` can however appear in the *J*-factor (e.g., :raw-html:`<A href="http://adsabs.harvard.edu/abs/2009JCAP...01..016B" target="_blank">Bringmann et al., 2009</A>).` :raw-html:`<br>`

   - Also, we discard writing the unit :math:`1/\mathrm{sr}` of the solid angle in Eq. :eq:`JDdefinitions`, which is silently assumed to match the dimensions of our *J*-factor units. Some authors do not assume such a silent :math:`1/\mathrm{sr}`, resulting in correspondingly different *J*-factor units (for instance in :raw-html:`<A href="http://adsabs.harvard.edu/abs/2009A&A...496..351P" target="_blank">Pieri et al., 2009</A>).` See also Appendix A of :raw-html:`<A href="http://adsabs.harvard.edu/abs/2011MNRAS.418.1526C" target="_blank">Charbonnier et al. (2011)</A>.`


Galactic flux (w/ subhaloes)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Milky-Way flux is given by

   .. math::

      \frac{\mathrm{d}\Phi_{\gamma,\nu}}{\mathrm{d}E}(E,\psi,\theta,\Delta\Omega)
      =\frac{\mathrm{d}\Phi^{PP}_{\gamma,\nu}}{\mathrm{d}E}(E)\times J^{\rm gal}(\psi,\theta,\Delta \Omega) \,,

in which :math:`\mathrm{d}\Omega=\mathrm{d}\beta\, \sin\alpha\, \mathrm{d}\alpha` is the elementary solid angle around the line of sight (l.o.s.) direction :math:`\psi,\theta` (longitude and latitude in Galactic coordinates, see :numref:`rst_physics_geometry`), and :math:`\frac{\mathrm{d}\Phi^{PP}_{\gamma,\nu}}{\mathrm{d}E}(E,z)` is the elementary source spectrum described in :numref:`rst_physics_spectra`.

The signal from the galaxy comes from the main smooth halo and subhaloes, with the total averaged density :math:`\langle\rho_{\rm tot}\rangle (r) = \rho_{\rm sm}(r) + \langle \rho_{\rm subs}\rangle(r)`, where :math:`\rho_{\rm sm}(r)` is the smooth component and :math:`\langle \rho_{\rm sub}\rangle(r)` is the spherical shell average density of substructures (at each radius :math:`r`). The astrophysical contribution to the annihilation flux is thus explicitly written to be

   .. math::

      J^{\rm gal} = \int_0^{\Delta\Omega}\int_{l_{\rm min}}^{l_{\rm max}} \left( \rho_{\rm sm} + \sum_i \rho_{\rm cl}^i\right)^2 {\rm d} l \, {\rm d}\Omega\;,

where :math:`\rho_{\rm cl}^i` corresponds to the inner density of the :math:`i`-th clump contained in the volume element. Three terms arise from this equation (smooth only, substructure contribution, and cross-product):

   .. math::

      J^{\rm gal}_{\rm sm} &\equiv \int_0^{\Delta\Omega}\int_{l_{\rm min}}^{l_{\rm max}} \rho_{\rm sm}^2 {\rm d} l \, {\rm d}\Omega\;,\\
      J^{\rm gal}_{\rm subs} &\equiv \int_0^{\Delta\Omega}\int_{l_{\rm min}}^{l_{\rm max}} \left(\sum_i \rho_{\rm cl}^i\right)^2 {\rm d} l \, {\rm d}\Omega\;,\\
      J^{\rm gal}_{\rm cross-prod} &\equiv 2\int_0^{\Delta\Omega}\int_{l_{\rm min}}^{l_{\rm max}} \rho_{\rm sm}\sum_i \rho_{\rm cl}^i {\rm d} l \, {\rm d}\Omega\;.


The various options of :program:`CLUMPY` (see :numref:`rst_doc_module_g`) evaluate these terms, with several levels of substructures if desired (see :numref:`rst_subsec_average`). Actually, all the subhaloes are not resolved, so that :program:`CLUMPY` allows to calculate the average values from subhaloes (and cross-prod), or simulate realistic 2D skymaps, following the flow chart shown in :numref:`fig_flowchart`.

   .. _fig_flowchart:

   .. figure:: DocImages/flow_chart_logic2.png
      :figclass: align-center

      Flow chart of the different steps leading to the creation of a :math:`\gamma`-ray skymap.


Single halo flux
~~~~~~~~~~~~~~~~

Assuming the halo is local (:math:`z\sim 0`) or that its span is small enough (:math:`\Delta z/z\ll 1`), one may separate spectral and astrophysical contributions in the flux calculation. The flux for a single halo (galactic or extragalactic), with the elementary source spectrum :math:`\frac{{\rm d}\Phi^{PP}_{\gamma,\nu}}{{\rm d}E}(E,z)` described in :numref:`rst_physics_spectra`, reads:

   - For annihilating DM:

      .. math::

         \frac{\mathrm{d}\Phi_{\rm ann}}{\mathrm{d} E_{\gamma,\nu}}(E_{\gamma,\nu},\,\Delta\Omega)\;=
         \frac{{\rm d} \Phi^{PP~\rm annihil.}_{\gamma,\nu}(E)}{{\rm d} E}
         \times (1+z)^3\; \int_0^{\Delta \Omega} \int_{\rm l.o.s.} \,\rho_{\rm halo}^2\,{\rm d} l \,{\rm d}\Omega\,.

   - For decaying DM:

      .. math::

         \frac{{\rm d}\Phi_{\rm decay}}{{\rm d} E_{\gamma,\nu}}(E_{\gamma,\nu},\,\Delta\Omega) \;=
         \frac{{\rm d} \Phi^{PP~\rm decay}_{\gamma,\nu}(E)}{{\rm d} E}
         \times\; \int_0^{\Delta \Omega} \int_{\rm l.o.s.} \,\rho_{\rm halo}\,{\rm d} l \,{\rm d} \Omega\,.

The line-of-sight distance (l.o.s.) :math:`l` to the object now corresponds to the comoving distance, while :math:`\rho_{\rm halo}(r)` is given in comoving coordinates. These equations generalise the *J*/*D*-factors defined above. All *J*/*D*-factor and flux calculations on the Galactic level and for single haloes are implemented in the library `janalysis.h <doxygen/janalysis_8h.html>`_.


.. _rst_subsec_extragal:

Extragalactic flux
~~~~~~~~~~~~~~~~~~

The intensity of the extragalactic signal averaged over the whole sky depends on the elementary source spectrum :math:`\frac{{\rm d}\Phi^{PP}_{\gamma,\nu}}{{\rm d}E}(E,z)` (see :numref:`rst_physics_spectra`) and cosmological-related parameters (see :ref:`rst_physics_cosmo`). The formulae implemented in `extragal.h <doxygen/extragal_8h.html>`_ read:

   - **Annihilating DM**

      .. math::

         \;\left\langle\frac{{\rm d}  \Phi}{{\rm d}E_{\gamma,\nu}\,{\rm d}\Omega}\right\rangle_{\rm sky}^{\rm annihil.} \;= \;\overline{\varrho}^2_{\mathrm{DM},\,0} \times \int\limits_0^{z_\mathrm{max}} c\,{\rm d} z\, \frac{(1+z)^3}{H(z)}\left\langle\delta^2(z)\right\rangle \frac{{\rm d} \Phi^{PP~\rm annihil.}_{\gamma,\nu}(E,z)}{{\rm d} E}\,,

      with :math:`E_{\gamma,\nu}` the observed energy, :math:`{\rm d}\Omega` the elementary solid angle, :math:`c` the speed of light, and :math:`\overline{\varrho}_{\mathrm{DM},\,0}` the DM density of the Universe today, :math:`H(z)` the Hubble constant at redshift :math:`z`, and :math:`\langle\delta^2 \rangle=1 + \mathrm{Var}(\delta)` is the intensity multiplier related to the DM inhomogeneity :math:`\delta`. Indeed, density fluctuations in the  Universe, :math:`\varrho_\mathrm{DM}(\Omega,\,z)=\delta(\Omega,\,z)\times \overline{\varrho}_\mathrm{DM}(z)`,  boost the rate of DM annihilations: for smoothly distributed DM, :math:`\delta\equiv\mathrm{Var}(\delta)=0`  and :math:`\langle \delta^2 \rangle = 1`, whereas for a high density contrast, :math:`\langle\delta^2 \rangle\approx \mathrm{Var}(\delta) \gg 1`.

   - **Decaying DM** (with lifetime :math:`\tau_\mathrm{DM}`)

      .. math::

         \;\left\langle\frac{{\rm d}  \Phi}{{\rm d}E_{\gamma,\nu}\,{\rm d}\Omega}\right\rangle_{\rm sky}^{\rm decay} \;= \;\overline{\varrho}_{\mathrm{DM},\,0} \times \int\limits_0^{z_\mathrm{max}} c\,{\rm d} z\, \frac{1}{H(z)}\, \frac{{\rm d} \Phi^{PP~\rm decay}_{\gamma,\nu}(E,z)}{{\rm d} E}\,.


.. note::

   In version 3, :program:`CLUMPY` only accounts for the prompt :math:`\gamma`-ray emission, given by the above equations. The diffuse secondary emission, produced by the Inverse Compton (IC) scattering of DM-produced :math:`e^{+/-}` with CMB photons (e.g., :raw-html:`<a href="http://cdsads.u-strasbg.fr/abs/2009JCAP...07..020P" target=_blank>Profumo & Jeltema, 2009</a>`) or other radiation fields (:raw-html:`<a href="https://ui.adsabs.harvard.edu/#abs/2009NuPhB.821..399C" target=_blank>Cirelli and Panci, 2009</a>`), is not yet implemented. IC scattering may significantly increase the :math:`\gamma`-ray emission from DM annihilations/decays at energies :math:`E_\gamma \lesssim 10^{-2}\,m_{\mathrm{DM}}`.


Computing the diffuse  :math:`\gamma`-ray or :math:`\nu` intensity from various redshifts is described in the extragalactic :numref:`rst_doc_module_e`.

.. warning::

   Including various resolved objects from different redshifts is not yet included in :program:`CLUMPY`.


Angular power spectrum
~~~~~~~~~~~~~~~~~~~~~~

The intensity angular power spectrum (APS) of the signal over the full sphere :math:`C_\ell` of an intensity map :math:`I(\vartheta, \varphi)` is defined as

   .. math:: C_\ell = \frac{1}{2\ell + 1} \sum\limits_m |a_{\ell m}|^2 \;,

with :math:`a_{lm}`` the coefficients of the intensity map decomposed into spherical harmonics :math:`Y_{lm}`,

   .. math:: I(\vartheta, \varphi) = \sum\limits_{\ell= 0}^{\ell_{max}} \sum\limits_{m= -\ell}^{m= +\ell} a_{\ell m}\, Y_{\ell m}(\vartheta, \varphi).
