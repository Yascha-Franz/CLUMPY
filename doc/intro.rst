.. _rst_intro:

.. role::  raw-html(raw)
    :format: html

Introduction
============

Physics goals
--------------------------------------------------------------------------

:program:`CLUMPY` is dedicated to the calculation of :math:`\gamma`-ray or neutrino signals from  annihilating and decaying astrophysical Dark Matter. Dark matter haloes are present at many scales (sub-Galactic, Galactic,  cluster scale). The Galactic centre is the obvious location for indirect detection of dark matter annihilation. However, it is plagued by a large background of astrophysical sources. Other potentially promising targets are dSphs, dark clumps of the Galactic halo, gradients of the smooth halo (towards but slightly offset the Galactic centre), galaxy clusters, or the overall diffuse extragalactic emission.

For this reason, several modules are available in :program:`CLUMPY`, to deal with all the above situations. Each module is reached by using the appropriate flag in the command line. Specific formats for user-defined ASCII input files are also required, depending on the options used. Help for the various options and allowed parameters are designed to be self-explaining.

Specifically, :program:`CLUMPY` addresses the following physics problems of indirect dark matter annihilation/decay signals:

   :*J*-factors: Calculate astrophysical *J*- and *D*-factors from a spherical or triaxial DM distribution in the Galaxy (smooth, mean and/or drawn subhaloes) and/or for extragalactic haloes;

   :γ-ray/ν fluxes: Calculate fluxes for Galactic and/or extragalactic contributions, combining the astrophysical factor and the particle physics factor;

   :Jeans analysis: Perform a Jean analysis to reconstruct DM profiles from kinematic data (:math:`\chi^2` analysis or MCMC analysis) and to calculate the associated *J*-factors, i.e., integrating the collisionless Boltzmann equation in spherical symmetry, assuming steady-state and negligible rotational support. This relates the dynamics of a collisionless tracer population (e.g., stars in a dwarf spheroidal galaxy or galaxies in a galaxy cluster) to the underlying gravitational potential.

For a detailed description of the physics problem, go to :numref:`rst_physics`.


Ingredients
--------------------------------------------------------------------------

*J*-factor and flux calculations require many inputs from particle physics, cosmology, structure formation, DM halo distributions, etc. This is detailed in this documentation and in  the `CLUMPY publications <#release-history>`__. These inputs are handled by `parametrisations <parameters.html>`__ and their associated `keywords <enumerators.html>`__, and they describe for instance:

   :(Sub-)haloes:  Profile parametrisations (Zhao, NFW, Einasto, etc.), concentration-mass dependence (Bullock, Moliné, etc.),…

   :Extragalactic: Cosmology parameters, mass distribution (Press-Schechter, Tinker, etc.), absorption (Finke, Franceschini, etc.), …

   :Jeans analysis: Light profile (Plummer, Zhao, etc.), anisotropy profile (constant, Osipkov, etc.), …

   :Skymaps: Integration angle, smoothing (by Gaussian PSF), angular power spectrum (APS), …


.. _rst_release_history:

Release history
--------------------------------------------------------------------------

We provide below as tarballs previous :program:`CLUMPY` releases and bug fixes, and the associated publications. However, if not for specific demands, always use the last release version from the `git repository <https://gitlab.com/clumpy/CLUMPY>`__!

* **v1 (or v11.09):** `Charbonnier, Combet, and Maurin, CPC 183, 656 (2012) <http://cdsads.u-strasbg.fr/abs//2012CoPhC.183..656C>`__ -- *CLUMPY: a code for γ-ray signals from dark matter structures*.

   * *09/2011*: (v1) :download:`2011.09<DocData/CLUMPY_v2011.09.CPC.tar.gz>` (CPC release)
   * *01/2012*: (v1) :download:`2011.09_corr2<DocData/CLUMPY_v2011.09.CPC_corr2.tar.gz>` (ROOT 2D replaced by log-log interpolation)
   * *08/2014*: (v1) :download:`2011.09_corr3<DocData/CLUMPY_v2011.09.CPC_corr3.tar.gz>` (updated for clang++ and option -g7, -g8, and -h5)
   * *03/2015*: (v1) :download:`2011.09_corr4<DocData/CLUMPY_v2011.09.CPC_corr4.tar.gz>` (bug fix gMW_TOT_SHAPE_PARAMS and gal. clumps)

* **v2 or v15.06:** `Bonnivard, Hütten, Nezri, Charbonnier, Combet, and Maurin, CPC 200, 336 (2016) <http://cdsads.u-strasbg.fr/abs/2016CoPhC.200..336B>`__ -- *CLUMPY: Jeans analysis, γ-ray and ν fluxes from dark matter (sub-)structures*.

   * *06/2015*: (v2) :download:`2015.06<DocData/CLUMPY_v2015.06.CPC.tar.gz>` (CPC submission)
   * *07/2015*: (v2) :download:`2015.06_corr1<DocData/CLUMPY_v2015.06.CPC_corr1.tar.gz>` (bootstrap Jeans, :program:`HEALPix` instal., more pop. study plots)
   * *11/2015*: (v2)  :download:`2015.06_corr2<DocData/CLUMPY_v2015.06.CPC_corr2.tar.gz>` (bug fix decay lifetime, keywords for integrated flux)


* **v3 or v18.06**: `Hütten, Combet, and Maurin (2018)  <http://adsabs.harvard.edu/abs/2018arXiv180608639H>`__ -- *CLUMPY v3: γ-ray and ν signals from dark matter at all scales*.

   * *06/2018*: (v3.0.0) :download:`2018.06<DocData/CLUMPY_v2018.06.CPC.tar.gz>` (CPC submission)

  See `publication  <http://adsabs.harvard.edu/abs/2018arXiv180608639H>`__ for an overview of the new features. **Major bug fixes w.r.t. v2**:
    
  - When calculating subhalo structural parameters from a mass-concentration relation :math:`c_\Delta(m_\Delta)` in :math:`\Delta=200`, the transformation to :math:`\Delta_{\rm vir}` was incorrect, overestimating annihilation *J*-factors/luminosities and the signal from unresolved substructure by a factor :math:`\sim 3`. Fixed since v3.0.0. 
  
  - Fixed (since v3.0.0) error in neutrino mixing angle components :math:`U_{22}` and :math:`U_{3i}`.

Developers/contacts
--------------------------------------------------------------------------

For any question, or if you believe that you have a module that would be a bonus for a future :program:`CLUMPY` release, feel free to `contact us <clumpy@lpsc.in2p3.fr>`__ to discuss the matter:

   * `Céline Combet <celine.combet@lpsc.in2p3.fr>`__ (*DM distributions, clump drawing, extragalactic module*)
   * `Moritz Hütten <mhuetten@mpp.mpg.de>`__ (*HEALPix/FITS implementation, APS, I/O interfaces, extragalactic module*)
   * `David Maurin <dmaurin@lpsc.in2p3.fr>`__ (*project coordinator*)

**Past contributors**

   - Vincent Bonnivard (*Jeans analysis module in v2*)
   - Aldée Charbonnier (*wrote unreleased v0*)
   - Emmanuel Nezri (*particle physics module in v2*)
