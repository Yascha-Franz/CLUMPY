.. _rst_doc_module_h:

Haloes: :math:`\tt -h`
--------------------------------

Run on (a list of) non-Galactic haloes. 

.. note:: All default parameter files from the following run examples are generated with the :option:`-D` flag,

   .. code-block:: console

    $ clumpy -hX -D

.. _rst_halofile_format:

Format of halo definition files
++++++++++++++++++++++++++++++++++++++++++++++++++++

All submodules require the definition of the properties of one or more haloes in a separate text file. This halo definition file is called from within the main parameter file or the command line with the parameter :option:`gLIST_HALOES` (submodules :option:`-h0` to :option:`-h7`) or :option:`gLIST_HALOES_JEANS` (submodules :option:`-h8` to :option:`-h10`).

- The :option:`gLIST_HALOES` is formatted as follows. The example below is given for the Abell 2261 cluster (after CLASH, Coe et al., 2012) with arbitrary triaxiality values. Preformatted example files are also provided with the code in :download:`$CLUMPY/data/list_generic.txt <../data/list_generic.txt>` and :download:`$CLUMPY/data/list_generic_triaxial.txt <../data/list_generic_triaxial.txt>`.

  .. code-block:: console

      #*********************************************************************************************************************************************************#
      #                  [OBJECT LOCATION AND SIZE]                |             DM DISTRIBUTION (RHO_TOT)                      [TRIAXIALITY]                   #
      # Name           Type      l       b      d     z   Rdelta   |     rhos       rs     prof.   #1   #2   #3       IsTriaxial  a   b   c   rot1  rot2  rot3  #
      #  -               -     [deg]   [deg]  [kpc]   -    [kpc]   |  [Msol/kpc3]  [kpc]  [enum]    -    -    -           -       -   -   -   [deg] [deg] [deg] #
      #*********************************************************************************************************************************************************#
      
      Abell_2261       CLUSTER  55.56  31.95   -1 0.2249   3e3         7.01e5    652.   kEINASTO   0.17  0   0            0    1.47 1.22 0.74  0.    0.    0.



  .. table::
  
     ==================== ============================================ =======================================================================================================================================================================================================================================================
     **Parameter**        **Unit**                                     **Comment**                                                                                                                                                                                                                                  
     -------------------- -------------------------------------------- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     :option:`Name`       :math:`\rm -`                                Arbitrary name to identify your object                                                                                                                                                                                                                 
     :option:`Type`       :math:`\rm -`                                :keyword:`DSPH`, :keyword:`CLUSTER`, or :keyword:`GALAXY` (for external galaxies). This keyword decides which values are used to describe the halo substructure, defined in the main parameter file (see :numref:`rst_universal_substructure_props`)  
     :option:`l`          :math:`\rm deg`                              Galactic longitude of position   
     :option:`b`          :math:`\rm deg`                              Galactic latitude of position                                                                                                                                                                        
     :option:`d`          :math:`\rm kpc`                              Distance given as comoving line-of-sight distance, :math:`d_{\rm c}`                                                                                                     
     :option:`z`          :math:`\rm -`                                Distance given as redshift. Either :option:`d` or :option:`z` must be set to :keyword:`-1`                                                                                                                                         
     :option:`Rdelta`     :math:`\rm kpc`                              Outer bound of the halo, :math:`R_{\Delta}`                                                                                                                                                    
     :option:`rhos`       :math:`\rm M_\odot\,kpc^{-3}`                DM density at the scale radius, :math:`\rho_{\rm s} = \rho(r_{\rm s})`. Can be set to :keyword:`-1` for :keyword:`kNODES` DM density profile; in this case, the raw numerical values from the file :option:`gLIST_HALOES_NODES` are used in units of :math:`\rm kpc` and :math:`\rm M_\odot\,kpc^{-3}` (see :numref:`rst_kNODES`) for the profile (:option:`rs` is obsolete).
     :option:`rs`         :math:`\rm kpc`                              Scale radius, :math:`r_{\rm s}`                                                                                                                                                                                     
     :option:`prof.`      :math:`\rm -`                                Keyword for DM density profile. See :numref:`rst_enumerators_dm_profiles` for possible keywords                                                                                                                                                        
     :option:`#1,#2,#3`   :math:`\rm -`                                Shape parameters of the density profile.                                                                                                                                                                                                               
     :option:`IsTriaxial` :math:`\rm -`                                Boolean (:keyword:`1`/:keyword:`0`) to switch on/off triaxiality of the halo. See :ref:`rst_trixial` for details                                                                                                                                                                                                              
     :option:`a`          :math:`\rm -`                                1st axis ratio :math:`a` of triaxial halo  (if no rotation angle, major along x-axis)                                                                                                                                                                                                            
     :option:`b`          :math:`\rm -`                                2nd axis ratio :math:`b` of triaxial halo                                                                                                                                                                                                                                                         
     :option:`c`          :math:`\rm -`                                3rd axis ratio :math:`c` of triaxial halo  (if no rotation angle, minor along z-axis)                                                                                                                                                                                                            
     :option:`rot1`       :math:`\rm deg`                              1st rotation angle of triaxial halo, :math:`[-180^\circ,180^\circ]`                                                                                                                                                                                                              
     :option:`rot2`       :math:`\rm deg`                              2nd rotation angle of triaxial halo, :math:`[-90^\circ,90^\circ]`                                                                                                                                                                                                             
     :option:`rot3`       :math:`\rm deg`                              3rd rotation angle of triaxial halo, :math:`[-180^\circ,180^\circ]`                                                                                                                                                                                                             
     ==================== ============================================ =======================================================================================================================================================================================================================================================
   
  .. note ::
 
     1. Everything written behind a ``#`` (or a blank line) is discarded
     2. A DM shape is always defined by a DM family and three shape parameters (:option:`#1,#2,#3`). If the chosen profile takes less than three shape parameters, a numeric dummy value has to be assigned to the unnecessary ones. **We recommend to set the dummy value to -1, which lets the code check inconsistencies with the substructure description.**
     3. Substructure properties must be set in clumpy_params.txt for each object type, see :numref:`rst_universal_substructure_props`.
     4. :math:`R_{\Delta}`, :math:`r_{\rm s}`, and :math:`\rho_{\rm s}` have to be provided in comoving coordinates. Although they are given in units of kiloparsecs, all calculations in :program:`CLUMPY` can also be performed on the Megaparsec scale.
     5. The last 7 parameters for triaxiality are optional, i.e., the file is also read if no values are given, and :option:`IsTriaxial` is set to :keyword:`False`.


- The :option:`gLIST_HALOES_JEANS` is formatted as follows. A preformatted example file is also provided with the code in :download:`$CLUMPY/data/list_generic_jeans.txt <../data/list_generic_jeans.txt>`.

  .. code-block:: console

      #*************************************************************************************************************************************************************************************#
      #     [OBJECT SIZE]           |           [DATA]           |         DM DISTRIBUTION (RHO_TOT)        |             Light Profile                |        Anisotropy  Profile         #
      # Name    Type   Rdelta Rmax  |    Vel            Light    |  rhos       rs     prof.   #1   #2   #3  |     L        rs*    prof.   #1   #2   #3 | beta_0 beta_inf  prof.   ra   eta  #
      #  -        -    [kpc]  [kpc] |     -               -      |[Msol/kpc3]  [kpc]  [enum]    -    -    - |[Lsol/kpc3]  [kpc]  [enum]    -    -    - |   -       -     [enum]   kpc   -   #
      #*************************************************************************************************************************************************************************************#
      
      Test1     DSPH    10.   500. data_sigmap.txt data_light.txt  5.522e7    1.5   kZHAO    1.0  4.0  1.0  5.522e7      1.5  kZHAO3D  1.0  4.0  1.0     -1.   1.   kCONSTANT   1.0  4.0 

  .. table::
  
     ==================== ============================================ =======================================================================================================================================================================================================================================================
     **Parameter**        **Unit**                                     **Comment**                                                                                                                                                                                                                                  
     -------------------- -------------------------------------------- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     :option:`Name`       :math:`\rm -`                                Arbitrary name to identify your object                                                                                                                                                                                                                 
     :option:`Type`       :math:`\rm -`                                :keyword:`DSPH`, :keyword:`CLUSTER`, or :keyword:`GALAXY` (for external galaxies). This keyword decides which values are used to describe the halo substructure, defined in the main parameter file (see :numref:`rst_universal_substructure_props`)  
     :option:`Rdelta`     :math:`\rm kpc`                              Virial radius of the Dark matter halo (used as truncation radius of the halo for astrophysical factors) 
     :option:`Rmax`       :math:`\rm kpc`                              Maximum integration radius for Jeans analysis                                                                                                                                                                       
     :option:`Vel`        :math:`\rm -`                                Velocity data of the object, if available. File must be in the same folder as this file or given as absolute path. The data are drawn on the canvas, except if the entry is set to :keyword:`-`                                                                                               
     :option:`Light`      :math:`\rm -`                                Light data of the object, if available. File must be in the same folder as this file or given as absolute path. The data are drawn on the canvas, except if the entry is set to :keyword:`-`                                                                                                                                        
     :option:`rhos`       :math:`\rm M_\odot\,kpc^{-3}`                DM density at the scale radius, :math:`\rho_{\rm s} = \rho(r_{\rm s})`. Like for :option:`gLIST_HALOES`, can be set to :keyword:`-1` for :keyword:`kNODES` (see above for details).                                                                                                                           
     :option:`rs`         :math:`\rm kpc`                              Scale radius, :math:`r_{\rm s}`                                                                                                                                                                                     
     :option:`prof.`      :math:`\rm -`                                Keyword for DM density profile. See :numref:`rst_enumerators_dm_profiles` for possible keywords                                                                                                                                                        
     :option:`#1,#2,#3`   :math:`\rm -`                                Shape parameters of the density profile                                                                                                                                                                                                              
     :option:`L`          :math:`\rm L_\odot\,kpc^{-3}`                Luminosity of a halo (see :numref:`rst_subsec_luminosity`)                                                                                                                                                                                                             
     :option:`rs*`        :math:`\rm kpc`                              Scale radius (see :numref:`rst_physics_profiles`)                                                                                                                                                                                                       
     :option:`prof.`      :math:`\rm -`                                Light profile. See :ref:`rst_enumerators_lightprofiles` for possible keywords                                                                                                                                                                                                                                                        
     :option:`#1,#2,#3`   :math:`\rm -`                                Shape parameters of light profile                                                                                                                                                                                                            
     :option:`beta_0`     :math:`\rm deg`                              :math:`\beta_0`                                                                                                                                                                                                             
     :option:`beta_inf`   :math:`\rm -`                                :math:`\beta_\infty`                                                                                                                                                                                                            
     :option:`prof.`      :math:`\rm -`                                Anisotropy profile. See :ref:`rst_enumerators_anisotropyprofiles` for possible keywords                                                                                                                                                                                                             
     :option:`ra`         :math:`\rm kpc`                              :math:`r_a`                                                                                                                                                                                                          
     :option:`eta`        :math:`\rm -`                                :math:`\eta`                                                                                                                                                                                                            
     ==================== ============================================ =======================================================================================================================================================================================================================================================
 
   
  .. note ::
 
     1. A Light profile is always defined by a Light family, see :ref:`rst_enumerators_lightprofiles`, 3 shape parameters (:option:`#1,#2,#3`), a normalisation and a scale radius. If the chosen profile takes less than three shape parameters, a numeric dummy value has to be assigned to the unnecessary ones.
     2. An Anisotropy profile is always defined by a Anisotropy family, see :ref:`rst_enumerators_anisotropyprofiles`, 4 parameters: :math:`\beta_0` (if Constant or Baes profiles), :math:`\beta_\infty` (if Baes), scale radius (if Baes or Osipkov), and sharpness of the transition (if Baes).




:math:`\tt -h0`: mass profiles (1D)
++++++++++++++++++++++++++++++++++++++++++++++++++++

Calculates :math:`M_{\rm halo}(r)` in one dimension of all haloes defined in :option:`gLIST_HALOES`:

.. code-block:: console

    $ clumpy -h0 -i clumpy_params_h0.txt


.. figure:: DocImages/h0D.png
   :align: center
   :figclass: align-center
   :scale: 65%

   Mass profiles of the haloes in :file:`data/list_generic.txt`.

Note that triaxial haloes can be studied as well.

------------

.. _rst_h1:

:math:`\tt -h1`: density profiles (1D)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Calculates :math:`\rho_{\rm halo}(r)` (smooth, sub-continuum and total) of  **spherically symmetric** haloes defined in :option:`gLIST_HALOES` in one dimension:

.. code-block:: console

    $ clumpy -h1 -i clumpy_params_h1.txt


.. figure:: DocImages/h1D.png
   :align: center
   :figclass: align-center
   :scale: 65%

   Density profiles  of the haloes in :file:`data/list_generic.txt`.

------------

:math:`\tt -h2`: *J*-factors (1D)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Calculates :math:`J(\alpha_{\rm int})` towards a the halo centers defined in :option:`gLIST_HALOES`. Note that triaxial haloes can be studied as well.

.. code-block:: console

    $ clumpy -h2 -i clumpy_params_h2.txt


.. figure:: DocImages/h2D.png
   :align: center
   :figclass: align-center
   :scale: 60%

   *J*-factors (of smooth, sub-continuum and total components) of the haloes in :file:`data/list_generic.txt` as a function of the integration radius :math:`\alpha_{\rm int}` of the search cone :math:`\Delta\Omega`.

If enabled with the :option:`gSIM_IS_WRITE_FLUXMAPS = True`, also the corresponding fluxes are calculated. The corresponding :program:`ROOT` figure is shown in :numref:`fig_h2D-flux`.

------------

:math:`\tt -h3`: :math:`{\rm d}J/{\rm d}\Omega` (1D)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Calculates :math:`{\rm d}J/{\rm d}\Omega(\theta)` as a function of the distance :math:`\theta` from the centre **spherically symmetric** haloes defined in :option:`gLIST_HALOES`.

.. code-block:: console

    $ clumpy -h3 -i clumpy_params_h3.txt


.. figure:: DocImages/h3D.png
   :align: center
   :figclass: align-center
   :scale: 65%

   :math:`{\rm d}J/{\rm d}\Omega` (of smooth, sub-continuum and total components) over the distance :math:`\theta` from the centers of the haloes  in :file:`data/list_generic.txt`.

If enabled with the :option:`gSIM_IS_WRITE_FLUXMAPS=True`, also the corresponding intensities are calculated. The corresponding :program:`ROOT` figure is shown in :numref:`fig_h3D-intensities`.

------------

:math:`\tt -h4`: :math:`\langle J_{\rm pixel}\rangle` and :math:`\langle{\rm d}J/{\rm d}\Omega\rangle` (2D)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



.. code-block:: console

    $ clumpy -h4 -i clumpy_params_h4.txt

.. figure:: DocImages/h4D.png
   :align: center
   :figclass: align-center
   :scale: 60%


Corresponding flux- and intensity maps for the :option:`-h4` and :option:`-h5` are calculated  with the :option:`gSIM_IS_WRITE_FLUXMAPS = True` enabled.

------------

:math:`\tt -h5`: h4 + drawn subhaloes (2D)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

    $ clumpy -h5 -i clumpy_params_h5.txt


.. figure:: DocImages/h5D.png
   :align: center
   :figclass: align-center
   :scale: 60%

This is figure :numref:`fig_h5D` from the :ref:`rst_quick_start`.


------------

:math:`\tt -h6`: :math:`\alpha_f` for which :math:`J_f=f\times J_{\rm max}`
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


.. code-block:: console

    $ clumpy -h6 -i clumpy_params_h6.txt

.. code-block:: console

   ....

   >>>>> Load $CLUMPY/data/list_generic.txt
     - List of haloes loaded from /home/iwsatlas1/mhuetten/software/clumpy3/data/list_generic.txt
   #**********************************************************************************************************#
   #                  [OBJECT LOCATION AND SIZE]                |            DM DISTRIBUTION (RHO_TOT)        #
   # Name           Type      l       b      d      z   Rdelta  |     rhos       rs    prof.   #1   #2   #3   #
   #  -               -     [deg]   [deg]  [kpc]    -   [kpc]   |  [Msol/kpc3]  [kpc] [enum]    -    -    -   #
   #**********************************************************************************************************#
   rs01_gamma05    kDSPH     +20.0  +20.0    100  0.000     10     1.7e+09      0.1  kZHAO      1    3  0.5
   rs10_gamma10    kDSPH    +179.0   +0.0    100  0.000 18.1025     2.5e+07        1  kZHAO      1    3    1
   Abell_2261      kCLUSTER  +55.6  +31.9 941045  0.225   3000     7.0e+05      652  kEINASTO 0.17    3    1
   #**********************************************************************************************************#
   >>>>> Integration angle (in deg) to get J_x=80% of J (insensitive to rhos)
         for all haloes in /home/iwsatlas1/mhuetten/software/clumpy3/data/list_generic.txt

      Halo            d       alpha_100%    J_100%      alpha_x       J_x      as=atan(rs/d) alpha_x/a_s
                    [kpc]       [deg]   [Msol^2/kpc^5]   [deg]   [Msol^2/kpc^5]     [deg]         -
    rs01_gamma05   1.00e+02   1.72e+00     3.21e+11     7.62e-02    2.57e+11       5.73e-02    1.33e+00
    rs10_gamma10   1.00e+02   1.72e+00     4.32e+11     4.33e+00    3.46e+11       5.73e-01    7.55e+00
    Abell_2261     9.41e+05   1.83e-04     3.48e+10     4.72e-02    2.78e+10       3.97e-02    1.19e+00

      [alpha_int(f*Jtot)]  clumpy -h6 -i param_file


------------

:math:`\tt -h7`: test :math:`d` for which :math:`J_{\rm point-like}\approx J_{\rm full}`
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


.. code-block:: console

    $ clumpy -h7 -i clumpy_params_h7.txt

.. code-block:: console

   ....

   >>>>> Load $CLUMPY/data/list_generic.txt
     - List of haloes loaded from /home/iwsatlas1/mhuetten/software/clumpy3/data/list_generic.txt
   #**********************************************************************************************************#
   #                  [OBJECT LOCATION AND SIZE]                |            DM DISTRIBUTION (RHO_TOT)        #
   # Name           Type      l       b      d      z   Rdelta  |     rhos       rs    prof.   #1   #2   #3   #
   #  -               -     [deg]   [deg]  [kpc]    -   [kpc]   |  [Msol/kpc3]  [kpc] [enum]    -    -    -   #
   #**********************************************************************************************************#
   rs01_gamma05    kDSPH     +20.0  +20.0    100  0.000     10     1.7e+09      0.1  kZHAO      1    3  0.5
   rs10_gamma10    kDSPH    +179.0   +0.0    100  0.000 18.1025     2.5e+07        1  kZHAO      1    3    1
   Abell_2261      kCLUSTER  +55.6  +31.9 941045  0.225   3000     7.0e+05      652  kEINASTO 0.17    3    1
   #**********************************************************************************************************#
   >>>>> Distance [kpc] to set the halo (for alpha_int=0.1 deg)
         for which J(d) = 80% of Jpoint-like (=L/d^2)
         for all haloes in /home/iwsatlas1/mhuetten/software/clumpy3/data/list_generic.txt

      Halo       J_pointlike    J_x        d
                     [Msol^2/kpc^5]      [kpc]
    rs01_gamma05   9.22e-01   9.22e-01  5.73e+07
    rs10_gamma10   2.47e-01   2.47e-01  1.04e+08
    Abell_2261     1.25e+02   1.25e+02  1.40e+10


:math:`\tt -h8`: :math:`\sigma_p` from file (Jeans)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

    $ clumpy -h8 -i clumpy_params_h8.txt


.. figure:: DocImages/h8D.png
   :align: center
   :figclass: align-center
   :scale: 65%

   :option:`-h8`  output :program:`ROOT` figure


------------

:math:`\tt -h9`:  :math:`I(r)` from file (Jeans)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

    $ clumpy -h9 -i clumpy_params_h9.txt


.. figure:: DocImages/h9D.png
   :align: center
   :figclass: align-center
   :scale: 65%

   :option:`-h9`  output :program:`ROOT` figure


------------



:math:`\tt -h10`: :math:`\beta_{\rm ani}` from file (Jeans)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

    $ clumpy -h10 -i clumpy_params_h10.txt


.. figure:: DocImages/h10D.png
   :align: center
   :figclass: align-center
   :scale: 65%

   :option:`-h10` output :program:`ROOT` figure




