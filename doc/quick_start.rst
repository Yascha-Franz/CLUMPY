.. _rst_quick_start:

.. sectionauthor:: Moritz Huetten <moritz.huetten@desy.de>

.. role::  raw-html(raw)
    :format: html

Quick start tutorial
====================

Executing a :program:`CLUMPY` run
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

After successful installation and adding the :program:`CLUMPY` binary to your :envvar:`PATH` environment, type:

.. code-block:: console

    $ clumpy

.. code-block:: console

     _____________________________________________________________  ___  __________
    |   _       o     _____  _       _    _  __  __  _____ __ .    /   \       .   |
    |  (_)   .       / ____|| |  o  | |  | ||  \/  ||  __ \\ \   /(     )          |
    |            .  | |   . | |     | |  | || \  / || |__) |\ \_/ /\___/     o     |
    | .             | |     | |   . | |  | || |\/| ||  ___/  \   /       .         |
    |    __   o     | |____ | |____ | |__| || |  | || |     . | |     o         .  |
    |   /  \       . \_____||______| \____/ |_|  |_||_|  o    |_|    .         _   |
    |   \__/                                                               .  (_)  |
    |               v3.0, git-ID abcdefg (0 commits after last tag)                |
    |______________________________________________________________________________|

      Incomplete or wrong command line input - mind the correct usage:
         clumpy -g  => run on the Galactic halo only + list of halos [optional]
         clumpy -h  => run on (a list of) halos (not the Gal. halo)
         clumpy -e  => run on extragalactic DM
         clumpy -s  => run on statistical-like file (or on a list of files)
         clumpy -o  => export the output FITS file into a different format
         clumpy -z  => gamma and neutrino spectra
         clumpy -f  => append extension with fluxmaps to existing FITS file


:program:`CLUMPY` consists of various modules which are selected by a flag directly following the executable. Let us choose the Galactic module:

.. code-block:: console

    $ clumpy -g


.. code-block:: console

    ...
     ______________________________________________________________________________
    |                                                                              |
    |                              Simulation mode: g                              |
    |______________________________________________________________________________|

      Incomplete or wrong command line input - mind the correct usage:
       [M(r)                    ]  clumpy -g0 -i param_file -any_parameter (or --any_parameter)
       [rho_sm+<sub>(r)         ]  clumpy -g1 -i param_file -any_parameter (or --any_parameter)
       [Jsm+<sub>(alpha_int)    ]  clumpy -g2 -i param_file -any_parameter (or --any_parameter)
       [Jsm+<sub>(theta)        ]  clumpy -g3 -i param_file -any_parameter (or --any_parameter)
       [Jsm+<sub>+list(theta)   ]  clumpy -g4 -i param_file -any_parameter (or --any_parameter)
       [2D-skymap Jsm+<sub>     ]  clumpy -g5 -i param_file -any_parameter (or --any_parameter)
       [2D-skymap Jsm+<sub>+list]  clumpy -g6 -i param_file -any_parameter (or --any_parameter)
       [2D-skymap Jsm+sub       ]  clumpy -g7 -i param_file -any_parameter (or --any_parameter)
       [2D-skymap Jsm+sub+list  ]  clumpy -g8 -i param_file -any_parameter (or --any_parameter)

     N.B.: To print default parameters and write them to a file, use -D (or "-g1D", "-g2D", etc.)
           Append flag --all or -a to print/write ALL possible parameters readable by CLUMPY
     N.B.: Default output is both displays on screen and printing to files.
           => use option -d for displays only (e.g., -g1 -d)
           => use option -p for print only (e.g., -g1 -p)
     N.B.: The order of options does not matter, except for the very first option,
           which must be the simulation module (in this case -gX).

which lists the available options and also some other hints, e.g., how to generate a default parameter file. Let us generate a default parameter file for computing the density profile of the Galactic DM halo:

.. code-block:: console

    $ clumpy -g1 -D

.. code-block:: console

    ...
     ______________________________________________________________________________
    |                                                                              |
    |                             Simulation mode: g1                              |
    |______________________________________________________________________________|

       rho_sm+<sub>(r):  clumpy -g1 -i clumpy_params_g1.txt

       Default parameters for simulation mode g1 have been written to file clumpy_params_g1.txt
       Now execute:

       $CLUMPY/bin/clumpy -g1 -i clumpy_params_g1.txt

       or alternatively, use the following default command line variables & values:

       $CLUMPY/bin/clumpy -g1 --gCOSMO_DELTA0=200 --gCOSMO_FLAG_DELTA_REF=kRHO_CRIT --gDM_SUBS_MMIN=1e-06 --gDM_SUBS_MMAXFRAC=0.01 --gDM_RHOSAT=1e+19 --gMW_TOT_FLAG_PROFILE=kEINASTO --gMW_TOT_SHAPE_PARAMS_0=0.17 --gMW_TOT_RSCALE=15.14 --gMW_RMAX=260 --gMW_RSOL=8 --gMW_RHOSOL=0.4 --gMW_SUBS_DPDV_FLAG_PROFILE=kHOST --gMW_SUBS_DPDV_SHAPE_PARAMS_0=0.68 --gMW_SUBS_DPDV_RSCALE_TO_RS_HOST=13.14399 --gMW_SUBS_DPDM_SLOPE=1.9 --gMW_SUBS_M1=1e+08 --gMW_SUBS_M2=1e+10 --gMW_SUBS_N_INM1M2=150 --gSIM_NX=100 --gSIM_IS_XLOG=1 --gSIM_R_MIN=0.01 --gSIM_R_MAX=100 --gSIM_IS_ASTRO_OR_PP_UNITS=0 --gSIM_OUTPUT_DIR=output/ --gSIM_IS_WRITE_ROOTFILES=1

Now we find a file :file:`clumpy_params_g1.txt` in the execution directory. With this, we are ready to execute a :program:`CLUMPY` run:

.. code-block:: console

    $ clumpy -g1 -i clumpy_params_g1.txt

which ends with:

.. code-block:: console

    ...
    _______________________

     ... output [ASCII] written in: output/gal.rhor.output
     ... output [use ROOT TBrowser] written in: output/gal1D.rhor.root
    _______________________

and a :program:`ROOT` pop-up plot in case you have linked :program:`CLUMPY` against :program:`ROOT` in the installation:

.. _fig_g1D:

.. figure:: DocImages/g1D.png
   :align: center
   :figclass: align-center
   :scale: 65%

   Density profile of the Galactic DM halo with :program:`CLUMPY`'s default parameters.

In the same spirit, all :program:`CLUMPY` modules can be controlled. Lastly, let us change a parameter not in the parameter file, but do it directly on the command line. For example,

.. code-block:: console

    $ clumpy -g1 -i clumpy_params_g1.txt --gSIM_IS_XLOG=0

overwrites the value already given in the file and computes the density profile on a linear grid:

.. figure:: DocImages/g1D-linear.png
   :align: center
   :figclass: align-center
   :scale: 65%

   Density profile of the Galactic DM halo on a linear x-scale.


Displaying a 2D run with Python
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Let us compute a :math:`\gamma`-ray intensity skymap from some dwarf galaxy including substructure. This is done by executing

.. code-block:: console

    $ clumpy -h5 -D; clumpy -h5 -i clumpy_params_h5.txt

If :program:`ROOT` is linked, the following two plots are shown:

.. _fig_h5D:

.. figure:: DocImages/h5D.png
   :align: center
   :figclass: align-center
   :width: 100%

   Intensity skymap from the :option:`-h5` module, with default parameters and displayed with :program:`ROOT`.


Also, an output :program:`FITS` file has been written to ``output/annihil_rs01_gamma052D_FOVdiameter2.0deg_nside1024.fits``.

.. note ::

    The example halo ``rs01_gamma05`` is read from the definition file :file:`$CLUMPY/data/list_generic.txt`. Therefore, you need the :envvar:`CLUMPY` environmental variable pointing to your installation directory.

Now, we start Python

.. code-block:: console

    $ python

and execute the following commands (click on the little ``>>>`` on the right to hide them):

.. code-block:: python

    >>> import healpy as hp
    >>> from astropy.io import fits
    >>> from matplotlib import pyplot as plt
    >>> outfile = 'output/annihil_rs01_gamma052D_FOVdiameter2.0deg_nside1024.fits'
    >>> ext = 3 # fluxes and intensities
    >>> col = 3 # gamma-ray intensity
    >>> data = hp.read_map(outfile, partial=True, hdu=ext, field=col-1)
    NSIDE = 1024
    ORDERING = NESTED in fits file
    INDXSCHM = EXPLICIT
    Ordering converted to RING
    >>> hdulist = fits.open(outfile)
    >>> dtheta = hdulist[ext].header['SIZE_Y']
    >>> dtheta_orth = hdulist[ext].header['SIZE_X']
    >>> title = hdulist[ext].header['TTYPE'+str(col+1)]
    >>> units = hdulist[ext].header['TUNIT'+str(col+1)]
    >>> hdulist.close()
    >>> hp.cartview(data, lonra=[-dtheta_orth/2,dtheta_orth/2], latra=[-dtheta/2,dtheta/2], norm='log', max=data.max()/10, title=title, unit=units)
    >>> plt.show()


.. _fig_h5D-pythonplot-intensity:

.. figure:: DocImages/h5D-pythonplot-intensity.png
   :align: center
   :figclass: align-center

   Intensity skymap from the default :option:`-h5` module, displayed with healpy and matplotlib.

.. note::

   :numref:`fig_h5D-pythonplot-intensity` is mirrored w.r.t. :numref:`fig_h5D`. This can be easily  reconciled by using the ``flip='geo'`` parameter in :raw-html:`<A href="http://healpy.readthedocs.io" target="_blank">healpy</A>'s` ``cartview()`` function.

You can also choose another extension and column:

.. code-block:: python

    >>> ext = 2 # dJ/dOmega
    >>> col = 2 # smooth DM only
    >>> data = hp.read_map(outfile, partial=True, hdu=ext, field=col-1)
    NSIDE = 1024
    ORDERING = NESTED in fits file
    INDXSCHM = EXPLICIT
    Ordering converted to RING
    >>> hdulist = fits.open(outfile)
    >>> title = hdulist[ext].header['TTYPE'+str(col+1)]
    >>> units = hdulist[ext].header['TUNIT'+str(col+1)]
    >>> hdulist.close()
    >>> hp.cartview(data, lonra=[-dtheta_orth/2,dtheta_orth/2], latra=[-dtheta/2,dtheta/2], norm='log', title=title, unit=units)
    >>> plt.show()

.. _fig_h5D-pythonplot-Jsmooth:

.. figure:: DocImages/h5D-pythonplot-Jsmooth.png
   :align: center
   :figclass: align-center

   *J*-factor skymap from the default :option:`-h5` module, displayed with healpy and matplotlib.

:download:`Download this little script as a Jupyter-notebook <DocData/plotClumpyMaps.ipynb>`

To obtain an overview what is stored in the :program:`FITS` file and which extensions ``ext`` and columns ``map`` you can select, type

.. code-block:: console

    $ clumpy -o2 -i output/annihil_rs01_gamma052D_FOVdiameter2.0deg_nside1024.fits

which prints

.. code-block:: console

   ...
    ______________________________________________________________________________
   |                                                                              |
   |                             Simulation mode: o2                              |
   |______________________________________________________________________________|

     Usage:
      [ASCII export of FITS extension]  clumpy -o1 -i results.fits [extension]
      [cutsky map to whole-sky FITS map]  clumpy -o2 -i results.fits [extension] [map]
      [get input params from FITS extension]  clumpy -o3 -i results.fits [extension]

    >>>>> read FITS headers from file output/annihil_rs01_gamma052D_FOVdiameter2.0deg_nside1024.fits ...
      => FITS file contains 3 extensions:

         Extension 1 :
           name of this extension: JFACTOR
           physical maps in this extension: 5
              map 0: PIXEL                [Healpix Pixel indices]
              map 1: Jtot                 Units: GeV^2 cm^-5
              map 2: Jsmooth              Units: GeV^2 cm^-5
              map 3: Jsub                 Units: GeV^2 cm^-5
              map 4: Jcrossp              Units: GeV^2 cm^-5
              map 5: Jdrawn               Units: GeV^2 cm^-5
           n_side of the maps: 1024
           ordering scheme of the maps: 1
           number of pixels (n_pix) in the maps: 960

         Extension 2 :
           name of this extension: JFACTOR_PER_SR
           physical maps in this extension: 5
              map 0: PIXEL                [Healpix Pixel indices]
              map 1: Jtot_per_sr          Units: GeV^2 cm^-5 sr^-1
              map 2: Jsmooth_per_sr       Units: GeV^2 cm^-5 sr^-1
              map 3: Jsub_per_sr          Units: GeV^2 cm^-5 sr^-1
              map 4: Jcrossp_per_sr       Units: GeV^2 cm^-5 sr^-1
              map 5: Jdrawn_per_sr        Units: GeV^2 cm^-5 sr^-1
           n_side of the maps: 1024
           ordering scheme of the maps: 1
           number of pixels (n_pix) in the maps: 960

         Extension 3 :
           name of this extension: INTEGRATED_FLUXES
           physical maps in this extension: 4
              map 0: PIXEL                [Healpix Pixel indices]
              map 1: Flux_gamma           Units: cm^-2 s^-1
              map 2: Flux_neutrino        Units: cm^-2 s^-1
              map 3: Intensity_gamma      Units: cm^-2 s^-1 sr^-1
              map 4: Intensity_neutrino   Units: cm^-2 s^-1 sr^-1
           n_side of the maps: 1024
           ordering scheme of the maps: 1
           number of pixels (n_pix) in the maps: 960

     N.B.: for the -o2 option, the size of the whole-sky FITS files
           will be about ~ GB (with lots of BLIND values), for NSIDE ~ 8000!


You can use this facility to convert the output into fullsky maps  more flexible readable by many :program:`FITS` viewers, see :ref:`rst_FITS_display` for details.

