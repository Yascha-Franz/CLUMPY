.. _rst_physics_spectra:

.. sectionauthor:: David Maurin <dmaurin@lpsc.in2p3.fr> and Moritz Huetten <moritz.huetten@desy.de>

.. role::  raw-html(raw)
    :format: html

Source spectrum (spectra.h)
~~~~~~~~~~~~~~~~~~~~~~~~~~~

DM annihilation/source spectra calculations are implemented in the library `spectra.h <doxygen/spectra_8h.html>`_.

Definitions and formulae
+++++++++++++++++++++++++

The elementary :math:`\gamma`-ray (or neutrino) spectrum seen from a local observer can be generically written for:

   - *Annihilating* DM (:option:`gPP_DM_IS_ANNIHIL_OR_DECAY = True`):

      .. math:: \frac{{\rm d} \Phi^{PP~\rm annihil.}_{\gamma,\nu}}{{\rm d} E}(E,z) = \frac{1}{4\pi}\, \frac{\langle \sigma_{\rm ann}v \rangle}{\delta m_{\rm DM}^{2}} \times \sum_{f}B_{f}\,\left.\frac{{\rm d}N^{f}_{\gamma,\nu}}{{\rm d}E_0}\right|_{E_0= (1+z)\,E}\times e^{-\tau(E,z)}\,.
        :label: eq_pp2 
       
   - *Decaying* DM  (:option:`gPP_DM_IS_ANNIHIL_OR_DECAY = False`):

      .. math:: \frac{{\rm d} \Phi^{PP~\rm decay}_{\gamma,\nu}}{{\rm d} E}(E,z) = \frac{1}{4\pi}\frac{1}{\tau_{\rm DM}\,m_{\rm DM}}
               \times \sum_{f}B_{f}\,\left.\frac{{\rm d}N^{f}_{\gamma,\nu}}{{\rm d}E_0}\right|_{E_0= (1+z)\,E}\times e^{-\tau(E,z)}\,.
         :label: eq_pp3


The various terms are detailed in :numref:`tab_ppparameters` below (see :numref:`rst_subsection_absorption`). Note that the differential γ-ray/ν yield per annihilation (or decay) is evaluated at :math:`E_0 = (1 + z)E` to get a photon at :math:`E` today. For local targets (Galaxy, close clusters), :math:`z\sim 0`, energies are not redshifted, and :math:`e^{-\tau(E,z)}\sim 1` (no absorption).

:raw-html:`<center>`

.. _tab_ppparameters:

.. table:: Parameters for the particle physics factor of *annihilating* or *decaying* DM and the corresponding :program:`CLUMPY` parameter names.

   ================================================================ ================================================================================================================================================================================================ ===========================================================================
   **Symbol**                                                       **Physical quantity**                                                                                                                                                                            **CLUMPY parameter name**
   ---------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ ---------------------------------------------------------------------------
   :math:`m_{\rm DM}`                                               Mass of the DM candidate, :math:`[m_{\rm DM}]=\rm GeV`.                                                                                                                                          :option:`gPP_DM_MASS_GEV`
   :math:`B_f`                                                      Branching ratios into the final state :math:`f` (see `below <#secondary-particle-spectra-final-states-and-branching-ratios>`_).                                                                  :option:`gPP_BR`
   :math:`{\rm d}N^{f}_{\gamma,\nu}/{\rm d}E`                       Model for the :math:`\gamma`-ray or :math:`\nu` yield of final state :math:`f`, see `below <#secondary-particle-spectra-final-states-and-branching-ratios>`_ and :ref:`rst_enumerators_spectra`. :option:`gPP_FLAG_SPECTRUMMODEL`
   :math:`\langle\sigma_{\rm ann}v\rangle`, :math:`\delta`          *Annihilation*: velocity averaged annihilation cross section :math:`[\langle\sigma_{\rm ann}v\rangle]={\rm cm}^3 {\rm s}^{-1}`, :math:`\delta=2` for a Majorana and 4 for a Dirac fermion.       :option:`gPP_DM_ANNIHIL_SIGMAV_CM3PERS` and :option:`gPP_DM_ANNIHIL_DELTA`
   :math:`\tau_{\rm DM}`                                            *Decay*: decay lifetime, :math:`[\tau_{\rm DM}]=\rm s`                                                                                                                                           :option:`gPP_DM_DECAY_LIFETIME_S`
   :math:`\tau(E,z)`                                                :math:`\gamma`-ray attenuation factor (no absorption for :math:`\nu`), tabulated in energy and redshift according to various models, see :ref:`rst_enumerators_tauEBL`.                          :option:`gEXTRAGAL_FLAG_ABSORPTIONPROFILE`
   ================================================================ ================================================================================================================================================================================================ ===========================================================================

:raw-html:`</center>`


.. note::

   - :program:`CLUMPY` only considers *prompt* emission at source (e.g., :raw-html:`<a href=http://adsabs.harvard.edu/abs/2011JCAP...03..051C target="_blank">Cirelli et al., 2011)</a>`, without taking into account effects like Inverse Compton scattering (see, e.g., :raw-html:`<a href="http://cdsads.u-strasbg.fr/abs/2009JCAP...07..020P" target=_blank>Profumo & Jeltema, 2009</a> or <a href="https://ui.adsabs.harvard.edu/#abs/2009NuPhB.821..399C" target=_blank>Cirelli and Panci, 2009</a>`). :raw-html:`<br>`

   - The location of the :math:`1/4\pi` factor appearing in Eqs. :eq:`eq_pp2` and :eq:`eq_pp3` is arbitrary. We follow :raw-html:`<A href="http://adsabs.harvard.edu/abs/2009A&A...496..351P" target="_blank">Pieri et al. (2009)</A>` and include it in the particle physics factor. In other works, it can appear in the astrophysical factor :math:`J` (e.g., :raw-html:`<A href="http://adsabs.harvard.edu/abs/2009JCAP...01..016B" target="_blank">Bringmann et al., 2009</A>).` :raw-html:`<br>`

   - For the particle physics factor, we use the units

     .. math:: \left[ \frac{{\rm d} \Phi^{PP~\rm annihil.}_{\gamma,\nu}}{{\rm d} E}\right] = {\rm cm}^{3}{\rm ~s}^{-1} {\rm ~GeV}^{-3}\,, \quad\left[\frac{{\rm d} \Phi^{PP~\rm decay}_{\gamma,\nu}}{{\rm d} E}\right] = {\rm ~s}^{-1} {\rm ~GeV}^{-2}.

     According to the definition of the *J*-factor in the Section :ref:`rst_physics_integr_los`, we discard writing the dimensionless solid angle in the above units. Some authors do however explicitly express the steradians, :math:`\rm sr`, in their units (for instance in :raw-html:`<A href="http://adsabs.harvard.edu/abs/2009A&A...496..351P" target="_blank">Pieri et al., 2009</A>).` See also Appendix A of :raw-html:`<A href="http://adsabs.harvard.edu/abs/2011MNRAS.418.1526C" target="_blank">Charbonnier et al. (2011)</A>.`


.. _rst_subsection_absorption:

Absorption :math:`e^{-\tau(E,z)}`
++++++++++++++++++++++++++++++++++

Absorption of the :math:`\gamma`-ray photons are due to pair production after interaction with the extragalactic background light (EBL) and the cosmic microwave background (CMB). In this :program:`CLUMPY` version, absorption on the CMB, which affects the signal from nearby (:math:`<1\,{\rm Mpc}`) sources above several tens of TeV is not implemented.

Several types of models exist in the literature to describe the EBL:

   - Backward modelling, starts from the observed multi-wavelength properties of existing local galaxies and extrapolate to larger redshifts (e.g. :raw-html:`<a href="http://cdsads.u-strasbg.fr/abs/2008A&A...487..837F,2017A&A...603A..34F" target=_blank>Francescini et al., 2008, 2017</a>`).
   - Forward modelling relies on semi-analytical models of galaxy formation and evolution to compute the opacity at any redshift (e.g. :raw-html:`<a href="http://cdsads.u-strasbg.fr/abs/2012MNRAS.422.3189G,2013ApJ...768..197I" target=_blank>Gilmore et al. 2012, and Inoue et al. 2013</a>`). 
   - Other methods focus instead on the mechanisms responsible for the EBL, i.e. emission of stars and dust, and integrate over stellar properties and star formation rate :raw-html:`<a href="http://cdsads.u-strasbg.fr/abs/2010ApJ...712..238F" target=_blank>(Finke et al., 2010)</a>`, or rely on the direct observation of galaxies over the redshift range of interest :raw-html:`<a href="http://cdsads.u-strasbg.fr/abs/2011MNRAS.410.2556D" target=_blank>(Dominguez et al., 2011)</a>`.

Given the variety in the modelling approaches, the results for :math:`\tau(E,z)` of each of the works cited above is implemented in :program:`CLUMPY` (see :ref:`rst_enumerators_tauEBL`). At each redshift, we transform the tabulated data from these works into coefficients of a fitted :math:`11^{\rm th}` order polynomial in energy. The optical depth :math:`\tau` is then computed from the polynomial in energy and interpolated in redshift.


.. _rst_branching_ratios:

Branching ratios :math:`B_f`
++++++++++++++++++++++++++++

DM particles may annihilate or decay into various products with some model-dependent (unknown) branching fraction. While the models :keyword:`kBERGSTROM98`, :keyword:`kTASITSIOMI02`, and :keyword:`kBRINGMANN08` describe the average :math:`\gamma`-ray yield for some specific SUSY model, for the spectra from :keyword:`kCIRELLI11_EW` and :keyword:`kCIRELLI11_NOEW`, the user can specify her/his own mixing  out of 28 branching channels for 28 primary channels reported in  :raw-html:`<a href=http://www.marcocirelli.net/PPPC4DMID.html  target="_blank">PPPC 4 DM ID</a>`:


  :raw-html:`<center>`

  .. _tab_branchingchannels:

  .. table:: Possible branching channels to select for the :keyword:`kCIRELLI11_EW` and :keyword:`kCIRELLI11_NOEW` final states.

      =============================== ================================= ================================== ===============================
      [00] :math:`\;e_{L}^+e_L^-`     [01] :math:`\;e_R^+e_R^-`         [02] :math:`\;e^+e^-`
      [03] :math:`\;\mu_L^+\mu_L^-`   [04] :math:`\;\mu_R^+\mu_R^-`     [05] :math:`\;\mu^+\mu^-`
      [06] :math:`\;\tau_L^+\tau_L^-` [07] :math:`\;\tau_R^+\tau_R^-`   [08] :math:`\;\tau^+\tau^-`
      [09] :math:`\;q\bar{q}`         [10] :math:`\;c\bar{c}`           [11] :math:`\;b\bar{b}`            [12]   :math:`\;t\bar{t}`
      [13] :math:`\;W_L^+W_L^-`       [14] :math:`\;W_T^+W_T^-`         [15] :math:`\;W^+W^-`
      [16] :math:`\;Z_LZ_L`           [17] :math:`\;Z_TZ_T`             [18] :math:`\;ZZ`
      [19] :math:`\;gg`               [20] :math:`\;\gamma\gamma`       [21] :math:`\;hh`
      [22] :math:`\;\nu_e\nu_e`       [23] :math:`\;\nu_\mu\nu_\mu`     [24] :math:`\;\nu_\tau\nu_\tau`
      [25] :math:`\;VV\rightarrow 4e` [26] :math:`\;VV\rightarrow 4\mu` [27] :math:`\;VV\rightarrow 4\tau`
      =============================== ================================= ================================== ===============================

  :raw-html:`</center>`


Any linear combination of these channels can be selected via the :option:`gPP_BR` input parameter set to a comma-separated list of 28 numbers specifying the relative contribution :math:`B_f` of each channel. E.g.::

    --gPP_BR=0,0,0,0,0,0,0,0,0.5,0,0,0.5,0,0,0,0,0,0,0,0,1e-4,0,0,0,0,0,0,0

on the command line or (note the correct usage of spaces) in a parameter file::

    gPP_BR = 0,0,0,0,0,0,0,0,0.5,0,0,0.5,0,0,0,0,0,0,0,0,1e-4,0,0,0,0,0,0,0

selects the case where 50% of the DM particles annihilate/decay into :math:`\tau^+\tau^-`, 50% into :math:`b\bar{b}`, and with a probability of :math:`10^{-4}` directly into two :math:`\gamma`-rays.

.. note::

   If the sum :math:`\sum B_f \neq 1`, like in the example above, the sum will be renormalised, but a warning is printed.


.. _rst_doc_finalstates:

Enabled spectra :math:`{\rm d}N/{\rm d}E`
++++++++++++++++++++++++++++++++++++++++++

Various parametrisations can be selected with the :option:`gPP_FLAG_SPECTRUMMODEL`, for the annihilation/decay of DM particles of mass parameter :math:`m_{\chi}`. An overview of these possible models and keywords for the :option:`gPP_FLAG_SPECTRUMMODEL` variable is given in :ref:`rst_enumerators_spectra`, detailed below, and illustrated from :numref:`fig_spectra` (taken from :raw-html:`<a href=http://arxiv.org/abs/1104.0412 target="_blank">Charbonnier et al., 2011</a>`).

   .. _fig_spectra:

   .. figure:: DocImages/spectra.png
      :figclass: align-center
      :scale: 85%

      Average :math:`\gamma`-ray yield according to :raw-html:`<a href=http://adsabs.harvard.edu/abs/1998APh.....9..137B target="_blank">Bergstr&ouml;m et al. (1998)</a>` and :raw-html:`<a href=http://adsabs.harvard.edu/abs/2008JHEP...01..049B target="_blank">Bringmann et al. (2008)</a>`, and  pure branching channels from :raw-html:`<a href=http://adsabs.harvard.edu/abs/2004PhRvD..70j3529F target="_blank">Fornengo et al. (2004)</a>.`


**Average γ-ray production for annihilating DM**

   Defining the variable :math:`x = \frac{E_{\gamma}}{m_{\chi}}` and :math:`\frac{{\rm d}N_{\gamma}}{{\rm d}E_{\gamma}} = \frac{1}{m_{\chi}} \times \frac{{\rm d}N_{\gamma}}{{\rm d}x}`, we have

      - :keyword:`kBERGSTROM98` :raw-html:`<a href=http://adsabs.harvard.edu/abs/1998APh.....9..137B target="_blank">(Bergstr&ouml;m et al., 1998)</a>:`

         .. math:: \frac{{\rm d}N_{\gamma}}{{\rm d}x} = 0.73 \times \frac{\exp(-7.8x)}{x^{1.5}}\,,

      - :keyword:`kTASITSIOMI02` :raw-html:`<a href=http://adsabs.harvard.edu/abs/2002PhRvD..66h3006T target="_blank">(Tasitsiomi et al., 2002)</a>:`

         .. math::  \frac{{\rm d}N_{\gamma}}{{\rm d}x} =\left[ \frac{10}{3} - \frac{5}{4} \, x^{0.5} - \frac{5}{2} \, x^{-0.5} + \frac{5}{12} \, x^{-1.5}\right]

      - :keyword:`kBRINGMANN08` :raw-html:`<a href=http://adsabs.harvard.edu/abs/2008JHEP...01..049B target="_blank">(Eq.(8) of Bringmann et al., 2008)</a>:`

         .. math:: \frac{{\rm d}N_{\gamma}}{{\rm d}x}= \frac{\alpha_{em}}{\pi} \, \frac{4\left(1-x+x^{2} \right) ^{2}}{\left( 1-x+\epsilon/2 \right) x} \times \left[ \ln \left(2 \frac{1-x+\epsilon/2}{\epsilon} \right) - \frac{1}{2} + x - x^{3} \right]

         .. note:: This parametrisation corresponds to the internal bremsstrahlung, which strongly depends on the nature of the neutralino (here, it corresponds to an heavy neutralino, pure Higgsino or Wino).

**γ-ray and ν production accounting from specific channels**

   :keyword:`kCIRELLI11_EW` and :keyword:`kCIRELLI11_NOEW` :raw-html:`<a href=http://adsabs.harvard.edu/abs/2011JCAP...03..051C,2012JCAP...10E.001C target="_blank">(Cirelli et al., 2011+2012)</a>` provide tabulated spectra of recent PYTHIA simulations :raw-html:`<a href=http://www.marcocirelli.net/PPPC4DMID.html  target="_blank">(PPPC 4 DM ID)</a>`. These are included in :program:`CLUMPY` and  stored in the directory :file:`$CLUMPY/data/PPPC4DMID-spectra/`. If :keyword:`kCIRELLI11_EW` or :keyword:`kCIRELLI11_NOEW` are chosen, the resulting spectrum is calculated by a 2D linear interpolation on :math:`\log(E)` and :math:`\log(m)` from the tabulated spectra. Note that both the results with (EW) or without (NOEW) electro-weak corrections are available. The final state (:math:`\gamma`-ray or neutrinos) can be selected via the :option:`gSIM_FLUX_FLAG_FINALSTATE` parameter, with the possible enumerator values given in :numref:`tab_finalstates`:

   :raw-html:`<center>`

   .. _tab_finalstates:

   .. csv-table:: Possible keyword choices for the :option:`gSIM_FLUX_FLAG_FINALSTATE` parameter.
      :file: tables/gENUM_FINALSTATE.csv_table
      :widths: 10, 10

   :raw-html:`</center>`

   .. note::

      The *charged particle final states* (anti-protons, positrons, and electrons) are only implemented in :program:`CLUMPY` for displaying the elementary energy spectrum at source with::

         $ clumpy -z -i your_parameter_file.txt

      Apart from this exception, :program:`CLUMPY` is designed to deal only with neutral γ-ray and ν final states of dark matter annihilation/decay processes.


Neutrino flavour and mixing
+++++++++++++++++++++++++++

For distant astrophysical sources, the journey in vacuum can be described by an average oscillation that must be accounted for.

.. note:: :program:`CLUMPY` provides the flux for :math:`\nu_{e}`, :math:`\nu_{\mu}` or :math:`\nu_{\tau}`. DM detection with neutrino telescopes are sensitive to :math:`\nu_{\mu}+\bar{\nu}_{\mu}= 2\times \nu_{\mu}`.  This factor 2 for anti-neutrino contribution is not accounted for in :program:`CLUMPY` and must be set by hand by the user.

**Flavour selection**

   If neutrino fluxes are selected as outputs::

      gPP_FLAG_SPECTRUMMODEL = kCIRELLI_EW
      gSIM_FLUX_FLAG_FINALSTATE = kNEUTRINO

   the considered final state neutrino flavour must be selected by the parameter :option:`gSIM_FLUX_FLAG_NUFLAVOUR`:

      :raw-html:`<center>`

      .. _tab_neutrinospecies:

      .. csv-table:: Possible keyword choices for the :option:`gSIM_FLUX_FLAG_NUFLAVOUR` parameter.
         :file: tables/gENUM_NUFLAVOUR.csv_table
         :widths: 10, 10

      :raw-html:`</center>`


**Neutrino oscillation**

   For distant astrophysical sources, the travelling in vacuum and transition between the different flavour states can be described by average oscillations :raw-html:`<a href=http://adsabs.harvard.edu/abs/1987RvMP...59..671B target="_blank">(Bilenky and Petcov, 1987)</a>:`

      .. math::

            P(\nu_l\rightarrow\nu_{l'})=P(\bar{\nu_l}\rightarrow\bar{\nu}_{l'})=\sum_{k=1}^3 |U_{l'k}|^2 |U_{lk}|^2.


      - :math:`U` is the neutrino mixing matrix and :math:`k=1,2,3` for the 3 mass eigenstates.
      - The mixing angles :math:`\{\theta_{12},\,\theta_{23},\,\theta_{13}\}` correspond to the input parameters

        .. math::

            \tt gPP\_NUMIXING\_THETA12\_DEG\:,\\
            \tt gPP\_NUMIXING\_THETA13\_DEG\:,\\
            \tt gPP\_NUMIXING\_THETA23\_DEG\:,


      - The :math:`CP` phase is set to :math:`\delta=0` as the default value (see, e.g., :raw-html:`<a href=http://adsabs.harvard.edu/abs/2014PhRvD..90i3006F target="_blank">Forero et al., 2014</a> for recent values and a discussion).`







