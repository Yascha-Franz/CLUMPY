.. _rst_physics_2D:

.. sectionauthor:: Moritz Huetten <moritz.huetten@desy.de>

.. role::  raw-html(raw)
    :format: html

2D skymaps (healpix_fits.h)
----------------------------

Galactic and extragalactic signals correspond to 2D skymaps. The file `healpix_fits.h <doxygen/healpix__fits_8h.html>`__ contains many tools to manipulate 2D skymaps in several formats, in particular to:

   - make use of the :program:`HEALPix` pixelisation scheme on which to calculate the quantities of interest;
   - to add the contribution from single DM haloes (e.g. drawn subhaloes, or known DM haloes such as dSphs or other objects from a list).
   - to handle the :program:`FITS` format input/output of 2D maps.

.. note :: For 2D calculations, :program:`CLUMPY` so far only treats cases where the spatial and spectral part of the indirect DM signal are separable, i.e., the morphology of the signal is not varying with energy.

When we calculate the *J*-factor for a 2D-skymap with a large number of pixels, we only calculate J for the continuum in fewer directions and interpolate the results for all the other directions (pixels) of the skymap. If the :abbr:`FOV (field of view)` does not encompass the halo centre, we use a simple linear interpolation grid. Towards the halo centre, **J** has a strong dependence on the position and a logarithmic step is used.




:program:`HEALPix` pixelisation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Two-dimensional maps of :math:`J(\Delta\Omega_{\rm pix})`, :math:`{\rm d}J/{\rm d}\Omega`, or of the :math:`\gamma`-ray/:math:`\nu` fluxes/intensities are drawn in the :program:`HEALPix` pixelisation scheme. The :raw-html:`<A href="http://healpix.sourceforge.net" target="_blank">HEALPix</A>` (Hierarchical Equal Area isoLatitude Pixelation) package provides a large set of routines for efficient manipulation and analysis of numerical data on the discretized sphere :raw-html:`<A href="http://adsabs.harvard.edu/abs/2005ApJ...622..759G" target="_blank">(Gorski et al., 2005)</A>`.

.. _fig_healpix_pixelization:

.. figure:: DocImages/healpix_pixelization.png
   :align: center
   :figclass: align-center
   :width: 60%
   
   Illustration of the :program:`HEALPix` pixelisation of the sphere. The displayed :program:`HEALPix` resolutions are (clockwise from top left): :math:`N_{\rm side}=1,\,2,\,4,\,8`. If the uppermost pixel in the  light grey base pixel is :math:`\#1`, then the Galactic centre is located in the centre of the dark grey base pixel. :raw-html:`<br>Figure credit: <A href="http://adsabs.harvard.edu/abs/2005ApJ...622..759G" target="_blank">Gorski et al. (2005)</A>.`

The :program:`HEALPix` scheme discretises the full sphere into :math:`N_{\rm pix} = 12\,N_{\rm side}^2` equal-area quadrilateral pixels. It is 

.. math::  \Delta\Omega_{\rm pix} = \frac{\pi}{3\,N_{\rm side}^2}\,{\rm sr}\quad \Rightarrow \quad d_{\rm pix} \approx \frac{58.6^{\circ}}{N_{\rm side}}\,,
   :label: eq_pixelsize

with :math:`d_{\rm pix}` the *approximate* edge-length of each pixel, assuming that the pixels are quadratic (which they are not) and :math:`\Delta\Omega_{\rm pix} \ll \pi`.

:math:`N_{\rm side}` is a resolution parameter and allowed to take any integer value,  :math:`N_{\rm side}\geq 1` in :program:`CLUMPY` via the parameter :option:`gSIM_HEALPIX_NSIDE`.
 
.. note:: 

    - While :option:`gSIM_HEALPIX_NSIDE` is allowed to take any integer value, it is recommended, especially for large or high-resolution skymaps, to choose :math:`N_{\rm side} = 2^n, \,n\in \mathbb{N}^+`. This choice allows a more efficient storage of the data in the :program:`HEALPix` :keyword:`NESTED` scheme with faster I/O and handling by :program:`FITS` viewers (which may not be able to deal with :math:`N_{\rm side} \neq 2^n` maps). :raw-html:`<br><br>`

    - The current :program:`HEALPix` version allows only :math:`N_{\rm side} \leq 2^{13} = 8192`, which determines the maximum resolution for 2D skymaps in :program:`CLUMPY`. However, with correspondingly :math:`d_{\rm pix} \gtrsim 0.007^{\circ} = 26''`, this is well below the instrumental resolution of current :math:`\gamma`-ray or :math:`\nu` instruments.

In :program:`CLUMPY`, the :program:`HEALPix` grid is always initialised in Galactic coordinates. I.e., in the :keyword:`RING` ordering scheme (:option:`gSIM_HEALPIX_SCHEME` = :keyword:`RING`), the Galactic north pole lies at the intersections of the pixels :math:`\#1`, :math:`\#2`, :math:`\#3`, and :math:`\#4`, and the Galactic south pole between the pixels  :math:`\#N_{\rm pix}-3`, :math:`\#N_{\rm pix}-2`, :math:`\#N_{\rm pix}-1`, and :math:`\#N_{\rm pix}`. The Galactic *anti-center*, :math:`(\psi,\theta)_\oplus = (\pi,0)_\oplus` is found where the pixels :math:`\#N_{\rm pix}/2`, and :math:`\#N_{\rm pix}/2 + 1` touch. For :math:`N_{\rm side} = 2^n`, :program:`CLUMPY` choses the storage-efficient :keyword:`NESTED` scheme (:option:`gSIM_HEALPIX_SCHEME` = :keyword:`NESTED`) by default, if not explicitly overwritten by the user. Here, the pixel ordering scheme is more intricate, and the reader is referred to the :raw-html:`<A href="http://healpix.sourceforge.net/html/intronode4.htm" target="_blank">HEALPix documentation</A>` for further information.

Oversampled averaging
~~~~~~~~~~~~~~~~~~~~~~~~

In the :program:`FITS` files,  :program:`CLUMPY` provides the *J*-factor values with respect to the pixels' sizes in the map, :math:`\Delta\Omega_{\rm pix}` according to  Eq. :eq:`eq_pixelsize`. However, the value of :math:`J(\Delta\Omega_{\rm pix})` is based on an circular "oversampled averaging" over a region slightly larger than the pixel surface, as illustrated in :numref:`fig_healpix_oversampling`. This approach is much faster than calculating the exact signal contained in a quadrilateral :program:`HEALPix` pixel (for the expense of some accuracy) while it is more accurate, especially for very steep DM profiles, than simply calculating the line-of-sight integral at a single direction (e.g., the pixel center) and multiplying it with :math:`\Delta\Omega_{\rm pix}`.

.. note ::

   The :math:`\mathrm{d}J/\mathrm{d}\Omega` values given in the :program:`FITS` output rely on the same approach as decribed above and are effectively an averaged  :math:`\langle\mathrm{d}J/\mathrm{d}\Omega\rangle` over the circular regions in :numref:`fig_healpix_oversampling`. They are **not** simply the line-of-sight integral towards the  pixel centers (blue points in :numref:`fig_healpix_oversampling`).


.. _fig_healpix_oversampling:

.. figure:: DocImages/healpix_oversampling.png
   :align: center
   :figclass: align-center
   :width: 70%

   Illustration of the :program:`HEALPix` grid and :program:`CLUMPY`'s minimal oversampling in the :program:`HEALPix` scheme. Blue: pixel centers, pink: pixel boundaries, green: Integration area adapted to the :program:`HEALPix` resolution.



Field of view choices
~~~~~~~~~~~~~~~~~~~~~~~~~~


For 2D maps, different :abbr:`FOV (field of view)` shapes or masking shapes can be chosen. Either rectangular shapes (limited by great circles, i.e., the shortest connection between four given points on the sphere, "rectangular mode"), circular ones ("disk mode") and, for :abbr:`FOV (field of view)` centred in latitude in the Galactic plane, also limited by lines in constant longitude and latitude ("strip mode"). All figures below show skymaps of the Galactic DM emission obtained with  the :option:`-g5` option (i.e., no drawn substructures). 


Rectangular field of view
++++++++++++++++++++++++++++++++++++++

For a standard rectangular field of view, the centre of the field of view and its dimensions in diameter are specified via the parameters listed in :numref:`tab_2DFOVparameters`:

:raw-html:`<center>` 

.. _tab_2DFOVparameters:

.. table:: Parameters to define a 2D field of view
     
           ======================================================================================================= ==========================================================================
           **Quantities**                                                                                          **Parameter names**
           ------------------------------------------------------------------------------------------------------- --------------------------------------------------------------------------
           Field of view center, :math:`\psi_0` or :math:`l_0`, :math:`\rm [deg]`                                       :option:`gSIM_PSI_OBS_DEG`
           Field of view centre , :math:`\theta_0` or :math:`b_0`, :math:`\rm [deg]`                                     :option:`gSI_THETA_OBS_DEG`
           Field of view diameter :math:`\Delta\theta_\perp` in :math:`\theta_\perp` direction, :math:`\rm [deg]`  :option:`gSIM_THETA_ORTH_SIZE_DEG`
           Field of view diameter :math:`\Delta\theta` in :math:`\theta` direction, :math:`\rm [deg]`              :option:`gSIM_THETA_SIZE_DEG`
           ======================================================================================================= ==========================================================================

:raw-html:`</center>`   

.. note:: The field of view diameter :math:`\Delta\theta_\perp` only falls together with some :math:`\Delta\psi` for :math:`\theta_0 = 0^{\circ}`.

In the rectangular mode, the field of view is limited by *great circles*. By this, :math:`\Delta\theta` corresponds to the field of view diameter in latitudinal direction between the corners, while :math:`\Delta\theta_\perp` is the distance (orthogonal to the latitudinal direction) *through the skymap centre between the the field of view edges*:


.. _fig_FOVshapes_example3:

.. figure:: DocImages/FOVshapes_example3.png
   :align: center
   :figclass: align-center

   Rectangular field of view, limited by great circles, of dimension :math:`\,60^{\circ}\times 40^{\circ}` around the Galactic centre.

:numref:`fig_FOVshapes_example3` is obtained with:

.. code-block:: console

         $ clumpy -g5 --gSIM_PSI_OBS_DEG=0 --gSIM_THETA_OBS_DEG=0 --gSIM_THETA_ORTH_SIZE_DEG=60 --gSIM_THETA_SIZE_DEG=40 -i all_other_parameters.txt  

Note that the user has to create the :file:`all_other_parameters.txt` file with e.g. the :option:`-D`.  For further infos about how to create a figure like :numref:`fig_FOVshapes_example3` from the :program:`CLUMPY` output, see :numref:`rst_FITS_display` below.

.. note::

     For :math:`\Delta\theta_\perp=\Delta\theta \rightarrow 180^{\circ}`, the field of view in rectangular mode approaches a half-sphere (as shown in :numref:`fig_FOVshapes_example2`). It is not possible to draw a rectangular field of view larger than a half-sphere. 


Strip mode
++++++++++++++++++++++++++++++++++++++

The *strip mode* is a variant of the rectangular mode, in which the northern and southern field of view limits are given by *lines of constant latitude*. It is only available for field of views symmetric w.r.t. the Galactic equator (:math:`\theta_0 \equiv 0^{\circ}`) and triggered by setting :option:`gSI_THETA_OBS_DEG = s`. In contrast, the central position in longitudinal coordinates, :math:`\psi_0`,  can be freely chosen. For example, 

.. code-block:: console

         $ clumpy -g5 --gSIM_PSI_OBS_DEG=0 --gSIM_THETA_OBS_DEG=s --gSIM_THETA_ORTH_SIZE_DEG=60 --gSIM_THETA_SIZE_DEG=40 -i all_other_parameters.txt 

creates a skymap in a field of view as shown in :numref:`fig_FOVshapes_example4`.

.. _fig_FOVshapes_example4:

.. figure:: DocImages/FOVshapes_example4.png
   :align: center
   :figclass: align-center

   Strip-mode field of view of dimension :math:`\,60^{\circ}\times 40^{\circ}` around the Galactic centre.

The strip mode can be used to draw maps of the full sky, see `Fullsky maps`_.




Circular field of view
++++++++++++++++++++++++++++++++++++++

By setting :option:`gSIM_THETA_ORTH_SIZE_DEG = d`, a *circular/disk-shaped* field of view is chosen, and the parameter :option:`gSIM_THETA_SIZE_DEG` now corressponds to the **diameter** of the field of view. For example, :numref:`fig_FOVshapes_example2` shows a circular field of view with :option:`gSIM_THETA_SIZE_DEG = 180`:

.. _fig_FOVshapes_example2:

.. figure:: DocImages/FOVshapes_example2.png
   :align: center
   :figclass: align-center

   Circular field of view around the Galactic centre with diameter of :math:`180^{\circ}`. Note that this is also the limit of a **rectangular** field of view on the sphere and its diameters approaching :math:`180^{\circ}`.

:numref:`fig_FOVshapes_example2` is obtained with the command:

.. code-block:: console

         $ clumpy -g5 --gSIM_PSI_OBS_DEG=0 --gSIM_THETA_OBS_DEG=0 --gSIM_THETA_ORTH_SIZE_DEG=d --gSIM_THETA_SIZE_DEG=180 -i all_other_parameters.txt 


The circular field of view diameter can be as large as the full sky, see `Fullsky maps`_.

Fullsky maps 
++++++++++++++++++++++++++++++++++++++

To compute a fullsky map, either the disk mode :keyword:`d` or the strip mode :keyword:`s` can be used. Both options yield the same result. For example:

.. code-block:: console

    $ clumpy -g5 --gSIM_PSI_OBS_DEG=0 --gSIM_THETA_OBS_DEG=0 --gSIM_THETA_ORTH_SIZE_DEG=d --gSIM_THETA_SIZE_DEG=360 -i all_other_parameters.txt 
    
    
results into a fullsky map, as shown in :numref:`fig_FOVshapes_example1`. 

.. _fig_FOVshapes_example1:

.. figure:: DocImages/FOVshapes_example1.png
   :align: center
   :figclass: align-center

   Fullsky map of the Galactic DM halo.


For such a fullsky field of view, the :math:`(l,b)` position of the field-of-view centre is irrelevant (any choices results into the exactly same skymap). The fullsky mode can also be triggered with the *strip mode* :

.. code-block:: console

     $ clumpy -g5 --gSIM_PSI_OBS_DEG=0 --gSIM_THETA_OBS_DEG=s --gSIM_THETA_ORTH_SIZE_DEG=360 --gSIM_THETA_SIZE_DEG=180 -i all_other_parameters.txt 

Again, for a fullsky field of view, the longitudinal position of the field of view centre is irrelevant.



Masking
++++++++

:program:`CLUMPY` accepts negative values for the field of view dimensions to trigger masking. Negative values result in subtraction of this field of view from a fullsky map. In the following, we give in short some command examples with the resulting skymaps:

.. code-block:: console

    $ clumpy -g5 --gSIM_PSI_OBS_DEG=0 --gSIM_THETA_OBS_DEG=s --gSIM_THETA_ORTH_SIZE_DEG=360 --gSIM_THETA_SIZE_DEG=-30 -i all_other_parameters.txt 

.. _fig_FOVshapes_example6:

.. figure:: DocImages/FOVshapes_example6.png
   :align: center
   :figclass: align-center

   Masking the Galactic plane with the strip mode.



.. code-block:: console

    $ clumpy -g5 --gSIM_PSI_OBS_DEG=0 --gSIM_THETA_OBS_DEG=s --gSIM_THETA_ORTH_SIZE_DEG=60 --gSIM_THETA_SIZE_DEG=-40 -i all_other_parameters.txt 

.. _fig_FOVshapes_example7:

.. figure:: DocImages/FOVshapes_example7.png
   :align: center
   :figclass: align-center

   Masking the Galactic centre with the strip mode.



.. code-block:: console

    $ clumpy -g5 --gSIM_PSI_OBS_DEG=0 --gSIM_THETA_OBS_DEG=0 --gSIM_THETA_ORTH_SIZE_DEG=d --gSIM_THETA_SIZE_DEG=-30 -i all_other_parameters.txt 


.. _fig_FOVshapes_example5:

.. figure:: DocImages/FOVshapes_example5.png
   :align: center
   :figclass: align-center

   Masking the Galactic centre with a circular mask.



.. note:: Masking is worth to **speed up the computation time**, as :program:`CLUMPY` calculates only the pixel values outside the mask. However, choosing a mask smaller than a half-sphere **increases the output file size**, as explicit :program:`HEALPix` pixel labeling must be done to define the location of the unmasked pixels on the sphere.







.. _rst_FITS_display:

Accessing the :program:`FITS` output
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Viewing the :program:`FITS` files
+++++++++++++++++++++++++++++++++++

:program:`CLUMPY`'s 2D skymaps are stored in the :raw-html:`<A href="http://fits.gsfc.nasa.gov/" target="_blank">FITS</A>` (Flexible Image Transport System) file format :raw-html:`<A href="http://adsabs.harvard.edu/abs/2010A%26A...524A..42P" target="_blank">(Pence et al., 2010)</A>`.  This format allows to clearly and efficiently store several maps as binary tables in one single file, and to append extensive meta information in the :program:`FITS` headers of the output file. Both fullsky (implicit :program:`HEALPix` pixel labeling) and part-sky maps (explicit pixel labeling) are handled in the :program:`FITS` format.

:program:`CLUMPY` skymap :program:`FITS` files are composed of several extensions (*J*-factor skymaps, smoothed *J*-factor skymaps, intensities,...; see also :ref:`rst_doc_modules`, :ref:`rst_doc_module_f` :option:`-f` for :program:`CLUMPY`'s feature of adding extra extensions to a :program:`FITS` file), each containing various columns (e.g., :math:`J_{\rm sm}`, :math:`J_{\rm subs}`, :math:`J_{\rm cross-prod}`, etc.). Each extension possesses its own :program:`FITS` header, containing the relevant input parameters of the performed simulation.



The following two programs are able to directly read and display these :program:`FITS` files (however, only for :math:`N_{\rm side} = 2^n`):


- :raw-html:`<b><a href=http://aladin.u-strasbg.fr target="_blank">Aladin</a></b>` **sky atlas** 


  The :program:`Aladin sky atlas` is a nice viewer which comes with a lot of neat features: Switch between various interactive projections, switch coordinate systems, overlay various maps from different files. It has an interface to various **survey skymaps** along the electromagnetic spectrum (DSS, XMM Newton, Fermi) to easily overlay with your input skymap. See the :ref:`rst_gallery` for some example. :raw-html:`<br><br>`

- The Python module :raw-html:`<b><a href=http://healpy.readthedocs.io target="_blank">healpy</a></b>` (part of the :program:`HEALPix` package) is able to directly read any column of any extension in the :program:`FITS` output into numpy arrays with simple one-line commands


Other :program:`FITS` viewers are also able to display skymaps stored in :program:`FITS` files and :program:`HEALPix` pixelisation format, but only for *fullsky maps in single extension, single column files*. :program:`CLUMPY` comes with an internal postprocessing module to extract single columns from a :program:`FITS` extension (e.g. "the integrated intensity of Galactic DM of only resolved substructure") and to store them in a fullsky :program:`HEALPix`-:program:`FITS` file. This is explained in :ref:`rst_doc_modules`, :numref:`rst_doc_module_o`, using the :option:`-o2` option.

The following two viewers are supported to display single-extension 2D fullsky maps in the :program:`HEALPix` scheme (including the aforementioned viewers):


- :raw-html:`<b><a href=http://ds9.si.edu target="_blank">ds9</a></b>`  supports :program:`HEALPix` format from version 7 on.

  In :program:`ds9`, you can switch coordinate systems and display horizontal and vertical  1D-profiles.
  
  .. note:: To get the right colour scale for part-sky maps in :program:`ds9`, go to :menuselection:`Scale --> Scale Parameters` (at the very bottom) and raise the lower limit to get an appropriate contrast.

- `Lambda skyviewer <http://lambda.gsfc.nasa.gov/toolbox/tb_skyviewer_ov.cfm>`__.

In addition to those :program:`FITS` viewers,

- `fv (fits viewer) <http://heasarc.gsfc.nasa.gov/ftools/fv/>`__

can be used to browse the extensions, columns, and their numeric data of a :program:`FITS` file and to display the :program:`FITS` headers. :program:`fv` is also able to delete extensions from a file, which may be useful after the procedure from :ref:`rst_doc_modules`, :ref:`rst_doc_module_f` :option:`-f` has been applied to add various intensity extensions to a file.

.. _rst_FITS_to_ASCII:

Conversion to ASCII
+++++++++++++++++++++

Also, :program:`FITS` extensions can be converted into **ASCII files**. This is further explained in :ref:`rst_doc_modules`, :numref:`rst_doc_module_o`, using the :option:`-o1` option.




Conversion to projected :program:`FITS` images
++++++++++++++++++++++++++++++++++++++++++++++

:program:`CLUMPY` comes with a Python script which, based on (and requiring) healpy and astropy, converts the skymaps saved in the :program:`HEALPix` pixelisation scheme into projected :program:`FITS` images. These images can also be displayed with all above :program:`FITS` viewers (including :program:`fv`).


The conversion to :program:`FITS` images is further explained in the :ref:`rst_physics_instruments` section.

.. note:: The conversion into projected :program:`FITS` images is degrading the original information.




Restoring an input parameter file out of the FITS metadata
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The :option:`-o3` option can be used to generate back the :program:`CLUMPY` input parameter file out of the header of an output :program:`FITS` file extension:

   .. code-block:: console
   
        $ clumpy -o3 -i results.fits 1

creates an input parameter file out of the metadata stored in the header of the first extension of the file :file:`results.fits`.

