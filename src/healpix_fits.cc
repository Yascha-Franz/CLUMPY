/*! \file healpix_fits.cc \brief (see healpix_fits.h) */

// CLUMPY includes
#include "../include/clumps.h"
#include "../include/healpix_fits.h"
#include "../include/geometry.h"
#include "../include/inlines.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/spectra.h"
#include "../include/janalysis.h"

// HEALPIX includes
#include <alm.h>
#include <alm_healpix_tools.h>
#include <alm_powspec_tools.h>
//#include <arr.h>
//#include <datatypes.h>
//#include <fitshandle.h>
//#include <healpix_base.h>
//#include <healpix_data_io.h>
//#include <healpix_map.h>
//#include <lsconstants.h>
//#include <powspec.h>
//#include <powspec_fitsio.h>
//#include <xcomplex.h>
#include <rotmatrix.h>
//#include <vec3.h>

// C++ std libraries
using namespace std;
#include <fstream>
#include <string_utils.h>
#include <string.h>
#include "fitsio.h"
#include "unistd.h"

//______________________________________________________________________________
void add_halo_in_map(double par_tot[11], double const &eps, vector<double> &j_map, double const j_bkd,
                     rangeset<int> &rs_pix_fov, bool is_subs, double const &mtot,
                     double const &f_dm, double par_dpdv[10], double par_subs[25], double ntot_subs, int is_sz)
{

   //--- In an existing 2D skymap[n_psi][n_theta], calculates and adds the contribution
   //    of a drawn spherically symmetric sub-clump.
   //    Most of the time, the sub-clump is not point-like. Moreover,
   //    the sub-clumps adds up to an existing contribution (smooth or unresolved sub-cl).
   //    To optimise the calculation time, we only calculate clumps that outshine
   //    a fraction of j_bkd. The strategy is to start with the clump centre
   //    and then look in adjacent pixels and stop when the condition is met.
   //
   //  par_tot[0]     rho_tot host normalisation [Msol kpc^{-3}]
   //  par_tot[1]     rho_tot host scale radius [kpc]
   //  par_tot[2]     rho_tot host shape parameter #1
   //  par_tot[3]     rho_tot host shape parameter #2
   //  par_tot[4]     rho_tot host shape parameter #3
   //  par_tot[5]     rho_tot host card_profile [gENUM_PROFILE]
   //  par_tot[6]     rho_tot host radius [kpc]
   //  par_tot[7]     l_HC: distance observer to host centre [kpc]
   //  par_tot[8]     psi_HC: longitude of host centre [rad]
   //  par_tot[9]     theta_HC: latitude of host centre [rad]
   //  par_tot[10]    z: redshift of the halo
   //  eps            Relative precision sought for integrations
   //  j_map          2D array[n_psi*n_theta] of j values for halo
   //  j_bkd          Value of background towards halo direction (e.g. Gal. + <galsubs>...)
   //  rs_pix_fov     rangeset containing the healpix pixel numbers of the FOV
   //  is_subs        Add contribution <subs> from current host halo in j_map
   //  mtot           Total mass of host halo
   //  f_dm           Fraction of the halo mass in subs
   //  par_dpdv[0]    dPdV_Host normalisation  [kpc^{-3}]
   //  par_dpdv[1]    dPdV_Host scale radius [kpc]
   //  par_dpdv[2]    dPdV_Host shape parameter #1
   //  par_dpdv[3]    dPdV_Host shape parameter #2
   //  par_dpdv[4]    dPdV_Host shape parameter #3
   //  par_dpdv[5]    dPdV_Host card_profile [gENUM_PROFILE]
   //  par_dpdv[6]    dPdV_Host radius [kpc]
   //  par_dpdv[7]    l_HC: distance observer to Host Centre [kpc]
   //  par_dpdv[8]    psi_HC: longitude of dpdv [rad]
   //  par_dpdv[9]    theta_HC: latitude of dpdv [rad]
   //  par_subs[0]    rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_subs[1]    rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_subs[2]    rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_subs[3]    rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]    host halo outer radius where to stop integration [kpc]
   //  par_subs[5]    eps: relative precision sought L calculation
   //  par_subs[6]    z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]    mdelta: mass of rho_cl(r)
   //  par_subs[8]    dPdM normalisation [1/Msol]
   //  par_subs[9]    dPdM slope alphaM
   //  par_subs[10]   dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)[UNUSED for DECAY]
   //  par_subs[11]   dPdc mean concentration <c>            [UNUSED for DECAY]
   //  par_subs[12]   dPdc standard deviation                [UNUSED for DECAY]
   //  par_subs[13]   dPdc card_profile [gENUM_CDELTA_DIST]  [UNUSED for DECAY]
   //  par_subs[14]   f: mass fraction of substructures      [UNUSED for DECAY]
   //  par_subs[15]   nlevel: sub-sub...halos (1=no sub)     [UNUSED for DECAY]
   //  par_subs[16]   dPdV normalisation  [kpc^{-3}]         [UNUSED for DECAY]
   //  par_subs[17]   dPdV scale radius [kpc]                [UNUSED for DECAY]
   //  par_subs[18]   dPdV shape parameter #1                [UNUSED for DECAY]
   //  par_subs[19]   dPdV shape parameter #2                [UNUSED for DECAY]
   //  par_subs[20]   dPdV shape parameter #3                [UNUSED for DECAY]
   //  par_subs[21]   dPdV card_profile [gENUM_PROFILE]      [UNUSED for DECAY]
   //  par_subs[22]   host halo outer radius (unused)        [UNUSED for DECAY]
   //  par_subs[23]   ratio rs dPdV to rs_cl                 [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]   R_host (corresp. to current dPdV)      [only for ANNIHILATION and nlevel>1]
   //  ntot_subs      Total number of subs in host halo

   double z = par_tot[10];
   // set alpha_int to value corresponding to observation of proper coordinates
   // (correct for angular diameter distortion)
   double alphaint_orig = gSIM_ALPHAINT;
   if (z > ZMIN_EXTRAGAL) gSIM_ALPHAINT = atan(1. / (1 + z) * MY_TAN(gSIM_ALPHAINT));

   // create vector here again from rangeset (instead of parsing it directly into this
   // function, because rangeset is also needed in this function for fast calculations.)
   vector<int> v_pix_fov;
   rs_pix_fov.toVector(v_pix_fov);
   T_Healpix_Base<int> hp_gridprop(gSIM_HEALPIX_NSIDE, gSIM_HEALPIX_SCHEME, SET_NSIDE) ;

   double psi_cl = par_tot[8] ;
   double theta_cl = par_tot[9] ;

   pointing ptg_cl(PI / 2 - theta_cl, psi_cl) ;
   vec3_t<double> vec3_cl = ptg_cl.to_vec3();
   vec3_t<double> vec3_pix;

   // find the closest Pixel to the clump centre
   int i_pix_closest = hp_gridprop.ang2pix(ptg_cl) ;
   pointing ptg_i_pix_closest = hp_gridprop.pix2ang(i_pix_closest);
   double psi_i_pix_closest = ptg_i_pix_closest.phi;
   check_psi(psi_i_pix_closest);
   double theta_i_pix_closest = PI / 2 - ptg_i_pix_closest.theta;

   //--- return if outside of the map
   if (rs_pix_fov.contains(i_pix_closest) == false) {
      return;
   }

   // Need to find all the pixels that are concerned with this clump.
   // Therefore, search the angular distance from the clump center, where
   // J_cl_in_pixel >= continuous_background * min(eps,0.01). As the J-factor contribution
   // from the clumps is spherically symmetric and decreases monotonously from
   // the clumps center, all the pixels within this radius are concerned.
   // Do the search along the theta-direction, as it is a great-circle path.
   // N.B.: all clumps are drawn at least within 2*SIMU_ALPHAINT. It turns out
   // that most of the clumps fall below above threshold continuous_background * 0.01
   // within this radius, so even if one increases (e.g, 0.01 -> 0.1) the threshold,
   // it won't change much for most of the clumps (but one will note the discontinuity
   // between the drawn clumps and the background for the few large clumps).


   rangeset<int> rs_pix_clump ;
   double *j_1D = NULL;
   vector<double> phi_tab;
   // Array for interpolations
   vector<double> j_1D_base ;
   vector<double> phi_base ;

   // intervals along the 1D-clump profile to be calculated:
   // these values turned out to give sufficient accuracy with sufficient speed.
   // Possibly good to make them dependent on gSIM_EPS.
   double delta_phi = gSIM_RESOLUTION / 10. ; // might slow down calculation: (10. * log10(1./gSIM_EPS)) ;
   double alpha_scale =  asin(par_tot[1] / par_tot[7]);
   double delta_phi_base = min(gSIM_RESOLUTION / 20., alpha_scale / 20.) ;
   // N.B: It might appear strange that delta_phi_base is smaller than delta_phi, but
   // - delta_phi_base is not constant and will be increased (see below).
   // - this choice has turned out to yield accurate results and calculates still
   //   quite fast. If you change it, make sure that it satisfies your needs for
   //   accuracy!
   double clump_outshine_radius = - delta_phi_base; // initial value will then be zero in loop.
   double max_rad; // maximum l.o.s distance from the clump center to integrate
   if (par_tot[6] < par_tot[7]) {
      max_rad = asin(par_tot[6] / par_tot[7]);
      if (max_rad <= gSIM_ALPHAINT) max_rad = gSIM_ALPHAINT / 2.;
      else max_rad -= SMALL_NUMBER; //check if this fix is necessary to avoid simpson_log/lin_adapt errors
   } else max_rad = PI - SMALL_NUMBER;

   double sign = 1. ;
   if (theta_i_pix_closest > 0) sign = -1. ; // don't crash by creating |theta| > PI/2, so always go towards the equator!

   double j_current;
   if (is_sz == 0) { // we're dealing with an DM annihilation/decay map
      // only needed for is_subs:
      double mmax = gDM_SUBS_MMAXFRAC * mtot;
      double l1 = max(0., par_tot[7] - par_tot[6]);
      double l2 = par_tot[7] + par_tot[6];
      double condition = j_bkd * gSIM_EPS;

      // find clump radius by going from the clump center towards the Galactic equator.
      j_current = 1.e40 ;
      while (j_current >= condition || clump_outshine_radius < 2. * gSIM_ALPHAINT + gSIM_RESOLUTION) {  // all clumps are drawn at least within 2*SIMU_ALPHAINT
         clump_outshine_radius += delta_phi_base;
         if (clump_outshine_radius >= max_rad) break;
         // some log-like enlargement of the intervals:
         delta_phi_base *= 1.2; // <- the closer to 1., the better; but calculation will get slower...
         if (f_dm > 1.e-3 && is_subs)
            j_current = jsmooth_mix(mtot, par_tot, psi_cl, theta_cl + sign * clump_outshine_radius, l1, l2, eps, f_dm, par_dpdv);
         else
            j_current = jsmooth(par_tot, psi_cl, theta_cl + sign * clump_outshine_radius, eps);
         if (is_subs) {
            j_current += jsub_continuum(ntot_subs, par_dpdv, psi_cl, theta_cl + sign * clump_outshine_radius, l1, l2, par_subs, gDM_SUBS_MMIN, mmax);
            if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
               j_current  += jcrossprod_continuum(mtot, par_tot, psi_cl, theta_cl + sign * clump_outshine_radius, eps, f_dm, par_dpdv);
            }
         }
         if (j_current < 1.e-40) j_current = 1.e-40;
         j_1D_base.push_back(j_current);
         phi_base.push_back(clump_outshine_radius);
      }
      // draw an extra interval:
      if (clump_outshine_radius >= max_rad) clump_outshine_radius = max_rad ;
      else clump_outshine_radius += delta_phi_base;
      if (f_dm > 1.e-3 && is_subs)
         j_current = jsmooth_mix(mtot, par_tot, psi_cl, theta_cl + sign * clump_outshine_radius, l1, l2, eps, f_dm, par_dpdv);
      else
         j_current = jsmooth(par_tot, psi_cl, theta_cl + sign * clump_outshine_radius, eps);
      if (is_subs) {
         j_current += jsub_continuum(ntot_subs, par_dpdv, psi_cl, theta_cl + sign * clump_outshine_radius, l1, l2, par_subs, gDM_SUBS_MMIN, mmax);
         if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
            j_current  += jcrossprod_continuum(mtot, par_tot, psi_cl, theta_cl + sign * clump_outshine_radius, eps, f_dm, par_dpdv);
         }
      }
   } else { // we're dealing with an SZ map

      // double condition = j_bkd;

      // // find clump radius by going from the clump center towards the Galactic equator.
      // j_current = 1.e40 ;
      // while (j_current > condition){// || clump_outshine_radius < 2. * gSIM_ALPHAINT + gSIM_RESOLUTION) {
      //   // all clumps are drawn at least within 2*SIMU_ALPHAINT
      //   clump_outshine_radius += delta_phi_base;
      //   if (clump_outshine_radius >= max_rad) break;
      //   // some log-like enlargement of the intervals:
      //   delta_phi_base *= 1.3; // <- the closer to 1., the better; but calculation will get slower...

      //   j_current = sz_ysmooth(par_tot, psi_cl, theta_cl + sign * clump_outshine_radius, eps);
      //   if (j_current < 1.e-40) j_current = 1.e-40;
      //   j_1D_base.push_back(j_current);
      //   phi_base.push_back(clump_outshine_radius);
      // }
      // // draw an extra interval:
      // if (clump_outshine_radius >= max_rad) clump_outshine_radius = max_rad ;
      // else clump_outshine_radius += delta_phi_base;
      // j_current = sz_ysmooth(par_tot, psi_cl, theta_cl + sign * clump_outshine_radius, eps);


      //double sigma_beam = gSIM_GAUSSBEAM_SZ_FWHM * 180 * 60. / 2.3548 / PI; // arcmin
      int N;
      if (is_sz == -1) {
         N = 100;
         //cout << "Halo is point-like"<< endl;
      } else {
         N = is_sz;
         //cout << "Performing convolution with Npix = "<<is_sz << endl;
      }
      vector<vector<double> > y1d(2, vector<double>(N / 2));

      double theta_cl_ref = par_tot[8];
      double ps_cl_ref = par_tot[9];
      par_tot[8] = 0.;
      par_tot[9] = 0.;
      //y_convolved1d(par_tot, sigma_beam, is_sz, y1d);
      par_tot[8] = theta_cl_ref;
      par_tot[9] = ps_cl_ref;


      for (int j = 0; j < (int)y1d[0].size(); j++) {
         j_1D_base.push_back(y1d[1][j]);
         phi_base.push_back(y1d[0][j]*PI / (60.*180.));
      }
      j_current = y1d[1][y1d[0].size() - 1];
      clump_outshine_radius = y1d[0][y1d[0].size() - 1] * PI / (60.*180.);
      y1d.clear();
   }

   if (j_current < 1.e-40) j_current = 1.e-40;
   j_1D_base.push_back(j_current);
   phi_base.push_back(clump_outshine_radius);

   phi_base[0] = 0.; // be sure that it is really zero.

   // now allocate 1D clump profile:
   clump_outshine_radius -= SMALL_NUMBER;
   int n_1D = int(ceil(clump_outshine_radius / delta_phi)) + 1 ;
   delta_phi = clump_outshine_radius / double(n_1D - 1.) ; // corrected for the ceil()
   for (int i = 0; i < n_1D; ++i) phi_tab.push_back(i * delta_phi);
   j_1D = new double[n_1D];

   // fill 1D-profile by lin-log interpolating between values alreay calculated for finding clump_outshine_radius
   for (int i = 0; i < n_1D; ++i) {
      if (phi_tab[i] >= phi_base.back()) {
         print_warning("healpix_fits.cc", "add_halo_in_map()", "phi_tab value exceeding interpolation range.");
         continue;
      }

      j_1D[i] = interp1D(phi_tab[i], phi_base, j_1D_base, kLINLOG);

      if (std::isinf((float)j_1D[i]) || isnan(float(j_1D[i]))) {
         j_1D[i] = 1.e-40;
         print_warning("healpix_fits.cc", "add_halo_in_map()", "Found nan-value, replaced with 1e-40.");
      }
   }

   //cout << j_1D_base[0]<<" " << j_1D[0]<< " " << j_1D[10]<<endl;;

   // find all pixels in disk with radius clump_outshine_radius around the clump center ptg_cl:
   hp_gridprop.query_disc(ptg_cl, clump_outshine_radius, rs_pix_clump);

   // intersect clump pixels with FOV pixels:
   rs_pix_clump = rs_pix_clump.op_and(rs_pix_fov) ;

   vector<int> v_pix_clump;
   rs_pix_clump.toVector(v_pix_clump) ;
   int index_j_map ;
   pointing ptg_i_pix ;
   // draw the clump!
   //cout << "Number of pix in cluster = " << rs_pix_clump.nval()<<endl;
   for (int i = 0; i < rs_pix_clump.nval(); ++i) {
      int i_pix = v_pix_clump[i] ;
      vec3_pix  = hp_gridprop.pix2vec(i_pix);
      double phi_pix = hp_angular_dist(vec3_cl, vec3_pix);
      if (phi_pix >= phi_tab.back()) continue;

      int i1D = binary_search(phi_tab, phi_pix);
      // look for closest value:
      if (i1D < int(phi_tab.size()) and phi_pix - phi_tab[i1D] > phi_tab[i1D + 1] - phi_pix) i1D++;
      index_j_map = binary_search(v_pix_fov, i_pix);
      j_map[index_j_map] += j_1D[i1D];
   }

   // Free memory
   if (j_1D) delete[] j_1D;
   j_1D = NULL;

   gSIM_ALPHAINT = alphaint_orig;
   return;
}


//______________________________________________________________________________
void do_safegridextension(const double &dtheta_orth, const double &dtheta, const double &extend_dist,
                          string &grid_mode, double &dtheta_orth_extended,
                          double &dtheta_extended)
{
   //--- Extends a Clumpy Healpix grid safely - by taking into account the inverse
   //    mode as well as the fact that the surface of the sphere is limited.
   // INPUTS:
   //  dtheta_orth  size of FOV in great-circle direction orthogonal to theta axis, in rad.
   //  dtheta       size of FOV on theta axis, in rad.
   //  extend_dist  distance in rad the grid is extended in dtheta_orth and dtheta direction
   // Input/Output:
   //  grid_mode    grid mode of the grid (RECT/STRIP/DISK). In one case, it is changed
   //               from RECT to DISK
   // OUTPUTS:
   //  dtheta_orth_extended  extended size of FOV in great-circle direction orthogonal to theta axis, in rad.
   //  dtheta _extended      extended size of FOV on theta axis, in rad.

   // look for dtheta:

   if (fabs(dtheta) >= PI - extend_dist) {
      // extended grid would expoit the whole theta coordiate range
      if (grid_mode == "STRIP") {
         if (dtheta > 0) dtheta_extended = PI ;
         if (dtheta < 0) dtheta_extended = - PI + extend_dist ;  // assure inverse mode.
      } else if (grid_mode == "RECT") {
         // this case only happens for abs(dtheta_orth), abs(dtheta) close to PI -> limit of hemisphere:
         grid_mode = "DISK" ;
         dtheta_extended = dtheta + extend_dist; // inverse mode: abs(value) is getting smaller, as supposed to be!
      } else if (grid_mode == "DISK" && fabs(dtheta) >= 2 * PI - extend_dist) {
         // for DISK mode, dtheta > PI is not a problem, but approaching dtheta = 2 * PI is:
         if (dtheta > 0) dtheta_extended = 2 * PI ; // limit of full sphere
         if (dtheta < 0) dtheta_extended = - 2 * PI + extend_dist ;  // assure inverse mode.
      } else dtheta_extended = dtheta + extend_dist;
   } else {
      dtheta_extended = dtheta + extend_dist ; // inverse mode: abs(value) is getting smaller, as supposed to be!
      if (dtheta < 0 && fabs(dtheta) < extend_dist) {
         // extended grid is full sky!
         grid_mode = "DISK";
         dtheta_extended = 2 * PI ;
      }
   }

   // look for dtheta_orth - for DISK mode this is irrelevant, because it is not used at all.
   if ((grid_mode == "RECT" && fabs(dtheta_orth) >= PI - extend_dist) || (grid_mode == "STRIP" && fabs(dtheta_orth) >= 2 * PI - extend_dist)) {
      if (grid_mode == "RECT") {
         // limit of hemisphere:
         grid_mode = "DISK" ;
         dtheta_extended = dtheta_orth + extend_dist; // make disk with diameter dtheta_orth_extended.
      } else if (grid_mode == "STRIP") dtheta_orth_extended = 2 * PI; // limit of full sphere

      if (dtheta_orth < 0) dtheta_orth_extended = -fabs(dtheta_orth_extended) ; // make sure that inverse mode is not broken by positive dtheta_orth_extended.
   } else {
      dtheta_orth_extended = dtheta_orth + extend_dist ;
      if (dtheta_orth < 0 && fabs(dtheta_orth) < extend_dist) {
         // extended grid is full sky!
         grid_mode = "DISK";
         dtheta_orth_extended = 2 * PI ;
      }
   }
   // make dtheta_orth_extended = dtheta_extended for DISK mode:
   if (grid_mode == "DISK") dtheta_orth_extended = dtheta_extended;

   return;
}

////______________________________________________________________________________
void fits_append_fluxextension(string &filename)
{
   //--- Appends an additional extension to the FITS file 'filename' and writes
   //    flux (and intensity) maps into it. All neccessary information about the J-factors
   //    is taken from the FITS headers, and not from the parameter file. Only
   //    the (new) information about the particle physics model is taken from
   //    the global parameters initialized from the parameter file.

   char char_tmp[512];

   ifstream isfile(filename.c_str());
   if (!isfile) {
      printf("\n====> ERROR: fits_append_fluxextension() in healpix_fits.cc");
      printf("\n             File %s does not exist.", filename.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }

   fitshandle fh_file ;
   fh_file.open(filename);

   string extname_str ;
   string fieldname_str;
   string fieldunits_str;
   string extname_str_trim ;
   string fieldname_str_trim;
   int n_side_read ;
   Healpix_Ordering_Scheme hp_scheme;

   int n_side_jfactor = -1;
   int n_side_jsmoothed = -1;

   int n_pix_fov_read;
   int n_pix_fov_jfactor = -1;
   int n_pix_fov_jsmoothed = -1;
   int n_maps_read ;
   int i_extension = 1;
   Healpix_Ordering_Scheme hp_scheme_jfactor = RING;
   Healpix_Ordering_Scheme hp_scheme_jsmoothed = RING;
   string fieldunits_str_jfactor;
   string fieldunits_str_jsmoothed;

   int i_ext_jfactor = -1;
   int i_ext_jsmoothed = -1;
   int i_ext_headerdata;
   int i_field_jfactor = -1;
   int i_field_jsmoothed = -1;
   int i_field_jsmoothed_gamma = -1;
   int i_field_jsmoothed_nu = -1;

   cout << ">>>>> read FITS file " << filename << " ..."  << endl;
   int n_extensions = fits_read_nextensions(fh_file);

   for (i_extension = 1; i_extension < n_extensions + 1; ++i_extension) {
      int i_field = 0 ;
      fits_read_fieldinfo(fh_file, i_extension, i_field, extname_str, fieldname_str, fieldunits_str) ;
      extname_str_trim = removeblanks_from_startstop(string_fixlength(extname_str, 20)) ;
      if (extname_str_trim == "JFACTOR" or extname_str_trim == "DFACTOR") {
         cout << "      ... found extension containing unsmoothed J-factors" << endl;
         if (i_ext_jfactor != -1) {
            print_warning("healpix_fits.cc", "fits_append_fluxextension()",
                          "There is more than one J-Factor extension in this file. Values are extracted from the last one.");
         }
         i_ext_jfactor = i_extension;
         n_maps_read = fits_read_ncolumns(fh_file, i_extension, n_side_read, hp_scheme, n_pix_fov_read);
         hp_scheme_jfactor = hp_scheme;
         n_side_jfactor = n_side_read;
         n_pix_fov_jfactor = n_pix_fov_read;

         for (i_field = 0; i_field <= n_maps_read; ++i_field) {
            fits_read_fieldinfo(fh_file, i_extension, i_field, extname_str, fieldname_str, fieldunits_str) ;
            fieldname_str_trim = removeblanks_from_startstop(string_fixlength(fieldname_str, 20)) ;
            if (fieldname_str_trim == "Jtot" || fieldname_str_trim == "Dtot") {
               cout << "      ... found correct map " << fieldname_str_trim << " in it ..." << endl;
               i_field_jfactor = i_field;
               fieldunits_str_jfactor = removeblanks_from_startstop(string_fixlength(fieldunits_str, 20)) ;
               break;
            }
         }

      } else if (extname_str_trim == "JTOT_SMOOTHED" or extname_str_trim == "DFACTOR_SMOOTHED") {
         cout << "      ... found extension containing J-factors smoothed by instrumental beam" << endl;
         if (i_ext_jsmoothed != -1) {
            print_warning("healpix_fits.cc", "fits_append_fluxextension()",
                          "There is more than one J-Factor extension in this file. Values are extracted from the last one.");
         }
         i_ext_jsmoothed = i_extension;
         n_maps_read = fits_read_ncolumns(fh_file, i_extension, n_side_read, hp_scheme, n_pix_fov_read);
         hp_scheme_jsmoothed = hp_scheme;
         n_side_jsmoothed = n_side_read;
         n_pix_fov_jsmoothed = n_pix_fov_read;

         for (i_field = 0; i_field <= n_maps_read; ++i_field) {
            fits_read_fieldinfo(fh_file, i_extension, i_field, extname_str, fieldname_str, fieldunits_str) ;
            fieldname_str_trim = removeblanks_from_startstop(string_fixlength(fieldname_str, 20)) ;
            if (fieldname_str_trim == "Jtot" || fieldname_str_trim == "Dtot") {
               cout << "      ... found single map " << fieldname_str_trim << " in it ..." << endl;
               i_field_jsmoothed = i_field;
               fieldunits_str_jsmoothed = removeblanks_from_startstop(string_fixlength(fieldunits_str, 20)) ;
               break;
            } else if (fieldname_str_trim == "Jtot_gamma" || fieldname_str_trim == "Dtot_gamma") {
               cout << "      ... found map " << fieldname_str_trim << " (smoothed by Gamma instrument's beam) in it ..." << endl;
               i_field_jsmoothed_gamma = i_field;
               fieldunits_str_jsmoothed = removeblanks_from_startstop(string_fixlength(fieldunits_str, 20)) ;
            } else if (fieldname_str_trim == "Jtot_neutrino" || fieldname_str_trim == "Dtot_neutrino") {
               cout << "      ... found map " << fieldname_str_trim << " (smoothed by Neutrino instrument's beam) in it ..." << endl;
               i_field_jsmoothed_nu = i_field;
               fieldunits_str_jsmoothed = removeblanks_from_startstop(string_fixlength(fieldunits_str, 20)) ;
            }
         }
      }
   }
   // check if Jfactor extension has been found:
   if (i_field_jfactor == -1) {
      printf("\n====> ERROR: fits_append_fluxextension() in healpix_fits.cc");
      printf("\n             No matching Jfactor map found in file");
      printf("\n             => abort()\n\n");
      abort();
   }
   // check for consistency between the extensions when smoothed data is present:
   else if (i_ext_jsmoothed != -1) {
      if (n_side_jsmoothed != n_side_jfactor) {
         printf("\n====> ERROR: fits_append_fluxextension() in healpix_fits.cc");
         printf("\n             FITS extensions of unsmoothed and smoothed data don't fit together:");
         printf("\n             NSIDE (unsmoothed) = %d, NSIDE (smoothed) = %d", n_side_jfactor, n_side_jsmoothed);
         printf("\n             => abort()\n\n");
         abort();
      } else if (n_pix_fov_jsmoothed != n_pix_fov_jfactor) {
         printf("\n====> ERROR: fits_append_fluxextension() in healpix_fits.cc");
         printf("\n             FITS extensions of unsmoothed and smoothed data don't fit together:");
         printf("\n             # pixels (unsmoothed) = %d, # pixels (smoothed) = %d", n_pix_fov_jfactor, n_pix_fov_jsmoothed);
         printf("\n             => abort()\n\n");
         abort();
      } else if (hp_scheme_jsmoothed != hp_scheme_jfactor) {
         printf("\n====> ERROR: fits_append_fluxextension() in healpix_fits.cc");
         printf("\n             FITS extensions of unsmoothed and smoothed data don't fit together:");
         printf("\n             Ordering scheme bool (unsmoothed) = %d, ordering scheme bool = %d", hp_scheme_jfactor, hp_scheme_jsmoothed);
         printf("\n             => abort()\n\n");
         abort();
      } else if (fieldunits_str_jsmoothed != fieldunits_str_jfactor) {
         printf("\n====> ERROR: fits_append_fluxextension() in healpix_fits.cc");
         printf("\n             FITS extensions of unsmoothed and smoothed data don't fit together:");
         printf("\n             units (unsmoothed) = %s, units (smoothed) = %s", fieldunits_str_jfactor.c_str(), fieldunits_str_jsmoothed.c_str());
         printf("\n             => abort()\n\n");
         abort();
      } else if (i_field_jsmoothed == -1 && i_field_jsmoothed_gamma == -1 && i_field_jsmoothed_nu == -1) {
         printf("\n====> ERROR: fits_append_fluxextension() in healpix_fits.cc");
         printf("\n             No matching smoothed Jfactor map found in extension");
         printf("\n             => abort()\n\n");
         abort();
      };
      // now everything is fine and we can get the metadata from the header of our choice.
      i_ext_headerdata = i_ext_jsmoothed;
   } else {
      i_ext_headerdata = i_ext_jfactor;
   }


   vector<string> inputvars_fromfile = fits_read_params2string(filename, i_ext_headerdata);

   vector<bool> is_filled(gN_INPUTPARAMS, true);
   vector<bool> is_needed(gN_INPUTPARAMS, true);

   string ann_or_dec_info;
   // compare with gSIM_INPUTPARAM_VALUESTRING for what is still missing and abort if something is missing.
   if (inputvars_fromfile[kPP_DM_IS_ANNIHIL_OR_DECAY] == "1") {
      ann_or_dec_info = "annihilation";
      gPARAMS_REQUIRED[gSIM_FLAG_MODE].push_back(kPP_DM_ANNIHIL_DELTA);
      gPARAMS_REQUIRED[gSIM_FLAG_MODE].push_back(kPP_DM_ANNIHIL_SIGMAV_CM3PERS);
      if (gSIM_INPUTPARAM_VALUESTRING[kPP_DM_ANNIHIL_DELTA] == "-999") is_filled[kPP_DM_ANNIHIL_DELTA] = false;
      if (gSIM_INPUTPARAM_VALUESTRING[kPP_DM_ANNIHIL_SIGMAV_CM3PERS] == "-999") is_filled[kPP_DM_ANNIHIL_SIGMAV_CM3PERS] = false;
      if (gSIM_INPUTPARAM_VALUESTRING[kPP_DM_DECAY_LIFETIME_S] != "-999") is_needed[kPP_DM_DECAY_LIFETIME_S] = false;
   } else if (inputvars_fromfile[kPP_DM_IS_ANNIHIL_OR_DECAY] == "0") {
      ann_or_dec_info = "decay";
      gPARAMS_REQUIRED[gSIM_FLAG_MODE].push_back(kPP_DM_DECAY_LIFETIME_S);
      if (gSIM_INPUTPARAM_VALUESTRING[kPP_DM_DECAY_LIFETIME_S] == "-999") is_filled[kPP_DM_DECAY_LIFETIME_S] = false;
      if (gSIM_INPUTPARAM_VALUESTRING[kPP_DM_ANNIHIL_DELTA] != "-999") is_needed[kPP_DM_ANNIHIL_DELTA] = false;
      if (gSIM_INPUTPARAM_VALUESTRING[kPP_DM_ANNIHIL_SIGMAV_CM3PERS] != "-999") is_needed[kPP_DM_ANNIHIL_SIGMAV_CM3PERS] = false;
   }
   if (atof(inputvars_fromfile[kSIM_REDSHIFT].c_str()) > ZMIN_EXTRAGAL) {
      gPARAMS_REQUIRED[gSIM_FLAG_MODE].push_back(kEXTRAGAL_FLAG_ABSORPTIONPROFILE);
      if (gSIM_INPUTPARAM_VALUESTRING[kEXTRAGAL_FLAG_ABSORPTIONPROFILE] == "-999") is_filled[kEXTRAGAL_FLAG_ABSORPTIONPROFILE] = false;
   }

   if (find(is_filled.begin(), is_filled.end(), false) != is_filled.end()) {

      vector<bool> is_printed(gN_INPUTPARAMS, false); // to check already printed missing values

      cout << "\n====> fits_append_fluxextension() in healpix_fits.cc" << endl;
      cout << "             The input J-Factor map tells that we are dealing with DM " << ann_or_dec_info << " at redshift z = " << inputvars_fromfile[kSIM_REDSHIFT] << "." << endl;
      cout << "             Therefore, we need to specifiy the following parameters in the input" <<  endl;
      cout << "             parameter textfile according to the scheme below (just copy and paste"  << endl;
      cout << "             the printout into the parameter file for some standard parameters)."  << endl << endl;
      cout << string_fixlength("# Variable name", 40) << string_fixlength("Unit", 14) << string_fixlength("Standard Value", 17)
           << string_fixlength("(Format)", 17) << string_fixlength("(Comment)", 47) << endl << endl;

      for (int i = 0; i < (int)gPARAMS_REQUIRED[gSIM_FLAG_MODE].size(); ++i) {
         if (!is_filled[gPARAMS_REQUIRED[gSIM_FLAG_MODE][i]] and !is_printed[gPARAMS_REQUIRED[gSIM_FLAG_MODE][i]]) {
            cout << string_fixlength(gSIM_INPUTPARAMS[gPARAMS_REQUIRED[gSIM_FLAG_MODE][i]][kSIM_INPUTPARAM_VARNAME], 40)
                 << string_fixlength(gSIM_INPUTPARAMS[gPARAMS_REQUIRED[gSIM_FLAG_MODE][i]][kSIM_INPUTPARAM_UNIT], gSIM_PARAMLENGTH_MAX)
                 << string_fixlength(gSIM_STANDARD_INPUTPARAMS[gPARAMS_REQUIRED[gSIM_FLAG_MODE][i]], gSIM_PARAMLENGTH_MAX) << "   "
                 << string_fixlength("<" + string(gSIM_INPUTPARAMS[gPARAMS_REQUIRED[gSIM_FLAG_MODE][i]][kSIM_INPUTPARAM_DATATYPE]) + ">", gSIM_PARAMLENGTH_MAX + 3)
                 << string_fixlength(gSIM_INPUTPARAMS[gPARAMS_REQUIRED[gSIM_FLAG_MODE][i]][kSIM_INPUTPARAM_DESCRIPTION], 47) << endl;
            is_printed[gPARAMS_REQUIRED[gSIM_FLAG_MODE][i]] = true;
         }
      }
      printf("\n             => Don't worry - add them and restart the run again!\n\n");
      abort();
   }
   if (find(is_needed.begin(), is_needed.end(), false) != is_needed.end()) {
      print_warning("healpix_fits.cc", "fits_append_fluxextension()",
                    "The following parameters set in the parameter file are irrelevant for the performed simulation:");
      cout << string_fixlength("# Variable name", 40)  << string_fixlength("(Comment)", 47) << endl << endl;

      for (int i = 0; i < gN_INPUTPARAMS; ++i) {
         if (!is_needed[i]) {
            cout << string_fixlength(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME], 40) << string_fixlength(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_DESCRIPTION], 47) << endl;
         }
      }
      printf("\n\n");
   }

   // now merge the input variables from the -f simulation into the parameters read from the
   // fits file and dump everything into the global variables:
   vector<string> inputvars_global = inputparameters_globalparams2string(gSIM_FLAG_MODE);

   for (int i = 0; i < (int)gPARAMS_REQUIRED[gSIM_FLAG_MODE].size(); ++i) {
      inputvars_fromfile[gPARAMS_REQUIRED[gSIM_FLAG_MODE][i]] =  inputvars_global[gPARAMS_REQUIRED[gSIM_FLAG_MODE][i]];
   }
   // variable gSIM_IS_WRITE_FLUXMAPS is now 1, of course:
   inputvars_fromfile[kSIM_IS_WRITE_FLUXMAPS] = "1";
   // now we can also synchronize gSIM_INPUTPARAM_VALUESTRING and inputvars_fromfile
   // for clarity and writing the correct FITS header:
   gSIM_INPUTPARAM_VALUESTRING = inputvars_fromfile;

   inputparameters_string2globalparams(inputvars_fromfile);

   // small check:
   if (gSIM_HEALPIX_NSIDE != n_side_jfactor) {
      sprintf(char_tmp, "gSIM_HEALPIX_NSIDE = %d and = %d in file do not match.", gSIM_HEALPIX_NSIDE, n_side_jfactor);
      print_warning("healpix_fits.cc", "fits_append_fluxextension()", string(char_tmp));
   }

   int smooth_gamma_or_nu_or_both = 0;
   bool is_gamma = false;
   bool is_nu = false ;
   if (atoi(inputvars_fromfile[kSIM_GAUSSBEAM_GAMMA_FWHM].c_str()) != -1) is_gamma = true;
   if (atoi(inputvars_fromfile[kSIM_GAUSSBEAM_NEUTRINO_FWHM].c_str()) != -1) is_nu = true;
   if (is_gamma && !is_nu)  smooth_gamma_or_nu_or_both = 1;
   else if (!is_gamma && is_nu)  smooth_gamma_or_nu_or_both = 2;
   else if (is_gamma && is_nu)  smooth_gamma_or_nu_or_both = 3;

   // read columns from input fits file:
   vector<int> pixnums_fov_read;
   vector<double> j_tot;

   vector<double> j_gamma_smoothed;
   vector<double> j_nu_smoothed;

   fits_read_clumpymaps(fh_file, i_ext_jfactor, i_field_jfactor, extname_str, fieldname_str, fieldunits_str, n_pix_fov_jfactor, pixnums_fov_read, j_tot);
   if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3) {
      fits_read_clumpymaps(fh_file, i_ext_jsmoothed, i_field_jsmoothed_gamma, extname_str, fieldname_str, fieldunits_str, n_pix_fov_jfactor, pixnums_fov_read, j_gamma_smoothed) ;
   }
   if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3) {
      fits_read_clumpymaps(fh_file, i_ext_jsmoothed, i_field_jsmoothed_nu, extname_str, fieldname_str, fieldunits_str, n_pix_fov_jfactor, pixnums_fov_read, j_nu_smoothed) ;
   }


   // finally, calculate and write flux and intensity maps into appended extension to FITS file!
   if (smooth_gamma_or_nu_or_both == 0) {
      fits_write_fluxmaps(pixnums_fov_read, j_tot, fh_file);
   } else if (smooth_gamma_or_nu_or_both == 1) {
      fits_write_fluxmaps(pixnums_fov_read, j_tot, j_gamma_smoothed, smooth_gamma_or_nu_or_both, fh_file);
   } else if (smooth_gamma_or_nu_or_both == 2) {
      fits_write_fluxmaps(pixnums_fov_read, j_tot, j_nu_smoothed, smooth_gamma_or_nu_or_both, fh_file);
   } else if (smooth_gamma_or_nu_or_both == 3) {
      fits_write_fluxmaps(pixnums_fov_read, j_tot, j_gamma_smoothed, j_nu_smoothed, smooth_gamma_or_nu_or_both, fh_file);
   }

   fh_file.close();

   // Free memory
   vector<int>().swap(pixnums_fov_read); // deallocate memory
   vector<double>().swap(j_tot); // deallocate memory
   vector<double>().swap(j_gamma_smoothed); // deallocate memory
   vector<double>().swap(j_nu_smoothed); // deallocate memory

   return;
}

//______________________________________________________________________________
void fits_print_info(string &filename)
{
   //--- Prints information about the content of a (clumpy) fits file "filename".

   ifstream isfile(filename.c_str());
   if (!isfile) {
      printf("\n====> ERROR: fits_print_info() in healpix_fits.cc");
      printf("\n             File %s does not exist.", filename.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }

   string extname_str ;
   string fieldname_str ;
   string fieldunits_str ;
   string extname_str_trim ;
   int n_side_read ;
   Healpix_Ordering_Scheme hp_scheme ;
   int n_pix_fov_read ;
   int i_extension  = 1;
   //string str_scheme ;

   fitshandle fh_file;
   fh_file.open(filename);

   cout << ">>>>> read FITS headers from file " << filename << " ..."  << endl;
   int n_extensions = fits_read_nextensions(fh_file);
   cout << "   => FITS file contains " << n_extensions << " extensions:\n"  << endl;


   for (i_extension = 1; i_extension < n_extensions + 1; ++i_extension) {
      cout << "      Extension " << i_extension << " :"  << endl;
      int i_field = 0 ;
      fits_read_fieldinfo(fh_file, i_extension, i_field, extname_str, fieldname_str, fieldunits_str) ;
      extname_str_trim = removeblanks_from_startstop(string_fixlength(extname_str, 20)) ;
      cout << "        name of this extension: " << extname_str_trim  << endl;
      int n_maps_read = fits_read_ncolumns(fh_file, i_extension, n_side_read, hp_scheme, n_pix_fov_read);

      cout << "        physical maps in this extension: " << n_maps_read  << endl;
      for (i_field = 0; i_field <= n_maps_read; ++i_field) {
         fits_read_fieldinfo(fh_file, i_extension, i_field, extname_str, fieldname_str, fieldunits_str) ;
         if (i_field == 0) cout << "           map " << i_field << ": " << string_fixlength(fieldname_str, 20)  << " [Healpix Pixel indices]" << endl;
         else cout << "           map " << i_field << ": " << string_fixlength(fieldname_str, 20)  << " Units: " << string_fixlength(fieldunits_str, 20) << endl;
      }

      cout << "        n_side of the maps: " << n_side_read  << endl;
      cout << "        ordering scheme of the maps: " << hp_scheme  << endl;
      cout << "        number of pixels (n_pix) in the maps: " << n_pix_fov_read  << endl;
      cout << ""  << endl;
   }
   fh_file.close();
   return;
}

//______________________________________________________________________________
void fits_to_simplefits(string &filename, int &i_extension, int &i_field)
{
   //--- Extracts a specific field in a specific extension from a fits output file
   //    and writes it into a whole-sky Healpix map.
   // INPUTS:
   //  filename      name of cut-sky input file, including possible path.
   //  i_extension   extension to be read, starting with index 1.
   //  i_field       index of field to be read, index 0 will read Healpix pixels.
   // OUTPUTS:
   //  writes a full-sky fits file in the same directory as the input file.

   ifstream isinfile(filename.c_str());
   if (!isinfile) {
      printf("\n====> ERROR: fits_to_simplefits() in healpix_fits.cc");
      printf("\n             File %s does not exist.", filename.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }

   int n_side_read ;
   int n_pix_read ;
   int n_pix_fov_read ;
   int n_maps_read ;
   string extname_str;
   string fieldname_str;
   string fieldunits_str;
   string extname_str_trim ;
   string fieldname_str_trim ;
   string filename_out = filename.substr(0, filename.size() - 5);
   T_Healpix_Base<int> hp_gridprop_read;
   Healpix_Ordering_Scheme hp_scheme;
   string str_scheme ;

   fitshandle fh_file_in ;
   fh_file_in.open(filename);

   int n_extensions = fits_read_nextensions(fh_file_in);
   if (i_extension > n_extensions) {
      printf("\n====> ERROR: fits_to_simplefits() in healpix_fits.cc");
      printf("\n             FITS file contains only %d extensions.", n_extensions);
      printf("\n             => abort()\n\n");
      abort();
   }

   cout << ">>>>> read extension " << i_extension << " from file " << filename << " ..."  << endl;

   n_maps_read = fits_read_ncolumns(fh_file_in, i_extension, n_side_read, hp_scheme, n_pix_fov_read);
   if (i_field > n_maps_read) {
      printf("\n====> ERROR: fits_to_simplefits() in healpix_fits.cc");
      printf("\n             FITS extension #%d contains only %d + 1 columns (starting with index 0).", i_extension, n_maps_read);
      printf("\n             => abort()\n\n");
      abort();
   }

   cout << "      maps in this extension: " << n_maps_read  << endl;
   cout << "      n_side of the maps: " << n_side_read  << endl;
   cout << "      number of pixels (n_pix) in the maps: " << n_pix_fov_read  << endl;
   cout << "      output map will always be saved in RING Healpix ordering scheme. " << endl;

   // always save in RING scheme:
   hp_gridprop_read.SetNside(n_side_read, RING);
   n_pix_read = hp_gridprop_read.Npix();

   vector<int> pixnums_fov_read;
   vector<double> data_read;

   fits_read_clumpymaps(fh_file_in, i_extension, i_field, extname_str, fieldname_str, fieldunits_str, n_pix_fov_read, pixnums_fov_read, data_read) ;

   extname_str_trim = removeblanks_from_startstop(string_fixlength(extname_str, 20)) ;
   fieldname_str_trim = removeblanks_from_startstop(string_fixlength(fieldname_str, 20)) ;

   cout << "\n      Name of accessed extension: " << extname_str_trim << endl;
   cout << "      Quantity in accessed map: " << fieldname_str_trim << endl;
   if (i_field != 0) cout << "      Physical unit of accessed map: " << string_fixlength(fieldunits_str, 20) << endl;

   // read additional FITS header keywords:
   double fsky = double(n_pix_fov_read) / double(n_pix_read) ; // fraction of sky covered by FOV;

   vector<string> inputvars_fromfile = fits_read_params2string(filename, i_extension);

   fh_file_in.close();

   filename_out.append("-") ;
   filename_out.append(extname_str_trim) ;
   filename_out.append("-") ;
   filename_out.append(fieldname_str_trim) ;
   filename_out.append(".fits") ;

   // write Healpix map and output whole sky fits file:
   cout << ">>>>> full-sky array init (may take a while for high resolutions)..." << endl;

   vector<double> data_read_fullsky(n_pix_read, HPX_BLIND_VALUE);
   double mean_map_tmp = 1e-40;
   double mean_map = 1e-40;

   cout << ">>>>> full-sky array filling (may take a while for large FOV)...\n" << endl;
   for (int i = 0; i < n_pix_fov_read; i +=  1) {
      if (hp_scheme == NEST) data_read_fullsky[hp_gridprop_read.nest2ring(pixnums_fov_read[i])] = data_read[i];
      else data_read_fullsky[pixnums_fov_read[i]] = data_read[i];
      mean_map_tmp += (data_read[i] - mean_map_tmp) / double(i + 1) ;
   } ;
   mean_map = mean_map_tmp;

   ifstream isfile(filename_out.c_str());
   if (isfile) {
      print_warning("healpix_fits.cc", "fits_to_simplefits()",
                    "Output file " + filename_out + " already exists and will be overwritten. You have 5 seconds to kill the process with CTRL + C ...");
      usleep(5e6);
      //fh_file_out.open(filename_out);
      //fh_file_out.delete_file(filename_out);
      remove(filename_out.c_str());
   }

   cout << ">>>>> write FITS file " << filename_out << " ..."  << endl;
   if (i_field == 0) cout << "      (although writing out pixel numbers does not make much sense. But if you want it, please!)" << endl;
   cout << endl;

   fitshandle fh_file_out;
   fh_file_out.create(filename_out);

   // workaround for writing the tables
   fh_file_out.close();

   // now work directly with FITSIO routines, because HEALPix fitshandle.cc seems to behave unexpectedly :-(
   fitsfile *fptr;       // pointer to the FITS file
   int status, hdutype;
   long firstrow, firstelem;

   int tfields   = 1;
   int nrows    = n_pix_read;

   const char *filename_char = filename_out.c_str();

   char extname[] = "dummyname";

   /* define the name, datatype, and physical units */
   char *ttype[tfields];
   char *tform[tfields];
   char *tunit[tfields];

   ttype[0] = strdup(fieldname_str.c_str());

   int repcount = 1;
   string tform_type;
   switch (gSIM_HEALPIX_FITS_DATATYPE) {
      case PLANCK_FLOAT32:
         tform_type = "E";
         break;
      case PLANCK_FLOAT64:
         tform_type = "D";
         break;
      default:
         printf("\n====> ERROR: fits_write_clumpymaps() in healpix_fits.cc");
         printf("\n             gSIM_HEALPIX_FITS_DATATYPE must be either .");
         printf("\n             PLANCK_FLOAT32 (single precision) or PLANCK_FLOAT64 (double precision).");
         printf("\n             => abort()\n\n");
         abort();
   }
   char char_tmp[100];
   sprintf(char_tmp, "%d%s", repcount, tform_type.c_str());
   tform[0] = strdup(char_tmp);
   tunit[0] = strdup("dummyunit");

   status = 0;

   // open the FITS file
   if (fits_open_file(&fptr, filename_char, READWRITE, &status))
      fits_printerror(status);

   // got to end of the file
   if (fits_movabs_hdu(fptr, 1, &hdutype, &status))
      fits_printerror(status);

   // append a new empty binary table onto the FITS file
   if (fits_create_tbl(fptr, BINARY_TBL, nrows, tfields, ttype, tform, tunit, extname, &status))
      fits_printerror(status);

   firstrow  = 1;  // first row in table to write
   firstelem = 1;  // first element in row

   fits_write_col(fptr, TDOUBLE, 1, firstrow, firstelem, nrows, &data_read_fullsky[0], &status);

   // close the FITS file
   if (fits_close_file(fptr, &status))
      fits_printerror(status);

   // ... and open it again at the end of the workaround
   fh_file_out.open(filename_out);
   fh_file_out.goto_hdu(fh_file_out.num_hdus()); // go to last extension

   // add HEALPIX compliant keywords:
   fh_file_out.set_key("PIXTYPE", string("HEALPIX"), "HEALPIX pixelisation");
   fh_file_out.set_key("ORDERING", string("RING"), "Pixel ordering scheme, either RING or NESTED");
   fh_file_out.set_key("INDXSCHM", string("IMPLICIT"), "Indexing: IMPLICIT or EXPLICIT");
   fh_file_out.set_key("FIRSTPIX", 0, " First pixel # (0 based)");
   fh_file_out.set_key("LASTPIX", n_pix_read - 1, " Last pixel # (0 based)");


   fh_file_out.set_key("EXTNAME", extname_str, "name of this binary table extension");
   // add BAD_DATA keyword:
   fh_file_out.set_key("BAD_DATA", HPX_BLIND_VALUE, "Sentinel value given to bad pixels");
   // update correct unit keyword:
   fh_file_out.set_key("TUNIT1", fieldunits_str, "physical unit of field");
   // add correct coordinate system keyword:
   fh_file_out.set_key("COORDSYS", string("G"), "Pixelisation coordinate system");

   // write input header keywords:
   fh_file_out.add_comment("------------------------------------------------------------------------");
   fh_file_out.add_comment("------------------------------------------------------------------------");
   fh_file_out.add_comment("Input parameters for this output file:");
   fh_file_out.add_comment("------------------------------------------------------------------------");
   for (int i = 0; i < gN_INPUTPARAMS; ++i) {
      if (inputvars_fromfile[i] != "-999") {
         fits_write_key_from_inputparams(fh_file_out, gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_FITSNAME], inputvars_fromfile[i],
                                         gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_DATATYPE], gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_UNIT],
                                         gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_DESCRIPTION]);
      }
   }
   fh_file_out.add_comment("------------------------------------------------------------------------");
   fh_file_out.add_comment("------------------------------------------------------------------------");
   fh_file_out.add_comment("Special information:");
   fh_file_out.add_comment("------------------------------------------------------------------------");
   // add mean:
   fh_file_out.set_key("MEAN", mean_map, "mean value in FOV on f_sky");
   // add fsky :
   fh_file_out.set_key("F_SKY", fsky, "fraction of sky covered by FOV");

   fh_file_out.close();
   // Free memory
   vector<double>().swap(data_read_fullsky); // deallocate memory
   vector<int>().swap(pixnums_fov_read); // deallocate memory
   vector<double>().swap(data_read); // deallocate memory
   return;
}

//______________________________________________________________________________
void fits_to_ascii(string &filename, int &i_extension)
{
   //--- Extracts a specific extension from a fits output file and writes all fields
   //    in this extension column-wise into an ASCII-file.
   // INPUTS:
   //  filename      name of cut-sky input file, including possible path.
   //  i_extension   extension to be read, starting with index 1.
   // OUTPUTS:
   //  writes a ASCII-file in the same directory as the input file.

   ifstream isfile(filename.c_str());
   if (!isfile) {
      printf("\n====> ERROR: fits_to_ascii() in healpix_fits.cc");
      printf("\n             File %s does not exist.", filename.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }

   cout << ">>>>> read extension " << i_extension << " from file " << filename << " ..."  << endl;

   int n_side_read ;
   int n_pix_fov_read ;
   int n_maps_read ;
   int i_field;

   string extname_str;

   string filename_out = filename.substr(0, filename.size() - 5);
   T_Healpix_Base<int> hp_gridprop_read;
   Healpix_Ordering_Scheme hp_scheme ;
   string str_scheme ;
   fitshandle fh_file ;
   fh_file.open(filename);

   // maximum 6 maps + Healpix pixels to be read:
   vector<int> pixnums_fov_read;
   vector<double> data1_read;
   vector<double> data2_read;
   vector<double> data3_read;
   vector<double> data4_read;
   vector<double> data5_read;
   vector<double> data6_read;

   string field1name_str;
   string field1units_str;

   string field2name_str;
   string field2units_str;

   string field3name_str;
   string field3units_str;

   string field4name_str;
   string field4units_str;

   string field5name_str;
   string field5units_str;

   string field6name_str;
   string field6units_str;

   int n_extensions = fits_read_nextensions(fh_file);
   if (i_extension > n_extensions) {
      printf("\n====> ERROR: fits_to_simplefits() in healpix_fits.cc");
      printf("\n             FITS file contains only %d extensions.", n_extensions);
      printf("\n             => abort()\n\n");
      abort();
   }

   n_maps_read = fits_read_ncolumns(fh_file, i_extension, n_side_read, hp_scheme, n_pix_fov_read);

   hp_gridprop_read.SetNside(n_side_read, hp_scheme);

   cout << "      maps in this extension: " << n_maps_read  << endl;
   cout << "      n_side of the maps: " << n_side_read  << endl;
   cout << "      number of pixels (n_pix) in the maps: " << n_pix_fov_read  << endl;

   // read maps:
   i_field = 1;
   fits_read_clumpymaps(fh_file, i_extension, i_field, extname_str, field1name_str, field1units_str, n_pix_fov_read, pixnums_fov_read, data1_read);

   if (n_maps_read >= 2) {
      i_field = 2;
      fits_read_clumpymaps(fh_file, i_extension, i_field, extname_str, field2name_str, field2units_str, n_pix_fov_read, pixnums_fov_read, data2_read);
   }

   if (n_maps_read >= 3) {
      i_field = 3;
      fits_read_clumpymaps(fh_file, i_extension, i_field, extname_str, field3name_str, field3units_str, n_pix_fov_read, pixnums_fov_read, data3_read);
   }

   if (n_maps_read >= 4) {
      i_field = 4;
      fits_read_clumpymaps(fh_file, i_extension, i_field, extname_str, field4name_str, field4units_str, n_pix_fov_read, pixnums_fov_read, data4_read);
   }

   if (n_maps_read >= 5) {
      i_field = 5;
      fits_read_clumpymaps(fh_file, i_extension, i_field, extname_str, field5name_str, field5units_str, n_pix_fov_read, pixnums_fov_read, data5_read);
   }

   if (n_maps_read == 6) {
      i_field = 6;
      fits_read_clumpymaps(fh_file, i_extension, i_field, extname_str, field6name_str, field6units_str, n_pix_fov_read, pixnums_fov_read, data6_read);
   }

   // read additional FITS header keywords:
   double psi_deg ;
   double theta_deg;
   double theta_orth_size_deg;
   double theta_size_deg;
   double precision;

   fh_file.goto_hdu(i_extension + 1); // extension labeling starts with primary header = 1.
   fh_file.get_key("PSI_0", psi_deg);
   fh_file.get_key("THETA_0", theta_deg);
   fh_file.get_key("SIZE_X", theta_orth_size_deg);
   fh_file.get_key("SIZE_Y", theta_size_deg);
   fh_file.get_key("EPS", precision);
   fh_file.close();

   int sigdigits;
   if (precision == 999) {
      precision = 1e-2;
      sigdigits = 2;
   } else if (precision == 998) {
      precision = 5e-4;
      sigdigits = 2;
   } else sigdigits = int(ceil(-log10(precision)));

   // create output file:
   filename_out.append("-") ;

   string extname_str_trim = removeblanks_from_startstop(string_fixlength(extname_str, 20)) ;
   filename_out.append(extname_str_trim) ;
   filename_out.append(".dat") ;

   cout << "\n>>>>> write ASCII file " << filename_out << " ...\n"  << endl;

   FILE *fp;
   fp = fopen(filename_out.c_str(), "w");

   fprintf(fp, "# Skymap with all components\n");
   fprintf(fp, "# Looking in direction (psi,theta)=(%+le, %+le) with (dpsi,dtheta)=(%le, %le) deg\n", psi_deg, theta_deg, theta_orth_size_deg, theta_size_deg);
   fprintf(fp, "# Healpix NSIDE is: %d (matches with given RING ordered pixel numbers)\n", n_side_read);
   if (i_extension == 1) fprintf(fp, "# => J-factor integration region (= pixel size) is: %.*le sr\n", sigdigits, PI / 3 * pow(n_side_read, -2));
   fprintf(fp, "# Columns are as follows:\n");

   size_t tab_length = 20;

   if (n_maps_read == 1) {
      fprintf(fp, "#     psi   |   theta  | %s| Pixel number \n", string_fixlength(field1name_str, tab_length).c_str());
      fprintf(fp, "#         [deg]        | %s| (Ring ordered) \n", string_fixlength(field1units_str, tab_length).c_str());
   } else if (n_maps_read == 2) {
      fprintf(fp, "#     psi   |   theta  | %s| %s| Pixel number \n", string_fixlength(field1name_str, tab_length).c_str(),
              string_fixlength(field2name_str, tab_length).c_str());
      fprintf(fp, "#         [deg]        | %s| %s| (Ring ordered) \n", string_fixlength(field1units_str, tab_length).c_str(),
              string_fixlength(field2units_str, tab_length).c_str());
   } else if (n_maps_read == 3) {
      fprintf(fp, "#     psi   |   theta  | %s| %s| %s| Pixel number \n", string_fixlength(field1name_str, tab_length).c_str(),
              string_fixlength(field2name_str, tab_length).c_str(),
              string_fixlength(field3name_str, tab_length).c_str());
      fprintf(fp, "#         [deg]        | %s| %s| %s| (Ring ordered) \n", string_fixlength(field1units_str, tab_length).c_str(),
              string_fixlength(field2units_str, tab_length).c_str(),
              string_fixlength(field3units_str, tab_length).c_str());
   } else if (n_maps_read == 4) {
      fprintf(fp, "#     psi   |   theta  | %s| %s| %s| %s| Pixel number \n", string_fixlength(field1name_str, tab_length).c_str(),
              string_fixlength(field2name_str, tab_length).c_str(),
              string_fixlength(field3name_str, tab_length).c_str(),
              string_fixlength(field4name_str, tab_length).c_str());
      fprintf(fp, "#         [deg]        | %s| %s| %s| %s| (Ring ordered) \n", string_fixlength(field1units_str, tab_length).c_str(),
              string_fixlength(field2units_str, tab_length).c_str(),
              string_fixlength(field3units_str, tab_length).c_str(),
              string_fixlength(field4units_str, tab_length).c_str());
   } else if (n_maps_read == 5) {
      fprintf(fp, "#     psi   |   theta  | %s| %s| %s| %s| %s| Pixel number \n", string_fixlength(field1name_str, tab_length).c_str(),
              string_fixlength(field2name_str, tab_length).c_str(),
              string_fixlength(field3name_str, tab_length).c_str(),
              string_fixlength(field4name_str, tab_length).c_str(),
              string_fixlength(field5name_str, tab_length).c_str());
      fprintf(fp, "#         [deg]        | %s| %s| %s| %s| %s| (Ring ordered) \n", string_fixlength(field1units_str, tab_length).c_str(),
              string_fixlength(field2units_str, tab_length).c_str(),
              string_fixlength(field3units_str, tab_length).c_str(),
              string_fixlength(field4units_str, tab_length).c_str(),
              string_fixlength(field5units_str, tab_length).c_str());
   } else if (n_maps_read == 6) {
      fprintf(fp, "#     psi   |   theta  | %s| %s| %s| %s| %s| %s| Pixel number \n", string_fixlength(field1name_str, tab_length).c_str(),
              string_fixlength(field2name_str, tab_length).c_str(),
              string_fixlength(field3name_str, tab_length).c_str(),
              string_fixlength(field4name_str, tab_length).c_str(),
              string_fixlength(field5name_str, tab_length).c_str(),
              string_fixlength(field6name_str, tab_length).c_str());
      fprintf(fp, "#         [deg]        | %s| %s| %s| %s| %s| %s| (Ring ordered) \n", string_fixlength(field1units_str, tab_length).c_str(),
              string_fixlength(field2units_str, tab_length).c_str(),
              string_fixlength(field3units_str, tab_length).c_str(),
              string_fixlength(field4units_str, tab_length).c_str(),
              string_fixlength(field5units_str, tab_length).c_str(),
              string_fixlength(field6units_str, tab_length).c_str());
   }

   for (int i = 0; i < n_pix_fov_read; ++i) {
      pointing ptg_i_pix = hp_gridprop_read.pix2ang(pixnums_fov_read[i]) ;
      double psi = ptg_i_pix.phi ;
      check_psi(psi) ;
      double theta = PI / 2 - ptg_i_pix.theta ;
      int pixnum;

      if (hp_scheme == NEST) pixnum = hp_gridprop_read.nest2ring(pixnums_fov_read[i]);
      else pixnum = pixnums_fov_read[i];

      if (n_maps_read == 1) {
         fprintf(fp, "%+11.*f %+11.*f %.*le %d \n", sigdigits + 1, psi * RAD_to_DEG, sigdigits + 1, theta * RAD_to_DEG, sigdigits, data1_read[i], pixnum);
      } else if (n_maps_read == 2) {
         fprintf(fp, "%+11.*f %+11.*f %.*le %.*le %d \n", sigdigits + 1, psi * RAD_to_DEG, sigdigits + 1, theta * RAD_to_DEG, sigdigits, data1_read[i], sigdigits, data2_read[i], pixnum);
      } else if (n_maps_read == 3) {
         fprintf(fp, "%+11.*f %+11.*f %.*le %.*le %.*le %d \n", sigdigits + 1, psi * RAD_to_DEG, sigdigits + 1, theta * RAD_to_DEG, sigdigits, data1_read[i], sigdigits, data2_read[i], sigdigits, data3_read[i], pixnum);
      } else if (n_maps_read == 4) {
         fprintf(fp, "%+11.*f %+11.*f %.*le %.*le %.*le %.*le %d \n", sigdigits + 1, psi * RAD_to_DEG, sigdigits + 1, theta * RAD_to_DEG, sigdigits, data1_read[i], sigdigits, data2_read[i], sigdigits, data3_read[i], sigdigits, data4_read[i], pixnum);
      } else if (n_maps_read == 5) {
         fprintf(fp, "%+11.*f %+11.*f %.*le %.*le %.*le %.*le %.*le %d \n", sigdigits + 1, psi * RAD_to_DEG, sigdigits + 1, theta * RAD_to_DEG, sigdigits, data1_read[i], sigdigits, data2_read[i], sigdigits, data3_read[i], sigdigits, data4_read[i], sigdigits, data5_read[i], pixnum);
      } else if (n_maps_read == 6) {
         fprintf(fp, "%+11.*f %+11.*f %.*le %.*le %.*le %.*le %.*le %.*le %d \n", sigdigits + 1, psi * RAD_to_DEG, sigdigits + 1, theta * RAD_to_DEG, sigdigits, data1_read[i], sigdigits, data2_read[i], sigdigits, data3_read[i], sigdigits, data4_read[i], sigdigits, data5_read[i], sigdigits, data6_read[i], pixnum);
      }
   }
   fclose(fp);

   vector<int>().swap(pixnums_fov_read); // deallocate memory
   vector<double>().swap(data1_read); // deallocate memory
   vector<double>().swap(data2_read); // deallocate memory
   vector<double>().swap(data3_read); // deallocate memory
   vector<double>().swap(data4_read); // deallocate memory
   vector<double>().swap(data5_read); // deallocate memory
   vector<double>().swap(data6_read); // deallocate memory
   return;
}

//______________________________________________________________________________
void fits_write_clumpymaps(fitshandle &fh_file, vector<int> &pixnums_fov, vector<double> &data1)
{
   //--- Writes the explicit pixel number and one data map into an extension at the end
   //    of the file 'filename' or creates 'filename' if it doesn't exist yet.
   //    This is a wrapper around fits_write_clumpymaps(six input maps) for only
   //    one input map. For the detailed documentation see below.

   int nmaps = 1;
   vector<double> data2, data3, data4, data5, data6;
   fits_write_clumpymaps(fh_file, pixnums_fov, data1, data2, data3, data4, data5, data6, nmaps);
   return;
}

//______________________________________________________________________________
void fits_write_clumpymaps(fitshandle &fh_file, vector<int> &pixnums_fov, vector<double> &data1, vector<double> &data2)
{
   //--- Writes the explicit pixel number and two data maps into an extension at the end
   //    of the file 'filename' or creates 'filename' if it doesn't exist yet.
   //    This is a wrapper around fits_write_clumpymaps(six input maps) for only
   //    one input map. For the detailed documentation see below.
   int nmaps = 2;
   vector<double> data3, data4, data5, data6;
   fits_write_clumpymaps(fh_file, pixnums_fov, data1, data2, data3, data4, data5, data6, nmaps);
   return;
}

//______________________________________________________________________________
void fits_write_clumpymaps(fitshandle &fh_file, vector<int> &pixnums_fov, vector<double> &data1, vector<double> &data2, vector<double> &data3)
{
   //--- Writes the explicit pixel number and three data maps into an extension at the end
   //    of the file 'filename' or creates 'filename' if it doesn't exist yet.
   //    This is a wrapper around fits_write_clumpymaps(six input maps) for only
   //    one input map. For the detailed documentation see below.
   int nmaps = 3;
   vector<double> data4, data5, data6;
   fits_write_clumpymaps(fh_file, pixnums_fov, data1, data2, data3, data4, data5, data6, nmaps);
   return;
}

//______________________________________________________________________________
void fits_write_clumpymaps(fitshandle &fh_file, vector<int> &pixnums_fov, vector<double> &data1, vector<double> &data2, vector<double> &data3, vector<double> &data4)
{
   //--- Writes the explicit pixel number and four data maps into an extension at the end
   //    of the file 'filename' or creates 'filename' if it doesn't exist yet.
   //    This is a wrapper around fits_write_clumpymaps(six input maps) for only
   //    one input map. For the detailed documentation see below.
   int nmaps = 4;
   vector<double> data5, data6;
   fits_write_clumpymaps(fh_file, pixnums_fov, data1, data2, data3, data4, data5, data6, nmaps);
   return;
}

//______________________________________________________________________________
void fits_write_clumpymaps(fitshandle &fh_file, vector<int> &pixnums_fov, vector<double> &data1, vector<double> &data2, vector<double> &data3, vector<double> &data4, vector<double> &data5)
{
   //--- Writes the explicit pixel number and five data maps into an extension at the end
   //    of the file 'filename' or creates 'filename' if it doesn't exist yet.
   //    This is a wrapper around fits_write_clumpymaps(six input maps) for only
   //    one input map. For the detailed documentation see below.
   int nmaps = 5;
   vector<double> data6;
   fits_write_clumpymaps(fh_file, pixnums_fov, data1, data2, data3, data4, data5, data6, nmaps);
   return;
}

//______________________________________________________________________________
void fits_write_clumpymaps(fitshandle &fh_file, vector<int> &pixnums_fov, vector<double> &data1, vector<double> &data2, vector<double> &data3, vector<double> &data4, vector<double> &data5, vector<double> &data6, int nmaps)
{
   //--- Writes the explicit pixel number and six data maps into an extension at the end
   //    of the file 'filename' or creates 'filename' if it doesn't exist yet.
   //    The data format (single/double precision) can be controlled via the gSIM_HEALPIX_FITS_DATATYPE
   //    parameter. Only dummy names for extension name, column names and units are set.

   string filename = fh_file.fileName();

   // check if file already exists:
   ifstream isfile(filename.c_str());
   if (!isfile) fh_file.create(filename);

   int n_ext = fh_file.num_hdus();

   if (n_ext == 0) {
      cout << "  ... create output file [FITS] " << filename << endl;
   } else if (n_ext == 1) {
      cout << "  ... adding first data extension after primary header to output file " << filename  << endl;
   } else if (n_ext == 2) {
      cout << "  ... adding second data extension to output file " << filename << endl;
   } else if (n_ext == 3) {
      cout << "  ... adding third data extension to output file " << filename << endl;
   } else {
      cout << "  ... adding " << n_ext << "th  data extension to output file " << filename << endl;
   }

   // workaround for writing the tables
   fh_file.close();

   // now work directly with FITSIO routines, because HEALPix fitshandle.cc seems to behave unexpectedly :-(
   fitsfile *fptr;       // pointer to the FITS file, defined in fitsio.h
   int status, hdutype;
   long firstrow, firstelem;

   int tfields = nmaps + 1;
   int nrows   = pixnums_fov.size();

   const char *filename_char = filename.c_str();

   char extname[] = "dummyname";

   // define the name, datatype, and physical units
   char *ttype[tfields];
   char *tform[tfields];
   char *tunit[tfields];

   // first column contains pixel numbers:
   ttype[0] = strdup("PIXEL");
   tform[0] = strdup("1J");
   tunit[0] = strdup("\0"); // will not appear

   ttype[1] = strdup("dummy1");
   if (nmaps >= 2) ttype[2] = strdup("dummy2");
   if (nmaps >= 3) ttype[3] = strdup("dummy3");
   if (nmaps >= 4) ttype[4] = strdup("dummy4");
   if (nmaps >= 5) ttype[5] = strdup("dummy5");
   if (nmaps == 6) ttype[6] = strdup("dummy6");

   int repcount = 1;
   string tform_type;
   switch (gSIM_HEALPIX_FITS_DATATYPE) {
      case PLANCK_FLOAT32:
         tform_type = "E";
         break;
      case PLANCK_FLOAT64:
         tform_type = "D";
         break;
      default:
         printf("\n====> ERROR: fits_write_clumpymaps() in healpix_fits.cc");
         printf("\n             gSIM_HEALPIX_FITS_DATATYPE must be either .");
         printf("\n             PLANCK_FLOAT32 (single precision) or PLANCK_FLOAT64 (double precision).");
         printf("\n             => abort()\n\n");
         abort();
   }

   char char_tmp[100];
   for (int i = 1; i < tfields; ++i) {
      sprintf(char_tmp, "%d%s", repcount, tform_type.c_str());
      tform[i] = strdup(char_tmp);
      tunit[i] = strdup("dummyunit");
   }

   status = 0;

   // open the FITS file
   if (fits_open_file(&fptr, filename_char, READWRITE, &status))
      fits_printerror(status);

   // got to end of the file
   if (fits_movabs_hdu(fptr, n_ext, &hdutype, &status))
      fits_printerror(status);

   // append a new empty binary table onto the FITS file
   if (fits_create_tbl(fptr, BINARY_TBL, nrows, tfields, ttype, tform, tunit, extname, &status))
      fits_printerror(status);

   firstrow  = 1;  // first row in table to write
   firstelem = 1;  // first element in row

   // write the maps:
   fits_write_col(fptr, TINT,  1, firstrow, firstelem, nrows, &pixnums_fov[0], &status);
   fits_write_col(fptr, TDOUBLE, 2, firstrow, firstelem, nrows, &data1[0], &status);
   if (nmaps >= 2) fits_write_col(fptr, TDOUBLE, 3, firstrow, firstelem, nrows, &data2[0], &status);
   if (nmaps >= 3) fits_write_col(fptr, TDOUBLE, 4, firstrow, firstelem, nrows, &data3[0], &status);
   if (nmaps >= 4) fits_write_col(fptr, TDOUBLE, 5, firstrow, firstelem, nrows, &data4[0], &status);
   if (nmaps >= 5) fits_write_col(fptr, TDOUBLE, 6, firstrow, firstelem, nrows, &data5[0], &status);
   if (nmaps == 6) fits_write_col(fptr, TDOUBLE, 7, firstrow, firstelem, nrows, &data6[0], &status);

   // close the FITS file
   if (fits_close_file(fptr, &status))
      fits_printerror(status);

   // ... and open it again at the end of the workaround
   fh_file.open(filename);
   // write HEALPix related keywords:
   fh_file.goto_hdu(fh_file.num_hdus()); // go to last extension

   fh_file.add_comment("------------------------------------------------------------------------");
   fh_file.add_comment("------------------------------------------------------------------------");
   fh_file.add_comment("HEALPIX specific keywords");
   fh_file.add_comment("------------------------------------------------------------------------");
   fh_file.set_key("PIXTYPE", string("HEALPIX"), "HEALPIX Pixelisation");
   fh_file.set_key("NSIDE", gSIM_HEALPIX_NSIDE, "Resolution parameter for HEALPix");
   string scheme;
   if (gSIM_HEALPIX_SCHEME == NEST) scheme = "NESTED";
   else if (gSIM_HEALPIX_SCHEME == RING) scheme = "RING";
   else {
      cout << "ERROR in fits_write_clumpymaps(), scheme must be NEST or RING" << endl;
      abort();
   }
   fh_file.set_key("ORDERING", scheme, "Pixel ordering scheme, either RING or NESTED");
   fh_file.set_key("INDXSCHM", string("EXPLICIT"), "Indexing : IMPLICIT or EXPLICIT");
   fh_file.set_key("GRAIN", 1, "Grain of pixel indexing");
   fh_file.add_comment("GRAIN=0 : no indexing of pixel data (IMPLICIT)");
   fh_file.add_comment("GRAIN=1 : 1 pixel index -> 1 pixel data (EXPLICIT)");
   fh_file.add_comment("GRAIN>1 : 1 pixel index -> data of GRAIN consecutive pixels (EXPLICIT)");
   fh_file.set_key("COORDSYS", string("G"), "Pixelisation coordinate system");
   fh_file.add_comment("G = Galactic, E = ecliptic, C = celestial = equatorial");

   string object_type;
   T_Healpix_Base<int> hp_gridprop(gSIM_HEALPIX_NSIDE, gSIM_HEALPIX_SCHEME, SET_NSIDE);
   if (nrows == hp_gridprop.Npix()) object_type = "PARTIAL"; // always part sky, because of explicit indexing
   else object_type = "PARTIAL";
   fh_file.set_key("OBJECT", object_type, "Sky coverage represented by data");
   fh_file.set_key("OBS_NPIX", nrows, "Number of pixels observed and recorded");
   fh_file.set_key("BAD_DATA", HPX_BLIND_VALUE, "Sentinel value given to bad pixels");
   fh_file.add_comment("------------------------------------------------------------------------");
   fh_file.add_comment("------------------------------------------------------------------------");
   fh_file.add_comment("Clumpy specific keywords");
   fh_file.add_comment("------------------------------------------------------------------------");
   fh_file.set_key("CREATOR", string("Clumpy (LPSC & DESY)"), "Software having created this file");
   fh_file.set_key("VERSION", gCLUMPY_VERSION, "Software version");

   // write input header keywords:
   fh_file.add_comment("------------------------------------------------------------------------");
   fh_file.add_comment("------------------------------------------------------------------------");
   fh_file.add_comment("Input parameters for this output file:");
   fh_file.add_comment("------------------------------------------------------------------------");
   fh_file.set_key(gSIM_INPUTPARAMS[kSIM_FLAG_MODE][kSIM_INPUTPARAM_FITSNAME],
                   string(gNAMES_SIMUMODES[gSIM_FLAG_MODE]), gSIM_INPUTPARAMS[kSIM_FLAG_MODE][kSIM_INPUTPARAM_DESCRIPTION]);

   for (int i = 0; i < gN_INPUTPARAMS; ++i) {
      if (gSIM_INPUTPARAM_VALUESTRING[i] != "-999") {
         fits_write_key_from_inputparams(fh_file, gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_FITSNAME], gSIM_INPUTPARAM_VALUESTRING[i],
                                         gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_DATATYPE], gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_UNIT],
                                         gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_DESCRIPTION]);
      }
   }
   // update again NSIDE:
   fh_file.set_key("NSIDE", gSIM_HEALPIX_NSIDE, "Resolution parameter for HEALPix");
   // always set redshift:
   fh_file.set_key("REDSHIFT", gSIM_REDSHIFT, "Redshift of object(s) in the calculation");

   return;
}

//______________________________________________________________________________
void fits_write_fluxmaps(vector<int> &arr_pix_fov, vector<double> &j, fitshandle &fh_file)
{
   //--- Writes flux and intensity maps as an appended extension to the file 'filename'.
   //    This is a wrapper around fits_write_fluxmaps(three input maps) for only
   //    one input map. For the detailed documentation see below.
   vector<double> j_gamma_smoothed;
   vector<double> j_nu_smoothed ;
   int smooth_gamma_or_nu_or_both = 0;

   fits_write_fluxmaps(arr_pix_fov, j, j_gamma_smoothed, j_nu_smoothed, smooth_gamma_or_nu_or_both, fh_file);
   return;
}

//______________________________________________________________________________
void fits_write_fluxmaps(vector<int> &arr_pix_fov, vector<double> &j, vector<double> &j_smoothed, int smooth_gamma_or_nu_or_both,
                         fitshandle &fh_file)
//--- Writes flux and intensity maps as an appended extension to the file 'filename'.
//    This is a wrapper around fits_write_fluxmaps(three input maps) for two
//    input maps. For the detailed documentation see below.
{
   // only one smoothed map is parsed; ignore here whether it's gamma or neutrino,
   // it is encoded in smooth_gamma_or_nu_or_both.
   vector<double> j_gamma_smoothed;
   vector<double> j_nu_smoothed;
   if (smooth_gamma_or_nu_or_both == 1) {
      j_gamma_smoothed = j_smoothed;
   } else if (smooth_gamma_or_nu_or_both == 2) {
      j_nu_smoothed = j_smoothed;
   } else {
      print_error("healpix_fits.cc", "fits_write_fluxmaps()", "smooth_gamma_or_nu_or_both value should indicate that exactly one smoothed map is present.");
   }

   fits_write_fluxmaps(arr_pix_fov, j, j_gamma_smoothed, j_nu_smoothed, smooth_gamma_or_nu_or_both, fh_file);
   return;
}

//______________________________________________________________________________
void fits_write_fluxmaps(vector<int> arr_pix_fov, vector<double> &j, vector<double> &j_gamma_smoothed,
                         vector<double> &j_nu_smoothed, int smooth_gamma_or_nu_or_both, fitshandle &fh_file)
{
   //--- Writes flux and intensity maps as an appended extension to the output file connected to fh_file.
   // Inputs:
   //    arr_pix_fov       vector of Healpix pixel numbers
   //    j                 J-Factor map which will be converted into:
   //                         - flux and intensity column for final state GAMMA
   //                         - flux and intensity column for final state NEUTRINO
   //                      So there will be always four columns present in the extra extension.
   //    j_gamma_smoothed  if a smoothed map is found in the input file, (only!)
   //                      the intensity map for the smoothed map will be written (5th column)
   //    j_nu_smoothed     if a second smoothed map is found (i.e., smoothing had been
   //                      done for gamma and neutrino), the intensity map for this beam
   //                      will be written into the 6th column of the new extension.
   //    smooth_gamma_or_nu_or_both contains the information, if an gamma/and or neutrino
   //                      beam is present.
   //    fh_file           Fitshandle object connected to the output file.


   // If user-unit is ASTRO, we need to convert here (flux requires PP unit)
   bool unit_ref = gSIM_IS_ASTRO_OR_PP_UNITS;
   gSIM_IS_ASTRO_OR_PP_UNITS = false;

   double par_spec[6];
   par_spec[0] = gPP_DM_MASS_GEV;
   par_spec[1] = gPP_FLAG_SPECTRUMMODEL;
   par_spec[2] = kGAMMA;
   par_spec[3] = gSIM_REDSHIFT;
   par_spec[4] = 0.;
   par_spec[5] = 0.; // EBL uncertainty

   double int_area = PI / 3. * pow(gSIM_HEALPIX_NSIDE, -2);

   // allocate arrays according to what is there:

   int n_pix_fov = arr_pix_fov.size();

   vector<double> flux_gamma(n_pix_fov);
   vector<double> flux_nu(n_pix_fov);
   vector<double> intensity_gamma(n_pix_fov);
   vector<double> intensity_nu(n_pix_fov);

   vector<double> flux_gamma_smoothed;
   vector<double> intensity_gamma_smoothed;

   vector<double> flux_nu_smoothed;
   vector<double> intensity_nu_smoothed;

   bool is_smooth_gammaflux = false;
   bool is_smooth_nuflux = false;

   if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3) {
      flux_gamma_smoothed.resize(n_pix_fov);
      intensity_gamma_smoothed.resize(n_pix_fov);
      is_smooth_gammaflux = true;
   }

   if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3) {
      flux_nu_smoothed.resize(n_pix_fov);
      intensity_nu_smoothed.resize(n_pix_fov);
      is_smooth_nuflux = true;
   }

   // calculate fluxes!
   for (int i = 0; i < n_pix_fov; ++i) {
      par_spec[2] = kGAMMA;
      if (j[i] < 2e-40) j[i] = 1e-99;
      flux_gamma[i] = flux(par_spec, gSIM_FLUX_AT_E_GEV, j[i], gSIM_FLUX_IS_INTEG_OR_DIFF);
      if (unit_ref) convert_to_PP_units(1, flux_gamma[i]);
      intensity_gamma[i] = flux_gamma[i] / int_area;
      if (flux_gamma[i] < 1e-80) {
         flux_gamma[i] = 1e-40;
         intensity_gamma[i] = 1e-40;
      }
      if (is_smooth_gammaflux) {
         flux_gamma_smoothed[i] = flux(par_spec, gSIM_FLUX_AT_E_GEV, j_gamma_smoothed[i], gSIM_FLUX_IS_INTEG_OR_DIFF);
         if (unit_ref) convert_to_PP_units(1, flux_gamma_smoothed[i]);
         intensity_gamma_smoothed[i] = flux_gamma_smoothed[i] / int_area;
      }

      par_spec[2] = kNEUTRINO;
      if (gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_EW || gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_NOEW)
         flux_nu[i] = flux(par_spec, gSIM_FLUX_AT_E_GEV, j[i], gSIM_FLUX_IS_INTEG_OR_DIFF);
      else
         flux_nu[i] = 1e-40;
      if (unit_ref) convert_to_PP_units(1, flux_nu[i]);
      intensity_nu[i] = flux_nu[i] / int_area;
      if (flux_nu[i] < 1e-30) {
         flux_nu[i] = 1e-40;
         intensity_nu[i] = 1e-40;
      }
      if (is_smooth_nuflux) {
         if (gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_EW || gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_NOEW)
            flux_nu_smoothed[i] = flux(par_spec, gSIM_FLUX_AT_E_GEV, j_nu_smoothed[i], gSIM_FLUX_IS_INTEG_OR_DIFF);
         else
            flux_nu_smoothed[i] = 1e-40;
         if (unit_ref) convert_to_PP_units(1, flux_nu_smoothed[i]);
         intensity_nu_smoothed[i] = flux_nu_smoothed[i] / int_area;
      }
   }

   // convert back to initial units:
   gSIM_IS_ASTRO_OR_PP_UNITS = unit_ref;

   string units_flux = "cm^-2 s^-1";
   string units_intensity = "cm^-2 s^-1 sr^-1";
   if (!gSIM_FLUX_IS_INTEG_OR_DIFF) {
      units_flux = "cm^-2 s^-1 GeV^-1";
      units_intensity = "cm^-2s^-1sr^-1GeV^-1";
   }
   string unit_comment = "physical unit of field";

   string flux_gamma_label = "Flux_gamma";
   string flux_nu_label =  "Flux_neutrino";
   string intensity_gamma_label = "Intensity_gamma";
   string intensity_nu_label = "Intensity_neutrino";
   string intensity_gamma_beam_label = "Intensity_gamma_beam";
   string intensity_nu_beam_label = "Intensity_nu_beam";

   // append to FITS file:
   if (smooth_gamma_or_nu_or_both == 0) {
      fits_write_clumpymaps(fh_file, arr_pix_fov, flux_gamma, flux_nu, intensity_gamma, intensity_nu) ;
   } else if (smooth_gamma_or_nu_or_both == 1) {
      fits_write_clumpymaps(fh_file, arr_pix_fov, flux_gamma, flux_nu, intensity_gamma, intensity_nu, intensity_gamma_smoothed) ;
      fh_file.set_key("TUNIT6", units_intensity, unit_comment);
      fh_file.set_key("TTYPE6", intensity_gamma_beam_label, "label for field 6");
   } else if (smooth_gamma_or_nu_or_both == 2) {
      fits_write_clumpymaps(fh_file, arr_pix_fov, flux_gamma, flux_nu, intensity_gamma, intensity_nu, intensity_nu_smoothed) ;
      fh_file.set_key("TUNIT6", units_intensity, unit_comment);
      fh_file.set_key("TTYPE6", intensity_nu_beam_label, "label for field 6");
   } else if (smooth_gamma_or_nu_or_both == 3) {
      fits_write_clumpymaps(fh_file, arr_pix_fov, flux_gamma, flux_nu, intensity_gamma, intensity_nu, intensity_gamma_smoothed, intensity_nu_smoothed) ;
      fh_file.set_key("TUNIT6", units_intensity, unit_comment);
      fh_file.set_key("TUNIT7", units_intensity, unit_comment);
      fh_file.set_key("TTYPE6", intensity_gamma_beam_label, "label for field 6");
      fh_file.set_key("TTYPE7", intensity_nu_beam_label, "label for field 7");
   }

   // set all other missing correct units:
   fh_file.goto_hdu(fh_file.num_hdus()); // go to last extension
   fh_file.set_key("TUNIT2", units_flux, unit_comment);
   fh_file.set_key("TUNIT3", units_flux, unit_comment);
   fh_file.set_key("TUNIT4", units_intensity, unit_comment);
   fh_file.set_key("TUNIT5", units_intensity, unit_comment);
   fh_file.set_key("TTYPE2", flux_gamma_label, "label for field 2");
   fh_file.set_key("TTYPE3", flux_nu_label, "label for field 3");
   fh_file.set_key("TTYPE4", intensity_gamma_label, "label for field 4");
   fh_file.set_key("TTYPE5", intensity_nu_label, "label for field 5");

   // set correct extension name:
   string extname ;
   if (gSIM_FLUX_IS_INTEG_OR_DIFF) extname = "INTEGRATED_FLUXES";
   else extname = "DIFFERENTIAL_FLUXES";
   fh_file.set_key("EXTNAME", extname, "name of this binary table extension");

   // do not close FITS file here.

   // Free memory
   vector<double>().swap(flux_gamma);
   vector<double>().swap(flux_nu);

   if (is_smooth_gammaflux) vector<double>().swap(flux_gamma_smoothed);
   if (is_smooth_nuflux) vector<double>().swap(flux_nu_smoothed);

   vector<double>().swap(intensity_gamma);
   vector<double>().swap(intensity_nu);

   if (is_smooth_gammaflux) vector<double>().swap(intensity_gamma_smoothed);
   if (is_smooth_nuflux) vector<double>().swap(intensity_nu_smoothed);

   return;
}

//______________________________________________________________________________
int fits_read_nextensions(fitshandle &fh_file)
{
   //--- Returns the number of extensions in a FITS file.
   // INPUTS:
   //  fh_file  Fitshandle object of the FITS file to check
   // OUTPUTS:
   //  n_ext    number of extensions in the file (without primary extension!)

   // get total number of HDUs:
   string filename = fh_file.fileName();

   int n_ext = fh_file.num_hdus();
   if (n_ext == 0) {
      printf("\n====> ERROR: fits_read_nextensions() in healpix_fits.cc");
      printf("\n             file %s does not contain any FITS extension.", filename.c_str());
      printf("\n             Maybe corrupted or not a proper FITS file?");
      printf("\n             => abort()\n\n");
      // close the file
      fh_file.close();
      abort();
   } else if (n_ext == 1) {
      printf("\n====> ERROR: fits_read_nextensions() in healpix_fits.cc");
      printf("\n             file %s does not seem to contain clumpy output.", filename.c_str());
      printf("\n             => abort()\n\n");
      // close the file
      fh_file.close();
      abort();
   }
   // go to primary HDU
   fh_file.goto_hdu(1);
   // get number of axes
   vector<int64>  fh_axes = fh_file.axes();
   int n_axis = fh_axes.size();

   if (n_axis > 0) {
      // there is a FITS image in the first extension
      printf("\n====> ERROR: fits_read_nextensions() in healpix_fits.cc");
      printf("\n             file %s does not seem to contain clumpy output (it contains a FITS image).", filename.c_str());
      printf("\n             => abort()\n\n");
      // close the file
      fh_file.close();
      abort();
   }
   // fh_file.close(); Do not close here!
   // all checks passed: There is a primary HDU and at least one additional extension: OK!
   n_ext -= 1;

   return n_ext;
}

//______________________________________________________________________________
int fits_read_ncolumns(fitshandle &fh_file, const int i_extension, int &n_side, Healpix_Ordering_Scheme &hp_scheme, int &n_pix)
{
   //--- Returns the number of maps and/or the pixel ordering of a FITS file extension containing HEALPix maps.
   // INPUTS:
   //  fh_file      Fitshandle object of the FITS file to check
   //  i_extension  extension number (starting with one (zero: primary header))
   // OUTPUTS:
   //  n_maps       number of columns in extension i_extension
   //  n_side       Nside parameter of map
   //  hp_scheme    Healpix ordering scheme (RING or NEST)
   //  n_pix        length of map (does not need to be 12 N_side**2 for part-sky maps!)

   string filename = fh_file.fileName();

   char char_tmp[512];

   // get total number of HDUs:
   int n_ext = fh_file.num_hdus();
   if (n_ext == 0) {
      printf("\n====> ERROR: fits_read_ncolumns() in healpix_fits.cc");
      printf("\n             file %s does not contain any FITS extension.", filename.c_str());
      printf("\n             Maybe corrupted or not a proper FITS file?");
      printf("\n             => abort()\n\n");
      // close the file
      fh_file.close();
      abort();
   } else if (n_ext == 1) {
      printf("\n====> ERROR: fits_read_ncolumns() in healpix_fits.cc");
      printf("\n             file %s does not seem to contain clumpy output.", filename.c_str());
      printf("\n             => abort()\n\n");
      // close the file
      fh_file.close();
      abort();
   }

   // go to desired HDU
   fh_file.goto_hdu(i_extension + 1);  // extension labeling starts with primary header = 1.


   int n_maps = fh_file.ncols() ;

   if (n_maps == 0) {
      printf("\n====> ERROR: fits_read_ncolumns() in healpix_fits.cc");
      printf("\n             extension %d of file %s does not contain any map.", i_extension, filename.c_str());
      printf("\n             Maybe corrupted or not a proper FITS file?");
      printf("\n             => abort()\n\n");
      // close the file
      fh_file.close();
      abort();
   } else if (n_maps == 1) {
      sprintf(char_tmp, "Extension %d of file %s contains only a single map. Check INDXSCHM keyword ...", i_extension, filename.c_str());
      print_warning("healpix_fits.cc", "fits_read_ncolumns()", string(char_tmp));
      bool is_indxschm = fh_file.key_present("INDXSCHM");
      if (is_indxschm == true) {
         string indxschm;
         fh_file.get_key("INDXSCHM", indxschm);
         if (indxschm == "IMPLICIT") {
            printf("\n             OK. Extension seems to contain a HEALPix map with implicit ordering.\n\n");
         } else {
            printf("\n====> ERROR: fits_read_ncolumns() in healpix_fits.cc");
            printf("\n             This extension does not contain a proper HEALPix map with implicit ordering.");
            printf("\n             INDXSCHM keyword is:  %s", indxschm.c_str());
            printf("\n             => abort()\n\n");
            // close the file
            fh_file.close();
            abort();
         }
      } else {
         printf("\n====> ERROR: fits_read_ncolumns() in healpix_fits.cc");
         printf("\n             Index scheme (IMPLICIT/EXPLICIT) not specified in the header.");
         printf("\n             This extension seems to not contain a proper HEALPix map.");
         printf("\n             => abort()\n\n");
         // close the file
         fh_file.close();
         abort();
      }

   }

   n_pix = fh_file.nelems(1);

   fh_file.get_key("NSIDE", n_side);

   string scheme_str;
   fh_file.get_key("ORDERING", scheme_str);
   if (scheme_str == "RING") hp_scheme = RING;
   else if (scheme_str == "NESTED") hp_scheme = NEST;
   else {
      printf("\n====> ERROR: fits_read_ncolumns() in healpix_fits.cc");
      printf("\n             HEALPix ordering scheme in extension %d of file %s is neither RING or NESTED", i_extension, filename.c_str());
      printf("\n             => abort()\n\n");
   };

   // do not close file here!
   //fh_file.close();

   // do not count the first field (the pixel number)
   n_maps -= 1;

   return n_maps;
}


void fits_read_fieldinfo(fitshandle &fh_file, const int i_extension, const int i_field, string &extname, string &fieldname, string &phys_unit)
{
   //--- this routine reads the name of the extension and the field and its physical units
   //--- from a cutsky Healpix fits file (that should have been created by write_fits_clumpymaps_f90),
   //--- given the extension and field index of that map.
   // INPUTS:
   //  fh_file      Fitshandle object of the FITS file to check
   //  i_extension  extension number (starting with one (zero: primary header))
   //  i_field      index of field to be read, index 0 will read Healpix pixels.
   // OUTPUTS:
   //  extname      Name of extension/HDU
   //  fieldname    Name of field/column
   //  phys_unit    Name of field unit

   string filename = fh_file.fileName();

   // get total number of HDUs:
   int n_ext = fh_file.num_hdus();
   if (n_ext == 0) {
      printf("\n====> ERROR: fits_read_fieldinfo() in healpix_fits.cc");
      printf("\n             file %s does not contain any FITS extension.", filename.c_str());
      printf("\n             Maybe corrupted or not a proper FITS file?");
      printf("\n             => abort()\n\n");
      // close the file
      fh_file.close();
      abort();
   } else if (n_ext == 1) {
      printf("\n====> ERROR: fits_read_fieldinfo() in healpix_fits.cc");
      printf("\n             file %s does not seem to contain clumpy output.", filename.c_str());
      printf("\n             => abort()\n\n");
      // close the file
      fh_file.close();
      abort();
   }
   // go to desired HDU
   fh_file.goto_hdu(i_extension + 1);  // extension labeling starts with primary header = 1.
   // read parameters:
   fh_file.get_key("EXTNAME", extname);
   fieldname = fh_file.colname(i_field + 1);
   phys_unit = fh_file.colunit(i_field + 1);

   // do not close file here!
   //fh_file.close();

   return;
}

//______________________________________________________________________________
void fits_read_clumpymaps(fitshandle &fh_file, const int i_extension, const int i_field, string &extname, string &fieldname, string &phys_unit, int &n_pix_fov_read, vector<int> &pixnums_fov_read, vector<double> &data_fov_read)
{
   //--- This routine reads a map from a cutsky Healpix fits file (that should have been created by
   //--- fits_write_clumpymaps), given the extension and field index of that map.
   // INPUTS:
   //  fh_file          Fitshandle object of the FITS file to check
   // i_extension       extension number (starting with one (zero: primary header))
   //  i_field          index of field to be read, index 0 will read Healpix pixels.
   // OUTPUTS:
   //  extname          Name of extension/HDU
   //  fieldname        Name of field/column
   //  phys_unit        Name of field unit
   //  n_pix_fov_read   Number of pixels in part-sky map
   //  pixnums_fov_read HEALPix pixel indices of map (with length n_pix_fov_read)
   //  data_fov_read    Values of the HEALPix pixels in map (with length n_pix_fov_read)

   string filename = fh_file.fileName();

   // read meta info:
   fits_read_fieldinfo(fh_file, i_extension, i_field,  extname, fieldname, phys_unit);
   Healpix_Ordering_Scheme hp_scheme; // unused in this function.
   int n_side;
   int n_maps = fits_read_ncolumns(fh_file, i_extension, n_side, hp_scheme, n_pix_fov_read);

   if (n_maps == 0) {
      printf("\n====> ERROR: fits_read_clumpymaps() in healpix_fits.cc");
      printf("\n             extension %d of file %s contains only a single map.", i_extension, filename.c_str());
      printf("\n             This routine requires explicit pixel labeling, i.e. at minimum two maps");
      printf("\n             (one for the pixel numbers, one for pixel values)");
      printf("\n             => abort()\n\n");
      // close the file
      fh_file.close();
      abort();
   } else {
      // check if first map name is PIXEL
      if (fh_file.colname(1) == "PIXEL") printf("      ... found pixel numbers in first column of extension %d.\n", i_extension);
      else {
         printf("\n====> ERROR: fits_read_clumpymaps() in healpix_fits.cc");
         printf("\n             The first column of extension %d of file %s contains ", i_extension, filename.c_str());
         printf("\n             does not have the column name 'PIXEL', so apparently, ");
         printf("\n             the first column does not contain the pixel numbers for an ");
         printf("\n             explicit numbering needed for this function.");
         printf("\n             => abort()\n\n");
         // close the file
         fh_file.close();
         abort();
      }
   }

   // read data with fitsio routines to avoid healpix' read_column and read_entire_column (buggy...):
   fh_file.close();
   fitsfile *fptr;
   int status = 0;
   int hdunum = i_extension + 1;  // extension labeling starts with primary header = 1.
   int anynull, hdutype;
   long frow = 1;
   long felem = 1;
   long nelem = n_pix_fov_read;
   int intnull = 0;
   double floatnull = 0.;
   int *pixnums_fov_read_arr = new int[n_pix_fov_read];
   double *data_fov_read_arr = new double[n_pix_fov_read];
   const char *filename_char  = filename.c_str();

   if (fits_open_file(&fptr, filename_char, READONLY, &status))
      fits_printerror(status);
   if (fits_movabs_hdu(fptr, hdunum, &hdutype, &status))
      fits_printerror(status);

   fits_read_col(fptr, TINT, 1, frow, felem, nelem, &intnull, pixnums_fov_read_arr, &anynull, &status);
   fits_read_col(fptr, TDOUBLE, i_field + 1, frow, felem, nelem, &floatnull, data_fov_read_arr, &anynull, &status);

   if (fits_close_file(fptr, &status))
      fits_printerror(status);

   pixnums_fov_read.assign(pixnums_fov_read_arr, pixnums_fov_read_arr + n_pix_fov_read);
   data_fov_read.assign(data_fov_read_arr, data_fov_read_arr + n_pix_fov_read);

   delete[] pixnums_fov_read_arr;
   pixnums_fov_read_arr = NULL;
   delete[] data_fov_read_arr;
   data_fov_read_arr = NULL;

   fh_file.open(filename);

   return;
}

//______________________________________________________________________________
vector<string> fits_read_params2string(const string &filename, const int i_ext)
{
   //--- Loads all input parameters from the header of extension i_ext in the FITS file
   //    file_name.
   // Inputs:
   //  file_name     Name of FITS file
   //  i_ext         extension number (starting from zero.)


   fitshandle fh_file;
   fh_file.open(filename);

   int n_ext = fits_read_nextensions(fh_file);
   if (i_ext > n_ext) {
      printf("\n====> ERROR: fits_read_params2string() in healpix_fits.cc");
      printf("\n             file contains only %d extensions, selected %dth extension.", n_ext, i_ext);
      printf("\n             => abort()\n\n");
      // close the file
      fh_file.close();
      abort();
   }
   fh_file.goto_hdu(i_ext + 1); // extension labeling starts with 0.

   // get all keys
   vector<string> keylist;
   fh_file.get_all_keys(keylist);

   // get simulation mode:
   string simu_mode_string;
   int enum_simumode_read;
   if (find(keylist.begin(), keylist.end(), gSIM_INPUTPARAMS[kSIM_FLAG_MODE][kSIM_INPUTPARAM_FITSNAME]) != keylist.end()) {
      fh_file.get_key(gSIM_INPUTPARAMS[kSIM_FLAG_MODE][kSIM_INPUTPARAM_FITSNAME], simu_mode_string);
      enum_simumode_read = string_to_enum("FLAG_MODE", "k" + simu_mode_string);
   } else {
      printf("\n====> ERROR: fits_read_params2string() in healpix_fits.cc");
      printf("\n             key %s missing in FITS file header of extension %d.", gSIM_INPUTPARAMS[kSIM_FLAG_MODE][kSIM_INPUTPARAM_FITSNAME], i_ext);
      printf("\n             => abort()\n\n");
      // close the file
      fh_file.close();
      abort();
   }

   // write the read keywords into a non-global string
   vector<string> inputparam_valuestring(gN_INPUTPARAMS, "-999");

   // now loop through the header keywords:
   vector<bool> is_filled(gN_INPUTPARAMS, false);
   vector<bool> is_needed_dummy(gN_INPUTPARAMS); // dummy: purpose is to check for irrelevant parameters, not needed here.


   // iterate three times to get dependent parameters:
   for (int k = 0; k < 3; ++k) {
      // iterate three times:
      // 1.) get dependent parameters
      // 2.) set values of dependent variables missed in first round (as they weren't known to be required)
      // 3.) get and set parameters dependent of values from other parameters
      if (k == 2) get_dependent_required_params(999, inputparam_valuestring, enum_simumode_read, is_needed_dummy);

      // fill input values from fits file into ordered vector "inputparam_valuestring":
      for (int i = 0; i < gN_INPUTPARAMS; ++i) {
         for (int j = 0; j < (int)keylist.size(); ++j) {
            if (gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_FITSNAME] == keylist[j]) {
               is_filled[i] = true;
               // extract only parameters which are needed by the simulation with enum_simumode_read
               fh_file.get_key(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_FITSNAME], inputparam_valuestring[i]);
               // correct for boolean variables:
               if (inputparam_valuestring[i] == "F") inputparam_valuestring[i] = "0";
               if (inputparam_valuestring[i] == "T") inputparam_valuestring[i] = "1";
               // remove final dot:
               if (inputparam_valuestring[i][inputparam_valuestring[i].size() - 1] == '.')
                  inputparam_valuestring[i] = inputparam_valuestring[i].substr(0, inputparam_valuestring[i].size() - 1);
               // check for variables which demand further specifications depending on value.
               // and add values to gPARAMS_REQUIRED:
               if (k == 0 or k == 2) get_dependent_required_params(i, inputparam_valuestring, enum_simumode_read, is_needed_dummy);
            }
         }
      }
//     for (int i = 0; i < (int)gPARAMS_REQUIRED[enum_simumode_read].size(); ++i) {
//      cout << "required: " << gSIM_INPUTPARAMS[gPARAMS_REQUIRED[enum_simumode_read][i]][kSIM_INPUTPARAM_VARNAME] << endl;
//     }
//     abort();
   }

   // write enum_simumode_read into string:
   inputparam_valuestring[kSIM_FLAG_MODE] = simu_mode_string;

   // read also HEALPix compliant parameters:
   if (find(keylist.begin(), keylist.end(), string("ORDERING")) != keylist.end()) {
      fh_file.get_key("ORDERING", inputparam_valuestring[kSIM_HEALPIX_SCHEME]);
      if (inputparam_valuestring[kSIM_HEALPIX_SCHEME] != "NESTED") {
         if (inputparam_valuestring[kSIM_HEALPIX_SCHEME] != "RING") {
            printf("\n====> ERROR: fits_read_params2string() in healpix_fits.cc");
            printf("\n             gSIM_HEALPIX_SCHEME must be either RING or NESTED.\n");
            cout << "             Is: " << inputparam_valuestring[kSIM_HEALPIX_SCHEME] << endl;
            printf("\n             => abort()\n\n");
            abort();
         }
      }
   }

   // special treatment for the SM branching channels:
   string branching_ratios_list = "";
   bool is_branching_channels = false;
   for (int i = 0; i < gN_PP_BR; ++i) {
      char char_tmp[25];
      string this_branching_ratio = "0";
      sprintf(char_tmp, "%02d", i);
      string key_check = "BR_" + string(char_tmp);
      if (find(keylist.begin(), keylist.end(), key_check) != keylist.end()) {
         is_branching_channels = true;
         fh_file.get_key(key_check, this_branching_ratio);
      }
      branching_ratios_list += this_branching_ratio;
      branching_ratios_list += ",";
   }

   if (is_branching_channels) {
      inputparam_valuestring[kPP_BR] = branching_ratios_list;
      is_filled[kPP_BR] = true;
   }

   //  for (int i = 0; i < gN_INPUTPARAMS; ++i) {
   //    cout << gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME] << "\t" << inputparam_valuestring[i] << "\t filled: " << is_filled[i]  << "\t needed: " << is_needed[i]  << endl;
   // }


   // now check if parameters are missing:
   bool is_something_missing = false;
   for (int i = 0; i < (int)gPARAMS_REQUIRED[enum_simumode_read].size(); ++i) {
      if (!is_filled[gPARAMS_REQUIRED[enum_simumode_read][i]]) {
         is_something_missing = true;
         break;
      }
   }
   if (is_something_missing) {
      cout << "\n====> ERROR: fits_read_params2string() in healpix_fits.cc" << endl;
      cout << "             The following parameter keys are missing in the input FITS file " << filename.c_str() <<  endl;
      cout << "             and should be present for copying this header."  << endl;
      cout << string_fixlength("# FITS header keyword", 40) << string_fixlength("Unit", 14) << string_fixlength("Value", 17) << "(Comment)" << endl << endl;

      vector<bool> is_printed(gN_INPUTPARAMS, false); // to check already printed missing values

      for (int i = 0; i < (int)gPARAMS_REQUIRED[enum_simumode_read].size(); ++i) {
         if (!is_filled[gPARAMS_REQUIRED[enum_simumode_read][i]] and  !is_printed[gPARAMS_REQUIRED[enum_simumode_read][i]]) {
            cout << string_fixlength(gSIM_INPUTPARAMS[gPARAMS_REQUIRED[enum_simumode_read][i]][kSIM_INPUTPARAM_FITSNAME], 40)
                 << string_fixlength(gSIM_INPUTPARAMS[gPARAMS_REQUIRED[enum_simumode_read][i]][kSIM_INPUTPARAM_UNIT], 14)
                 << string_fixlength("<" + string(gSIM_INPUTPARAMS[gPARAMS_REQUIRED[enum_simumode_read][i]][kSIM_INPUTPARAM_DATATYPE]) + ">", 17)
                 << gSIM_INPUTPARAMS[gPARAMS_REQUIRED[enum_simumode_read][i]][kSIM_INPUTPARAM_DESCRIPTION] << endl;
            is_printed[gPARAMS_REQUIRED[enum_simumode_read][i]] = true;
         }
      }
      printf("\n             => abort()\n\n");
      abort();
   }

   return inputparam_valuestring;
}


//______________________________________________________________________________
void fits_write_key_from_inputparams(fitshandle &fh_file, const string &keyname, const string &value, const string &type, const  string &unit, const string &comment)
{
   //--- This routine sets a key in the header of the file connected to fh_file,
   //--- by reading the vector of strings of all input values and transforming the string values
   //    into the correct datatype according to type. Additionally, the unit will be added to
   //    the comment string.
   // INPUTS:
   //  fh_file          Fitshandle object of the FITS file to write
   //  keyname          Name of key to write
   //  value            value of the key (in string format)
   //  type             datatype of value in string format according to gSIM_INPUTPARAMS
   //  unit             unit of key according to gSIM_INPUTPARAMS, will be put in front of comment
   //  comment          comment to the key according to gSIM_INPUTPARAMS

   // add unit in front of comment in case it not 'none':
   //   cout << type << " " << value << endl;
   string comment_withunit;
   if (unit != "[-]") comment_withunit = unit + ", " + comment;
   else comment_withunit = comment;

   string datatype = type;
   if (value == "kHOST") datatype = "string";

   string value_tmp = value;
   if (keyname.substr(0, 4) == "LIST" and value.size() > 67)
      value_tmp = value.substr(value.find_last_of("\\/") + 1, value.size());

   // set key:
   char char_tmp[200];
   if (datatype[0] == 'f') {
      if (keyname == "SIZE_X" and gSIM_THETA_ORTH_SIZE_DEG == "d") {
         sprintf(char_tmp, "%.6g", gSIM_THETA_SIZE * RAD_to_DEG);
         value_tmp = string(char_tmp);
      } else if (keyname == "SIZE_Y" and gSIM_THETA_ORTH_SIZE_DEG == "d") {
         double dtheta_deg = gSIM_THETA_SIZE * RAD_to_DEG;
         if (dtheta_deg > 180.) dtheta_deg = 180.;
         sprintf(char_tmp, "%.6g", dtheta_deg);
         value_tmp = string(char_tmp);
      } else if (keyname == "THETA_0" and gSIM_THETA_OBS_DEG == "s") {
         double theta0 = 0.;
         sprintf(char_tmp, "%g", theta0);
         value_tmp = string(char_tmp);
      }
      fh_file.set_key(keyname, atof(value_tmp.c_str()), comment_withunit);
   } else if (datatype[0] == 'i') {
      fh_file.set_key(keyname, atoi(value_tmp.c_str()), comment_withunit);
   } else if (datatype[0] == 's') {
      fh_file.set_key(keyname, value_tmp, comment_withunit);
   } else if (datatype[0] == 'b') {
      fh_file.set_key(keyname, bool(atoi(value.c_str())), comment_withunit);
   } else if (datatype[0] == 'c') { // comma-separated list
      vector<double> list;
      string2list(value_tmp, ",", list);
      for (int k = 0; k < (int)list.size(); ++k) {
         sprintf(char_tmp, "%02d", k);
         string keyname_k = keyname + string(char_tmp);
         string comment_extended;
         if (keyname == "BR_") {
            comment_extended = string(gNAMES_PP_BR[k]) + " " + comment_withunit;
         } else {
            comment_extended = comment_withunit;
         }
         fh_file.set_key(keyname_k, list[k], comment_extended);
      }
   }
   return;
}

//______________________________________________________________________________
void fits_params2reproduction(string &filename, int &i_extension)
{

   // read file:
   ifstream isfile(filename.c_str());
   if (!isfile) {
      printf("\n====> ERROR: fits_params2reproduction() in healpix_fits.cc");
      printf("\n             File %s does not exist.", filename.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }

   cout << ">>>>> read header from " << i_extension << " in file " << filename << " ..."  << endl;

   string extname_str;
   int i_field = 0;
   string fieldname_str;
   string fieldunits_str;

   string filename_out = filename.substr(0, filename.size() - 5);

   fitshandle fh_file ;
   fh_file.open(filename);

   int n_extensions = fits_read_nextensions(fh_file);
   if (i_extension > n_extensions) {
      printf("\n====> ERROR: fits_params2reproduction() in healpix_fits.cc");
      printf("\n             FITS file contains only %d extensions.", n_extensions);
      printf("\n             => abort()\n\n");
      abort();
   }

   fits_read_fieldinfo(fh_file, i_extension, i_field, extname_str, fieldname_str, fieldunits_str);

   vector<string> input_params = fits_read_params2string(filename, i_extension);
   inputparameters_string2globalparams(input_params);

   filename_out += "_";
   filename_out += extname_str;
   filename_out += "_params.txt";

   inputparameters_write_paramfile(filename_out, gSIM_FLAG_MODE, true, filename);
   return;
}


//______________________________________________________________________________
void fits_printerror(const int status)
{
   /*****************************************************/
   /* Print out cfitsio error messages and exit program */
   /*****************************************************/


   if (status) {
      fits_report_error(stderr, status); /* print error report */

      exit(status);      /* terminate the program, returning error status */
   }
   return;
}


//______________________________________________________________________________
void hp_get_powspec(Healpix_Map<double> &map, const string &weights_dir, PowSpec &spectrum)
{
   //--- Returns the angular power spectrum of the map 'map'.
   // INPUTS:
   //  map          Healpix_Map object containing a full-sky dataset.
   //  weights_dir  Directory of ring weights shipped with the Healpix package
   //               for improved quadrature mode.
   //  spectrum     Uninitialized PowerSpectrum object (will be overwritten, if already initialized).
   // OUTPUTS:
   //  spectrum     1-column PowerSpectrum object containing the angular power spectrum

   bool is_get_powspec = true;
   double fwhm = 0.;
   hp_smooth_or_powspec(map, fwhm, weights_dir, is_get_powspec, spectrum);
}

//______________________________________________________________________________
double hp_nside2resol(const int n_side)
{
   //--- Returns the resolution [rad] of a map with given NSIDE.
   //    As it implemented now, this is just the T_Healpix_Base.max_pixrad() function,
   //    which gives the maximum distance of any pixel center to the its corners.
   // INPUTS:
   //  n_side       Healpix map resolution.
   // OUTPUTS:
   //  the maximum distance of any pixel center to the its corners [rad].

   T_Healpix_Base<int> hp_gridprop;
   hp_gridprop.SetNside(n_side, gSIM_HEALPIX_SCHEME);
   //old:
   //int n_pix = hp_gridprop.Npix();
   //double delta_omega = 4 * PI / n_pix;
   //double pixrad = acos(1 - delta_omega / (2 * PI)) ;
   //double pixrad = sqrt(delta_omega) ;
   double pixrad = hp_gridprop.max_pixrad();
   return pixrad;
}

//______________________________________________________________________________
int hp_resol2nside(const double pixrad)
{
   //--- Returns the NSIDE according to given resolution pixel radius (in [rad]),
   //    such that the maximum distance of any Healpix pixel center to its corners
   //    becomes smaller than pixrad (all pixels are fully contained within a
   //    a circle with radius pixrad).
   //    Moreover, only NSIDE = power of 2 are chosen. The resulting NSIDE thus corresponds
   //    only approximately to the input pixrad, by yielding a resolution better or
   //    at least equal to the input pixrad, i.e., most probably you won't find a pixel
   //    really touching the circumscribed circle with radius pixrad.
   //    For a given NSIDE, you can return the exact pixrad of the smallest circle
   //    compassing all Healpix pixels with the hp_nside2resol function.
   //
   // INPUTS:
   //  pixrad       radius of a circle that should comprise all map pixels.
   // OUTPUTS:
   //  the NSIDE of the most coarse resolution with NSIDE = 2^n fullfilling this.

   int n_side = 1;
   T_Healpix_Base<int> hp_gridprop;
   double pixrad_tmp = PI;
   hp_gridprop.SetNside(1, gSIM_HEALPIX_SCHEME);
   // increase gSIM_HEALPIX_NSIDE, until a circular pixel radius is smaller than input pixrad
   // and the maximum distance to any Healpix pixel corner is smaller than the input pixrad.
   int n_side_exp = 0 ;
   while (pixrad_tmp > pixrad) {
      n_side_exp += 1;
      if (n_side_exp > 13) {
         printf("\n====> ERROR: hp_resol2nside() in healpix_fits.cc");
         printf("\n             NSIDE exceeds 2^13 = 8192, which is currently not supported.");
         printf("\n             => abort()\n\n");
         abort();
      }
      n_side = int(pow(2., n_side_exp));
      hp_gridprop.SetNside(n_side, gSIM_HEALPIX_SCHEME);
      // old:
      /* int n_pix = hp_gridprop.Npix();
      double delta_omega = 4 * PI / n_pix;
      pixrad = acos(1 - delta_omega / (2 * PI));*/
      pixrad_tmp = hp_gridprop.max_pixrad();
   }
   return n_side;
}

//______________________________________________________________________________
int hp_ring_above(const double z, const int n_side)
{
   //--- Returns the index of the next ring to the north of z=cos(theta0) ("nth ring").
   //    It may return 0; in this case z lies north of all rings.
   //    N.B.: exact copy of the protected member function T_Healpix_Base< I >::ring_above( double  z )
   //    of Healpix v3.11 (Definition at line 53 of file healpix_base.cc.). Because it is protected,
   //    it cannot be accessed by remote code.
   // INPUTS:
   //  z            z = cos(theta0) z-coordinate on a unit sphere for theta: [0, PI].
   //  n_side       Healpix resolution.
   // OUTPUTS:
   //  the amount of rings with theta < theta0.
   // (As in Feb. 2015, this function is nowhere used by Clumpy)

   double az = abs(z);
   if (az <= 0.66666666666) // equatorial region
      return int (n_side * (2 - 1.5 * z));
   int i_ring = int(n_side * sqrt(3 * (1 - az)));
   return (z > 0) ? i_ring : 4 * n_side - i_ring - 1;
}

//______________________________________________________________________________
rangeset<int> hp_set_pix_fov(const string &grid_mode, const double &psi, const double &theta,
                             const double &dtheta_orth, const double &dtheta, const int &n_side, const Healpix_Ordering_Scheme &hp_scheme)
{
   //--- Returns a rangeset object containing all pixel numbers of the Healpix pixels
   //    within the field of view. The rangeset objects store just the range (beginning and
   //    end of a sequence) of subsequent integers.
   // INPUTS:
   //   grid_mode_tmp: string, either "DISK", "STRIP" or "RECT"
   //   psi_tmp [rad]:  psi_tmp center of FOV
   //   theta_tmp [rad]: theta_tmp center of FOV
   //   dtheta_orth_tmp [rad]: full width of FOV perpendicular to theta_tmp direction.
   //   dtheta_tmp [rad]: full width of FOV in theta_tmp direction.
   //   n_side [int]: Healpix n_side parameter of the map (corresponds to the resolution).
   //   Healpix_Ordering_Scheme: either RING or NEST.
   // OUTPUTS:
   //   rs_pix_fov: rangeset containing the Healpix pixel numbers of the FOV grid

   T_Healpix_Base<int> hp_gridprop(n_side, hp_scheme, SET_NSIDE) ;

   rangeset<int>  rs_pix_fov;
   rangeset<int>  rs_pix_all;
   rs_pix_all.append(0, hp_gridprop.Npix());

   // store input coordinates for later returning them unchanged:
   double psi_tmp = psi ;
   double theta_tmp = theta ;
   double dtheta_orth_tmp = dtheta_orth ;
   double dtheta_tmp = dtheta ;
   string grid_mode_tmp = grid_mode ;

   bool is_inversion = false;
   bool is_shortcut = false;

   // check input values (also to give future developers a better understandable
   // error message when exceeding the allowed values):
   if (fabs(theta_tmp * RAD_to_DEG) > 180. + SMALL_NUMBER) {
      printf("\n====> ERROR: hp_set_pix_fov() in healpix_fits.cc");
      printf("\n             FOV center exceeding |theta_tmp|>90 [deg] makes no sense.");
      printf("\n             => abort()\n\n");
      abort();
   }

   // special case: limit semi-sphere in RECT mode:
   if (grid_mode_tmp == "RECT" && fabs(dtheta_tmp) == PI && fabs(dtheta_orth_tmp) == PI) {
      grid_mode_tmp = "DISK" ;
   }

   // DISK mode!
   if (grid_mode_tmp == "DISK")  {
      // check input values:
      if (fabs(dtheta_tmp * RAD_to_DEG) > 360. + SMALL_NUMBER) {
         printf("\n====> ERROR: hp_set_pix_fov() in healpix_fits.cc");
         printf("\n             Disk diameter |d|>360 [deg] makes no sense.");
         printf("\n             => abort()\n\n");
         abort();
      }

      // invert FOV, if chosen by negative grid dimensions:
      if (dtheta_tmp < 0.) {
         dtheta_tmp = - dtheta_tmp;
         is_inversion = true;
         // ...but not if dtheta_tmp is "minus zero", draw full sky FOV then:
         if (fabs(dtheta_tmp) < gSIM_RESOLUTION) {
            dtheta_tmp = 2 * PI;
            is_inversion = false;
         }
      }

      if (dtheta_tmp > PI) {
         // calculate FOV by inversion for radii > 90. to fix bad behaviour for very large angles:
         theta_tmp = - theta_tmp;
         psi_tmp = psi_tmp + PI;
         dtheta_tmp = 2 * PI - dtheta_tmp;
         if (is_inversion) is_inversion = false;
         else is_inversion = true;
         // ...but not if dtheta_tmp is "minus zero", draw full sky FOV then:
         if (fabs(dtheta_tmp) < gSIM_RESOLUTION) {
            dtheta_tmp = 2 * PI;
            is_inversion = false;
         }
      }

      double radius = dtheta_tmp / 2.;
      pointing ptg_gridcenter = pointing(PI / 2. - theta_tmp, psi_tmp);
      hp_gridprop.query_disc(ptg_gridcenter, radius, rs_pix_fov);  // fills healpixFOVpixels with the corresponding pixel numbers
   }

   // STRIP mode, region limited by constant latitudes and constant longitudes.
   if (grid_mode_tmp == "STRIP")  {
      // remember, for theta_tmp = 0 dtheta_orth_tmp = dpsi!

      // check input values:
      if (fabs(dtheta_orth_tmp * RAD_to_DEG) > 360. + SMALL_NUMBER || fabs(dtheta_tmp * RAD_to_DEG) > 180. + SMALL_NUMBER) {
         printf("\n====> ERROR: hp_set_pix_fov() in healpix_fits.cc");
         printf("\n             Strip exceeding |dpsi| = |%f| > 360 [deg] or |dtheta_tmp| = |%f| > 180 [deg] causes troubles.", dtheta_orth_tmp * RAD_to_DEG, dtheta_tmp * RAD_to_DEG);
         printf("\n             => abort()\n\n");
         abort();
      }

      // invert FOV, if chosen by negative grid dimensions:
      if (dtheta_tmp < 0. || dtheta_orth_tmp < 0.) {
         // exclude special cases:
         if ((dtheta_tmp < 0. && fabs(dtheta_tmp) < gSIM_RESOLUTION) || (dtheta_orth_tmp < 0. && fabs(dtheta_orth_tmp) < gSIM_RESOLUTION)) {
            // draw fullsky!
            rs_pix_fov = rs_pix_all ;
            is_shortcut = true;
         } else {
            // the most common case...
            dtheta_tmp = fabs(dtheta_tmp);
            dtheta_orth_tmp = fabs(dtheta_orth_tmp);
            is_inversion = true;
         }

      }

      if (fabs(dtheta_tmp) == PI && fabs(dtheta_orth_tmp) == 2 * PI) {
         // special case for fullsky (or in the inverse mode, nothing...)
         rs_pix_fov = rs_pix_all ;
      } else if (!is_shortcut) {
         // the most common case: switch to RING ordering for STRIP mode:
         hp_gridprop.SetNside(n_side, RING);

         // Properties of the RING ordered Healpix grid:

         pointing    ptg_theta_max(PI / 2 - theta_tmp - dtheta_tmp / 2, psi_tmp) ;
         pointing    ptg_theta_min(PI / 2 - theta_tmp + dtheta_tmp / 2, psi_tmp) ;

         int i_pix_ringmin_fov = hp_gridprop.ang2pix(ptg_theta_max) ;
         int i_pix_ringmax_fov = hp_gridprop.ang2pix(ptg_theta_min) ;

         int i_ringmin_fov = hp_gridprop.pix2ring(i_pix_ringmin_fov) ;
         int i_ringmax_fov = hp_gridprop.pix2ring(i_pix_ringmax_fov) ;

         if (dtheta_orth_tmp == PI) dtheta_orth_tmp -= SMALL_NUMBER; // fix odd behavior for dtheta_orth_tmp == PI

         bool is_inversion_strip = false;
         bool is_shifted = false;
         check_psi(psi_tmp) ;
         double psi_min = psi_tmp - dtheta_orth_tmp / 2;
         double psi_max = psi_tmp + dtheta_orth_tmp / 2;

         if (psi_min < 0.  && psi_max > 0.) {
            double tmp = psi_min  ;
            psi_min = psi_max;
            psi_max = 2 * PI + tmp ;
            is_inversion_strip = true;
         } else if (psi_min < 0.  && psi_max < 0.) {
            psi_min = 2 * PI + psi_min;
            psi_max = 2 * PI + psi_max;
            is_inversion_strip = false;
         } ;

         double frac_psi_min = psi_min / (2 * PI) ;
         double frac_psi_max = psi_max / (2 * PI) ;

         int i_pix_start ;
         int n_pix_ring ;

         for (int i_ring = i_ringmin_fov; i_ring <= i_ringmax_fov; ++i_ring) {
            hp_gridprop.get_ring_info_small(i_ring, i_pix_start, n_pix_ring, is_shifted);
            if (is_inversion_strip == true) {
               rs_pix_fov.add(i_pix_start, i_pix_start + n_pix_ring);
               if (i_ring % 2 == 0) {
                  rs_pix_fov.remove(i_pix_start + (int)(frac_psi_min * n_pix_ring) + 1, i_pix_start + (int)(frac_psi_max * n_pix_ring));
               } else rs_pix_fov.remove(i_pix_start + (int)(frac_psi_min * n_pix_ring) + 1, i_pix_start + (int)(frac_psi_max * n_pix_ring) + 1);
            } else {
               if (i_ring % 2 == 0) {
                  rs_pix_fov.add(i_pix_start + (int)(frac_psi_min * n_pix_ring), i_pix_start + (int)(frac_psi_max * n_pix_ring) + 1);
               } else  rs_pix_fov.add(i_pix_start + (int)(frac_psi_min * n_pix_ring) + 1, i_pix_start + (int)(frac_psi_max * n_pix_ring) + 1);
            } ;
         }
         if (hp_scheme == NEST) {
            cout << "      strip creation in NESTED scheme can be a bit slow for large FOV, please be patient..."  << endl;
            vector<int> v_pix_fov;
            rs_pix_fov.toVector(v_pix_fov);
            rangeset<int>  rs_pix_fov_NEST;
            for (int i = 0; i < rs_pix_fov.nval(); ++i) {
               int i_pix = (int)hp_gridprop.ring2nest(v_pix_fov[i]);
               rs_pix_fov_NEST.add(i_pix);
            }
            rs_pix_fov = rs_pix_fov_NEST;
            // switch back to NEST mode:
            hp_gridprop.SetNside(n_side, NEST);
         }
      }
   }

   // RECT mode, rectangular area limited by 4 great circles
   if (grid_mode_tmp == "RECT")  {
      // N.B. : Because of the non-commutativity of the rotation (matrices)
      // on a sphere, we can choose two different realizations of such a
      // rectangular FOV: either
      // 1. the diameter of the FOV through the FOV center in theta_orth-
      //    direction is dtheta_orth_tmp, and the distances of the FOV corners
      //    in theta_tmp-direction are dtheta_tmp.
      // or
      // 2. the diameter of the FOV through the FOV center in theta_tmp-direction
      //    is dtheta_tmp, and the distances of the FOV corners in theta_orth-
      //    direction are dtheta_orth_tmp.
      // We choose the 1. realization. Mathematically, this corresponds to the
      //  matrix multiplication below rotmat_cornerX = rotmat_zprime * rotmat_y.
      //  which is not the same as rotmat_y * rotmat_zprime.

      // check input values:
      if (fabs(dtheta_tmp) == PI && fabs(dtheta_orth_tmp) == PI) {
         // do nothing; this is ok.
      } else if (fabs(dtheta_tmp) >= PI || fabs(dtheta_orth_tmp) >= PI) {
         printf("\n====> ERROR: hp_set_pix_fov() in healpix_fits.cc");
         printf("\n             Rectangular grid for |dtheta_orth_tmp| or |dtheta_tmp| >= 180 [deg] not supported");
         printf("\n             (exception |dtheta_orth_tmp| = |dtheta_tmp| = 180 [deg] is possible).");
         printf("\n             => abort()\n\n");
         abort();
      }

      // invert FOV, if chosen by negative grid dimensions:
      if (dtheta_tmp < 0. || dtheta_orth_tmp < 0.) {
         // exclude special cases:
         if ((dtheta_tmp < 0. && fabs(dtheta_tmp) < gSIM_RESOLUTION) || (dtheta_orth_tmp < 0. && fabs(dtheta_orth_tmp) < gSIM_RESOLUTION)) {
            // draw fullsky!
            rs_pix_fov = rs_pix_all ;
            is_shortcut = true;
         } else {
            // the most common case...
            dtheta_tmp = fabs(dtheta_tmp);
            dtheta_orth_tmp = fabs(dtheta_orth_tmp);
            is_inversion = true;
         }
      }

      if (!is_shortcut) {
         // vectors along the coordinate axes:
         vec3_t<double> vec3_x_axis;
         vec3_x_axis.Set(1, 0, 0) ;
         vec3_t<double> vec3_y_axis;
         vec3_y_axis.Set(0, 1, 0) ;
         vec3_t<double> vec3_z_axis;
         vec3_z_axis.Set(0, 0, 1) ;
         vec3_t<double> vec3_x_axis_prime;
         vec3_x_axis_prime.Set(1, 0, 0) ;
         vec3_t<double> vec3_z_axis_prime;
         vec3_z_axis_prime.Set(0, 0, 1) ;

         // create rotation matrices for the center of FOV:
         rotmatrix rotmat_yprime;
         rotmatrix rotmat_z;

         // ... and for the corners:
         rotmatrix rotmat_y_pos;
         rotmatrix rotmat_y_neg;
         rotmatrix rotmat_zprime_pos;
         rotmatrix rotmat_zprime_neg;

         rotmatrix rotmat_corner1prime;
         rotmatrix rotmat_corner2prime;
         rotmatrix rotmat_corner3prime;
         rotmatrix rotmat_corner4prime;
         rotmatrix rotmat_corner1prime2;
         rotmatrix rotmat_corner2prime2;
         rotmatrix rotmat_corner3prime2;
         rotmatrix rotmat_corner4prime2;

         // first, stay in the psi_tmp = 0 plane and rotate x- and z-axes
         // around y-axis around -theta_tmp into system prime:
         rotmat_yprime.Make_Axis_Rotation_Transform(vec3_y_axis, - theta_tmp);
         rotmat_yprime.Transform(vec3_x_axis, vec3_x_axis_prime) ;
         rotmat_yprime.Transform(vec3_z_axis, vec3_z_axis_prime) ;

         // define the rotations for dtheta_orth_tmp and dtheta_tmp...
         rotmat_y_pos.Make_Axis_Rotation_Transform(vec3_y_axis, - dtheta_tmp / 2);
         rotmat_y_neg.Make_Axis_Rotation_Transform(vec3_y_axis,  dtheta_tmp / 2);
         rotmat_zprime_pos.Make_Axis_Rotation_Transform(vec3_z_axis_prime, dtheta_orth_tmp / 2);
         rotmat_zprime_neg.Make_Axis_Rotation_Transform(vec3_z_axis_prime, - dtheta_orth_tmp / 2);

         // ... and combine them to the rotations for the four corners:

         // rotmat_zprime_pos * rotmat_y_pos
         matmult(rotmat_zprime_pos, rotmat_y_pos, rotmat_corner1prime) ; // north east corner
         matmult(rotmat_zprime_neg, rotmat_y_pos, rotmat_corner2prime) ;
         matmult(rotmat_zprime_neg, rotmat_y_neg, rotmat_corner3prime) ;
         matmult(rotmat_zprime_pos, rotmat_y_neg, rotmat_corner4prime) ;

         // finally, rotate all corners around the z-axis by angle psi_tmp into final system prime2:
         rotmat_z.Make_Axis_Rotation_Transform(vec3_z_axis, psi_tmp);
         matmult(rotmat_z, rotmat_corner1prime, rotmat_corner1prime2) ; // north east corner
         matmult(rotmat_z, rotmat_corner2prime, rotmat_corner2prime2) ;
         matmult(rotmat_z, rotmat_corner3prime, rotmat_corner3prime2) ;
         matmult(rotmat_z, rotmat_corner4prime, rotmat_corner4prime2) ;

         // rotate x_prime-axis vector to the four corners of the FOV:
         vec3_t<double> vec3_corner1;
         vec3_t<double> vec3_corner2;
         vec3_t<double> vec3_corner3;
         vec3_t<double> vec3_corner4;

         rotmat_corner1prime2.Transform(vec3_x_axis_prime, vec3_corner1);
         rotmat_corner2prime2.Transform(vec3_x_axis_prime, vec3_corner2);
         rotmat_corner3prime2.Transform(vec3_x_axis_prime, vec3_corner3);
         rotmat_corner4prime2.Transform(vec3_x_axis_prime, vec3_corner4);

         // transform vectors into pointing objects:
         pointing ptg_corner1;
         pointing ptg_corner2;
         pointing ptg_corner3;
         pointing ptg_corner4;

         ptg_corner1.from_vec3(vec3_corner1) ;
         ptg_corner2.from_vec3(vec3_corner2) ;
         ptg_corner3.from_vec3(vec3_corner3) ;
         ptg_corner4.from_vec3(vec3_corner4) ;

         // create polygon
         vector<pointing> poly_fov_rect;
         poly_fov_rect.push_back(ptg_corner1);
         poly_fov_rect.push_back(ptg_corner2);
         poly_fov_rect.push_back(ptg_corner3);
         poly_fov_rect.push_back(ptg_corner4);

         // create FOV!
         hp_gridprop.query_polygon(poly_fov_rect, rs_pix_fov);
      }
   }

   // invert FOV, if inversion applied:
   if (is_inversion == true) {
      rs_pix_fov = rs_pix_all.op_andnot(rs_pix_fov) ;
   }

   // return error if FOV is empty:
   if (rs_pix_fov.nval() == 0 || dtheta_orth == 0. || dtheta == 0.) {

      printf("\n====> ERROR: hp_set_pix_fov() in healpix_fits.cc");
      printf("\n             Chosen field of view contains no pixels.");
      printf("\n             This may happen for too small FOV at too low resolutions.");
      printf("\n             => abort()\n\n");
      abort();
   }

   return rs_pix_fov;
}

//______________________________________________________________________________
void hp_set_resolution(int &n_pix, double &delta_omega, int &smooth_gamma_or_nu_or_both, bool is_sz)
{
   //--- Returns the full sphere pixel number n_pix and the pixel size delta_omega, and
   //    correctly sets all other resolution parameters if some of them were chosen as -1.
   // INPUTS:
   //   gSIM_HEALPIX_NSIDE
   //   gSIM_ALPHAINT   [rad]
   //   gSIM_GAUSSBEAM_GAMMA_FWHM   [rad]
   // OUTPUTS:
   //   gSIM_HEALPIX_SCHEME (will be set to NESTED, if gSIM_HEALPIX_NSIDE = power of two, otherwise RING)
   //   gSIM_HEALPIX_NSIDE  (will only be set/changed if input value was DYNAMIC or -1)
   //   gSIM_ALPHAINT  (will be set/changed if input value was -1)
   //   gSIM_GAUSSBEAM_GAMMA_FWHM  (will be set/changed if input value was -1)
   //   n_pix
   //   delta_omega [sr]
   //   smooth_gamma_or_nu_or_both (whether FWHM of gamma and/or neutrino beam is set)

   T_Healpix_Base<int> hp_gridprop;

   char char_tmp[512];

   // the user may choose a beam smoothing gSIM_GAUSSBEAM_GAMMA_FWHM and/or gSIM_GAUSSBEAM_NEUTRINO_FWHM.
   // Only one of both is needed to compare with the other map resolution defining parameters
   // gSIM_ALPHAINT and gSIM_HEALPIX_NSIDE. So make the comparison easier:
   double beam_input_min ;
   bool is_gamma_constraint = true ;
   if (gSIM_GAUSSBEAM_GAMMA_FWHM != -1 && gSIM_GAUSSBEAM_NEUTRINO_FWHM != -1) {
      beam_input_min = min(gSIM_GAUSSBEAM_GAMMA_FWHM, gSIM_GAUSSBEAM_NEUTRINO_FWHM) ;
      smooth_gamma_or_nu_or_both = 3 ;
      if (gSIM_GAUSSBEAM_GAMMA_FWHM < gSIM_GAUSSBEAM_NEUTRINO_FWHM) is_gamma_constraint = true;
      else is_gamma_constraint = false;
   } else if (gSIM_GAUSSBEAM_GAMMA_FWHM != -1) {
      beam_input_min = gSIM_GAUSSBEAM_GAMMA_FWHM;
      smooth_gamma_or_nu_or_both = 1 ;
   } else if (gSIM_GAUSSBEAM_NEUTRINO_FWHM != -1) {
      beam_input_min = gSIM_GAUSSBEAM_NEUTRINO_FWHM;
      smooth_gamma_or_nu_or_both = 2 ;
   } else if (gSIM_GAUSSBEAM_SZ_FWHM != -1) {
      //for SZ map, use the same set-up as smoothing a GAMMA map
      beam_input_min = gSIM_GAUSSBEAM_SZ_FWHM * 10.;
      smooth_gamma_or_nu_or_both = 1 ;
   } else {
      beam_input_min = -1; // (no beam input! All beam values < 0 are set to -1 in params.cc)
      smooth_gamma_or_nu_or_both = 0 ;
   }

   // All cases where gSIM_HEALPIX_NSIDE is not set:
   if (gSIM_HEALPIX_NSIDE == -1) {
      // in this case, choose default gSIM_HEALPIX_SCHEME = NEST (set in params.cc)
      //and choose gSIM_HEALPIX_NSIDE as power of 2
      // (done by hp_resol2nside() function).

      // Case 0 ("000") = nothing is set:
      if (gSIM_ALPHAINT == -1 && beam_input_min == -1) {
         printf("\n====> ERROR: hp_set_resolution() in healpix_fits.cc");
         printf("\n             At least one of the input parameters gSIM_HEALPIX_NSIDE");
         printf("\n             or gSIM_GAMMA(NEUTRINO)BEAM_FWHM_DEG");
         printf("\n             must be explicitly set to a positive value.");
         printf("\n             => abort()\n\n");
         abort();
      }

      // Case 1 ("001") = only Gaussian Beam is set:
      else if (gSIM_ALPHAINT == -1 && beam_input_min != -1) {
         // - set gSIM_ALPHAINT = 1/3 gSIM_GAMMA(NEUTRINO)BEAM_FWHM.
         //   (in 2D, the Nyquist criterion is FWHM ~ 3 pixels. With 2*sigma < FWHM, we are definitely on the safe side.)
         // - set gSIM_HEALPIX_NSIDE according to gSIM_ALPHAINT.
         gSIM_ALPHAINT = 1. / 3. * beam_input_min ;
         gSIM_HEALPIX_NSIDE = hp_resol2nside(gSIM_ALPHAINT) ;
         hp_gridprop.SetNside(gSIM_HEALPIX_NSIDE, gSIM_HEALPIX_SCHEME);
         gSIM_RESOLUTION = hp_nside2resol(gSIM_HEALPIX_NSIDE);
         if (is_gamma_constraint) cout << ">>>>> choose gSIM_HEALPIX_NSIDE according to input gSIM_GAUSSBEAM_GAMMA_FWHM_DEG = " << gSIM_GAUSSBEAM_GAMMA_FWHM *RAD_to_DEG << " [deg]." << endl;
         else cout << ">>>>> choose gSIM_HEALPIX_NSIDE according to input gSIM_GAUSSBEAM_NEUTRINO_FWHM_DEG = " << gSIM_GAUSSBEAM_NEUTRINO_FWHM *RAD_to_DEG << " [deg]." << endl;
         cout << "   => gSIM_HEALPIX_NSIDE = " << gSIM_HEALPIX_NSIDE  << endl;
      }

      // Case 2 ("010") = only alpha_int is set:
      else if (gSIM_ALPHAINT != -1 && beam_input_min == -1) {
         gSIM_HEALPIX_NSIDE = hp_resol2nside(gSIM_ALPHAINT) ;
         hp_gridprop.SetNside(gSIM_HEALPIX_NSIDE, gSIM_HEALPIX_SCHEME);
         gSIM_RESOLUTION = hp_nside2resol(gSIM_HEALPIX_NSIDE);
         cout << ">>>>> choose gSIM_HEALPIX_NSIDE with resolution close to input gSIM_ALPHAINT_DEG = " << gSIM_ALPHAINT *RAD_to_DEG << " [deg]." << endl;
         cout << "   => gSIM_HEALPIX_NSIDE = " << gSIM_HEALPIX_NSIDE  << endl;
      }

      // Case 3 ("011") = both alpha_int and Gaussian Beam are set:
      else if (gSIM_ALPHAINT != -1 && beam_input_min != -1) {
         // - check if gSIM_ALPHAINT <= 1/3 gSIM_GAMMA(NEUTRINO)BEAM_FWHM
         // - set set gSIM_HEALPIX_NSIDE according to gSIM_ALPHAINT.
         if (gSIM_ALPHAINT > 1. / 3. * beam_input_min + SMALL_NUMBER) {
            printf("\n====> ERROR: hp_set_resolution() in healpix_fits.cc");
            printf("\n             The integration angle gSIM_ALPHAINT=%.le should be chosen", gSIM_ALPHAINT);
            printf("\n             equal or smaller 1/3 of your Gamma/Neutrino beam FWHM(=%le)", beam_input_min);
            printf("\n             for not violating the Nyquist criterion in two dimensions.");
            printf("\n             => abort()\n\n");
            abort();
         }
         gSIM_HEALPIX_NSIDE = hp_resol2nside(gSIM_ALPHAINT) ;
         hp_gridprop.SetNside(gSIM_HEALPIX_NSIDE, gSIM_HEALPIX_SCHEME);
         gSIM_RESOLUTION = hp_nside2resol(gSIM_HEALPIX_NSIDE);
         cout << ">>>>> choose gSIM_HEALPIX_NSIDE with resolution close to input gSIM_ALPHAINT_DEG = " << gSIM_ALPHAINT *RAD_to_DEG << " [deg]." << endl;
         cout << "   => gSIM_HEALPIX_NSIDE = " << gSIM_HEALPIX_NSIDE << endl;
      }
   }

   // All cases where gSIM_HEALPIX_NSIDE is set:
   else {

      if (gSIM_HEALPIX_NSIDE > 8192) {
         printf("\n====> ERROR: hp_set_resolution() in healpix_fits.cc");
         printf("\n             NSIDE=%d exceeds 2^13 = 8192, which is currently not supported", gSIM_HEALPIX_NSIDE);
         printf("\n             => abort()\n\n");
         abort();
      }

      T_Healpix_Base<int> hp_testPow2grid(gSIM_HEALPIX_NSIDE, RING, SET_NSIDE);
      // RING scheme for hp_gridprop is "dummy value" - we only want to know
      // if gSIM_HEALPIX_NSIDE is power of two. Default gSIM_HEALPIX_SCHEME is NEST (params.cc)
      int n_side_exp = hp_testPow2grid.Order(); // check if n_side is a power of 2.
      // the NESTED hp_scheme is supposed to be MUCH faster than the RING hp_scheme
      // for finding neighbor pixels which are repeatedly called to draw clumps later.
      if (n_side_exp == -1) {  // != power of 2:
         gSIM_HEALPIX_SCHEME = RING; // must choose RING scheme in this case.
         sprintf(char_tmp, "Your chosen gSIM_HEALPIX_NSIDE=%d is not a power of 2. "
                 "You may get problems plotting the results "
                 "(e.g., Aladin and Healpy won't work, but ds9 will.", gSIM_HEALPIX_NSIDE);
         print_warning("healpix_fits.cc", "hp_set_resolution()", string(char_tmp));
      } // else stay with default scheme set in params.cc

      if (gSIM_HEALPIX_NSIDE % 2 != 0) {
         sprintf(char_tmp, "Your chosen gSIM_HEALPIX_NSIDE=%d is an odd number. "
                 "This creates a pixel center/value right on the Galactic Center at (l,b) = (0,0). "
                 "If the Halo profile diverges there, CLUMPY will possibly crash.", gSIM_HEALPIX_NSIDE);
         print_warning("healpix_fits.cc", "hp_set_resolution()", string(char_tmp));

      }

      gSIM_RESOLUTION = hp_nside2resol(gSIM_HEALPIX_NSIDE);

      // Case 4 ("100") = only gSIM_HEALPIX_NSIDE is set:
      if (gSIM_ALPHAINT == -1 && beam_input_min == -1) {
         // - adapt gSIM_ALPHAINT matching exactly to the pixel size, no smoothing:
         gSIM_ALPHAINT = hp_nside2resol(gSIM_HEALPIX_NSIDE);
         //cout << ">>>>> choose gSIM_ALPHAINT_DEG according to input gSIM_HEALPIX_NSIDE = " << gSIM_HEALPIX_NSIDE << endl;
         //cout << "   => gSIM_ALPHAINT_DEG = " << gSIM_ALPHAINT *RAD_to_DEG << " [deg]." << endl;
      }

      // Case 5 ("101") = gSIM_HEALPIX_NSIDE and Gaussian beam are set:
      else if (gSIM_ALPHAINT == -1 && beam_input_min != -1) {
         // - set gSIM_ALPHAINT to the minimal not undersampling value
         // - check if this gSIM_ALPHAINT is <= 1./3. * gSIM_GAMMA(NEUTRINO)BEAM_FWHM
         // - if not, abort: can't further decrease gSIM_ALPHAINT, because this
         //   would cause undersampling with the given NSIDE.
         // - print a warning if gSIM_HEALPIX_NSIDE is much finer than the beam.

         gSIM_ALPHAINT = hp_nside2resol(gSIM_HEALPIX_NSIDE);
         if (gSIM_ALPHAINT > 1. / 3. * beam_input_min + SMALL_NUMBER) {
            printf("\n====> ERROR: hp_set_resolution() in healpix_fits.cc");
            printf("\n             For the given gSIM_HEALPIX_NSIDE=%d, there is no", gSIM_HEALPIX_NSIDE);
            printf("\n             integration angle gSIM_ALPHAINT equal or smaller than");
            printf("\n             1/3 of your Gamma/Neutrino beam FWHM not violating the Nyquist");
            printf("\n             criterion in two dimensions. Increase gSIM_HEALPIX_NSIDE,");
            printf("\n             or increase gSIM_GAUSSBEAM_GAMMA_FWHM/gSIM_GAUSSBEAM_NEUTRINO_FWHM to > %f [deg].", 3. * gSIM_ALPHAINT * RAD_to_DEG);
            printf("\n             => abort()\n\n");
            abort();
         }
         cout << ">>>>> choose gSIM_ALPHAINT_DEG according to input gSIM_HEALPIX_NSIDE = " << gSIM_HEALPIX_NSIDE << endl;
         cout << "   => gSIM_ALPHAINT_DEG = " << gSIM_ALPHAINT *RAD_to_DEG << " [deg]." << endl;

         if (gSIM_RESOLUTION < 1. / 200. * beam_input_min) {
            sprintf(char_tmp, "Your chosen map resolution = %.2le [deg] is more than 200 times bigger than "
                    "your instrument's beam FWHM = %.2le [deg]. Do you really want this?", gSIM_RESOLUTION * RAD_to_DEG, beam_input_min * RAD_to_DEG);
            print_warning("healpix_fits.cc", "hp_set_resolution()", string(char_tmp));
         }
      }

      // Case 6 ("110") = gSIM_HEALPIX_NSIDE and gSIM_ALPHAINT are set:
      else if (gSIM_ALPHAINT != -1 && beam_input_min == -1) {
         // - check if the J-factor integration area given by gSIM_ALPHAINT
         //   is bigger/equal than the pixel size. If not, abort.
         // - print a warning if gSIM_HEALPIX_NSIDE is much finer than gSIM_ALPHAINT.

         if (gSIM_ALPHAINT < hp_nside2resol(gSIM_HEALPIX_NSIDE) && !is_sz) {

            //do not want this abort implemented in case of SZ Compton-y paramter calculation
            printf("\n====> ERROR: hp_set_resolution() in healpix_fits.cc");
            printf("\n             The chosen gSIM_ALPHAINT = %2le [deg] corresponds to a J-Factor", gSIM_ALPHAINT * RAD_to_DEG);
            printf("\n             integration area smaller than the pixel size (map resolution)");
            printf("\n             This causes undersampling. Increase gSIM_ALPHAINT_DEG to > %f [deg]",
                   hp_nside2resol(gSIM_HEALPIX_NSIDE) * RAD_to_DEG);
            printf("\n             or increase gSIM_HEALPIX_NSIDE");
            printf("\n             => abort()\n\n");
            abort();
         }

         if (gSIM_RESOLUTION < 1. / 10. * gSIM_ALPHAINT) {
            sprintf(char_tmp, "Your chosen map resolution = %.2le [deg] is more than 10 times bigger than "
                    "your J-factor integration angle gSIM_ALPHAINT = %.2le [deg]. Do you really want this?", gSIM_RESOLUTION * RAD_to_DEG, gSIM_ALPHAINT * RAD_to_DEG);
            print_warning("healpix_fits.cc", "hp_set_resolution()", string(char_tmp));
         }
      }

      // Case 7 ("111") = gSIM_HEALPIX_NSIDE, gSIM_ALPHAINT and gSIM_GAMMA(NEUTRINO)BEAM_FWHM are all set:
      else if (gSIM_ALPHAINT != -1 && beam_input_min != -1) {
         // - check if the chosen gSIM_ALPHAINT is <= 1./3. * gSIM_GAMMA(NEUTRINO)BEAM_FWHM
         // - if not, abort: can't further decrease gSIM_ALPHAINT, because this
         //   would cause undersampling with the given NSIDE.
         // - check if the J-factor integration area given by gSIM_ALPHAINT
         //   is bigger/equal than the pixel size. If not, abort.
         // - print a warning if gSIM_HEALPIX_NSIDE is much finer than gSIM_ALPHAINT.
         // - print a warning if gSIM_HEALPIX_NSIDE is much finer than the beam.

         if (gSIM_ALPHAINT < hp_nside2resol(gSIM_HEALPIX_NSIDE)) {
            printf("\n====> ERROR: hp_set_resolution() in healpix_fits.cc");
            printf("\n             The chosen gSIM_ALPHAINT = %.2le [deg] corresponds to a J-Factor", gSIM_ALPHAINT * RAD_to_DEG);
            printf("\n             integration area smaller than the pixel size (map resolution)");
            printf("\n             This causes undersampling. Increase gSIM_ALPHAINT_DEG to > %f [deg]",  hp_nside2resol(gSIM_HEALPIX_NSIDE) * RAD_to_DEG);
            printf("\n             or increase gSIM_HEALPIX_NSIDE");
            printf("\n             => abort()\n\n");
            abort();
         }
         if (gSIM_ALPHAINT > 1. / 3. * beam_input_min + SMALL_NUMBER) {
            printf("\n====> ERROR: hp_set_resolution() in healpix_fits.cc");
            printf("\n             the integration angle gSIM_ALPHAINT = %.2le [deg] should be chosen", gSIM_ALPHAINT * RAD_to_DEG);
            printf("\n             equal or smaller 1/3 your Gamma/Neutrino beam FWHM, i.e.");
            printf("\n             gSIM_ALPHAINT_DEG <= %f [deg]", 1. / 3. * beam_input_min * RAD_to_DEG);
            printf("\n             for not violating the Nyquist criterion in two dimensions.");
            printf("\n             => abort()\n\n");
            abort();
         }

         if (gSIM_RESOLUTION < 1. / 10. * gSIM_ALPHAINT) {
            sprintf(char_tmp, "Your chosen map resolution = %.2le [deg] is more than 10 times bigger than "
                    "your J-factor integration angle gSIM_ALPHAINT = %.2le [deg]. Do you really want this?", gSIM_RESOLUTION * RAD_to_DEG, gSIM_ALPHAINT * RAD_to_DEG);
            print_warning("healpix_fits.cc", "hp_set_resolution()", string(char_tmp));
         }
         if (gSIM_RESOLUTION < 1. / 200. * beam_input_min) {
            sprintf(char_tmp, "Your chosen map resolution = %.2le [deg] is more than 200 times bigger than "
                    "your smallest instrument's beam FWHM = %.2le [deg]. Do you really want this?", gSIM_RESOLUTION * RAD_to_DEG, beam_input_min  * RAD_to_DEG);
            print_warning("healpix_fits.cc", "hp_set_resolution()", string(char_tmp));
         }
      }
   }

   hp_gridprop.SetNside(gSIM_HEALPIX_NSIDE, gSIM_HEALPIX_SCHEME);
   n_pix = hp_gridprop.Npix();
   delta_omega = 4 * PI / n_pix;
   gSIM_HEALPIX_DELTAOMEGA = delta_omega;

   cout << ">>>>> Approximate average pixel edge length is " << sqrt(delta_omega) * RAD_to_DEG << " [deg]." << endl;
   cout << "   => Pixel area is Delta Omega = " << delta_omega << " sr." << endl;
   //cout << "   => Oversampling ratio (Delta Omega_alphaint / Delta Omega) = " << 2. * PI * (1 - cos(gSIM_ALPHAINT)) / delta_omega << endl;
   //cout << "      (this ratio should never be smaller than Pi/2 = 1.57..., otherwise undersampling)\n" << endl;

   return;
}

//______________________________________________________________________________
void hp_smooth_map(Healpix_Map<double> &map, double &fwhm, string const &weights_dir)
{
   //--- Returns the full-sky map map smoothed with a beam of FWHM of fwhm [rad].
   // INPUTS:
   //  map          Healpix_Map object containing a full-sky dataset.
   //  fwhm         Full width of half maximum in rad of a Gaussian beam.
   //  weights_dir  Directory of ring weights shipped with the Healpix package
   //               for improved quadrature mode.
   // OUTPUTS:
   //  map          Healpix_Map object containing a full-sky dataset, now smoothed.

   bool is_get_powspec = false;
   PowSpec spectrum;
   hp_smooth_or_powspec(map, fwhm, weights_dir, is_get_powspec, spectrum);

   return;
}

//______________________________________________________________________________
void hp_smooth_map_and_get_powspec(Healpix_Map<double> &map, double &fwhm, string const &weights_dir, PowSpec &spectrum)
{
   //--- Returns the full-sky map map smoothed with a beam of FWHM of fwhm [rad].
   //    and the angular power spectrum of the unsmoothed map.
   // INPUTS:
   //  map          Healpix_Map object containing a full-sky dataset.
   //  fwhm         Full width of half maximum in rad of a Gaussian beam.
   //  weights_dir  Directory of ring weights shipped with the Healpix package
   //               for improved quadrature mode.
   //  spectrum     Uninitialized PowerSpectrum object (will be overwritten, if already initialized).
   // OUTPUTS:
   //  map          Healpix_Map object containing a full-sky dataset, now smoothed.
   //  spectrum     1-column PowerSpectrum object containing the angular power spectrum
   //               of the unsmoothed map

   bool is_get_powspec = true;
   hp_smooth_or_powspec(map, fwhm, weights_dir, is_get_powspec, spectrum);

   return;
}

//______________________________________________________________________________
void hp_smooth_or_powspec(Healpix_Map<double> &map, double &fwhm, string const &weights_dir, bool is_get_powspec, PowSpec &spectrum)
{
   //--- Returns the full-sky map map smoothed with a beam of FWHM of fwhm [rad].
   //    and/or a power spectrum of the unsmoothed map.
   //    This function is basically copied and pasted from the code for the
   //    Healpix C++ smoothing binary, smoothing_cxx_module.cc, lines 60 to 83.
   // INPUTS:
   //  map          Healpix_Map object containing a full-sky dataset.
   //  fwhm         Full width of half maximum in rad of a Gaussian beam.
   //  weights_dir  Directory of ring weights shipped with the Healpix package
   //               for improved quadrature mode.
   //  is_get_powspec Choose if you want to calculate the power spectrum.
   //  spectrum     Uninitialized PowerSpectrum object (will be overwritten, if already initialized).
   // OUTPUTS:
   //  map          Healpix_Map object containing a full-sky dataset, now smoothed.
   //  spectrum     1-column PowerSpectrum object containing the angular power spectrum
   //                of the unsmoothed map, only returned for is_get_powspec = true.

   char char_tmp[512];

   bool is_smoothing = true;
   if (fwhm < 0.)
      cout << "NOTE: negative FWHM supplied, doing a deconvolution..." << endl;
   else if (fwhm == 0.) is_smoothing = false;

   size_t nmod = map.replaceUndefWith0();
   if (nmod != 0) {
      sprintf(char_tmp, "Replaced %d undefined map pixels with a value of 0.", (int)nmod);
      print_warning("healpix_fits.cc", "hp_smooth_or_powspec()", string(char_tmp));
   }

   if (gSIM_HEALPIX_ITER == -1) gSIM_HEALPIX_ITER = int(fabs(log10(gSIM_EPS))) - 1;
   cout << "  ... number of iterations: " << gSIM_HEALPIX_ITER << endl;
   if (gSIM_HEALPIX_NLMAX_FAC == -1) gSIM_HEALPIX_NLMAX_FAC = 2. + 6. / PI * atan(0.5 * (-log10(gSIM_EPS) - 1.)) ;
   // - gSIM_HEALPIX_NLMAX_FAC -> 4 for gSIM_EPS -> 0.
   // - gSIM_HEALPIX_NLMAX_FAC = 2 for gSIM_EPS = 0.1, the minimal gSIM_HEALPIX_NLMAX_FAC
   //   that makes sense without producing heavy artefacts.
   int n_side  = map.Nside() ;
   int nlmax = int(gSIM_HEALPIX_NLMAX_FAC * double(n_side)) ;
   cout << "  ... maximum multipole index l for which alm are computed: " << nlmax << endl;

   int n_l = 2 * n_side;
   arr<double> weight;
   weight.alloc(n_l);

   char tmp[20];
   sprintf(tmp, "%05d", n_side);
   string weights_dir_tmp = weights_dir;
   resolve_envvar(weights_dir_tmp);
   string weightfile_str = weights_dir_tmp + "/weight_ring_n" + (string)tmp + ".fits";

   ifstream is_weightfile(weightfile_str.c_str());

   if (is_weightfile) {
      cout << "  ... found ring weights file for chosen NSIDE of this simulation in:" << endl;
      cout << "      " << weightfile_str << endl;
      cout << "      => use improved quadrature mode for alm computation..." << endl;

      // open FITS file which contains the weights
      fitsfile *fptr;       /* pointer to the FITS file, defined in fitsio.h */
      int status = 0;
      int hdunum = 2;
      int anynull, hdutype;
      long frow = 1;
      long felem = 1;
      long nelem = n_l;
      double floatnull = 0;
      double weight_load[n_l];
      const char *filename_char  = weightfile_str.c_str();     /* name of existing FITS file   */

      if (fits_open_file(&fptr, filename_char, READONLY, &status))
         fits_printerror(status);
      if (fits_movabs_hdu(fptr, hdunum, &hdutype, &status))
         fits_printerror(status);

      fits_read_col(fptr, TDOUBLE, 1, frow, felem, nelem, &floatnull, weight_load, &anynull, &status);

      if (fits_close_file(fptr, &status))
         fits_printerror(status);

      for (size_t m = 0; m < weight.size(); ++m) {
         weight[m] = weight_load[m] + 1;
      }
   } else {
      cout << "  ... no ring weights file for chosen NSIDE of this simulation found" << endl;
      if (weights_dir != "-1") cout << "      at " << weightfile_str << endl;
      cout << "      => alm computation done without improved quadrature mode (default)..." << endl;
      weight.fill(1);
   }

   Alm<xcomplex<double> > alm(nlmax, nlmax);
   double avg = map.average();
   map.Add(double(-avg));
   if (map.Scheme() == NEST) map.swap_scheme();

   map2alm_iter(map, alm, gSIM_HEALPIX_ITER, weight);
   //map2alm(map, alm,weight);

   if (is_get_powspec) {
      spectrum.Set(1, nlmax);
      alm(0, 0) += avg * sqrt(4. * PI); // this un-does map.Add(double(-avg));
      extract_powspec(alm, spectrum);
   }

   if (is_smoothing) {
      smoothWithGauss(alm, fwhm);
      alm2map(alm, map);
   }

   map.Add(double(avg));

   return;
}

//______________________________________________________________________________
void j_interpolation(double *j_interp, rangeset<int> &rs_pix_fov, string &grid_mode,
                     const double &psi, const double &theta, const double &dtheta_orth, const double &dtheta,
                     int switch_j, double const &mtot, double par_tot[10],
                     double const &eps, double const &f_dm,  double par_dpdv[10],
                     double par_subs[25], double ntot_subs, int n_mass, double *l_crit,
                     double *m1, double *m2)
{
   //--- Calculates any continuum contributions (smooth, sub continuum or crossproduct),
   //    relying on a 2D interpolation to speed-up the calculation in some cases
   //    (whenever skymap has a high number of pixels, see criterion below)
   //
   // INPUTS:
   //  rs_pix_fov rangeset containing the healpix pixel numbers of the FOV
   //  grid_mode    DISK, RECT or STRIP grid.
   //  psi          psi coordinate of center of FOV
   //  theta        theta coordinate of center of FOV
   //  dtheta_orth  size of FOV in great-circle direction orthogonal to theta axis.
   //  dtheta       size of FOV on theta axis.
   //  switch_j     Switch to select which J to interpolate
   //                  - 0 = j_smooth
   //                  - 1 = j_sub_continuum
   //                  - 2 = j_crossprod
   //  mtot         Total mass of the host halo [Msol]
   //  par_tot[0]   rho_tot host normalisation [Msol kpc^{-3}]
   //  par_tot[1]   rho_tot host scale radius [kpc]
   //  par_tot[2]   rho_tot host shape parameter #1
   //  par_tot[3]   rho_tot host shape parameter #2
   //  par_tot[4]   rho_tot host shape parameter #3
   //  par_tot[5]   rho_tot host card_profile [gENUM_PROFILE]
   //  par_tot[6]   rho_tot host radius [kpc]
   //  par_tot[7]   l_HC: distance observer to host centre [kpc]
   //  par_tot[8]   psi_HC: longitude of host centre [rad]
   //  par_tot[9]   theta_HC: latitude of host centre [rad]
   //  eps          Relative precision sought for integration
   //  f_dm         Fraction of DM in subclumps (in the host halo)
   //  par_dpdv[0]  dPdV_Host normalisation  [kpc^{-3}]
   //  par_dpdv[1]  dPdV_Host scale radius [kpc]
   //  par_dpdv[2]  dPdV_Host shape parameter #1
   //  par_dpdv[3]  dPdV_Host shape parameter #2
   //  par_dpdv[4]  dPdV_Host shape parameter #3
   //  par_dpdv[5]  dPdV_Host card_profile [gENUM_PROFILE]
   //  par_dpdv[6]  dPdV_Host radius [kpc]
   //  par_dpdv[7]  l_HC: distance observer to Host Centre [kpc]
   //  par_dpdv[8]  psi_HC: longitude of dpdv [rad]
   //  par_dpdv[9]  theta_HC: latitude of dpdv [rad]
   //  par_subs[0]  rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_subs[1]  rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_subs[2]  rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_subs[3]  rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]  host halo outer radius where to stop integration [kpc]
   //  par_subs[5]  eps: relative precision sought L calculation
   //  par_subs[6]  z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]  mdelta: mass of rho_cl(r)
   //  par_subs[8]  dPdM normalisation [1/Msol]
   //  par_subs[9]  dPdM slope alphaM
   //  par_subs[10] dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)     [UNUSED for DECAY]
   //  par_subs[11] dPdc mean concentration <c>         [UNUSED for DECAY]
   //  par_subs[12] dPdc standard deviation             [UNUSED for DECAY]
   //  par_subs[13] dPdc card_profile [gENUM_CDELTA_DIST] [UNUSED for DECAY]
   //  par_subs[14] f: mass fraction of substructures   [UNUSED for DECAY]
   //  par_subs[15] nlevel: sub-sub...halos (1=no sub)  [UNUSED for DECAY]
   //  par_subs[16] dPdV normalisation  [kpc^{-3}]      [UNUSED for DECAY]
   //  par_subs[17] dPdV scale radius [kpc]             [UNUSED for DECAY]
   //  par_subs[18] dPdV shape parameter #1             [UNUSED for DECAY]
   //  par_subs[19] dPdV shape parameter #2             [UNUSED for DECAY]
   //  par_subs[20] dPdV shape parameter #3             [UNUSED for DECAY]
   //  par_subs[21] dPdV card_profile [gENUM_PROFILE]   [UNUSED for DECAY]
   //  par_subs[22] host halo outer radius (unused)     [UNUSED for DECAY]
   //  par_subs[23]  ratio rs dPdV to rs_cl             [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]   R_host (corresp. to current dPdV) [only for ANNIHILATION and nlevel>1]
   //  n_mass       Number of mass ranges (if switch_j==1 or switch_j==2)
   //  l_crit       Array[n_mass] of critical distances [kpc]
   //  m1           Array[n_mass] of lower value for the subs mass range [Msol]
   //  m2           Array[n_mass] of upper value for the subs mass range [Msol]
   // OUTPUTS:
   //  j_interp     2D array[n_pix_fov] of j values [Msol^2/kpc^5] annihil., [Msol/kpc^3] decay

   char char_tmp[512];
   bool is_interpolate = false;
   bool is_simple_interp = false;
   double lmin = max(0., par_tot[7] - par_tot[6]);
   double lmax = par_tot[7] + par_tot[6];
   double z = par_subs[6];

   // set alpha_int to value corresponding to observation of proper coordinates
   // (correct for angular diameter distortion)
   double alphaint_orig = gSIM_ALPHAINT;
   if (z > ZMIN_EXTRAGAL) gSIM_ALPHAINT = atan(1. / (1 + z) * MY_TAN(gSIM_ALPHAINT));

   double fivedeg_rad = 5.* DEG_to_RAD;

   // create vector here again from rangeset (instead of parsing it directly into this
   // function, because the rangeset is also needed here for fast calculations.)
   vector<int> v_pix_fov;
   rs_pix_fov.toVector(v_pix_fov);
   int n_pix_fov = v_pix_fov.size();

   T_Healpix_Base<int> hp_gridprop(gSIM_HEALPIX_NSIDE, gSIM_HEALPIX_SCHEME, SET_NSIDE) ;
   int n_pix = hp_gridprop.Npix();

   // conditions for interpolation:
   int n_ref;
   int n_base = 0;

   // assure that interpolation NSIDE is not too coarse (determine minimum resolution,
   // for which interpolation is done): If the discrepancy between original and
   // interpolation grid becomes too big, features may be introduced into the interpolated
   // grid which result from the pixel shapes of the interpolation grid.
   int n_side_min ;
   int n_side_min_approx = gSIM_HEALPIX_NSIDE / pow(2., 6 + log10(gSIM_EPS)) ;
   // find nearest n_side as a power of two:
   double n_side_min_exp_raw = log2(n_side_min_approx) ;
   if (n_side_min_approx - pow(2, floor(n_side_min_exp_raw)) < pow(2, ceil(n_side_min_exp_raw)) - n_side_min_approx)
      n_side_min = pow(2, floor(n_side_min_exp_raw)) ;
   else n_side_min = pow(2, ceil(n_side_min_exp_raw)) ;
   if (n_side_min < 16) n_side_min = 16 ; // absolute n_side_min:

   // simulate fake rectangular grid for condition is_interpolate:
   int n_psi =  int(sqrt(fabs(dtheta_orth) / fabs(dtheta) * n_pix_fov));
   int n_theta =  n_pix_fov / n_psi ;

   // Even if gSIM_HEALPIX_NSIDE has NOT been chosen as a power of two, the
   // interpolation grids have always a n_side as a power of two.
   int n_side_max = hp_resol2nside(gSIM_RESOLUTION) ; // <- power of two also here!
   // assure that n_side_max won't be bigger than gSIM_HEALPIX_NSIDE (this may happen for gSIM_HEALPIX_NSIDE != 2^n):
   if (n_side_max > gSIM_HEALPIX_NSIDE) n_side_max /= 2 ;

   // now check if interpolation is applied:
   if ((n_psi > n_base && n_theta > n_base) || n_psi > n_base * 10 || n_theta > n_base * 10) {
      is_interpolate = true;
      // switch off interpolation for very low resolutions:
      if (gSIM_HEALPIX_NSIDE  <= n_side_min || n_side_max  <= n_side_min) {
         is_interpolate = false;
         cout << "      No interpolation is done because of low resolution/high demanded precision." << endl;
      }
   }  else cout << "      No interpolation is done because of sufficiently few pixels in the FOV." << endl;

   // used both for simple ("lin-log") and multiple-grids ("log-log") interpolation:
   double dtheta_orth_extended = dtheta_orth; // slightly increased FOV for interpolation grid
   double dtheta_extended = dtheta; // slightly increased FOV for interpolation grid

   // (theta, phi) pointing to angles of pixels in both normal and interpolation (base) grid.
   // Attention: In Healpix, theta is counted in [0, 2pi]!
   pointing ptg_i_pix_base ;
   pointing ptg_i_pix ;

   double alpha_quad_start =  5. * gSIM_RESOLUTION * log(1. / gSIM_EPS);
   // 1D-interpolation:
   // alpha_quad_start = log-log grid scaling.
   // 2D-interpolation:
   // alpha_quad_start = radius of innermost grid around Gal. center with the highest resolution,
   // used for interpolation on multiple grids. (scaling value for the ranges
   // of the grids in the multiple-grids case and also used for the scaling of the log-like
   // correction to the linear interpolation close to the Galactic center:

   if (gSIM_IS_CIRCULAR_SYMMETRY) {

      // We can take advantage of the circular line-of-sight symmetry (from spherical symmetry of the halo)
      // and reduce our calculations to a 1D-problem, both when not doing any interpolation
      // and all and for our interpolation approach.
      cout << "      Use line-of-sight rotational symmetry for calculation..." << endl;

      // number of interpolation grid points:
      n_ref = 100;
      n_base = max(n_ref, int(sqrt(fabs(dtheta_orth * dtheta)) / (gSIM_RESOLUTION)));

      double theta_1D = 0.;
      vector<double> phi_tab;

      vector<double> j_1D;

      // Array for interpolations
      vector<double> j_1D_base;
      vector<double> phi_base;
      vector<int> iphi_inbase;

      // Get the minimum and the maximum distance of the FOV from the GC:
      double phi_min = PI ;
      double phi_max = 0.;

      // short-cut for full-sky map:
      if (n_pix_fov == n_pix) {
         phi_min = 0.;
         phi_max = PI;
      } else {
         for (int i = 0; i < n_pix_fov; ++i) {
            ptg_i_pix = hp_gridprop.pix2ang(v_pix_fov[i]) ;
            double phi_candidate = psitheta_to_phi(ptg_i_pix.phi, PI / 2 - ptg_i_pix.theta) ;
            if (phi_candidate < phi_min) {
               phi_min = phi_candidate ;
            } ;
            if (phi_candidate > phi_max) {
               phi_max = phi_candidate ;
            }
         }
         // extend phi_min, phi_max a bit:
         phi_min -= gSIM_RESOLUTION ;
         phi_max += gSIM_RESOLUTION ;
         if (phi_min < 0.) phi_min = 0.;
         if (phi_max > PI) phi_max = PI;
      }

      double max_rad;
      if (par_tot[6] < par_tot[7]) max_rad = tan((1 + z) * par_tot[6] / par_tot[7]) + gSIM_ALPHAINT;
      else max_rad = PI;

      if (max_rad < phi_max) phi_max = max_rad;

      // allocate j_1D:
      double delta_phi = gSIM_RESOLUTION / (10. * log10(1. / gSIM_EPS)) ;
      int n_1D = int(ceil((phi_max - phi_min) / delta_phi)) + 1 ;
      delta_phi = (phi_max - phi_min) / double(n_1D - 1.) ; // corrected for the ceil()
      for (int i = 0; i < n_1D; ++i) phi_tab.push_back(phi_min + i * delta_phi);

      j_1D.assign(n_1D, 1.e-40);

      double phi_proper;

      // now calculate 1D-halo profile between phi_min and phi_max:
      if (is_interpolate) {
         // do it like in ClumpyV1:
         if (phi_min > fivedeg_rad) is_simple_interp = true;

         if (is_simple_interp) printf("        ... Fill interpolation function (%d lin-steps) ...\n", n_base);
         else printf("        ... Fill interpolation function (%d log-steps) ...\n", n_base);

         j_1D_base.assign(n_base, 1.e-40);
         phi_base.assign(n_base, 1.e-40);
         iphi_inbase.assign(n_1D, -1);
         double delta_phi_base;
         if (is_simple_interp) delta_phi_base = (phi_max - phi_min) / double(n_base - 1);
         else delta_phi_base = pow(phi_max / alpha_quad_start, 1. / double(n_base - 1));


         for (int i = 0; i < n_base; ++i) {
            if (is_simple_interp) phi_base[i]  = phi_min + i * delta_phi_base;
            else phi_base[i] = alpha_quad_start * pow(delta_phi_base, i);

            if (z < ZMIN_EXTRAGAL) phi_proper = phi_base[i];
            else phi_proper = atan(1. / (1 + z) * MY_TAN(phi_base[i]));

            double jopt = 1.e-40;
            if (switch_j == 0) {
               if (f_dm > 1.e-3)
                  jopt = jsmooth_mix(mtot, par_tot, phi_proper, theta_1D, lmin, lmax, eps, f_dm, par_dpdv);
               else
                  jopt = jsmooth(par_tot, phi_proper, theta_1D, lmin, lmax, eps);
            } else if (switch_j == 1) {
               // N.B.: we have to take into account all mass decades
               for (int k = 0; k < n_mass; ++k) {
                  if (l_crit[k] < lmax)
                     jopt += jsub_continuum(ntot_subs, par_dpdv, phi_proper, theta_1D,
                                            l_crit[k], lmax,  par_subs, m1[k], m2[k]);
               }
            } else if (switch_j == 2) {
               // N.B.: we have to take into account all mass decades
               for (int k = 0; k < n_mass; ++k) {
                  if (l_crit[k] < lmax)
                     jopt += frac_nsubs_in_m1m2(&par_subs[8], m1[k], m2[k], gSIM_EPS)
                             * jcrossprod_continuum(mtot, par_tot, phi_proper, theta_1D,
                                                    l_crit[k], lmax, eps, f_dm, par_dpdv);
               }
            }
            if (jopt == 0.) jopt = 1.e-40;
            j_1D_base[i] = jopt;
         }
         // Set indices for phi_base[iphi_inbase] for phi_tab[i]
         // Search (only once) for interpolation indices for angles
         for (int i = 0; i < n_1D; ++i)
            iphi_inbase[i] = lower_bound(phi_base.begin(), phi_base.end(), phi_tab[i]) - phi_base.begin() - 1;
      }

      if (is_interpolate) printf("        ... and interpolate for %d l.o.s. directions ...\n", n_1D);

      for (int i = 0; i < n_1D; ++i) {
         j_1D[i] = 1.e-40;
         // Interpolation if applies
         // N.B.: we always calculate J towards the halo centre, and also
         // close to external values (to avoid interpolation problems because no values!!!)
         if (is_interpolate && is_simple_interp &&  iphi_inbase[i] >= 0 && iphi_inbase[i] < n_base - 1) {
            // This is a 1D lin-log interpolation:
            j_1D[i] = interp1D(phi_tab[i], phi_base, j_1D_base, kLINLOG);
         } else if (is_interpolate && !is_simple_interp && iphi_inbase[i] >= 0 && iphi_inbase[i] < n_base - 1) {
            j_1D[i] = interp1D(phi_tab[i], phi_base, j_1D_base, kLOGLOG);
         } else {
            // no interpolation:
            if (z < ZMIN_EXTRAGAL) phi_proper = phi_tab[i];
            else phi_proper = atan(1. / (1 + z) * MY_TAN(phi_tab[i]));

            if (switch_j == 0)
               j_1D[ i ] = jsmooth_mix(mtot, par_tot, phi_proper, theta_1D, eps, f_dm, par_dpdv);
            else if (switch_j == 1) {
               for (int k = 0; k < n_mass; ++k) {
                  if (l_crit[k] < lmax)
                     j_1D[i ] += jsub_continuum(ntot_subs, par_dpdv, phi_proper, theta_1D,
                                                l_crit[k], lmax,  par_subs, m1[k], m2[k]);
               }
            } else if (switch_j == 2) {
               for (int k = 0; k < n_mass; ++k) {
                  if (l_crit[k] < lmax)
                     j_1D[i ] += frac_nsubs_in_m1m2(&par_subs[8], m1[k], m2[k], gSIM_EPS)
                                 * jcrossprod_continuum(mtot, par_tot, phi_proper, theta_1D,
                                                        l_crit[k], lmax, eps, f_dm, par_dpdv);
               }
            }
         }
      }

      // ... and fill all pixels within the FOV!
      for (int i = 0; i < n_pix_fov; ++i) {
         ptg_i_pix = hp_gridprop.pix2ang(v_pix_fov[i]) ;
         double phi_pix = psitheta_to_phi(ptg_i_pix.phi, PI / 2 - ptg_i_pix.theta) ;
         if (phi_pix >= phi_tab.back()) continue;
         int i1D = binary_search(phi_tab, phi_pix);
         // look for closest value:
         if (i1D < int(phi_tab.size()) and phi_pix - phi_tab[i1D] > phi_tab[i1D + 1] - phi_pix) i1D++;
         j_interp[i] = j_1D[i1D];
      }

      // Free memory
      j_1D.clear();
      phi_base.clear();
      iphi_inbase.clear();
   } else {
      // We do not have a circular symmetry and thus have to do the
      // full 2D calculation, both when interpolating and when not interpolating.

      // number of interpolation grid points:
      if (gNAMES_SIMUMODES[gSIM_FLAG_MODE][0] == 'g' && fabs(par_tot[7] - gMW_RSOL) / gMW_RSOL < 1.e-3 && fabs(par_tot[6] - gMW_RMAX) / gMW_RMAX < 1.e-3)
         n_ref = 41;
      else n_ref = 21;
      n_base = max(n_ref, int(sqrt(fabs(dtheta_orth * dtheta)) / (20.*gSIM_RESOLUTION)));

      // Arrays and grid for simple-grid interpolation:
      vector<double> jfactor_base;
      T_Healpix_Base<int> hp_gridprop_base ;
      rangeset<int> rs_pix_fov_base ;
      vector<int> v_pix_fov_base;

      // base grids for multiple-grids interpolation:
      vector< T_Healpix_Base<int> > v_hp_gridprop_base ;
      T_Healpix_Base<int> hp_gridprop_base_run ;
      vector< rangeset<int> > v_rs_pix_fov_base ;    // this is a complicated object: it is a vector containing the rangesets
      // of the different interpolation grids, which in turn contain the ranges
      // (integers) for the pixel numbers of these grids.
      vector< vector<int> > v_v_pix_fov_base ;
      rangeset<int> rs_pix_fov_base_run ;
      vector<int> v_pix_fov_base_run ;
      vector<double> loggrid_radii;

      vector<int> v_n_pix_i_grid ;

      vector< vector<double> > v_v_jfactor_base;
      vector<double> v_jfactor_base_run ;
      vector<double> arr_dist_GC_base(4, 0.);

      // pixels and weights of base pixels in interpolation:
      fix_arr<int, 4> pix_base ;
      fix_arr<double, 4> j_base ;
      fix_arr<double, 4> wgt_base ;
      int i0 ;
      int i1 ;
      int i2 ;
      int i3 ;

      int i_grid_min = -1;
      int i_grid_max = -1;

      // Check if skymap passes through neighborhood of the Galactic center,
      // i.e., through a region of 5 deg radius = 10 deg diameter. This is needed
      // both for the 1D-short cut and for the full 2D calculations.
      double fivedeg_diameter = 2. * fivedeg_rad;
      string disk_around_GC = "DISK" ;
      double GC_psi = 0. ;
      double GC_theta = 0. ;
      rangeset <int> rs_pix_aroundGC = hp_set_pix_fov(disk_around_GC, GC_psi, GC_theta,  fivedeg_diameter,  fivedeg_diameter, gSIM_HEALPIX_NSIDE, gSIM_HEALPIX_SCHEME);
      rangeset <int> rs_intersectGC ;

      double psi_proper, theta_proper, phi_proper_to_phi_comoving;

      rs_intersectGC = rs_pix_aroundGC.op_and(rs_pix_fov) ;
      // Check J-factor values at GC and Anticenter to know range and steepness
      // of the data on the whole sphere:
      double jmin = 1.e-40;
      double jmax = 1.e-40;
      if (switch_j == 0) {
         if (f_dm > 1.e-3) {
            jmax = jsmooth_mix(mtot, par_tot, GC_psi, GC_theta, lmin, lmax, eps, f_dm, par_dpdv);
            jmin = jsmooth_mix(mtot, par_tot, GC_psi + PI, GC_theta, lmin, lmax, eps, f_dm, par_dpdv);
         } else {
            jmax = jsmooth(par_tot, GC_psi, GC_theta, lmin, lmax, eps);
            jmin = jsmooth(par_tot, GC_psi + PI, GC_theta, lmin, lmax, eps);
         }
      } else if (switch_j == 1) {
         // N.B.: we have to take into account all mass decades
         for (int k = 0; k < n_mass; ++k) {
            if (l_crit[k] < lmax) {
               jmax += jsub_continuum(ntot_subs, par_dpdv, GC_psi, GC_theta,
                                      l_crit[k], lmax,  par_subs, m1[k], m2[k]);
               jmin += jsub_continuum(ntot_subs, par_dpdv, GC_psi + PI, GC_theta,
                                      l_crit[k], lmax,  par_subs, m1[k], m2[k]);
            }
         }
      } else if (switch_j == 2) {
         // N.B.: we have to take into account all mass decades
         for (int k = 0; k < n_mass; ++k) {
            if (l_crit[k] < lmax) {
               jmax += frac_nsubs_in_m1m2(&par_subs[8], m1[k], m2[k], gSIM_EPS)
                       * jcrossprod_continuum(mtot, par_tot, GC_psi, GC_theta,
                                              l_crit[k], lmax, eps, f_dm, par_dpdv);
               jmin += frac_nsubs_in_m1m2(&par_subs[8], m1[k], m2[k], gSIM_EPS)
                       * jcrossprod_continuum(mtot, par_tot, GC_psi + PI, GC_theta,
                                              l_crit[k], lmax, eps, f_dm, par_dpdv);
            }
         }
      }

      if (is_interpolate) {
         // The J factor is steep towards the Galactic center. Two cases arise:
         //   1. skymap does NOT pass through a circular region around (0,0) with radius 5 degs:
         //        => we use a simple lin-log interpolation on a single interpolation grid
         //   2. skymap passes through a circular region around (0,0) with radius 5 degs:
         //        => we use a lin-lin interpolation on several ring-shaped interpolation
         //           grids with higher resolution towards the Galactic center. Additionally,
         //           some corrections to the linear interpolation are performed where the
         //           profile is rising very fast.

         bool is_simple_interp = false;
         //--- Case 1: linear interpolation for FOVs more than five degrees away from the GC at (0,0):
         if (rs_intersectGC.size() == 0) {
            is_simple_interp = true;

            // find the appropriate NSIDE for the interpolation grid:
            double frac_igrid2totgrid = pow(double(n_base), 2) / double(n_pix_fov) ;
            double n_side_base_approx = sqrt(frac_igrid2totgrid) * double(gSIM_HEALPIX_NSIDE) ;   // because n_pix ~ n_side^2

            int n_side_base ;

            // find nearest n_side as a power of two:
            double n_side_base_exp_raw = log2(n_side_base_approx) ;
            if (n_side_base_approx - pow(2, floor(n_side_base_exp_raw)) < pow(2, ceil(n_side_base_exp_raw)) - n_side_base_approx)
               n_side_base = pow(2, floor(n_side_base_exp_raw)) ;
            else n_side_base = pow(2, ceil(n_side_base_exp_raw)) ;

            // assure that there will be interpolation at all:
            if (n_side_base == gSIM_HEALPIX_NSIDE) n_side_base /= 2 ;
            // assure that interpolation NSIDE is not too coarse:
            if (n_side_base < n_side_min) n_side_base = n_side_min ;

            hp_gridprop_base.SetNside(n_side_base, gSIM_HEALPIX_SCHEME) ;

            printf("        ... calculate smooth contributions on simple (linear) interpolation grid with NSIDE = %d ...\n", n_side_base);

            // extend interpolation grid a bit to have most pixels to interpolate between the base pixels:
            double extend_dist = 10. * hp_nside2resol(n_side_base) ;
            do_safegridextension(dtheta_orth, dtheta, extend_dist, grid_mode, dtheta_orth_extended, dtheta_extended) ;
            rs_pix_fov_base = hp_set_pix_fov(grid_mode, psi, theta,  dtheta_orth_extended,  dtheta_extended, n_side_base, gSIM_HEALPIX_SCHEME);

            int n_pix_fov_base = rs_pix_fov_base.nval(); // number of pixels in the FOV.
            // Create vector of all Healpix map pixels on interpolation grid:
            rs_pix_fov_base.toVector(v_pix_fov_base);

            jfactor_base.assign(n_pix_fov_base, 1.e-40);

            int percentage = 10 ;
            for (int i = 0; i < n_pix_fov_base; ++i) {
               int progress = (100 * i) / n_pix_fov_base  ;
               if (progress == percentage) {
                  printf("        ... %d%% done ...\n", progress);
                  percentage += 10 ;
               } ;

               ptg_i_pix_base = hp_gridprop_base.pix2ang(v_pix_fov_base[i]) ;
               psi_proper = ptg_i_pix_base.phi;
               theta_proper = PI / 2 - ptg_i_pix_base.theta;
               if (z > ZMIN_EXTRAGAL) {
                  phi_proper_to_phi_comoving = atan(1. / (1 + z) * MY_TAN(psitheta_to_phi(psi_proper, theta_proper))) / psitheta_to_phi(psi_proper, theta_proper) ;
                  // update psi_proper, theta_proper:
                  greatcircle_intermediate_points(0, 0, psi_proper, theta_proper,
                                                  phi_proper_to_phi_comoving,
                                                  psi_proper, theta_proper);
               }
               double jopt = 1.e-40; // keep here 1.e-40 instead of Healpix' blinded value to have
               // a cross check if really all pixels are filled.
               if (switch_j == 0) {
                  if (f_dm > 1.e-3)
                     jopt = jsmooth_mix(mtot, par_tot, psi_proper, theta_proper, lmin, lmax, eps, f_dm, par_dpdv);
                  else
                     jopt = jsmooth(par_tot, psi_proper, theta_proper, lmin, lmax, eps);
               } else if (switch_j == 1) {
                  // N.B.: we have to take into account all mass decades
                  for (int k = 0; k < n_mass; ++k) {
                     if (l_crit[k] < lmax)
                        jopt += jsub_continuum(ntot_subs, par_dpdv, psi_proper, theta_proper,
                                               l_crit[k], lmax,  par_subs, m1[k], m2[k]);
                  }
               } else if (switch_j == 2) {
                  // N.B.: we have to take into account all mass decades
                  for (int k = 0; k < n_mass; ++k) {
                     if (l_crit[k] < lmax)
                        jopt += frac_nsubs_in_m1m2(&par_subs[8], m1[k], m2[k], gSIM_EPS)
                                * jcrossprod_continuum(mtot, par_tot, psi_proper, theta_proper,
                                                       l_crit[k], lmax, eps, f_dm, par_dpdv);
                  }
               }
               if (jopt == 0.) jopt = 1.e-40;
               jfactor_base[i] = jopt;
            }
         }
         //--- Case 2: refine grids towards the halo center and use some log-like correction close to the center:
         else {

            // Check how close the FOV is to the halo center:
            double phi_min = fivedeg_diameter / 2. ;
            // check only the pixels intersecting the inner region with radius 5 degrees:
            vector<int> v_pix_intersectGC;
            rs_intersectGC.toVector(v_pix_intersectGC);
            int n_pix_intersectGC = rs_intersectGC.nval();
            for (int i = 0; i < n_pix_intersectGC; ++i) {
               ptg_i_pix = hp_gridprop.pix2ang(v_pix_intersectGC[i]) ;
               double phi_min_candidate = psitheta_to_phi(ptg_i_pix.phi, PI / 2 - ptg_i_pix.theta) ;
               if (phi_min_candidate < phi_min) {
                  phi_min = phi_min_candidate ;
               } ;
            } ;
            /* Check if FOV contains one of the four pixels around the halo center (just for
               interest, it is /\ not really necessary for the further calculation,
               but nice to    / 1\  know whether the FOV touches the halo center):
                             /\  /\
                            /3 \/ 2\
                            \  /\  /
                             \/4 \/
                              \  /
                               \/
               for NSIDE = even the GC (l,b) = (0,0) always lies in the middle
               of four pixels! For NSIDE = odd, the GC lies on a pixel center. */
            pointing ptg_GCpix1 = pointing(PI / 2. - SMALL_NUMBER, 0.);
            pointing ptg_GCpix2 = pointing(PI / 2., SMALL_NUMBER);
            pointing ptg_GCpix3 = pointing(PI / 2., - SMALL_NUMBER);
            pointing ptg_GCpix4 = pointing(PI / 2. + SMALL_NUMBER, 0.);
            int GCpixel1 = hp_gridprop.ang2pix(ptg_GCpix1);
            int GCpixel2 = hp_gridprop.ang2pix(ptg_GCpix2);
            int GCpixel3 = hp_gridprop.ang2pix(ptg_GCpix3);
            int GCpixel4 = hp_gridprop.ang2pix(ptg_GCpix4);
            if (rs_pix_fov.contains(GCpixel1) || rs_pix_fov.contains(GCpixel2)
                  || rs_pix_fov.contains(GCpixel3) || rs_pix_fov.contains(GCpixel4)) {
               printf("        ... FOV touches the halo center.  \n");
            } else printf("        ... closest distance of FOV to halo center is %.4f [deg].  \n", phi_min * RAD_to_DEG);

            // number of grid resolutions:
            int exp_min = (int)log2((double)n_side_min) ;
            int exp_max = (int)log2((double)n_side_max) ;
            int n_grids = exp_max - exp_min + 1 ;
            i_grid_min = 0 ;
            i_grid_max = n_grids - 1;

            printf("        ==> refine grid towards halo center on %d interpolation grids between NSIDE_min = %d and NSIDE_max = %d \n", n_grids, n_side_min, n_side_max);
            printf("            and use interpolation correction for the steep profile close to halo center...  \n");

            // divide the 180 degrees in n_grids log-spaced intervals:
            loggrid_radii.assign(n_grids, 1.e-40);
            double step_n_grids ;
            step_n_grids = pow(PI / alpha_quad_start, 1. / double(n_grids - 1)) ;
            int n_side_run = n_side_max ;

            // these objects are only temporarily used right here:
            rangeset<int> rs_hp_pix_outerGCdisk_base_run ;
            rangeset<int> rs_hp_pix_innerGCdisk_base_run ;
            rangeset<int> rs_hp_pix_ring_base_run ;

            // extend interpolation grid a bit to have most pixels to interpolate between the base pixels:

            double extend_dist = 10 * hp_nside2resol(n_side_min) ;
            //   cout << "before do safe "<<dtheta_orth << " " << dtheta << " " << extend_dist << " " << grid_mode << " " << dtheta_orth_extended << " " << dtheta_extended<<endl;
            do_safegridextension(dtheta_orth, dtheta, extend_dist, grid_mode, dtheta_orth_extended, dtheta_extended) ;
            //cout << "after do safe "<<dtheta_orth << " " << dtheta << " " << extend_dist << " " << grid_mode << " " << dtheta_orth_extended << " " << dtheta_extended<<endl;
            bool is_pixel_detected = false ;

            // compute innermost disk around GC (in case nside = power of 2, this corresponds to the original resolution ):
            loggrid_radii[0] = alpha_quad_start;
            double loggrid_diameter_extension = 10. * hp_nside2resol(n_side_run) ;
            double loggrid_outerdiameter_extended = 2. * loggrid_radii[0] +  loggrid_diameter_extension;
            hp_gridprop_base_run.SetNside(n_side_run, gSIM_HEALPIX_SCHEME) ;
            //cout << "            generate grid #0 with NSIDE = " << n_side_run  << " with radius "<< loggrid_radii[0] * RAD_to_DEG << " [deg] around GC ... " << endl;
            rs_hp_pix_ring_base_run = hp_set_pix_fov(disk_around_GC, GC_psi, GC_theta,  loggrid_outerdiameter_extended,  loggrid_outerdiameter_extended, n_side_run, gSIM_HEALPIX_SCHEME);
            //hp_gridprop_base_run.query_disc_inclusive  ( pointing( PI / 2. -  GC_theta, GC_psi ), loggrid_radius_extended, rs_hp_pix_ring_base_run, 32) ;

            rs_pix_fov_base_run = hp_set_pix_fov(grid_mode, psi, theta,  dtheta_orth_extended,  dtheta_extended, n_side_run, gSIM_HEALPIX_SCHEME);
            rs_pix_fov_base_run = rs_pix_fov_base_run.op_and(rs_hp_pix_ring_base_run) ;
            v_n_pix_i_grid.push_back(rs_pix_fov_base_run.nval());
            //cout << "            => contains "<< v_n_pix_i_grid[0] << " pixels." << endl;
            v_rs_pix_fov_base.push_back(rs_pix_fov_base_run) ;

            rs_pix_fov_base_run.toVector(v_pix_fov_base_run);
            v_v_pix_fov_base.push_back(v_pix_fov_base_run) ;

            v_hp_gridprop_base.push_back(hp_gridprop_base_run) ;
            rs_hp_pix_ring_base_run.clear() ;
            rs_pix_fov_base_run.clear() ;
            n_side_run = n_side_run / 2;

            if (n_grids > 1) {
               for (int i_grid = 1; i_grid < n_grids; ++i_grid) {
                  loggrid_radii[i_grid] =  alpha_quad_start * pow(step_n_grids, i_grid) ;
                  loggrid_diameter_extension = 10. * hp_nside2resol(n_side_run) ;
                  if (i_grid != n_grids - 1) {
                     loggrid_outerdiameter_extended = 2. * loggrid_radii[i_grid] + loggrid_diameter_extension ;
                  } else loggrid_outerdiameter_extended = 2. * loggrid_radii[i_grid] ;

                  double loggrid_innerdiameter_extended = 2. * loggrid_radii[i_grid - 1] -  loggrid_diameter_extension;

                  //cout << "            generate grid #"<< i_grid <<" with NSIDE = " << n_side_run  << " between radius "<< loggrid_radii[i_grid-1] * RAD_to_DEG << " [deg] and "<< loggrid_radii[i_grid] * RAD_to_DEG << " [deg] around GC ... " << endl;
                  rs_hp_pix_outerGCdisk_base_run = hp_set_pix_fov(disk_around_GC, GC_psi, GC_theta,  loggrid_outerdiameter_extended,  loggrid_outerdiameter_extended, n_side_run, gSIM_HEALPIX_SCHEME);

                  if (loggrid_innerdiameter_extended < loggrid_diameter_extension) rs_hp_pix_innerGCdisk_base_run.clear();
                  else rs_hp_pix_innerGCdisk_base_run = hp_set_pix_fov(disk_around_GC, GC_psi, GC_theta,  loggrid_outerdiameter_extended,  loggrid_innerdiameter_extended, n_side_run, gSIM_HEALPIX_SCHEME);
                  rs_hp_pix_ring_base_run = rs_hp_pix_outerGCdisk_base_run.op_andnot(rs_hp_pix_innerGCdisk_base_run) ;
                  rs_pix_fov_base_run = hp_set_pix_fov(grid_mode, psi, theta,  dtheta_orth_extended,  dtheta_extended, n_side_run, gSIM_HEALPIX_SCHEME);
                  rs_pix_fov_base_run = rs_pix_fov_base_run.op_and(rs_hp_pix_ring_base_run) ;
                  v_n_pix_i_grid.push_back(rs_pix_fov_base_run.nval());
                  //cout << "            => contains "<< v_n_pix_i_grid[i_grid] << " pixels." << endl;

                  // exclude grids from looping over later, if no FOV pixels fall into it
                  // (it should be safe doing it this way, as the grids containing pixels always lie next to each other.)
                  if (rs_pix_fov_base_run.nval() != 0) is_pixel_detected = true ;
                  if (rs_pix_fov_base_run.nval() == 0 && is_pixel_detected == false) i_grid_min = i_grid_min + 1 ;
                  else if (rs_pix_fov_base_run.nval() == 0 && is_pixel_detected == true) i_grid_max = i_grid_max - 1 ;

                  v_rs_pix_fov_base.push_back(rs_pix_fov_base_run) ;

                  rs_pix_fov_base_run.toVector(v_pix_fov_base_run);
                  v_v_pix_fov_base.push_back(v_pix_fov_base_run) ;

                  hp_gridprop_base_run.SetNside(n_side_run, gSIM_HEALPIX_SCHEME) ;
                  v_hp_gridprop_base.push_back(hp_gridprop_base_run) ;

                  n_side_run = n_side_run / 2;

                  // deallocate memory
                  rs_hp_pix_outerGCdisk_base_run.clear() ;
                  rs_hp_pix_innerGCdisk_base_run.clear() ;
                  rs_hp_pix_ring_base_run.clear() ;
                  rs_pix_fov_base_run.clear() ;
                  vector<int>().swap(v_pix_fov_base_run);
               }
            }

            printf("        ==> Calculate l.o.s. integrals on interpolation grids #%d to #%d ...\n", i_grid_min, i_grid_max);
            for (int i_grid = 0; i_grid < n_grids; ++i_grid) {
               v_jfactor_base_run.resize(v_n_pix_i_grid[i_grid]);
               v_v_jfactor_base.push_back(v_jfactor_base_run) ;
            }

            // Fill interpolation grids:
            for (int i_grid = i_grid_min ; i_grid <= i_grid_max; ++i_grid) {
               printf("        ... Fill interpolation grid #%d ...\n", i_grid);
               rs_pix_fov_base_run = v_rs_pix_fov_base[i_grid] ;
               hp_gridprop_base_run = v_hp_gridprop_base[i_grid] ;
               v_pix_fov_base_run = v_v_pix_fov_base[i_grid] ;
               int n_pix_fov_base = rs_pix_fov_base_run.nval(); // number of pixels in the FOV.

               int percentage = 10 ;
               for (int i = 0; i < n_pix_fov_base; ++i) {
                  if ((switch_j == 0 && n_pix_fov_base > 5000) || (switch_j != 0 && n_pix_fov_base > 1000)) {
                     int progress = (100 * i) / n_pix_fov_base  ;
                     if (progress == percentage) {
                        printf("        ... %d%% done ...\n", progress);
                        percentage += 10 ;
                     }
                  }

                  ptg_i_pix_base = hp_gridprop_base_run.pix2ang(v_pix_fov_base_run[i]) ;
                  psi_proper = ptg_i_pix_base.phi;
                  theta_proper = PI / 2 - ptg_i_pix_base.theta;
                  if (z > ZMIN_EXTRAGAL) {
                     phi_proper_to_phi_comoving = atan(1. / (1 + z) * MY_TAN(psitheta_to_phi(psi_proper, theta_proper))) / psitheta_to_phi(psi_proper, theta_proper) ;
                     // update psi_proper, theta_proper:
                     greatcircle_intermediate_points(0, 0, psi_proper, theta_proper,
                                                     phi_proper_to_phi_comoving,
                                                     psi_proper, theta_proper);
                  }
                  double jopt = 1.e-40; // keep here 1.e-40 instead of Healpix' blinded value to have
                  // a cross check if really all pixels are filled.
                  if (switch_j == 0) {
                     if (f_dm > 1.e-3)
                        jopt = jsmooth_mix(mtot, par_tot, psi_proper, theta_proper, lmin, lmax, eps, f_dm, par_dpdv);
                     else
                        jopt = jsmooth(par_tot, psi_proper, theta_proper, lmin, lmax, eps);
                  } else if (switch_j == 1) {
                     // N.B.: we have to take into account all mass decades
                     for (int k = 0; k < n_mass; ++k) {
                        if (l_crit[k] < lmax)
                           jopt += jsub_continuum(ntot_subs, par_dpdv, psi_proper, theta_proper,
                                                  l_crit[k], lmax,  par_subs, m1[k], m2[k]);
                     }
                  } else if (switch_j == 2) {
                     // N.B.: we have to take into account all mass decades
                     for (int k = 0; k < n_mass; ++k) {
                        if (l_crit[k] < lmax)
                           jopt += frac_nsubs_in_m1m2(&par_subs[8], m1[k], m2[k], gSIM_EPS)
                                   * jcrossprod_continuum(mtot, par_tot, psi_proper, theta_proper,
                                                          l_crit[k], lmax, eps, f_dm, par_dpdv);
                     }
                  }
                  if (jopt == 0.) jopt = 1.e-40;
                  v_v_jfactor_base[i_grid][i] = jopt;
               }
            }
         }
      }

      // interpolation grids have been calculated, now the actual interpolation on
      // the original grid has to be done:
      if (is_interpolate) printf("        ==> and interpolate for %d l.o.s. directions ...\n", n_pix_fov);

      // Fill j_smooth (use interpolation if required)
      int percentage = 10 ;
      int count_out_of_grid = 0 ; // count pixels whose values were calculated without interpolation.

      for (int i = 0; i < n_pix_fov; ++i) {
         bool is_really_interpolate = false ; // additional condition to check whether pixel
         // for interpolation lies between interpolation base pixels.
         if (!is_interpolate || n_pix_fov > 2e5) {
            int progress = (100 * i) / n_pix_fov  ;
            if (progress == percentage) {
               printf("        ... %d%% done ...\n", progress);
               percentage += 10 ;
            }
         }
         j_interp[i] = 1.e-40;
         ptg_i_pix = hp_gridprop.pix2ang(v_pix_fov[i]);
         psi_proper = ptg_i_pix.phi;
         theta_proper = PI / 2 - ptg_i_pix.theta;
         if (z > ZMIN_EXTRAGAL) {
            phi_proper_to_phi_comoving = atan(1. / (1 + z) * MY_TAN(psitheta_to_phi(psi_proper, theta_proper))) / psitheta_to_phi(psi_proper, theta_proper) ;
            // update psi_proper, theta_proper:
            greatcircle_intermediate_points(0, 0, psi_proper, theta_proper,
                                            phi_proper_to_phi_comoving,
                                            psi_proper, theta_proper);
         }
         // Interpolation if applies...
         //--- Case 1: simple base grid:
         if (is_interpolate && is_simple_interp) {
            // This is a 2D lin-log interpolation:
            hp_gridprop_base.get_interpol(ptg_i_pix, pix_base, wgt_base);
            //hp_get_loglog_interpol( hp_gridprop_base, ptg_i_pix, pix_base, wgt_base);
            if (rs_pix_fov_base.contains(pix_base[0]) &&  rs_pix_fov_base.contains(pix_base[1]) &&  rs_pix_fov_base.contains(pix_base[2]) &&  rs_pix_fov_base.contains(pix_base[3])) {
               // N.B.: we only interpolate if the corresponding pixel lies within the interpolation grid
               // (additionally to the above interpolation grid extension we do this additional check to avoid
               //  interpolation problems/buggy crashes because of no values)
               is_really_interpolate = true ;
               i0 = binary_search(v_pix_fov_base, pix_base[0]);
               j_base[0] = jfactor_base[i0] ;
               i1 = binary_search(v_pix_fov_base, pix_base[1]);
               j_base[1] = jfactor_base[i1] ;
               i2 = binary_search(v_pix_fov_base, pix_base[2]);
               j_base[2] = jfactor_base[i2] ;
               i3 = binary_search(v_pix_fov_base, pix_base[3]);
               j_base[3] = jfactor_base[i3] ;
               // interpolate!
               j_interp[i] = hp_log_interp2D(j_base, wgt_base);
            } ;
         }
         //--- Case 2: use grid-refinement and log-like correction close to the GC
         else if (is_interpolate && !is_simple_interp) {
            double dist_GC_pix = psitheta_to_phi(ptg_i_pix.phi, PI / 2 - ptg_i_pix.theta);

            double loglog_scale_rad =  0.5 * pow(10., -3) * gSIM_RESOLUTION * jmax / jmin ; // this factor decreases almost linearly with
            // increasing alpha_int and increases linearly with increasing angle gSIM_RESOLUTION (i.e., lower resolution).
            // If alpha_int is coupled to the resolution, it depends on the steepness of the profile, which effect dominates:
            // -> very steep profile at the GC: factor decreases with increasing alpha_int and gSIM_RESOLUTION
            // -> profile slowly rising at the GC: factor increases with increasing gSIM_RESOLUTION.
            // alternatively: 2 * alpha_quad_start?

            double loginterpolexponent = exp(- dist_GC_pix / loglog_scale_rad) ;

            for (int i_grid = i_grid_min ; i_grid <= i_grid_max; ++i_grid) {
               if (dist_GC_pix <= loggrid_radii[i_grid]) {
                  rs_pix_fov_base_run = v_rs_pix_fov_base[i_grid] ;
                  v_pix_fov_base_run = v_v_pix_fov_base[i_grid] ;
                  hp_gridprop_base_run = v_hp_gridprop_base[i_grid] ;
                  hp_gridprop_base_run.get_interpol(ptg_i_pix, pix_base, wgt_base);
                  if (rs_pix_fov_base_run.contains(pix_base[0]) &&  rs_pix_fov_base_run.contains(pix_base[1])
                        &&  rs_pix_fov_base_run.contains(pix_base[2]) &&  rs_pix_fov_base_run.contains(pix_base[3])) {
                     // N.B.: we only interpolate if the corresponding pixel lies within the interpolation grid
                     // (additionally to the above interpolation grid extension, we do this additional check to avoid
                     //  interpolation problems/buggy crashes because of no values)
                     is_really_interpolate = true;

                     double normalize_weight = 0;
                     for (size_t m = 0; m < 4; ++m) {
                        ptg_i_pix_base = hp_gridprop_base_run.pix2ang(pix_base[m]);
                        arr_dist_GC_base[m] = psitheta_to_phi(ptg_i_pix_base.phi, PI / 2 - ptg_i_pix_base.theta);
                        normalize_weight += pow(arr_dist_GC_base[m], loginterpolexponent) * wgt_base[m] ;
                     }

                     for (size_t m = 0; m < 4; ++m) {
                        wgt_base[m] = pow(arr_dist_GC_base[m], loginterpolexponent) * wgt_base[m] / normalize_weight ;
                     }

                     i0 = binary_search(v_pix_fov_base_run, pix_base[0]);
                     j_base[0] = v_v_jfactor_base[i_grid][i0] ;
                     i1 = binary_search(v_pix_fov_base_run, pix_base[1]);
                     j_base[1] = v_v_jfactor_base[i_grid][i1] ;
                     i2 = binary_search(v_pix_fov_base_run, pix_base[2]);
                     j_base[2] = v_v_jfactor_base[i_grid][i2] ;
                     i3 = binary_search(v_pix_fov_base_run, pix_base[3]);
                     j_base[3] = v_v_jfactor_base[i_grid][i3] ;
                     // interpolate!
                     j_interp[i] = hp_log_interp2D(j_base, wgt_base);
                     rs_pix_fov_base_run.clear();
                     break;
                  }
               }
               rs_pix_fov_base_run.clear();
               vector<int>().swap(v_pix_fov_base_run) ;
            }
         }

         // compute los integral directly without interpolating:
         if (!is_really_interpolate) {
            count_out_of_grid += 1 ;
            if (count_out_of_grid == 1 && is_interpolate) {
               print_warning("healpix_fits.cc", "j_interpolation()", "There are some l.o.s. direction falling out of the interpolation grid(s). "
                             "This may heavily slow down the calculation and should not happen unless on very coarse grids (tell the developers otherwise!)");
            }
            if (switch_j == 0)
               j_interp[ i ] = jsmooth_mix(mtot, par_tot, psi_proper, theta_proper, eps, f_dm, par_dpdv);
            else if (switch_j == 1) {
               for (int k = 0; k < n_mass; ++k) {
                  if (l_crit[k] < lmax)
                     j_interp[i ] += jsub_continuum(ntot_subs, par_dpdv, psi_proper, theta_proper,
                                                    l_crit[k], lmax,  par_subs, m1[k], m2[k]);
               }
            } else if (switch_j == 2) {
               for (int k = 0; k < n_mass; ++k) {
                  if (l_crit[k] < lmax)
                     j_interp[i ] += frac_nsubs_in_m1m2(&par_subs[8], m1[k], m2[k], gSIM_EPS)
                                     * jcrossprod_continuum(mtot, par_tot, psi_proper, theta_proper,
                                                            l_crit[k], lmax, eps, f_dm, par_dpdv);
               }
            }
         }
      }
      printf("            %d l.o.s. directions calculated without interpolating (%.1f%%)", count_out_of_grid, 100. * float(count_out_of_grid) / float(n_pix_fov));
      printf("\n");

      // deallocate memory for very big objects:
      jfactor_base.clear();
      rs_pix_fov_base.clear() ; // rs_pix_fov_base.reserve( 0 ) ;
      rs_pix_aroundGC.clear() ;
      vector<int>().swap(v_pix_fov_base);
      vector< vector<int> >().swap(v_v_pix_fov_base);
      vector< rangeset<int> >().swap(v_rs_pix_fov_base);
      vector< vector<double> >().swap(v_v_jfactor_base);
   }

   // check for NaN values and convert units
   for (int i = 0; i < n_pix_fov; ++i) {
      if (std::isinf((float)j_interp[i]) || isnan(float(j_interp[i]))) {
         j_interp[i] = 1.e-40;
         ptg_i_pix = hp_gridprop.pix2ang(v_pix_fov[i]) ;
         double psi_nan = ptg_i_pix.phi;
         check_psi(psi_nan) ;
         double theta_nan = PI / 2. - ptg_i_pix.theta ;
         sprintf(char_tmp, "Found nan-value at (psi, theta) = (%.2le,%.2le) deg! Replace with 1e-40.", psi_nan * RAD_to_DEG, theta_nan * RAD_to_DEG);
         print_warning("healpix_fits.cc", "j_interpolation()", string(char_tmp));
      }
   }
   vector<int>().swap(v_pix_fov);

   cout << endl;

   gSIM_ALPHAINT = alphaint_orig;
   return;
}

//______________________________________________________________________________
double hp_log_interp2D(fix_arr<double, 4> &base_values, fix_arr<double, 4> &wgt)
{
   //--- Returns lin-log simple 2D interpolation ("biLOG") for f(x,y) on a 2D grid,
   //    with the weights given by Healpix' get_interpol() function.
   //    This is nothing else than the adaption of a simple 2D bilinear interpolation:
   //    A simple bilinear interpolation can be written as (check, e.g., Wikipedia):
   //      f_int = Sum wgt_i * f_i, with f_i the four base points f11, f12, f21, f22 and
   //      wgt[0] = (x2 - x)  * (y2 - y) / (  (x2 -x1) * (y2 -y1) )
   //      wgt[1] = (x2 - x)  * (y - y1) / (  (x2 -x1) * (y2 -y1) )
   //      wgt[2] = (x - x1)  * (y2 - y) / (  (x2 -x1) * (y2 -y1) )
   //      wgt[3] = (x - x1)  * (y - y1) / (  (x2 -x1) * (y2 -y1) )
   //        -> Sum wgt_i = 1 ;
   //    which should yield, combined with the respective routines for getting the
   //    (linear) interpolation weights (T_Healpix_Base.get_interpol()), exactly the
   //    same result as with the two nested 1D loop lin-log-interpolations in clumpy v1.
   //    Now, log(f_int) = Sum wgt_i * log(f_i), and one obtains the expression:

   return pow(base_values[0], wgt[0]) *   pow(base_values[1], wgt[1])
          * pow(base_values[2], wgt[2]) *  pow(base_values[3], wgt[3]) ;
}


//______________________________________________________________________________
double hp_angular_dist(const vec3_t<double> vec3_a, const vec3_t<double> vec3_b)
{
   //--- computes the angular distance dist (in rad) between 2 vectors vec3_a and vec3_b
   //    dist = atan2 ( |vec3_a x vec3_b| / (vec3_a . vec3_b) )
   //    (more accurate than acos(vec3_a. vec3_b) when dist close to 0 or Pi.

   // INPUTS:
   //  vec3_a      3D vector of Healpix class vec3_t
   //  vec3_b      3D vector of Healpix class vec3_t
   // OUTPUTS:
   //  angular distance between vec3_a and vec3_b in [rad]


   // scalar product     s = A. cos(theta)
   double sprod = dotprod(vec3_a, vec3_b);
   // vectorial product  v = A. sin(theta)
   vec3_t<double> vec3_c = crossprod(vec3_a, vec3_b);
   double vprod = sqrt(dotprod(vec3_c, vec3_c));
   // theta = atan( |v|/s) in [0,Pi]
   return atan2(vprod, sprod);
}
