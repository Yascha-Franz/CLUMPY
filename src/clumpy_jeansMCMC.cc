// C++ std libraries
#include <stdlib.h>
using namespace std;

// CLUMPY includes
#include "../include/clumps.h"
#include "../include/geometry.h"
#include "../include/inlines.h"
#include "../include/integr.h"
#include "../include/integr_los.h"
#include "../include/janalysis.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/profiles.h"
#include "../include/stat.h"
#include "../include/jeans_analysis.h"

// GreAT includes
#include "TGreatModel.h"
#include "TGreatManager.h"
#include "TGreatEstimator.h"
#include "TGreatPoint.h"
#include "TGreatMCMCStep.h"
#include "TGreatMCMCAlgorithmCovariance.h"


// ROOT includes
#if IS_ROOT
#include <TApplication.h>
#include <TRandom.h>
#include <TFile.h>
#endif

const int n_stat_params = 9; //number of possible free parameters
const int n_jeans_param = 20; // number of jeans parameters

string names_jeans_params [n_jeans_param] = {"log10(rho_s)", "log10(r_s)", "alpha", "beta", "gamma", "DM_profile", "Rvir", "rho_s*", "r_s*", "alpha*", "beta*", "gamma*", "Light_profile", "R", "eps", "beta_0", "beta_infinity", "log10(r_a)", "eta", "Aniso_profile"};
double param[n_jeans_param]; // initial values + needed values for jeans analysis

struct gStructHalo stat_halo; // structure containing the Jeans parameters (filled from a Jeans analysis parameters file)
struct gStructJeansAnalysis stat_analysis; // structure containing the kinematic data (filled from a Jeans kinematic data file)

int N = 0; // number of trials points
TGreatManager<TGreatMCMCAlgorithmCovariance> *MyManager;
double vmean = 0.; // mean velocity of the dSph (for unbinned analysis)
double eps; // precision

int type_analysis; // 0: binned with data = sigmap2; 1: binned with data = sigmap; 2: unbinned
int ndata; // number of points in data file - used only for the header of the output file

//______________________________________________________________________________
void Create_file(double longit, double lat, double dist, int numberdata, string name, string inputname)
{

   // This function creates a statistical file (readable by CLUMPY, options -s) from the MCMC output .root file.

   // Name of output .dat file
   string dat = ".dat";
   string outputfile;;
   outputfile = inputname;
   outputfile.erase(outputfile.end() - 5, outputfile.end());
   outputfile = outputfile + dat ;

   // Names of the parameters written in the statistical file.
   vector<string> names_params_to_write;
   names_params_to_write.push_back("rhos(Msol/kpc3)");
   names_params_to_write.push_back("rs(kpc)");
   names_params_to_write.push_back("Rvir(kpc)");
   names_params_to_write.push_back("profile");
   names_params_to_write.push_back("alpha");
   names_params_to_write.push_back("beta");
   names_params_to_write.push_back("gamma");
   names_params_to_write.push_back("lightprofile");
   names_params_to_write.push_back("L(Lsol)");
   names_params_to_write.push_back("rL(kpc)");
   names_params_to_write.push_back("alpha*");
   names_params_to_write.push_back("beta*");
   names_params_to_write.push_back("gamma*");
   names_params_to_write.push_back("anisoprofile");
   names_params_to_write.push_back("beta0");
   names_params_to_write.push_back("betainf");
   names_params_to_write.push_back("raniso(kpc)");
   names_params_to_write.push_back("eta");
   names_params_to_write.push_back("chi2");


   // Get the tree from the MCMC .root file
   TFile *file = TFile::Open(inputname.c_str());
   TTree *mcmc = (TTree *) file->Get("mcmc"); // The tree must have the name "mcmc"

   // Construct an estimator
   TGreatEstimator<TGreatMCMCAlgorithmCovariance> *estimator = new TGreatEstimator<TGreatMCMCAlgorithmCovariance>(mcmc);

   int Nfreepar =  estimator->GetNParameters(); // Number of free parameters
   vector<double> Likelihood;
   vector< vector <double> > Parameter;

   // Get the parameters values from the MCMC. Only the "independant" samples are considered (burn-in and correlations removed)
   for (TGreatMCMCSample *sample = estimator->GetFirstIndSample(); sample != 0; sample = estimator->GetNextIndSample()) {
      vector<double> line;
      for (int i = 0; i < n_jeans_param; i++) {
         line.push_back(sample->GetValue(names_jeans_params[i].c_str()));
      }
      Parameter.push_back(line);
      Likelihood.push_back(sample->GetLogProb());
   }

   // Create the header of the output .txt file
   FILE *fp;
   fp = fopen(outputfile.c_str(), "w");
   fprintf(fp, "Name     long.(deg)  lat.(deg)  l(kpc)  Ndata  Npar  Rmax\n");
   fprintf(fp, "%s         %le        %le         %le     %d    %d   %le \n\n", name.c_str(), longit, lat, dist, numberdata, Nfreepar, gSIM_JEANS_RMAX);

   for (int k = 0; k < int(names_params_to_write.size()); k++) {
      if (k == 0)
         fprintf(fp, "%s", names_params_to_write[k].c_str());
      else if (k == int(names_params_to_write.size()) - 1)
         fprintf(fp, " %s \n", names_params_to_write[k].c_str());
      else
         fprintf(fp, " %s", names_params_to_write[k].c_str());
   }

   // Fills the output .txt file
   for (int p = 0; p < int(Parameter.size()); p++) {
      string k = "k";
      string DM_prof = string(gNAMES_PROFILE[int(Parameter[p][5])]);
      DM_prof = k + DM_prof;
      string Light_prof = gNAMES_LIGHTPROFILE[int(Parameter[p][12])];
      Light_prof = k + Light_prof;
      string Ani_prof = gNAMES_ANISOTROPYPROFILE[int(Parameter[p][19])];
      Ani_prof = k + Ani_prof;

      fprintf(fp, "%le  %le  %le  %s  %le %le %le %s %le %le %le %le %le %s %le %le %le %le %le\n", pow(10, Parameter[p][0]), pow(10, Parameter[p][1]), stat_analysis.HaloSize,
              DM_prof.c_str(), Parameter[p][2], Parameter[p][3], Parameter[p][4], Light_prof.c_str(), Parameter[p][7], Parameter[p][8], Parameter[p][9],
              Parameter[p][10], Parameter[p][11], Ani_prof.c_str(),  Parameter[p][15],  Parameter[p][16],  pow(10., Parameter[p][17]),  Parameter[p][18], Likelihood[p]);
   }

   fclose(fp);
   cout << "file saved in : " << outputfile.c_str() << endl;
}

//______________________________________________________________________________
void SetParameters()    // initialization of the parameters
{
   // Set the values of the parameters from the Jeans analysis file

   //  param[0]     Dark matter density normalisation: density [Msol/kpc^3]
   //  param[1]     Dark matter density Scale radius [kpc]
   //  param[2]     Dark matter Shape parameter #1
   //  param[3]     Dark matter Shape parameter #2
   //  param[4]     Dark matter Shape parameter #3
   //  param[5]     Dark matter card_profile [gENUM_PROFILE]
   //  param[6]     Max radius of integration
   //  param[7]     Normalisation of Light profile [Lsol]
   //  param[8]     Light profile scale radius [kpc]
   //  param[9]     Light profile Shape parameter #1
   //  param[10]    Light profile Shape parameter #2
   //  param[11]    Light profile Shape parameter #3
   //  param[12]    Light profile card_profile [gENUM_LIGHTPROFILE] for Light Profile
   //  param[13]    R : projected radius considered for calculation of a projected quantity (kpc) // unused
   //  param[14]    eps : precision for integrations
   //  param[15]    Anisotropy at r=0
   //  param[17]    Anisotropy at r=+infinity
   //  param[17]    Scale radius [kpc]
   //  param[18]    Shape parameter
   //  param[19]    card_profile [gENUM_ANISOTROPYPROFILE] for Anisotropy Profile

   param[0] = stat_analysis.Value[0];
   param[1] = stat_analysis.Value[1];
   param[2] = stat_analysis.Value[2];
   param[3] = stat_analysis.Value[3];
   param[4] = stat_analysis.Value[4];
   param[5] = stat_analysis.HaloProfile;
   param[6] = stat_analysis.HaloSize * 100.;
   param[7] = stat_analysis.LightParam[0];
   param[8] = stat_analysis.LightParam[1];
   param[9] = stat_analysis.LightParam[2];
   param[10] = stat_analysis.LightParam[3];
   param[11] = stat_analysis.LightParam[4];
   param[12] = stat_analysis.LightProfile;
   param[13] = 100.; // unused
   param[14] = eps;
   param[15] = stat_analysis.Value[5];
   param[16] = stat_analysis.Value[6];
   param[17] = stat_analysis.Value[7];
   param[18] = stat_analysis.Value[8];
   param[19] = stat_analysis.AnisoProfile;

   return;
}


//______________________________________________________________________________
double MyLogLikelihoodFunctionSigmap(TGreatPoint &point)
{
   // Number of trials points proposed
   N = N + 1;
   if (N % 1000 == 0)
      cout << N << " trial points proposed" << endl;

   // Get proposed points to compute the likelihood
   for (int k = 0; k < n_jeans_param; k++)
      param[k] = point.GetValue(names_jeans_params[k]);

   // if proposed parameters are out of range: skip the point
   for (int k = 0; k < 5; k++) {
      if (stat_analysis.IsFreeParam[k] == 1) {
         if (point.GetValue(names_jeans_params[k]) < stat_analysis.LowerRange[k] || point.GetValue(names_jeans_params[k]) > stat_analysis.UpperRange[k])
            return -99.e15;
      }
   }
   for (int k = 5; k < 9; k++) {
      if (stat_analysis.IsFreeParam[k] == 1) {
         if (point.GetValue(names_jeans_params[k + 10]) < stat_analysis.LowerRange[k] || point.GetValue(names_jeans_params[k + 10]) > stat_analysis.UpperRange[k])
            return -99.e15;
      }
   }
   // Copy parameters proposed by the MCMC
   double parameter[n_jeans_param];
   for (int k = 0; k < n_jeans_param; k++)
      parameter[k] = param[k];

   // put parameters back to linear scale
   parameter[0] = pow(10., parameter[0]);
   parameter[1] = pow(10., parameter[1]);
   parameter[17] = pow(10., parameter[17]);

   return log_likelihood_jeans_binned(parameter, stat_halo, type_analysis);
}

//______________________________________________________________________________
double MyLogLikelihoodFunctionVel(TGreatPoint &point)
{
   // Number of trials points proposed
   N = N + 1;
   if (N % 1000 == 0)
      cout << N << " trial points proposed" << endl;

   // Get proposed points to compute the likelihood
   for (int k = 0; k < n_jeans_param; k++)
      param[k] = point.GetValue(names_jeans_params[k]);

   // if proposed parameters are out of range: skip the point
   for (int k = 0; k < 5; k++) {
      if (stat_analysis.IsFreeParam[k] == 1) {
         if (point.GetValue(names_jeans_params[k]) < stat_analysis.LowerRange[k] || point.GetValue(names_jeans_params[k]) > stat_analysis.UpperRange[k])
            return -99.e15;
      }
   }
   for (int k = 5; k < 9; k++) {
      if (stat_analysis.IsFreeParam[k] == 1) {
         if (point.GetValue(names_jeans_params[k + 10]) < stat_analysis.LowerRange[k] || point.GetValue(names_jeans_params[k + 10]) > stat_analysis.UpperRange[k])
            return -99.e15;
      }
   }

   // Copy parameters proposed by the MCMC
   double parameter[n_jeans_param];
   for (int k = 0; k < n_jeans_param; k++)
      parameter[k] = param[k];

   // put parameters back to linear scale
   parameter[0] = pow(10., parameter[0]);
   parameter[1] = pow(10., parameter[1]);
   parameter[17] = pow(10., parameter[17]);

   return log_likelihood_jeans_unbinned(parameter, stat_halo, vmean);
}

//______________________________________________________________________________
int Run(string outputfilemcmc, string outputfileana, int typeanalysis, int Nchains, int Npoints)
{

   // Create a model
   TGreatModel *MyModel = new TGreatModel();
   // Set the free parameters
   for (int k = 0; k < n_stat_params; k++) {
      if (stat_analysis.IsFreeParam[k] == 1) {
         if (k < 5)
            MyModel->AddParameter(names_jeans_params[k], "", stat_analysis.Value[k], stat_analysis.LowerRange[k], stat_analysis.UpperRange[k], TGreatParameter::kFreeP);
         else
            MyModel->AddParameter(names_jeans_params[k + 10], "", stat_analysis.Value[k], stat_analysis.LowerRange[k], stat_analysis.UpperRange[k], TGreatParameter::kFreeP);
      }
   }
   // Set the fixed parameters
   for (int k = 0; k < n_stat_params; k++) {
      if (stat_analysis.IsFreeParam[k] == 0) {
         if (k < 5)
            MyModel->AddParameter(names_jeans_params[k], "", stat_analysis.Value[k], stat_analysis.LowerRange[k], stat_analysis.UpperRange[k], TGreatParameter::kFixedP);
         else
            MyModel->AddParameter(names_jeans_params[k + 10], "", stat_analysis.Value[k], stat_analysis.LowerRange[k], stat_analysis.UpperRange[k], TGreatParameter::kFixedP);
      }
   }

   // Add the other fixed parameters to MyModel
   for (int j = 5; j < 15; j++)
      MyModel->AddParameter(names_jeans_params[j], "", param[j], 0., 1., TGreatParameter::kFixedP);
   MyModel->AddParameter(names_jeans_params[19], "", param[19], 0., 1., TGreatParameter::kFixedP);

   // Set the likelihood function
   if (typeanalysis == 0 || typeanalysis == 1) //Sigmap keyword in the data file
      MyModel->SetLogLikelihoodFunction(MyLogLikelihoodFunctionSigmap);
   else if (typeanalysis == 2) //Vel keyword in the data file
      MyModel->SetLogLikelihoodFunction(MyLogLikelihoodFunctionVel);

   //Define the factory to run the algorithm
   MyManager = new TGreatManager<TGreatMCMCAlgorithmCovariance> (MyModel);
   MyManager->GetAlgorithm()->SetUpdateStatus(true); // update the covariance matrix after each chain

   // Get the covariance matrix from a previous run if it exits, otherwise the
   // default covariance matrix will be used.

   TFile *File = new TFile(outputfileana.c_str());

   if (!File->IsZombie()) {
      TMatrixD *Cov = (TMatrixD *) File->Get("Cov");
      if (Cov) MyManager->GetAlgorithm()->SetCovarianceMatrix(Cov->GetMatrixArray());
   }

   // Set output file name
   MyManager->SetOutputFileName(outputfilemcmc.c_str());
   MyManager->SetTreeName("mcmc");

   // Set number of chains
   MyManager->SetNTrialLists(Nchains);
   // Set number of trial points per chain
   MyManager->SetNTrials(Npoints);

   // Run the MCMC analysis
   MyManager->Run();
   delete MyModel;
   return 0;
}

//______________________________________________________________________________
void Ana(string const &outputfileana, string const &mcmcfile = "0")
{
   string name;
   if (mcmcfile == "0") {
      name = MyManager->GetOutputFileName();
   } else {
      name = mcmcfile;
   }

   // Get the tree from the file produced in the function Run()
   TFile *file = TFile::Open(name.c_str());
   TTree *mcmc = (TTree *) file->Get("mcmc");

   // Construct an estimator
   TGreatEstimator<TGreatMCMCAlgorithmCovariance> *estimator = new TGreatEstimator<TGreatMCMCAlgorithmCovariance>(mcmc);

   // Compute and plot the PDF for the free parameters of the model
   estimator->ComputePDF();
   estimator->PlotPDF();

   // Compute and print the covariance matrix of the free parameters of the model
   TVectorD *Mu;
   TMatrixD *Cov;
   estimator->ComputeCovarianceMatrix(Mu, Cov);

   TFile *fileout = TFile::Open(outputfileana.c_str(), "recreate");
   Mu->Write("Mu");
   Cov->Write("Cov");
   fileout->Close();

   // Display covariance matrix
   Mu->Print();
   Cov->Print();

}

//______________________________________________________________________________
void MCMCanalysis(string const &datafile, string const &outputfilemcmc, string const &outputfileana, int nchains, int npoints)
{

   SetParameters(); // Set parameters values
   halo_load_data4jeans(datafile, stat_halo, 0); // load the data
   ndata = int(stat_halo.JeansData.Radius.size());

   cout << "---------------------------------------------------" << endl;

   if (stat_halo.JeansData.TypeData == "Sigmap2") { // Binned with data = sigmap2
      type_analysis = 0;
      cout << "        Binned MCMC Jeans analysis - Sigmap2" << endl;
   } else if (stat_halo.JeansData.TypeData == "Sigmap") { // Binned with data = sigmap
      type_analysis = 1;
      cout << "        Binned MCMC Jeans analysis - Sigmap" << endl;
   } else if (stat_halo.JeansData.TypeData == "Vel") { // Unbinned
      type_analysis = 2;
      cout << "        Unbinned MCMC Jeans analysis - Vel" << endl;
   } else {
      cout << "        Error: wrong type of TypeData in " << datafile << endl;
      return;
   }

   cout << "---------------------------------------------------" << endl;
   cout << "Free parameters:" << endl;
   for (int k = 0; k < n_stat_params; k++) {
      if (stat_analysis.IsFreeParam[k] == 1) {
         if (k < 5)
            cout << names_jeans_params[k] << endl;
         else
            cout << names_jeans_params[k + 10] << endl;
      }
   }

   // Compute mean velocity for unbinned analysis
   double termup = 0;
   double termdown = 0.;
   if (type_analysis == 2) {
      for (int k = 0; k < ndata; k++) {
         termup += stat_halo.JeansData.MembershipProb[k] * stat_halo.JeansData.VelocData[k] / pow(stat_halo.JeansData.ErrVelocData[k], 2);
         termdown += stat_halo.JeansData.MembershipProb[k] / pow(stat_halo.JeansData.ErrVelocData[k], 2);
      }
   }

   if (type_analysis == 2)
      vmean = termup / termdown;

   Run(outputfilemcmc, outputfileana, type_analysis, nchains, npoints);
   Create_file(stat_analysis.Longitude, stat_analysis.Latitude, stat_analysis.Distance, ndata, stat_analysis.Name, MyManager->GetOutputFileName());
   Ana(outputfileana);

}

//______________________________________________________________________________
int main(int argc, char *argv [])
{
   //--- Root application (enables displays)
#if IS_ROOT
   gSIM_ROOTAPP = new TApplication("App", NULL, NULL);
   gSIM_CLUMPYAD = orphanCLUMPYadonplots();
#endif

   // get the absolute path of executable:
   get_clumpy_dirs(string(argv[0]));

   bool is_arg = true;
   if (argc <= 1 || (argc >= 2 && ((string)argv[1]).substr(0, 2) != "-r"))
      is_arg = false;
   else {
      string opt = argv[1];

      //#################################//
      //# MCMC analysis related calls  #//
      //#################################//

      // Print example values
      if (opt.find_first_of("D") != string::npos) {
         printf("   MCMC Jeans analysis:  %s -r %sdata/data_sigmap.txt output/stat_example.root output/stat_exampleAna.root 10000 8 %sdata/params_jeans.txt 0.05\n", argv[0], gPATH_TO_CLUMPY_HOME.c_str(), gPATH_TO_CLUMPY_HOME.c_str());
         printf("\n");
         return 1;
      }
   }

   // Usage of -1, -2, ..
   if (!is_arg || argc != 9) {
      printf("  usage:\n");
      printf("   [MCMC analysis:         ]  %s -r data_file OutputfileMCMC OutputfileAna Npoints Nchains params_file eps\n", argv[0]);
      printf("        -> data_file: file name (for sigma_p or velocity data)\n");
      printf("        -> OutputfileMCMC: name of output MCMC root file\n");
      printf("        -> OutputfileAna: name of output analysis root file\n");
      printf("        -> Npoints: number of trials points per chain\n");
      printf("        -> Nchains: number of chains\n");
      printf("        -> params_file: Jeans parameter file name\n");
      printf("        -> eps: precision\n");
      printf("\n");
      printf(" N.B.: to see default parameter values (examples), use \"-rD\". \n");
      printf(" If a previous OutputfileAna exists, its covariance matrix will be used for the proposition function of this run.\n");
      printf(" !!! If the list of free parameters is changed, using the previous OutputfileAna will cause a crash. Please change the name of the output. !!! \n\n");
      printf(" The choice of analysis (binned or unbinned) depends on the Jeans keyword from data_file.\n");

      printf("\n\n");
      return 1;
   }

   string datafile = argv[2];
   string name_mcmc = argv[3];
   string outdir_mcmc = name_mcmc.substr(0, name_mcmc.find_last_of("\\/"));
   if (name_mcmc.size() < 6) {
      cout << "Error: OutputfileMCMC must end by \".root\" " << endl;
      return 0;
   }
   if (outdir_mcmc != name_mcmc) makedir(outdir_mcmc.c_str());

   string root = name_mcmc.substr(name_mcmc.size() - 5);
   if (root != ".root") {
      cout << "Error: OutputfileMCMC must end by \".root\" " << endl;
      return 0;
   }

   string name_ana = argv[4];
   string outdir_ana = name_ana.substr(0, name_ana.find_last_of("\\/"));
   if (name_ana.size() < 6) {
      cout << "Error: OutputfileAna must end by \".root\" " << endl;
      return 0;
   }
   if (outdir_ana != name_ana) makedir(outdir_ana.c_str());

   string rootana = name_ana.substr(name_ana.size() - 5);
   if (rootana != ".root") {
      cout << "Error: OutputfileAna must end by \".root\" " << endl;
      return 0;
   }

   int Npoints = atoi(argv[5]);
   int Nchains = atoi(argv[6]);

   string file_name = argv[7];
   load_jeansanalysis_parameters(file_name, stat_analysis);
   eps = atof(argv[8]);

   // Run the analysis
   MCMCanalysis(datafile, name_mcmc, name_ana, Nchains, Npoints);

   gSIM_ROOTAPP->Run(0);

#if IS_ROOT
   delete gSIM_CLUMPYAD;
#endif
}

/*! \file clumpy_jeansMCMC.cc
  \brief <b>Jeans analysis executable</b> (MCMC analysis, only if GreAT is installed)
*/
