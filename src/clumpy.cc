// CLUMPY includes
#include "../include/extragal.h"
#include "../include/healpix_fits.h"
#include "../include/inlines.h"
#include "../include/janalysis.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/spectra.h"

// C++ std libraries
//using namespace std;
//#include <iostream>
//#include <math.h>
//#include <stdlib.h>

//______________________________________________________________________________
int main(int argc, char *argv [])
{

   //--- Root application (enables displays): Quite susceptible to segmentation faults.
#if IS_ROOT
   gSIM_ROOTAPP = new TApplication("App", NULL, NULL);
   gSIM_CLUMPYAD = orphanCLUMPYadonplots();
#endif


   // get the absolute path of executable:
   get_clumpy_dirs(string(argv[0]));

   // update code version to print, if git repository is present:
   string git_command = "cd " + gPATH_TO_CLUMPY_HOME + "; git describe --abbrev=7 --always --tags; cd " + gPATH_TO_USER_EXE;
   string git_tag = sys_command(git_command);
   git_tag = removeblanks_from_startstop(git_tag);
   git_tag = removechars(git_tag, "\n");
   string clumpy_version_print = gCLUMPY_VERSION;
   if (git_tag.size() > 0) {
      string git_commit_hash;
      string git_commit_nr;
      git_commit_nr = git_tag.substr(git_tag.find_first_of("-") + 1, git_tag.size() - 1);
      git_commit_nr = git_commit_nr.substr(0, git_commit_nr.find_last_of("-"));

      string ::size_type checknr = git_commit_nr.substr(0, 1).find_first_of("0123456789");

      git_commit_hash = string(git_tag.substr(git_tag.find_last_of("-") + 2, git_tag.size() - 1));
      string idprint = ", git-ID " + git_commit_hash;

      if (checknr == string::npos) {
         git_commit_nr = "0"; // tag = zero commits after tag
         idprint = "";
      }

      clumpy_version_print = clumpy_version_print + idprint + " (" + git_commit_nr + " commits after last tag)";
      // maschine-readable ID info:
      gCLUMPY_VERSION = gCLUMPY_VERSION + "_ID" + git_commit_hash;
   }
   int version_len = clumpy_version_print.size();
   int space1 = int(floor((78 - version_len) / 2.));
   string fill1(space1, ' ');
   string fill2(78  - version_len - space1, ' ');
   cout << " _____________________________________________________________  ___  __________ " << endl;
   cout << "|   _       o     _____  _       _    _  __  __  _____ __ .    /   \\       .   |" << endl;
   cout << "|  (_)   .       / ____|| |  o  | |  | ||  \\/  ||  __ \\\\ \\   /(     )          |" << endl;
   cout << "|            .  | |   . | |     | |  | || \\  / || |__) |\\ \\_/ /\\___/     o     |" << endl;
   cout << "| .             | |     | |   . | |  | || |\\/| ||  ___/  \\   /       .         |" << endl;
   cout << "|    __   o     | |____ | |____ | |__| || |  | || |     . | |     o         .  |" << endl;
   cout << "|   /  \\       . \\_____||______| \\____/ |_|  |_||_|  o    |_|    .         _   |" << endl;
   cout << "|   \\__/                                                               .  (_)  |" << endl;
   cout << "|"  << fill1 <<  clumpy_version_print  << fill2 << "|" << endl;
   cout << "|______________________________________________________________________________|\n" << endl;


   // initialize required parameter vector.
   get_required_params();


   //-------------------------------------------------//
   // Self-explanatory executable
   // first argument must always be the simulation mode
   // Print explanation and abort if not found.
   //-------------------------------------------------//
   if (argc <= 1 ||
         (argc >= 2 && ((string)argv[1]).substr(0, 2) != "-g"
          && ((string)argv[1]).substr(0, 2) != "-h"
          && ((string)argv[1]).substr(0, 2) != "-e"
          && ((string)argv[1]).substr(0, 2) != "-s"
          && ((string)argv[1]).substr(0, 2) != "-o"
          && ((string)argv[1]).substr(0, 2) != "-z"
          && ((string)argv[1]).substr(0, 2) != "-f")) {
      printf("  Incomplete or wrong command line input - mind the correct usage:\n");
      printf("     %s -g  => run on the Galactic halo only + list of halos [optional]\n", argv[0]);
      printf("     %s -h  => run on (a list of) halos (not the Gal. halo)\n", argv[0]);
      printf("     %s -e  => run on extragalactic DM\n", argv[0]); // + list of halos [optional] + Galactic halo [optional]\n", argv[0]);
      printf("     %s -s  => run on statistical-like file (or on a list of files)\n", argv[0]);
      printf("     %s -o  => export the output FITS file into a different format\n", argv[0]);
      printf("     %s -z  => gamma and neutrino spectra \n", argv[0]);
      printf("     %s -f  => append extension with fluxmaps to existing FITS file \n", argv[0]);
      printf("\n");
      return 1;
   } else {

      //-------------------------------------------------//
      // After simulation mode has been found, now determine submode
      // without use of get_opt():

      string opt = string(argv[1]);
      bool is_print_default_params = false;
      bool is_write_all_params = false;

      // Find options -p (print) or -g (graph) in arg[1]
      if (opt.find_first_of("p") != string::npos) gSIM_IS_DISPLAY = false;
      if (opt.find_first_of("d") != string::npos) {
#if IS_ROOT
         gSIM_IS_PRINT = false;
#else
         print_warning("clumpy.cc", "main()", "Display only (-d) option has no effect"
                       " when ROOT is not installed.");
#endif
      }
      if (opt.find_first_of("n") != string::npos) {
         gSIM_IS_PRINT = false;
         gSIM_IS_DISPLAY = false;
      }
      if (opt.find_first_of("D") != string::npos) {
         is_print_default_params = true;
         if (opt.find_first_of("a") != string::npos and argc == 2) {
            is_write_all_params = true;
         } else if (opt.find_first_of("a") == string::npos and argc == 3) {
            if (string(argv[2]) == "-a" or string(argv[2]) == "--all") is_write_all_params = true;
         } else if (opt.find_first_of("a") == string::npos and argc == 2) {
            // do nothing
            is_write_all_params = false;
         } else {
            print_error("clumpy.cc", "main()", "Error while reading the command line parameters. "
                        "If you want to use the option -a or --all to print/write all parameters, "
                        "or D to generate a default parameter file, "
                        "do not provide additional options in the command line.");
         }
      }


      // Search for selection
      int choice = -1;
      string choice_str = "";
      string ::size_type pos = opt.find_first_of("0123456789");
      string ::size_type pos_last = opt.find_last_of("0123456789");
      if (pos != string::npos)
         choice = atoi(opt.substr(pos, pos_last).c_str());

      if (choice != -1) choice_str = opt.substr(pos, pos_last - 1);

      string flag_mode = opt.substr(1, 1) + choice_str;
      string mode_str = "Simulation mode: ";
      vector<string> gNAMES_SIMUMODES_str;
      gNAMES_SIMUMODES_str.assign(gNAMES_SIMUMODES, gNAMES_SIMUMODES + sizeof gNAMES_SIMUMODES / sizeof gNAMES_SIMUMODES[0]);
      // find simulation mode. If not found, error is printed out below
      if (find(gNAMES_SIMUMODES_str.begin(), gNAMES_SIMUMODES_str.end(), flag_mode) != gNAMES_SIMUMODES_str.end()) {
         gSIM_FLAG_MODE = string_to_enum("FLAG_MODE", "k"  + flag_mode);
         // find corresponding standard parameters:
         gSIM_STANDARD_INPUTPARAMS = inputparameters_globalparams2string(gSIM_FLAG_MODE);
         mode_str += string(gNAMES_SIMUMODES_str[gSIM_FLAG_MODE]);

         // hack to allow hidden alpha_int in 2D skymaps:
         if (gSIM_FLAG_MODE == kg5 or gSIM_FLAG_MODE == kg6 or gSIM_FLAG_MODE == kg7 or gSIM_FLAG_MODE == kg8
               or gSIM_FLAG_MODE == kh4  or gSIM_FLAG_MODE == kh5) {
            gSIM_ALPHAINT = -1; // (alpha_int later automatically set corresponding to map resolution)
         }

      } else {
         mode_str += flag_mode[0];
      }

      // print simulation mode:
      int mode_len = mode_str.size();
      space1 = int(floor((78 - mode_len) / 2.));
      string fill3(space1, ' ');
      string fill4(78  - mode_len - space1, ' ');
      cout << " ______________________________________________________________________________ " << endl;
      cout << "|                                                                              |" << endl;
      cout << "|"  << fill3 <<  mode_str  << fill4 << "|" << endl;
      cout << "|______________________________________________________________________________|\n" << endl;

      //-------------------------------------------------//
      // The required simulation has been determined,
      // and all other options are now read by get_opt().
      // The possible gSIM_COMMANDLINE_OPTIONS are defined in params.cc

      // But before we start to read the command line parameters, we do
      // a coarse pre-check whether the user input makes sense:
      // all argv values, except filenames, should start with a dash, a number,
      // or a k (in case of keywords):
      for (int i = 2; i < argc; ++i) {
         if (!isdigit(argv[i][0]) and argv[i][0] != '-') {
            // check if we have a filename:
            if (string(argv[i - 1]) != "-i" and string(argv[i - 1]) != "-r"
                  and string(argv[i - 1]) != "--infile" and string(argv[i - 1]) != "--readfile"
                  and argv[i][0] != 'k' /*CLUMPY keywords*/) {
               print_error("clumpy.cc", "main()", "Error while reading the command line parameter '"
                           + string(argv[i]) + "'.", false);
               printf("\n      Note that: \n");
               printf("       - variable names must be prepended by a dash\n");
               printf("       - command line variable values must be numbers (or CLUMPY keywords)\n");
               printf("       - a parameter file must be announced by a leading '-i' or '--infile' \n");
               printf(COLOR_BRED "      => abort()\n\n" COLOR_RESET);
               abort();
            }
         }
      }

      string file = "-1";
      string second_file = "-1"; // second file for modes which require two input files to be loaded
      vector<string> inputparams_from_commandline(gN_INPUTPARAMS, "-999");
      vector<bool> is_filled_inputparams_from_commandline(gN_INPUTPARAMS, false);
      string shortopts = "ghesozf1234567890::";

      int option_index;

      // only when non printing/writing default parameter file:
      if (!is_print_default_params) {
         while (1) {
            int getopt_ret = getopt_long_only(argc, argv, shortopts.c_str(),
                                              gSIM_COMMANDLINE_OPTIONS,  &option_index);

            if (getopt_ret ==  -1) break;
            else if (getopt_ret == '?') {
               print_error("clumpy.cc", "main()",
                           "Something unexpected went wrong during input parameter loading with getopt().");
            }

            // Check if any of the parameters shall be read from a file
            else if (getopt_ret == gN_INPUTPARAMS + 1) {
               file = optarg;
               if (optarg[0] == '-') getopt_ret = '?';
            }
            // Check if there is a second (FITS) file to read
            else if (getopt_ret == gN_INPUTPARAMS + 2) {
               second_file = optarg;
               if (optarg[0] == '-') getopt_ret = '?';
            }

            // Find again options -p (print) or -g (graph) via getopt()
            // in case they are given separately (lead by a dash).
            else if (getopt_ret == gN_INPUTPARAMS + 3) gSIM_IS_DISPLAY = false;
            else  if (getopt_ret == gN_INPUTPARAMS + 4) gSIM_IS_PRINT = false;
            else  if (getopt_ret == gN_INPUTPARAMS + 5) {
               gSIM_IS_PRINT = false;
               gSIM_IS_DISPLAY = false;
            }

            // Check for "default option" -D via getopt()
            // in case it is given separately (lead by a dash).
            else if (getopt_ret == gN_INPUTPARAMS + 6) is_print_default_params = true;

            // Check for write_all_params flag
            // in case it is given separately (lead by a dash).
            else if (getopt_ret == gN_INPUTPARAMS + 7) is_write_all_params = true;

            // Now read all parameters from command line
            for (int i = 0; i <= gN_INPUTPARAMS; ++i) {
               //inputparams_from_commandline[i] = "-999";
               if (getopt_ret == i and optarg != NULL) {
                  int i_tweak;
                  if (i != gN_INPUTPARAMS) i_tweak = i;
                  else i_tweak = 63; // something is strange with index 63 - check for different systems!
                  if (is_filled_inputparams_from_commandline[i_tweak]) {
                     print_error("clumpy.cc", "main()",
                                 "Error while reading the command line parameter '"
                                 + string(gSIM_INPUTPARAMS[i_tweak][kSIM_INPUTPARAM_VARNAME])
                                 + "'. Parameter is specified twice in the command line.");

                  }
                  is_filled_inputparams_from_commandline[i_tweak] = true;
                  inputparams_from_commandline[i_tweak] = optarg;
               }
            }
         }
      }

      string standard_paramfile = "clumpy_params_" + string(gNAMES_SIMUMODES[gSIM_FLAG_MODE]) + ".txt";
      if (is_write_all_params) standard_paramfile = "clumpy_params_all.txt";
      if (is_print_default_params && is_write_all_params) gSIM_FLAG_MODE = -1;

//      for (int i = 0; i < gN_INPUTPARAMS; ++i) {
//         cout << i << " " <<gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME] << " "<< inputparams_from_commandline[i] << endl;
//      }
//      abort();

      // Vector of r (or alpha, or theta) denoted x (lin or log scale)
      vector <double> x;

      if (opt.substr(0, 2) == "-g") {

         //###############################//
         //# Galactic-halo related calls #//
         //###############################//

         // Usage of -1, -2, -3...
         if (choice < 0 || choice > 8 || (is_print_default_params == false && argc < 3)) {
            printf("  Incomplete or wrong command line input - mind the correct usage:\n");
            printf("   [M(r)                    ]  %s -g0 -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [rho_sm+<sub>(r)         ]  %s -g1 -i param_file -any_parameter (or --any_parameter)\n", argv[0]); //rmin[kpc] rmax[kpc] n is_log
            printf("   [Jsm+<sub>(alpha_int)    ]  %s -g2 -i param_file -any_parameter (or --any_parameter)\n", argv[0]); //amin[deg] amax[deg] n is_log  psi_obs[deg] theta_obs[deg]
            printf("   [Jsm+<sub>(theta)        ]  %s -g3 -i param_file -any_parameter (or --any_parameter)\n", argv[0]); //tmin[deg] tmax[deg] n is_log
            printf("   [Jsm+<sub>+list(theta)   ]  %s -g4 -i param_file -any_parameter (or --any_parameter)\n", argv[0]); //tmin[deg] tmax[deg] n is_log sort_contrast_thresh phi_cut[deg]
            printf("   [2D-skymap Jsm+<sub>     ]  %s -g5 -i param_file -any_parameter (or --any_parameter)\n", argv[0]); //psi_obs[deg] theta_obs[deg] theta_orth_size[deg] theta_size[deg]
            printf("   [2D-skymap Jsm+<sub>+list]  %s -g6 -i param_file -any_parameter (or --any_parameter)\n", argv[0]); //psi_obs[deg] theta_obs[deg] theta_orth_size[deg] theta_size[deg] is_subs_list
            printf("   [2D-skymap Jsm+sub       ]  %s -g7 -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [2D-skymap Jsm+sub+list  ]  %s -g8 -i param_file -any_parameter (or --any_parameter)\n", argv[0]); //psi_obs[deg] theta_obs[deg] theta_orth_size[deg] theta_size[deg] user_rse[%%] is_subs_list
            printf("\n");
            printf(" N.B.: To print default parameters and write them to a file, use -D (or \"-g1D\", \"-g2D\", etc.)\n");
            printf("       Append flag --all or -a to print/write ALL possible parameters readable by CLUMPY\n");
            printf(" N.B.: Default output is both displays on screen and printing to files.\n");
            printf("       => use option -d for displays only (e.g., -g1 -d)\n");
            printf("       => use option -p for print only (e.g., -g1 -p)\n");
            printf(" N.B.: The order of options does not matter, except for the very first option, \n");
            printf("       which must be the simulation module (in this case -gX).\n\n");
            printf("\n\n");
            return 1;
         }

         // Print example values
         if (is_print_default_params) {
            if (choice == 0)      printf("   M(r):  %s -g0 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 1) printf("   rho_sm+<sub>(r):  %s -g1 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 2) printf("   Jsm+<sub>(alpha_int):  %s -g2 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 3) printf("   Jsm+<sub>(theta):  %s -g3  -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 4) printf("   Jsm+<sub>+list(theta):  %s -g4 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 5) printf("   2D-skymap Jsm+<sub>:  %s -g5 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 6) printf("   2D-skymap Jsm+<sub>+list:  %s -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 7) printf("   2D-skymap Jsm+<sub>+sub:  %s -g7 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 8) printf("   2D-skymap Jsm+<sub>+sub+list:  %s -g8 -i %s\n", argv[0], standard_paramfile.c_str());
            inputparameters_write_paramfile(standard_paramfile, gSIM_FLAG_MODE, false);
            printf("\n");
            return 1;
         }

         // now we can load the parameters
         if (!is_print_default_params) load_parameters(file, inputparams_from_commandline, gSIM_FLAG_MODE);

         // Switch selection
         double theta_obs;
         switch (choice) {
            case 0:
               // M(r) for Gal. halo
               x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
               gal_j1D(x, 3);
               printf("\n   [M(r)]  %s -g0 -i param_file\n", argv[0]);
               break;
            case 1:
               // rho(r) for Gal. halo
               x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
               gal_j1D(x, 0);
               printf("\n   [rho_sm+<sub>(r)]  %s -g1 -i param_file\n", argv[0]);
               break;
            case 2:
               // Jsm(alpha_int) + <Jsub(alpha_int)> for Gal. halo
               x =  make_1D_grid(gSIM_ALPHAINT_MIN, gSIM_ALPHAINT_MAX, gSIM_NX, gSIM_IS_XLOG);
               theta_obs = atof(gSIM_THETA_OBS_DEG.c_str()) * DEG_to_RAD;
               gal_j1D(x, 1, false, gSIM_PSI_OBS, theta_obs);
               printf("\n   [Jsm+<sub>(alpha_int)]  %s -g2 -i param_file\n", argv[0]);
               break;
            case 3:
               // Jsm(theta_obs) + <Jsub(theta_obs)> for Gal. halo
               x =  make_1D_grid(gSIM_THETA_MIN, gSIM_THETA_MAX, gSIM_NX, gSIM_IS_XLOG);
               gal_j1D(x, 2);
               printf("\n   [Jsm+<sub>(theta_obs)]  %s -g3 -i param_file\n", argv[0]);
               break;
            case 4:
               // Jsm(theta_obs) + <Jsub(theta_obs)> + Jlist
               x =  make_1D_grid(gSIM_THETA_MIN, gSIM_THETA_MAX, gSIM_NX, gSIM_IS_XLOG);
               gal_j1D(x, 2, true, 0., 0., gSIM_SORT_CONTRAST_THRESH, gSIM_PHI_CUT);
               printf("\n   [Jsm+<sub>+list(theta_obs)]  %s -g4 -i param_file\n", argv[0]);
               break;
            case 5:
               // 2D-map Jsm+<sub> (Gal. halo)
               gal_j2D(gSIM_PSI_OBS, gSIM_THETA_OBS_DEG, gSIM_THETA_ORTH_SIZE_DEG, gSIM_THETA_SIZE, 0);
               printf("   [2D-skymap Jsm+<sub>]  %s -g5 -i param_file\n", argv[0]);
               break;
            case 6:
               // 2D-map Jsm+sub (Gal. halo) + Jlist
               gal_j2D(gSIM_PSI_OBS, gSIM_THETA_OBS_DEG, gSIM_THETA_ORTH_SIZE_DEG, gSIM_THETA_SIZE, 1, 0.);
               printf("   [2D-skymap Jsm+<sub>+list]  %s -g6 -i param_file\n", argv[0]);
               break;
            case 7:
               // 2D-map Jsm+sub (Gal. halo)
               gal_j2D(gSIM_PSI_OBS, gSIM_THETA_OBS_DEG, gSIM_THETA_ORTH_SIZE_DEG, gSIM_THETA_SIZE, 2, gSIM_USER_RSE);
               printf("   [2D-skymap Jsm+sub]  %s -g7 -i param_file \n", argv[0]);
               break;
            case 8:
               // 2D-map Jsm+sub (Gal. halo) + Jlist
               gal_j2D(gSIM_PSI_OBS, gSIM_THETA_OBS_DEG, gSIM_THETA_ORTH_SIZE_DEG, gSIM_THETA_SIZE, 3, gSIM_USER_RSE);
               printf("   [2D-skymap Jsm+sub+list]  %s -g8  -i param_file\n", argv[0]);
               break;
            default:
               break;
         }

      } else if (opt.substr(0, 2) == "-h") {

         //#################################//
         //# External-halos related calls #//
         //#################################//

         // Usage of -h1, -h2, -h3...
         if (choice < 0 || choice > 10 || (is_print_default_params == false && argc < 3)) {
            printf("  Incomplete or wrong command line input - mind the correct usage:\n");
            printf("   [M(r)                     ]  %s -h0  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [rho_sm+<sub>(r)          ]  %s -h1  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [Jsm+<sub>(alpha_int)     ]  %s -h2  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [Jsm+<sub>(theta)         ]  %s -h3  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [2D-skymap Jsm+<sub>(halo)]  %s -h4  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [2D-skymap Jsm+sub(halo)  ]  %s -h5  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [alpha_int(f*Jtot)        ]  %s -h6  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [dist(f*Jpointlike)       ]  %s -h7  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [sigma_p(R)               ]  %s -h8  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [I(R)                     ]  %s -h9  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [beta_ani(r)              ]  %s -h10 -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("\n");
            printf(" N.B.: To print default parameters and write them to a file, use -D (or \"-h1D\", \"-h2D\", etc.)\n");
            printf("       Append flag --all or -a to print/write ALL possible parameters readable by CLUMPY\n");
            printf(" N.B.: Default output is both displays on screen and printing to files.\n");
            printf("       => use option -d for displays only (e.g., -h1 -d)\n");
            printf("       => use option -p for print only (e.g., -h1 -p)\n");
            printf(" N.B.: The order of options does not matter, except for the very first option, \n");
            printf("       which must be the simulation module (in this case -hX).\n\n");
            printf("\n\n");
            return 1;
         }

         // Print example values
         if (is_print_default_params) {
            if (choice == 0)      printf("   M(r):  %s -h0 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 1) printf("   rho_sm+<sub>(r):  %s -h1 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 2) printf("   Jsm+<sub>(alpha_int):  %s -h2 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 3) printf("   Jsm+<sub>(theta):  %s -h3 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 4) printf("   2D-skymap Jsm+<sub>(halo):  %s -h4 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 5) printf("   2D-skymap Jsm+sub(halo): %s -h5 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 6) printf("   alpha_int to get f*Jtot:  %s -h6 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 7) printf("   d for which J=frac*Jpointlike:  %s -h7 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 8) printf("   sigma_p(R):  %s -h8 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 9) printf("   I(R):        %s -h9 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 10)printf("   beta_ani(r): %s -h10 -i %s\n", argv[0], standard_paramfile.c_str());
            inputparameters_write_paramfile(standard_paramfile, gSIM_FLAG_MODE, false);
            printf("\n");
            return 1;
         }

         // now we can load the parameters
         if (!is_print_default_params) load_parameters(file, inputparams_from_commandline, gSIM_FLAG_MODE);

         string name;

         switch (choice) {
            case 0:
               // M(r) for a list of halos
               x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
               halo_j1D(x, 3);
               printf("\n   [M(r)]  %s -h0 -i param_file\n", argv[0]);
               break;
            case 1:
               // rho_sm(r) + dPdV_Jsub(r)> for a list of halos
               x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
               halo_j1D(x, 0);
               printf("\n   [rho_sm+<sub>(r)]  %s -h1 -i param_file\n", argv[0]);
               break;
            case 2:
               // Jsm(alpha_int) + <Jsub(alpha_int)> (and boost) for a list of halos
               x =  make_1D_grid(gSIM_ALPHAINT_MIN, gSIM_ALPHAINT_MAX, gSIM_NX, gSIM_IS_XLOG);
               halo_j1D(x, 1);
               printf("\n   [Jsm+<sub>(alpha_int)]  %s -h2 -i param_file\n", argv[0]);
               break;
            case 3:
               // Jsm(theta) + <Jsub(theta)> for a list of halos
               x =  make_1D_grid(gSIM_THETA_MIN, gSIM_THETA_MAX, gSIM_NX, gSIM_IS_XLOG);
               halo_j1D(x, 2);
               printf("\n   [Jsm+<sub>(theta)]  %s -h3 -i param_file\n", argv[0]);
               break;
            case 4:
               // 2D-skymap Jsm+<sub> (for a single halo)
               halo_j2D(gLIST_HALONAME, gSIM_THETA_SIZE, false);
               printf("\n   [2D-skymap Jsm+<sub>(halo)]  %s -h4 -i param_file\n", argv[0]);
               break;
            case 5:
               // 2D-skymap Jsm+sub (for a single halo)
               halo_j2D(gLIST_HALONAME, gSIM_THETA_SIZE, true, gSIM_USER_RSE);
               printf("\n   [2D-skymap Jsm+sub(halo)]  %s -h5 -i param_file\n", argv[0]);
               break;
            case 6:
               // Containment angle [such as J(angle)= f*Jtot]
               halo_fracjtot_alphaint(gLIST_HALOES, gSIM_JFRACTION, gPP_DM_IS_ANNIHIL_OR_DECAY, gDM_RHOSAT);
               printf("\n   [alpha_int(f*Jtot)]  %s -h6 -i param_file\n", argv[0]);
               break;
            case 7:
               // Distance for which point-hike approx holds [such as J(d)= f*Jpointlike = f*L/d^2]
               halo_fracjpointlike_dist(gLIST_HALOES, gSIM_ALPHAINT, gSIM_JFRACTION, gPP_DM_IS_ANNIHIL_OR_DECAY, gDM_RHOSAT);
               printf("\n   [dist(f*Jpointlike)]   %s -h7 -i param_file\n", argv[0]);
               break;
            case 8:
               // sigma_p for a list of halos
               x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
               halo_jeans(x, 0);
               printf("\n   [sigma_p(R)]  %s -h8 -i param_file\n", argv[0]);
               break;
            case 9:
               // sigma_p for a list of halos
               x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
               halo_jeans(x, 1);
               printf("\n   [I(R)]  %s -h9 -i param_file\n", argv[0]);
               break;
            case 10:
               // sigma_p for a list of halos
               x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
               halo_jeans(x, 2);
               printf("\n   [beta_ani(r)]  %s -h10 -i param_file\n", argv[0]);
               break;
            default:
               break;
         }
         return 1;

      } else if (opt.substr(0, 2) == "-e") {

         //#################################//
         //# Extragalactic related calls #//
         //#################################//

         // Usage of -e1, -e2, -e3...
         if (choice < 0 || choice > 6 || (is_print_default_params == false && argc < 3)) {
            printf("  Incomplete or wrong command line input - mind the correct usage:\n");
            printf("   [Distances & Omega's      ]  %s -e0 -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [Mass function (M,z)      ]  %s -e1 -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [cdelta-mdelta, Lum, Boost]  %s -e2 -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [Intensity multiplier     ]  %s -e3 -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [EBL absorption tau       ]  %s -e4 -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [Mean intens. dI/dE(M,z;E)]  %s -e5 -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [Mean intensity dI/dE(E)  ]  %s -e6 -i param_file -any_parameter (or --any_parameter)\n", argv[0]);

            printf("\n");
            printf(" N.B.: To print default parameters and write them to a file, use -D (or \"-e1D\", \"-e2D\", etc.)\n");
            printf("       Append flag --all or -a to print/write ALL possible parameters readable by CLUMPY\n");
            printf(" N.B.: Default output is both displays on screen and printing to files.\n");
            printf("       => use option -d for displays only (e.g., -e1 -d)\n");
            printf("       => use option -p for print only (e.g., -e1 -p)\n");
            printf(" N.B.: The order of options does not matter, except for the very first option, \n");
            printf("       which must be the simulation module (in this case -eX).\n\n");
            printf("\n\n");
            return 1;
         }

         // Print example values
         if (is_print_default_params) {
            if (choice == 0)      printf("   Distances & Omega's:  %s -e0 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 1) printf("   Mass function (M,z):  %s -e1 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 2) printf("   cdelta-mdelta and Lum:  %s -e2 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 3) printf("   Intensity multiplier:  %s -e3 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 4) printf("   EBL absorption tau:  %s -e4 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 5) printf("   Average intensity(M,z; E):  %s -e5 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 6) printf("   Average intensity(E; M,z):  %s -e6 -i %s\n", argv[0], standard_paramfile.c_str());
            inputparameters_write_paramfile(standard_paramfile, gSIM_FLAG_MODE, false);
            printf("\n");
            return 1;
         }

         // now we can load the parameters
         if (!is_print_default_params) load_parameters(file, inputparams_from_commandline, gSIM_FLAG_MODE);

         // Switch selection
         string name;

         switch (choice) {
            case 0: {
                  // Calculate cosmic distances and Omega parameters.
                  x =  make_1D_grid(gSIM_EXTRAGAL_ZMIN, gSIM_EXTRAGAL_ZMAX, gSIM_EXTRAGAL_NZ, gSIM_EXTRAGAL_IS_ZLOG);
                  analyse_distances(x);
                  printf("\n   [Distances & Omega's]  %s -e0 -i param_file\n", argv[0]);
                  break;
               }
            case 1: {
                  // Calculate halo mass function at various redshifts
                  vector<double> M_vec =  make_1D_grid(gSIM_EXTRAGAL_MMIN, gSIM_EXTRAGAL_MMAX, gSIM_EXTRAGAL_NM, gSIM_EXTRAGAL_IS_MLOG);
                  vector<double> z_vec =  make_1D_grid(gSIM_EXTRAGAL_ZMIN, gSIM_EXTRAGAL_ZMAX, gSIM_EXTRAGAL_NZ, gSIM_EXTRAGAL_IS_ZLOG);
                  analyse_massfunction(z_vec, M_vec, gEXTRAGAL_FLAG_MASSFUNCTION);
                  printf("\n   [Mass function (M,z)]  %s -e1 -i param_file\n", argv[0]);
                  break;
               }
            case 2: {
                  // cdelta-mdelta and Lum
                  plot_c_lum_boost_vs_m(gSIM_EXTRAGAL_MMIN, gSIM_EXTRAGAL_MMAX, gSIM_NX, gEXTRAGAL_FLAG_CDELTAMDELTA_LIST,
                                        gDM_SUBS_MMIN, gDM_SUBS_MMAXFRAC, gHALO_SUBS_MASSFRACTION[kEXTRAGAL], gEXTRAGAL_SUBS_DPDM_SLOPE_LIST,
                                        gDM_SUBS_NUMBEROFLEVELS, gDM_LOGCDELTA_STDDEV);
                  printf("   [cdelta-mdelta, Lum, Boost]  %s -e2 -i param_file\n", argv[0]);
                  break;
               }
            case 3: {
                  // Plot intensity multiplier
                  vector<double> M_vec =  make_1D_grid(gSIM_EXTRAGAL_MMIN, gSIM_EXTRAGAL_MMAX, gSIM_EXTRAGAL_NM, gSIM_EXTRAGAL_IS_MLOG);
                  vector<double> z_vec =  make_1D_grid(gSIM_EXTRAGAL_ZMIN, gSIM_EXTRAGAL_ZMAX, gSIM_EXTRAGAL_NZ, gSIM_EXTRAGAL_IS_ZLOG);
                  analyse_intensitymultiplier(z_vec, M_vec);
                  printf("\n   [Intensity multiplier(M,z)]  %s -e3 -i param_file\n", argv[0]);
                  break;
               }
            case 4: {
                  // Plot EBL absorption
                  vector<double> e_vec =  make_1D_grid(gSIM_FLUX_EMIN_GEV, gSIM_FLUX_EMAX_GEV, gSIM_NX, 1);
                  vector<double> z_vec =  make_1D_grid(gSIM_EXTRAGAL_ZMIN, gSIM_EXTRAGAL_ZMAX, gSIM_EXTRAGAL_NZ, gSIM_EXTRAGAL_IS_ZLOG);
                  analyse_ebl(e_vec, z_vec, gEXTRAGAL_FLAG_ABSORPTIONPROFILE);
                  printf("\n   [EBL absorption tau]  %s -e4 -i param_file\n", argv[0]);
                  break;
               }
            case 5: {
                  // Calculate differential flux at fixed energy for various redshift/ mass decades
                  vector<double> M_vec =  make_1D_grid(gSIM_EXTRAGAL_MMIN, gSIM_EXTRAGAL_MMAX, gSIM_EXTRAGAL_NM, gSIM_EXTRAGAL_IS_MLOG);
                  vector<double> z_vec =  make_1D_grid(gSIM_EXTRAGAL_ZMIN, gSIM_EXTRAGAL_ZMAX, gSIM_EXTRAGAL_NZ, gSIM_EXTRAGAL_IS_ZLOG);
                  analyse_intensity_mass_z_decades(z_vec, M_vec, gSIM_FLUX_AT_E_GEV);
                  printf("\n   [Mean intens. dI/dE(M,z;E)]  %s -e5 -i param_file\n", argv[0]);
                  break;
               }
            case 6: {
                  // Calculate differential flux for fixed redshift/ mass decades
                  analyse_intensity_mean(gSIM_FLUX_EMIN_GEV, gSIM_FLUX_EMAX_GEV, gSIM_NX);
                  printf("\n   [Mean intensity dI/dE(E)  ]  %s -e6 -i param_file\n", argv[0]);
                  break;
               }
            default:
               break;
         }
         return 1;

      } else if (opt.substr(0, 2) == "-s") {

         //########################################//
         //# Stat. ana. single halo related calls #//
         //########################################//

         // Usage of -s1, -s2, -s3...
         if (choice < 0 || choice > 12 || (is_print_default_params == false && argc < 3)) {
            printf("  Incomplete or wrong command line input - mind the correct usage:\n");
            printf("   [CLs M(r)        ]  %s -s0  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [PDF logL_or_chi2]  %s -s1  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [PDF correlations]  %s -s2  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [Best fit params ]  %s -s3  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [CLs params      ]  %s -s4  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [CLs rho(r)      ]  %s -s5  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [CLs J(alphaint) ]  %s -s6  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [CLs J(theta)    ]  %s -s7  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [CLs sigma_p(R)  ]  %s -s8  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [CLs vr2(r)      ]  %s -s9  -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [CLs beta_ani(r) ]  %s -s10 -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [CLs nu(r)       ]  %s -s11 -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("   [CLs I(R)        ]  %s -s12 -i param_file -any_parameter (or --any_parameter)\n", argv[0]);
            printf("\n");
            printf(" N.B.: To print default parameters and write them to a file, use -D (or \"-s1D\", \"-s2D\", etc.)\n");
            printf("       Append flag --all or -a to print/write ALL possible parameters readable by CLUMPY\n");
            /*
                        printf("    - stat_files: either a single file or a list of files\n");
                        printf("    - For -s2\n");
                        printf("        - rkpc_to_plot_mr: if not 0., radius [kpc] where to draw the mass distribution\n");
                        printf("        - enter stat_files, r_for_mr, and rho_sat[Msol/kpc3] to display the list of possible IDs\n");
                        printf("    - For -s8\n");
                        printf("        - if data files contain Sigmap2 data, draw sigma_p^2 instead of sigma_p\n");
                        printf("    - For -s0 and -s5 to -s12\n");
                        printf("        - x-axis is in log-scale (so xmin cannot be 0) unless hidden variable gSIM_IS_XLOG=0 forced.\n");
                        printf("        - switch_stat: 0->for PDF CL method (recommended), 1->for chi2 CL method, 2->for both methods, 3->mean and dispersion (e.g., for boostrap)\n");
                        printf("        - is_annihil: 1->for annihilations, 0->for decay\n");
            */
            printf(" N.B.: The order of options does not matter, except for the very first option, \n");
            printf("       which must be the simulation module (in this case -sX).\n\n");
            printf("\n");
            return 1;
         }

         // Print example values
         if (is_print_default_params) {
            if (choice == 0)      printf("   CLs M(r):  %s -s0 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 1) printf("   PDF logL_or_chi2:  %s -s1 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 2) printf("   PDF correlations:  %s -s2 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 3) printf("   Best fit params:  %s -s3 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 4) printf("   CLs params:  %s -s4 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 5) printf("   CLs rho(r):  %s -s5 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 6) printf("   CLs J(alphaint):  %s -s6 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 7) printf("   CLs J(theta):  %s -s7 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 8) printf("   CLs sigma_p(R):  %s -s8 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 9) printf("   CLs vr2(r):  %s -s9 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 10) printf("   CLs beta_ani(r):  %s -s10 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 11) printf("   CLs nu(r):  %s -s11 -i %s\n", argv[0], standard_paramfile.c_str());
            else if (choice == 12) printf("   CLs I(R):  %s -s12 -i %s\n", argv[0], standard_paramfile.c_str());
            inputparameters_write_paramfile(standard_paramfile, gSIM_FLAG_MODE, false);
            printf("\n");
            return 1;
         }

         // now we can load the parameters
         if (!is_print_default_params) load_parameters(file, inputparams_from_commandline, gSIM_FLAG_MODE);


         // Convert comma-separated list of CLs into vector<double> of CLs
         vector<double> cls;
         if ((choice >= 5 && choice <= 12) || choice == 0) {
            vector<string> tmp_x_cl;
            string2list(gSTAT_CL_LIST, ",", tmp_x_cl);
            for (int ii = 0; ii < (int)tmp_x_cl.size(); ++ii)
               cls.push_back(atof(tmp_x_cl[ii].c_str()));
         }


         switch (choice) {
            case 0: {
                  // M(r) CLs
                  x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
                  stat_CLs(x, gSTAT_FILES, cls, 8, gDM_RHOSAT, gSIM_EPS, 0., gSTAT_MODE);
                  printf("   [CLs rho(r)]  %s -s0(rmin[kpc] rmax[kpc] n CL1,CL2,...CLi switch_stat rho_sat[Msol/kpc3] eps)\n", argv[0]);
                  break;
               }
            case 1: {
                  // Chi2
                  stat_draw_chi2(gSTAT_FILES, gSTAT_IS_LOGL_OR_CHI2);
                  printf("   [PDF logL_or_chi2]  %s -s1(stat_files  is_logl_or_chi2)\n", argv[0]);
                  break;
               }
            case 2: {
                  // Correlations
                  vector<int> ids;
                  vector<string> tmp_x_id;
                  string2list(gSTAT_ID_LIST, ",", tmp_x_id);
                  for (int ii = 0; ii < (int)tmp_x_id.size(); ++ii)
                     ids.push_back(atof(tmp_x_id[ii].c_str()));
                  stat_draw_correlations(ids, gSTAT_FILES, gSTAT_RKPC_FOR_MR, gDM_RHOSAT);
                  printf("   [PDF correlations]  %s -s2(stat_file  rkpcnotzero_to_plot_Mr  rho_sat[Msol/kpc3] ID1,ID2,...IDn)\n", argv[0]);
                  break;
               }
            case 3: {
                  // Retrieve best-fit model parameters
                  if (gSTAT_MODE < 0 || gSTAT_MODE > 3) {
                     print_error("clumpy.cc", "main()", "Wrong stat method chosen");
                  }
                  stat_find_bestmodel(gSTAT_FILES, gSTAT_MODE);
                  printf("   [Best fit params]  %s -s3(stat_files switch_stat)\n", argv[0]);
                  break;
               }
            case 4: {
                  // Retrieve model with min and max val for all
                  // models passing the x% cl constraint
                  stat_find_CLs(gSTAT_FILES, gSTAT_CL, gSTAT_MODE);
                  printf("   [CLs params]  %s -s4(stat_files x_cl switch_stat)\n", argv[0]);
                  break;
               }
            case 5: {
                  // rho CLs
                  x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
                  stat_CLs(x, gSTAT_FILES, cls, 0, gDM_RHOSAT, 0., 0., gSTAT_MODE);
                  printf("   [CLs rho(r)]  %s -s5(stat_files rmin[kpc] rmax[kpc] n CL1,CL2,...CLi switch_stat rho_sat[Msol/kpc3] is_astro_unit)\n", argv[0]);
                  break;
               }
            case 6: {
                  // J(alpha_int) CLs
                  double alphaint_min_deg = gSIM_ALPHAINT_MIN * RAD_to_DEG;
                  double alphaint_max_deg = gSIM_ALPHAINT_MAX * RAD_to_DEG;
                  x =  make_1D_grid(alphaint_min_deg, alphaint_max_deg, gSIM_NX, gSIM_IS_XLOG);
                  stat_CLs(x, gSTAT_FILES, cls, 1, gDM_RHOSAT, gSIM_EPS, 0., gSTAT_MODE);
                  printf("   [CLs J,D(alphaint)]  %s -s6(stat_files amin[deg] amax[deg] n CL1,CL2,...CLi switch_stat rho_sat[Msol/kpc3] eps is_annihil is_astro_unit)\n", argv[0]);
                  break;
               }
            case 7: {
                  // J(theta) CLs
                  double theta_min_deg = gSIM_THETA_MIN * RAD_to_DEG;
                  double theta_max_deg = gSIM_THETA_MAX * RAD_to_DEG;
                  x =  make_1D_grid(theta_min_deg, theta_max_deg, gSIM_NX, gSIM_IS_XLOG);
                  stat_CLs(x, gSTAT_FILES, cls, 2, gDM_RHOSAT, gSIM_EPS, gSIM_ALPHAINT, gSTAT_MODE);
                  printf("   [CLs J,D(theta)]  %s -s7(stat_files tmin[deg] tmax[deg] n CL1,CL2,...CLi switch_stat rho_sat[Msol/kpc3] eps is_annihil alpha_int[deg] is_astro_unit)\n", argv[0]);
                  break;
               }
            case 8: {
                  // sig_p2(R) CLs
                  x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
                  if (gSTAT_DATAFILES == "-1") {
                     stat_CLs(x, gSTAT_FILES, cls, 3, gDM_RHOSAT, gSIM_EPS, 0, gSTAT_MODE);
                     printf("   [CLs #sigma_{p}(R)]  %s -s8(stat_files Rmin[kpc] Rmax[kpc] n CL1,CL2,...CLi switch_stat rho_sat[Msol/kpc3] eps)\n", argv[0]);
                  } else  {
                     stat_CLs(x, gSTAT_FILES, cls, 3, gDM_RHOSAT, gSIM_EPS, 0, gSTAT_MODE, gSTAT_DATAFILES);
                     printf("   [CLs #sigma_{p}(R)]  %s -s8(stat_files Rmin[kpc] Rmax[kpc] n CL1,CL2,...CLi switch_stat rho_sat[Msol/kpc3] eps data_files)\n", argv[0]);
                  }
                  break;
               }
            case 9: {
                  // vr2(r) CLs
                  x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
                  stat_CLs(x, gSTAT_FILES, cls, 4, gDM_RHOSAT, gSIM_EPS, 0, gSTAT_MODE);
                  printf("   [CLs v_{r}^{2}(r)]  %s -s9(stat_files rmin[kpc] rmax[kpc] n CL1,CL2,...CLi switch_stat rho_sat[Msol/kpc3] eps)\n", argv[0]);
                  break;
               }
            case 10: {
                  // beta_ani(r) CLs
                  x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
                  stat_CLs(x, gSTAT_FILES, cls, 5, gDM_RHOSAT, 0., 0., gSTAT_MODE);
                  printf("   [CLs #beta_{ani}(r)]  %s -s10(stat_files rmin[kpc] rmax[kpc] n CL1,CL2,...CLi[-] switch_stat)\n", argv[0]);
                  break;
               }
            case 11: {
                  // nu(r) CLs
                  x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
                  stat_CLs(x, gSTAT_FILES, cls, 6, gDM_RHOSAT, gSIM_EPS, 0., gSTAT_MODE);
                  printf("   [CLs #nu(r)]  %s -s11(stat_files rmin[kpc] rmax[kpc] n CL1,CL2,...CLi[-] switch_stat eps)\n", argv[0]);
                  break;
               }
            case 12: {
                  // I(R) CLs
                  x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
                  if (gSTAT_DATAFILES == "-1") {
                     stat_CLs(x, gSTAT_FILES, cls, 7, gDM_RHOSAT, gSIM_EPS, 0., gSTAT_MODE);
                     printf("   [CLs I(R)]  %s -s12(stat_files rmin[kpc] rmax[kpc] n CL1,CL2,...CLi[-] switch_stat eps)\n", argv[0]);
                  } else {
                     stat_CLs(x, gSTAT_FILES, cls, 7, gDM_RHOSAT, gSIM_EPS, 0., gSTAT_MODE, gSTAT_DATAFILES);
                     printf("   [CLs I(R)]  %s -s12(stat_files rmin[kpc] rmax[kpc] n CL1,CL2,...CLi[-] switch_stat eps data_files)\n", argv[0]);
                  }
                  break;
               }
            default:
               break;

         }
         return 1;

         //#####################################//
         //# Output manipulation related calls #//
         //#####################################//

      } else if (opt.substr(0, 2) == "-o") {

         // Print example values
         if (is_print_default_params) {
            if (choice == 1)
               printf("   write extension 1 from Clumpy output file results.fits into ASCII table:  %s -o1 -i results.fits 1 \n", argv[0]);
            else if (choice == 2)
               printf("   write column 2 from extension 1 in Clumpy output file results.fits to whole-sky Healpix map (may become a large file!):  %s -o2 -i results.fits 1 2\n", argv[0]);
            printf("\n");
            return 1;
         }

         // Usage of -o1, -o2, -o3: print file info, if file is given.
         if ((choice == 1 && argc == 4)
               || (choice == 2 && argc == 4)
               || (choice == 3 && argc == 4)
            ) {
            if (string(argv[2]).substr(0, 2) == "-i") {
               printf("  Usage:\n");
               printf("   [ASCII export of FITS extension]  %s -o1 -i results.fits [extension]\n", argv[0]);
               printf("   [cutsky map to whole-sky FITS map]  %s -o2 -i results.fits [extension] [map]\n", argv[0]);
               printf("   [get input params from FITS extension]  %s -o3 -i results.fits [extension]\n", argv[0]);
               printf("\n");
               string file_to_convert = file;
               fits_print_info(file_to_convert) ;
               printf("  N.B.: for the -o2 option, the size of the whole-sky FITS files\n");
               printf("        will be about ~ GB (with lots of BLIND values), for NSIDE ~ 8000!\n");
               printf("\n");
               return 1;
            }
         }

         // Usage of -o1, -o2
         if (choice < 1 || (choice == 1 && argc != 5)
               || (choice == 2 && argc != 6)
               || (choice == 3 && argc != 5) || (choice > 3)
            ) {
            printf("  Incomplete or wrong command line input - mind the correct usage:\n");
            printf("   [ASCII export of FITS extension]  %s -o1 -i results.fits [extension]\n", argv[0]);
            printf("   [cutsky map to whole-sky FITS map]  %s -o2 -i results.fits [extension] [map]\n", argv[0]);
            printf("   [get input params from FITS extension]  %s -o3 -i results.fits [extension]\n", argv[0]);
            printf("\n");
            printf("  Typing  %s -oN -i results.fits \n", argv[0]);
            printf("  without further arguments will print you information about the file content.\n");
            printf("  N.B.: for the -o2 option, the size of the whole-sky FITS files\n");
            printf("        will be about ~ GB (with lots of BLIND values), for NSIDE ~ 8000!\n");
            printf("\n");
            return 1;
         }

         // Switch selection
         string file_to_convert;
         int i_field ;
         int i_extension ;
         switch (choice) {
            case 1:
               // ASCII output:
               file_to_convert = file;
               i_extension = atoi(argv[4]) ;
               if (i_extension == 0) {
                  print_error("clumpy.cc", "main()", "Extension numbering starts with 1");
               }
               fits_to_ascii(file_to_convert, i_extension) ;
               printf("   [ASCII export of FITS extension]  %s -o1 %s\n", argv[0], argv[2]);
               break;
            case 2:
               // Healpix whole sky maps:
               file_to_convert = file;
               i_extension = atoi(argv[4]) ;
               i_field = atoi(argv[5]) ;
               if (i_extension == 0) {
                  print_error("clumpy.cc", "main()", "Extension numbering starts with 1");
               }
               fits_to_simplefits(file_to_convert, i_extension, i_field) ;
               printf("   [cutsky map to whole-sky FITS map]  %s -o2 results.fits %s %d %d\n", argv[0], file_to_convert.c_str(), i_extension, i_field);
               break;
            case 3:
               // Extract input parameters from FITS file:
               file_to_convert = file;
               i_extension = atoi(argv[4]) ;
               if (i_extension == 0) {
                  print_error("clumpy.cc", "main()", "Extension numbering starts with 1");
               }
               fits_params2reproduction(file_to_convert, i_extension) ;
               printf("\n   [get input params from FITS extension]  %s -o3 %s\n", argv[0], argv[2]);
               break;
            default:
               break;
         }
         return 1;

         //##############################//
         //# Gamma and Neutrino spectra #//
         //##############################//
      } else if (opt.substr(0, 2) == "-z") {

         // Print example values
         if (is_print_default_params) {
            printf("   Flux or dPP/dE (jfactor<0):  %s -z -i %s\n", argv[0], standard_paramfile.c_str());
            inputparameters_write_paramfile(standard_paramfile, gSIM_FLAG_MODE, false);
            return 1;
         }

         // Usage of -1, -2, -3...
         if (choice != -1 || (is_print_default_params == false && argc < 3)) {
            printf("  usage:\n");
            printf(" [ Flux or dPP/dE (jfactor<=0)   ]  %s -z  -i param_file  -any_parameter (or --any_parameter)\n", argv[0]);
            printf("\n N.B.: for a working example, use parameters from option \"-zD\".\n");
            printf("\n\n");
            return 1;
         }

         // now we can load the parameters
         if (!is_print_default_params) load_parameters(file, inputparams_from_commandline, gSIM_FLAG_MODE);

         double par_spec[6];
         //  par_spec[0]   Mass of DM candidate [GeV]
         //  par_spec[1]   Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
         //  par_spec[2]   Card for PP final state (gENUM_FINALSTATE)
         //  par_spec[3]   Redshift of the emitted particle
         //  par_spec[4]   e_pow (=0 -> particle flux, =1 -> energy flux, =2 ...)
         //  par_spec[5]   systematic uncertainty on EBL (only used for z>0)

         par_spec[0] = gPP_DM_MASS_GEV;
         par_spec[1] = gPP_FLAG_SPECTRUMMODEL;
         par_spec[2] = gSIM_FLUX_FLAG_FINALSTATE;
         par_spec[3] = gSIM_REDSHIFT;
         par_spec[4] = gSIM_XPOWER;
         par_spec[5] = 0.;

         if (gSIM_IS_ASTRO_OR_PP_UNITS) convert_to_PP_units(1, gSIM_JFACTOR);

         fluxes_plot(par_spec, gSIM_FLUX_EMIN_GEV, gSIM_FLUX_EMAX_GEV, gSIM_NX, gSIM_JFACTOR);


         //##############################//
         //# Append fluxmaps #//
         //##############################//

      } else if (opt.substr(0, 2) == "-f") {

         // Print example values
         if (is_print_default_params) {
            printf("   Flux and Intensity maps:  %s -f  -i clumpy_params.txt -r inputoutputfile.fits \n", argv[0]);
            inputparameters_write_paramfile(standard_paramfile, gSIM_FLAG_MODE, false);
            return 1;
         }

         // Usage of -1, -2, -3...
         if (argc < 6) {
            printf("  usage:\n");
            printf(" [ Flux and Intensity maps   ]  %s -f -i param_file -r inputoutput_file \n", argv[0]);
            printf("\n\n");
            return 1;
         }

         // now we can load the parameters. The following global variables are unset because they are later checked to be set by the user.
         gPP_DM_ANNIHIL_DELTA  = -999;
         gPP_DM_ANNIHIL_SIGMAV_CM3PERS  = -999;
         gPP_DM_DECAY_LIFETIME_S = -999;
         gEXTRAGAL_FLAG_ABSORPTIONPROFILE = -999;
         if (!is_print_default_params) load_parameters(file, inputparams_from_commandline, gSIM_FLAG_MODE);

         fits_append_fluxextension(second_file);
         printf("   [ Flux and Intensity maps   ]  %s -f -i %s  -r %s\n", argv[0], argv[3], argv[5]);
      }
   }
#if IS_ROOT
   if (gSIM_IS_DISPLAY) delete gSIM_ROOTAPP;
   gSIM_ROOTAPP = NULL;
   delete gSIM_CLUMPYAD;
#endif
   return 0;
}


/*! \file clumpy.cc
  \brief <b>CLUMPY executable</b> (profiles, 1D or 2D-\f$J\f$ skymaps, stat. analysis, ...)

  \mainpage

<b> This is the Doxygen developer documentation. <a href=../index.html>Get back to the user documentation</a>.</b>

If you want to contribute to the code or have found a bug, don't hesitate to contact the CLUMPY crew, <a mailto=clumpy@lpsc.in2p3.fr>clumpy@lpsc.in2p3.fr</a>!

*/

