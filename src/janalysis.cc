/*! \file janalysis.cc \brief (see janalysis.h) */

// CLUMPY includes
#include "../include/clumps.h"
#include "../include/cosmo.h"
#include "../include/geometry.h"
#include "../include/healpix_fits.h"
#include "../include/inlines.h"
#include "../include/janalysis.h"
#include "../include/jeans_analysis.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/profiles.h"
#include "../include/spectra.h"
#include "../include/stat.h"

// HEALPix includes
#include <fitshandle.h>
#include <powspec_fitsio.h>

#if IS_ROOT
// ROOT includes
#include <TArrow.h>
#include <TH2D.h>
#include <THStack.h>
#include <TLegendEntry.h>
#include <TLegend.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TPaveText.h>
#include <TStyle.h>
#include <TPaletteAxis.h>
#include <TNtuple.h>
#include <TFile.h>
#include <TF3.h>
#include <TRandom.h>
#endif

// C++ std libraries
using namespace std;
#include <iomanip>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <algorithm>

//______________________________________________________________________________
void convert_to_PP_units(int switch_qty, double &val_y)
{
   //--- Converts value to PP units if required (gSIM_IS_ASTRO_OR_PP_UNITS).
   //
   // INPUTS:
   //  switch_qty     Quantity to convert
   //                    0 -> rho [Msol/kpc^3]
   //                    1 -> J [M^2/kpc^5] or D [M/kpc^2]
   //                    2 -> mass [Msol]
   // INPUT/OUTPUT:
   //  val_y          Value to convert

   // If unit asked for is ASTRO, nothing to do
   if (gSIM_IS_ASTRO_OR_PP_UNITS)
      return;

   double convert = 1;
   if (switch_qty == 0)
      convert = 1. / GEVperCM3_to_MSOLperKPC3;
   else if (switch_qty == 1) {
      if (gPP_DM_IS_ANNIHIL_OR_DECAY)
         convert = 1. / GEV2perCM5_to_MSOL2perKPC5;
      else
         convert = 1. / GEVperCM2_to_MSOLperKPC2;
   } else if (switch_qty == 2)
      convert = 1. / GEV_to_MSOL;

   val_y *= convert;
}

//______________________________________________________________________________
void convert_to_PP_units(int switch_qty, int n_y, double val_y[])
{
   //--- Converts array of values to PP units if required (gSIM_IS_ASTRO_OR_PP_UNITS).
   //
   // INPUTS:
   //  switch_qty     Quantity to convert
   //                    0 -> rho [Msol/kpc^3]
   //                    1 -> J [M^2/kpc^5] or D [M/kpc^2]
   //                    2 -> mass [Msol]
   //  ny             number of values in val_y
   // INPUT/OUTPUT:
   //  val_y          Array of values to convert

   // If unit asked for is ASTRO, nothing to do
   if (gSIM_IS_ASTRO_OR_PP_UNITS)
      return;

   for (int i = 0; i < n_y; ++i)
      convert_to_PP_units(switch_qty, val_y[i]);
}

//______________________________________________________________________________
void gal_set_pardpdv(double par_dpdv[10])
{
   //--- Fills parameters for the spatial distribution of clumps in the Galaxy.
   //
   // OUTPUTS:
   //  par_dpdv[0]    dpdv normalisation [kpc^{-3}]
   //  par_dpdv[1]    dpdv scale radius [kpc]
   //  par_dpdv[2]    dpdv shape parameter #1
   //  par_dpdv[3]    dpdv shape parameter #2
   //  par_dpdv[4]    dpdv shape parameter #3
   //  par_dpdv[5]    dpdv card_profile [gENUM_PROFILE]
   //  par_dpdv[6]    Radius of dpdv [kpc]
   //  par_dpdv[7]    Distance observer - host halo centre [kpc]
   //  par_dpdv[8]    Longitude (psi) of host halo [rad]
   //  par_dpdv[9]    Latitude (theta) of host halo [rad]

   par_dpdv[1] = gMW_SUBS_DPDV_RSCALE_TO_RS_HOST * gMW_TOT_RSCALE;
   par_dpdv[2] = gMW_SUBS_DPDV_SHAPE_PARAMS[0];
   par_dpdv[3] = gMW_SUBS_DPDV_SHAPE_PARAMS[1];
   par_dpdv[4] = gMW_SUBS_DPDV_SHAPE_PARAMS[2];
   par_dpdv[5] = gMW_SUBS_DPDV_FLAG_PROFILE;
   par_dpdv[6] = gMW_RMAX;
   par_dpdv[7] = gMW_RSOL;
   par_dpdv[8] = 0.;
   par_dpdv[9] = 0.;

   if (par_dpdv[5] == kDPDV_PIERI11) {

      gsl_function F;
      F.function = &solve_rbias;

      const int npar1 = 10;
      double par_tot[npar1];
      double par_tmp[2];
      par_tmp[0] = 1.;
      par_tmp[1] = gMW_SUBS_DPDM_SLOPE;
      gal_set_partot(par_tot);

      double mdelta = mass_singlehalo(par_tot, gSIM_EPS);
      double mmax_subs = gDM_SUBS_MMAXFRAC * mdelta;
      dpdm_setnormprob(&par_tmp[0], gDM_SUBS_MMIN, mmax_subs, gSIM_EPS);
      double ntot = nsubtot_from_nsubm1m2(par_tmp, gMW_SUBS_M1, gMW_SUBS_M2, gMW_SUBS_N_INM1M2, gSIM_EPS);
      double mass_mean = mean1cl_mass(par_tmp, gDM_SUBS_MMIN, mmax_subs, gSIM_EPS);
      double fsub = mass_mean * ntot / mdelta;

      if (fsub > SMALL_NUMBER) {
         rbias_params_for_rootfinding params = {mdelta, par_tot[6], par_tot[0], par_tot[1], par_tot[2], par_tot[3], par_tot[4], int(par_tot[5]), fsub};
         F.params = &params;

         double rmin = 1e-5;
         double rmax = 1e5;
         double eps = 1e-8;
         int return_status = 0;
         double rbias =  rootsolver_gsl(gsl_root_fsolver_brent, F, rmin, rmax, eps, return_status); // faster than gsl_root_fsolver_brent and gsl_root_fsolver_bisection
         par_dpdv[1] = gMW_TOT_RSCALE;
         par_dpdv[2] = rbias;
         par_dpdv[3] = 0;
         par_dpdv[4] = 0;
      }
   }

   // Find proper normalisation for dpdv to be a probability, i.e. int_0^rvir dpdv = 1
   dpdv_setnormprob(par_dpdv, gSIM_EPS);
}

//______________________________________________________________________________
void gal_set_parsmooth(double par_smooth[21])
{
   //--- Fills parameters for the Galactic smooth halo.
   //
   // OUTPUTS:
   //  par_smooth[0]  rho_1(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par_smooth[1]  rho_1(r) scale radius [kpc]
   //  par_smooth[2]  rho_1(r) shape parameter #1
   //  par_smooth[3]  rho_1(r) shape parameter #2
   //  par_smooth[4]  rho_1(r) shape parameter #3
   //  par_smooth[5]  rho_1(r) card_profile [gENUM_PROFILE]
   //  par_smooth[6]  rho_1(r) effective radius where to stop integration [kpc]
   //  par_smooth[7]  rho_1(r) distance observer - clump centre [kpc]
   //  par_smooth[8]  rho_1(r) psi_cl to clump centre [rad]
   //  par_smooth[9]  rho_1(r) theta_cl to clump centre [rad]
   //  par_smooth[10] switch_rho: selects which combination of rho_1 and rho_2 to use
   //                    0 -> rho(r) = rho1(r)
   //                    1 -> rho(r) = rho1(r)-rho2(r)
   //                    2 -> rho(r) = rho1(r)*rho2(r)
   //  par_smooth[11] rho_2(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par_smooth[12] rho_2(r) scale radius [kpc]
   //  par_smooth[13] rho_2(r) shape parameter #1
   //  par_smooth[14] rho_2(r) shape parameter #2
   //  par_smooth[15] rho_2(r) shape parameter #3
   //  par_smooth[16] rho_2(r) card_profile [gENUM_PROFILE]
   //  par_smooth[17] rho_2(r) effective radius where to stop integration [kpc]
   //  par_smooth[18] rho_2(r) distance observer - clump centre [kpc]
   //  par_smooth[19] rho_2(r) psi_cl to clump centre [rad]
   //  par_smooth[20] rho_2(r) theta_cl to clump centre [rad]

   const int npar1 = 10;
   const int npar2 = 25;
   double par_tot[npar1];
   double par_dpdv[npar1];
   double par_subs[npar2];
   gal_set_partot(par_tot);
   gal_set_pardpdv(par_dpdv);

   // par_subs contains f_dm and Mtot host
   gal_set_parsubs(par_subs);
   double m_subs = par_subs[14] * mass_singlehalo(par_tot, gSIM_EPS);

   for (int i = 0; i < 10; i++) par_smooth[i] = par_tot[i];
   par_smooth[10] = 1;
   for (int i = 11; i < 21; i++) par_smooth[i] = par_dpdv[i - 11];
   par_smooth[11] *= m_subs;
}

//______________________________________________________________________________
void gal_set_parsubs(double par_subs[25])
{
   //--- Fills parameters for mass distribution, number and profiles for Galactic clumps.
   //
   // OUTPUTS:
   //  par_subs[0]    rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_subs[1]    rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_subs[2]    rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_subs[3]    rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]    host halo outer radius where to stop integration [kpc]
   //  par_subs[5]    eps: relative precision sought L calculation
   //  par_subs[6]    z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]    mdelta: virial mass of the clump (UNUSED in this function)
   //  par_subs[8]    dPdM normalisation [1/Msol]             [UNUSED for DECAY]
   //  par_subs[9]    dPdM slope alphaM                       [UNUSED for DECAY]
   //  par_subs[10]   dPdc gENUM_CDELTAMDELTA (cdelta-mdelta) [UNUSED for DECAY]
   //  par_subs[11]   dPdc mean concentration <c>   (UNUSED in this function)
   //  par_subs[12]   dPdc standard deviation                 [UNUSED for DECAY]
   //  par_subs[13]   dPdc card_profile [gENUM_CDELTA_DIST]   [UNUSED for DECAY]
   //  par_subs[14]   f: mass fraction of substructures       [UNUSED for DECAY]
   //  par_subs[15]   nlevel: sub-sub...halos (1=no sub)      [UNUSED for DECAY]
   //  par_subs[16]   dPdV normalisation  [kpc^{-3}]          [UNUSED for DECAY]
   //  par_subs[17]   dPdV scale radius [kpc]                 [UNUSED for DECAY]
   //  par_subs[18]   dPdV shape parameter #1                 [UNUSED for DECAY]
   //  par_subs[19]   dPdV shape parameter #2                 [UNUSED for DECAY]
   //  par_subs[20]   dPdV shape parameter #3                 [UNUSED for DECAY]
   //  par_subs[21]   dPdV card_profile [gENUM_PROFILE]       [UNUSED for DECAY]
   //  par_subs[22]   host halo outer radius (unused)         [UNUSED for DECAY]
   //  par_subs[23]   ratio rs dPdV to rs_cl                  [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]   R_host (corresp. to current dPdV)       [only for ANNIHILATION and nlevel>1]


   if (gDM_SUBS_NUMBEROFLEVELS > 0 && (
            (gMW_SUBS_DPDV_FLAG_PROFILE == kDPDV_GAO04 && (gMW_SUBS_DPDV_SHAPE_PARAMS[2] - gMW_SUBS_DPDV_SHAPE_PARAMS[1] > 1.))
            || (gMW_SUBS_DPDV_FLAG_PROFILE == kZHAO && gMW_SUBS_DPDV_SHAPE_PARAMS[1] < 2.))) {

      printf("\n====> ERROR: gal_set_parsubs() in janalysis.cc");
      printf("\n             Selected parameters for dpdv profile (%s) do not allow sub-substructures (outer slope < 2)", gNAMES_PROFILE[gMW_SUBS_DPDV_FLAG_PROFILE]);
      printf("\n             => abort()\n\n");
      abort();
   }

   // Need par_tot and par_dpdv!
   const int npar = 10;
   double par_tot[npar];
   gal_set_partot(par_tot);
   double mtot = mass_singlehalo(par_tot, gSIM_EPS);
   double mmax_subs = gDM_SUBS_MMAXFRAC * mtot;
   double par_dpdv[npar];
   gal_set_pardpdv(par_dpdv);

   par_subs[0] = gMW_SUBS_SHAPE_PARAMS[0];
   par_subs[1] = gMW_SUBS_SHAPE_PARAMS[1];
   par_subs[2] = gMW_SUBS_SHAPE_PARAMS[2];
   par_subs[3] = gMW_SUBS_FLAG_PROFILE;
   par_subs[4] = gMW_RMAX;
   par_subs[5] = gSIM_EPS;
   par_subs[6] = 0.;
   //par_subs[7] = mtot; // check!
   par_subs[7] = 0.; // will only be used - and updated later - for nlevel > 1 substructures.
   par_subs[8] = 1.;
   par_subs[9] = gMW_SUBS_DPDM_SLOPE;
   par_subs[10] = gMW_SUBS_FLAG_CDELTAMDELTA;
   double Delta_c = delta_x_to_delta_crit(gCOSMO_DELTA0, gCOSMO_FLAG_DELTA_REF, par_subs[6]);
   // Update inner profile (has only effects for mass-dependent profiles, like ISHIYAMA14)
   // work later with average c(M) relation in Galactic halo, in case  mass-dependent halo profile is chosen
   double xpos = -1;
   mdelta_to_innerslope(mtot, Delta_c, par_subs[10], par_subs, par_subs[6], xpos);
   //par_subs[11] = mdelta_to_cdelta(mtot, Delta_c, gMW_SUBS_FLAG_CDELTAMDELTA, par_tot, par_subs[6]/*z*/);
   par_subs[11] = 0.; // will only be used - and updated later - for nlevel > 1 substructures.
   par_subs[12] = gDM_LOGCDELTA_STDDEV;
   par_subs[13] = gDM_FLAG_CDELTA_DIST;
   par_subs[14] = 0; //  f: mass fraction of substructures. Is updated below

   for (int i = 0; i < 7; ++i)
      par_subs[i + 16] = par_dpdv[i];
   par_subs[23] = gMW_SUBS_DPDV_RSCALE_TO_RS_HOST;

   // Find proper normalisation for dpdm to be a probability, i.e. int_Mmin^Mmax dpdm dM = 1
   dpdm_setnormprob(&par_subs[8], gDM_SUBS_MMIN, mmax_subs, gSIM_EPS);
   if (gDM_SUBS_NUMBEROFLEVELS > 0 or gMW_SUBS_N_INM1M2 > 0) {
      par_subs[15] = gDM_SUBS_NUMBEROFLEVELS;
      // Calculate f_dm => par_subs[14]
      double ntot = nsubtot_from_nsubm1m2(&par_subs[8], gMW_SUBS_M1, gMW_SUBS_M2, gMW_SUBS_N_INM1M2, gSIM_EPS);
      double mass_mean = mean1cl_mass(&par_subs[8], gDM_SUBS_MMIN, mmax_subs, gSIM_EPS);
      double f_dm = mass_mean * ntot / mtot;
      par_subs[14] = f_dm;
   } else {
      par_subs[14] = 0; //  f: mass fraction of substructures = 0
      par_subs[15] = 1; //  dummy value to avoid code to crash.
   }
}

//______________________________________________________________________________
void gal_set_partot(double par_tot[10])
{
   //--- Fills parameters for the total DM halo of the Galaxy.
   //
   // OUTPUTS:
   //  par_tot[0]     rho_tot normalisation [kpc^{-3}]
   //  par_tot[1]     rho_tot scale radius [kpc]
   //  par_tot[2]     rho_tot shape parameter #1
   //  par_tot[3]     rho_tot shape parameter #2
   //  par_tot[4]     rho_tot shape parameter #3
   //  par_tot[5]     rho_tot card_profile [gENUM_PROFILE]
   //  par_tot[6]     Radius of rho_tot [kpc]
   //  par_tot[7]     Distance observer - host halo centre [kpc]
   //  par_tot[8]     Longitude (psi) of host halo [rad]
   //  par_tot[9]     Latitude (theta) of host halo [rad]


   par_tot[1] = gMW_TOT_RSCALE;
   par_tot[2] = gMW_TOT_SHAPE_PARAMS[0];
   par_tot[3] = gMW_TOT_SHAPE_PARAMS[1];
   par_tot[4] = gMW_TOT_SHAPE_PARAMS[2];
   par_tot[5] = gMW_TOT_FLAG_PROFILE;
   par_tot[6] = gMW_RMAX;
   par_tot[7] = gMW_RSOL;
   par_tot[8] = 0.;
   par_tot[9] = 0.;

   // Find proper normalisation to have rho_tot(gMW_RSOL) = gMW_RHOSOL
   set_par0_given_rhoref(par_tot, gMW_RSOL, gMW_RHOSOL);
}

//______________________________________________________________________________
void gal_j1D(vector<double> const &x, int switch_y, bool is_list_halos,
             double psi_los, double theta_los, double const &contrast_thresh, double const &phi_cut_deg)
{
   //--- Prints/Plots y(x) and y(x)/y(x[0]) for Jgal [smooth, <cl>, halos].
   //    N.B.: if is_list_halos=true, takes into account the list of halos
   //    file 'gLIST_HALOES'.
   //
   //  x              Grid of x-axis values
   //  param_file     Parameter file to initialise all CLUMPY parameters
   //  switch_y       Quantity to display
   //                    0 => rho(r_kpc)
   //                    1 => J(alphaint_deg)
   //                    2 => J(theta_deg)
   //                    3 => Mass(r_kpc)
   //  is_list_halos  Whether to add on the graph J from all halos in list    [only if switch_y = 2]
   //  psi_los        Longitude of the line of sight [rad]                    [only if switch_y=1]
   //  theta_los      Latitude of the line of sight [rad]                     [only if switch_y=1]
   //  contrast_thresh Print/sort halos with J_halo/J_Gal>contrast_thresh     [if switch_y=2 and is_list_halos=true]
   //  phi_cut_deg    Discard halos with phi (w.r.t. Gal. centre)<phi_cut_deg [if switch_y=2 and is_list_halos=true]


   // Set ROOT style
#if IS_ROOT
   rootstyle_set2CLUMPY();
#endif

   char tmp[500];

   char j_or_d = 'D';
   if (gPP_DM_IS_ANNIHIL_OR_DECAY)
      j_or_d = 'J';

   // Is flux?
   bool is_flux = gSIM_IS_WRITE_FLUXMAPS;
   if (switch_y == 0 || switch_y == 3)
      is_flux = false;
   double par_spec[6];
   par_spec[0] = gPP_DM_MASS_GEV;
   par_spec[1] = gPP_FLAG_SPECTRUMMODEL;
   par_spec[2] = kGAMMA;
   par_spec[3] = 0.;
   par_spec[4] = 0.;
   par_spec[5] = 0.;

   // Is subs?
   bool is_subs = true;
   if (gMW_SUBS_N_INM1M2 == 0 || switch_y == 3)
      is_subs = false;

   double old_alpha_aperture = gSIM_ALPHAINT;
   int n_x = x.size();

   // What is going to be calculated (for prints and plots)
   // -> units_or_canvasname_1D(switch_y, switch_name, is_norm)
   string cvs_name = units_or_canvasname_1D(switch_y, 0, false);
   string x_name = units_or_canvasname_1D(switch_y, 1, false);
   string y_name = units_or_canvasname_1D(switch_y, 2, false);
   string y_name_shell_tmptmptmp = y_name.substr(0, y_name.size() - 1);
   string y_name_shell_tmptmp = y_name_shell_tmptmptmp + " sr^{-1}]";
   string y_name_shell_tmp = y_name_shell_tmptmp.substr(y_name_shell_tmptmp.find_first_of("("), y_name_shell_tmptmp.size());
   string y_name_shell;
   if (gPP_DM_IS_ANNIHIL_OR_DECAY) y_name_shell = "dJ/d#Omega" + y_name_shell_tmp;
   else y_name_shell = "dD/d#Omega" + y_name_shell_tmp;
   string y_name_flux_tmp = "flux [# cm^{-2} s^{-1}";
   string y_name_intensity_tmp = "intensity [# cm^{-2} s^{-1} sr^{-1}";
   string y_name_flux;
   string y_name_intensity;
   if (!gSIM_FLUX_IS_INTEG_OR_DIFF) {
      y_name_flux = "Differential " + y_name_flux_tmp + " GeV^{-1}]";
      y_name_intensity = "Differential " + y_name_intensity_tmp + " GeV^{-1}]";
   } else {
      y_name_flux = "Integrated " + y_name_flux_tmp + "]";
      y_name_intensity = "Integrated " + y_name_intensity_tmp + "]";
   }

   // Set Gal. parameters
   const int npar1 = 10;
   double par_galtot[npar1];
   double par_galtot_tmp[npar1]; // for calculating r_200
   double par_galdpdv[npar1];
   double par_galdpdv_N_cl[npar1];
   double par_galclmean[npar1];
   gal_set_partot(par_galtot);
   gal_set_partot(par_galtot_tmp);
   double m_gal = mass_singlehalo(par_galtot, gSIM_EPS);

   const int npar2 = 25;
   double par_galsubs[npar2];

   double f_dm = 0., ntot_subs = 0.;
   double mmin_subs = gDM_SUBS_MMIN;
   double mmax_subs = m_gal * gDM_SUBS_MMAXFRAC;

   const int npar3 = 21;
   double par_galsmooth[npar3];

   // Mass fraction in clumps in the galaxy (filled later on)
   // (and other default values)
   double lmin = 0., lmax = gMW_RSOL + gMW_RMAX;

   if (is_subs) {
      cout << ">>>>> " << y_name << " for smooth,  clumps (Mtot=" << m_gal << " Msol)";
      if ((switch_y == 1 || switch_y == 2) && gPP_DM_IS_ANNIHIL_OR_DECAY)
         cout << ", and cross-product" << endl;
      else
         cout << endl;
      // find the mass fraction in form of clumps
      gal_set_pardpdv(par_galdpdv);
      gal_set_pardpdv(par_galdpdv_N_cl);
      gal_set_parsubs(par_galsubs);
      gal_set_parsmooth(par_galsmooth);
      ntot_subs = nsubtot_from_nsubm1m2(&par_galsubs[8], gMW_SUBS_M1, gMW_SUBS_M2, gMW_SUBS_N_INM1M2, gSIM_EPS);
      f_dm = par_galsubs[14];
      cout << "  => f_dm=" << par_galsubs[14] << endl;
      if (switch_y == 0) {
         for (int ii = 0; ii < npar1; ++ii)
            par_galclmean[ii] = par_galdpdv[ii];
         par_galclmean[0] *= par_galsubs[14] * m_gal;  /* f_dm * mgal */
      }
      //print_partot(par_galtot);
      //print_pardpdv(par_galdpdv);
      //print_parsubs(par_galsubs);
   } else
      cout << ">>>>> " << y_name << " for smooth (Mtot=" << m_gal << " Msol)" << endl;


   if (switch_y == 1 || switch_y == 2 || switch_y == 3)
      cout << "  => Relative precision: " << gSIM_EPS << endl;

   //--- x vector in degrees (for switch_y == 1 || switch_y == 2)
   vector<double> x_deg;
   //--- Storage variables (for flux)
   vector<double> flux_gamma(n_x, 1.e-40);
   vector<double> flux_nu(n_x, 1.e-40);
   vector<double> intensity_gamma(n_x, 1.e-40);
   vector<double> intensity_nu(n_x, 1.e-40);
   //--- Storage variables (for smooth and tot)
   vector<double> y_sm(n_x, 1.e-40);
   vector<double> y_tot(n_x, 1.e-40);
   //--- Storage variables (for meansub)
   vector<double> y_cl(n_x, 1.e-40);
   vector<double> y_cp(n_x, 1.e-40);
   vector<double> N_cl(n_x, 1.e-40);

   vector<double> y_sm_shell(n_x, 1.e-40);
   vector<double> y_cl_shell(n_x, 1.e-40);
   vector<double> y_cp_shell(n_x, 1.e-40);
   vector<double> y_tot_shell(n_x, 1.e-40);


   // ASCII Outputs/prints
   FILE *fp[4] = {NULL, NULL, NULL, NULL};
   FILE *fp_current;
   string f_name = gSIM_OUTPUT_DIR + "gal." + cvs_name + ".output";
   fp[0] = fopen(f_name.c_str(), "w");
   fp[1] = stdout;
   string f_name_diff = gSIM_OUTPUT_DIR + "gal." + cvs_name + "diff.output";;
   string f_name_flux;
   if (is_flux) {
      if (switch_y == 1) f_name_flux  = gSIM_OUTPUT_DIR + "gal.fluxes.output";
      else if (switch_y == 2) {
         f_name_flux  = gSIM_OUTPUT_DIR + "gal.intensities.output";
      } else {
         printf("\n====> ERROR: gal_j1D() in janalysis.cc");
         printf("\n             Flux calculation w/o switch_y == 1/2 not allowed.");
         printf("\n             => abort()\n\n");
         abort();
      }
      fp[3] = fopen(f_name_flux.c_str(), "w");
      if (switch_y == 2) fp[2] = fopen(f_name_diff.c_str(), "w");
   }

   // First call of flux (dummy to load file not in the middle of other prints)
   if (is_flux) {
      par_spec[2] = kGAMMA;
      flux(par_spec, gSIM_FLUX_AT_E_GEV, 1., false);
      par_spec[2] = kNEUTRINO;
      if (gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_EW || gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_NOEW) {
         flux(par_spec, gSIM_FLUX_AT_E_GEV, 1., false);
      }
   }

   int ff_max = 2;
   if (switch_y == 2) ff_max = 3;

   // Print header
   if (gSIM_IS_PRINT) {
      // Print header
      for (int ff = 0; ff < ff_max; ++ff) {
         fp_current = fp[ff];
         fprintf(fp_current, "# x= %s ,  ", x_name.c_str());
         if (ff < 2) fprintf(fp_current, "y= %s\n", y_name.c_str());
         else if (ff == 2) fprintf(fp_current, "y= %s\n", y_name_shell.c_str());
         if (!is_subs || switch_y == 3) {
            fprintf(fp_current, "#        x       y\n");
         } else {
            if (switch_y != 0 && gPP_DM_IS_ANNIHIL_OR_DECAY)
               fprintf(fp_current, "#      x     y[    sm       <subs>     <cp>       tot     <subs>/tot]\n");
            else if (switch_y == 0)
               fprintf(fp_current, "#      x     y[    sm       <subs>      tot    <subs>/tot  N_cl(<r)/N_tot]\n");
            else
               fprintf(fp_current, "#      x     y[    sm       <subs>      tot    <subs>/tot]\n");
         }
      }
      if (is_flux and switch_y == 1) fprintf(fp[3], "# %s\t%s (Gammas)\t%s (Neutrinos)\n", x_name.c_str(), y_name_flux.c_str(), y_name_flux.c_str());
      else if (is_flux and switch_y == 2) fprintf(fp[3], "# %s\t%s (Gammas)\t%s (Neutrinos)\n", x_name.c_str(), y_name_intensity.c_str(), y_name_intensity.c_str());
   }

   double Rdelta;
   double Mdelta;
   double Delta_c = delta_x_to_delta_crit(gCOSMO_DELTA0, gCOSMO_FLAG_DELTA_REF, gSIM_REDSHIFT);
   double gSIM_EPS_tmp = gSIM_EPS;
   gSIM_EPS *= 0.1;
   Rdelta = shapeparams_to_Rdelta(par_galtot, Delta_c, gSIM_REDSHIFT);
   gSIM_EPS = gSIM_EPS_tmp;
   Mdelta = Rdelta_to_mdelta(Rdelta, Delta_c, gSIM_REDSHIFT);

   // only for switch_y = 2:
   double alpha_int_shell = 1e-6;
   double int_area = 2. * PI * (1 - cos(alpha_int_shell)) ;

   // Calculate y
   // 0. rho(r)

   if (switch_y == 0) {
      for (int k = 0; k < n_x; ++k) {

         double tmp_x = x[k];

         // cut off at Rvir;
         if (tmp_x > gMW_RMAX) {
            y_sm[k] = 1e-40;
            y_tot[k] = 1e-40;
            y_cl[k] = 1e-40;
            par_galdpdv_N_cl[6] = gMW_RMAX;
            N_cl[k] = mass_singlehalo(par_galdpdv_N_cl, gSIM_EPS);
         } else {

            if (is_subs)
               rho_mix(tmp_x, par_galsmooth, y_sm[k]);
            else
               rho(tmp_x, par_galtot, y_sm[k]);
            // subs
            y_tot[k] = y_sm[k];
            if (is_subs) {
               rho(tmp_x, par_galclmean, y_cl[k]);
               y_tot[k] += y_cl[k];
               par_galdpdv_N_cl[6] = tmp_x;
               N_cl[k] = mass_singlehalo(par_galdpdv_N_cl, gSIM_EPS); // N_cl is normalized!
            }
            // Convert before printing
            convert_to_PP_units(0, y_cl[k]);
            convert_to_PP_units(0, y_tot[k]);
            convert_to_PP_units(0, y_sm[k]);
         }
         // Print on screen and in file)
         if (gSIM_IS_PRINT) {
            for (int ff = 0; ff < 2; ++ff) {
               fp_current = fp[ff];
               if (!is_subs)
                  fprintf(fp_current, "  %.*le   %.*le\n", gSIM_SIGDIGITS + 2, x[k], gSIM_SIGDIGITS, y_sm[k]);
               else
                  fprintf(fp_current, "  %.*le     %.*le   %.*le   %.*le   %.*le   %.*le\n",
                          gSIM_SIGDIGITS + 2, x[k], gSIM_SIGDIGITS, y_sm[k], gSIM_SIGDIGITS, y_cl[k],
                          gSIM_SIGDIGITS, y_tot[k], gSIM_SIGDIGITS, y_cl[k] / y_tot[k], gSIM_SIGDIGITS, N_cl[k]);
            }
         }
      }
      // (1) => J(alpha_int) ; (2) => J(theta_los)
   } else if (switch_y == 1 || switch_y == 2) {
      for (int k = 0; k < n_x; ++k) {

         // 1) => J(alpha_int)
         if (switch_y == 1) {
            gSIM_ALPHAINT = x[k];
            if (gSIM_ALPHAINT < 1.e-30) continue;

            // (2) => J(theta_los)
         } else if (switch_y == 2) {
            // Update value of direction where the instrument points
            psi_los = x[k];
         }

         double y_ref = jsmooth(par_galtot, psi_los, theta_los, gSIM_EPS);
         double y_ref_shell;
         if (switch_y == 2) {
            gSIM_ALPHAINT = alpha_int_shell;
            y_ref_shell = jsmooth(par_galtot, psi_los, theta_los, gSIM_EPS) / int_area;
            gSIM_ALPHAINT = old_alpha_aperture;
         }

         if (is_subs) {
            y_sm[k] = jsmooth_mix(m_gal, par_galtot, psi_los, theta_los, gSIM_EPS, f_dm, par_galdpdv);
            if (switch_y == 2) {
               gSIM_ALPHAINT = alpha_int_shell;
               y_sm_shell[k] = jsmooth_mix(m_gal, par_galtot, psi_los, theta_los, gSIM_EPS, f_dm, par_galdpdv) / int_area;
               gSIM_ALPHAINT = old_alpha_aperture;
            }

            y_cl[k] = jsub_continuum(ntot_subs, par_galdpdv, psi_los, theta_los, lmin, lmax, par_galsubs, mmin_subs, mmax_subs);
            if (switch_y == 2) {
               gSIM_ALPHAINT = alpha_int_shell;
               y_cl_shell[k] = jsub_continuum(ntot_subs, par_galdpdv, psi_los, theta_los, lmin, lmax, par_galsubs, mmin_subs, mmax_subs) / int_area;
               gSIM_ALPHAINT = old_alpha_aperture;
            }

            y_tot[k] = y_sm[k] + y_cl[k];
            y_tot_shell[k] = y_sm_shell[k] + y_cl_shell[k];

            if (switch_y != 0 && gPP_DM_IS_ANNIHIL_OR_DECAY) {
               y_cp[k] = jcrossprod_continuum(m_gal, par_galtot, psi_los, theta_los, lmin, lmax, gSIM_EPS, f_dm, par_galdpdv);
               if (switch_y == 2) {
                  gSIM_ALPHAINT = alpha_int_shell;
                  y_cp_shell[k] = jcrossprod_continuum(m_gal, par_galtot, psi_los, theta_los, lmin, lmax, gSIM_EPS, f_dm, par_galdpdv) / int_area;
                  gSIM_ALPHAINT = old_alpha_aperture;
               }

               y_tot[k] += y_cp[k];
               y_tot_shell[k] += y_cp_shell[k];
            }

         } else {
            y_sm[k] = y_ref;
            if (switch_y == 2) y_sm_shell[k] =  y_ref_shell;

            y_tot[k] = y_sm[k];
            if (switch_y == 2) y_tot_shell[k] = y_sm_shell[k];
         }

         // now x[k] can be converted to degrees for display purpose:
         x_deg.push_back(x[k] * RAD_to_DEG);

         // DAVID
         // Simple Extra-galactic contrib (for decay only)
         //y_sm[k] =  3.63521e+09 / (4.*PI) * (2. * PI * (1.-cos(gSIM_ALPHAINT)));
         //y_tot[k] = y_sm[k];

         // Convert before printing
         convert_to_PP_units(1, y_cp[k]);
         convert_to_PP_units(1, y_cl[k]);
         convert_to_PP_units(1, y_tot[k]);
         convert_to_PP_units(1, y_sm[k]);
         convert_to_PP_units(1, y_cp_shell[k]);
         convert_to_PP_units(1, y_cl_shell[k]);
         convert_to_PP_units(1, y_tot_shell[k]);
         convert_to_PP_units(1, y_sm_shell[k]);

         // Calculate flux (gamma, and neutrino if available)
         if (is_flux) {
            bool unit_ref = gSIM_IS_ASTRO_OR_PP_UNITS;
            par_spec[2] = kGAMMA;
            if (!gSIM_FLUX_IS_INTEG_OR_DIFF) {
               flux_gamma[k] = flux(par_spec, gSIM_FLUX_AT_E_GEV, y_tot[k], false);
               intensity_gamma[k] = flux(par_spec, gSIM_FLUX_AT_E_GEV, y_tot_shell[k], false);
            } else {
               flux_gamma[k] = flux(par_spec, -999, y_tot[k], true);
               intensity_gamma[k] = flux(par_spec, -999, y_tot_shell[k], true);
            }

            par_spec[2] = kNEUTRINO;
            if (gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_EW || gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_NOEW) {
               if (!gSIM_FLUX_IS_INTEG_OR_DIFF) {
                  flux_nu[k] = flux(par_spec, gSIM_FLUX_AT_E_GEV, y_tot[k], false);
                  intensity_nu[k] = flux(par_spec, gSIM_FLUX_AT_E_GEV, y_tot_shell[k], false);
               } else {
                  flux_nu[k] = flux(par_spec, -999, y_tot[k], true);
                  intensity_nu[k] = flux(par_spec, -999, y_tot_shell[k], true);
               }
            }

            // Watch out: if user-unit is ASTRO, we need to convert here (flux requires PP unit)
            if (gSIM_IS_ASTRO_OR_PP_UNITS) {
               gSIM_IS_ASTRO_OR_PP_UNITS = false;
               convert_to_PP_units(1, flux_gamma[k]);
               convert_to_PP_units(1, flux_nu[k]);
               convert_to_PP_units(1, intensity_gamma[k]);
               convert_to_PP_units(1, intensity_nu[k]);
            }
            gSIM_IS_ASTRO_OR_PP_UNITS = unit_ref;
         }

         // Print on screen and in file)
         if (gSIM_IS_PRINT) {
            for (int ff = 0; ff < ff_max; ++ff) {
               fp_current = fp[ff];
               if (ff < 2) {
                  if (!is_subs)
                     fprintf(fp_current, "  %.*le   %.*le\n", gSIM_SIGDIGITS + 2, x_deg[k], gSIM_SIGDIGITS, y_sm[k]);
                  else {
                     if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                        fprintf(fp_current, "  %.*le     %.*le   %.*le   %.*le   %.*le   %.*le\n",
                                gSIM_SIGDIGITS + 2, x_deg[k], gSIM_SIGDIGITS, y_sm[k], gSIM_SIGDIGITS, y_cl[k],
                                gSIM_SIGDIGITS, y_cp[k], gSIM_SIGDIGITS, y_tot[k], gSIM_SIGDIGITS, y_cl[k] / y_tot[k]);
                     else
                        fprintf(fp_current, "  %.*le     %.*le   %.*le   %.*le   %.*le\n",
                                gSIM_SIGDIGITS + 2, x_deg[k], gSIM_SIGDIGITS, y_sm[k], gSIM_SIGDIGITS, y_cl[k],
                                gSIM_SIGDIGITS, y_tot[k], gSIM_SIGDIGITS, y_cl[k] / y_tot[k]);
                  }
               } else if (ff == 2) {
                  if (!is_subs)
                     fprintf(fp_current, "  %.*le   %.*le\n", gSIM_SIGDIGITS + 2, x_deg[k], gSIM_SIGDIGITS, y_sm_shell[k]);
                  else {
                     if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                        fprintf(fp_current, "  %.*le     %.*le   %.*le   %.*le   %.*le   %.*le\n",
                                gSIM_SIGDIGITS + 2, x_deg[k], gSIM_SIGDIGITS, y_sm_shell[k], gSIM_SIGDIGITS, y_cl_shell[k],
                                gSIM_SIGDIGITS, y_cp_shell[k], gSIM_SIGDIGITS, y_tot_shell[k], gSIM_SIGDIGITS, y_cl_shell[k] / y_tot_shell[k]);
                     else
                        fprintf(fp_current, "  %.*le     %.*le   %.*le   %.*le   %.*le\n",
                                gSIM_SIGDIGITS + 2, x_deg[k], gSIM_SIGDIGITS, y_sm_shell[k], gSIM_SIGDIGITS, y_cl_shell[k],
                                gSIM_SIGDIGITS, y_tot_shell[k], gSIM_SIGDIGITS, y_cl_shell[k] / y_tot_shell[k]);
                  }
               }

            }
            if (is_flux and switch_y == 1) fprintf(fp[3], "  %.*le   %.*le   %.*le\n", gSIM_SIGDIGITS + 2, x_deg[k], gSIM_SIGDIGITS, flux_gamma[k], gSIM_SIGDIGITS, flux_nu[k]);
            else if (is_flux and switch_y == 2) fprintf(fp[3], "  %.*le   %.*le   %.*le\n", gSIM_SIGDIGITS + 2, x_deg[k], gSIM_SIGDIGITS, intensity_gamma[k], gSIM_SIGDIGITS, intensity_nu[k]);
         }
      }
      // M(r)
   } else if (switch_y == 3) {
      for (int k = 0; k < n_x; ++k) {

         par_galtot[6] = x[k];
         if (x[k] > gMW_RMAX) par_galtot[6] = gMW_RMAX;
         y_tot[k] = mass_singlehalo(par_galtot, gSIM_EPS);

         // Print on screen and in file)
         if (gSIM_IS_PRINT) {
            for (int ff = 0; ff < 2; ++ff) {
               fp_current = fp[ff];
               fprintf(fp_current, "  %.*le     %.*le\n", gSIM_SIGDIGITS + 2, x[k], gSIM_SIGDIGITS, y_tot[k]);
            }
         }
      }


   }

   if (switch_y == 0 or switch_y == 3) {
      printf("\n R_%d(z = %g) = %.*f kpc.", int(Delta_c), gSIM_REDSHIFT, gSIM_SIGDIGITS, Rdelta);
      printf("\n M_%d(z = %g) = %.*le Msol.", int(Delta_c), gSIM_REDSHIFT, gSIM_SIGDIGITS, Mdelta);
   }

   //--------------------------------
   // Add Jlist if J(theta) selected
   //--------------------------------
   // DAVID
   // Higher-level option (not available from text-interface)
   // => Integrates on alpha_frac (for which Jfrac = frac Jtot) instead
   //    of fixed angle
   bool is_alpha_frac = false;
   double frac = 0.8;

   vector<double> list_phi;
   pair<double, int> tmp_pair;
   vector<pair<double, int> > list_j, list_contrast, list_jgal, list_boost;
   vector<string> list_name;
   vector<struct gStructHalo> list_halos;
   FILE *fp_list[2] = {NULL, NULL};
   //cout << gLIST_HALOES << endl;
   string list_haloes_filename = gLIST_HALOES.substr(gLIST_HALOES.find_last_of("\\/") + 1, gLIST_HALOES.size() + 1);
   string f_list = gSIM_OUTPUT_DIR + list_haloes_filename + ".gal.list." + cvs_name + ".output";
   //string f_list = "gal.list." + cvs_name + ".output";

   if (is_list_halos && switch_y == 2) {
      cout << endl;
      halo_load_list(gLIST_HALOES, list_halos);
      cout << endl;

      // Set gal bkd (logj) for interpolation
      gSIM_ALPHAINT = old_alpha_aperture;

      cout << endl;
      cout << "     ----------" << endl;
      cout << "   * All halos:" << endl;
      cout << "     ----------" << endl;

      if (gSIM_IS_PRINT)
         fp_list[0] = fopen(f_list.c_str(), "w");

      // Calculate and fill rho for all halos in list
      for (int i = 0; i < (int)list_halos.size() ; ++i) {
         // Stores names
         list_name.push_back(halo_get_name(i, list_halos));


         // Stores phi_halo (angle away from the Galactic centre)
         double psi_halo = list_halos[i].PsiDeg * DEG_to_RAD;
         check_psi(psi_halo);
         double theta_halo = list_halos[i].ThetaDeg * DEG_to_RAD;
         double phi = psitheta_to_phi(psi_halo, theta_halo);
         list_phi.push_back(phi);

         // Set halo parameters (and triaxiality)
         double par_halotot[npar1 + 1];
         halo_set_partot(i, par_halotot, list_halos, true);
         halo_set_triaxiality(i, list_halos);

         double corr = 1.;
         if (is_alpha_frac) {
            // When dealing with a list of object, it is useful to calculat Jx% (e.g. J80%, i.e.
            // integrating on alpha_80% which gives 80% of the signal).
            // N.B.: as we also calculate the DM galactic background, we have to
            // correct for the integration angle at alphaint_ref, so that Jx = corr * Jref,
            // with corr = (alpha_x)^2/(alpha_ref)^2 if the option is 'on' (1 otherwise).
            if (i == 0) {
               sprintf(tmp, "We use gSIM_ALPHAINT fraction alpha_%d throughout", int(frac * 100));
               print_warning("janalysis.cc", "gal_j1D()", string(tmp));
               cout << "    NAME             Index   alpha_s(deg)    alpha_frac(deg)  alphax\%/alphars     Jfrac" << endl;
            }
            double alpha_frac = halo_findfracjtot_alphaint(i, list_halos, frac);
            //double alpha_frac = 0.;
            //find_fracjtot_alphaint(par_halotot,frac,alpha_frac);
            gSIM_ALPHAINT = alpha_frac;
            double Jfrac = halo_jtot(i, list_halos, par_halotot[8], par_halotot[9], gSIM_EPS, true);
            convert_to_PP_units(1, Jfrac);
            corr = alpha_frac * alpha_frac / (old_alpha_aperture * old_alpha_aperture);
            printf(" %-16s   %4d       %.*le         %.*le         %.*le       %.*le\n",
                   (list_halos[i].Name).c_str(), i + 1, gSIM_SIGDIGITS, asin(list_halos[i].Rscale / list_halos[i].l) * RAD_to_DEG,
                   gSIM_SIGDIGITS, alpha_frac * RAD_to_DEG, gSIM_SIGDIGITS, alpha_frac / asin(list_halos[i].Rscale / list_halos[i].l),
                   gSIM_SIGDIGITS, Jfrac);
            // Trick to have alpha_80% drawn in histogram!
            // N.B.: it may print/display unreliable results for other quantities when used!
            list_halos[i].Rscale = tan(alpha_frac) * list_halos[i].l / (1 + list_halos[i].z);
         }

         //--- Form pairs <J,i> and <J/Jgal,i> for further usage (print & draw)
         // Calculate J (standard calculation)
         double jhalo_tot = halo_jtot(i, list_halos, par_halotot[8], par_halotot[9], gSIM_EPS, true);
         convert_to_PP_units(1, jhalo_tot);

         // Set 'no clump' to enable the boost calculation (if applicable)
         double boost = 0.;
         if (gPP_DM_IS_ANNIHIL_OR_DECAY && list_halos[i].Subs_mfrac > 1.e-5) {
            double tmp_f =  list_halos[i].Subs_mfrac;
            list_halos[i].Subs_mfrac = 0.;
            double jhalo_tot_nosubs = halo_jtot(i, list_halos, par_halotot[8], par_halotot[9], gSIM_EPS, true);
            convert_to_PP_units(1, jhalo_tot_nosubs);
            list_halos[i].Subs_mfrac = tmp_f;
            boost = jhalo_tot / jhalo_tot_nosubs;
            tmp_pair = make_pair(boost, i);
            list_boost.push_back(tmp_pair);
         }

         tmp_pair = make_pair(jhalo_tot, i);
         list_j.push_back(tmp_pair);


         vector<double> log10y_tot;
         for (int j = 0; j < (int)x.size(); ++j)
            log10y_tot.push_back(log10(y_tot[j]));

         tmp_pair = make_pair(corr * pow(10., interp1D(phi, x, log10y_tot, kLINLIN)), i);

         list_jgal.push_back(tmp_pair);
         tmp_pair = make_pair(list_j[i].first / list_jgal[i].first, i);
         list_contrast.push_back(tmp_pair);

         //--- Print on screen
         if (gSIM_IS_PRINT) {
            string tmp = list_halos[i].Name + " (" + list_halos[i].Type + ")";
            for (int ff = 0; ff < 2; ++ff) {
               if (is_alpha_frac && ff == 0) continue;
               if (ff == 1) fp_current = fp_list[0];
               else fp_current = stdout;

               if (i == 0) {
                  fprintf(fp_current, "#                halo          #in list      phi      %chalo      %cgal    %chalo/%cgal   log10(%chalo)  log10(%cgal)",
                          j_or_d, j_or_d, j_or_d, j_or_d, j_or_d, j_or_d);
                  if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
                     fprintf(fp_current, "   boost\n");
                     fprintf(fp_current, "#                                           [deg]       %s       -      -\n", y_name.c_str());
                  } else {
                     fprintf(fp_current, "\n");
                     fprintf(fp_current, "#                                           [deg]       %s       -    \n", y_name.c_str());
                  }
               }
               if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                  fprintf(fp_current, "        %-25s  %-6d   %5.1f    %.*le  %.*le   %.*le      %.*f        %.*f    %.*le\n",
                          tmp.c_str(), i + 1, list_phi[i] * RAD_to_DEG, gSIM_SIGDIGITS, list_j[i].first, gSIM_SIGDIGITS, list_jgal[i].first,
                          gSIM_SIGDIGITS, list_contrast[i].first, gSIM_SIGDIGITS, log10(list_j[i].first), gSIM_SIGDIGITS, log10(list_jgal[i].first), gSIM_SIGDIGITS, boost);
               else
                  fprintf(fp_current, "        %-25s  %-6d   %5.1f    %.*le  %.*le   %.*le      %.*f        %.*f\n",
                          tmp.c_str(), i + 1, list_phi[i] * RAD_to_DEG, gSIM_SIGDIGITS, list_j[i].first, gSIM_SIGDIGITS, list_jgal[i].first,
                          gSIM_SIGDIGITS, list_contrast[i].first, gSIM_SIGDIGITS, log10(list_j[i].first), gSIM_SIGDIGITS, log10(list_jgal[i].first));
            }
         }
      }
      // Print on screen sorted Jhalo and Jhalo/JGal.bkd values
      // N.B.: after this call, list_j are sorted by j values,
      //       and list_contrast by contrast values (their index
      //       is still available as these two objects are pairs).
      pop_study_sort_j(list_halos, list_j, list_contrast, list_boost, contrast_thresh);
   }

   if (gSIM_IS_PRINT) {
      fclose(fp[0]);
      cout << endl;
      cout << "_______________________" << endl << endl;
      cout << " ... output [ASCII] written in: " << f_name << endl;
      if (switch_y == 2) {
         fclose(fp[2]);
         cout << " ... output [ASCII] written in: " << f_name_diff << endl;
      }
      if (is_flux) {
         fclose(fp[3]);
         cout << " ... output [ASCII] written in: " << f_name_flux << endl;
      }
      if (is_list_halos && switch_y == 2) {
         fclose(fp_list[0]);
         cout << " ... output [ASCII] written in: " << f_list << " (#" << list_halos.size() << " halos)" << endl;
      }
   }

#if IS_ROOT

   TMultiGraph **multi = NULL;
   TLegend **leg = NULL;
   TLegend **leg_flux = NULL;
   TLegend **leg_list = NULL;
   TPaveText **txt = NULL;

   TGraph **gr_tot = NULL;
   TGraph **gr_sm = NULL;
   TGraph **gr_cl = NULL;
   TGraph **gr_cp = NULL;
   TGraph **gr_list = NULL;
   TGraph **gr_gamma = NULL;
   TGraph **gr_nu = NULL;

   TCanvas **c_gal = NULL;
   TCanvas **c_flux = NULL;

   TGraph *gr_popmean = NULL;
   TCanvas **c_pop = NULL;
   TH1D **h_pop = NULL;
   TGraphAsymmErrors **gr_pop = NULL;


   if (gSIM_IS_PRINT && !gSIM_IS_WRITE_ROOTFILES)
      cout << "_______________________" << endl << endl;

   // Save .root format?
   string f_root = gSIM_OUTPUT_DIR + "gal1D." + cvs_name + ".root";
   TFile *root_file = NULL;
   if (gSIM_IS_WRITE_ROOTFILES)
      root_file = new TFile(f_root.c_str(), "recreate");

   if (gSIM_IS_DISPLAY || gSIM_IS_WRITE_ROOTFILES) {
      bool is_logx = gSIM_IS_XLOG;
      bool is_logy = true;

      int iter = 1;
      if (switch_y == 2) iter = 2;

      multi = new TMultiGraph*[iter];
      leg = new TLegend*[iter];
      leg_flux = new TLegend*[iter];
      leg_list = new TLegend*[iter];
      txt = new TPaveText*[iter];

      gr_tot = new TGraph*[iter];
      gr_sm =  new TGraph*[iter];
      gr_cl =  new TGraph*[iter];
      gr_cp =  new TGraph*[iter];
      gr_gamma =  new TGraph*[iter];
      gr_nu =  new TGraph*[iter];

      c_gal =  new TCanvas*[iter];
      c_flux = new TCanvas*[iter];

      for (int i = 0; i < iter; ++i) {

         if (i == 1) {
            y_name = y_name_shell;
            y_name_flux = y_name_intensity;
         }
         // determine y-axis plotting range:
         double ymin = 1e40;
         double ymax = 1e-40;
         for (int k = 0; k < n_x; ++k) {
            if (y_sm[k] > ymax)  ymax = y_sm[k];
            if (y_cl[k] > ymax)  ymax = y_cl[k];
            if (y_cp[k] > ymax)  ymax = y_cp[k];
            if (y_tot[k] > ymax) ymax = y_tot[k];

            if (y_sm[k]  < ymin and y_sm[k] > 1e-40) ymin = y_sm[k];
            if (y_cl[k]  < ymin and y_cl[k] > 1e-40) ymin = y_cl[k];
            if (y_cp[k]  < ymin and y_cp[k] > 1e-40) ymin = y_cp[k];
            if (y_tot[k] < ymin and y_tot[k] > 1e-40) ymin = y_tot[k];
         }

         // TText related
         vector<string> text;
         bool is_text = false;

         // Set graph properties depending on switch_y
         if (switch_y == 1) {
            if (x[0] <= 1.e-40) is_logy = false;
         } else if (switch_y == 2) {
            char title[200];
            sprintf(title, "#alpha_{int} = %g^{o}", gSIM_ALPHAINT * RAD_to_DEG);
            if (i == 0) text.push_back(title);
            is_text = true;
         } else if (switch_y == 3) {
            sprintf(tmp, "#rho_{tot} = %s  #Rightarrow  M_{gal}=%.*le M_{#odot}", legend_for_profile(&par_galtot[2]).c_str(), gSIM_SIGDIGITS, m_gal);
            text.push_back(tmp);
            is_text = true;
         }

         // If Galactic clumps
         if (is_subs) {
            is_text = true;


            sprintf(tmp, "#rho_{tot} = %s  #Rightarrow  M_{gal} = %.*le M_{#odot}", legend_for_profile(&par_galtot[2]).c_str(), gSIM_SIGDIGITS, m_gal);
            text.push_back(tmp);

            sprintf(tmp, "%.*le", gSIM_SIGDIGITS, f_dm);
            f_dm = atof(tmp);
            sprintf(tmp, "Subs: f = %g with  M_{sub} #in [%g M_{#odot} , %g M_{gal}]", f_dm, gDM_SUBS_MMIN, gDM_SUBS_MMAXFRAC);
            text.push_back(tmp);

            sprintf(tmp, "  - dP/dV #propto %s", legend_for_profile(&par_galdpdv[2]).c_str());
            text.push_back(tmp);

            sprintf(tmp, "  - dP/dM #propto M^{-%g}", gMW_SUBS_DPDM_SLOPE);
            text.push_back(tmp);

            if (switch_y != 0) {
               sprintf(tmp, "  - #rho_{cl} = %s + c_{#Delta}(M_{#Delta}) = %s", legend_for_profile(&par_galsubs[0]).c_str(),
                       gNAMES_CDELTAMDELTA[gMW_SUBS_FLAG_CDELTAMDELTA]);
               text.push_back(tmp);
            }
         }

         if (switch_y == 0 or switch_y == 3) {
            is_text = true;
            sprintf(tmp, "#Delta_{crit}(z = %g) = %d", gSIM_REDSHIFT,  int(Delta_c));
            text.push_back(tmp);
            sprintf(tmp, "R_{#Delta}(z = %g) = %g kpc", gSIM_REDSHIFT, Rdelta);
            text.push_back(tmp);
            sprintf(tmp, "M_{#Delta}(z = %g) = %g M_{#odot} ", gSIM_REDSHIFT, Mdelta);
            text.push_back(tmp);
         }

         vector<double> xd;
         for (int k = 0; k < n_x; ++k) {
            if (switch_y == 1 || switch_y == 2) xd.push_back(x_deg[k]);
            else xd.push_back(x[k]);
         }

         if (gSIM_IS_WRITE_ROOTFILES)
            root_file->cd();

         multi[i] = new TMultiGraph();
         leg[i] = new TLegend(0.15, 0.15, 0.4, 0.35);

         leg[i]->SetFillColor(kWhite);
         leg[i]->SetTextSize(0.03);
         leg[i]->SetBorderSize(0);
         leg[i]->SetFillStyle(0);

         if (is_subs || switch_y == 3) {
            if (i == 0) gr_tot[i] = new TGraph(n_x, &xd[0], &y_tot[0]);
            else gr_tot[i] = new TGraph(n_x, &xd[0], &y_tot_shell[0]);
            gr_tot[i]->SetName("tot");
            gr_tot[i]->SetLineColor(kBlack);
            gr_tot[i]->SetLineWidth(2);
            gr_tot[i]->SetLineStyle(1);
            gr_tot[i]->SetTitle("tot");
            gr_tot[i]->GetXaxis()->SetTitle(x_name.c_str());
            gr_tot[i]->GetYaxis()->SetTitle(y_name.c_str());
            multi[i]->Add(gr_tot[i], "L");
            leg[i]->AddEntry(gr_tot[i], "Total", "L");
            if (gSIM_IS_WRITE_ROOTFILES)
               gr_tot[i]->Write();
         }

         if (i == 0) gr_sm[i] =  new TGraph(n_x, &xd[0], &y_sm[0]);
         else gr_sm[i] =  new TGraph(n_x, &xd[0], &y_sm_shell[0]);
         gr_sm[i]->SetName("smooth");
         gr_sm[i]->SetLineColor(kBlue);
         gr_sm[i]->SetLineWidth(3);
         gr_sm[i]->SetLineStyle(2);
         gr_sm[i]->SetTitle("smooth");
         gr_sm[i]->GetXaxis()->SetTitle(x_name.c_str());
         gr_sm[i]->GetYaxis()->SetTitle(y_name.c_str());
         if (switch_y != 3) {
            multi[i]->Add(gr_sm[i], "L");
            leg[i]->AddEntry(gr_sm[i], "Gal. smooth", "L");
            if (gSIM_IS_WRITE_ROOTFILES)
               gr_sm[i]->Write();
         }

         if (is_subs && switch_y != 3) {
            if ((switch_y != 0) && gPP_DM_IS_ANNIHIL_OR_DECAY) {
               if (i == 0) gr_cp[i] = new TGraph(n_x, &xd[0], &y_cp[0]);
               else gr_cp[i] = new TGraph(n_x, &xd[0], &y_cp_shell[0]);
               gr_cp[i]->SetName("crossprod");
               gr_cp[i]->SetLineColor(kGreen + 1);
               gr_cp[i]->SetLineWidth(3);
               gr_cp[i]->SetLineStyle(6);
               gr_cp[i]->SetTitle("crossprod");
               gr_cp[i]->GetXaxis()->SetTitle(x_name.c_str());
               gr_cp[i]->GetYaxis()->SetTitle(y_name.c_str());
               multi[i]->Add(gr_cp[i], "L");
               leg[i]->AddEntry(gr_cp[i], "Gal. cross-prod.", "L");
               if (gSIM_IS_WRITE_ROOTFILES)
                  gr_cp[i]->Write();

            }
            if (i == 0) gr_cl[i] = new TGraph(n_x, &xd[0], &y_cl[0]);
            else gr_cl[i] = new TGraph(n_x, &xd[0], &y_cl_shell[0]);
            gr_cl[i]->SetName("clump");
            gr_cl[i]->SetLineColor(kRed);
            gr_cl[i]->SetLineWidth(3);
            gr_cl[i]->SetLineStyle(3);
            gr_cl[i]->SetTitle("clump");
            gr_cl[i]->GetXaxis()->SetTitle(x_name.c_str());
            gr_cl[i]->GetYaxis()->SetTitle(y_name.c_str());
            multi[i]->Add(gr_cl[i], "L");
            leg[i]->AddEntry(gr_cl[i], "Gal. <subs>", "L");
            if (gSIM_IS_WRITE_ROOTFILES)
               gr_cl[i]->Write();
         }


         int nphi = 20;
         vector<double> phi;
         vector<double> jpopphi_mean;
         for (int n = 0; n < nphi; ++n) {
            phi.push_back(n * PI / double(nphi));
            jpopphi_mean.push_back(-1.e-40);
         }

         if (is_list_halos && switch_y == 2 && i == 0) {
            leg_list[i] = new TLegend(0.75, 0.65, 0.9, 0.95);
            leg_list[i]->SetFillColor(kWhite);
            leg_list[i]->SetTextSize(0.03);
            leg_list[i]->SetBorderSize(0);
            leg_list[i]->SetFillStyle(0);

            gr_list = new TGraph*[(int)list_halos.size()];
            int n_j = (int) list_contrast.size();
            for (int j = n_j - 1; j >= 0; --j) {
               // Index k in list of halos
               int k = list_contrast[j].second;
               // Find corresponding index l in list_j
               int l = 0;
               for (int m = 0; m < n_j; ++m) {
                  l = m;
                  if (list_j[m].second == k)
                     break;
               }

               gr_list[j] = new TGraph(1);
               gr_list[j]->SetName(list_name[k].c_str());
               gr_list[j]->SetTitle(list_name[k].c_str());
               gr_list[j]->SetPoint(0, list_phi[k] * RAD_to_DEG, list_j[l].first);

               // If not many objects in list => different symbols for each object
               if ((int)list_halos.size() < 50) {
                  gr_list[j]->SetMarkerStyle(rootmarker(n_j - j));
                  gr_list[j]->SetMarkerColor(rootcolor(n_j - j));
                  gr_list[j]->SetMarkerSize(1.2);
                  leg_list[i]->AddEntry(gr_list[j], list_name[k].c_str(), "P");
               } else {
                  // If many objects => different symbols only for more contrasted
                  // (those with contrast > contrast_thresh
                  if (list_contrast[j].first > contrast_thresh) {
                     gr_list[j]->SetMarkerStyle(rootmarker(n_j - j));
                     gr_list[j]->SetMarkerColor(rootcolor(n_j - j));
                     gr_list[j]->SetMarkerSize(1.2);
                     leg_list[i]->AddEntry(gr_list[j], list_name[k].c_str(), "P");
                  } else {
                     gr_list[j]->SetMarkerStyle(2);
                     gr_list[j]->SetMarkerColor(kBlack);
                     gr_list[j]->SetMarkerSize(1.);
                  }
               }
               gr_list[j]->GetXaxis()->SetTitle(x_name.c_str());
               gr_list[j]->GetYaxis()->SetTitle(y_name.c_str());
               if (gSIM_IS_WRITE_ROOTFILES)
                  gr_list[j]->Write();
               multi[i]->Add(gr_list[j], "P");

               // Find n(phi) of current halo
               int nphi_found = int((list_phi[k] * RAD_to_DEG) / (180. / (double)nphi));
               jpopphi_mean[nphi_found] += list_j[l].first;
            }

            // Rescale by the ratio of solid angles to get the correct Jmean
            //   - all object in psi bin: Omega_obj =  2PI*(cos(phi)-cos(phi+alpha_int))
            //   - current observed Omega: Omega = 2PI(1-cos(alpha_int))
            for (int n = nphi - 1; n >= 0; --n) {
               if (jpopphi_mean[n] < 1.e-40) {
                  phi.erase(phi.begin() + n);
                  jpopphi_mean.erase(jpopphi_mean.begin() + n);
               } else {
                  double ratio_omega = fabs((cos(phi[n]) - cos(phi[n] + gSIM_ALPHAINT)) / (1. - cos(gSIM_ALPHAINT)));
                  jpopphi_mean[n] /= ratio_omega;
               }
            }
            gr_popmean = new TGraph((int)phi.size(), &phi[0], &jpopphi_mean[0]);

            // If list from population large enough, calculate <J> from pop (angular bins)
            if ((int)list_halos.size() > 1000) {
               gr_popmean->SetName("jpop_mean");
               gr_popmean->SetMarkerStyle(21);
               gr_popmean->SetMarkerColor(kYellow + 1);
               gr_popmean->SetMarkerSize(1.2);
               gr_popmean->SetLineStyle(9);
               gr_popmean->SetLineColor(kYellow + 1);
               gr_popmean->SetLineWidth(1);
               gr_popmean->GetXaxis()->SetTitle(x_name.c_str());
               gr_popmean->GetYaxis()->SetTitle(y_name.c_str());
               if (gSIM_IS_WRITE_ROOTFILES)
                  gr_popmean->Write();
               multi[i]->Add(gr_popmean, "LP");
               leg[i]->AddEntry(gr_popmean, "<Jpop>", "LP");
            }
         }
         // Fluxes
         if (is_flux) {
            leg_flux[i] = new TLegend(0.15, 0.5, 0.65, 0.95);
            leg_flux[i]->SetFillColor(kWhite);
            leg_flux[i]->SetTextSize(0.03);
            leg_flux[i]->SetBorderSize(0);
            leg_flux[i]->SetFillStyle(0);
            string leg_gamma = legend_for_spectrum(par_spec, false);
            leg_flux[i]->SetHeader(leg_gamma.c_str());

            if (i == 0) {
               gr_gamma[i] = new TGraph(n_x, &xd[0], &flux_gamma[0]);
               gr_nu[i] = new TGraph(n_x, &xd[0], &flux_nu[0]);
            } else {
               gr_gamma[i] = new TGraph(n_x, &xd[0], &intensity_gamma[0]);
               gr_nu[i] = new TGraph(n_x, &xd[0], &intensity_nu[0]);
            }

            char tmp_range[500];

            if (gSIM_FLUX_IS_INTEG_OR_DIFF) {
               if (i == 0) sprintf(tmp_range, "Integrated flux in [%g, %g] GeV and area #Delta#Omega = 2#pi(1-cos(#alpha_{int}))", gSIM_FLUX_EMIN_GEV, gSIM_FLUX_EMAX_GEV);
               else if (i == 1) sprintf(tmp_range, "Integrated intensity in [%g, %g] GeV", gSIM_FLUX_EMIN_GEV, gSIM_FLUX_EMAX_GEV);
               TLegendEntry *entry = leg_flux[i]->AddEntry((TObject *)0, tmp_range, "h");
               entry->SetTextColor(kBlack);
               gr_gamma[i]->SetName("flux_gamma_integr");
               gr_gamma[i]->SetTitle("");
               gr_gamma[i]->SetLineColor(kAzure);
               gr_gamma[i]->SetLineWidth(3);
               gr_gamma[i]->SetLineStyle(1);
               gr_gamma[i]->GetXaxis()->SetTitle(x_name.c_str());
               gr_gamma[i]->GetYaxis()->SetTitle(y_name_flux.c_str());
               entry = leg_flux[i]->AddEntry(gr_gamma[i], "#gamma-rays", "L");
               entry->SetTextColor(kBlack);
               if (gSIM_IS_WRITE_ROOTFILES && switch_y != 0)
                  gr_gamma[i]->Write();

               gr_nu[i]->SetName("flux_nu_integr");
               gr_nu[i]->SetTitle("");
               gr_nu[i]->SetLineColor(kAzure);
               gr_nu[i]->SetLineWidth(3);
               gr_nu[i]->SetLineStyle(7);
               gr_nu[i]->GetXaxis()->SetTitle(x_name.c_str());
               gr_nu[i]->GetYaxis()->SetTitle(y_name_flux.c_str());

               if (gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_EW || gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_NOEW) {
                  string tmp_leg = "#nu (" + (string)gNAMES_NUFLAVOUR[gSIM_FLUX_FLAG_NUFLAVOUR] + ")";
                  entry = leg_flux[i]->AddEntry(gr_nu[i], tmp_leg.c_str(), "L");
                  entry->SetTextColor(kBlack);
                  if (gSIM_IS_WRITE_ROOTFILES && switch_y != 0)
                     gr_nu[i]->Write();
               }
            } else {
               if (i == 0) sprintf(tmp_range, "Differential flux at E = %g GeV  and area #Delta#Omega = 2#pi(1-cos(#alpha_{int}))", gSIM_FLUX_AT_E_GEV);
               else if (i == 1) sprintf(tmp_range, "Differential intensity at E = %g GeV", gSIM_FLUX_AT_E_GEV);
               TLegendEntry *entry = leg_flux[i]->AddEntry((TObject *)0, tmp_range, "h");
               entry->SetTextColor(kBlack);
               gr_gamma[i]->SetName("flux_gamma");
               gr_gamma[i]->SetTitle("");
               gr_gamma[i]->SetLineColor(kRed);
               gr_gamma[i]->SetLineWidth(3);
               gr_gamma[i]->SetLineStyle(1);
               gr_gamma[i]->GetXaxis()->SetTitle(x_name.c_str());
               gr_gamma[i]->GetYaxis()->SetTitle(y_name_flux.c_str());
               leg_flux[i]->AddEntry(gr_gamma[i], "#gamma-rays", "L");
               if (gSIM_IS_WRITE_ROOTFILES && switch_y != 0)
                  gr_gamma[i]->Write();

               gr_nu[i]->SetName("flux_nu");
               gr_nu[i]->SetTitle("");
               gr_nu[i]->SetLineColor(kRed);
               gr_nu[i]->SetLineWidth(3);
               gr_nu[i]->SetLineStyle(7);
               gr_nu[i]->GetXaxis()->SetTitle(x_name.c_str());
               gr_nu[i]->GetYaxis()->SetTitle(y_name_flux.c_str());

               if (gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_EW || gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_NOEW) {
                  string tmp_leg = "#nu (" + (string)gNAMES_NUFLAVOUR[gSIM_FLUX_FLAG_NUFLAVOUR] + ")";
                  leg_flux[i]->AddEntry(gr_nu[i], tmp_leg.c_str(), "L");
                  if (gSIM_IS_WRITE_ROOTFILES && switch_y != 0)
                     gr_nu[i]->Write();
               }
            }
         }

         // Draw canvas
         string tmp_cvs = cvs_name;
         if (i == 1) tmp_cvs += "_shell";

         c_gal[i] = new TCanvas(tmp_cvs.c_str(), tmp_cvs.c_str(), 0, i * 550, 650, 500);
         c_gal[i]->SetLogx(is_logx);
         c_gal[i]->SetLogy(is_logy);
         c_gal[i]->SetGridx(0);
         c_gal[i]->SetGridy(0);

         // Plot graphs and legends
         multi[i]->Draw("A");
         gSIM_CLUMPYAD->Draw();
         multi[i]->GetXaxis()->SetTitle(x_name.c_str());
         multi[i]->GetYaxis()->SetTitle(y_name.c_str());
         gPad->SetTickx(1);
         gPad->SetTicky(1);
         leg[i]->Draw("SAME");
         if (is_list_halos && switch_y == 2 && i == 0)
            leg_list[i]->Draw("SAME");

         // Set y-range:
         if (ymin <  1e-9 * ymax)
            ymin =  1e-9 * ymax;
         if (i == 0) multi[i]->SetMaximum(1.5 * ymax);
         if (i == 0) multi[i]->SetMinimum(0.3 * ymin);

         // Text if required

         if (is_subs) {
            txt[i] = new TPaveText(0.2, 0.63, 0.5, 0.93, "NDC");
            txt[i]->SetTextSize(0.03);
         } else {
            txt[i] = new TPaveText(0.2, 0.73, 0.5, 0.93, "NDC");
            txt[i]->SetTextSize(0.04);
         }
         txt[i]->SetTextFont(132);
         txt[i]->SetFillColor(0);
         txt[i]->SetBorderSize(0);
         txt[i]->SetTextColor(kGray + 2);
         txt[i]->SetFillStyle(0);
         txt[i]->SetTextAlign(12);
         for (int ii = 0; ii < (int)text.size(); ++ii)
            txt[i]->AddText(text[ii].c_str());
         if (is_text) txt[i]->Draw();

         c_gal[i]->Update();
         c_gal[i]->Modified();

         if (gSIM_IS_WRITE_ROOTFILES) {
            root_file->cd();
            gPad->Write();
         }

         // Fluxes
         if (is_flux) {
            string flux_or_intensity = "Fluxes";
            if (i == 1) flux_or_intensity = "Intensities";
            c_flux[i] = new TCanvas(flux_or_intensity.c_str(), flux_or_intensity.c_str(), 600, i * 550, 650, 500);
            c_flux[i]->SetLogx(is_logx);
            c_flux[i]->SetLogy(is_logy);
            c_flux[i]->SetGridx(0);
            c_flux[i]->SetGridy(0);

            // Plot graphs and legends
            gr_gamma[i]->Draw("AL");
            gPad->SetTickx(1);
            gPad->SetTicky(1);
            gr_gamma[i]->GetXaxis()->SetTitle(x_name.c_str());
            gr_gamma[i]->GetYaxis()->SetTitle(y_name_flux.c_str());

            if (gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_EW || gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_NOEW)
               gr_nu[i]->Draw("LSAME");

            gSIM_CLUMPYAD->Draw();

            leg_flux[i]->Draw("SAME");
            if (gSIM_IS_WRITE_ROOTFILES) {
               root_file->cd();
               gPad->Write();
            }
         }

         // Population study (plots)
         if (is_list_halos && switch_y == 2 && i == 0)
            pop_study_plots(list_halos, list_j, list_contrast, list_boost, c_pop, h_pop, gr_pop, phi_cut_deg, "pop");
      }

      if (gSIM_IS_WRITE_ROOTFILES) {
         cout << " ... output [use ROOT TBrowser] written in: " << f_root << endl;
      }

      if (gSIM_IS_PRINT)
         cout << "_______________________" << endl << endl;

      if (gSIM_IS_DISPLAY)
         gSIM_ROOTAPP->Run(kTRUE);

      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->Close();
         delete root_file;
      }

      // Free memory

      if (leg) delete[] leg;
      leg = NULL;
      if (multi) delete[] multi;
      multi = NULL;
      if (c_gal) delete[] c_gal;
      c_gal = NULL;

      if (gr_gamma) delete[] gr_gamma;
      if (gr_nu) delete[] gr_nu;
      if (c_flux) delete[] c_flux;
      if (leg_flux) delete[] leg_flux;

      if (txt) delete[] txt;
      txt = NULL;
      if (leg_list) delete[] leg_list;
      leg_list = NULL;
      if (c_pop) delete[] c_pop;
      c_pop = NULL;
      if (h_pop) delete[] h_pop;
      h_pop = NULL;
      if (gr_pop) delete[] gr_pop;
      gr_pop = NULL;
      if (gr_list) delete[] gr_list;
      gr_list = NULL;
   }
#endif
   gSIM_ALPHAINT = old_alpha_aperture;
}

//______________________________________________________________________________
void gal_j2D(double &psi, string const &theta_deg_str,
             string const &theta_orth_size_deg_str, double &dtheta, int switch_j,
             double const &user_rse)
{
   //--- Creates a fov_deg x fov_deg skymap around direction (psi_deg, theta_deg)
   // with all ingredients together (smooth + analytical continuous
   // clump flux + drawn clump when needed)
   //  param_file     Clumpy parameters
   //  psi_deg        Galactic longitude of the l.o.s [deg]
   //  theta_deg      Galactic latitude of the l.o.s [deg]
   //  theta_orth_size_deg   Dimension of the Skymap in great-circle direction orthogonal to theta [deg]
   //  theta_size_deg Dimension of the Skymap in theta-direction [deg]
   //  switch_j       Skymap to calculate
   //                    0: sm + <sub> + cross-prod
   //                    1: sm + <sub> + cross-prod + list
   //                    2: sm + <sub> + cross-prod + drawn
   //                    3: sm + <sub> + cross-prod + drawn + list
   //  user_rse       Relative standard error allowed (subclumps drawn vs mean value) [only if switch_j=2 or 3]


   // Set ROOT style: do not set it! Mess with the colour code!
   //rootstyle_set2CLUMPY();

   char char_tmp[512];

   char j_or_d = 'D';
   if (gPP_DM_IS_ANNIHIL_OR_DECAY)
      j_or_d = 'J';

   bool is_subs = true;
   if (gMW_SUBS_N_INM1M2 == 0)
      is_subs = false;

   bool is_subs_drawn = false;
   if (switch_j == 2 || switch_j == 3) {
      is_subs_drawn = true;
#if !IS_ROOT
      printf("\n====> ERROR: gal_j2D() in janalysis.cc");
      printf("\n             You need ROOT to be installed to draw random clumps");
      printf("\n             from a multivariate probability distribution.");
      printf("\n             => abort()\n\n");
      abort();
#endif
   }

   bool is_list_added = false;
   if (switch_j == 1 || switch_j == 3)
      is_list_added = true;

   string out_name = "gal2D";
   if (gPP_DM_IS_ANNIHIL_OR_DECAY)
      out_name = "annihil_" + out_name;
   else
      out_name = "decay_" + out_name;


   // Direction around which the map is built [psi_deg,theta_deg in degrees]
   double theta;
   double dtheta_orth; // initialise here because theta_orth_size_deg_str is still a string.


   string grid_mode = "RECT";

   if (theta_deg_str == "s") {
      grid_mode = "STRIP";
      dtheta_orth = atof(theta_orth_size_deg_str.c_str()) * DEG_to_RAD;
      theta = 0.;
   } else {
      theta = atof(theta_deg_str.c_str()) * DEG_to_RAD;
   }
   if (theta_orth_size_deg_str == "d") {
      grid_mode = "DISK";
      dtheta_orth = dtheta;
      // theta_size_deg itself will be corrected later to real range of theta values on the sphere.
   } else {
      dtheta_orth = atof(theta_orth_size_deg_str.c_str()) * DEG_to_RAD;
   }
   if (theta_deg_str == "s" && theta_orth_size_deg_str == "d") {
      printf("\n====> ERROR: gal_j2D() in janalysis.cc");
      printf("\n             Cannot select STRIP mode and DISK mode at the same time.");
      printf("\n             => abort()\n\n");
      abort();
   }

   //--- print information about map dimensions
   printf("            ______________________________________________________\n\n");
   if (grid_mode == "DISK") {
      printf("                 Skymap in direction (l,b) = (%.1f,%.1f) [deg]\n", psi * RAD_to_DEG, theta * RAD_to_DEG);
      printf("                   circular FOV with diameter = %.1f [deg].\n", dtheta * RAD_to_DEG);
      if (fabs(theta) > PI / 2) {
         printf("\n====> ERROR: gal_j2D() in janalysis.cc");
         printf("\n             FOV center exceeding |theta|>90 [deg] makes no sense.");
         printf("\n             => abort()\n\n");
         abort();
      }
      if (fabs(dtheta) > 2. * PI) {
         printf("\n====> ERROR: gal_j2D() in janalysis.cc");
         printf("\n             Disk diameter |d|>360 [deg] makes no sense.");
         printf("\n             => abort()\n\n");
         abort();
      }
   } else if (grid_mode == "STRIP")  {
      printf("        Skymap around galactic plane in direction (l,b) = (%.1f,0) [deg]\n", psi * RAD_to_DEG);
      printf("                             FOV  = [%.1fx%.1f]\n", atof(theta_orth_size_deg_str.c_str()), dtheta * RAD_to_DEG);
      if (fabs(dtheta_orth) > 2. * PI || fabs(dtheta) > PI) {
         printf("\n====> ERROR: gal_j2D() in janalysis.cc");
         printf("\n             Strip exceeding |dpsi|>360 [deg] or |dtheta|>180 [deg] causes troubles.");
         printf("\n             => abort()\n\n");
         abort();
      }
   } else if (grid_mode == "RECT")  {
      printf("                 Skymap in direction (l,b) = (%.1f,%.1f) [deg]\n", psi * RAD_to_DEG, theta * RAD_to_DEG);
      printf("                          rectangular FOV = [%.1fx%.1f]\n", atof(theta_orth_size_deg_str.c_str()), dtheta * RAD_to_DEG);
      if (fabs(theta) > PI / 2.) {
         printf("\n====> ERROR: gal_j2D() in janalysis.cc");
         printf("\n             FOV center exceeding |theta|>90 [deg] makes no sense.");
         printf("\n             => abort()\n\n");
         abort();
      }
      if (fabs(dtheta) == PI && fabs(dtheta_orth) == PI) {
         // do nothing; this is ok, it will be accounted for in geometry.cc, set_healpix_fov()
      } else if (fabs(dtheta) >= PI || fabs(dtheta_orth) >= PI) {
         printf("\n====> ERROR: gal_j2D() in janalysis.cc");
         printf("\n             Rectangular grid for |dtheta_orth| or |dtheta| >= 180 [deg] not supported");
         printf("\n             (the exception |dtheta_orth| = |dtheta| = 180 [deg] is possible.)");
         printf("\n             N.B.: if you want to draw a full-sky map, choose FOV dimensions either by");
         printf("\n               - RING mode:  theta_orth_size = d, theta_size = 360.");
         printf("\n               - STRIP mode: theta_obs = s, theta_orth_size = 360., theta_size = 180.");
         printf("\n             Both ways are completely equivalent. See documentation for further details.\n\n");
         printf("\n             => abort()\n\n");
         abort();
      }
   }
   printf("            ______________________________________________________\n\n");

   if (dtheta_orth < 0. || dtheta < 0.) {
      print_warning("janalysis.cc", "gal_j2D()", "Negative FOV dimensions -> FOV is inverted (exclusion region)");
   }

   /********************** INITIALIZE HEALPIX GRID/FOV  ***********************/

   int n_pix;          // number of pixels on the whole sphere
   double delta_omega; // surface area of each pixel on the sphere = J-factor integration area
   int smooth_gamma_or_nu_or_both;  // will the output be smoothed by an instrumental beam
   // and if yes, by which one?
   hp_set_resolution(n_pix, delta_omega, smooth_gamma_or_nu_or_both);

   // For the grid creation, we also have to know if there will be a smoothing
   // done later, because in this case, the grid for the smooth contributions
   // will be extended a bit. Smoothing of the maps will only be performed if
   //  - a beam width is provided in the input file (smooth_gamma_or_nu_or_both != 0)
   //  - substructures are drawn (is_subs_drawn = true or is_list_added = true)
   //  combine these conditions to a single one:

   if (smooth_gamma_or_nu_or_both == 0) {
      cout << "      No additional smoothing of map by instrumental beam is applied.\n" << endl;
   } else if (smooth_gamma_or_nu_or_both == 1) {
      cout << "      Output map will be smoothed by gSIM_GAUSSBEAM_GAMMA_FWHM_DEG = "
           << gSIM_GAUSSBEAM_GAMMA_FWHM *RAD_to_DEG << " [deg]," << endl;
      cout << "      and the result will be stored in the 3rd extension of the output FITS file.\n" << endl;
   } else if (smooth_gamma_or_nu_or_both == 2) {
      cout << "      Output map will be smoothed by gSIM_GAUSSBEAM_NEUTRINO_FWHM_DEG = "
           << gSIM_GAUSSBEAM_NEUTRINO_FWHM *RAD_to_DEG << " [deg]," << endl;
      cout << "      and the result will be stored in the 3rd extension of the output FITS file.\n" << endl;
   } else if (smooth_gamma_or_nu_or_both == 3) {
      cout << "      Output map will be smoothed by gSIM_GAUSSBEAM_GAMMA_FWHM_DEG = "
           << gSIM_GAUSSBEAM_GAMMA_FWHM *RAD_to_DEG << " [deg]" << endl;
      cout << "      and separately smoothed by gSIM_GAUSSBEAM_NEUTRINO_FWHM_DEG = "
           << gSIM_GAUSSBEAM_NEUTRINO_FWHM *RAD_to_DEG << " [deg]," << endl;
      cout << "      and both results will be stored in the 3rd extension of the output FITS file.\n" << endl;
      if (gSIM_GAUSSBEAM_GAMMA_FWHM > gSIM_GAUSSBEAM_NEUTRINO_FWHM) {
         sprintf(char_tmp, "Your neutrino instrument resolution (%.2le deg) is better "
                 "than your gamma resolution (%.2le deg) => Is this really what you want?",
                 gSIM_GAUSSBEAM_NEUTRINO_FWHM * RAD_to_DEG, gSIM_GAUSSBEAM_GAMMA_FWHM * RAD_to_DEG);
         print_warning("janalysis.cc", "gal_j2D()", string(char_tmp));
      }
   }

   // the following function now creates the FOV:
   rangeset<int> rs_pix_fov = hp_set_pix_fov(grid_mode, psi,  theta, dtheta_orth,
                              dtheta, gSIM_HEALPIX_NSIDE, gSIM_HEALPIX_SCHEME) ;

   vector<int> v_pix_fov;
   rs_pix_fov.toVector(v_pix_fov);
   int n_pix_fov = rs_pix_fov.nval(); // number of pixels in the FOV.
   T_Healpix_Base<int> hp_gridprop(gSIM_HEALPIX_NSIDE, gSIM_HEALPIX_SCHEME, SET_NSIDE) ;

   double fsky = double(n_pix_fov) / double(n_pix) ; // fraction of sky covered by FOV

   cout << ">>>>> N.B.: FOV dimensions with NSIDE = " << gSIM_HEALPIX_NSIDE
        << "   =>   " << n_pix_fov << " pixels." << endl;
   cout << "   => surface of the FOV: " << double(n_pix_fov) * delta_omega << " sr." << endl;
   cout << "   => fraction of sky covered: " << fsky * 100. << "%\n" << endl;

   // if there will be a smoothing of the maps,
   // additionally create jgal_smooth/subs/cross_continuum on a bigger grid:
   double dtheta_orth_extended ;
   double dtheta_extended;
   rangeset<int> rs_pix_fov_extended;
   vector<int> v_pix_fov_extended ;
   int n_pix_fov_extended = 0;
   string grid_mode_extended = grid_mode ;

   if (smooth_gamma_or_nu_or_both != 0 && n_pix_fov != n_pix) {
      double grid_extension = 5. * max(gSIM_GAUSSBEAM_GAMMA_FWHM, gSIM_GAUSSBEAM_NEUTRINO_FWHM);
      do_safegridextension(dtheta_orth, dtheta, grid_extension, grid_mode_extended,
                           dtheta_orth_extended, dtheta_extended) ;
      rs_pix_fov_extended = hp_set_pix_fov(grid_mode_extended, psi,  theta,  dtheta_orth_extended,  dtheta_extended,
                                           gSIM_HEALPIX_NSIDE, gSIM_HEALPIX_SCHEME) ;
      rs_pix_fov_extended.toVector(v_pix_fov_extended);
      n_pix_fov_extended = rs_pix_fov_extended.nval(); // number of pixels in the FOV.
      cout << "   => size of extended grid for proper smoothing: " << n_pix_fov_extended << " pixels.\n" << endl;
   }

   /************************* GET GRID DIMENSIONS  ****************************/

   // now, as the FOV is created, we can determine the FOV dimensions dpsi in psi direction
   // and the coordinate range dtheta_coord, which may differ from the grid size dtheta.
   double dpsi ;
   double psi_raw_min = 2. * PI + SMALL_NUMBER  ; // psi in [0, 2PI]
   double psi_raw_max = 0 - SMALL_NUMBER ;
   double psi_checked_min = PI + SMALL_NUMBER ; // psi in [- PI, PI]
   double psi_checked_max = - PI - SMALL_NUMBER ;
   double dtheta_coord ;
   double theta_max = - PI / 2. - SMALL_NUMBER;
   double theta_min = + PI / 2. + SMALL_NUMBER;
   pointing ptg_i_pix ;


   for (int i = 0; i < n_pix_fov; ++i) {
      ptg_i_pix = hp_gridprop.pix2ang(v_pix_fov[i]) ;
      double psi_current = ptg_i_pix.phi ;
      if (psi_current < psi_raw_min) psi_raw_min = psi_current ;
      if (psi_current > psi_raw_max) psi_raw_max = psi_current ;
      check_psi(psi_current) ;
      if (psi_current < psi_checked_min) psi_checked_min = psi_current ;
      if (psi_current > psi_checked_max) psi_checked_max = psi_current ;

      double theta_current = PI / 2. - ptg_i_pix.theta ;
      if (theta_current <  theta_min) theta_min = theta_current;
      if (theta_current >  theta_max) theta_max = theta_current;
   }

   // determine delta_psi at the latitude theta as reference needed later:
   int n_pix_ring;
   int startpix ;
   bool shift ;
   pointing ptg_gridcenter = pointing(PI / 2. - theta, psi);
   int i_ring = hp_gridprop.pix2ring(hp_gridprop.ang2pix(ptg_gridcenter));
   hp_gridprop.get_ring_info_small(i_ring, startpix, n_pix_ring, shift);
   double delta_psi_fovcenter = 2. * PI / n_pix_ring;

   // now determine dpsi, by also checking delicate cases when FOV crosses psi = PI or psi = 0.:
   if (theta == 0.) {
      dpsi = dtheta_orth ;
   } else {
      if (fabs(psi_raw_max - psi_raw_min - psi_checked_max + psi_checked_min) < gSIM_EPS) {
         dpsi = psi_checked_max - psi_checked_min + delta_psi_fovcenter / 2;
      } else {
         if (theta - dtheta / 2. < - PI / 2 || theta + dtheta / 2. > PI / 2) {
            dpsi = 2. * PI ;
         } else if (psi_checked_max - psi_checked_min < 2.* PI - delta_psi_fovcenter) {
            // FOV crosses psi = 0.
            dpsi = psi_checked_max - psi_checked_min + delta_psi_fovcenter / 2 ;
         } else if (psi_raw_max - psi_raw_min < 2.* PI - delta_psi_fovcenter) {
            // FOV crosses psi = PI:
            dpsi = psi_raw_max - psi_raw_min + delta_psi_fovcenter / 2 ;
         } else {
            printf("\n====> ERROR: gal_j2D() in janalysis.cc");
            printf("\n             This is a bug...");
            printf("\n                psi_raw_min: %le [deg]", psi_raw_min * RAD_to_DEG);
            printf("\n                psi_raw_max: %le [deg]", psi_raw_max * RAD_to_DEG);
            printf("\n                psi_checked_min: %le [deg]", psi_checked_min * RAD_to_DEG);
            printf("\n                psi_checked_max: %le [deg]", psi_checked_max * RAD_to_DEG);
            printf("\n             => abort()\n\n");
            abort();
         }
      }
   }
   if (dpsi > 2. * PI) dpsi = 2. * PI;

   double psi_min = psi - dpsi / 2.; //rad
   double psi_max = psi + dpsi / 2.; //rad

   // now, what about dtheta_coord, theta_min and theta_max?
   // if the FOV does not cross the poles, it's easy and overwrite theta_min, theta_max calculated before:
   // but there is an exception for the RECT mode, where dtheta_coord gets slightly bigger for large FOV:
   if (dtheta > 0. && dtheta_orth > 0. && theta - dtheta / 2. >= - PI / 2 && theta + dtheta / 2. <= PI / 2) {
      if (theta - dtheta / 2. < theta_min) theta_min = theta - dtheta / 2. ;   // rad
      if (theta + dtheta / 2. > theta_max) theta_max = theta + dtheta / 2. ;   // rad
   }
   // if not, for the DISK mode, we can get the result also easily with a simple expression:
   else if (dtheta > 0. && dtheta_orth > 0. && grid_mode == "DISK") {
      if (theta - dtheta / 2. >= - PI / 2) {
         theta_min = theta - dtheta / 2. ; // rad
      } else theta_min = - PI / 2. ;
      if (theta + dtheta / 2. <= PI / 2.) {
         theta_max = theta + dtheta / 2. ; // rad
      } else theta_max = + PI / 2. ;
   }

   // in all other cases (inclusive inverse mode), stay with the values theta_min, theta_max calculated
   // before and get:
   if ((dtheta < 0. || dtheta_orth < 0.) && fabs(dtheta_orth) < PI) dtheta_coord = PI ;
   else dtheta_coord = theta_max - theta_min ;

   /********************* OUTPUT FILE CREATION AND CHECK  *********************/

   string filename_core_str ;
   char f_name[500];

   if (is_subs_drawn) {
      if (grid_mode == "DISK")
         sprintf(f_name, "%s_LOS%.0f_%.0f_FOVdiameter%.1fdeg_nside%d",
                 out_name.c_str(), psi * RAD_to_DEG, theta * RAD_to_DEG, dtheta * RAD_to_DEG,
                 gSIM_HEALPIX_NSIDE);
      else
         sprintf(f_name, "%s_LOS%.0f_%.0f_FOV%.0fx%.0f_nside%d",
                 out_name.c_str(), psi * RAD_to_DEG, theta * RAD_to_DEG, dtheta_orth * RAD_to_DEG, dtheta * RAD_to_DEG,
                 gSIM_HEALPIX_NSIDE);
   } else {
      if (grid_mode == "DISK")
         sprintf(f_name, "%s_LOS%.0f_%.0f_FOVdiameter%.1fdeg_nside%d",
                 out_name.c_str(), psi * RAD_to_DEG, theta * RAD_to_DEG, dtheta * RAD_to_DEG,
                 gSIM_HEALPIX_NSIDE);
      else
         sprintf(f_name, "%s_LOS%.0f_%.0f_FOV%.0fx%.0f_nside%d",
                 out_name.c_str(), psi * RAD_to_DEG, theta * RAD_to_DEG, dtheta_orth * RAD_to_DEG, dtheta * RAD_to_DEG,
                 gSIM_HEALPIX_NSIDE);

   }
   filename_core_str.append(f_name);

   string filename_fits_str = gSIM_OUTPUT_DIR + filename_core_str + ".fits";
   string filename_root_str = gSIM_OUTPUT_DIR + filename_core_str + ".root";
   string filename_list_str = gSIM_OUTPUT_DIR + filename_core_str + ".list";
   string filename_drawn_str = gSIM_OUTPUT_DIR + filename_core_str + ".drawn";

   string powspecfilename_str = gSIM_OUTPUT_DIR + "powerspec_" + filename_core_str + ".fits";
   string popstudy_drawn_filename_str = gSIM_OUTPUT_DIR + "popstudy_drawn_" + filename_core_str + ".root";
   string popstudy_list_filename_str = gSIM_OUTPUT_DIR + "popstudy_list_" + filename_core_str + ".root";

   fitshandle fh_file_out;
   const char *filename_fits_char = filename_fits_str.c_str();

   if (gSIM_IS_PRINT) {
      ifstream isfile(filename_fits_char);
      if (isfile) {
         print_warning("janalysis.cc", "gal_j2D()", "Output file " + filename_fits_str + " already exists and "
                       "will be overwritten. You have 5 seconds to kill the process with CTRL + C");
         usleep(5e6);
         remove(filename_fits_char);
      }
      fh_file_out.create(filename_fits_str);
   }


   /****************** FILL ARRAYS AND START TO CALCULATE!  *******************/

   //--- Array declarations and initialisation
   vector<double> j_tot(n_pix_fov, 1.e-40);
   vector<double> jgal_sm(n_pix_fov, 1.e-40);
   vector<double> jgal_sub_continuum;
   vector<double> jgal_crossprod;
   vector<double> jgal_sub_drawn;
   vector<double> j_list_tot;

   // switch to full 2D calculation of smooth contributions, if triaxial halo is chosen:
   if (gMW_TRIAXIAL_IS) gSIM_IS_CIRCULAR_SYMMETRY = false;

   // when choosing a smoothing, these quantities are calculated on a bigger grid
   vector<double> j_tot_beam; // (will be initialised later to save memory.)
   vector<double> jgal_sm_beam;
   vector<double> jgal_sub_continuum_beam;
   vector<double> jgal_crossprod_beam;

   char fname_stat[500];

   // Create drawn clumps arrays, if required
   if (is_list_added)
      j_list_tot.assign(n_pix_fov, -1.e-40);
   if (is_subs) {
      jgal_sub_continuum.assign(n_pix_fov, 1.e-40);
      if (gPP_DM_IS_ANNIHIL_OR_DECAY)
         jgal_crossprod.assign(n_pix_fov, 1.e-40);
      if (is_subs_drawn)
         jgal_sub_drawn.assign(n_pix_fov, -1.e-40);
   }

   if (smooth_gamma_or_nu_or_both != 0 && n_pix_fov != n_pix) {
      // Create smooth maps in case there will be an instrumental beam smoothin later:
      jgal_sm_beam.assign(n_pix_fov_extended, 1.e-40);
      if (is_subs) {
         jgal_sub_continuum_beam.assign(n_pix_fov_extended, 1.e-40);
         if (gPP_DM_IS_ANNIHIL_OR_DECAY)
            jgal_crossprod_beam.assign(n_pix_fov_extended, 1.e-40);
      }
   }


   // Set Gal. parameters
   const int n_partot = 10;
   double par_galtot[n_partot];
   double par_galdpdv[n_partot];       // Two last parameters are psi(gal_centre) and theta(gal_centre)
   double par_galdpdv_modif[n_partot]; // Two last parameters are psi_los and theta_los
   gal_set_partot(par_galtot);
   double m_gal = mass_singlehalo(par_galtot, gSIM_EPS);

   const int n_parsubs = 25;
   double par_galsubs[n_parsubs];

   double f_dm = 0., ntot_subs = 0.;
   double mmin_subs = gDM_SUBS_MMIN;
   double mmax_subs = m_gal * gDM_SUBS_MMAXFRAC;
   double Delta_c = delta_x_to_delta_crit(gCOSMO_DELTA0, gCOSMO_FLAG_DELTA_REF, 0. /*redshift*/);

   cout << ">>>>> Mass of host galaxy Mgal=" << m_gal << " Msol" << endl;
   cout << ">>>>> Density at scale radius " << par_galtot[1] << " kpc: rho_s =" << par_galtot[0] << " Msol/kpc^3" << endl;
   cout << ">>>>> vmax = " << sqrt(par_galtot[0] * pow(par_galtot[1], 2) * 11.244 / (KG_to_MSOL * KPC_to_KM * 1000.) * 6.673e-11) / 1000. << " km/s-1" << endl;

   if (is_subs) {
      // find the mass fraction in clumps
      gal_set_pardpdv(par_galdpdv);
      gal_set_pardpdv(par_galdpdv_modif);
      gal_set_parsubs(par_galsubs);
      ntot_subs = nsubtot_from_nsubm1m2(&par_galsubs[8], gMW_SUBS_M1, gMW_SUBS_M2, gMW_SUBS_N_INM1M2, gSIM_EPS);
      f_dm =  par_galsubs[14];
      double mtot_subs = f_dm * m_gal;
      double m_smooth = m_gal - mtot_subs;
      double dpdv_local = 0;
      rho(gMW_RSOL, par_galdpdv, dpdv_local);
      double par_galclmean[n_partot];
      for (int ii = 0; ii < n_partot; ++ii)
         par_galclmean[ii] = par_galdpdv[ii];
      par_galclmean[0] *= par_galsubs[14] * m_gal;  /* f_dm * mgal */
      double rho_subs_local = 0;
      rho(gMW_RSOL, par_galclmean, rho_subs_local);
      double f_dm_loc = rho_subs_local / gMW_RHOSOL;

      printf(">>>>> total number of subclumps in halo: %le\n", ntot_subs);
      printf(">>>>> total mass of subclumps in halo: %le Msol\n", mtot_subs);
      printf("   => Mass of smooth halo: %le Msol\n", m_smooth);
      printf("   => Fraction of total mass contained in subclumps: %le\n", f_dm);
      printf("   => Fraction of local mass contained in subclumps: %le\n", f_dm_loc);
      printf("   => local subhalo density dN^2/dVdM(rsol) = k*(M/Msol)^-alpha : k = %le kpc^-3Msol^-1\n", dpdv_local * ntot_subs * par_galsubs[8]);

      // Set properly two last parameters of par_galdpdv_modif to psi_los and theta_los (in rad)
      par_galdpdv_modif[8] = psi;
      par_galdpdv_modif[9] = theta;
   }

   printf("\n");

   // Calculate smooth
   //------------------
   // (using interpolation to speed-up calculation when required)
   printf("    * smooth halo contribution\n");
   if (smooth_gamma_or_nu_or_both == 0 || n_pix_fov == n_pix) {
      j_interpolation(&jgal_sm[0], rs_pix_fov, grid_mode, psi, theta, dtheta_orth, dtheta,
                      0, m_gal, par_galtot, gSIM_EPS, f_dm, par_galdpdv, par_galsubs);
   } else {
      j_interpolation(&jgal_sm_beam[0], rs_pix_fov_extended, grid_mode_extended, psi, theta, dtheta_orth_extended,
                      dtheta_extended, 0, m_gal, par_galtot, gSIM_EPS, f_dm, par_galdpdv, par_galsubs);
      // copy values on extended grid into jgal_sm array:
      for (int i = 0; i < n_pix_fov; ++i) {
         int hp_index = v_pix_fov[i] ;
         int i_extended = binary_search(v_pix_fov_extended, hp_index);
         jgal_sm[i] = jgal_sm_beam[i_extended];
      }
   }
   // + search for direction (indices) of minimal flux
   // [used below to decide which clumps to draw]
   double j_min = 1.e40;
   int i_fluxmin = -1;
   pointing ptg_i_pix_min ;
   for (int i = 0; i < n_pix_fov; ++i) {
      if (jgal_sm[i] < j_min) {
         j_min = jgal_sm[i];
         i_fluxmin = i;
      }
   }
   if (i_fluxmin < 0) {
      printf("\n====> ERROR: gal_j2D() in janalysis.cc");
      printf("\n             No minimum found, means that no pixel in this map!");
      printf("\n             => abort()\n\n");
      abort();
   }
   ptg_i_pix_min = hp_gridprop.pix2ang(v_pix_fov[i_fluxmin]) ;


   /********************************/
   /** OPTION 2 or 3: DRAW CLUMPS **/
   /********************************/
   // Pairs (to keep indices of sorted vectors)
   vector<pair<double, int> > list_jcl, list_contrastcl, dummy;

   vector<double> list_jcl_stat_all; // save j_cl of all realizations
   vector< vector<double> > list_jcl_stat_realizations(gSTAT_N_REALIZATIONS, vector<double>(0)); // save j_cl of one realization
   int num_clumps_stat = 0.;
   int *nb_to_draw_stat = NULL;
   int nbins_stat = 0;
   double jlogbin_stat_min = 1.e10;
   double jlogbin_stat_max = 1.e-10;

   pair<double, int> tmp_pair;
   vector<string> list_name;
   vector<struct gStructHalo> list_clumps;

   unsigned int counter = 0; // number of drawn clumps

   string unit = units_or_canvasname_1D(1, 2, 0);
   string to_replace = "#alpha_{int}";
   char tmp_unit[50];
   sprintf(tmp_unit, "%.4f^{#circ}", gSIM_ALPHAINT * RAD_to_DEG);
   string replace_with = string(tmp_unit);
   replace_substring(unit, to_replace, replace_with);

   if (is_subs) {
      if (is_subs_drawn) {

         //--- Set Seed
         printf("========> SEED=%d\n", gSIM_SEED);
#if IS_ROOT
         gRandom->SetSeed(gSIM_SEED);
#endif


         //--- Find l_crit for each mass decade (below which to draw clumps)
         // N.B.: cannot start in 0 because of diverging variance calculation
         double lmin = 1.e-3;
         double lmax = gMW_RMAX + gMW_RSOL;


         //--- Number of clumps for each mass decade (in the whole halo)
         // and boundaries of mass decade (useful if the whole range is not a
         // power of 10)
         vector<double> n_subs_decade;
         vector<double> m1, m2;
         vector<double> l_crit;

         //--- Number of clumps to draw in the size*size map
         int kmax = 0; // number of decades
         for (double mass = mmin_subs; mass < mmax_subs; mass *= 10.) {
            m1.push_back(mass);
            if (mass * 10. > mmax_subs)  m2.push_back(mmax_subs);
            else m2.push_back(mass * 10.);
            n_subs_decade.push_back(ntot_subs * frac_nsubs_in_m1m2(&par_galsubs[8], m1[kmax], m2[kmax], gSIM_EPS));
            ++kmax;
         }


         //-------------//
         // Find lcrit: //
         //-------------//
         // Distance below which continuous clumps treatment fails for each mass decade.
         // To be conservative, we only determine it for the most constraining pixel of the map,
         // i.e., the pixel for which Jsm is the smallest.
         // N.B.: criterion is to have an RSE <= user_rse
         printf("    * find l_crit for all mass decades\n\n");
         double jsm_min = jsmooth_mix(m_gal, par_galtot, ptg_i_pix_min.phi, PI / 2 - ptg_i_pix_min.theta, gSIM_EPS, f_dm, par_galdpdv);
         for (int k = 0; k < kmax; k++) {
            l_crit.push_back(lmin);
            find_lcritgal_los(n_subs_decade[k], par_galdpdv, ptg_i_pix_min.phi, PI / 2 - ptg_i_pix_min.theta,
                              lmin, lmax, par_galsubs, m1[k], m2[k], user_rse, gSIM_EPS, jsm_min, l_crit[k]);
            printf("        ... mass decade: %+3.1f - %+3.1f  l_crit=%.*le kpc\n", log10(m1[k]), log10(m2[k]), gSIM_SIGDIGITS, l_crit[k]);
         }
         printf("\n\n");


         // If tabulated parametrisation of  tidal disruption of clumps in Galaxy:
         //  -> load necessary files to draw correctly clumps
         vector <double> stref17_rgalkpc_for_c200min, stref17_c200_min,
                stref17_rgalkpc_for_rt2rs, stref17_c200_for_rt2rs;
         vector <vector <double> > stref17_rt2rs;
         if (gMW_SUBS_TABULATED_IS) {
            printf("        [*** <START> Overrides from tabulated semi-analytical model ***]\n");
            printf("N.B.: ensure tabulated values consistently obtained from smooth/sub halos properties (no check perform here)\n");
            // Read table of l_crit, and update
            vector <double> stref17_log10m1, stref17_log10m2, stref17_lcrit;
            string filename = gMW_SUBS_TABULATED_LCRIT;

            printf("         (i) read and update l_crit(Mi) from %s\n", filename.c_str());
            stref17_load_lcrit(get_path(filename, false), stref17_log10m1, stref17_log10m2, stref17_lcrit);
            for (int k = 0; k < kmax; ++k) {
               double l_crit_old = l_crit[k];
               // Use most stringent l_crit if:
               //  - minimal mass outside tabulation
               //  - range run does not match range tabulated
               if (log10(m1[k]) < stref17_log10m1[0] - 1.e-5) {
                  printf("\n====> WARNING: gal_j2D() in janalysis.cc");
                  printf("\n             Lower mass decade run (%.1f) smaller than tabulated one (%.1f)", log10(m1[k]), stref17_log10m1[0]);
                  printf("\n             => Replace current l_crit=%le by first tabulated value %le\n", l_crit[k], stref17_lcrit[0]);
                  l_crit[k] = stref17_lcrit[0];
               } else if (log10(m2[k]) > stref17_log10m2[stref17_log10m2.size() - 1] + 1.e-5) {
                  printf("\n====> WARNING: gal_j2D() in janalysis.cc");
                  printf("\n             Upper mass decade run (%.1f) larger than tabulated one (%.1f)", log10(m2[k]), stref17_log10m1[stref17_log10m2.size() - 1]);
                  printf("\n             => Replace current l_crit=%le by last tabulated value %le\n", l_crit[k], stref17_lcrit[stref17_lcrit.size() - 1]);
                  l_crit[k] = stref17_lcrit[stref17_lcrit.size() - 1];
               } else {
                  int i_m = binary_search(stref17_log10m1, log10(m1[k]));
                  if ((fabs(log10(m1[k])) < 1.e-5 && fabs(stref17_log10m1[i_m]) > 1.e-5)
                        || (fabs(log10(m2[k])) < 1.e-5 && fabs(stref17_log10m2[i_m]) > 1.e-5)
                        || (fabs(log10(m1[k]) - stref17_log10m1[i_m]) / stref17_log10m1[i_m]) > 1.e-5
                        || (fabs(log10(m2[k]) - stref17_log10m2[i_m]) / stref17_log10m2[i_m]) > 1.e-5) {
                     printf("\n====> WARNING: gal_j2D() in janalysis.cc");
                     printf("\n             Non-matching mass decades ( [%.1f,%.1f] run vs  [%.1f,%.1f] tabulated)",
                            log10(m1[k]), log10(m2[k]), stref17_log10m1[i_m], stref17_log10m2[i_m]);
                     l_crit[k] = stref17_lcrit[i_m];
                     if (log10(m1[k]) < stref17_log10m1[i_m] && i_m > 0 && stref17_lcrit[i_m - 1] < l_crit[k])
                        l_crit[k] = stref17_lcrit[i_m - 1];
                     if (log10(m2[k]) > stref17_log10m2[i_m] && i_m < (int) stref17_lcrit.size() - 1 && stref17_lcrit[i_m + 1] < l_crit[k])
                        l_crit[k] = stref17_lcrit[i_m + 11];
                     printf("\n             => Replace current l_crit=%le by smallest value  interval %le\n", l_crit_old, l_crit[k]);
                  } else
                     l_crit[k] = stref17_lcrit[i_m];
               }
               printf("        ... mass decade: %+3.1f - %+3.1f  l_crit=%.*le replaced by %.*le kpc\n", log10(m1[k]), log10(m2[k]),
                      gSIM_SIGDIGITS, l_crit_old, gSIM_SIGDIGITS, l_crit[k]);

            }
            // Read table of cmin
            filename = gMW_SUBS_TABULATED_CMIN_OF_R;
            printf("         (ii) read cmin(r_gal) from %s\n", filename.c_str());
            stref17_load_cmin200_vs_rgal(get_path(filename, false), stref17_rgalkpc_for_c200min, stref17_c200_min);

            // Read table of rt/rs
            filename = gMW_SUBS_TABULATED_RTIDAL_TO_RS;
            printf("         (iii) read rt/rs(r_gal,c200) from %s\n", filename.c_str());
            stref17_load_rt2rs_vs_rgal_c200(get_path(filename, false), stref17_rgalkpc_for_rt2rs, stref17_c200_for_rt2rs, stref17_rt2rs);

            printf("        [*** <DONE>  Overrides from tabulated semi-analytical models ***]\n\n");
         }



         //---------------------------------------//
         // Find #clumps to draw per mass decade: //
         //---------------------------------------//
         // Below the number of clumps to be drawn is based on the distribution function dpdv_lcosalphabeta.
         // For the RECT and the STRIP mode, as we have a ~ FOV*FOV size aperture but a spherical
         // distribution function, the strategy is to draw all the clumps within a(=alpha_int)<d_max
         // and discard those who fell out of FOV*FOV.
         //
         // In case of grid extension for a map later to be smoothed by an instrumental beam, still do the
         // calculation for the original grid (maybe wrong? To check!)
         //
         //                       FOV (deg)
         //
         //                   ----------------
         //                  |              / |
         //                  | a= d_max   /   |
         //                  |          /     |
         //                  |        /       |   FOV (deg)
         //                  |                |
         //                  |                |
         //                  |                |
         //                  |                |
         //                   ----------------
         //
         // So, determine d_max:
         double pixdist_max = 0. ;
         double psi_orig = psi; // bec. FOV is swapped in inverse mode to draw clumps
         double theta_orig = theta ;

         // short-cut for DISK mode:
         if (grid_mode == "DISK" && dtheta < 2. * PI) {
            if (dtheta > 0) pixdist_max = dtheta / 2. ;
            else {
               //inverse mode:
               pixdist_max = PI + dtheta / 2. ;
               theta = - theta;
               psi = psi + PI;
            }
            // short-cut for whole sky (includes DISK mode, as dtheta_orth is set to 2 * PI in this case):
         } else if (dtheta_orth == 2 * PI || dtheta == PI) {
            pixdist_max = PI ;
         } else {
            vec3_t<double> vec3_FOVcenter ;
            vec3_t<double> vec3_pix;
            pointing ptg_FOVcenter ;

            if (dtheta_orth > 0 && dtheta > 0) {
               ptg_FOVcenter.theta = PI / 2 - theta;
               ptg_FOVcenter.phi = psi;
            } else {
               // inverse mode:
               theta = - theta;
               psi = psi + PI;
               ptg_FOVcenter.theta = PI / 2 - theta;
               ptg_FOVcenter.phi = psi;
            }
            vec3_FOVcenter = ptg_FOVcenter.to_vec3();
            for (int i = 0; i < n_pix_fov; ++i) {
               vec3_pix = hp_gridprop.pix2vec(v_pix_fov[i]) ;
               double pixdist = hp_angular_dist(vec3_FOVcenter, vec3_pix);
               if (pixdist > pixdist_max) pixdist_max = pixdist ;
            }
         }
         // enlarge pixdist_max a bit:
         if (pixdist_max + gSIM_RESOLUTION >= PI) pixdist_max = PI ;
         else pixdist_max += gSIM_RESOLUTION ;

         // Clumpy V1:
         // pixdist_max = max(dpsi, dtheta) / sqrt(2.);

         vector<double> nb_mean_to_draw(kmax, 0.);
         vector<int> nb_to_draw(kmax, 0);
         // find how many clumps need to be drawn in the size*size map
         printf("    * find #clumps to draw below l_crit for each mass decade (%.0f x %.0f deg map)\n\n", dpsi * RAD_to_DEG, dtheta_coord * RAD_to_DEG);
         printf(" mass decade %s | #clumps (Gal) | l_crit (kpc) | <#clumps to draw> => #clumps = Poisson(<#clumps>)  \n", legend_for_delta().c_str());
         double gSIM_ALPHAINT_real = gSIM_ALPHAINT; // keep in mind, what is the real experimental resolution
         //
         // For a given mass decade and field of view, the mean number of clumps <n> can be a number smaller to 1.
         // To ensure that the correct statistical number is drawn, we draw the actual number to draw from
         // a Poisson variate distribution of mean <n>

         // Enlarge the integration angle up to disk containing whole FOV to calculate <n>
         gSIM_ALPHAINT = pixdist_max ;
         for (int k = 0; k < kmax; ++k) {
            // Calculate <n> between 0 and l_crit(M)
            lmin = 0.;
            lmax = l_crit[k];
            nb_mean_to_draw[k] = n_subs_decade[k] * frac_nsubs_in_foi(par_galdpdv, psi, theta, lmin, lmax, gSIM_EPS);
            // Draw n from Poisson distrib of mean <n>
#if IS_ROOT
            if (nb_mean_to_draw[k] > 0) {
               TRandom r(gSIM_SEED);
               nb_to_draw[k] = r.Poisson(nb_mean_to_draw[k]);

            } else
#endif
               nb_to_draw[k] = 0;
            printf("  %+2.1f --> %+2.1f      %.3le       %.3le        %.3le    =>     %d\n",
                   log10(m1[k]), log10(m2[k]), n_subs_decade[k], l_crit[k], nb_mean_to_draw[k], nb_to_draw[k]);
         }
         printf("\n      ... and correct for the low-mass decades:\n");

         //correct l_crit:
         int k_nmax = distance(nb_mean_to_draw.begin(), max_element(nb_mean_to_draw.begin(), nb_mean_to_draw.end()));
         double log_step = (log10(nb_mean_to_draw[k_nmax]) - log10(nb_mean_to_draw[0])) / k_nmax;
         for (int k = 0; k < k_nmax; ++k) {
            double nb_mean_to_draw_tmp = nb_mean_to_draw[0] * pow(10, k * log_step);
            if (nb_mean_to_draw_tmp > nb_mean_to_draw[k]) {
               nb_mean_to_draw[k] = nb_mean_to_draw_tmp;
               // find corresponding l_crit:
               nb_mean_to_draw_tmp = 0.;
               int iter = 0;
               lmax = l_crit[k];
               while (abs((nb_mean_to_draw[k] - nb_mean_to_draw_tmp) / nb_mean_to_draw[k]) > gSIM_EPS) {
                  lmin = 0.;
                  // if too few drawn clumps, increase lcrit:

                  double log_step1 = (log10(gMW_RMAX + gMW_RSOL) - log10(1e-3)) / pow(2., iter);
                  if (nb_mean_to_draw_tmp < nb_mean_to_draw[k]) {
                     lmax = pow(10., (log10(lmax) + log_step1));
                  } else {
                     lmax = pow(10., (log10(lmax) - log_step1));
                  }
                  nb_mean_to_draw_tmp = n_subs_decade[k] * frac_nsubs_in_foi(par_galdpdv, psi, theta, lmin, lmax, gSIM_EPS);
                  iter++;
                  if (iter > 30) {
                     cout << "aborted, stay with former lcrit" << endl;
                     nb_mean_to_draw_tmp = nb_mean_to_draw[k];
                     lmax = l_crit[k];
                     break;
                  }
               }
               l_crit[k] = lmax;
               nb_mean_to_draw[k] = nb_mean_to_draw_tmp;
#if IS_ROOT
               if (nb_mean_to_draw[k] > 0) {
                  TRandom r(gSIM_SEED);
                  nb_to_draw[k] = r.Poisson(nb_mean_to_draw[k]);
               } else
#endif
                  nb_to_draw[k] = 0;
            }
         }
         printf(" mass decade %s | #clumps (Gal) | l_crit (kpc) | <#clumps to draw> => #clumps = Poisson(<#clumps>)  \n", legend_for_delta().c_str());
         for (int k = 0; k < kmax; ++k) {
            printf("  %+2.1f --> %+2.1f      %.3le       %.3le        %.3le    =>     %d\n",
                   log10(m1[k]), log10(m2[k]), n_subs_decade[k], l_crit[k], nb_mean_to_draw[k], nb_to_draw[k]);
         }
         printf("\n\n");

         // restore opening angle to the desired one.
         gSIM_ALPHAINT = gSIM_ALPHAINT_real;
         psi = psi_orig; // bec. in inverse mode, FOV has been swapped to draw clumps
         theta = theta_orig ;

         // Calculate Jsub_continuum below l_crit
         //--------------------------------------
         // (using interpolation to speed-up calculation when required)
         printf("    * <subs> contribution (>l_crit) for all mass decades\n");

         if (smooth_gamma_or_nu_or_both == 0 || n_pix_fov == n_pix) {
            j_interpolation(&jgal_sub_continuum[0], rs_pix_fov, grid_mode, psi, theta, dtheta_orth, dtheta,
                            1, m_gal, par_galtot, gSIM_EPS, f_dm, par_galdpdv,
                            par_galsubs, ntot_subs, kmax, &l_crit[0], &m1[0], &m2[0]);
         } else {
            j_interpolation(&jgal_sub_continuum_beam[0], rs_pix_fov_extended, grid_mode_extended, psi, theta, dtheta_orth_extended, dtheta_extended,
                            1, m_gal, par_galtot, gSIM_EPS, f_dm, par_galdpdv,
                            par_galsubs, ntot_subs, kmax, &l_crit[0], &m1[0], &m2[0]);
            // copy values on extended grid into jgal_sub_continuum array:
            for (int i = 0; i < n_pix_fov; ++i) {
               int hp_index = v_pix_fov[i] ;
               int i_extended = binary_search(v_pix_fov_extended, hp_index);
               jgal_sub_continuum[i] = jgal_sub_continuum_beam[i_extended];
            }
         }

         // Calculate associated cross-prod (below l_crit)
         //--------------------------------------
         // (using interpolation to speed-up calculation when required)
         if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
            printf("    * calculating Jcross-prod (>l_crit) for all mass decades\n");

            if (smooth_gamma_or_nu_or_both == 0 || n_pix_fov == n_pix) {
               j_interpolation(&jgal_crossprod[0], rs_pix_fov, grid_mode, psi, theta, dtheta_orth, dtheta,
                               2, m_gal, par_galtot, gSIM_EPS, f_dm, par_galdpdv,
                               par_galsubs, ntot_subs, kmax, &l_crit[0], &m1[0], &m2[0]);
            } else {
               j_interpolation(&jgal_crossprod_beam[0], rs_pix_fov_extended, grid_mode_extended, psi, theta, dtheta_orth_extended, dtheta_extended,
                               2, m_gal, par_galtot, gSIM_EPS, f_dm, par_galdpdv,
                               par_galsubs, ntot_subs, kmax, &l_crit[0], &m1[0], &m2[0]);
               // copy values on extended grid into jgal_crossprod array:
               for (int i = 0; i < n_pix_fov; ++i) {
                  int hp_index = v_pix_fov[i] ;
                  int i_extended = binary_search(v_pix_fov_extended, hp_index);
                  jgal_crossprod[i] = jgal_crossprod_beam[i_extended];
               }
            }
         }

         //------------------------------------------------------------//
         // Draw clumps and check if they are in the area of interest  //
         //------------------------------------------------------------//
         printf("    * Draw the clumps (in opening angle containing the FOV) and add to skymap\n");


#if IS_ROOT
         // Create TF1 for dpdlogm
         TF1 *f_mass = new TF1("f_mass", dpdlogm, log10(mmin_subs), log10(mmax_subs), 2);
         f_mass->SetNpx(100);
         f_mass->SetParameters(par_galsubs[8], par_galsubs[9]);
#endif

         //--- Open file to store J for all drawn clumps
         FILE *fp_drawn = NULL;

         if (gSIM_IS_PRINT) {
            fp_drawn = fopen(filename_drawn_str.c_str(), "w");
            fprintf(fp_drawn, "# This file lists the properties and %ccl(centre) for all drawn clumps\n", j_or_d);
            fprintf(fp_drawn, "# Name           Type    long    lat      d     z   Rdelta   |     rhos       rs     prof.   #1   #2   #3   |      %c         %c/%ccontinuum |  Mdelta     Mtid     Rtid    Mequdens Requdens  Dgal\n",
                    j_or_d, j_or_d, j_or_d);
            fprintf(fp_drawn, "#  -               -     [deg]   [deg]  [kpc]   -    [kpc]   |  [Msol/kpc3]  [kpc]  [enum]    -    -    -   | %s   -   |  [Msol]    [Msol]   [kpc]      [Msol]  [kpc]   [kpc]\n", unit.c_str());
            fprintf(fp_drawn, "#\n");
         }

         //--- loop on mass decades
         counter = 0;
         for (int k = 0; k < kmax; ++k) {
            printf("        ... decade %d of %d (%.1f --> %.1f):  ", k + 1, kmax, log10(m1[k]), log10(m2[k]));
            printf("%d clumps to draw\n", nb_to_draw[k]);
            lmin = 0.;

#if IS_ROOT
            // Create ROOT random generator for the distribution dpdv_lcosalphabeta
            TF3 *f_lalphabeta = new TF3("f_lalphabeta", dpdv_lcosalphabeta, lmin, l_crit[k],
                                        MY_COS(pixdist_max), 1., 0., 2 * PI, 10);
            f_lalphabeta->SetNpx(5000);
            f_lalphabeta->SetNpy(30);
            f_lalphabeta->SetNpz(30);
            f_lalphabeta->SetParameters(par_galdpdv_modif);
#endif
            // If more than 1 cl to draw, draw it!
            if (nb_to_draw[k] >= 1) {
               int percentage = 10 ;
               unsigned int counter_dec = 0;
               int n_nondisrupted = 0;
               int n_disrupted = 0;
               for (int i = 0; i < nb_to_draw[k]; ++i) {

                  // status report:
                  int progress = (100 * i) / nb_to_draw[k]  ;
                  if (nb_to_draw[k] > 1e4 && progress == percentage) {
                     printf("        ... %d%% done ...\n", progress);
                     percentage += 5 ;
                  } else if (nb_to_draw[k] > 1e3 && progress == percentage) {
                     printf("        ... %d%% done ...\n", progress);
                     percentage += 10 ;
                  } ;

                  // assign position for clump
                  double l_cl = 0.;
                  double cos_alpha_cl = 0.;
                  double beta_cl = 0;
                  double psi_cl, theta_cl, r_cl, x_cl;
#if IS_ROOT
                  f_lalphabeta->GetRandom3(l_cl, cos_alpha_cl, beta_cl); // between lmin and l_crit[k]
#endif
                  double par_tmp[2], x[3];
                  x[0] = l_cl;
                  x[1] = acos(cos_alpha_cl);
                  x[2] = beta_cl;
                  par_tmp[0] = psi;
                  check_psi(par_tmp[0]);
                  par_tmp[1] = theta;
                  lalphabeta_to_lpsitheta(x, par_tmp);
                  psi_cl = x[1];
                  check_psi(psi_cl);
                  theta_cl = x[2];
                  x[1] = acos(cos_alpha_cl);
                  x[2] = beta_cl;
                  lalphabeta_to_rpsitheta(x, par_tmp);
                  r_cl = x[0];
                  x_cl = r_cl / gMW_RMAX;

                  // N.B.: in 2D representation, the user can ask for psi angles >180 deg
                  // or <-180 deg (but not both at the same time)
                  // The correction to add 2pi or subtract 2pi on psi_cl should
                  // not be necessary anymore as it checked differently if the
                  // clump is in the FOV.

                  pointing ptg_cl(PI / 2 - theta_cl, psi_cl) ;
                  // find the closest Pixel to the clump centre
                  int i_pix_closest = hp_gridprop.ang2pix(ptg_cl) ;

                  // assign mass in the given mass decade
                  double log_mass_cl = 0.;
#if IS_ROOT
                  log_mass_cl = f_mass->GetRandom(log10(m1[k]), log10(m2[k]));
#endif

                  // sometimes, nan values are thrown...
                  if (std::isinf(float(log_mass_cl)) || isnan(float(log_mass_cl))) {
                     sprintf(char_tmp, "Found nan-value for log_mass_cl in mass decade (%.1f --> %.1f). Throw random generator again...",
                             log10(m1[k]),  log10(m2[k]));
                     print_warning("janalysis.cc", "gal_j2D()", string(char_tmp));
                     int try_n_times = 0;
                     while (std::isinf(float(log_mass_cl)) || isnan(float(log_mass_cl))) {
#if IS_ROOT
                        log_mass_cl = f_mass->GetRandom(log10(m1[k]), log10(m2[k]));
#endif
                        try_n_times++;
                        if (try_n_times == 100) break;
                     }
                     if (try_n_times == 100) {
                        print_warning("janalysis.cc", "gal_j2D()", "Rethrowing random generator failed. Discard clump.");
                        continue;
                     }
                  }

                  // if cl falls in FOV*FOV, add its contribution
                  if (rs_pix_fov.contains(i_pix_closest)) {

                     ++counter;
                     ++counter_dec;

                     // fill clump parameters from its mass
                     double mass_cl = pow(10., log_mass_cl);
                     double c = 0.;

                     if (gDM_FLAG_CDELTA_DIST != kDIRAC) { //we draw the concentration from its distribution if gMW_SUBS_FLAG_CDELTAMDELTA is not kDIRAC
                        double cvir_mean = mdelta_to_cdelta(mass_cl, Delta_c, gMW_SUBS_FLAG_CDELTAMDELTA, &par_galsubs[0], 0., x_cl);
                        const int n_par_c = 3;
                        double par_conc[n_par_c] = {cvir_mean, gDM_LOGCDELTA_STDDEV, (double)gDM_FLAG_CDELTA_DIST};
#if IS_ROOT
                        TF1 *f_c = new TF1("f_c", dpdc, 0.1 * cvir_mean, 10.*cvir_mean, 3);
                        f_c->SetNpx(500);
                        f_c->SetParameters(par_conc);
                        c = f_c->GetRandom();
                        delete f_c;
                        f_c = NULL;
#endif

                        // If tabulated parametrisation of tidal disruption of clumps in Galaxy:
                        //  -> retrieve c200_min for current clump (log_mass_cl, r_cl, c)
                        double cmin = 0.;
                        if (gMW_SUBS_TABULATED_IS) {
                           cmin = interp1D(r_cl, stref17_rgalkpc_for_c200min, stref17_c200_min, kLOGLOG);
                           if (c < cmin) {
                              ++n_disrupted;
                              --counter;
                              --counter_dec;
                              continue;
                           } else
                              ++n_nondisrupted;
                        }
                     }

                     const int npar = 11;
                     double par_cl[npar];
                     mdelta_to_par(par_cl, mass_cl, Delta_c, (int)par_galsubs[10] /*card cdelta-mdelta*/, &par_galsubs[0],
                                   gSIM_EPS, 0. /*redshift*/, c, x_cl);
                     par_cl[7] = l_cl;
                     par_cl[8] = psi_cl;
                     par_cl[9] = theta_cl;
                     par_cl[10] = 0.; // redshift

                     // check the physical size of the clump:
                     const int npar_size = 18;
                     double par_size[npar_size];
                     for (int i_size = 0; i_size < 6; ++i_size) {
                        par_size[i_size] = par_cl[i_size];
                     }
                     par_size[6] = 0.;
                     for (int i_size = 7; i_size < 13; ++i_size) {
                        par_size[i_size] = par_galtot[i_size - 7];
                     }
                     par_size[13] = r_cl;
                     par_size[14] = gSIM_EPS;

                     par_size[15] = 0.; // n_call: must be initialised to 0!
                     par_size[16] = 0;  // Select method used to estimate physical size. 0: tidal radius condition, 1: equal-density condition
                     par_size[17] = 1;  // enable soft fail of function (returns -999 in that case)
                     double r_tidal = find_halo_physicalsize(par_cl[6], par_size);

                     par_size[15] = 0.; // n_call: must be initialised to 0!
                     par_size[16] = 1;
                     double r_equaldens = find_halo_physicalsize(par_cl[6], par_size);

                     // calculate tidal mass:
                     double m_tidal;
                     if (r_tidal == -999) {
                        m_tidal = 1. / 0.;
                        r_tidal = 1. / 0.;
                        print_warning("janalysis.cc", "gal_j2D()", "Subhalo tidal radius could not be determined. Set it and m_tidal to NaN.");
                     } else {
                        par_size[6] = r_tidal;
                        m_tidal = mass_singlehalo(par_size, gSIM_EPS);

                     }
                     // calculate equal density mass:
                     double m_equaldens;
                     if (r_equaldens == -999) {
                        m_equaldens = 1. / 0.;
                        r_equaldens = 1. / 0.;
                        print_warning("janalysis.cc", "gal_j2D()", "Subhalo equal density radius could not be determined. Set it and m_equaldens to NaN.");
                     } else {
                        par_size[6] = r_equaldens;
                        m_equaldens = mass_singlehalo(par_size, gSIM_EPS);
                     }

                     // Add clump properties in list
                     char tmp_n_cl[50];
                     sprintf(tmp_n_cl, "clump#%d", int(counter));
                     gStructHalo s_clump;
                     s_clump.Name = tmp_n_cl;
                     s_clump.Type = "GAL.CLUMP";
                     s_clump.PsiDeg = psi_cl * RAD_to_DEG;
                     s_clump.ThetaDeg = theta_cl * RAD_to_DEG;;
                     s_clump.l = l_cl;
                     s_clump.z = 0.;
                     s_clump.Rvir = par_cl[6];
                     // If tabulated parametrisation of tidal disruption of clumps in Galaxy:
                     //  -> update physical size of clump to tidal radius (rt2rs(r_cl,c)*rs)
                     if (gMW_SUBS_TABULATED_IS) {
                        double r_stref17 = par_cl[1] * interp2D(r_cl, c, stref17_rgalkpc_for_rt2rs, stref17_c200_for_rt2rs, stref17_rt2rs, kLOGLOG);
                        par_size[6] = r_stref17;
                        double m_stref17 = mass_singlehalo(par_size, gSIM_EPS);
                        //printf("  Clump #%d: m_cl=%.2le, r_cl=%.2le, c_cl=%.2le  | r/m_200=%.2le/%.2le r/m_tidal=%.2le/%.2le   r/m_equaldens=%.2le/%.2le r/m_stref17=%.2le/%.2le\n",
                        //       (int)counter, pow(10., log_mass_cl), r_cl, c, par_cl[6], mass_cl, r_tidal, m_tidal, r_equaldens, m_equaldens, r_stref17, m_stref17);
                        if (m_stref17 < gDM_SUBS_MMIN) {
                           ++n_disrupted;
                           --counter;
                           --counter_dec;
                           --n_nondisrupted;
                           continue;
                        }
                        s_clump.Rvir = r_stref17;
                        par_cl[6] = r_stref17;
                        mass_cl = m_stref17;
                     }

                     s_clump.Rhos = par_cl[0];
                     s_clump.Rscale = par_cl[1];
                     s_clump.HaloProfile = (int)par_cl[5];
                     s_clump.ShapeParam1 = par_cl[2];
                     s_clump.ShapeParam2 = par_cl[3];
                     s_clump.ShapeParam3 = par_cl[4];
                     s_clump.Mtot = mass_cl;

                     // check for subsubs in halo to draw:
                     if (par_galsubs[15] > 1) {
                        s_clump.Subs_Inner_ShapeParam1 = par_galsubs[0];
                        s_clump.Subs_Inner_ShapeParam2 = par_galsubs[1];
                        s_clump.Subs_Inner_ShapeParam3 = par_galsubs[2];
                        s_clump.Subs_Inner_Profile = par_galsubs[3];
                        s_clump.Subs_dPdM_Slope = par_galsubs[9];
                        s_clump.Subs_Inner_CDELTAMDELTA = par_galsubs[10];
                        s_clump.Subs_dPdV_Rs_to_Rshost = par_galsubs[23];
                        s_clump.Subs_Mmax = gDM_SUBS_MMAXFRAC * s_clump.Mtot;
                        s_clump.Subs_mfrac = par_galsubs[14];
                        s_clump.Subs_dPdV_Rscale = par_galsubs[23] * s_clump.Rscale;
                        s_clump.Subs_dPdV_ShapeParam1 = par_galsubs[18];
                        s_clump.Subs_dPdV_ShapeParam2 = par_galsubs[19];
                        s_clump.Subs_dPdV_ShapeParam3 = par_galsubs[20];
                        s_clump.Subs_dPdV_Profile = par_galsubs[21];
                     }

                     list_clumps.push_back(s_clump);

                     bool is_subsubs = false;
                     double par_subsub[n_parsubs];
                     const int n_pardpdv = 10;
                     double par_subs_dpdv[n_pardpdv];
                     double nsubsub_tot = 0;
                     if (par_galsubs[15] > 1 && s_clump.Subs_Mmax > gDM_SUBS_MMIN && s_clump.Subs_mfrac * s_clump.Mtot > gDM_SUBS_MMIN) {
                        is_subsubs = true;
                        halo_set_parsubs(counter - 1, par_subsub, list_clumps);
                        par_subsub[15] -= 1; // reduce gDM_SUBS_NUMBEROFLEVELS by one.
                        halo_set_pardpdv(counter - 1, par_subs_dpdv, list_clumps, false);
                        nsubsub_tot = nsubtot_from_msubtot(s_clump.Mtot,
                                                           gDM_SUBS_MMIN, s_clump.Subs_Mmax, &par_subsub[8], gSIM_EPS);
                     }

                     //pointing ptg_i_pix_closest = hp_gridprop.pix2ang(i_pix_closest);
                     //double psi_ipix_closest = ptg_i_pix_closest.phi;
                     //double theta_i_pix_closest = PI / 2 - ptg_i_pix_closest.theta;
                     //check_psi(psi_ipix_closest);

                     // Form pairs (to keep indices of sorted vectors)
                     // compute the flux in the central pixel
                     // do it with psi_ipix_closest, theta_i_pix_closest or with psi_cl, theta_cl???
                     double j_cl_centre;
                     if (s_clump.Subs_mfrac > 1.e-3 && is_subsubs)
                        j_cl_centre = jsmooth_mix(s_clump.Mtot, par_cl, psi_cl, theta_cl, gSIM_EPS, s_clump.Subs_mfrac, par_subs_dpdv);
                     else
                        j_cl_centre = jsmooth(par_cl, psi_cl, theta_cl, gSIM_EPS);
                     if (is_subsubs) {
                        double l1 = max(0., par_cl[7] - par_cl[6]);
                        double l2 = par_cl[7] + par_cl[6];
                        j_cl_centre += jsub_continuum(nsubsub_tot, par_subs_dpdv, psi_cl, theta_cl, l1, l2, par_subsub, gDM_SUBS_MMIN, s_clump.Subs_Mmax);
                        if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
                           j_cl_centre  += jcrossprod_continuum(s_clump.Mtot, par_cl, psi_cl, theta_cl, gSIM_EPS, s_clump.Subs_mfrac, par_subs_dpdv);
                        }
                     }

                     // find vector index of i_pix_closest
                     int index_i_pix_closest = binary_search(v_pix_fov, i_pix_closest);
                     double j_continuum = jgal_sm[index_i_pix_closest] + jgal_sub_continuum[index_i_pix_closest];
                     if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                        j_continuum += jgal_crossprod[index_i_pix_closest];
                     tmp_pair = make_pair(j_cl_centre / j_continuum, counter - 1);
                     list_contrastcl.push_back(tmp_pair);
                     double rhos_clump = par_cl[0];
                     double j_fraction = j_cl_centre / j_continuum;

                     convert_to_PP_units(1, j_cl_centre);
                     tmp_pair = make_pair(j_cl_centre, counter - 1);
                     list_jcl.push_back(tmp_pair);

                     // convert rho_0  to rho_s as used in profiles.cc for ZHAO profile:
                     if (int(par_cl[5]) == kZHAO) rhos_clump /= pow(2, (par_cl[3] - par_cl[4]) / par_cl[2]);

                     // print in file
                     if (gSIM_IS_PRINT) {
                        fprintf(fp_drawn, " %7d         DSPH  %+7.2f %+7.2f  %.*le %.f %.*le    %.*le %.*le  k%s  %g   %g   %g    %.*le %.*le   %.*le %.*le %.*le %.*le %.*le %.*le\n",
                                int(counter), psi_cl * RAD_to_DEG, theta_cl * RAD_to_DEG,
                                gSIM_SIGDIGITS,  l_cl, -1., gSIM_SIGDIGITS, par_cl[6],
                                gSIM_SIGDIGITS, rhos_clump, gSIM_SIGDIGITS, par_cl[1],
                                gNAMES_PROFILE[int(par_cl[5])], par_cl[2], par_cl[3], par_cl[4],
                                gSIM_SIGDIGITS, j_cl_centre, gSIM_SIGDIGITS,  j_fraction,
                                gSIM_SIGDIGITS, mass_cl, gSIM_SIGDIGITS, m_tidal, gSIM_SIGDIGITS, r_tidal,
                                gSIM_SIGDIGITS,  m_equaldens,
                                gSIM_SIGDIGITS, r_equaldens, gSIM_SIGDIGITS, r_cl);
                     }

                     // Add all pixels in the map:
                     // Force to gSIM_EPS_DRAWN relative precision for subclumps, otherwise too slow
                     // check for subsubs in halo to draw:
                     if (is_subsubs) {
                        add_halo_in_map(par_cl, gSIM_EPS_DRAWN, jgal_sub_drawn, j_continuum, rs_pix_fov, true,
                                        list_clumps[counter - 1].Mtot,  list_clumps[counter - 1].Subs_mfrac,
                                        par_subs_dpdv, par_subsub, nsubsub_tot);
                     } else {
                        add_halo_in_map(par_cl, gSIM_EPS_DRAWN, jgal_sub_drawn, j_continuum, rs_pix_fov, false);
                     }
                  }
               }
               printf("            => %d more clumps in the FOV, altogether %d clumps in the FOV\n", int(counter_dec), int(counter));
               // If tabulated parametrisation of tidal disruption of clumps in Galaxy:
               //  -> print number of clumps not tidally disrupted vs number drawn
               if (gMW_SUBS_TABULATED_IS)
                  printf("           => among which, %d are disrupted and %d not disrupted (from tabulated semi-analytical models)\n", n_disrupted, n_nondisrupted);
            } else printf("            => no need to draw any clump!\n");
#if IS_ROOT
            delete f_lalphabeta;
            f_lalphabeta = NULL;
#endif
         }

         if (gSIM_IS_PRINT) fclose(fp_drawn);
         // Print on screen sorted Jcl and Jcl/JGal.bkd values
         // N.B.: after this call, list_jcl are sorted by j values,
         //       and list_contrastcl by contrast values (their index
         //       is still available as these two objects are pairs).
         printf("\n   * Clumps sorted j_cl and contrast\n");
         pop_study_sort_j(list_clumps, list_jcl, list_contrastcl, dummy, user_rse / 100.);
         cout << endl << endl;

         // statistical realization:
         // Get min and max for statistical realizations histogram borders from "first" realization above:
         //int n_j_stat = list_clumps.size();

#if IS_ROOT
         if (gSTAT_N_REALIZATIONS != 0) {

            int n_j_stat = list_clumps.size();

            nb_to_draw_stat = new int[gSTAT_N_REALIZATIONS];

            for (int k = 0; k < n_j_stat; ++k) {
               if (log10(list_jcl[k].first) < jlogbin_stat_min) jlogbin_stat_min = log10(list_jcl[k].first);
               if (log10(list_jcl[k].first) > jlogbin_stat_max) jlogbin_stat_max = log10(list_jcl[k].first);
            }
            jlogbin_stat_min = round(jlogbin_stat_min) - 0.5;
            jlogbin_stat_max = round(jlogbin_stat_max) + 2.5;
            nbins_stat = int(jlogbin_stat_max - jlogbin_stat_min) * 10;


            cout << "    * repeat computation of Jcl_center in " << gSTAT_N_REALIZATIONS << " statistical realizations" << endl;

            // status report:
            /*               progress = (100 * l) / num_realizations_stat  ;
                           if (num_realizations_stat > 1e4 && progress == percentage) {
                              printf("        ... %d%% done ...\n", progress);
                              percentage += 5 ;
                           } else  {
                              printf("        ... %d%% done ...\n", progress);
                              percentage += 10 ;
                           } ;*/
            for (int k = 0; k < kmax; ++k) {
               // this takes a while to create, that is the reason why we loop over the mass decades,
               // and then over the realizations
               TF3 *f_lalphabeta_stat = new TF3("f_lalphabeta_stat", dpdv_lcosalphabeta, lmin, l_crit[k],
                                                MY_COS(pixdist_max), 1., 0., 2 * PI, 10);
               f_lalphabeta_stat->SetNpx(5000);
               f_lalphabeta_stat->SetNpy(30);
               f_lalphabeta_stat->SetNpz(30);
               f_lalphabeta_stat->SetParameters(par_galdpdv_modif);

               int nb_to_draw_stat_decade = 0;
               TRandom r(gSIM_SEED);
               for (int n = 0; n < gSTAT_N_REALIZATIONS; ++n) {
                  if (nb_mean_to_draw[k] > 0) {
                     nb_to_draw_stat[n] = r.Poisson(nb_mean_to_draw[k]);
                  } else
                     nb_to_draw_stat[n] = 0;
                  nb_to_draw_stat_decade += nb_to_draw_stat[n];
               }

               printf("        ... decade %d of %d (%.1f --> %.1f):  ", k + 1, kmax, log10(m1[k]), log10(m2[k]));
               printf("%d clumps to draw\n", nb_to_draw_stat_decade);

               for (int n = 0; n < gSTAT_N_REALIZATIONS; ++n) {
                  if (nb_to_draw_stat[n] >= 1) {
                     for (int i = 0; i < nb_to_draw_stat[n]; ++i) {
                        double l_cl_stat, cos_alpha_cl_stat, beta_cl_stat, psi_cl_stat, theta_cl_stat, r_cl_stat, x_cl_stat;
                        f_lalphabeta_stat->GetRandom3(l_cl_stat, cos_alpha_cl_stat, beta_cl_stat); // between lmin and l_crit[k]
                        double par_tmp_stat[2], x_stat[3];
                        x_stat[0] = l_cl_stat;
                        x_stat[1] = acos(cos_alpha_cl_stat);
                        x_stat[2] = beta_cl_stat;
                        par_tmp_stat[0] = psi;
                        check_psi(par_tmp_stat[0]);
                        par_tmp_stat[1] = theta;
                        lalphabeta_to_lpsitheta(x_stat, par_tmp_stat);
                        psi_cl_stat = x_stat[1];
                        check_psi(psi_cl_stat);
                        theta_cl_stat = x_stat[2];
                        x_stat[1] = acos(cos_alpha_cl_stat);
                        x_stat[2] = beta_cl_stat;
                        lalphabeta_to_rpsitheta(x_stat, par_tmp_stat);
                        r_cl_stat = x_stat[0];
                        x_cl_stat = r_cl_stat / gMW_RMAX;
                        pointing ptg_cl_stat(PI / 2 - theta_cl_stat, psi_cl_stat) ;
                        int i_pix_closest_stat = hp_gridprop.ang2pix(ptg_cl_stat) ;
                        if (!rs_pix_fov.contains(i_pix_closest_stat)) continue;
                        ++num_clumps_stat;
                        double log_mass_cl_stat = f_mass->GetRandom(log10(m1[k]), log10(m2[k]));

                        if (std::isinf(float(log_mass_cl_stat)) || isnan(float(log_mass_cl_stat))) {
                           sprintf(char_tmp, "Found nan-value for log_mass_cl in mass decade (%.1f --> %.1f). Throw random generator again...",
                                   log10(m1[k]),  log10(m2[k]));
                           print_warning("janalysis.cc", "gal_j2D()", string(char_tmp));
                           int try_n_times = 0;
                           while (std::isinf(float(log_mass_cl_stat)) || isnan(float(log_mass_cl_stat))) {
                              log_mass_cl_stat = f_mass->GetRandom(log10(m1[k]), log10(m2[k]));
                              try_n_times++;
                              if (try_n_times == 100) break;
                           }
                           if (try_n_times == 100) {
                              print_warning("janalysis.cc", "gal_j2D()", "Rethrowing random generator failed. Discard clump.");
                              continue;
                           }
                        }

                        double mass_cl_stat = pow(10., log_mass_cl_stat);
                        double c_stat = 0.;
                        if (gDM_FLAG_CDELTA_DIST != kDIRAC) {
                           double cvir_mean_stat = mdelta_to_cdelta(mass_cl_stat, Delta_c, gMW_SUBS_FLAG_CDELTAMDELTA, &par_galsubs[0], 0, x_cl_stat);

                           const int n_par_c_stat = 3;
                           double par_conc_stat[n_par_c_stat] = {cvir_mean_stat, gDM_LOGCDELTA_STDDEV, (double)gDM_FLAG_CDELTA_DIST};
                           TF1 *f_c_stat = new TF1("f_c_stat", dpdc, 0.01 * cvir_mean_stat, 100.*cvir_mean_stat, 3);
                           f_c_stat->SetNpx(100);
                           f_c_stat->SetParameters(par_conc_stat);
                           c_stat = f_c_stat->GetRandom();
                           delete f_c_stat;
                           f_c_stat = NULL;
                        }
                        const int npar = 10;
                        double par_cl_stat[npar];
                        mdelta_to_par(par_cl_stat, mass_cl_stat, Delta_c, (int)par_galsubs[10] /*card cdelta-mdelta*/, &par_galsubs[0],
                                      gSIM_EPS, 0. /*redshift*/, c_stat, x_cl_stat);
                        par_cl_stat[7] = l_cl_stat;
                        par_cl_stat[8] = psi_cl_stat;
                        par_cl_stat[9] = theta_cl_stat;
                        double j_cl_centre_stat =  jsmooth(par_cl_stat, psi_cl_stat,  theta_cl_stat, gSIM_EPS);

                        convert_to_PP_units(1, j_cl_centre_stat);

                        list_jcl_stat_all.push_back(j_cl_centre_stat);
                        list_jcl_stat_realizations[n].push_back(j_cl_centre_stat);
                     }
                  }
               }
               delete f_lalphabeta_stat;
               f_lalphabeta_stat = NULL;
            }
            delete[] nb_to_draw_stat;
            nb_to_draw_stat = NULL;
            cout << "        => average # of clumps in FOV in this statistical sample: <N> = " << double(num_clumps_stat) / double(gSTAT_N_REALIZATIONS) << endl;
         }

         delete f_mass;
         f_mass = NULL;
#endif
      } else {
         // If no clumps drawn, calculate <Jcl> only  //
         //-------------------------------------------//
         // (using interpolation to speed-up calculation when required)

         printf("    * <subs> contribution\n");
         // Dummy kmax, l_crit, m1 and m2 values formatted as required by j_interp
         vector<double> l_crit, m1, m2;
         int kmax = 1;
         l_crit.push_back(max(0., par_galtot[7] - par_galtot[6]));
         m1.push_back(mmin_subs);
         m2.push_back(mmax_subs);

         if (smooth_gamma_or_nu_or_both == 0 || n_pix_fov == n_pix) {
            j_interpolation(&jgal_sub_continuum[0], rs_pix_fov, grid_mode, psi, theta, dtheta_orth, dtheta,
                            1, m_gal, par_galtot, gSIM_EPS, f_dm, par_galdpdv,
                            par_galsubs, ntot_subs, kmax, &l_crit[0], &m1[0], &m2[0]);
         } else {
            j_interpolation(&jgal_sub_continuum_beam[0], rs_pix_fov_extended, grid_mode_extended, psi, theta,
                            dtheta_orth_extended, dtheta_extended,
                            1, m_gal, par_galtot, gSIM_EPS, f_dm, par_galdpdv,
                            par_galsubs, ntot_subs, kmax, &l_crit[0], &m1[0], &m2[0]);
            // copy values on extended grid into jgal_sub_continuum array:
            for (int i = 0; i < n_pix_fov; ++i) {
               int hp_index = v_pix_fov[i] ;
               int i_extended = binary_search(v_pix_fov_extended, hp_index);
               jgal_sub_continuum[i] = jgal_sub_continuum_beam[i_extended];
            }
         }

         if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
            printf("    * cross-product contribution\n");
            if (smooth_gamma_or_nu_or_both == 0 || n_pix_fov == n_pix) {
               j_interpolation(&jgal_crossprod[0], rs_pix_fov, grid_mode, psi, theta, dtheta_orth, dtheta,
                               2, m_gal, par_galtot, gSIM_EPS, f_dm, par_galdpdv,
                               par_galsubs, ntot_subs, kmax, &l_crit[0], &m1[0], &m2[0]);
            } else {
               j_interpolation(&jgal_crossprod_beam[0], rs_pix_fov_extended, grid_mode_extended, psi, theta,
                               dtheta_orth_extended, dtheta_extended,
                               2, m_gal, par_galtot, gSIM_EPS, f_dm, par_galdpdv,
                               par_galsubs, ntot_subs, kmax, &l_crit[0], &m1[0], &m2[0]);
               // copy values on extended grid into jgal_crossprod array:
               for (int i = 0; i < n_pix_fov; ++i) {
                  int hp_index = v_pix_fov[i] ;
                  int i_extended = binary_search(v_pix_fov_extended, hp_index);
                  jgal_crossprod[i] = jgal_crossprod_beam[i_extended];
               }
            }
         }
      }
   }


   /***********************************************/
   /** OPTION 1 or 3: ADD J FROM OBJECTS IN LIST **/
   /***********************************************/
   // Pairs <J,i> and <J/Jgal,i> for further usage (print & draw)
   vector<pair<double, int> > list_jhalos, list_contrasthalos, list_boosthalos;
   vector<struct gStructHalo> list_halos_in_map;


   // The user-defined list of DM objects (e.g., dSphs, galaxy clusters) is added
   if (is_list_added) {
      printf("    * calculating list component (%s)\n", gLIST_HALOES.c_str());

      // Load list of 'objects' is to be used
      cout << endl;
      vector<struct gStructHalo> list_halos;
      halo_load_list(gLIST_HALOES, list_halos);
      cout << endl;

      //--- Open file to store J for all objects in the list
      FILE *fp_list = NULL;

      if (gSIM_IS_PRINT) {
         fp_list = fopen(filename_list_str.c_str(), "w");

         // System independent-name for gLIST_HALOES
         string clumpy_path = get_path("$CLUMPY");
         string list_halo_compact = gLIST_HALOES;
         if (list_halo_compact.find(clumpy_path) != string::npos and clumpy_path != "") {
            if (*clumpy_path.rbegin() == '/') list_halo_compact = "$CLUMPY" + list_halo_compact.substr(clumpy_path.size() - 1);
            else list_halo_compact = "$CLUMPY/" + list_halo_compact.substr(clumpy_path.size());
         }

         fprintf(fp_list, "# This file lists the properties and %cobj(centre) for all objects in %s\n", j_or_d, list_halo_compact.c_str());
         fprintf(fp_list, "# Name           Type    long    lat      d     z   Rdelta   |     rhos       rs     prof.   #1   #2   #3   |      %c         %c/%ccontinuum | Mdelta\n",
                 j_or_d, j_or_d, j_or_d);
         fprintf(fp_list, "#  -               -     [deg]   [deg]  [kpc]   -    [kpc]   |  [Msol/kpc3]  [kpc]  [enum]    -    -    -   |       %s              -        | [Msol]\n", unit.c_str());
         fprintf(fp_list, "#\n");
      }

      // Loop on all objects in list
      unsigned int counter_list_in = 0;
      for (int i = 0; i < (int)list_halos.size(); ++i) {
         double l_halo = list_halos[i].l;
         double psi_halo = list_halos[i].PsiDeg * DEG_to_RAD;
         check_psi(psi_halo);
         double theta_halo = list_halos[i].ThetaDeg * DEG_to_RAD;

         // Find the closest Pixel to the clump centre
         // N.B.: in 2D representation, the user can ask for psi angles >180 deg or <-180 deg (but not both at the same time)
         // Actually, the correction to add 2pi or subtract 2pi on psi_cl should
         // not be neccessary anymore as it is checked differently than before if the
         // clump is in the FOV. We do it nevertheless:
         if (psi_min < -PI && psi_halo > psi_max) psi_halo -= (2.*PI);
         if (psi_max > PI && psi_halo < psi_min) psi_halo += (2.*PI);
         check_psi(psi_halo) ;

         pointing ptg_cl(PI / 2 - theta_halo, psi_halo) ;

         int i_pix_closest = hp_gridprop.ang2pix(ptg_cl) ;

         // if object in map, do the calculation
         if (rs_pix_fov.contains(i_pix_closest)) {

            bool is_subs_list = false;
            if (list_halos[i].Subs_mfrac > 0) is_subs_list = true;
            if (is_subs_list)
               cout << "    => add " << list_halos[i].Name << " (" <<  list_halos[i].Type << ") from list (smooth+subs in object) in FOV" << endl;
            else
               cout << "    => add " << list_halos[i].Name << " (" <<  list_halos[i].Type << ") from list (smooth contrib.) in FOV" << endl;

            // Set halo parameters (and triaxiality)
            const int npar = 10;
            const int nparbis = 25;
            double par_list_tot[npar + 1];
            double par_list_dpdv[npar];
            double par_list_subs[nparbis];
            double nsub_tot = 0.;
            halo_set_partot(i, par_list_tot, list_halos, false);

            if (list_halos[i].Triaxial_Is) {
               print_warning("janalysis.cc", "gal_j2D()", "Triaxiality of halo from list is neglected "
                             "(triaxiality not implemented when adding halo on top of another halo).");
               list_halos[i].Triaxial_Is = false;
            }
            halo_set_triaxiality(i, list_halos);

            if (is_subs_list) {
               halo_set_pardpdv(i, par_list_dpdv, list_halos, false);
               halo_set_parsubs(i, par_list_subs, list_halos);
               double mmax = list_halos[i].Subs_Mmax;
               double mhalotot_subs = list_halos[i].Mtot * list_halos[i].Subs_mfrac;
               nsub_tot = nsubtot_from_msubtot(mhalotot_subs, gDM_SUBS_MMIN, mmax, &par_list_subs[8], gSIM_EPS);
            } else {
               list_halos[i].Subs_mfrac = 0.;
            }

            // Calculate halo signal (pixel closest to halo centre) and boost (if applicable)

            pointing ptg_i_pix_closest = hp_gridprop.pix2ang(i_pix_closest);
            double psi_ipix_closest = ptg_i_pix_closest.phi;
            //double theta_i_pix_closest = PI / 2 - ptg_i_pix_closest.theta;
            check_psi(psi_ipix_closest);

            // compute the flux in the central pixel
            // do it with psi_ipix_closest, theta_i_pix_closest or with psi_cl, theta_cl???
            double jhalo_centre =  halo_jtot(i, list_halos, psi_halo, theta_halo, gSIM_EPS, false); // =  jsmooth(par_list_tot, psi_halo, theta_halo, gSIM_EPS);
            // Set 'no clump' in halo to enable the boost calculation (if applicable)
            if (gPP_DM_IS_ANNIHIL_OR_DECAY && list_halos[i].Subs_mfrac > 1.e-5) {
               double tmp_f =  list_halos[i].Subs_mfrac;
               list_halos[i].Subs_mfrac = 0.;
               double jhalo_tot_nosubs = halo_jtot(i, list_halos, psi_halo, theta_halo, gSIM_EPS, false);
               list_halos[i].Subs_mfrac = tmp_f;
               double boost = jhalo_centre / jhalo_tot_nosubs;
               tmp_pair = make_pair(boost, i);
               list_boosthalos.push_back(tmp_pair);
            }

            // find vector index of i_pix_closest
            int index_i_pix_closest = binary_search(v_pix_fov, i_pix_closest);
            double jgal_continuum = jgal_sm[index_i_pix_closest];
            if (is_subs) {
               jgal_continuum += jgal_sub_continuum[index_i_pix_closest];
               if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                  jgal_continuum += jgal_crossprod[index_i_pix_closest];
            }

            // Form pairs (to keep indices of sorted vectors)
            tmp_pair = make_pair(jhalo_centre / jgal_continuum, counter_list_in);
            list_contrasthalos.push_back(tmp_pair);
            double j_fraction = jhalo_centre / jgal_continuum;

            convert_to_PP_units(1, jhalo_centre);
            tmp_pair = make_pair(jhalo_centre, counter_list_in);
            list_jhalos.push_back(tmp_pair);

            list_halos_in_map.push_back(list_halos[i]);
            ++counter_list_in;

            // convert printed quantities to PP units, if applies:
            double rhos_list = par_list_tot[0];
            // convert rho_0 back to rho_s as used in profiles.cc for ZHAO profile:
            if (int(par_list_tot[5]) == kZHAO) rhos_list /= pow(2, (par_list_tot[3] - par_list_tot[4]) / par_list_tot[2]);

            double z_print = list_halos[i].z;
            double d_print = -1.;
            if (z_print < SMALL_NUMBER) {
               z_print = -1.;
               d_print = l_halo;
            }

            // print in file
            if (gSIM_IS_PRINT) {
               string tmp = list_halos[i].Name + " " + list_halos[i].Type;
               fprintf(fp_list, "%-25s %+7.2f %+7.2f   %.*le %.f %.*le    %g %g k%s   %g   %g   %g      %.*le %.*le     %.*le\n",
                       tmp.c_str(), psi_halo * RAD_to_DEG, theta_halo * RAD_to_DEG,
                       gSIM_SIGDIGITS, d_print, z_print,
                       gSIM_SIGDIGITS, list_halos[i].Rvir * (1 + list_halos[i].z),
                       rhos_list / pow(1 + list_halos[i].z, 3), list_halos[i].Rscale * (1 + list_halos[i].z),
                       gNAMES_PROFILE[int(par_list_tot[5])], par_list_tot[2], par_list_tot[3], par_list_tot[4],
                       gSIM_SIGDIGITS, jhalo_centre, gSIM_SIGDIGITS,  j_fraction, gSIM_SIGDIGITS, list_halos[i].Mtot);
            }

            // Add all pixels shining above Jcontinuum in the map
            add_halo_in_map(par_list_tot, gSIM_EPS_DRAWN, j_list_tot, jgal_continuum, rs_pix_fov,
                            is_subs_list, list_halos[i].Mtot, list_halos[i].Subs_mfrac, par_list_dpdv,
                            par_list_subs, nsub_tot);
            if (list_halos[i].z > ZMIN_EXTRAGAL) {
               //cout << "    => omit " << list_halos[i].Name << " [" <<  list_halos[i].Type << "] from list: Can add only halos with redshift z <= 0.001." << endl;
               print_warning("janalysis.cc", "gal_j2D()", "Halo " + list_halos[i].Name + " (" +  list_halos[i].Type
                             + ") has non-negligible redshift. Be careful when later compute the spectrum based on the added J-factors.");
            }
         }
      }
      if (gSIM_IS_PRINT) fclose(fp_list);
      // Print on screen sorted Jhalo and Jhalo/JGal.bkd values
      // N.B.: after this call, list_j are sorted by j values,
      //       and list_contrast by contrast values (their index
      //       is still available as these two objects are pairs).
      pop_study_sort_j(list_halos_in_map, list_jhalos, list_contrasthalos, list_boosthalos, user_rse / 100.);
   }

   /********************* EVALUATE STATISTICAL REALIZATIONS *******************/
#if IS_ROOT
   double prob_min = 1;
   TH1D **h_stat = NULL;
   vector< vector< double > > v_v_jbins_stat(nbins_stat, vector<double> (9, 0.));
   // 0st row: bin center
   // 1st row: d<N>/dJ (<N> in Jbin)
   // 2nd row: Delta d<N>/dJ
   // 3rd row: <N_cumulative> (<N> >= Jbin)
   // 4th row: Delta <N_cumulative>
   // 5th row: p(>= 1 clump (>=J) )
   // 6th row: Delta p
   // 7th row: flux
   // 8th row: flux in Crab units.
   if (gSTAT_N_REALIZATIONS != 0) {

      // get Crab flux for comparison:
      TF1 *crab_spectrum = new TF1("crab", "3.27e-11 * x**(-2.4 - 0.15 * log10(x))", 0.01, 100); // limits in TeV

      double crab_flux;

      if (gSIM_FLUX_IS_INTEG_OR_DIFF) crab_flux = crab_spectrum->Integral(gSIM_FLUX_AT_E_GEV / 1000., 100.);
      else crab_flux = crab_spectrum->Eval(gSIM_FLUX_AT_E_GEV / 1000.); //, 0, 0, 0);

      // Poissonian probability distribution:
      h_stat = new TH1D*[3];
      h_stat[0] = new TH1D("h_subs_logn_logj_stat", "h_subs_logn_logj_stat", nbins_stat, jlogbin_stat_min, jlogbin_stat_max);
      h_stat[1] = new TH1D("h_subs_logn_logj_stat_cumul", "h_subs_logn_logj_stat_cumul", nbins_stat, jlogbin_stat_min, jlogbin_stat_max);
      h_stat[2] = new TH1D("h_subs_logprob_logj_stat", "h_subs_logprob_logj_stat", nbins_stat, jlogbin_stat_min, jlogbin_stat_max);
      double n_cumul = 0.;

      // convert list_jcl_stat_all into correct units:


      for (tsize k = 0; k < list_jcl_stat_all.size(); ++k)
         h_stat[0]->Fill(log10(list_jcl_stat_all[k]));
      h_stat[0]->Scale(1. / double(gSTAT_N_REALIZATIONS));
      // delete list_jcl_stat_all vector
      vector<double>().swap(list_jcl_stat_all);
      // Get bins and bin contents to store into 2D vector:
      for (int j = 1; j < nbins_stat + 1; ++j) {
         // Poisson PDF (the discrete one!):
         double upperlimit = 3.e1; // adjust the numeric upper integration limit such that
         // we get a sufficiently good precision < 1e-3
         TF1 *poisson_pdf = new TF1("poisson", "TMath::PoissonI(x,[0])", 0, upperlimit);
         v_v_jbins_stat[j - 1][0] = h_stat[0]->GetBinCenter(j);
         v_v_jbins_stat[j - 1][1] = h_stat[0]->GetBinContent(j);
         // set error of the mean:
         double delta_n = sqrt(h_stat[0]->GetBinContent(j) / double(gSTAT_N_REALIZATIONS));
         v_v_jbins_stat[j - 1][2] = delta_n;
         h_stat[0]->SetBinError(j, delta_n);
         n_cumul += h_stat[0]->GetBinContent(nbins_stat + 1 - j);
         h_stat[1]->SetBinContent(nbins_stat + 1 - j, n_cumul);
         v_v_jbins_stat[nbins_stat - j][3] = n_cumul;
         double delta_n_cumul = sqrt(n_cumul / double(gSTAT_N_REALIZATIONS));
         v_v_jbins_stat[nbins_stat - j][4] = delta_n_cumul;
         h_stat[1]->SetBinError(nbins_stat + 1 - j, delta_n_cumul);
         double prob, delta_prob;
         if (n_cumul > 10.) {
            prob = 1.; // is nearly 1 for any meaningful precision.
            delta_prob = 0.; // error is also nearly zero.
         } else {
            poisson_pdf->SetParameter(0, n_cumul); // Poisson PDF with <N> = n_cumul
            // integrate Poisson PDF from 1 to infinity, to get prob p(>= 1 clump(>= J))
            prob = poisson_pdf->Integral(1., upperlimit);

            poisson_pdf->SetParameter(0, n_cumul - delta_n_cumul);
            delta_prob = fabs(prob - poisson_pdf->Integral(1., upperlimit));
         }
         if (prob < prob_min && prob < 0.1 && prob > 1e-10) prob_min = prob;
         v_v_jbins_stat[nbins_stat - j][5] = prob; // is nearly 1 for any meaningful precision.
         v_v_jbins_stat[nbins_stat - j][6] = delta_prob;
         h_stat[2]->SetBinContent(nbins_stat + 1 - j, prob);
         h_stat[2]->SetBinError(nbins_stat + 1 - j, delta_prob);

         // calculate flux units:
         // If user-unit is ASTRO, we need to convert here (flux requires PP unit)
         bool unit_ref = gSIM_IS_ASTRO_OR_PP_UNITS;

         double par_spec[6];
         par_spec[0] = gPP_DM_MASS_GEV;
         par_spec[1] = gPP_FLAG_SPECTRUMMODEL;
         par_spec[2] = kGAMMA;
         par_spec[3] = 0.;
         par_spec[4] = 0.;
         par_spec[5] = 0.;

         double j_bin =  pow(10., v_v_jbins_stat[j - 1][0]);

         v_v_jbins_stat[j - 1][7] = flux(par_spec, gSIM_FLUX_AT_E_GEV, j_bin, gSIM_FLUX_IS_INTEG_OR_DIFF);

         // Watch out: if user-unit is ASTRO, we need to convert here (flux requires PP unit)
         if (gSIM_IS_ASTRO_OR_PP_UNITS) {
            gSIM_IS_ASTRO_OR_PP_UNITS = false;
            convert_to_PP_units(1, v_v_jbins_stat[j - 1][7]);
         }
         gSIM_IS_ASTRO_OR_PP_UNITS = unit_ref;
      }

      // write to file:
      //--- Open file to store J for all objects in the list
      FILE *fp_stat = NULL;
      if (grid_mode == "DISK")
         sprintf(fname_stat, "%s%s_LOS%.0f,%.0f_FOVdiameter%.1fdeg_rse%.0f_alphaint%.2fdeg_nside%d_Nrep%d.stat",
                 gSIM_OUTPUT_DIR.c_str(), out_name.c_str(), psi * RAD_to_DEG, theta * RAD_to_DEG, dtheta * RAD_to_DEG,
                 user_rse, gSIM_ALPHAINT * RAD_to_DEG, gSIM_HEALPIX_NSIDE, gSTAT_N_REALIZATIONS);
      else
         sprintf(fname_stat, "%s%s_LOS%.0f,%.0f_FOV%.0fx%.0f_rse%.0f_alphaint%.2fdeg_nside%d_Nrep%d.stat",
                 gSIM_OUTPUT_DIR.c_str(), out_name.c_str(), psi * RAD_to_DEG, theta * RAD_to_DEG, dtheta_orth * RAD_to_DEG, dtheta * RAD_to_DEG,
                 user_rse, gSIM_ALPHAINT * RAD_to_DEG, gSIM_HEALPIX_NSIDE, gSTAT_N_REALIZATIONS);

      if (gSIM_IS_PRINT) {
         fp_stat = fopen(fname_stat, "w");
         fprintf(fp_stat, "# This file lists the binned amounts of Jobj(centre) within the FOV for %d statistical repetitions\n", gSTAT_N_REALIZATIONS);
         if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
            if (gSIM_IS_ASTRO_OR_PP_UNITS) fprintf(fp_stat, "#   log(J_bincenter/(Msun^2/kpc^5))    <N>     Delta<N>     <N_cumul>     Delta<N_cumul>    p(>= 1 clump(>= J))   Delta p(>= 1 clump(>= J))   Flux   Flux in C.U.\n");
            else fprintf(fp_stat, "#   log(J_bincenter/(GeV^2/cm^5))    <N>     Delta<N>     <N_cumul>     Delta<N_cumul>    p(>= 1 clump(>= J))   Delta p(>= 1 clump(>= J))   Flux   Flux in C.U.\n");
            fprintf(fp_stat, "#\n");
         } else {
            if (gSIM_IS_ASTRO_OR_PP_UNITS) fprintf(fp_stat, "#   log(D_bincenter/(Msun/kpc^3))    <N>     Delta<N>     <N_cumul>     Delta<N_cumul>    p(>= 1 clump(>= J))   Delta p(>= 1 clump(>= J))   Flux   Flux in C.U.\n");
            else fprintf(fp_stat, "#   log(D_bincenter/(GeV/cm^2))    <N>     Delta<N>     <N_cumul>     Delta<N_cumul>    p(>= 1 clump(>= J))   Delta p(>= 1 clump(>= J))   Flux   Flux in C.U.\n");
            fprintf(fp_stat, "#\n");
         }
         for (int j = 0; j < nbins_stat; ++j)
            fprintf(fp_stat, "%le %le %le %le %le %le %le %le %le\n",
                    v_v_jbins_stat[j][0], v_v_jbins_stat[j][1], v_v_jbins_stat[j][2],
                    v_v_jbins_stat[j][3], v_v_jbins_stat[j][4], v_v_jbins_stat[j][5],
                    v_v_jbins_stat[j][6], v_v_jbins_stat[j][7], v_v_jbins_stat[j][7] / crab_flux) ;
         fclose(fp_stat);
      }
   }
#endif
   /******************* CONVERT ALL ARRAYS TO CORRECT UNITS ********************/

   convert_to_PP_units(1, n_pix_fov, &jgal_sm[0]);
   if (is_list_added) convert_to_PP_units(1, n_pix_fov, &j_list_tot[0]);
   if (is_subs) {
      convert_to_PP_units(1, n_pix_fov, &jgal_sub_continuum[0]);
      if (gPP_DM_IS_ANNIHIL_OR_DECAY)
         convert_to_PP_units(1, n_pix_fov, &jgal_crossprod[0]);
      if (is_subs_drawn)
         convert_to_PP_units(1, n_pix_fov, &jgal_sub_drawn[0]);
   }
   // with instrumental beam:
   if (smooth_gamma_or_nu_or_both != 0 && n_pix_fov != n_pix) {
      convert_to_PP_units(1, n_pix_fov_extended, &jgal_sm_beam[0]);
      if (is_subs) {
         convert_to_PP_units(1, n_pix_fov_extended, &jgal_sub_continuum_beam[0]);
         if (gPP_DM_IS_ANNIHIL_OR_DECAY)
            convert_to_PP_units(1, n_pix_fov_extended, &jgal_crossprod_beam[0]);
      }
   }

   /******************* WRITE FIRST TWO EXTENSIONS TO FILE  ********************/

   cout << endl;
   if (is_subs && is_subs_drawn && gSIM_IS_PRINT)
      cout << "  ... drawn clumps [ASCII] written in: " << filename_drawn_str << endl;
   if (is_list_added && gSIM_IS_PRINT)
      cout << "  ... drawn objects [ASCII] from list written in: " << filename_list_str << endl;

   // write first extension (this information will also be used for ROOT plotting):
   // first, fill all arrays:
   for (int i = 0; i < n_pix_fov; ++i) {
      j_tot[i] = jgal_sm[i];
      if (!is_subs) {
         if (is_list_added)
            j_tot[i] += j_list_tot[i];
      } else {
         if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
            j_tot[i] += jgal_sub_continuum[i] + jgal_crossprod[i];
            if (switch_j == 1) {
               j_tot[i] += j_list_tot[i];
            } else if (switch_j == 2) {
               j_tot[i] += jgal_sub_drawn[i];
            } else if (switch_j == 3) {
               j_tot[i] += jgal_sub_drawn[i] + j_list_tot[i];
            }
         } else {
            j_tot[i] += jgal_sub_continuum[i];
            if (switch_j == 1) {
               j_tot[i] += j_list_tot[i];
            } else if (switch_j == 2) {
               j_tot[i] += jgal_sub_drawn[i];
            } else if (switch_j == 3) {
               j_tot[i] += jgal_sub_drawn[i] + j_list_tot[i];
            }
         }
      }
   }

   double int_area = 2. * PI * (1 - cos(gSIM_ALPHAINT)) ;
   double jrescalefac = gSIM_HEALPIX_DELTAOMEGA / int_area;

   // rescale J-factors to pixel area:
   for (int i = 0; i < n_pix_fov; ++i) {
      if (jgal_sm[i] > 1.e-40) {
         jgal_sm[i] *= jrescalefac ;
         j_tot[i] *= jrescalefac ;
         if (is_list_added)
            j_list_tot[i] *= jrescalefac ;
         if (is_subs) {
            jgal_sub_continuum[i] *= jrescalefac ;
            if (gPP_DM_IS_ANNIHIL_OR_DECAY)
               jgal_crossprod[i] *= jrescalefac ;
            if (is_subs_drawn)
               jgal_sub_drawn[i] *= jrescalefac ;
         }
      }
   }

   if (smooth_gamma_or_nu_or_both != 0 && n_pix_fov != n_pix) {
      for (int i = 0; i < n_pix_fov_extended; ++i) {
         if (jgal_sm_beam[i] > 1.e-40) {
            jgal_sm_beam[i] *= jrescalefac ;
            if (is_subs) {
               jgal_sub_continuum_beam[i] *= jrescalefac ;
               if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                  jgal_crossprod_beam[i] *= jrescalefac ;
            }
         }
      }
   }

   //--- Array declarations and initialisation for 2nd extension:
   //    All values divided by the pixel size -> Jfactor per steradian
   vector<double> j_tot_sr;
   vector<double> jgal_sm_sr;
   vector<double> jgal_sub_continuum_sr;
   vector<double> jgal_crossprod_sr;
   vector<double> jgal_sub_drawn_sr;
   vector<double> j_list_tot_sr;

   if (gSIM_IS_PRINT) {

      // fill arrays for second extension only in case of gSIM_IS_PRINT:
      j_tot_sr.assign(n_pix_fov, 1.e-40);
      jgal_sm_sr.assign(n_pix_fov, 1.e-40);

      // Create additional arrays only if required
      if (is_list_added)
         j_list_tot_sr.assign(n_pix_fov, -1.e-40);
      if (is_subs) {
         jgal_sub_continuum_sr.assign(n_pix_fov, 1.e-40);
         if (gPP_DM_IS_ANNIHIL_OR_DECAY)
            jgal_crossprod_sr.assign(n_pix_fov, 1.e-40);
         if (is_subs_drawn)
            jgal_sub_drawn_sr.assign(n_pix_fov, -1.e-40);
      }

      for (int i = 0; i < n_pix_fov; ++i) {
         if (jgal_sm[i] > 1.e-40) {
            jgal_sm_sr[i] = jgal_sm[i] / gSIM_HEALPIX_DELTAOMEGA ;
            j_tot_sr[i] = j_tot[i] / gSIM_HEALPIX_DELTAOMEGA ;
            if (is_list_added)
               j_list_tot_sr[i] = j_list_tot[i] / gSIM_HEALPIX_DELTAOMEGA ;
            if (is_subs) {
               jgal_sub_continuum_sr[i] = jgal_sub_continuum[i] / gSIM_HEALPIX_DELTAOMEGA ;
               if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                  jgal_crossprod_sr[i] = jgal_crossprod[i] / gSIM_HEALPIX_DELTAOMEGA ;
               if (is_subs_drawn)
                  jgal_sub_drawn_sr[i] = jgal_sub_drawn[i] / gSIM_HEALPIX_DELTAOMEGA ;
            }
         }
      }
   }

   // set all negative values in 1st extension to positive 1e-40
   for (int i = 0; i < n_pix_fov; ++i) {
      if (is_list_added && j_list_tot[i] < 1e-40) j_list_tot[i] = 1e-40;
      if (is_subs && is_subs_drawn && jgal_sub_drawn[i] < 1e-40) jgal_sub_drawn[i] = 1e-40;
   }

   // set correct units for all columns:
   string j_units;
   string j_units_per_sr;
   if (gPP_DM_IS_ANNIHIL_OR_DECAY and gSIM_IS_ASTRO_OR_PP_UNITS) {
      j_units = "Msun^2 kpc^-5";
      j_units_per_sr = "Msun^2 kpc^-5 sr^-1";
   } else if (gPP_DM_IS_ANNIHIL_OR_DECAY and !gSIM_IS_ASTRO_OR_PP_UNITS) {
      j_units = "GeV^2 cm^-5";
      j_units_per_sr = "GeV^2 cm^-5 sr^-1";
   } else if (!gPP_DM_IS_ANNIHIL_OR_DECAY and gSIM_IS_ASTRO_OR_PP_UNITS) {
      j_units = "Msun kpc^-2";
      j_units_per_sr = "Msun kpc^-2 sr^-1";
   } else if (!gPP_DM_IS_ANNIHIL_OR_DECAY and !gSIM_IS_ASTRO_OR_PP_UNITS) {
      j_units = "GeV cm^-2";
      j_units_per_sr = "GeV cm^-2 sr^-1";
   } else {
      j_units = "Unknown (some error)";
      j_units_per_sr = "Unknown (some error)";
   }

   if (gSIM_IS_PRINT) {

      // set all negative values in 2nd extension to positive 1e-40
      for (int i = 0; i < n_pix_fov; ++i) {
         if (is_list_added && j_list_tot_sr[i] < 1e-40) j_list_tot_sr[i] = 1e-40;
         if (is_subs && is_subs_drawn && jgal_sub_drawn_sr[i] < 1e-40) jgal_sub_drawn_sr[i] = 1e-40;
      }


      // secondly, write arrays into fits file according to the performed simulation:
      if (!is_subs) {
         if (is_list_added) {
            fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot, jgal_sm, j_list_tot);
            if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
               fh_file_out.set_key("TTYPE2", string("Jtot"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jsmooth"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Jlist"),   "label for field 4");
               fh_file_out.set_key("EXTNAME", string("JFACTOR"), "name of this binary table extension");
            } else {
               fh_file_out.set_key("TTYPE2", string("Dtot"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dsmooth"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Dlist"),   "label for field 4");
               fh_file_out.set_key("EXTNAME", string("DFACTOR"), "name of this binary table extension");
            }
         } else {
            fits_write_clumpymaps(fh_file_out, v_pix_fov, jgal_sm) ;
            if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
               fh_file_out.set_key("TTYPE2", string("Jtot"),    "label for field 2");
               fh_file_out.set_key("EXTNAME", string("JFACTOR"), "name of this binary table extension");
            } else {
               fh_file_out.set_key("TTYPE2", string("Dtot"),   "label for field 2");
               fh_file_out.set_key("EXTNAME", string("DFACTOR"), "name of this binary table extension");
            }
         }
      } else {
         if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
            if (switch_j == 0) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot, jgal_sm, jgal_sub_continuum, jgal_crossprod) ;
               // set column names:
               fh_file_out.set_key("TTYPE2", string("Jtot"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jsmooth"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Jsub"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Jcrossp"), "label for field 5");
            } else if (switch_j == 1) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot, jgal_sm, jgal_sub_continuum, jgal_crossprod, j_list_tot) ;
               // set column names:
               fh_file_out.set_key("TTYPE2", string("Jtot"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jsmooth"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Jsub"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Jcrossp"), "label for field 5");
               fh_file_out.set_key("TTYPE6", string("Jlist"),   "label for field 6");
            } else if (switch_j == 2) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot, jgal_sm, jgal_sub_continuum, jgal_crossprod, jgal_sub_drawn) ;
               // set column names:
               fh_file_out.set_key("TTYPE2", string("Jtot"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jsmooth"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Jsub"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Jcrossp"), "label for field 5");
               fh_file_out.set_key("TTYPE6", string("Jdrawn"),  "label for field 6");
            } else if (switch_j == 3) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot, jgal_sm, jgal_sub_continuum, jgal_crossprod, jgal_sub_drawn, j_list_tot) ;
               // set column names:
               fh_file_out.set_key("TTYPE2", string("Jtot"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jsmooth"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Jsub"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Jcrossp"), "label for field 5");
               fh_file_out.set_key("TTYPE6", string("Jdrawn"),  "label for field 6");
               fh_file_out.set_key("TTYPE7", string("Jlist"),  "label for field 7");
            }
            // set correct extension name:
            fh_file_out.set_key("EXTNAME", string("JFACTOR"), "name of this binary table extension");
         } else {
            if (switch_j == 0) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot, jgal_sm, jgal_sub_continuum) ;
               // set column names:
               fh_file_out.set_key("TTYPE2", string("Dtot"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dsmooth"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Dsub"),    "label for field 4");
            } else if (switch_j == 1) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot, jgal_sm, jgal_sub_continuum, j_list_tot) ;
               // set column names:
               fh_file_out.set_key("TTYPE2", string("Dtot"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dsmooth"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Dsub"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Dlist"),   "label for field 5");
            } else if (switch_j == 2) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot, jgal_sm, jgal_sub_continuum, jgal_sub_drawn) ;
               // set column names:
               fh_file_out.set_key("TTYPE2", string("Dtot"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dsmooth"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Dsub"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Ddrawn"),  "label for field 5");
            } else if (switch_j == 3) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot, jgal_sm, jgal_sub_continuum, jgal_sub_drawn, j_list_tot) ;
               // set column names:
               fh_file_out.set_key("TTYPE2", string("Dtot"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dsmooth"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Dsub"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Ddrawn"),  "label for field 5");
               fh_file_out.set_key("TTYPE6", string("Dlist"),   "label for field 6");
            }
            // set correct extension name:
            fh_file_out.set_key("EXTNAME", string("DFACTOR"), "name of this binary table extension");
         }
      }
      // set correct units:
      // get number of columns finally written:
      int ncolumns = fh_file_out.ncols();
      for (int i = 2; i < ncolumns + 1; ++i) {
         sprintf(char_tmp, "TUNIT%d", i);
         fh_file_out.set_key(char_tmp, j_units, "physical unit of field");
      }

      // write second extension: All values divided by the pixel size -> Jfactor per steradian
      // write arrays into fits file according to the performed simulation:
      if (!is_subs) {
         if (is_list_added) {
            fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_sr, jgal_sm_sr, j_list_tot_sr) ;
            if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
               fh_file_out.set_key("TTYPE2", string("Jtot_per_sr"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jsmooth_per_sr"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Jlist_per_sr"),   "label for field 4");
               fh_file_out.set_key("EXTNAME", string("JFACTOR_PER_SR"), "name of this binary table extension");
            } else {
               fh_file_out.set_key("TTYPE2", string("Dtot_per_sr"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dsmooth_per_sr"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Dlist_per_sr"),   "label for field 4");
               fh_file_out.set_key("EXTNAME", string("DFACTOR_PER_SR"), "name of this binary table extension");
            }
         } else {
            fits_write_clumpymaps(fh_file_out, v_pix_fov, jgal_sm_sr) ;
            if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
               fh_file_out.set_key("TTYPE2", string("Jtot_per_sr"),    "label for field 2");
               fh_file_out.set_key("EXTNAME", string("JFACTOR_PER_SR"), "name of this binary table extension");
            } else {
               fh_file_out.set_key("TTYPE2", string("Dtot_per_sr"),   "label for field 2");
               fh_file_out.set_key("EXTNAME", string("DFACTOR_PER_SR"), "name of this binary table extension");
            }
         }
      } else {
         if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
            if (switch_j == 0) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_sr, jgal_sm_sr, jgal_sub_continuum_sr, jgal_crossprod_sr) ;
               // set column names:
               fh_file_out.set_key("TTYPE2", string("Jtot_per_sr"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jsmooth_per_sr"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Jsub_per_sr"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Jcrossp_per_sr"), "label for field 5");
            } else if (switch_j == 1) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_sr, jgal_sm_sr, jgal_sub_continuum_sr, jgal_crossprod_sr, j_list_tot_sr) ;
               // set column names:
               fh_file_out.set_key("TTYPE2", string("Jtot_per_sr"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jsmooth_per_sr"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Jsub_per_sr"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Jcrossp_per_sr"), "label for field 5");
               fh_file_out.set_key("TTYPE6", string("Jlist_per_sr"),   "label for field 6");
            } else if (switch_j == 2) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_sr, jgal_sm_sr, jgal_sub_continuum_sr, jgal_crossprod_sr, jgal_sub_drawn_sr) ;
               // set column names:
               fh_file_out.set_key("TTYPE2", string("Jtot_per_sr"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jsmooth_per_sr"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Jsub_per_sr"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Jcrossp_per_sr"), "label for field 5");
               fh_file_out.set_key("TTYPE6", string("Jdrawn_per_sr"),  "label for field 6");
            } else if (switch_j == 3) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_sr, jgal_sm_sr, jgal_sub_continuum_sr, jgal_crossprod_sr, jgal_sub_drawn_sr, j_list_tot_sr) ;
               // set column names:
               fh_file_out.set_key("TTYPE2", string("Jtot_per_sr"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jsmooth_per_sr"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Jsub_per_sr"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Jcrossp_per_sr"), "label for field 5");
               fh_file_out.set_key("TTYPE6", string("Jdrawn_per_sr"),  "label for field 6");
               fh_file_out.set_key("TTYPE7", string("Jlist_per_sr"),   "label for field 7");
            }
            // set correct extension name:
            fh_file_out.set_key("EXTNAME", string("JFACTOR_PER_SR"), "name of this binary table extension");
         } else {
            if (switch_j == 0) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_sr, jgal_sm_sr, jgal_sub_continuum_sr) ;
               // set column names:
               fh_file_out.set_key("TTYPE2", string("Dtot_per_sr"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dsmooth_per_sr"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Dsub_per_sr"),    "label for field 4");
            } else if (switch_j == 1) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_sr, jgal_sm_sr, jgal_sub_continuum_sr, j_list_tot_sr) ;
               // set column names:
               fh_file_out.set_key("TTYPE2", string("Dtot_per_sr"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dsmooth_per_sr"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Dlist_per_sr"),   "label for field 4");
            } else if (switch_j == 2) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_sr, jgal_sm_sr, jgal_sub_continuum_sr, jgal_sub_drawn_sr) ;
               // set column names:
               fh_file_out.set_key("TTYPE2", string("Dtot_per_sr"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dsmooth_per_sr"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Dsub_per_sr"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Ddrawn_per_sr"),  "label for field 5");
            } else if (switch_j == 3) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_sr, jgal_sm_sr, jgal_sub_continuum_sr, jgal_sub_drawn_sr, j_list_tot_sr) ;
               // set column names:
               fh_file_out.set_key("TTYPE2", string("Dtot_per_sr"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dsmooth_per_sr"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Dsub_per_sr"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Ddrawn_per_sr"),  "label for field 5");
               fh_file_out.set_key("TTYPE6", string("Dlist_per_sr"),   "label for field 6");
            }
            // set correct extension name:
            fh_file_out.set_key("EXTNAME", string("DFACTOR_PER_SR"), "name of this binary table extension");
         }
      }
      // set correct units for J per sr extension:
      for (int i = 2; i < ncolumns + 1; ++i) {
         sprintf(char_tmp, "TUNIT%d", i);
         fh_file_out.set_key(char_tmp, j_units_per_sr, "physical unit of field");
      }

      // clear memory:
      vector<double>().swap(j_tot_sr);
      vector<double>().swap(jgal_sm_sr);
      if (is_list_added)
         vector<double>().swap(j_list_tot_sr);
      if (is_subs) {
         vector<double>().swap(jgal_sub_continuum_sr);
         if (gPP_DM_IS_ANNIHIL_OR_DECAY)
            vector<double>().swap(jgal_crossprod_sr);
         if (is_subs_drawn)
            vector<double>().swap(jgal_sub_drawn_sr);
      }
   }

   /******************* SMOOTH MAPS IF BEAM WIDTHS ARE GIVEN ******************/

   // Additionally, the angular power spectrum is extracted at the same time if a full-sky map is given.
   // If the angular pwoer spectrum is extracted from a part-sky map, it is done always
   // separately, if or if no smoothing is applied.

   vector<double> j_tot_gammasmoothed;
   vector<double> j_tot_neutrinosmoothed;
   vector<double> j_tot_gammasmoothed_sr;
   vector<double> j_tot_neutrinosmoothed_sr;

   bool is_fullsky = false;
   if (n_pix_fov == n_pix) is_fullsky = true;

   PowSpec powerspec_j_tot ;
   T_Healpix_Base<int> hp_gridprop_fullsky;
   hp_gridprop_fullsky.SetNside(gSIM_HEALPIX_NSIDE, RING);

   string weights_dir = gSIM_HEALPIX_RING_WEIGHTS_DIR ;

   double j_tot_mean = 0.;

   // enter for smoothing:
   if (smooth_gamma_or_nu_or_both != 0) {
      // write Healpix map and output whole sky fits file:
      arr<double> arrhp_fullsky(n_pix, 0.);  // this takes a lot of memory...

      if (!is_fullsky) {
         j_tot_beam.assign(n_pix_fov_extended, 1.e-40);
         // stack up smooth contributions on extended grid:
         for (int i = 0; i < n_pix_fov_extended; ++i) {
            j_tot_beam[i] = jgal_sm_beam[i] ;
            if (is_subs) {
               j_tot_beam[i] += jgal_sub_continuum_beam[i];
               if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                  j_tot_beam[i] += jgal_crossprod_beam[i];
            }
         }
         // fill drawn clumps and/or from list onto smooth contributions on extended grid:
         for (int i = 0; i < n_pix_fov; ++i) {
            int hp_index = v_pix_fov[i] ;
            int i_extended = binary_search(v_pix_fov_extended, hp_index);
            if (is_subs && is_subs_drawn) j_tot_beam[i_extended] += jgal_sub_drawn[i] ;
            if (is_list_added) j_tot_beam[i_extended] += j_list_tot[i] ;
         }
         // write everything into a full-sky map:
         for (int i = 0; i < n_pix_fov_extended; ++i) {
            if (gSIM_HEALPIX_SCHEME == NEST) arrhp_fullsky[hp_gridprop_fullsky.nest2ring(v_pix_fov_extended[i])] = j_tot_beam[i];
            else arrhp_fullsky[v_pix_fov_extended[i]] = j_tot_beam[i];
         }
      } else {
         // for the fullsky, in the RING scheme, j_tot is already identical to arrhp_fullsky.
         // so switch only from NESTED to RING scheme and from double to single precision.
         for (int i = 0; i < n_pix_fov; ++i) {
            if (gSIM_HEALPIX_SCHEME == NEST) arrhp_fullsky[hp_gridprop_fullsky.nest2ring(v_pix_fov[i])] = j_tot[i];
            else arrhp_fullsky[i] = j_tot[i];
         }
      }

      if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3) {

         Healpix_Map<double> hp_map_gamma(arrhp_fullsky, RING);
         j_tot_mean = hp_map_gamma.average();
         cout << "\n>>>>> now smooth total " << j_or_d << "-factor skymap with instrumental beam of Gamma-ray telescope ..." << endl;

         j_tot_gammasmoothed.assign(n_pix_fov, 1.e-40);

         if (is_fullsky) hp_smooth_map_and_get_powspec(hp_map_gamma, gSIM_GAUSSBEAM_GAMMA_FWHM, weights_dir, powerspec_j_tot) ;
         else hp_smooth_map(hp_map_gamma, gSIM_GAUSSBEAM_GAMMA_FWHM, weights_dir) ;

         // Now, cut off the edges of the extended grid and write original FOV to file:
         for (int i = 0; i < n_pix_fov; ++i) {
            if (gSIM_HEALPIX_SCHEME == NEST) j_tot_gammasmoothed[i] = hp_map_gamma[hp_gridprop_fullsky.nest2ring(v_pix_fov[i])];
            else j_tot_gammasmoothed[i] = hp_map_gamma[v_pix_fov[i]];
         }
      }

      if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3) {

         Healpix_Map<double> hp_map_neutrino(arrhp_fullsky, RING);
         j_tot_mean = hp_map_neutrino.average();

         cout << "\n>>>>> now smooth total " << j_or_d << "-factor skymap with instrumental beam of neutrino telescope ..." << endl;

         j_tot_neutrinosmoothed.assign(n_pix_fov, 1.e-40);

         if (smooth_gamma_or_nu_or_both == 2 && is_fullsky) hp_smooth_map_and_get_powspec(hp_map_neutrino, gSIM_GAUSSBEAM_NEUTRINO_FWHM, weights_dir, powerspec_j_tot) ;
         else hp_smooth_map(hp_map_neutrino, gSIM_GAUSSBEAM_NEUTRINO_FWHM, weights_dir) ;

         // Now, cut off the edges of the extended grid and write original FOV to file:
         for (int i = 0; i < n_pix_fov; ++i) {
            if (gSIM_HEALPIX_SCHEME == NEST) j_tot_neutrinosmoothed[i] = hp_map_neutrino[hp_gridprop_fullsky.nest2ring(v_pix_fov[i])];
            else j_tot_neutrinosmoothed[i] = hp_map_neutrino[v_pix_fov[i]];
         }
      }

      // deallocate memory:
      arr<double>().swap(arrhp_fullsky); // deallocate memory
      // hp_map object?

      if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3)
         j_tot_gammasmoothed_sr.assign(n_pix_fov, 1.e-40);
      if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3)
         j_tot_neutrinosmoothed_sr.assign(n_pix_fov, 1.e-40);

      for (int i = 0; i < n_pix_fov; ++i) {
         if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3)
            j_tot_gammasmoothed_sr[i] = j_tot_gammasmoothed[i] / gSIM_HEALPIX_DELTAOMEGA ;
         if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3)
            j_tot_neutrinosmoothed_sr[i] = j_tot_neutrinosmoothed[i] / gSIM_HEALPIX_DELTAOMEGA ;
      }

      // write to fits file:
      cout << "" << endl;
      if (gSIM_IS_PRINT) {
         if (smooth_gamma_or_nu_or_both == 1) {
            fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_gammasmoothed, j_tot_gammasmoothed_sr) ;
            if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
               fh_file_out.set_key("TTYPE2", string("Jtot_gamma"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jtot_gamma_per_sr"), "label for field 3");
            } else {
               fh_file_out.set_key("TTYPE2", string("Dtot_gamma"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dtot_gamma_per_sr"), "label for field 3");

            }
         } else if (smooth_gamma_or_nu_or_both == 2) {
            fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_neutrinosmoothed, j_tot_neutrinosmoothed_sr) ;
            if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
               fh_file_out.set_key("TTYPE2", string("Jtot_neutrino"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jtot_neutrino_per_sr"), "label for field 3");
            } else {
               fh_file_out.set_key("TTYPE2", string("Dtot_neutrino"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dtot_neutrino_per_sr"), "label for field 3");
            }
            // set correct units for J per sr extension:
            fh_file_out.set_key("TUNIT2", j_units, "physical unit of field");
            fh_file_out.set_key("TUNIT3", j_units_per_sr, "physical unit of field");
         } else if (smooth_gamma_or_nu_or_both == 3) {
            fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_gammasmoothed, j_tot_gammasmoothed_sr, j_tot_neutrinosmoothed, j_tot_neutrinosmoothed_sr) ;
            if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
               fh_file_out.set_key("TTYPE2", string("Jtot_gamma"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jtot_gamma_per_sr"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Jtot_neutrino"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Jtotneutrino_per_sr"), "label for field 5");
            } else {
               fh_file_out.set_key("TTYPE2", string("Dtot_gamma"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dtot_gamma_per_sr"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Dtot_neutrino"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Dtot_neutrino_per_sr"), "label for field 5");
            }
            // set correct units for J per sr extension:
            fh_file_out.set_key("TUNIT2", j_units, "physical unit of field");
            fh_file_out.set_key("TUNIT3", j_units_per_sr, "physical unit of field");
            fh_file_out.set_key("TUNIT4", j_units, "physical unit of field");
            fh_file_out.set_key("TUNIT5", j_units_per_sr, "physical unit of field");
         }

         // set correct extension name:
         if (gPP_DM_IS_ANNIHIL_OR_DECAY) fh_file_out.set_key("EXTNAME", string("JTOT_SMOOTHED"), "name of this binary table extension");
         else fh_file_out.set_key("EXTNAME", string("DTOT_SMOOTHED"), "name of this binary table extension");
      }
   }


   /****** GET POWER SPECTRA OF ADDITIONAL COMPONENTS AND WRITE TO FILE *******/

   if (gSIM_IS_WRITE_GALPOWERSPECTRUM && gSIM_IS_PRINT) {

      Healpix_Map<double> hp_map;

      PowSpec powerspec_jgal_sm  ;
      PowSpec powerspec_jgal_sub_continuum ;
      PowSpec powerspec_jgal_crossprod ;
      PowSpec powerspec_jgal_sub_drawn ;
      PowSpec powerspec_j_list_tot ;

      arr<double> cl_j_tot;

      double jgal_sm_mean = 1.e-40;
      double jgal_sub_continuum_mean = 1.e-40;
      double jgal_crossprod_mean = 1.e-40;
      double jgal_sub_drawn_mean = 1.e-40;
      double j_list_tot_mean = 1.e-40;

      // calculate power spectrum of total component, if not done already:
      if (!is_fullsky || smooth_gamma_or_nu_or_both == 0) {
         arr<double> arrhp_fullsky(n_pix, 0.);  // this takes a lot of memory...
         for (int i = 0; i < n_pix_fov; ++i) {
            if (gSIM_HEALPIX_SCHEME == NEST) arrhp_fullsky[hp_gridprop_fullsky.nest2ring(v_pix_fov[i])] = j_tot[i];
            else arrhp_fullsky[i] = j_tot[i];
         }
         hp_map.Set(arrhp_fullsky, RING);
         j_tot_mean = hp_map.average() / gSIM_HEALPIX_DELTAOMEGA;
         cout << "\n>>>>> now calculate power spectrum of total " << j_or_d << "-factor skymap..." << endl;
         hp_get_powspec(hp_map, weights_dir, powerspec_j_tot) ;
         cl_j_tot = powerspec_j_tot.tt();
         arr<double>().swap(arrhp_fullsky); // deallocate memory
      } else {
         cl_j_tot =  powerspec_j_tot.tt(); // has been already calculated when smoothing
         cout << "\n>>>>> power spectrum of total " << j_or_d << "-factor skymap of full sky has been extracted with map smoothing..." << endl;
      }

      size_t lmax = cl_j_tot.size();
      arr<double> cl_jgal_sm(lmax, 1.e-40);
      arr<double> cl_jgal_sub_continuum(lmax, 1.e-40);
      arr<double> cl_jgal_crossprod(lmax, 1.e-40);
      arr<double> cl_jgal_sub_drawn(lmax, 1.e-40);
      arr<double> cl_j_list_tot(lmax, 1.e-40);

      // calculate power spectrum of smooth component:
      if (!is_subs && !is_list_added) {
         cl_jgal_sm = cl_j_tot;
      } else {
         arr<double> arrhp_fullsky(n_pix, 0.);  // this takes a lot of memory...
         for (int i = 0; i < n_pix_fov; ++i) {
            if (gSIM_HEALPIX_SCHEME == NEST) arrhp_fullsky[hp_gridprop_fullsky.nest2ring(v_pix_fov[i])] = jgal_sm[i];
            else arrhp_fullsky[i] = jgal_sm[i];
         }
         hp_map.Set(arrhp_fullsky, RING);
         jgal_sm_mean = hp_map.average() / gSIM_HEALPIX_DELTAOMEGA;
         cout << ">>>>> now calculate power spectrum of " << j_or_d << "-factor skymap from smooth halo only..." << endl;
         hp_get_powspec(hp_map, weights_dir, powerspec_jgal_sm) ;
         cl_jgal_sm = powerspec_jgal_sm.tt();
         arr<double>().swap(arrhp_fullsky); // deallocate memory
      }

      // calculate power spectrum of substructure continuum, if existing:
      if (is_subs) {
         arr<double> arrhp_fullsky(n_pix, 0.);  // this takes a lot of memory...
         for (int i = 0; i < n_pix_fov; ++i) {
            if (gSIM_HEALPIX_SCHEME == NEST) {
               arrhp_fullsky[hp_gridprop_fullsky.nest2ring(v_pix_fov[i])] = jgal_sub_continuum[i];
            } else {
               arrhp_fullsky[i] = jgal_sub_continuum[i];
            }
         }
         hp_map.Set(arrhp_fullsky, RING);
         jgal_sub_continuum_mean = hp_map.average()  / gSIM_HEALPIX_DELTAOMEGA;
         cout << ">>>>> now calculate power spectrum of " << j_or_d << "-factor from substructure continuum only..." << endl;
         hp_get_powspec(hp_map, weights_dir, powerspec_jgal_sub_continuum) ;
         cl_jgal_sub_continuum = powerspec_jgal_sub_continuum.tt();
         arr<double>().swap(arrhp_fullsky); // deallocate memory
      }

      // calculate power spectrum of crossproduct contribution (annihilation only):
      if (is_subs && gPP_DM_IS_ANNIHIL_OR_DECAY) {
         arr<double> arrhp_fullsky(n_pix, 0.);
         for (int i = 0; i < n_pix_fov; ++i) {
            if (gSIM_HEALPIX_SCHEME == NEST) {
               arrhp_fullsky[hp_gridprop_fullsky.nest2ring(v_pix_fov[i])] = jgal_crossprod[i];
            } else {
               arrhp_fullsky[i] += jgal_crossprod[i];
            }
         }
         hp_map.Set(arrhp_fullsky, RING);
         jgal_crossprod_mean = hp_map.average()  / gSIM_HEALPIX_DELTAOMEGA;
         cout << ">>>>> now calculate power spectrum of " << j_or_d << "-factor from cross-product component only..." << endl;
         hp_get_powspec(hp_map, weights_dir, powerspec_jgal_crossprod) ;
         cl_jgal_crossprod = powerspec_jgal_crossprod.tt();
         arr<double>().swap(arrhp_fullsky); // deallocate memory
      }

      // calculate power spectrum of drawn substructures, if existing:
      if (is_subs && is_subs_drawn) {
         arr<double> arrhp_fullsky(n_pix, 0.);  // this takes a lot of memory...
         for (int i = 0; i < n_pix_fov; ++i) {
            if (gSIM_HEALPIX_SCHEME == NEST) {
               arrhp_fullsky[hp_gridprop_fullsky.nest2ring(v_pix_fov[i])] = jgal_sub_drawn[i];
            } else {
               arrhp_fullsky[i] = jgal_sub_drawn[i];
            }
         }
         hp_map.Set(arrhp_fullsky, RING);
         jgal_sub_drawn_mean = hp_map.average() / gSIM_HEALPIX_DELTAOMEGA;
         cout << ">>>>> now calculate power spectrum of " << j_or_d << "-factor from drawn Galactic substructures only..." << endl;
         hp_get_powspec(hp_map, weights_dir, powerspec_jgal_sub_drawn) ;
         cl_jgal_sub_drawn = powerspec_jgal_sub_drawn.tt();
         arr<double>().swap(arrhp_fullsky); // deallocate memory
      }

      // calculate power spectrum of objects from list, if existing:
      if (is_list_added) {
         arr<double> arrhp_fullsky(n_pix, 0.);  // this takes a lot of memory...
         for (int i = 0; i < n_pix_fov; ++i) {
            if (gSIM_HEALPIX_SCHEME == NEST) {
               arrhp_fullsky[hp_gridprop_fullsky.nest2ring(v_pix_fov[i])] = j_list_tot[i];
            } else {
               arrhp_fullsky[i] = j_list_tot[i];
            }
         }
         hp_map.Set(arrhp_fullsky, RING);
         j_list_tot_mean = hp_map.average() / gSIM_HEALPIX_DELTAOMEGA;
         cout << ">>>>> now calculate power spectrum of " << j_or_d << "-factor from objects from list only..." << endl;
         hp_get_powspec(hp_map, weights_dir, powerspec_j_list_tot) ;
         cl_j_list_tot = powerspec_j_list_tot.tt();
         arr<double>().swap(arrhp_fullsky); // deallocate memory
      }

      // convert to intensity APS by dividing by gSIM_HEALPIX_DELTAOMEGA**2:
      for (int i = 0; i < lmax; ++i) {
         if (cl_j_tot[i] != 1.e-40)          cl_j_tot[i] /= pow(gSIM_HEALPIX_DELTAOMEGA, 2.);
         if (cl_jgal_sm[i] != 1.e-40)        cl_jgal_sm[i] /= pow(gSIM_HEALPIX_DELTAOMEGA, 2.);
         if (cl_jgal_sub_continuum[i] != 1.e-40) cl_jgal_sub_continuum[i] /= pow(gSIM_HEALPIX_DELTAOMEGA, 2.);
         if (cl_jgal_crossprod[i] != 1.e-40) cl_jgal_crossprod[i] /= pow(gSIM_HEALPIX_DELTAOMEGA, 2.);
         if (cl_jgal_sub_drawn[i] != 1.e-40) cl_jgal_sub_drawn[i] /= pow(gSIM_HEALPIX_DELTAOMEGA, 2.);
         if (cl_j_list_tot[i] != 1.e-40)     cl_j_list_tot[i] /= pow(gSIM_HEALPIX_DELTAOMEGA, 2.);
      }

      PowSpec powerspec_all ;
      powerspec_all.Set(cl_j_tot, cl_jgal_sm, cl_jgal_sub_continuum, cl_jgal_crossprod, cl_jgal_sub_drawn, cl_j_list_tot) ;

      ifstream ispowspecfile(powspecfilename_str.c_str());
      if (ispowspecfile) {
         fitshandle fh_file ;
         fh_file.delete_file(powspecfilename_str);
      }

      write_powspec_to_fits(powspecfilename_str, powerspec_all, 6);

      // correct header:

      string units_aps_str;
      if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
         if (gSIM_IS_ASTRO_OR_PP_UNITS) units_aps_str = "Msun^4/kpc^10/sr" ;
         else units_aps_str = "GeV^4/cm^10/sr" ;
      } else {
         if (gSIM_IS_ASTRO_OR_PP_UNITS) units_aps_str = "Msun^2/kpc^4/sr" ;
         else units_aps_str = "GeV^2/cm^4/sr" ;
      }

      string units_j_mean_str;
      if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
         if (gSIM_IS_ASTRO_OR_PP_UNITS) units_j_mean_str = "[Msun^2/kpc^5/sr] mean dJ/dOmega on full sky" ;
         else units_j_mean_str = "[GeV^2/cm^5/sr] mean dJ/dOmega on full sky" ;
      } else {
         if (gSIM_IS_ASTRO_OR_PP_UNITS) units_j_mean_str = "[Msun/kpc^2/sr] mean dD/dOmega on full sky" ;
         else units_j_mean_str = "[GeV/cm^2/sr] mean dD/dOmega on full sky" ;
      }

      fitshandle fh_powspecfile;
      fh_powspecfile.open(powspecfilename_str);
      fh_powspecfile.goto_hdu(2);

      fh_powspecfile.set_key("EXTNAME", string("Powerspectra"), "name of this binary table extension");
      fh_powspecfile.set_key("TTYPE1", string("Cl_Jtot"),       "APS of total dJ/dOmega map with all components");
      fh_powspecfile.set_key("TTYPE2", string("Cl_Jsmooth"),    "APS of dJ/dOmega map from smooth galactic halo");
      fh_powspecfile.set_key("TTYPE3", string("Cl_Jsubs"),      "APS of dJ/dOmega map from averaged subhalo cont.");
      fh_powspecfile.set_key("TTYPE4", string("Cl_Jcrossprod"), "APS of dJ/dOmega map from crossproduct contrib. ");
      fh_powspecfile.set_key("TTYPE5", string("Cl_Jdrawn"),     "APS of dJ/dOmega map from drawn gal. subhalos");
      fh_powspecfile.set_key("TTYPE6", string("Cl_Jlist"),      "APS of dJ/dOmega map from objects from list");

      fh_powspecfile.set_key("TUNIT1", units_aps_str, "physical unit of field");
      fh_powspecfile.set_key("TUNIT2", units_aps_str, "physical unit of field");
      fh_powspecfile.set_key("TUNIT3", units_aps_str, "physical unit of field");
      fh_powspecfile.set_key("TUNIT4", units_aps_str, "physical unit of field");
      fh_powspecfile.set_key("TUNIT5", units_aps_str, "physical unit of field");
      fh_powspecfile.set_key("TUNIT6", units_aps_str, "physical unit of field");

      fh_powspecfile.set_key("JTOT_M", j_tot_mean, units_j_mean_str);
      fh_powspecfile.set_key("JSM_M", jgal_sm_mean, units_j_mean_str);
      fh_powspecfile.set_key("JSUBCN_M", jgal_sub_continuum_mean, units_j_mean_str);
      fh_powspecfile.set_key("JCROSS_M", jgal_crossprod_mean, units_j_mean_str);
      fh_powspecfile.set_key("JDRAWN_M", jgal_sub_drawn_mean, units_j_mean_str);
      fh_powspecfile.set_key("JLIST_M", j_list_tot_mean, units_j_mean_str);



      // write input header keywords:
      fh_powspecfile.add_comment("------------------------------------------------------------------------");
      fh_powspecfile.add_comment("------------------------------------------------------------------------");
      fh_powspecfile.add_comment("Input parameters for this output file:");
      fh_powspecfile.add_comment("------------------------------------------------------------------------");
      fh_powspecfile.set_key(gSIM_INPUTPARAMS[kSIM_FLAG_MODE][kSIM_INPUTPARAM_FITSNAME],
                             string(gNAMES_SIMUMODES[gSIM_FLAG_MODE]), gSIM_INPUTPARAMS[kSIM_FLAG_MODE][kSIM_INPUTPARAM_DESCRIPTION]);

      for (int i = 0; i < gN_INPUTPARAMS; ++i) {
         if (gSIM_INPUTPARAM_VALUESTRING[i] != "-999") {
            fits_write_key_from_inputparams(fh_powspecfile, gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_FITSNAME], gSIM_INPUTPARAM_VALUESTRING[i],
                                            gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_DATATYPE], gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_UNIT],
                                            gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_DESCRIPTION]);
         }
      }
      fh_powspecfile.add_comment("------------------------------------------------------------------------");
      fh_powspecfile.add_comment("------------------------------------------------------------------------");
      fh_powspecfile.add_comment("Selected output quantities:");
      fh_powspecfile.add_comment("------------------------------------------------------------------------");

      fh_powspecfile.set_key("F_SKY", fsky, "fraction of sky covered by FOV");
      fh_powspecfile.set_key("N_DRAWN", int(counter), "number of drawn clumps");

      fh_powspecfile.close();

      cout << "\n  ... Full-sky angular power spectrum written to file: " << powspecfilename_str << endl;
   } else if (gSIM_IS_WRITE_GALPOWERSPECTRUM && !gSIM_IS_PRINT) {
      print_warning("janalysis.cc", "gal_j2D()", "Power spectra are only written if printing file is not suppressed.");

   }

   /******************* CALCULATE FLUX- AND INTENSITY MAPS ********************/

   // Is flux?
   bool is_flux = true;
   if (gPP_DM_MASS_GEV < gSIM_FLUX_AT_E_GEV) {
      is_flux = false;
      print_warning("janalysis.cc", "gal_j2D()", "No fluxes are calculated because pivot energy is bigger than DM particle mass.");
   }


   if (gSIM_IS_WRITE_FLUXMAPS && is_flux && gSIM_IS_PRINT) {
      cout << "\n>>>>> Calculate and write flux and intensity map(s) for given particle physics model..." << endl;

      if (smooth_gamma_or_nu_or_both == 0) {
         fits_write_fluxmaps(v_pix_fov, j_tot, fh_file_out);
      } else if (smooth_gamma_or_nu_or_both == 1) {
         fits_write_fluxmaps(v_pix_fov, j_tot, j_tot_gammasmoothed, smooth_gamma_or_nu_or_both, fh_file_out);
      } else if (smooth_gamma_or_nu_or_both == 2) {
         fits_write_fluxmaps(v_pix_fov, j_tot, j_tot_neutrinosmoothed, smooth_gamma_or_nu_or_both, fh_file_out);
      } else if (smooth_gamma_or_nu_or_both == 3) {
         fits_write_fluxmaps(v_pix_fov, j_tot, j_tot_gammasmoothed, j_tot_neutrinosmoothed, smooth_gamma_or_nu_or_both, fh_file_out);
      }
   } else if (gSIM_IS_WRITE_FLUXMAPS && is_flux && !gSIM_IS_PRINT) {
      print_warning("janalysis.cc", "gal_j2D()", "Flux maps are only written if printing file is not suppressed.");
   }


   /************************ CLOSING OUTPUT FILE WRITING **********************/

   if (gSIM_IS_PRINT) {

      // delete flux related keywords from first two extensions:
      if (gSIM_IS_WRITE_FLUXMAPS) {
         vector<string> keylist;
         for (int k = 2; k < 4; ++k) { // loop through first two data extensions
            fh_file_out.goto_hdu(k);
            fh_file_out.get_all_keys(keylist);
            int delete_flux_keys[] = {kPP_DM_ANNIHIL_DELTA, kPP_DM_ANNIHIL_SIGMAV_CM3PERS, kPP_DM_DECAY_LIFETIME_S,
                                      kSIM_FLUX_IS_INTEG_OR_DIFF, kSIM_FLUX_FLAG_NUFLAVOUR, kSIM_FLUX_FLAG_FINALSTATE,
                                      kPP_DM_MASS_GEV, kPP_FLAG_SPECTRUMMODEL,
                                      kSIM_FLUX_EMIN_GEV, kSIM_FLUX_EMAX_GEV, kSIM_FLUX_AT_E_GEV,
                                      kPP_NUMIXING_THETA12_DEG, kPP_NUMIXING_THETA13_DEG, kPP_NUMIXING_THETA23_DEG
                                     };
            for (int i = 0; i < int(sizeof(delete_flux_keys) / sizeof(delete_flux_keys[0])); ++i) {
               if (find(keylist.begin(), keylist.end(), gSIM_INPUTPARAMS[delete_flux_keys[i]][kSIM_INPUTPARAM_FITSNAME]) != keylist.end()) {
                  fh_file_out.delete_key(gSIM_INPUTPARAMS[delete_flux_keys[i]][kSIM_INPUTPARAM_FITSNAME]);
               }
            }
            for (int i = 0; i < (int)keylist.size(); ++i) { // delete branching ratios
               if (keylist[i].substr(0, 2) == "BR") {
                  fh_file_out.delete_key(keylist[i]);
               }
            }
            fh_file_out.set_key(gSIM_INPUTPARAMS[kSIM_IS_WRITE_FLUXMAPS][kSIM_INPUTPARAM_FITSNAME], false, gSIM_INPUTPARAMS[kSIM_IS_WRITE_FLUXMAPS][kSIM_INPUTPARAM_DESCRIPTION]);
         }
      }

      fh_file_out.close();
      cout << "_______________________" << endl << endl;
      cout << "      All output files successfully written in folder " << gSIM_OUTPUT_DIR << "\n" << endl;
      printf("      We recommend Aladin (http://aladin.u-strasbg.fr/) for visualizing and\n") ;
      printf("      browsing the FITS file. Alternative, less optimised but more widely \n") ;
      printf("      readable output formats can be generated via the ./bin/clumpy -o options.\n") ;
      if (gSIM_HEALPIX_NSIDE % 2 != 0) {
         print_warning("janalysis.cc", "gal_j2D()", "Some FITS viewers won't work for odd NSIDE, as chosen in this simulation.");
      }
      printf("\n") ;
      cout << "_______________________" << endl << endl;
   }

   /***************************  ROOT QUICK LOOK  *****************************/
#if IS_ROOT
   // #graph = smooth, <sub>, crossprod (in annihil.) drawn (optional), tot
   TCanvas *c_galtot = NULL;
   TCanvas *c_galtot_smoothed_gamma = NULL;
   TCanvas *c_galtot_smoothed_neutrino = NULL;
   TCanvas *c_galcomp = NULL;
   TCanvas *c_gal_aitoff = NULL;
   TH2D **h2d = NULL;
   THStack *h_stack = NULL;

   // Save .root format?
   TFile *root_file = NULL;
   if (gSIM_IS_WRITE_ROOTFILES) {
      root_file = new TFile(filename_root_str.c_str(), "recreate");
      root_file->cd();
   }

   // the plotting of the data on a rectangular grid  works only
   // if the FOV does not exceed |theta| > 90 degs.

   int disp_condition = true ;
   if (dtheta < 0. || dtheta_orth < 0. || theta_max < - PI / 2 || theta_min > PI / 2) disp_condition = false ;

   //--- Set binning:
   double delta_psi = sqrt(delta_omega) ;
   double delta_theta = sqrt(delta_omega) ;

   int n_psi ;
   int n_theta ;
   if (gSIM_HEALPIX_NSIDE % 2 == 0) {
      // force n_psi, n_theta to be even, as it fits better to the actual Healpix grid data
      // (for even NSIDE, no pixel with center at (0,0))
      n_psi = 2 * int(ceil(dtheta_orth / delta_psi) / 2.) + 2; // # of bins in psi, force to be even
      n_theta = 2 * int(ceil(dtheta_coord / delta_theta) / 2.) + 2 ; // # of bins in theta, force to be even
   } else {
      n_psi = 2 * int(ceil(dtheta_orth / delta_psi) / 2.) + 1; // # of bins in psi, force to be odd
      n_theta = 2 * int(ceil(dtheta_coord / delta_theta) / 2.) + 1 ;
   }
   delta_theta = dtheta_coord / (n_theta - 1) ; // correct delta_theta, accounted for the ceil()
   delta_psi = dpsi / (n_psi - 1) ;

   if (gSIM_IS_DISPLAY || gSIM_IS_WRITE_ROOTFILES) {
      printf("    * ROOT displays and/or output\n");

      if (!disp_condition) {
         printf("      (no skymap plots because FOV exceeds |theta| > 90 degs.) \n");
      } else {
         //--- Array declarations and initialisation for rectangular ROOT plots
         vector<double> j_tot_plot(n_theta * n_psi, 1.e-40);
         vector<double> jgal_sm_plot(n_theta * n_psi, 1.e-40);
         vector<double> jgal_sub_continuum_plot;
         vector<double> jgal_crossprod_plot;
         vector<double> jgal_sub_drawn_plot;
         vector<double> j_list_tot_plot;
         vector<double> j_tot_gammasmoothed_plot ;
         vector<double> j_tot_neutrinosmoothed_plot ;

         vector<double> psi_tab;
         vector<double> theta_tab;

         // Create Jdrawn and/or Jlist for rectangular ROOT plot if required
         if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3)
            j_tot_gammasmoothed_plot.assign(n_psi * n_theta, 1.e-40);
         if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3)
            j_tot_neutrinosmoothed_plot.assign(n_psi * n_theta, 1.e-40);
         if (is_list_added)
            j_list_tot_plot.assign(n_psi * n_theta, 1.e-40);
         if (is_subs) {
            jgal_sub_continuum_plot.assign(n_psi * n_theta, 1.e-40);
            if (gPP_DM_IS_ANNIHIL_OR_DECAY)
               jgal_crossprod_plot.assign(n_psi * n_theta, 1.e-40);
            if (is_subs_drawn)
               jgal_sub_drawn_plot.assign(n_psi * n_theta, 1.e-40);
         }

         // find plot ranges:
         double j_tot_min = 1.e40;
         for (int i = 0; i < n_pix_fov; ++i) {
            if (j_tot[i]  < j_tot_min) {
               j_tot_min = j_tot[i] ;
            }
         }
         double jgal_sm_min = 1.e40;
         for (int i = 0; i < n_pix_fov; ++i) {
            if (jgal_sm[i]  < jgal_sm_min) {
               jgal_sm_min = jgal_sm[i] ;
            }
         }

         // fill arrays for plotting:
         for (int j = 0; j < n_theta; ++j)
            theta_tab.push_back(theta_min + j * delta_theta);

         for (int i = 0; i < n_psi; ++i) {
            psi_tab.push_back(psi_min + i * delta_psi);
            for (int j = 0; j < n_theta; ++j) {
               if (fabs(theta_min + j * delta_theta) > PI / 2) continue;
               pointing ptg_angles_plot(PI / 2 - theta_min - j * delta_theta, psi_min + i * delta_psi) ;
               // find the pixel to this angle:
               int i_pix = hp_gridprop.ang2pix(ptg_angles_plot) ;
               // if Healpix map contains a value at this point, fill it in the plotting map:
               if (rs_pix_fov.contains(i_pix)) {
                  int index_i_pix = binary_search(v_pix_fov, i_pix);
                  jgal_sm_plot[i * n_theta + j] = jgal_sm[index_i_pix] ;
                  j_tot_plot[i * n_theta + j] = j_tot[index_i_pix]  ;
                  if (is_list_added)
                     j_list_tot_plot[i * n_theta + j] = j_list_tot[index_i_pix] ;
                  if (is_subs) {
                     jgal_sub_continuum_plot[i * n_theta + j] = jgal_sub_continuum[index_i_pix] ;
                     if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                        jgal_crossprod_plot[i * n_theta + j] = jgal_crossprod[index_i_pix] ;
                     if (is_subs_drawn)
                        jgal_sub_drawn_plot[i * n_theta + j] = jgal_sub_drawn[index_i_pix] ;
                  }
                  if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3)
                     j_tot_gammasmoothed_plot[i * n_theta + j] = j_tot_gammasmoothed[index_i_pix] ;
                  if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3)
                     j_tot_neutrinosmoothed_plot[i * n_theta + j] = j_tot_neutrinosmoothed[index_i_pix] ;
               }
               // else, fill with 1.e-40 to avoid crash of display in log
               else {
                  jgal_sm_plot[i * n_theta + j] = 1.e-40;
                  j_tot_plot[i * n_theta + j] = 1.e-40 ;
                  if (is_list_added)
                     j_list_tot_plot[i * n_theta + j] = 1.e-40;
                  if (is_subs) {
                     jgal_sub_continuum_plot[i * n_theta + j] = 1.e-40;
                     if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                        jgal_crossprod_plot[i * n_theta + j] = 1.e-40;
                     if (is_subs_drawn)
                        jgal_sub_drawn_plot[i * n_theta + j] = 1.e-40;
                  }
                  if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3)
                     j_tot_gammasmoothed_plot[i * n_theta + j] = 1.e-40;
                  if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3)
                     j_tot_neutrinosmoothed_plot[i * n_theta + j] = 1.e-40;
               }
            }
         }

         vector<string> gr_name;
         vector<int> colors;
         int n_g = 2;
         gr_name.push_back("h2d_galtot");
         colors.push_back(kBlue);
         gr_name.push_back("h2d_galsm");
         colors.push_back(kBlue);

         if (is_subs) {
            if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
               gr_name.push_back("h2d_galcrossprod");
               colors.push_back(kGreen + 1);
               ++ n_g;
            }
            gr_name.push_back("h2d_galmean");
            colors.push_back(kRed);
            ++n_g;

            if (is_subs_drawn) {
               gr_name.push_back("h2d_galdrawn");
               colors.push_back(kYellow);
               ++n_g;
            }
         }

         if (is_list_added) {
            gr_name.push_back("h2d_list");
            colors.push_back(kOrange);
            ++n_g;
         }

         if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3) {
            gr_name.push_back("h2d_gaussbeam_gamma");
            colors.push_back(kBlue);
            ++n_g;
         }

         if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3) {
            gr_name.push_back("h2d_gaussbeam_neutrino");
            colors.push_back(kBlue);
            ++n_g;
         }

         h2d = new TH2D*[n_g];

         // Create and fill 2D histos
         for (int g = 0; g < n_g; ++g) {
            h2d[g] = new TH2D(gr_name[g].c_str(), "",
                              n_psi, psi * RAD_to_DEG - dpsi * RAD_to_DEG / 2., psi * RAD_to_DEG + dpsi * RAD_to_DEG / 2.,
                              n_theta, theta * RAD_to_DEG - dtheta_coord * RAD_to_DEG / 2., theta * RAD_to_DEG + dtheta_coord * RAD_to_DEG / 2.);
         }

         for (int j = 0; j < n_psi ; ++j) {
            for (int k = 0; k < n_theta ; ++k) {
               h2d[0]->SetBinContent(j + 1, k + 1, j_tot_plot[j * n_theta + k]);
               h2d[1]->SetBinContent(j + 1, k + 1, jgal_sm_plot[j * n_theta + k]);

               if (smooth_gamma_or_nu_or_both == 1) {
                  h2d[n_g - 1]->SetBinContent(j + 1, k + 1, j_tot_gammasmoothed_plot[j * n_theta + k]);
               } else if (smooth_gamma_or_nu_or_both == 2) {
                  h2d[n_g - 1]->SetBinContent(j + 1, k + 1, j_tot_neutrinosmoothed_plot[j * n_theta + k]);
               } else if (smooth_gamma_or_nu_or_both == 3) {
                  h2d[n_g - 2]->SetBinContent(j + 1, k + 1, j_tot_gammasmoothed_plot[j * n_theta + k]);
                  h2d[n_g - 1]->SetBinContent(j + 1, k + 1, j_tot_neutrinosmoothed_plot[j * n_theta + k]);
               }

               // Fill histo for all other components
               if (is_subs) {
                  if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
                     h2d[2]->SetBinContent(j + 1, k + 1, jgal_crossprod_plot[j * n_theta + k]);
                     h2d[3]->SetBinContent(j + 1, k + 1, jgal_sub_continuum_plot[j * n_theta + k]);
                     if (is_subs_drawn && is_list_added) {
                        h2d[4]->SetBinContent(j + 1, k + 1, jgal_sub_drawn_plot[j * n_theta + k]);
                        h2d[5]->SetBinContent(j + 1, k + 1, j_list_tot_plot[j * n_theta + k]);
                     } else if (is_subs_drawn) {
                        h2d[4]->SetBinContent(j + 1, k + 1, jgal_sub_drawn_plot[j * n_theta + k]);
                     } else if (is_list_added) {
                        h2d[4]->SetBinContent(j + 1, k + 1, j_list_tot_plot[j * n_theta + k]);
                     }
                  } else {
                     h2d[2]->SetBinContent(j + 1, k + 1, jgal_sub_continuum_plot[j * n_theta + k]);
                     if (is_subs_drawn && is_list_added) {
                        h2d[3]->SetBinContent(j + 1, k + 1, jgal_sub_drawn_plot[j * n_theta + k]);
                        h2d[4]->SetBinContent(j + 1, k + 1, j_list_tot_plot[j * n_theta + k]);
                     } else if (is_subs_drawn) {
                        h2d[3]->SetBinContent(j + 1, k + 1, jgal_sub_drawn_plot[j * n_theta + k]);
                     } else if (is_list_added) {
                        h2d[3]->SetBinContent(j + 1, k + 1, j_list_tot_plot[j * n_theta + k]);
                     }
                  }
               }
            }
         }

         // Create and fill 2D stack
         char integr[200];
         sprintf(integr, "#Delta#Omega = %.2le sr", gSIM_HEALPIX_DELTAOMEGA);
         string histo_legend;
         if (!is_subs) {
            if (is_list_added) histo_legend = string(integr) + ": " + j_or_d + "_{sm} (blue) + " + j_or_d + "_{list} (orange)";
            else histo_legend = string(integr) + ": " + j_or_d + "_{sm} (blue)";
         } else {
            if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
               if (is_subs_drawn && is_list_added)
                  histo_legend = string(integr) + ": J_{sm} (blue) + J_{cross-prod} (green) + J_{<sub>} (red) + J_{drawn} (yellow) + J_{list} (orange)";
               else if (is_subs_drawn)
                  histo_legend = string(integr) + ": J_{sm} (blue) + J_{cross-prod} (green) + J_{<sub>} (red) + J_{drawn} (yellow)";
               else if (is_list_added)
                  histo_legend = string(integr) + ": J_{sm} (blue) + J_{cross-prod} (green) + J_{<sub>} (red) + J_{list} (orange)";
               else
                  histo_legend = string(integr) + ": J_{sm} (blue) + J_{cross-prod} (green) + J_{<sub>} (red)";
            } else {
               if (is_subs_drawn && is_list_added)
                  histo_legend = string(integr) + ": D_{sm} (blue) + D_{<sub>} (red) + D_{drawn} (yellow) + D_{list} (orange)";
               else if (is_subs_drawn)
                  histo_legend = string(integr) + ": D_{sm} (blue) + D_{<sub>} (red) + D_{drawn} (yellow)";
               else if (is_list_added)
                  histo_legend = string(integr) + ": D_{sm} (blue) + D_{<sub>} (red) + D_{list} (orange)";
               else
                  histo_legend = string(integr) + ": D_{sm} (blue) + D_{<sub>} (red)";
            }
         }

         // If too many pixels, skip plot of stack to save time + details not visible in that case.
         bool is_stack = true;
         if (n_pix_fov > 50000) {
            is_stack = false;
            cout << "      N.B.: n_pix_fov=" << n_pix_fov << "(>50000) => do not plot stacked contributions (time consuming)" << endl;
         }

         if (is_stack) {
            h_stack = new THStack("h_stack", histo_legend.c_str());
            gStyle->SetTitleY(0.96); // histos are redrawn: necessary to force title position
         }

         string  z_axis_name;
         if (gPP_DM_IS_ANNIHIL_OR_DECAY && gSIM_IS_ASTRO_OR_PP_UNITS)
            z_axis_name = "J_{tot} [M_{#odot}^{2} kpc^{-5}]";
         else if (!gPP_DM_IS_ANNIHIL_OR_DECAY && gSIM_IS_ASTRO_OR_PP_UNITS)
            z_axis_name = "D_{tot} [M_{#odot} kpc^{-2}]";
         else if (gPP_DM_IS_ANNIHIL_OR_DECAY && !gSIM_IS_ASTRO_OR_PP_UNITS)
            z_axis_name = "J_{tot} [GeV^{2} cm^{-5}]";
         else if (!gPP_DM_IS_ANNIHIL_OR_DECAY && !gSIM_IS_ASTRO_OR_PP_UNITS)
            z_axis_name = "D_{tot} [GeV cm^{-2}]";

         for (int g = 0; g < n_g; ++g) {
            if (g == 0 || (smooth_gamma_or_nu_or_both == 3 && g >= n_g - 2)
                  || ((smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 2) && g >= n_g - 1)) {
               h2d[g]->GetXaxis()->CenterTitle(1);
               h2d[g]->GetYaxis()->CenterTitle(1);
               h2d[g]->GetZaxis()->CenterTitle(1);
               h2d[g]->GetXaxis()->SetTitle("l [deg]");
               h2d[g]->GetYaxis()->SetTitle("b [deg]");
               h2d[g]->GetZaxis()->SetTitle(z_axis_name.c_str());
               h2d[g]->GetXaxis()->SetTitleOffset(1.5);
               h2d[g]->GetXaxis()->SetTitleFont(132);
               h2d[g]->GetXaxis()->SetTitleSize(0.05);
               h2d[g]->GetXaxis()->SetLabelFont(132);
               h2d[g]->GetXaxis()->SetLabelSize(0.04);
               h2d[g]->GetYaxis()->SetTitleOffset(1.9);
               h2d[g]->GetYaxis()->SetTitleFont(132);
               h2d[g]->GetYaxis()->SetTitleSize(0.05);
               h2d[g]->GetYaxis()->SetLabelFont(132);
               h2d[g]->GetYaxis()->SetLabelSize(0.04);
               h2d[g]->GetZaxis()->SetTitleOffset(0.8);
               h2d[g]->GetZaxis()->SetTitleFont(132);
               h2d[g]->GetZaxis()->SetTitleSize(0.05);
               h2d[g]->GetZaxis()->SetLabelFont(132);
               h2d[g]->GetZaxis()->SetLabelSize(0.05);
               h2d[g]->SetStats(0);
            } else  {
               h2d[g]->SetFillColor(colors[g]);
               if (is_stack) {
                  if (smooth_gamma_or_nu_or_both == 3 && g < n_g - 2)
                     h_stack->Add(h2d[g]);
                  if ((smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 2) && g < n_g - 1)
                     h_stack->Add(h2d[g]);
                  if (smooth_gamma_or_nu_or_both == 0)
                     h_stack->Add(h2d[g]);
               }
            }
         }
         if (is_stack) {
            if (is_subs) {
               h_stack->SetMinimum(jgal_sm_min / 2.);
            } else {
               h_stack->SetMinimum(jgal_sm_min);
            }
            h_stack->SetMaximum(h2d[0]->GetMaximum());
         }
         h2d[0]->SetMinimum(j_tot_min);
         h2d[0]->SetMaximum(h2d[0]->GetMaximum());
         if (smooth_gamma_or_nu_or_both != 0) {
            h2d[n_g - 1]->SetMinimum(j_tot_min);
            h2d[n_g - 1]->SetMaximum(h2d[0]->GetMaximum()); // same scale as unsmoothed
         }
         if (smooth_gamma_or_nu_or_both == 3) {
            h2d[n_g - 2]->SetMinimum(j_tot_min);
            h2d[n_g - 2]->SetMaximum(h2d[0]->GetMaximum()); // same scale as unsmoothed
         }

         if (gSIM_IS_WRITE_ROOTFILES && gSIM_IS_PRINT) {
            for (int g = 0; g < n_g; ++g) h2d[g]->Write();
         }

         const char *title;
         if (smooth_gamma_or_nu_or_both != 0) title = "c_galcomp (unsmoothed)";
         else title = "c_galcomp";
         if (is_stack) {
            c_galcomp = new TCanvas("c_galcomp", title, 0, 0, 650, 500);
            c_galcomp->SetLogx(0);
            c_galcomp->SetLogy(0);
            c_galcomp->SetLogz(1);
            c_galcomp->SetGridx(0);
            c_galcomp->SetGridy(0);
            gStyle->SetPalette(1);
            h_stack->Draw();
            gSIM_CLUMPYAD->Draw();
            h_stack->GetXaxis()->CenterTitle(1);
            h_stack->GetYaxis()->CenterTitle(1);
            h_stack->GetXaxis()->SetTitle("l [deg]");
            h_stack->GetYaxis()->SetTitle("b [deg]");
            h_stack->GetXaxis()->SetTitleOffset(1.5);
            h_stack->GetXaxis()->SetTitleFont(132);
            h_stack->GetXaxis()->SetTitleSize(0.05);
            h_stack->GetXaxis()->SetLabelFont(132);
            h_stack->GetXaxis()->SetLabelSize(0.04);
            h_stack->GetYaxis()->SetTitleOffset(1.9);
            h_stack->GetYaxis()->SetTitleFont(132);
            h_stack->GetYaxis()->SetTitleSize(0.05);
            h_stack->GetYaxis()->SetLabelFont(132);
            h_stack->GetYaxis()->SetLabelSize(0.04);
            h_stack->GetHistogram()->GetZaxis()->CenterTitle(1);
            h_stack->GetHistogram()->GetZaxis()->SetTitle(z_axis_name.c_str());
            h_stack->GetHistogram()->GetZaxis()->SetTitleOffset(0.95);
            h_stack->GetHistogram()->GetZaxis()->SetTitleFont(132);
            h_stack->GetHistogram()->GetZaxis()->SetTitleSize(0.05);
            h_stack->GetHistogram()->GetZaxis()->SetLabelFont(132);
            h_stack->GetHistogram()->GetZaxis()->SetLabelSize(0.05);
            h_stack->GetHistogram()->SetTitleFont(132);

            gPad->Update(); //to force the generation of the title
            TPaveText *h_title = (TPaveText *)gPad->GetPrimitive("title");
            //h_title->SetBorderSize(0);
            h_title->SetFillColor(kWhite);
            c_galcomp->Modified();
            c_galcomp->Update();

            if (gSIM_IS_WRITE_ROOTFILES && gSIM_IS_PRINT) c_galcomp->Write();
         }

         if (smooth_gamma_or_nu_or_both != 0) title = "c_galtot (unsmoothed)";
         else title = "c_galtot";
         if (is_stack)
            c_galtot = new TCanvas("c_galtot", "c_galtot", 500, 0, 650, 500);
         else
            c_galtot = new TCanvas("c_galtot", "c_galtot", 0, 0, 650, 500);
         c_galtot->SetLogx(0);
         c_galtot->SetLogy(0);
         c_galtot->SetLogz(1);
         c_galtot->SetGridx(0);
         c_galtot->SetGridy(0);
         h2d[0]->SetTitle(integr);
         gStyle->SetTitleY(0.96); // histos are redrawn: necessary to force title position
         h2d[0]->SetTitleFont(132);
         h2d[0]->SetTitleSize(0.02);
         h2d[0]->Draw("surf3");
         gSIM_CLUMPYAD->Draw();
         c_galtot->Modified();
         c_galtot->Update();

         if (gSIM_IS_WRITE_ROOTFILES && gSIM_IS_PRINT) c_galtot->Write();

         if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3) {
            c_galtot_smoothed_gamma = new TCanvas("c_galtot_smoothed_gamma", "c_galtot smoothed by Gamma resolution", 0, 500, 650, 500);
            c_galtot_smoothed_gamma->SetLogx(0);
            c_galtot_smoothed_gamma->SetLogy(0);
            c_galtot_smoothed_gamma->SetLogz(1);
            c_galtot_smoothed_gamma->SetGridx(0);
            c_galtot_smoothed_gamma->SetGridy(0);
            char smoot[200];
            sprintf(smoot, "Smoothing FWHM = %.2le [deg]", gSIM_GAUSSBEAM_GAMMA_FWHM * RAD_to_DEG);
            if (smooth_gamma_or_nu_or_both == 1) {
               h2d[n_g - 1]->SetTitle(smoot);
               h2d[n_g - 1]->SetTitleFont(132);
               h2d[n_g - 1]->SetTitleSize(0.02);
               h2d[n_g - 1]->Draw("surf3");
            } else {
               h2d[n_g - 2]->SetTitle(smoot);
               h2d[n_g - 2]->SetTitleFont(132);
               h2d[n_g - 2]->SetTitleSize(0.02);
               h2d[n_g - 2]->Draw("surf3");
            }
            gSIM_CLUMPYAD->Draw();
            c_galtot_smoothed_gamma->Modified();
            c_galtot_smoothed_gamma->Update();
            if (gSIM_IS_WRITE_ROOTFILES && gSIM_IS_PRINT) c_galtot_smoothed_gamma->Write();
         }

         if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3) {
            c_galtot_smoothed_neutrino = new TCanvas("c_galtot_smoothed_neutrino", "c_galtot smoothed by neutrino resolution", 650, 500, 650, 500);
            c_galtot_smoothed_neutrino->SetLogx(0);
            c_galtot_smoothed_neutrino->SetLogy(0);
            c_galtot_smoothed_neutrino->SetLogz(1);
            c_galtot_smoothed_neutrino->SetGridx(0);
            c_galtot_smoothed_neutrino->SetGridy(0);
            char smoot[200];
            sprintf(smoot, "Smoothing FWHM = %.2le [deg]", gSIM_GAUSSBEAM_NEUTRINO_FWHM * RAD_to_DEG);
            h2d[n_g - 1]->SetTitle(smoot);
            h2d[n_g - 1]->SetTitleFont(132);
            h2d[n_g - 1]->SetTitleSize(0.02);
            h2d[n_g - 1]->Draw("surf3");
            gSIM_CLUMPYAD->Draw();
            c_galtot_smoothed_neutrino->Modified();
            c_galtot_smoothed_neutrino->Update();
            if (gSIM_IS_WRITE_ROOTFILES && gSIM_IS_PRINT) c_galtot_smoothed_neutrino->Write();
         }


         if (smooth_gamma_or_nu_or_both != 0) title = "c_gal_aitoff (unsmoothed)";
         else title = "c_gal_aitoff";
         // Draw AITOFF projection only if right coordinates
         if (psi_tab[0] >= -PI - 1.e-5 && psi_tab[n_psi - 1] <= PI + 1e-5 &&
               theta_tab[0] >= -PI / 2. - 1.e-5 && theta_tab[n_theta - 1] <= PI / 2. + 1.e-5) {
            c_gal_aitoff = new TCanvas("c_gal_aitoff", title, 1300, 0, 650, 500);
            c_gal_aitoff->SetLogx(0);
            c_gal_aitoff->SetLogy(0);
            c_gal_aitoff->SetLogz(1);
            c_gal_aitoff->SetGridx(0);
            c_gal_aitoff->SetGridy(0);
            h2d[0]->Draw("aitoff");
            gSIM_CLUMPYAD->Draw();
            c_gal_aitoff->Modified();
            c_gal_aitoff->Update();
            if (gSIM_IS_WRITE_ROOTFILES && gSIM_IS_PRINT) c_gal_aitoff->Write();
         }

         jgal_sm_plot.clear();
         j_tot_plot.clear();
         jgal_sub_continuum_plot.clear();
         jgal_sub_drawn_plot.clear();
         jgal_crossprod_plot.clear();
         j_tot_gammasmoothed_plot.clear();
         j_tot_neutrinosmoothed_plot.clear();
      }

      //--- Population study
      // For Gal subclumps
      TCanvas **c_popcl = NULL;
      TH1D **h_popcl = NULL;
      TGraphAsymmErrors **gr_popcl = NULL;
      // For list haloes
      TCanvas **c_pophalos = NULL;
      TH1D **h_pophalos = NULL;
      TGraphAsymmErrors **gr_pophalos = NULL;

      if (is_list_added) {
         if (gSIM_IS_WRITE_ROOTFILES && gSIM_IS_PRINT)
            pop_study_plots(list_halos_in_map, list_jhalos, list_contrasthalos, list_boosthalos, c_pophalos, h_pophalos, gr_pophalos, 0., "list", popstudy_list_filename_str);
         else
            pop_study_plots(list_halos_in_map, list_jhalos, list_contrasthalos, list_boosthalos, c_pophalos, h_pophalos, gr_pophalos, 0., "list");
      }
      if (is_subs_drawn) {
         if (gSIM_IS_WRITE_ROOTFILES && gSIM_IS_PRINT)
            pop_study_plots(list_clumps, list_jcl, list_contrastcl, dummy, c_popcl, h_popcl, gr_popcl, 0., "subs", popstudy_drawn_filename_str);
         else
            pop_study_plots(list_clumps, list_jcl, list_contrastcl, dummy, c_popcl, h_popcl, gr_popcl, 0., "subs");
      }

      // statistical realizations plots:
      TCanvas **c_stat = NULL;
      if (gSTAT_N_REALIZATIONS != 0) {
         string stat_title;
         if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
            if (gSIM_IS_ASTRO_OR_PP_UNITS) stat_title = "log_{10}(J / M_{#odot}^{2} kpc^{-5})";
            else stat_title = "log_{10}(J / GeV^{2} cm^{-5})";
         } else {
            if (gSIM_IS_ASTRO_OR_PP_UNITS) stat_title = "log_{10}(D / M_{#odot} kpc^{-2})";
            else stat_title = "log_{10}(D / GeV cm^{-2})";
         };

         int i_stat_start = 7;
         int nfig_stat = 3;
         c_stat = new TCanvas*[nfig_stat];
         vector<string> c_name_stat;
         c_name_stat.push_back("logn_logj_stat");
         c_name_stat.push_back("cumullogn_logj_stat");
         c_name_stat.push_back("logprob_logj_stat");
         for (int i = 0; i < nfig_stat; ++i)
            c_name_stat[i] = "c_sub_" + c_name_stat[i];
         int x_pos_stat[nfig_stat + i_stat_start], y_pos_stat[nfig_stat + i_stat_start];
         for (int i = i_stat_start; i < i_stat_start + nfig_stat; ++i) {
            x_pos_stat[i] = 675 + (i % 3) * 310;
            y_pos_stat[i] = 20 + (i / 3) * 320;
         }
         for (int c = 0; c < nfig_stat; ++c) {
            c_stat[c] = new TCanvas(c_name_stat[c].c_str(), c_name_stat[c].c_str(), x_pos_stat[c], y_pos_stat[c], 300, 300);
            c_stat[c]->SetHighLightColor(kWhite);
            c_stat[c]->SetFillColor(kWhite);
            c_stat[c]->SetBorderMode(0);
            c_stat[c]->SetBorderSize(0);
            c_stat[c]->SetLeftMargin(0.15);
            c_stat[c]->SetRightMargin(0.01);
            c_stat[c]->SetTopMargin(0.01);
            c_stat[c]->SetBottomMargin(0.17);
            c_stat[c]->SetFrameBorderMode(0);
            c_stat[c]->SetFrameFillColor(kWhite);
            c_stat[c]->SetTickx(1);
            c_stat[c]->SetTicky(1);
            c_stat[c]->Range(-0.5, -4.5, 2.0, -0.15);
            c_stat[c]->SetLogx(0);
            c_stat[c]->SetLogy(1);
            c_stat[c]->SetGridx(0);
            c_stat[c]->SetGridy(0);
            if (c == 2) c_stat[c]->SetGrid();

            h_stat[c]->SetStats(0);
            h_stat[c]->SetLineColor(kBlack);
            h_stat[c]->SetLineStyle(1);
            h_stat[c]->SetLineWidth(1);
            h_stat[c]->SetTitle("");
            h_stat[c]->GetXaxis()->SetNdivisions(504);
            h_stat[c]->GetXaxis()->SetLabelOffset(0.01);
            h_stat[c]->GetXaxis()->SetLabelSize(0.065);
            h_stat[c]->GetXaxis()->SetLabelFont(132);
            h_stat[c]->GetXaxis()->SetTitleSize(0.075);
            h_stat[c]->GetXaxis()->SetTickLength(0.08);
            h_stat[c]->GetXaxis()->SetTitleOffset(1.);
            h_stat[c]->GetXaxis()->SetTitleFont(132);
            h_stat[c]->GetYaxis()->SetNdivisions(504);
            h_stat[c]->GetYaxis()->SetLabelOffset(0.);
            h_stat[c]->GetYaxis()->SetLabelSize(0.065);
            h_stat[c]->GetYaxis()->SetLabelFont(132);
            h_stat[c]->GetYaxis()->SetTitleSize(0.075);
            h_stat[c]->GetYaxis()->SetTickLength(0.08);
            h_stat[c]->GetYaxis()->SetTitleOffset(0.9);
            h_stat[c]->GetYaxis()->SetTitleFont(132);
            h_stat[c]->GetXaxis()->SetTitle(stat_title.c_str());
            if (c == 0) h_stat[c]->GetYaxis()->SetTitle("<N>");
            if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
               if (c == 1) h_stat[c]->GetYaxis()->SetTitle("<N_{cumulative}> (>J)");
               if (c == 2) h_stat[c]->GetYaxis()->SetTitle("p( #geq 1 clump( #geq J))");
            } else {
               if (c == 1) h_stat[c]->GetYaxis()->SetTitle("<N_{cumulative}> (>D)");
               if (c == 2) h_stat[c]->GetYaxis()->SetTitle("p( #geq 1 clump( #geq D))");
            }
            if (c != 2) h_stat[c]->SetMinimum(0.5 / double(gSTAT_N_REALIZATIONS));
            else h_stat[c]->SetMinimum(0.5 * prob_min);
            h_stat[c]->SetFillStyle(3001);
            h_stat[c]->Draw("E0HISTX0");
            gSIM_CLUMPYAD->Draw();
         }
      }

      if (gSIM_IS_WRITE_ROOTFILES && gSIM_IS_PRINT) {
         cout << " ... ROOT plots [use ROOT TBrowser] written in: " << filename_root_str << endl;
         cout << "_______________________" << endl << endl;
      }

      if (gSIM_IS_DISPLAY)
         gSIM_ROOTAPP->Run(kTRUE);

      if (root_file) {
         root_file->Close(); // closing ROOT file before display creates crash.
         delete root_file;
      }
      root_file = NULL;

      // Clean memory
      if (c_popcl) delete[] c_popcl;
      c_popcl = NULL;
      if (h_popcl) delete[] h_popcl;
      h_popcl = NULL;
      if (gr_popcl) delete[] gr_popcl;
      gr_popcl = NULL;
      if (c_pophalos) delete[] c_pophalos;
      c_pophalos = NULL;
      if (h_pophalos) delete[] h_pophalos;
      h_pophalos = NULL;
      if (gr_pophalos) delete[] gr_pophalos;
      gr_pophalos = NULL;
      if (gSTAT_N_REALIZATIONS != 0) {
         if (c_stat) delete[] c_stat;
         c_stat = NULL;
         if (h_stat) delete[] h_stat;
         h_stat = NULL;
      }

      delete[] h2d;
      h2d = NULL;
      if (h_stack) delete h_stack;
      h_stack = NULL;
      delete c_galcomp;
      c_galcomp = NULL;
      delete c_galtot;
      c_galtot = NULL;
      delete c_gal_aitoff;
      c_gal_aitoff = NULL;
      if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3) {
         delete c_galtot_smoothed_gamma ;
         c_galtot_smoothed_gamma = NULL;
      }
      if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3) {
         delete c_galtot_smoothed_neutrino ;
         c_galtot_smoothed_neutrino = NULL;
      }
   }
#endif
   // deallocate memory
   rs_pix_fov.clear() ;
   vector<int>().swap(v_pix_fov);

   if (smooth_gamma_or_nu_or_both != 0) {
      rs_pix_fov_extended.clear() ;
      vector<int>().swap(v_pix_fov_extended);
   }
}

//______________________________________________________________________________
void halo_files2files_data(string const &data_file, vector<string> &data_files)
{
   //--- Checks if data_file is a data analysis file, or a file containing a
   //    list of such files. Either way, a vector of names of files is returned.
   //
   //  data_files     File containing a single or a list of files


   string data_file_plain = data_file;
   resolve_envvar(data_file_plain);

   // Open file and check if exists
   ifstream f_mcmc(data_file_plain.c_str());
   if (!f_mcmc) {
      printf("\n====> ERROR: halo_files2files_data() in janalysis.cc");
      printf("\n             Cannot open (and read) file %s", data_file_plain.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }

   string line;
   data_files.clear();
   while (getline(f_mcmc, line)) {

      line.erase(remove_if(line.begin(), line.end(), isNotASCII), line.end());

      size_t first = line.find_first_not_of(" ");
      size_t last = line.find_first_of(" ", first + 1);
      string word = line.substr(first, last);
      word.erase(remove(word.begin(), word.end(), '\t'), word.end());

      string name = upper_case(word);
      if (name == "TYPEDATA") { // Find "TYPEDATA" keyword in the data file
         f_mcmc.close();
         data_files.push_back(data_file_plain);
         return;
      }
      if (word[0] == '#') continue;
      data_files.push_back(line);
   };
   f_mcmc.close();
   return;
}

//______________________________________________________________________________
double halo_findfracjtot_alphaint(int i, vector<struct gStructHalo> &list_halos,
                                  double const &frac)
{
   //--- Returns alphaint_rad [kpc] for which J(alphaint_rad)=frac*jmax is
   //    evaluated for alphaint encompassing the halo.
   //    N.B.: calculated with precision eps=0.1*(1-frac)), where jmax
   //    includes smooth+<subs>+cross-prod.
   //
   //  i              i-th element of list_halos
   //  list_halos     Vector of halos
   //  frac           Fraction sought


   double eps = 0.1 * (1. - frac);
   double alphaint_rad = 0.;
   double alphaint_ref = gSIM_ALPHAINT;

   // Set halo parameters (and triaxiality)
   const int npar1 = 10;
   double par_halotot[npar1 + 1];
   halo_set_partot(i, par_halotot, list_halos, true);
   halo_set_triaxiality(i, list_halos);


   // If subhalos
   if (list_halos[i].Subs_mfrac > 1.e-5) {
      const int npar_dpdv = 10;
      double par_halodpdv[npar_dpdv];
      const int npar_subs = 24;
      double par_halosubs[npar_subs];
      halo_set_pardpdv(i, par_halodpdv, list_halos, true);
      halo_set_parsubs(i, par_halosubs, list_halos);
      double mhalotot_subs = list_halos[i].Mtot * list_halos[i].Subs_mfrac;
      double nhalotot_subs = nsubtot_from_msubtot(mhalotot_subs, gDM_SUBS_MMIN, list_halos[i].Subs_Mmax, &par_halosubs[8], eps);

      // The half-angular size of the clump on the sky is asin(R_cl/l_cl)
      // N.B.: if the clump is too close, the angle is ill-defined. In that case
      // we set it to PI/2
      gSIM_ALPHAINT = min(PI / 2., fabs(asin(par_halotot[6] / par_halotot[7])));
      double jtot = halo_jtot(i, list_halos, par_halotot[8], par_halotot[9], eps, true);

      // Find alpha_int to have frac*Jtot
      find_fracjtot_alphaint(par_halotot, list_halos[i].Mtot, list_halos[i].Subs_mfrac,
                             par_halodpdv, par_halosubs, nhalotot_subs, jtot, frac, alphaint_rad);
   } else
      find_fracjtot_alphaint(par_halotot, frac, alphaint_rad);

   gSIM_ALPHAINT = alphaint_ref;
   return alphaint_rad;
}


//______________________________________________________________________________
void halo_fracjpointlike_dist(string const &file_halos, double const &alphaint,
                              double const &frac, bool is_annihil, double const &rho_sat)
{
   //--- Distance for which J = frac * Jpointlike = frac L/d^2.
   //
   //  file_halos     File containing the list of halos
   //  alphaint       Integration angle [rad]
   //  frac           Fraction of Jtot required
   //  rho_sat        Saturation density (in Msol/kpc3)
   //  is_annihil     For annihilation or decay


   gDM_RHOSAT = rho_sat;
   vector<struct gStructHalo> list_halos;
   halo_load_list(file_halos, list_halos);
   gPP_DM_IS_ANNIHIL_OR_DECAY = is_annihil;
   char j_or_d = 'J';
   if (!gPP_DM_IS_ANNIHIL_OR_DECAY)
      j_or_d = 'D';

   double eps = 1.e-2 * (1. - frac);
   cout << ">>>>> Distance [kpc] to set the halo (for alpha_int=" << alphaint *RAD_to_DEG << " deg)" << endl;
   cout << "      for which " << j_or_d << "(d) = " << frac * 100  << "% of " << j_or_d << "point-like (=L/d^2)" << endl;
   cout << "      for all halos in " << file_halos << endl << endl;

   const int n_obj = (int)list_halos.size();

   if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
      printf("   Halo       J_pointlike    J_x        d\n");
      printf("                  [Msol^2/kpc^5]      [kpc]\n");
   } else {
      printf("   Halo       D_pointlike    D_x        d\n");
      printf("                   [Msol/kpc^2]       [kpc]\n");
   }

   gSIM_ALPHAINT = alphaint;

   // Calculate distance for all halos
   for (int i = 0; i < n_obj ; ++i) {

      // Update par (and triaxiality) and store graph names
      const int n_par = 10;
      double par[n_par + 1];
      halo_set_partot(i, par, list_halos, true);
      halo_set_triaxiality(i, list_halos);

      const int n_par_tot = 21;
      double par_tmp[n_par_tot];
      for (int j = 0; j < 7; ++j)
         par_tmp[j] = par[j];
      for (int j = 7; j < 11; ++j)
         par_tmp[j] = 0.;
      for (int j = 11; j < n_par_tot; ++j)
         par_tmp[j] = 0.;

      // Fill the dist for which J(d) = (frac*Jpointlike=frac*L/d^2)
      double dist = 0.;
      find_fracjpointlike_dist(par, frac, dist);
      double jpointlike = lum_singlehalo_nosubs_mix(par_tmp, gSIM_EPS) / (dist * dist);
      par[7] = dist;
      double j = jsmooth(par, 0., 0., eps);

      printf(" %-12s   %.2le   %.2le  %.2le\n",
             (list_halos[i].Name).c_str(), jpointlike, j, dist);
   }
}

//______________________________________________________________________________
void halo_fracjtot_alphaint(string const &file_halos, double const &frac,
                            bool is_annihil, double const &rho_sat)
{
   //--- Integration angle for which J = frac * Jtot.
   //
   //  file_halos     File containing the list of halos
   //  frac           Fraction of Jtot required
   //  rho_sat        Saturation density (in Msol/kpc3)
   //  is_annihil     For annihilation or decay


   gDM_RHOSAT = rho_sat;
   vector<struct gStructHalo> list_halos;
   halo_load_list(file_halos, list_halos);
   gPP_DM_IS_ANNIHIL_OR_DECAY = is_annihil;
   char j_or_d = 'J';
   if (!gPP_DM_IS_ANNIHIL_OR_DECAY)
      j_or_d = 'D';

   double eps = 1.e-1 * (1. - frac);
   cout << ">>>>> Integration angle (in deg) to get J_x=" << frac * 100
        << "% of " << j_or_d << " (insensitive to rhos)" << endl;
   cout << "      for all halos in " << file_halos << endl << endl;
   double ref_ALPHA_APERTURE_EXP = gSIM_ALPHAINT;

   const int n_obj = (int)list_halos.size();

   if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
      printf("   Halo            d       alpha_100%%    J_100%%      alpha_x       J_x      as=asin(rs/d) alpha_x/a_s\n");
      printf("                 [kpc]       [deg]   [Msol^2/kpc^5]   [deg]   [Msol^2/kpc^5]     [deg]         -\n");
   } else {
      printf("   Halo            d       alpha_100%%    D_100%%      alpha_x       D_x      as=asin(rs/d) alpha_x/a_s\n");
      printf("                 [kpc]       [deg]    [Msol/kpc^2]    [deg]     [Msol/kpc^2]     [deg]         -\n");
   }
   // Calculate and fill angle_rpercent for all halos
   for (int i = 0; i < n_obj ; ++i) {

      // Update par (and triaxiality) and store graph names
      const int n_par = 10;
      double par[n_par + 1];
      halo_set_partot(i, par, list_halos, true);
      halo_set_triaxiality(i, list_halos);

      // The half-angular size of the clump on the sky is asin(R_cl/l_cl)
      // N.B.: if the clump is too close, the angle is ill-defined. In that case
      // we set it to PI/2
      gSIM_ALPHAINT = min(PI / 2., fabs(asin(par[6] / par[7])));

      // Fill the alpha(J_xpercent)
      double alpha_rad = halo_findfracjtot_alphaint(i, list_halos, frac);
      //find_fracjtot_alphaint(par, frac, alpha_rad);
      double alpha_deg = alpha_rad * RAD_to_DEG;
      double a_s = asin(list_halos[i].Rscale * (1 + list_halos[i].z) / list_halos[i].l) * RAD_to_DEG;
      gSIM_ALPHAINT = alpha_rad;
      double j_xpercent =  halo_jtot(i,  list_halos, 0., 0., eps, true);
      double jtot = j_xpercent / frac;

      printf(" %-12s   %.2le   %.2le     %.2le     %.2le    %.2le       %.2le    %.2le\n",
             (list_halos[i].Name).c_str(), list_halos[i].l,
             fabs(asin(par[3] / list_halos[i].l) * RAD_to_DEG), jtot,
             alpha_deg, j_xpercent, a_s, alpha_deg / a_s);
   }

   gSIM_ALPHAINT = ref_ALPHA_APERTURE_EXP;
}

//______________________________________________________________________________
int halo_get_index(string const &name, string const &type, vector<struct gStructHalo> &list_halos)
{
   //--- Returns the index in list_halos matching name and type (case insensitive).
   //
   //  name           Name of the object
   //  type           Type of the object
   //  list_halos     List of loaded halos


   // Check if list is empty
   const int n_obj = (int)list_halos.size();
   if (n_obj == 0) {
      printf("\n====> ERROR: halo_get_index() in janalysis.cc");
      printf("\n             The list 'list_halos' is empty, call first halo_load_list()");
      printf("\n             => abort()\n\n");
      abort();
   }

   string nam = upper_case(name);
   string typ = upper_case(type);
   for (int i = 0; i < n_obj ; ++i) {
      string nam_list = upper_case(list_halos[i].Name);
      string typ_list = upper_case(list_halos[i].Type);
      if (nam_list == nam && typ_list == typ)
         return i;
   }

   printf("\n====> ERROR: halo_get_index() in janalysis.cc");
   printf("\n             Name=%s and Type=%s not found in 'list_halos'", name.c_str(), type.c_str());
   printf("\n             => abort()\n\n");
   abort();
}

//______________________________________________________________________________
string halo_get_name(int i, vector<struct gStructHalo> &list_halos)
{
   //--- Returns ROOT-compliant (no '+', '-'...) name of i-th object.
   //
   //  i              i-th element of list_halos
   //  list_halos     Vector of halos


   if (i < 0 || i >= (int)list_halos.size()) {
      printf("\n====> ERROR: halo_get_name() in janalysis.cc");
      printf("\n             Index i=%d is out of range", i);
      printf("\n             => abort()\n\n");
      abort();
   }

   // returns a name (discarding characters troublesome for ROOT displays)
   string tmp_name = list_halos[i].Name;
   for (int iii = 0; iii < (int)tmp_name.size(); ++iii)
      if (tmp_name[iii] == ' ' || tmp_name[iii] == '-'
            || tmp_name[iii] == '+' || tmp_name[iii] == '.')
         tmp_name[iii] = '_';

   return tmp_name;
}

//______________________________________________________________________________
string halo_get_type(int i, vector<struct gStructHalo> &list_halos)
{
   //--- Returns ROOT-compliant (no '+', '-'...) name of i-th object.
   //
   //  i              i-th element of list_halos
   //  list_halos     Vector of halos


   if (i < 0 || i >= (int)list_halos.size()) {
      printf("\n====> ERROR: halo_get_type() in janalysis.cc");
      printf("\n             Index i=%d is out of range", i);
      printf("\n             => abort()\n\n");
      abort();
   }

   // Returns a name (discarding characters troublesome for ROOT displays)
   string tmp_type = list_halos[i].Type;
   root_trimname4object(tmp_type);
   return tmp_type;
}

//______________________________________________________________________________
void halo_j1D(vector<double> const &x, int switch_y)
{
   //--- Prints/Plots y(x) and y(x)/y(x[0]) for a halo: smooth (+ meansub & boost if is_subs=true).
   //
   //  x              Grid of x-axis values
   //  switch_y       Quantity to display
   //                    0 => rho(r_kpc)
   //                    1 => J(alphaint_deg)
   //                    2 => J(theta_deg)
   //                    3 => Mass(r_kpc)
   //  par_dpdv[0]    dpdv normalisation [kpc^{-3}]


   // DAVID TEST
   int switch_x_axis = 0;
   // If switch_y = 1 or switch_y=2, switch_x_axis has three options for x_axis
   //    0 => J(alpha_int)
   //    1 => J(alpha_int*d)
   //    2 => J(alpha_int/alpha_s)

   bool is_shell = false; // hardcoded switch suppressing printing/plotting of shell contributions


   // Load CLUMPY and halo parameters
   vector<struct gStructHalo> list_halos;

   halo_load_list(gLIST_HALOES, list_halos);

   // Set ROOT style
#if IS_ROOT
   rootstyle_set2CLUMPY();
#endif

   char j_or_d = 'D';
   if (gPP_DM_IS_ANNIHIL_OR_DECAY)
      j_or_d = 'J';

   // Is flux?
   bool is_flux = gSIM_IS_WRITE_FLUXMAPS;
   if (gPP_DM_MASS_GEV < gSIM_FLUX_AT_E_GEV || switch_y == 0 || switch_y == 3)
      is_flux = false;
   double par_spec[6];
   par_spec[0] = gPP_DM_MASS_GEV;
   par_spec[1] = gPP_FLAG_SPECTRUMMODEL;
   par_spec[2] = kGAMMA;
   par_spec[3] = 0.; // reshift; will be updated later
   par_spec[4] = 0.;
   par_spec[5] = 0.;

   vector<bool> is_subs;
   bool is_subs_all = false;
   for (int i = 0; i < (int)list_halos.size(); ++i) {
      is_subs.push_back(false);
      if (list_halos[i].Subs_mfrac >= 1.e-3) {
         is_subs[i] = true;
         is_subs_all = true;
      }
   }

   double old_alpha_aperture = gSIM_ALPHAINT;
   int n_x = x.size();
   int n_obj = (int)list_halos.size();


   // What is going to be calculated (for prints and plots)
   // -> units_or_canvasname_1D(switch_y, switch_name, is_norm)
   string cvs_name = units_or_canvasname_1D(switch_y, 0, false);
   string x_name = units_or_canvasname_1D(switch_y, 1, false);
   string y_name = units_or_canvasname_1D(switch_y, 2, false);

   string y_name_shell_tmp = y_name.substr(1, y_name.size() - 2);

   //if (gPP_DM_IS_ANNIHIL_OR_DECAY) sprintf(y_name_shell_char, "dJ/d#Omega%s sr^{-1}]", y_name_shell_tmp.c_str());
   string y_name_shell;
   if (gPP_DM_IS_ANNIHIL_OR_DECAY) y_name_shell = "dJ/d#Omega" + y_name_shell_tmp + " sr^{-1}]";
   else y_name_shell = "dD/d#Omega" + y_name_shell_tmp + " sr^{-1}]";

   string y_name_flux = "Flux [# cm^{-2} s^{-1}]";
   string y_name_intensity = "Intensity [# cm^{-2} s^{-1} sr^{-1}]";

   string y_name_norm = units_or_canvasname_1D(switch_y, 2, true);
   string leg_sm = "#rho_{sm}", leg_cl = "<#rho_{sub}>", leg_cp = "null", leg_tot = "#rho_{tot}";

   if (switch_y > 0) {
      char j_or_d_str[20];
      sprintf(j_or_d_str, "%c", j_or_d);
      leg_sm = (string)j_or_d_str + "_{sm}";
      leg_cl = "<" + (string)j_or_d_str + "_{sub}>";
      leg_cp = (string)j_or_d_str + "_{cross-prod}";
      if (switch_y != 3) leg_tot = (string)j_or_d_str + "_{tot}";
      else leg_tot = "M_{tot}";
   }

   if (!is_subs_all)
      cout << ">>>>> " << y_name << " for all halos in " << gLIST_HALOES << endl;
   else {
      cout << ">>>>> " << y_name << " for smooth and subclumps (for all halos in "
           << gLIST_HALOES << ")" << endl;
   }
   if (switch_y == 2)
      cout << "  => alpha_int = " << gSIM_ALPHAINT *RAD_to_DEG << " [deg]" << endl;
   if (switch_y == 1 || switch_y == 2)
      cout << "  => Relative precision: " << gSIM_EPS << endl;

   if (n_obj <= 0) {
      cout << "...... no halos found in the list: nothing to do" << endl;
      return;
   }

   //--- x vector in degrees (for switch_y == 1 || switch_y == 2)
   vector<double> x_deg;

   vector<vector<double> > flux_gamma(n_obj, vector<double>(n_x, 1.e-40));
   vector<vector<double> > flux_nu(n_obj, vector<double>(n_x, 1.e-40));
   vector<vector<double> > intensity_gamma(n_obj, vector<double>(n_x, 1.e-40));
   vector<vector<double> > intensity_nu(n_obj, vector<double>(n_x, 1.e-40));

   //--- Storage variables (for smooth)
   vector<string> names, names_sm, names_cl, names_cp, names_tot, names_boost, legend;
   vector<double> y_sm(n_obj * n_x, 1.e-40);
   vector<double> y_sm_norm(n_obj * n_x, 1.e-40);
   // Shell used only if switch_y==1 (i.e. J(alpha_int))
   vector<double> y_sm_shell(n_obj * n_x, 1.e-40);
   vector<double> y_sm_shell_norm(n_obj * n_x, 1.e-40);
   //--- Storage variables (for meansub)
   vector<double> y_cl(n_obj * n_x, 1.e-40);
   vector<double> y_cl_norm(n_obj * n_x, 1.e-40);
   vector<double> y_cp(n_obj * n_x, 1.e-40);
   vector<double> y_cp_norm(n_obj * n_x, 1.e-40);
   vector<double> y_tot(n_obj * n_x, 1.e-40);
   vector<double> y_tot_norm(n_obj * n_x, 1.e-40);
   vector<double> y_boost(n_obj * n_x, 1.e-40);        // used only if switch_y==1 or switch_y==2
   vector<double> y_cl_shell(n_obj * n_x, 1.e-40);     // used only if switch_y==1 (i.e. J(alpha_int))
   vector<double> y_cl_shell_norm(n_obj * n_x, 1.e-40); // used only if switch_y==1 (i.e. J(alpha_int))
   vector<double> y_cp_shell(n_obj * n_x, 1.e-40);     // used only if switch_y==1 (i.e. J(alpha_int))
   vector<double> y_cp_shell_norm(n_obj * n_x, 1.e-40); // used only if switch_y==1 (i.e. J(alpha_int))
   vector<double> y_tot_shell(n_obj * n_x, 1.e-40);    // used only if switch_y==1 (i.e. J(alpha_int))
   vector<double> y_tot_shell_norm(n_obj * n_x, 1.e-40); // used only if switch_y==1 (i.e. J(alpha_int))

   // way to plot finite values without segfaults...
   vector< vector<double> > x_deg_sm_shell_plot(n_obj);
   vector< vector<double> > x_deg_cl_shell_plot(n_obj);
   vector< vector<double> > x_deg_cp_shell_plot(n_obj);
   vector< vector<double> > x_deg_tot_shell_plot(n_obj);
   vector< vector<double> > y_sm_shell_plot(n_obj);
   vector< vector<double> > y_cl_shell_plot(n_obj);
   vector< vector<double> > y_cp_shell_plot(n_obj);
   vector< vector<double> > y_tot_shell_plot(n_obj);

   FILE *fp[3] = {NULL, NULL, NULL};
   FILE *fp_current;
   string f_halo_name = gLIST_HALOES;
   // If f_halo_name from a subdir, only keep name for storage
   vector<string> tmp_wdir;
   string2list(f_halo_name, "/", tmp_wdir);
   string f_name = gSIM_OUTPUT_DIR + tmp_wdir[tmp_wdir.size() - 1] + "." + cvs_name + ".output";
   fp[0] = fopen(f_name.c_str(), "w");
   fp[1] = stdout;
   string f_name_flux;
   if (is_flux) {
      if (switch_y == 1) f_name_flux  = gSIM_OUTPUT_DIR + tmp_wdir[tmp_wdir.size() - 1] + ".fluxes.output";
      else if (switch_y == 2) f_name_flux  = gSIM_OUTPUT_DIR + tmp_wdir[tmp_wdir.size() - 1] + ".intensities.output";
      else {
         printf("\n====> ERROR: halo_j1D() in janalysis.cc");
         printf("\n             Flux calculation w/o switch_y == 1/2 not allowed.");
         printf("\n             => abort()\n\n");
         abort();
      }
      fp[2] = fopen(f_name_flux.c_str(), "w");
   }

   // First call of flux (dummy to load file not in the middle of other prints)
   if (is_flux) {
      par_spec[2] = kGAMMA;
      par_spec[3] = 1.;
      flux(par_spec, gSIM_FLUX_AT_E_GEV, 1., false);
      par_spec[2] = kNEUTRINO;
      if (gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_EW || gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_NOEW) {
         flux(par_spec, gSIM_FLUX_AT_E_GEV, 1., false);
      }
      par_spec[3] = 0.;
   }

   vector<string> list_type;
   const int npar2 = 25;
   double par_subs[npar2];

   // DAVID
   // Higher-level printing option
   bool is_option = false;

   // Calculate and fill rho for all halos
   for (int i = 0; i < n_obj ; ++i) {
      if (is_option) {
         if (i == 0) {
            printf("#       NAME     IND_MCXC    l        b        d      alpha_s        %csm         %csub         %ctot \n", j_or_d, j_or_d, j_or_d);
            printf("#         -         -      [deg]    [deg]    [kpc]     [deg]           %s\n", y_name.c_str());
         }
      } else {
         printf("\n   ... processing %s %s [#%d]...\n", halo_get_name(i, list_halos).c_str(), halo_get_type(i, list_halos).c_str(), i + 1);
         if (is_subs[i])
            printf("  => frac_DM=%le, cdelta-mdelta=%s, dPdV=%s, alphaE=%le, rscale=%le \n", list_halos[i].Subs_mfrac, gNAMES_CDELTAMDELTA[list_halos[i].Subs_Inner_CDELTAMDELTA],  gNAMES_PROFILE[list_halos[i].Subs_dPdV_Profile], list_halos[i].Subs_dPdV_ShapeParam1,  list_halos[i].Subs_dPdV_Rscale);
      }

      // Update par (for smooth and sub, and triaxiality) and store graph names
      const int npar1 = 10;
      const int npar3 = 21;
      double par_tot[npar1 + 1];
      double par_dpdv[npar1];
      double par_cl_mean[npar1];
      double par_smooth[npar3];
      halo_set_partot(i, par_tot, list_halos, true);
      halo_set_triaxiality(i, list_halos);


      if (is_subs[i]) {
         halo_set_pardpdv(i, par_dpdv, list_halos, true);
         halo_set_parsubs(i, par_subs, list_halos);
         halo_set_parsmooth(i, par_smooth, list_halos, true);
         for (int ii = 0; ii < npar1; ++ii)
            par_cl_mean[ii] = par_dpdv[ii];
         par_cl_mean[0] *= list_halos[i].Mtot * list_halos[i].Subs_mfrac;
      }

      double mtot_subs = list_halos[i].Mtot * list_halos[i].Subs_mfrac;
      double mmin_subs = gDM_SUBS_MMIN;
      double mmax_subs = list_halos[i].Subs_Mmax;
      double ntot_subs = 0.;
      if (is_subs[i])
         ntot_subs = nsubtot_from_msubtot(mtot_subs, mmin_subs, mmax_subs, &par_subs[8], gSIM_EPS);
      double psi_los = 0.;
      double theta_los = 0.;
      double lmin = max(0., list_halos[i].l - list_halos[i].Rvir);
      double lmax = list_halos[i].l + list_halos[i].Rvir;

      string tmp_name = halo_get_name(i, list_halos) + "_" + halo_get_type(i, list_halos);
      names.push_back(tmp_name);
      names_sm.push_back("sm_" + tmp_name);
      legend.push_back(list_halos[i].Name + " (" + list_halos[i].Type + ")");


      if (is_subs_all) {
         // Add Type in list if new
         bool is_new = true;
         for (int k = 0; k < (int)list_type.size(); ++k) {
            if (list_halos[i].Type == list_type[k]) {
               is_new = false;
               break;
            }
         }
         if (is_new)
            list_type.push_back(list_halos[i].Type);

         // Names for graph
         names_cl.push_back("subcl_" + tmp_name);
         if (switch_y != 0 && gPP_DM_IS_ANNIHIL_OR_DECAY) {
            names_cp.push_back("crossprod_" + tmp_name);
         }
         names_tot.push_back("tot_" + tmp_name);
         names_boost.push_back("boost_" + tmp_name);
      }

      int ff_max = 2;
      if (is_flux) ff_max = 3;
      // Print header
      if (gSIM_IS_PRINT && !is_option) {
         // Print header
         for (int ff = 0; ff < ff_max; ++ff) {
            fp_current = fp[ff];
            if (ff == 0 or ff == 2) fprintf(fp_current, "#   ... processing %s ...\n", names[i].c_str());
         }
         for (int ff = 0; ff < 2; ++ff) {
            fp_current = fp[ff];
            fprintf(fp_current, "# x= %s\n", x_name.c_str());
            fprintf(fp_current, "# y= %s\n", y_name.c_str());
            if (!is_subs[i] || switch_y == 3) {
               if (switch_y == 2 or (switch_y == 1 and is_shell))
                  fprintf(fp_current, "#       x              y            y/y_norm   y/dOmega\n");
               else
                  fprintf(fp_current, "#       x              y            y/y_norm\n");
            } else {
               if (switch_y == 0) {
                  fprintf(fp_current, "#      x               y[sm|cl|tot]               y/y_norm[sm|cl|tot]\n");
               } else if (switch_y == 2 or (switch_y == 1 and is_shell)) {
                  if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                     fprintf(fp_current, "#      x                  y[sm|cl|cp|tot]                     y/y_norm[sm|cl|cp|tot]              y/dOmega[sm|cl|cp|tot]       Boost\n");
                  else
                     fprintf(fp_current, "#      x               y[sm|cl|tot]               y/y_norm[sm|cl|tot]        y/dOmega[sm|cl|tot]  Boost\n");
               } else {
                  if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                     fprintf(fp_current, "#      x                  y[sm|cl|cp|tot]                     y/y_norm[sm|cl|cp|tot]             Boost\n");
                  else
                     fprintf(fp_current, "#      x               y[sm|cl|tot]               y/y_norm[sm|cl|tot]         Boost\n");
               }
            }
         }
         if (switch_y == 1 and is_flux) fprintf(fp[2], "# %s\t%s (Gammas)\t%s (Neutrinos)\n", x_name.c_str(), y_name_flux.c_str(), y_name_flux.c_str());
         else if (switch_y == 2 and is_flux) fprintf(fp[2], "# %s\t%s (Gammas)\t%s (Neutrinos)\n", x_name.c_str(), y_name_intensity.c_str(), y_name_intensity.c_str());
      }

      // Calculate y
      // 0. rho(r)
      if (switch_y == 0) {
         double y_sm_max = 1e-40;
         double y_cl_max = 1e-40;
         double y_tot_max = 1e-40;

         for (int k = 0; k < n_x; ++k) {
            double tmp_x = x[k] / (1 + list_halos[i].z); // comoving coordinates

            // cut off at Rvir;
            if (x[k] > list_halos[i].Rvir * (1 + list_halos[i].z)) {
               y_sm[i * n_x + k] = 1e-40;
               y_tot[i * n_x + k] = 1e-40;
               y_cl[i * n_x + k] = 1e-40;
               y_sm_norm[i * n_x + k] = 1e-40;
               y_tot_norm[i * n_x + k] = 1e-40;
               y_cl_norm[i * n_x + k] = 1e-40;
            } else {

               // smooth halo
               if (is_subs[i])
                  rho_mix(tmp_x, par_smooth, y_sm[i * n_x + k]);
               else
                  rho(tmp_x, par_tot, y_sm[i * n_x + k]);

               if (y_sm[i * n_x + k] >  y_sm_max) y_sm_max = y_sm[i * n_x + k];

               // subs
               if (is_subs[i]) {
                  rho(tmp_x, par_cl_mean, y_cl[i * n_x + k]);
                  y_tot[i * n_x + k] = y_sm[i * n_x + k] + y_cl[i * n_x + k];

                  if (y_cl[i * n_x + k] >  y_cl_max) y_cl_max = y_cl[i * n_x + k];
                  if (y_tot[i * n_x + k] >  y_tot_max) y_tot_max = y_tot[i * n_x + k];
                  // Convert before printing
                  convert_to_PP_units(0, y_cl[i * n_x + k]);
                  y_cl[i * n_x + k] /= pow(1 + list_halos[i].z, 3.); // comoving coordinates
                  convert_to_PP_units(0, y_tot[i * n_x + k]);
                  y_tot[i * n_x + k] /= pow(1 + list_halos[i].z, 3.); // comoving coordinates
               }

               // Convert before printing
               convert_to_PP_units(0, y_sm[i * n_x + k]);
               y_sm[i * n_x + k] /= pow(1 + list_halos[i].z, 3.); // comoving coordinates

               // set y_norm:
               double y_sm_max_tmp = y_sm_max;
               double y_tot_max_tmp = y_tot_max;
               double y_cl_max_tmp = y_cl_max;
               convert_to_PP_units(0, y_sm_max_tmp);
               y_sm_max_tmp /= pow(1 + list_halos[i].z, 3.);
               if (is_subs[i]) {
                  convert_to_PP_units(0, y_tot_max_tmp);
                  y_tot_max_tmp /= pow(1 + list_halos[i].z, 3.);
               }
               if (is_subs[i]) {
                  convert_to_PP_units(0, y_cl_max_tmp);
                  y_cl_max_tmp /= pow(1 + list_halos[i].z, 3.);
               }
               for (int kk = 0; kk <= k; ++kk) {
                  y_sm_norm[i * n_x + kk] = y_sm[i * n_x + kk] / y_sm_max_tmp;
                  if (is_subs[i]) y_tot_norm[i * n_x + kk] = y_tot[i * n_x + kk] / y_tot_max_tmp;
                  if (is_subs[i]) y_cl_norm[i * n_x + kk] = y_cl[i * n_x + kk] / y_cl_max_tmp;
               }
            }
            // Print on screen and in file)
            if (gSIM_IS_PRINT) {
               for (int ff = 0; ff < 2; ++ff) {
                  fp_current = fp[ff];
                  if (!is_subs[i])
                     fprintf(fp_current, "  %.*le   %.*le   %.*le\n", gSIM_SIGDIGITS + 2, x[k],  gSIM_SIGDIGITS, y_sm[i * n_x + k],  gSIM_SIGDIGITS, y_sm_norm[i * n_x + k]);
                  else
                     fprintf(fp_current, "  %.*le     %.*le %.*le %.*le    %.*le %.*le %.*le\n",
                             gSIM_SIGDIGITS + 2, x[k],  gSIM_SIGDIGITS, y_sm[i * n_x + k],  gSIM_SIGDIGITS, y_cl[i * n_x + k],  gSIM_SIGDIGITS, y_tot[i * n_x + k],
                             gSIM_SIGDIGITS, y_sm_norm[i * n_x + k],  gSIM_SIGDIGITS, y_cl_norm[i * n_x + k], gSIM_SIGDIGITS,  y_tot_norm[i * n_x + k]);
               }
            }
         }

         // print inner and outer slope of profiles:
         double innerslope_tot;
         double outerslope_tot;
         double r_inner = 1e-40 * par_tot[1];
         double r_outer = 1e40 * par_tot[1];
         dlog_rho_dlog_r(r_inner, par_tot, innerslope_tot);
         dlog_rho_dlog_r(r_outer, par_tot, outerslope_tot);
         if (outerslope_tot < -1e6)
            printf("\n  rho_tot:    inner logarithmic slope = %+.3g,\t outer slope = -Infinity",
                   round(innerslope_tot, 3));
         else printf("\n  rho_tot:    inner logarithmic slope = %+.3g,\t outer slope = %+.3g",
                        round(innerslope_tot, 3), round(outerslope_tot, 3));
         if (is_subs[i]) {
            double innerslope_dpdv;
            double outerslope_dpdv;
            r_inner = 1e-40 * par_dpdv[1];
            r_outer = 1e40 * par_dpdv[1];
            dlog_rho_dlog_r(r_inner, par_dpdv, innerslope_dpdv);
            dlog_rho_dlog_r(r_outer, par_dpdv, outerslope_dpdv);
            if (outerslope_dpdv < -1e6)
               printf("\n  rho_<subs>: inner logarithmic slope = %+.3g,\t outer slope = -Infinity",
                      round(innerslope_dpdv, 3));
            else printf("\n  rho_<subs>: inner logarithmic slope = %+.3g,\t outer slope = %+.3g",
                           round(innerslope_dpdv, 3), round(outerslope_dpdv, 3));

         }

         // 1. J(alpha_int)
      } else if (switch_y == 1) {
         // Start with last bin (for normalisation purpose)
         // also, rescale alphaint with redshift to account for angular diameter distortion
         if (list_halos[i].z > ZMIN_EXTRAGAL) gSIM_ALPHAINT = atan(1. / (1 + list_halos[i].z) * MY_TAN(x[n_x - 1]));
         else gSIM_ALPHAINT = x[n_x - 1];

         // smooth contrib.
         if (is_subs[i])
            y_sm[i * n_x + n_x - 1] = jsmooth_mix(list_halos[i].Mtot, par_tot, psi_los, theta_los, gSIM_EPS, list_halos[i].Subs_mfrac, par_dpdv);
         else
            y_sm[i * n_x + n_x - 1] = jsmooth(par_tot, psi_los, theta_los, gSIM_EPS);

         // subs contrib.
         if (is_subs[i]) {
            y_cl[i * n_x + n_x - 1] = jsub_continuum(ntot_subs, par_dpdv, psi_los, theta_los, lmin, lmax, par_subs, mmin_subs, mmax_subs);
            y_tot[i * n_x + n_x - 1] = y_sm[i * n_x + n_x - 1] + y_cl[i * n_x + n_x - 1];
            if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
               y_cp[i * n_x + n_x - 1] = jcrossprod_continuum(list_halos[i].Mtot, par_tot, psi_los, theta_los, lmin, lmax, gSIM_EPS, list_halos[i].Subs_mfrac, par_dpdv);
               y_tot[i * n_x + n_x - 1] += y_cp[i * n_x + n_x - 1];
            }
         }

         double y_sm_max =  y_sm[i * n_x + n_x - 1];
         double y_cl_max =  y_cl[i * n_x + n_x - 1];
         double y_cp_max =  y_cp[i * n_x + n_x - 1];
         double y_tot_max =  y_tot[i * n_x + n_x - 1];

         for (int k = 0; k < n_x; ++k) {
            if (list_halos[i].z > ZMIN_EXTRAGAL) gSIM_ALPHAINT = atan(1. / (1 + list_halos[i].z) * MY_TAN(x[k]));
            else gSIM_ALPHAINT = x[k];

            if (gSIM_ALPHAINT > 1.e-30) {
               if (k != n_x - 1) {
                  if (is_subs[i])
                     y_sm[i * n_x + k] = jsmooth_mix(list_halos[i].Mtot, par_tot, psi_los, theta_los, gSIM_EPS, list_halos[i].Subs_mfrac, par_dpdv);
                  else
                     y_sm[i * n_x + k] = jsmooth(par_tot, psi_los, theta_los, gSIM_EPS);
               }
               y_sm_norm[i * n_x + k] = y_sm[i * n_x + k] / y_sm_max;
               if (is_subs[i]) {
                  if (k != n_x - 1) {
                     y_cl[i * n_x + k] = jsub_continuum(ntot_subs, par_dpdv, psi_los, theta_los, lmin, lmax, par_subs, mmin_subs, mmax_subs);
                     if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                        y_cp[i * n_x + k] = jcrossprod_continuum(list_halos[i].Mtot, par_tot, psi_los, theta_los, lmin, lmax, gSIM_EPS, list_halos[i].Subs_mfrac, par_dpdv);
                  }
                  y_cl_norm[i * n_x + k] = y_cl[i * n_x + k] / y_cl_max;
                  y_tot[i * n_x + k] = y_sm[i * n_x + k] + y_cl[i * n_x + k];
                  if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
                     y_cp_norm[i * n_x + k] = y_cp[i * n_x + k] / y_cp_max;
                     y_tot[i * n_x + k] += y_cp[i * n_x + k];
                  }
                  y_tot_norm[i * n_x + k] = y_tot[i * n_x + k] / y_tot_max;
                  y_boost[i * n_x + k] = y_tot[i * n_x + k] / jsmooth(par_tot, psi_los, theta_los, gSIM_EPS);
               }
            }

            // now x[k] can be converted to degrees for display purpose:
            x_deg.push_back(x[k] * RAD_to_DEG);

            // Convert before printing
            convert_to_PP_units(1, y_sm[i * n_x + k]);
            convert_to_PP_units(1, y_cl[i * n_x + k]);
            convert_to_PP_units(1, y_cp[i * n_x + k]);
            convert_to_PP_units(1, y_tot[i * n_x + k]);

            // Calculate shell contributions
            double delta_area = 0;
            if (k != 0) {
               double area1 = 2.* PI * (1. - MY_COS(x[k - 1]));
               double area2 = 2.* PI * (1. - MY_COS(x[k]));
               delta_area = (area2 - area1);
            }

            if (k == 0) {
               y_sm_shell[i * n_x + 0] = 1e-40;
               if (is_subs[i]) {
                  y_cl_shell[i * n_x + 0] = 1e-40;
                  y_tot_shell[i * n_x + 0] = 1e-40;
                  if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                     y_cp_shell[i * n_x + 0] = 1e-40;
               }
            } else if (y_sm[i * n_x + k - 1] > 0.) {
               if (fabs(y_sm[i * n_x + k] - y_sm[i * n_x + k - 1]) / y_sm[i * n_x + k - 1] > gSIM_EPS)
                  y_sm_shell[i * n_x + k] = (y_sm[i * n_x + k] - y_sm[i * n_x + k - 1]) / delta_area;
               if (is_subs[i]) {
                  if (fabs(y_cl[i * n_x + k] - y_cl[i * n_x + k - 1]) / y_cl[i * n_x + k - 1] > gSIM_EPS)
                     y_cl_shell[i * n_x + k] = (y_cl[i * n_x + k] - y_cl[i * n_x + k - 1]) / delta_area;

                  if (fabs(y_tot[i * n_x + k] - y_tot[i * n_x + k - 1]) / y_tot[i * n_x + k - 1] > gSIM_EPS)
                     y_tot_shell[i * n_x + k] = (y_tot[i * n_x + k] - y_tot[i * n_x + k - 1]) / delta_area;
                  if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
                     if (fabs(y_cp[i * n_x + k] - y_cp[i * n_x + k - 1]) / y_cp[i * n_x + k - 1] > gSIM_EPS)
                        y_cp_shell[i * n_x + k] = (y_cp[i * n_x + k] - y_cp[i * n_x + k - 1]) / delta_area;
                  }

               }
            }
            if (y_sm_shell[i * n_x + k] < 1e-33) y_sm_shell[i * n_x + k] = 1e-40;
            else {
               x_deg_sm_shell_plot[i].push_back(x[k] * RAD_to_DEG);
               y_sm_shell_plot[i].push_back(y_sm_shell[i * n_x + k]);
            }

            if (y_cp_shell[i * n_x + k] < 1e-33)  y_cp_shell[i * n_x + k] = 1e-40;
            else {
               x_deg_cp_shell_plot[i].push_back(x[k] * RAD_to_DEG);
               y_cp_shell_plot[i].push_back(y_cp_shell[i * n_x + k]);
            }
            if (y_cl_shell[i * n_x + k] < 1e-33)  y_cl_shell[i * n_x + k] = 1e-40;
            else {
               x_deg_cl_shell_plot[i].push_back(x[k] * RAD_to_DEG);
               y_cl_shell_plot[i].push_back(y_cl_shell[i * n_x + k]);
            }
            if (y_tot_shell[i * n_x + k] < 1e-33)  y_tot_shell[i * n_x + k] = 1e-40;
            else {
               x_deg_tot_shell_plot[i].push_back(x[k] * RAD_to_DEG);
               y_tot_shell_plot[i].push_back(y_tot_shell[i * n_x + k]);
            }

            y_sm_shell_norm[i * n_x + k] = y_sm_shell[i * n_x + k] / y_sm_shell[i * n_x + 1];
            if (is_subs[i]) {
               y_cl_shell_norm[i * n_x + k] = y_cl_shell[i * n_x + k] / y_cl_shell[i * n_x + 1];
               y_tot_shell_norm[i * n_x + k] = y_tot_shell[i * n_x + k] / y_tot_shell[i * n_x + 1];
               if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                  y_cp_shell_norm[i * n_x + k] = y_cp_shell[i * n_x + k] / y_cp_shell[i * n_x + 1];
            }
            if (y_sm_shell_norm[i * n_x + k] < 1e-40) y_sm_shell_norm[i * n_x + k] = 1e-40;
            if (y_cp_shell_norm[i * n_x + k] < 1e-40) y_cp_shell_norm[i * n_x + k] = 1e-40;
            if (y_tot_shell_norm[i * n_x + k] < 1e-40) y_tot_shell_norm[i * n_x + k] = 1e-40;
            if (y_cl_shell_norm[i * n_x + k] < 1e-40) y_cl_shell_norm[i * n_x + k] = 1e-40;

            convert_to_PP_units(1, y_sm_shell[i * n_x + k]);
            convert_to_PP_units(1, y_cl_shell[i * n_x + k]);
            convert_to_PP_units(1, y_cp_shell[i * n_x + k]);
            convert_to_PP_units(1, y_tot_shell[i * n_x + k]);

            // Calculate flux (gamma, and neutrino if available)
            if (is_flux) {
               bool unit_ref = gSIM_IS_ASTRO_OR_PP_UNITS;
               par_spec[2] = kGAMMA;
               par_spec[3] = list_halos[i].z;
               if (!gSIM_FLUX_IS_INTEG_OR_DIFF) flux_gamma[i][k] = flux(par_spec, gSIM_FLUX_AT_E_GEV, y_tot[i * n_x + k], false);
               else flux_gamma[i][k] = flux(par_spec, -999, y_tot[i * n_x + k], true);

               par_spec[2] = kNEUTRINO;
               if (gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_EW || gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_NOEW) {
                  if (!gSIM_FLUX_IS_INTEG_OR_DIFF) flux_nu[i][k] = flux(par_spec, gSIM_FLUX_AT_E_GEV, y_tot[i * n_x + k], false);
                  else  flux_nu[i][k] = flux(par_spec, -999, y_tot[i * n_x + k], true);
               }

               // Watch out: if user-unit is ASTRO, we need to convert here (flux requires PP unit)
               if (gSIM_IS_ASTRO_OR_PP_UNITS) {
                  gSIM_IS_ASTRO_OR_PP_UNITS = false;
                  convert_to_PP_units(1, flux_gamma[i][k]);
                  convert_to_PP_units(1, flux_nu[i][k]);
               }
               gSIM_IS_ASTRO_OR_PP_UNITS = unit_ref;
            }

            // Print on screen and in file)
            if (gSIM_IS_PRINT) {
               for (int ff = 0; ff < ff_max; ++ff) {
                  fp_current = fp[ff];
                  if (is_option) {
                     if (i == 0 && k == 0 && (ff == 0 or ff == 2)) {
                        fprintf(fp_current, "#       NAME     IND_MCXC    l        b        d      alpha_s        %csm         %csub         %ctot \n", j_or_d, j_or_d, j_or_d);
                        fprintf(fp_current, "#         -         -      [deg]    [deg]    [kpc]     [deg]           %s\n", y_name.c_str());
                     }
                     if (k == 0)
                        //fprintf(fp_current, "%-16s  %4d     %6.2f  %6.2f  %6.2le  %.2le   %.2le\n",
                        fprintf(fp_current, "%-16s  %4d     %6.2f  %6.2f  %6.2le  %.*le   %.*le    %.*le     %.*le\n",
                                halo_get_name(i, list_halos).c_str(), i + 1, list_halos[i].PsiDeg, list_halos[i].ThetaDeg,
                                list_halos[i].l,  gSIM_SIGDIGITS, asin(list_halos[i].Rscale / list_halos[i].l) * RAD_to_DEG,
                                gSIM_SIGDIGITS, y_sm[i * n_x + k],  gSIM_SIGDIGITS, y_cl[i * n_x + k],  gSIM_SIGDIGITS, y_tot[i * n_x + k]);
                     //y_sm[i * n_x + k]);
                  }
               }
               for (int ff = 0; ff < 2; ++ff) {
                  fp_current = fp[ff];
                  if (!is_option) {
                     if (!is_subs[i]) {
                        if (is_shell)
                           fprintf(fp_current, "  %.*le   %.*le   %.*le   %.*le   %*le\n",
                                   gSIM_SIGDIGITS + 2, x_deg[k],  gSIM_SIGDIGITS, y_sm[i * n_x + k],  gSIM_SIGDIGITS, y_sm_norm[i * n_x + k],
                                   gSIM_SIGDIGITS, y_sm_shell[i * n_x + k],  gSIM_SIGDIGITS, y_sm_shell_norm[i * n_x + k]);
                        else
                           fprintf(fp_current, "  %.*le   %.*le   %.*le\n",
                                   gSIM_SIGDIGITS + 2, x_deg[k],  gSIM_SIGDIGITS, y_sm[i * n_x + k],  gSIM_SIGDIGITS, y_sm_norm[i * n_x + k]);
                     } else {
                        if (switch_y != 0 && gPP_DM_IS_ANNIHIL_OR_DECAY) {
                           if (is_shell)
                              fprintf(fp_current, "  %.*le     %.*le %.*le %.*le %.*le    %.*le %.*le %.*le %.*le    %.*le %.*le %.*le %.*le    %.*le %.*le %.*le %.*le   %.*le\n",
                                      gSIM_SIGDIGITS + 2, x_deg[k],  gSIM_SIGDIGITS, y_sm[i * n_x + k],  gSIM_SIGDIGITS, y_cl[i * n_x + k],
                                      gSIM_SIGDIGITS, y_cp[i * n_x + k], gSIM_SIGDIGITS, y_tot[i * n_x + k],
                                      gSIM_SIGDIGITS, y_sm_norm[i * n_x + k],  gSIM_SIGDIGITS, y_cl_norm[i * n_x + k],
                                      gSIM_SIGDIGITS, y_cp_norm[i * n_x + k],  gSIM_SIGDIGITS, y_tot_norm[i * n_x + k],
                                      gSIM_SIGDIGITS, y_sm_shell[i * n_x + k],  gSIM_SIGDIGITS, y_cl_shell[i * n_x + k],
                                      gSIM_SIGDIGITS, y_cp_shell[i * n_x + k],  gSIM_SIGDIGITS, y_tot_shell[i * n_x + k],
                                      gSIM_SIGDIGITS, y_sm_shell_norm[i * n_x + k],  gSIM_SIGDIGITS, y_cl_shell_norm[i * n_x + k],
                                      gSIM_SIGDIGITS, y_cp_shell_norm[i * n_x + k],  gSIM_SIGDIGITS, y_tot_shell_norm[i * n_x + k],
                                      gSIM_SIGDIGITS, y_boost[i * n_x + k]);
                           else
                              fprintf(fp_current, "  %.*le     %.*le %.*le %.*le %.*le    %.*le %.*le %.*le %.*le    %.*le\n",
                                      gSIM_SIGDIGITS + 2, x_deg[k],  gSIM_SIGDIGITS, y_sm[i * n_x + k],  gSIM_SIGDIGITS, y_cl[i * n_x + k],
                                      gSIM_SIGDIGITS, y_cp[i * n_x + k], gSIM_SIGDIGITS, y_tot[i * n_x + k],
                                      gSIM_SIGDIGITS, y_sm_norm[i * n_x + k],  gSIM_SIGDIGITS, y_cl_norm[i * n_x + k],
                                      gSIM_SIGDIGITS, y_cp_norm[i * n_x + k],  gSIM_SIGDIGITS, y_tot_norm[i * n_x + k],
                                      gSIM_SIGDIGITS, y_boost[i * n_x + k]);
                        } else {
                           if (is_shell)
                              fprintf(fp_current, "  %.*le     %.*le %.*le %.*le   %.*le %.*le %.*le    %.*le %.*le %.*le   %.*le %.*le %.*le   %.*le\n",
                                      gSIM_SIGDIGITS + 2, x_deg[k],  gSIM_SIGDIGITS, y_sm[i * n_x + k],  gSIM_SIGDIGITS, y_cl[i * n_x + k],
                                      gSIM_SIGDIGITS, y_tot[i * n_x + k], gSIM_SIGDIGITS, y_sm_norm[i * n_x + k],
                                      gSIM_SIGDIGITS, y_cl_norm[i * n_x + k],  gSIM_SIGDIGITS, y_tot_norm[i * n_x + k],
                                      gSIM_SIGDIGITS, y_sm_shell[i * n_x + k],  gSIM_SIGDIGITS, y_cl_shell[i * n_x + k],
                                      gSIM_SIGDIGITS, y_tot_shell[i * n_x + k],  gSIM_SIGDIGITS, y_sm_shell_norm[i * n_x + k],
                                      gSIM_SIGDIGITS, y_cl_shell_norm[i * n_x + k],  gSIM_SIGDIGITS, y_tot_shell_norm[i * n_x + k],
                                      gSIM_SIGDIGITS, y_boost[i * n_x + k]);
                           else
                              fprintf(fp_current, "  %.*le     %.*le %.*le %.*le   %.*le %.*le %.*le    %.*le\n",
                                      gSIM_SIGDIGITS + 2, x_deg[k],  gSIM_SIGDIGITS, y_sm[i * n_x + k],  gSIM_SIGDIGITS, y_cl[i * n_x + k],
                                      gSIM_SIGDIGITS, y_tot[i * n_x + k], gSIM_SIGDIGITS, y_sm_norm[i * n_x + k],
                                      gSIM_SIGDIGITS, y_cl_norm[i * n_x + k],  gSIM_SIGDIGITS, y_tot_norm[i * n_x + k],
                                      gSIM_SIGDIGITS, y_boost[i * n_x + k]);
                        }
                     }
                  }
               }
               if (!is_option and is_flux) fprintf(fp[2], "  %.*le   %.*le   %.*le\n", gSIM_SIGDIGITS + 2, x_deg[k], gSIM_SIGDIGITS, flux_gamma[i][k], gSIM_SIGDIGITS, flux_nu[i][k]);
            }
         }

         // 2. J(theta)
      } else if (switch_y == 2) {

         double alpha_int_orig = gSIM_ALPHAINT; // no redshift correction

         double alpha_int_shell = 1e-6;
         double int_area = 2. * PI * (1 - cos(alpha_int_shell)) ;

         // update alpha_int according to redshift of object to account for angular diameter distortion
         if (list_halos[i].z > ZMIN_EXTRAGAL) gSIM_ALPHAINT = atan(1. / (1 + list_halos[i].z) * MY_TAN(gSIM_ALPHAINT));

         double alpha_int = gSIM_ALPHAINT;

         double y_sm_max = 1.;
         double y_cl_max = 1.;
         double y_cp_max = 1.;
         double y_tot_max = 1.;
         for (int k = 0; k < n_x; ++k) {
            // Update value of direction where the instrument points: Rescale the l.o.s
            // to account for angular diameter distortion
            if (list_halos[i].z > ZMIN_EXTRAGAL) psi_los = atan(1. / (1 + list_halos[i].z) * MY_TAN(x[k]));
            else psi_los = x[k];

            x_deg.push_back(x[k] * RAD_to_DEG);

            // smooth contrib
            if (is_subs[i]) {
               y_sm[i * n_x + k] = jsmooth_mix(list_halos[i].Mtot, par_tot, psi_los, theta_los, gSIM_EPS, list_halos[i].Subs_mfrac, par_dpdv);

               gSIM_ALPHAINT = alpha_int_shell;
               y_sm_shell[i * n_x + k] = jsmooth_mix(list_halos[i].Mtot, par_tot, psi_los, theta_los, gSIM_EPS, list_halos[i].Subs_mfrac, par_dpdv) / int_area;
               gSIM_ALPHAINT = alpha_int;

            } else {
               y_sm[i * n_x + k] = jsmooth(par_tot, psi_los, theta_los, gSIM_EPS);

               gSIM_ALPHAINT = alpha_int_shell;
               y_sm_shell[i * n_x + k] = jsmooth(par_tot, psi_los, theta_los, gSIM_EPS) / int_area;
               gSIM_ALPHAINT = alpha_int;

            }

            if (k > 0) y_sm_norm[i * n_x + k] = y_sm[i * n_x + k] / y_sm_max;
            else {
               y_sm_norm[i * n_x + 0] = 1.;
               y_sm_max = y_sm[i * n_x + 0];
            }

            // subs contrib
            if (is_subs[i]) {
               y_cl[i * n_x + k] = jsub_continuum(ntot_subs, par_dpdv, psi_los, theta_los, lmin, lmax, par_subs, mmin_subs, mmax_subs);

               gSIM_ALPHAINT = alpha_int_shell;
               y_cl_shell[i * n_x + k] = jsub_continuum(ntot_subs, par_dpdv, psi_los, theta_los, lmin, lmax, par_subs, mmin_subs, mmax_subs) / int_area;
               gSIM_ALPHAINT = alpha_int;

               y_tot[i * n_x + k] = y_sm[i * n_x + k] + y_cl[i * n_x + k];
               y_tot_shell[i * n_x + k] = y_sm_shell[i * n_x + k] + y_cl_shell[i * n_x + k];
               if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
                  y_cp[i * n_x + k] = jcrossprod_continuum(list_halos[i].Mtot, par_tot, psi_los, theta_los, lmin, lmax, gSIM_EPS, list_halos[i].Subs_mfrac, par_dpdv);

                  gSIM_ALPHAINT = alpha_int_shell;
                  y_cp_shell[i * n_x + k] = jcrossprod_continuum(list_halos[i].Mtot, par_tot, psi_los, theta_los, lmin, lmax, gSIM_EPS, list_halos[i].Subs_mfrac, par_dpdv) / int_area;
                  gSIM_ALPHAINT = alpha_int;

                  y_tot[i * n_x + k] += y_cp[i * n_x + k];
                  y_tot_shell[i * n_x + k] += y_cp_shell[i * n_x + k];
               }
               if (k > 0) {
                  y_cl_norm[i * n_x + k] = y_cl[i * n_x + k] / y_cl_max;
                  y_tot_norm[i * n_x + k] = y_tot[i * n_x + k] / y_tot_max;
                  if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                     y_cp_norm[i * n_x + k] = y_cp[i * n_x + k] / y_cp_max;
               } else {
                  y_cl_norm[i * n_x + 0] = 1.;
                  y_tot_norm[i * n_x + 0] = 1.;
                  y_cl_max = y_cl[i * n_x + 0];
                  y_tot_max = y_tot[i * n_x + 0];
                  if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
                     y_cp_norm[i * n_x + 0] = 1.;
                     y_cp_max = y_cp[i * n_x + 0];
                  }
               }
               if (y_sm[i * n_x + k] > 0)
                  y_boost[i * n_x + k] = y_tot[i * n_x + k] / jsmooth(par_tot, psi_los, theta_los, gSIM_EPS);
               else
                  y_boost[i * n_x + k] = y_boost[i * n_x + k - 1];
            } else
               y_tot_shell[i * n_x + k] = y_sm_shell[i * n_x + k];

            // Convert before printing
            convert_to_PP_units(1, y_sm[i * n_x + k]);
            convert_to_PP_units(1, y_cl[i * n_x + k]);
            convert_to_PP_units(1, y_cp[i * n_x + k]);
            convert_to_PP_units(1, y_tot[i * n_x + k]);
            convert_to_PP_units(1, y_sm_shell[i * n_x + k]);
            convert_to_PP_units(1, y_cl_shell[i * n_x + k]);
            convert_to_PP_units(1, y_cp_shell[i * n_x + k]);
            convert_to_PP_units(1, y_tot_shell[i * n_x + k]);

            x_deg_sm_shell_plot[i].push_back(x[k] * RAD_to_DEG);
            y_sm_shell_plot[i].push_back(y_sm_shell[i * n_x + k]);
            x_deg_cl_shell_plot[i].push_back(x[k] * RAD_to_DEG);
            y_cl_shell_plot[i].push_back(y_cl_shell[i * n_x + k]);
            x_deg_cp_shell_plot[i].push_back(x[k] * RAD_to_DEG);
            y_cp_shell_plot[i].push_back(y_cp_shell[i * n_x + k]);
            x_deg_tot_shell_plot[i].push_back(x[k] * RAD_to_DEG);
            y_tot_shell_plot[i].push_back(y_tot_shell[i * n_x + k]);

            // Calculate flux (gamma, and neutrino if available)
            if (is_flux) {
               bool unit_ref = gSIM_IS_ASTRO_OR_PP_UNITS;
               par_spec[2] = kGAMMA;
               if (!gSIM_FLUX_IS_INTEG_OR_DIFF)  intensity_gamma[i][k] = flux(par_spec, gSIM_FLUX_AT_E_GEV, y_tot_shell[i * n_x + k], false);
               else intensity_gamma[i][k] = flux(par_spec, -999, y_tot_shell[i * n_x + k], true);

               par_spec[2] = kNEUTRINO;
               if (gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_EW || gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_NOEW) {
                  if (!gSIM_FLUX_IS_INTEG_OR_DIFF) intensity_nu[i][k] = flux(par_spec, gSIM_FLUX_AT_E_GEV, y_tot_shell[i * n_x + k], false);
                  else intensity_nu[i][k] = flux(par_spec, -999, y_tot_shell[i * n_x + k], true);
               }

               // Watch out: if user-unit is ASTRO, we need to convert here (flux requires PP unit)
               if (gSIM_IS_ASTRO_OR_PP_UNITS) {
                  gSIM_IS_ASTRO_OR_PP_UNITS = false;
                  convert_to_PP_units(1, intensity_gamma[i][k]);
                  convert_to_PP_units(1, intensity_nu[i][k]);
               }
               gSIM_IS_ASTRO_OR_PP_UNITS = unit_ref;
            }

            // Print -on screen and in file)
            if (gSIM_IS_PRINT) {
               for (int ff = 0; ff < 2; ++ff) {
                  fp_current = fp[ff];
                  if (!is_subs[i])
                     fprintf(fp_current, "  %.*le   %.*le   %.*le  %.*le\n",
                             gSIM_SIGDIGITS + 2, x_deg[k],  gSIM_SIGDIGITS, y_sm[i * n_x + k],
                             gSIM_SIGDIGITS, y_sm_norm[i * n_x + k], gSIM_SIGDIGITS, y_sm_shell[i * n_x + k]);
                  else {
                     if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                        fprintf(fp_current, "  %.*le     %.*le %.*le %.*le %.*le    %.*le %.*le %.*le %.*le    %.*le %.*le %.*le %.*le    %.*le\n",
                                gSIM_SIGDIGITS + 2, x_deg[k],  gSIM_SIGDIGITS, y_sm[i * n_x + k],  gSIM_SIGDIGITS, y_cl[i * n_x + k],
                                gSIM_SIGDIGITS, y_cp[i * n_x + k], gSIM_SIGDIGITS, y_tot[i * n_x + k],
                                gSIM_SIGDIGITS, y_sm_norm[i * n_x + k],  gSIM_SIGDIGITS, y_cl_norm[i * n_x + k],
                                gSIM_SIGDIGITS, y_cp_norm[i * n_x + k],  gSIM_SIGDIGITS, y_tot_norm[i * n_x + k],
                                gSIM_SIGDIGITS, y_sm_shell[i * n_x + k],  gSIM_SIGDIGITS, y_cl_shell[i * n_x + k],
                                gSIM_SIGDIGITS, y_cp_shell[i * n_x + k],  gSIM_SIGDIGITS, y_tot_shell[i * n_x + k],
                                gSIM_SIGDIGITS, y_boost[i * n_x + k]);
                     else
                        fprintf(fp_current, "  %.*le     %.*le %.*le %.*le   %.*le %.*le %.*le    %.*le %.*le %.*le   %.*le\n",
                                gSIM_SIGDIGITS + 2, x_deg[k],  gSIM_SIGDIGITS, y_sm[i * n_x + k],  gSIM_SIGDIGITS, y_cl[i * n_x + k],
                                gSIM_SIGDIGITS, y_tot[i * n_x + k], gSIM_SIGDIGITS, y_sm_norm[i * n_x + k],
                                gSIM_SIGDIGITS, y_cl_norm[i * n_x + k],  gSIM_SIGDIGITS, y_tot_norm[i * n_x + k],
                                gSIM_SIGDIGITS, y_sm_shell[i * n_x + k],  gSIM_SIGDIGITS, y_cl_shell[i * n_x + k],
                                gSIM_SIGDIGITS, y_tot_shell[i * n_x + k],
                                gSIM_SIGDIGITS, y_boost[i * n_x + k]);
                  }
               }
               if (is_flux) fprintf(fp[2], "  %.*le   %.*le   %.*le\n", gSIM_SIGDIGITS + 2, x_deg[k], gSIM_SIGDIGITS, intensity_gamma[i][k], gSIM_SIGDIGITS, intensity_nu[i][k]);
            }
         }
         gSIM_ALPHAINT = alpha_int_orig;

         // 3. M(r)
      } else if (switch_y == 3) {
         // do calculation in comoving coordinates:
         par_tot[0] /= pow(1 + list_halos[i].z, 3.);
         par_tot[1] *= (1 + list_halos[i].z);
         par_tot[6] *= (1 + list_halos[i].z);

         double rvir = par_tot[6];
         // If x>Rvir, stop integration at Rvir nonetheless
         par_tot[6] = min(x[n_x - 1], rvir);
         double y_tot_max = mass_singlehalo(par_tot, gSIM_EPS);
         for (int k = 0; k < n_x; ++k) {
            // If x>Rvir, stop integration at Rvir nonetheless
            par_tot[6] = min(x[k], rvir);
            y_tot[i * n_x + k] = mass_singlehalo(par_tot, gSIM_EPS);
            y_tot_norm[i * n_x + k] = y_tot[i * n_x + k] / y_tot_max;

            // Print on screen and in file)
            if (gSIM_IS_PRINT) {
               for (int ff = 0; ff < 2; ++ff) {
                  fp_current = fp[ff];
                  fprintf(fp_current, "  %.*le   %.*le   %.*le\n",
                          gSIM_SIGDIGITS + 2, x[k],  gSIM_SIGDIGITS, y_tot[i * n_x + k], gSIM_SIGDIGITS, y_tot_norm[i * n_x + k]);
                  fp_current = NULL;
               }
            }
         }
      }
   }

   if (gSIM_IS_PRINT) {
      fclose(fp[0]);
      cout << endl;
      cout << "_______________________" << endl << endl;
      cout << " ... output [ASCII] written in: " << f_name << " (#" << n_obj << " objects)" << endl;
      if (is_flux) {
         fclose(fp[2]);
         cout << " ... output [ASCII] written in: " << f_name_flux << endl;
      }
   }

#if IS_ROOT
   // Displays or .root save
   string f_root = gSIM_OUTPUT_DIR + tmp_wdir[tmp_wdir.size() - 1] + "." + cvs_name + ".root";
   TFile *root_file = NULL;
   if (gSIM_IS_WRITE_ROOTFILES)
      root_file = new TFile(f_root.c_str(), "recreate");

   if (gSIM_IS_DISPLAY || gSIM_IS_WRITE_ROOTFILES) {
      // TGraph related
      int gr_line_width = 3;
      vector<string> gr_opt;
      vector<string> gr_shell_opt;
      vector<int> gr_color;
      vector<int> gr_style;
      int gr_sm_style = 2;
      int gr_cl_style = 3;
      int gr_cp_style = 6;
      int gr_tot_style = 1;
      for (int i = 0; i < n_obj; ++i) {
         gr_opt.push_back("L");
         gr_shell_opt.push_back("L");
         gr_color.push_back(rootcolor(i));
         gr_style.push_back(i + 1);
      }
      bool is_logx = gSIM_IS_XLOG;
      bool is_logy = true;

      // determine y-axis plotting range:
      double ymin = 1e40;
      double ymax = 1e-40;
      double ymin_shell = 1e40;
      double ymax_shell = 1e-40;
      double ymin_flux = 1e40;
      double ymax_flux = 1e-40;
      for (int i = 0; i < n_obj; ++i) {
         for (int k = 0; k < n_x; ++k) {
            if (y_sm[i * n_x + k] > ymax)  ymax = y_sm[i * n_x + k];
            if (y_cl[i * n_x + k] > ymax)  ymax = y_cl[i * n_x + k];
            if (y_cp[i * n_x + k] > ymax)  ymax = y_cp[i * n_x + k];
            if (y_tot[i * n_x + k] > ymax) ymax = y_tot[i * n_x + k];

            if (y_tot_shell[i * n_x + k] > ymax_shell) ymax_shell = y_tot_shell[i * n_x + k];

            if (switch_y == 1 and is_flux) {
               if (flux_gamma[i][k] > ymax_flux) ymax_flux = flux_gamma[i][k];
               if (flux_nu[i][k]    > ymax_flux) ymax_flux = flux_nu[i][k];
            } else if (switch_y == 2 and is_flux) {
               if (intensity_gamma[i][k] > ymax_flux) ymax_flux = intensity_gamma[i][k];
               if (intensity_nu[i][k]    > ymax_flux) ymax_flux = intensity_nu[i][k];
            }

            if (y_sm[i * n_x + k]  < ymin and y_sm[i * n_x + k] > 1e-40) ymin = y_sm[i * n_x + k];
            if (y_cl[i * n_x + k]  < ymin and y_cl[i * n_x + k] > 1e-40) ymin = y_cl[i * n_x + k];
            if (y_cp[i * n_x + k]  < ymin and y_cp[i * n_x + k] > 1e-40) ymin = y_cp[i * n_x + k];
            if (y_tot[i * n_x + k] < ymin and y_tot[i * n_x + k] > 1e-40) ymin = y_tot[i * n_x + k];

            if (y_tot_shell[i * n_x + k] < ymin_shell and y_tot_shell[i * n_x + k] > 1e-40) ymin_shell = y_tot_shell[i * n_x + k];

            if (switch_y == 1 and is_flux) {
               if (flux_gamma[i][k] < ymin_flux and flux_gamma[i][k] > 1e-40) ymin_flux = flux_gamma[i][k];
               if (flux_nu[i][k]    < ymin_flux and flux_nu[i][k] > 1e-40)    ymin_flux = flux_nu[i][k];
            } else if (switch_y == 2 and is_flux) {
               if (intensity_gamma[i][k] < ymin_flux and intensity_gamma[i][k] > 1e-40) ymin_flux = intensity_gamma[i][k];
               if (intensity_nu[i][k]    < ymin_flux and intensity_nu[i][k] > 1e-40)    ymin_flux = intensity_nu[i][k];
            }
         }
      }

      double ymin_norm = ymin / ymax;
      if (ymin <  1e-9 * ymax) {
         ymin =  1e-9 * ymax;
         ymin_norm = 2e-8;
      }

      if (ymin_shell <  1e-9 * ymax_shell) {
         ymin_shell =  1e-9 * ymax_shell;
      }

      if (is_flux and ymin_flux <  1e-9 * ymax_flux) {
         ymin_flux =  2e-8 * ymax_flux;
      }

      // Tlegend related
      double leg_xy[4] = {0., 0., 0., 0.};
      string leg_header = "Displayed halos:";
      // TText related
      vector<string> text;
      bool is_text = false;

      // Set graph properties depending on switch_y
      //  1. rho(r)
      if (switch_y == 0) {
         leg_xy[0] = 0.65;
         leg_xy[1] = 0.58;
         leg_xy[2] = 0.95;
         leg_xy[3] = 0.92;

         //  2. J(alpha_int) or 3. M(r)
      } else if (switch_y == 1) {
         leg_xy[0] = 0.62;
         leg_xy[1] = 0.13;
         leg_xy[2] = 0.95;
         leg_xy[3] = 0.62;
         if (x[0] <= 1.e-40) is_logy = false;

         //  3. J(theta)
      } else if (switch_y == 2) {
         leg_xy[0] = 0.68;
         leg_xy[1] = 0.6;
         leg_xy[2] = 0.95;
         leg_xy[3] = 0.95;
         char title[200];
         sprintf(title, "#alpha_{int} =%g^{o}", gSIM_ALPHAINT * RAD_to_DEG);
         text.push_back(title);
         is_text = true;

         //  4. M(r)
      } else if (switch_y == 3) {
         leg_xy[0] = 0.42;
         leg_xy[1] = 0.18;
         leg_xy[2] = 0.95;
         leg_xy[3] = 0.62;
         if (x[0] <= 1.e-40) is_logy = false;
      }

      // If subclumps
      if (is_subs_all && switch_y != 3) {
         is_text = true;

         char mmin_mmax[500];
         sprintf(mmin_mmax, "M_{sub}#in [%g M_{#odot} - %g M_{tot}]", gDM_SUBS_MMIN, gDM_SUBS_MMAXFRAC);
         text.push_back(mmin_mmax);

         // Loop on all types
         for (int i = 0; i < (int)list_type.size(); ++i) {
            // Find matching type for halo
            int i_type = -1;
            for (int ii = 0; ii < gN_TYPEHALOES; ++ii) {
               if (list_type[i] == gNAMES_TYPEHALOES[ii]) {
                  i_type = ii;
                  break;
               }
            }
            if (i_type < 0) {
               printf("\n====> ERROR: halo_j1D() in janalysis.cc");
               printf("\n             Halo type %s is not a type enabled in CLUMPY", list_type[i].c_str());
               printf("\n             => abort()\n\n");
               abort();
            }

            text.push_back((string)gNAMES_TYPEHALOES[i_type] + " config.");
            halo_set_parsubs(i, par_subs, list_halos);

            char tmp[500];
            sprintf(tmp, "f=%g", gHALO_SUBS_MASSFRACTION[i_type]);
            text.push_back(tmp);

            if (gHALO_SUBS_DPDV_FLAG_PROFILE[i_type] == -1)
               sprintf(tmp, "  - dP/dV profile #propto total");
            else
               sprintf(tmp, "  - dP/dV #propto %s", (legend_for_profile(&par_subs[18])).c_str());
            text.push_back(tmp);

            if (switch_y != 0) {
               sprintf(tmp, "  - dP/dM #propto M^{-%g}", gHALO_SUBS_DPDM_SLOPE[i_type]);
               text.push_back(tmp);

               sprintf(tmp, "  - #rho_{cl} = %s", (legend_for_profile(&par_subs[0])).c_str());
               text.push_back(tmp);

               sprintf(tmp, "   + c_{#Delta}(M_{#Delta}) = %s", gNAMES_CDELTAMDELTA[gHALO_SUBS_FLAG_CDELTAMDELTA[i_type]]);
               text.push_back(tmp);
            }
         }
      }

      if (gSIM_IS_WRITE_ROOTFILES)
         root_file->cd();
      int n_cvs_tot = 4;
      int n_cvs;
      if (!is_subs_all  && switch_y == 1  && is_shell) n_cvs = 3;
      else if (!is_subs_all  && switch_y == 1 && !is_shell) n_cvs = 2;
      else if (is_subs_all && switch_y == 1 && is_shell) n_cvs = 4;
      else if (is_subs_all && switch_y == 1 && !is_shell) n_cvs = 3;
      else if (switch_y == 2) n_cvs = 3;
      else n_cvs = 2;

      TMultiGraph **multi = new TMultiGraph*[n_cvs_tot];
      TLegend **leg = new TLegend*[n_cvs_tot];
      TCanvas **c_list = new TCanvas*[n_cvs_tot];
      TGraph **gr_sm =  new TGraph*[2 * n_obj];
      TGraph **gr_cl = NULL;
      TGraph **gr_cp = NULL;
      TGraph **gr_tot = NULL;
      TGraph **gr_sm_shell = NULL;
      TGraph **gr_cl_shell = NULL;
      TGraph **gr_cp_shell = NULL;
      TGraph **gr_tot_shell = NULL;
      TGraph **gr_boost = NULL;
      TH1D **histo_boost = NULL;
      TLegend *leg_boost = NULL;
      TCanvas *c_boost = NULL;

      if (switch_y == 1 or switch_y == 2)
         gr_sm_shell =  new TGraph*[2 * n_obj];
      if (switch_y == 3)
         gr_tot =  new TGraph*[2 * n_obj];
      else if (is_subs_all) {
         gr_cl =  new TGraph*[2 * n_obj];
         gr_tot =  new TGraph*[2 * n_obj];
         if (switch_y != 0 && gPP_DM_IS_ANNIHIL_OR_DECAY)
            gr_cp =  new TGraph*[2 * n_obj];
         if (switch_y == 1 or switch_y == 2) {
            gr_cl_shell =  new TGraph*[2 * n_obj];
            gr_tot_shell =  new TGraph*[2 * n_obj];
            if ((switch_y == 1 or switch_y == 2) && gPP_DM_IS_ANNIHIL_OR_DECAY)
               gr_cp_shell =  new TGraph*[2 * n_obj];
         }
         if (switch_y != 0)
            gr_boost = new TGraph*[n_obj];
      }

      if (switch_y == 1) {  // if J(alpha_int)
         if (switch_x_axis == 1)
            x_name = "#alpha_{int}#times d  [deg kpc]";
         else if (switch_x_axis == 2)
            x_name = "#alpha_{int}/#alpha_{s}";
      } else if (switch_y == 2) {  // if J(theta)
         if (switch_x_axis == 1)
            x_name = "#theta#times d  [deg kpc]";
         else if (switch_x_axis == 2)
            x_name = "#theta/#alpha_{s}";
      }
      string x_name_orig = x_name;
      string x_name_diff = "Distance from halo center #theta [deg]";
      vector<double> xd;
      for (int k = 0; k < n_x; ++k) {
         if (switch_y == 1 || switch_y == 2) xd.push_back(x_deg[k]);
         else xd.push_back(x[k]);
      }

      for (int cc = 0; cc < n_cvs; ++cc) {
         // Fill graphs
         int c;
         if (!is_subs_all && switch_y == 1 && cc == 2 && is_shell) c = 3;
         else if (switch_y == 2 && cc == 2) c = 3;
         else c = cc;

         if (c == 2 and !gPP_DM_IS_ANNIHIL_OR_DECAY) continue; // no boost plot.

         if (switch_y == 1 and c == 3) x_name = x_name_diff;
         else x_name = x_name_orig;

         multi[c] = new TMultiGraph();
         if (c == 2) {
            leg_xy[0] = 0.52;
            leg_xy[1] = 0.13;
            leg_xy[2] = 0.95;
            leg_xy[3] = 0.6;
         } else if (c == 3) {
            leg_xy[0] = 0.15;
            leg_xy[1] = 0.13;
            leg_xy[2] = 0.95;
            leg_xy[3] = 0.6;
         };
         leg[c] = new TLegend(leg_xy[0], leg_xy[1], leg_xy[2], leg_xy[3]);
         leg[c]->SetFillColor(kWhite);
         leg[c]->SetTextSize(0.03);
         leg[c]->SetBorderSize(0);
         leg[c]->SetHeader(leg_header.c_str());
         leg[c]->SetFillStyle(0);

         // y_name for graphs
         string cvs_y_name = y_name;
         if (c == 1)
            cvs_y_name = y_name_norm;
         else if (c == 2) {
            string boost = "[D_{sm} + <D_{subs}>] / D_{tot (no-subs)}";
            if (gPP_DM_IS_ANNIHIL_OR_DECAY)
               boost = "[J_{sm} + <J_{subs}> + J_{crossprod}] / J_{tot (no-subs)}";
            if (switch_y == 1)
               cvs_y_name = "Boost(#alpha_{int}) #equiv " + boost;
            else if (switch_y == 2)
               cvs_y_name = "Boost(#theta) #equiv " + boost;
         } else if (c == 3)  {
            cvs_y_name = y_name_shell;
         }

         for (int i = 0; i < n_obj; ++i) {
            // in case we want to plot the rescaled x_axis [J(alpha_int/xxx) or J(theta/xxx)]
            if (switch_y == 1 || switch_y == 2) {
               if (switch_x_axis == 1) {
                  xd.clear();
                  for (int k = 0; k < n_x; ++k)
                     xd.push_back(x[k]*list_halos[i].l);
               } else if (switch_x_axis == 2) {
                  xd.clear();
                  for (int k = 0; k < n_x; ++k) {
                     double alpha_s = asin(list_halos[i].Rscale / list_halos[i].l) * RAD_to_DEG;
                     xd.push_back(x[k] / alpha_s);
                  }
               }
            }

            // Fill and set graph properties!
            if (c == 0) {
               if (switch_y == 3) {
                  gr_tot[c * n_obj + i] = new TGraph(n_x, &xd[0], &y_tot[i * n_x + 0]);
                  gr_tot[c * n_obj + i]->SetName(names_tot[i].c_str());
                  gr_tot[c * n_obj + i]->SetTitle(names_tot[i].c_str());
                  gr_tot[c * n_obj + i]->GetXaxis()->SetTitle(x_name.c_str());
                  gr_tot[c * n_obj + i]->GetYaxis()->SetTitle(cvs_y_name.c_str());
                  if (gSIM_IS_WRITE_ROOTFILES)
                     gr_tot[c * n_obj + i]->Write();
               } else {
                  gr_sm[c * n_obj + i] = new TGraph(n_x, &xd[0], &y_sm[i * n_x + 0]);
                  gr_sm[c * n_obj + i]->SetName(names_sm[i].c_str());
                  gr_sm[c * n_obj + i]->SetTitle(names_sm[i].c_str());
                  gr_sm[c * n_obj + i]->GetXaxis()->SetTitle(x_name.c_str());
                  gr_sm[c * n_obj + i]->GetYaxis()->SetTitle(cvs_y_name.c_str());
                  if (gSIM_IS_WRITE_ROOTFILES)
                     gr_sm[c * n_obj + i]->Write();

                  if (is_subs_all) {
                     gr_cl[c * n_obj + i] = new TGraph(n_x, &xd[0], &y_cl[i * n_x + 0]);
                     gr_cl[c * n_obj + i]->SetName(names_cl[i].c_str());
                     gr_cl[c * n_obj + i]->SetTitle(names_cl[i].c_str());
                     gr_cl[c * n_obj + i]->GetXaxis()->SetTitle(x_name.c_str());
                     gr_cl[c * n_obj + i]->GetYaxis()->SetTitle(cvs_y_name.c_str());
                     if (gSIM_IS_WRITE_ROOTFILES)
                        gr_cl[c * n_obj + i]->Write();
                     gr_tot[c * n_obj + i] = new TGraph(n_x, &xd[0], &y_tot[i * n_x + 0]);
                     gr_tot[c * n_obj + i]->SetName(names_tot[i].c_str());
                     gr_tot[c * n_obj + i]->SetTitle(names_tot[i].c_str());
                     gr_tot[c * n_obj + i]->GetXaxis()->SetTitle(x_name.c_str());
                     gr_tot[c * n_obj + i]->GetYaxis()->SetTitle(cvs_y_name.c_str());
                     if (gSIM_IS_WRITE_ROOTFILES)
                        gr_tot[c * n_obj + i]->Write();
                     if (switch_y != 0 && gPP_DM_IS_ANNIHIL_OR_DECAY) {
                        gr_cp[c * n_obj + i] = new TGraph(n_x, &xd[0], &y_cp[i * n_x + 0]);
                        gr_cp[c * n_obj + i]->SetName(names_cp[i].c_str());
                        gr_cp[c * n_obj + i]->SetTitle(names_cp[i].c_str());
                        gr_cp[c * n_obj + i]->GetXaxis()->SetTitle(x_name.c_str());
                        gr_cp[c * n_obj + i]->GetYaxis()->SetTitle(cvs_y_name.c_str());
                        if (gSIM_IS_WRITE_ROOTFILES)
                           gr_cp[c * n_obj + i]->Write();
                     }
                  }
               }
            } else if (c == 1) {
               if (switch_y == 3) {
                  string tmp = names_tot[i] + "_norm";
                  gr_tot[c * n_obj + i] = new TGraph(n_x, &xd[0], &y_tot_norm[i * n_x + 0]);
                  gr_tot[c * n_obj + i]->SetName(names_tot[i].c_str());
                  gr_tot[c * n_obj + i]->SetTitle(names_tot[i].c_str());
                  gr_tot[c * n_obj + i]->GetXaxis()->SetTitle(x_name.c_str());
                  gr_tot[c * n_obj + i]->GetYaxis()->SetTitle("M(r)/M(r_{max})");
                  if (gSIM_IS_WRITE_ROOTFILES)
                     gr_tot[c * n_obj + i]->Write();
               } else {
                  string tmp = names_sm[i] + "_norm";
                  gr_sm[c * n_obj + i] = new TGraph(n_x, &xd[0], &y_sm_norm[i * n_x + 0]);
                  gr_sm[c * n_obj + i]->SetName(tmp.c_str());
                  gr_sm[c * n_obj + i]->SetTitle(tmp.c_str());
                  gr_sm[c * n_obj + i]->GetXaxis()->SetTitle(x_name.c_str());
                  gr_sm[c * n_obj + i]->GetYaxis()->SetTitle(cvs_y_name.c_str());
                  if (gSIM_IS_WRITE_ROOTFILES)
                     gr_sm[c * n_obj + i]->Write();
                  if (is_subs_all) {
                     gr_sm[c * n_obj + i]->SetLineStyle(gr_sm_style);
                     tmp = names_cl[i] + "_norm";
                     gr_cl[c * n_obj + i] = new TGraph(n_x, &xd[0], &y_cl_norm[i * n_x + 0]);
                     gr_cl[c * n_obj + i]->SetName(names_cl[i].c_str());
                     gr_cl[c * n_obj + i]->SetTitle(names_cl[i].c_str());
                     gr_cl[c * n_obj + i]->GetXaxis()->SetTitle(x_name.c_str());
                     gr_cl[c * n_obj + i]->GetYaxis()->SetTitle(cvs_y_name.c_str());
                     if (gSIM_IS_WRITE_ROOTFILES)
                        gr_cl[c * n_obj + i]->Write();
                     tmp = names_tot[i] + "_norm";
                     gr_tot[c * n_obj + i] = new TGraph(n_x, &xd[0], &y_tot_norm[i * n_x + 0]);
                     gr_tot[c * n_obj + i]->SetName(names_tot[i].c_str());
                     gr_tot[c * n_obj + i]->SetTitle(names_tot[i].c_str());
                     gr_tot[c * n_obj + i]->GetXaxis()->SetTitle(x_name.c_str());
                     gr_tot[c * n_obj + i]->GetYaxis()->SetTitle(cvs_y_name.c_str());
                     if (gSIM_IS_WRITE_ROOTFILES)
                        gr_tot[c * n_obj + i]->Write();
                     if (switch_y != 0 && gPP_DM_IS_ANNIHIL_OR_DECAY) {
                        tmp = names_cp[i] + "_norm";
                        gr_cp[c * n_obj + i] = new TGraph(n_x, &xd[0], &y_cp_norm[i * n_x + 0]);
                        gr_cp[c * n_obj + i]->SetName(names_cp[i].c_str());
                        gr_cp[c * n_obj + i]->SetTitle(names_cp[i].c_str());
                        gr_cp[c * n_obj + i]->GetXaxis()->SetTitle(x_name.c_str());
                        gr_cp[c * n_obj + i]->GetYaxis()->SetTitle(cvs_y_name.c_str());
                        if (gSIM_IS_WRITE_ROOTFILES)
                           gr_cp[c * n_obj + i]->Write();
                     }
                  }
               }
            } else if (c == 2) {
               string tmp = names_sm[i] + "_boost";
               gr_boost[i] = new TGraph(n_x, &xd[0], &y_boost[i * n_x + 0]);
               gr_boost[i]->SetName(names_boost[i].c_str());
               gr_boost[i]->SetTitle(names_boost[i].c_str());
               gr_boost[i]->SetLineColor(gr_color[i]);
               gr_boost[i]->SetMarkerColor(gr_color[i]);
               gr_boost[i]->SetLineWidth(gr_line_width);
               gr_boost[i]->SetLineStyle(gr_style[i]);
               gr_boost[i]->SetMarkerStyle(rootmarker(i));
               gr_boost[i]->SetMarkerSize(1);
               gr_boost[i]->GetXaxis()->SetTitle(x_name.c_str());
               gr_boost[i]->GetYaxis()->SetTitle(cvs_y_name.c_str());
               if (gSIM_IS_WRITE_ROOTFILES)
                  gr_boost[i]->Write();
               multi[c]->Add(gr_boost[i], gr_opt[i].c_str());
               leg[c]->AddEntry(gr_boost[i], (list_halos[i].Name).c_str(), gr_opt[i].c_str());
            } else if (c == 3) {
               // only the case for switch_y == 1
               string tmp = names_sm[i] + "_shell";
               gr_sm_shell[c * n_obj + i] = new TGraph(x_deg_sm_shell_plot[i].size(), &x_deg_sm_shell_plot[i][0], &y_sm_shell_plot[i][0]);
               gr_sm_shell[c * n_obj + i]->SetName(names_sm[i].c_str());
               gr_sm_shell[c * n_obj + i]->SetTitle(names_sm[i].c_str());
               gr_sm_shell[c * n_obj + i]->GetXaxis()->SetTitle(x_name.c_str());
               gr_sm_shell[c * n_obj + i]->GetYaxis()->SetTitle(cvs_y_name.c_str());
               gr_sm_shell[c * n_obj + i]->SetLineStyle(gr_sm_style);
               if (gSIM_IS_WRITE_ROOTFILES)
                  gr_sm_shell[c * n_obj + i]->Write();
               if (is_subs_all) {
                  tmp = names_cl[i] + "_shell";
                  gr_cl_shell[c * n_obj + i] = new TGraph(x_deg_cl_shell_plot[i].size(), &x_deg_cl_shell_plot[i][0], &y_cl_shell_plot[i][0]);
                  gr_cl_shell[c * n_obj + i]->SetName(names_cl[i].c_str());
                  gr_cl_shell[c * n_obj + i]->SetTitle(names_cl[i].c_str());
                  gr_cl_shell[c * n_obj + i]->GetXaxis()->SetTitle(x_name.c_str());
                  gr_cl_shell[c * n_obj + i]->GetYaxis()->SetTitle(cvs_y_name.c_str());
                  if (gSIM_IS_WRITE_ROOTFILES)
                     gr_cl_shell[c * n_obj + i]->Write();
                  tmp = names_tot[i] + "_shell";
                  gr_tot_shell[c * n_obj + i] = new TGraph(x_deg_tot_shell_plot[i].size(), &x_deg_tot_shell_plot[i][0], &y_tot_shell_plot[i][0]);
                  gr_tot_shell[c * n_obj + i]->SetName(names_tot[i].c_str());
                  gr_tot_shell[c * n_obj + i]->SetTitle(names_tot[i].c_str());
                  gr_tot_shell[c * n_obj + i]->GetXaxis()->SetTitle(x_name.c_str());
                  gr_tot_shell[c * n_obj + i]->GetYaxis()->SetTitle(cvs_y_name.c_str());
                  if (gSIM_IS_WRITE_ROOTFILES)
                     gr_tot_shell[c * n_obj + i]->Write();
                  if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
                     tmp = names_cp[i] + "_shell";
                     gr_cp_shell[c * n_obj + i] = new TGraph(x_deg_cp_shell_plot[i].size(), &x_deg_cp_shell_plot[i][0], &y_cp_shell_plot[i][0]);
                     gr_cp_shell[c * n_obj + i]->SetName(names_cp[i].c_str());
                     gr_cp_shell[c * n_obj + i]->SetTitle(names_cp[i].c_str());
                     gr_cp_shell[c * n_obj + i]->GetXaxis()->SetTitle(x_name.c_str());
                     gr_cp_shell[c * n_obj + i]->GetYaxis()->SetTitle(cvs_y_name.c_str());
                     if (gSIM_IS_WRITE_ROOTFILES)
                        gr_cp_shell[c * n_obj + i]->Write();
                  }

               }
            }

            // Set graph Style
            if (c != 2) {
               if (switch_y == 3) {
                  gr_tot[c * n_obj + i]->SetLineColor(gr_color[i]);
                  gr_tot[c * n_obj + i]->SetLineWidth(gr_line_width);
                  gr_tot[c * n_obj + i]->SetLineStyle(gr_tot_style);
               } else {
                  if (c != 3) {
                     gr_sm[c * n_obj + i]->SetLineColor(gr_color[i]);
                     gr_sm[c * n_obj + i]->SetLineWidth(gr_line_width);
                     gr_sm[c * n_obj + i]->SetLineStyle(gr_style[i]);
                  } else {
                     gr_sm_shell[c * n_obj + i]->SetLineColor(gr_color[i]);
                     gr_sm_shell[c * n_obj + i]->SetLineWidth(gr_line_width);
                     gr_sm_shell[c * n_obj + i]->SetLineStyle(gr_style[i]);
                  }
                  if (is_subs_all) {
                     if (c != 3) {
                        gr_sm[c * n_obj + i]->SetLineStyle(gr_sm_style);
                        gr_cl[c * n_obj + i]->SetLineColor(gr_color[i]);
                        gr_cl[c * n_obj + i]->SetLineWidth(gr_line_width);
                        gr_cl[c * n_obj + i]->SetLineStyle(gr_cl_style);
                        gr_tot[c * n_obj + i]->SetLineColor(gr_color[i]);
                        gr_tot[c * n_obj + i]->SetLineWidth(gr_line_width);
                        gr_tot[c * n_obj + i]->SetLineStyle(gr_tot_style);
                        if (switch_y != 0 && gPP_DM_IS_ANNIHIL_OR_DECAY) {
                           gr_cp[c * n_obj + i]->SetLineColor(gr_color[i]);
                           gr_cp[c * n_obj + i]->SetLineWidth(gr_line_width);
                           gr_cp[c * n_obj + i]->SetLineStyle(gr_cp_style);
                        }
                     } else {
                        gr_sm_shell[c * n_obj + i]->SetLineStyle(gr_sm_style);
                        gr_cl_shell[c * n_obj + i]->SetLineColor(gr_color[i]);
                        gr_cl_shell[c * n_obj + i]->SetLineStyle(gr_cl_style);
                        gr_cl_shell[c * n_obj + i]->SetLineWidth(gr_line_width);
                        gr_tot_shell[c * n_obj + i]->SetLineColor(gr_color[i]);
                        gr_tot_shell[c * n_obj + i]->SetLineStyle(gr_tot_style);
                        gr_tot_shell[c * n_obj + i]->SetLineWidth(gr_line_width);
                        if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
                           gr_cp_shell[c * n_obj + i]->SetLineColor(gr_color[i]);
                           gr_cp_shell[c * n_obj + i]->SetLineStyle(gr_cp_style);
                           gr_cp_shell[c * n_obj + i]->SetLineWidth(gr_line_width);
                        }
                     }
                  }
               }
            }

            // Add in multi and set legends
            if (c == 0 || c == 1 || c == 3) {
               if (switch_y == 3) {
                  multi[c]->Add(gr_tot[c * n_obj + i], gr_opt[i].c_str());
                  char tmp[500];
                  sprintf(tmp, ", z = %.*g", gSIM_SIGDIGITS, list_halos[i].z);
                  string redshift_leg = string(tmp);
                  sprintf(tmp, ", R_{#Delta} = %.*g kpc", gSIM_SIGDIGITS, list_halos[i].Rvir * (1 + list_halos[i].z));
                  string rvir_leg = string(tmp);
                  leg[c]->AddEntry(gr_tot[c * n_obj + i],
                                   (list_halos[i].Name + redshift_leg + rvir_leg).c_str(),
                                   gr_opt[i].c_str());
               } else {
                  if (c != 3) {
                     multi[c]->Add(gr_sm[c * n_obj + i], gr_opt[i].c_str());
                     if (!is_subs_all) {
                        leg[c]->AddEntry(gr_sm[c * n_obj + i], legend[i].c_str(), gr_opt[i].c_str());
                     } else {
                        leg[c]->AddEntry(gr_sm[c * n_obj + i], (list_halos[i].Name + " " + leg_sm).c_str(), gr_opt[i].c_str());
                        multi[c]->Add(gr_cl[c * n_obj + i], gr_opt[i].c_str());
                        multi[c]->Add(gr_tot[c * n_obj + i], gr_opt[i].c_str());
                        leg[c]->AddEntry(gr_cl[c * n_obj + i], (list_halos[i].Name + " " + leg_cl).c_str(), gr_opt[i].c_str());
                        leg[c]->AddEntry(gr_tot[c * n_obj + i], (list_halos[i].Name + " " + leg_tot).c_str(), gr_opt[i].c_str());
                        if (switch_y != 0 && gPP_DM_IS_ANNIHIL_OR_DECAY) {
                           multi[c]->Add(gr_cp[c * n_obj + i], gr_opt[i].c_str());
                           leg[c]->AddEntry(gr_cp[c * n_obj + i], (list_halos[i].Name + " " + leg_cp).c_str(), gr_opt[i].c_str());
                        }
                     }
                  } else {
                     multi[c]->Add(gr_sm_shell[c * n_obj + i], gr_shell_opt[i].c_str());
                     if (!is_subs_all) {
                        leg[c]->AddEntry(gr_sm_shell[c * n_obj + i], (list_halos[i].Name + " " + leg_sm).c_str(), gr_shell_opt[i].c_str());
                     }  else {
                        leg[c]->AddEntry(gr_sm_shell[c * n_obj + i], (list_halos [i].Name + " " + leg_sm).c_str(), gr_shell_opt[i].c_str());
                        multi[c]->Add(gr_cl_shell[c * n_obj + i], gr_shell_opt[i].c_str());
                        multi[c]->Add(gr_tot_shell[c * n_obj + i], gr_shell_opt[i].c_str());
                        leg[c]->AddEntry(gr_cl_shell[c * n_obj + i], (list_halos[i].Name + " " + leg_cl).c_str(), gr_shell_opt[i].c_str());
                        leg[c]->AddEntry(gr_tot_shell[c * n_obj + i], (list_halos[i].Name + " " + leg_tot).c_str(), gr_shell_opt[i].c_str());
                        if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
                           multi[c]->Add(gr_cp_shell[c * n_obj + i], gr_shell_opt[i].c_str());
                           leg[c]->AddEntry(gr_cp_shell[c * n_obj + i], (list_halos[i].Name + " " + leg_cp).c_str(), gr_shell_opt[i].c_str());
                        }
                     }
                  }
               }
            }
         }

         // Draw canvas
         string tmp_cvs = cvs_name;
         if (c == 1) tmp_cvs = cvs_name + "_norm";
         if (c == 2) tmp_cvs = cvs_name + "_boost";
         if (c == 3) tmp_cvs = cvs_name + "_diff";
         c_list[c] = NULL;

         c_list[c] = new TCanvas(tmp_cvs.c_str(), tmp_cvs.c_str(), c % 3 * 650, c / 3 * 550, 650, 500);
         c_list[c]->Range(-0.5, -4.5, 2.0, -0.15);
         c_list[c]->SetLogx(is_logx);
         c_list[c]->SetLogy(is_logy);
         c_list[c]->SetGridx(0);
         c_list[c]->SetGridy(0);

         // Plot graphs and legends
         multi[c]->Draw("A");
         gSIM_CLUMPYAD->Draw();
         multi[c]->GetXaxis()->SetTitle(x_name.c_str());

         if (c == 1 && switch_y == 3)
            cvs_y_name = "M(r) / M(r_{max})";
         multi[c]->GetYaxis()->SetTitle(cvs_y_name.c_str());

         if (c == 0) {
            multi[c]->SetMaximum(1.5 * ymax);
            multi[c]->SetMinimum(ymin);
         } else if (c == 1) {
            multi[c]->SetMaximum(2);
            multi[c]->SetMinimum(ymin_norm);
         } else if (c == 3) {
            multi[c]->SetMaximum(1.5 * ymax_shell);
            multi[c]->SetMinimum(ymin_shell);
         }
         multi[c]->GetXaxis()->SetLimits(xd[0], xd[xd.size() - 1 ]);

         gPad->SetTickx(1);
         gPad->SetTicky(1);
         leg[c]->Draw("SAME");

         // Text if required
         TPaveText *txt = NULL;
         if (is_subs_all) {
            if (c != 3) txt = new TPaveText(0.15, 0.15, 0.35, 0.60, "NDC");
            else txt = new TPaveText(0.65, 0.50, 0.95, 0.95, "NDC");
            txt->SetTextSize(0.03);
         } else {
            txt = new TPaveText(0.3, 0.8, 0.5, 0.9, "NDC");
            txt->SetTextSize(0.05);
         }
         txt->SetTextFont(132);
         txt->SetFillColor(0);
         txt->SetBorderSize(0);
         txt->SetTextColor(kGray + 2);
         txt->SetFillStyle(0);
         txt->SetTextAlign(12);
         int ii_start = 0;
         if (c == 3) ii_start = 1;
         for (int ii = ii_start; ii < (int)text.size(); ++ii)
            txt->AddText(text[ii].c_str());
         if (is_text) txt->Draw();

         c_list[c]->Update();
         c_list[c]->Modified();

         if (gSIM_IS_WRITE_ROOTFILES) {
            root_file->cd();
            gPad->Write();
         }
      }

      // Fluxes
      TGraph **gr_gamma = NULL;
      TGraph **gr_nu = NULL;
      TCanvas *c_fluxes = NULL;
      TLegend *leg_fluxes = NULL;
      if (is_flux) {
         gr_gamma = new TGraph*[n_obj];
         gr_nu = new TGraph*[n_obj];
         leg_fluxes = new TLegend(0.45, 0.6, 0.65, 0.95);
         leg_fluxes->SetFillColor(kWhite);
         leg_fluxes->SetTextSize(0.03);
         leg_fluxes->SetBorderSize(0);
         leg_fluxes->SetFillStyle(0);
         par_spec[2] = kGAMMA;
         string leg_gamma = legend_for_spectrum(par_spec, false, false);
         leg_fluxes->SetHeader(leg_gamma.c_str());
         char tmp_ttt[500];

         if (gSIM_FLUX_IS_INTEG_OR_DIFF) {
            if (switch_y == 1) sprintf(tmp_ttt, "Integrated flux in [%g, %g] GeV and area #Delta#Omega = 2#pi(1-cos(#alpha_{int}))", gSIM_FLUX_EMIN_GEV, gSIM_FLUX_EMAX_GEV);
            else if (switch_y == 2) sprintf(tmp_ttt, "Integrated flux in [%g, %g] GeV", gSIM_FLUX_EMIN_GEV, gSIM_FLUX_EMAX_GEV);
         } else {
            if (switch_y == 1) sprintf(tmp_ttt, "Differential flux at E = %g GeV  and area #Delta#Omega = 2#pi(1-cos(#alpha_{int}))", gSIM_FLUX_AT_E_GEV);
            else if (switch_y == 2) sprintf(tmp_ttt, "Differential flux at E = %g GeV", gSIM_FLUX_AT_E_GEV);
         }

         leg_fluxes->AddEntry((TObject *)0, tmp_ttt, "h");
         sprintf(tmp_ttt, "Final state: #gamma (solid); #nu %s (dashed)", gNAMES_NUFLAVOUR[gSIM_FLUX_FLAG_NUFLAVOUR]);
         leg_fluxes->AddEntry((TObject *)0, tmp_ttt, "h");

         c_fluxes = NULL;

         string flux_or_intensity = "Fluxes";
         if (switch_y == 2) flux_or_intensity = "Intensities";

         c_fluxes = new TCanvas(flux_or_intensity.c_str(), flux_or_intensity.c_str(), 650, 550, 650, 500);
         c_fluxes->SetLogx(is_logx);
         c_fluxes->SetLogy(is_logy);
         c_fluxes->SetGridx(0);
         c_fluxes->SetGridy(0);

         for (int i = 0; i < n_obj; ++i) {
            char tmp_tmp[500];

            if (switch_y == 1) gr_gamma[i] = new TGraph(n_x, &xd[0], &flux_gamma[i][0]);
            else if (switch_y == 2) gr_gamma[i] = new TGraph(n_x, &xd[0], &intensity_gamma[i][0]);

            sprintf(tmp_tmp, "flux_gamma_%s_%d", names[i].c_str(), i);
            gr_gamma[i]->SetName(tmp_tmp);
            gr_gamma[i]->SetTitle("");
            gr_gamma[i]->SetLineColor(gr_color[i]);
            gr_gamma[i]->SetLineWidth(2);
            gr_gamma[i]->SetLineStyle(1);
            gr_gamma[i]->GetXaxis()->SetTitle(x_name.c_str());
            if (switch_y == 1) gr_gamma[i]->GetYaxis()->SetTitle(y_name_flux.c_str());
            else if (switch_y == 2) gr_gamma[i]->GetYaxis()->SetTitle(y_name_intensity.c_str());
            if (gSIM_IS_WRITE_ROOTFILES && switch_y != 0)
               gr_gamma[i]->Write();
            sprintf(tmp_tmp, "%s, z = %g", names[i].c_str(), list_halos[i].z);
            TLegendEntry *entry = leg_fluxes->AddEntry(gr_gamma[i], tmp_tmp, "L");
            entry->SetTextColor(gr_color[i]);

            if (switch_y == 1) gr_nu[i] = new TGraph(n_x, &xd[0], &flux_nu[i][0]);
            else if (switch_y == 2)  gr_nu[i] = new TGraph(n_x, &xd[0], &intensity_nu[i][0]);

            sprintf(tmp_tmp, "flux_nu_%s_%d", names[i].c_str(), i);
            gr_nu[i]->SetName(tmp_tmp);
            gr_nu[i]->SetTitle("");
            gr_nu[i]->SetLineColor(gr_color[i]);
            gr_nu[i]->SetLineWidth(2);
            gr_nu[i]->SetLineStyle(7);
            gr_nu[i]->GetXaxis()->SetTitle(x_name.c_str());
            gr_nu[i]->GetYaxis()->SetTitle(y_name_flux.c_str());
            if (gSIM_IS_WRITE_ROOTFILES && switch_y != 0)
               gr_nu[i]->Write();

            if (gSIM_IS_DISPLAY) {
               if (i == 0) {
                  gr_gamma[i]->Draw("AL");
                  gSIM_CLUMPYAD->Draw();
                  gr_gamma[i]->GetXaxis()->SetTitle(x_name.c_str());
                  if (switch_y == 1) gr_gamma[i]->GetYaxis()->SetTitle(y_name_flux.c_str());
                  else if (switch_y == 2) gr_gamma[i]->GetYaxis()->SetTitle(y_name_intensity.c_str());
                  gPad->SetTickx(1);
                  gPad->SetTicky(1);
                  gr_gamma[i]->SetMaximum(1.5 * ymax_flux);
                  gr_gamma[i]->SetMinimum(ymin_flux);
               } else
                  gr_gamma[i]->Draw("LSAME");

               gr_nu[i]->Draw("LSAME");
               gSIM_CLUMPYAD->Draw();
            }
         }
         leg_fluxes->Draw("SAME");
      }

      if (gSIM_IS_WRITE_ROOTFILES && is_flux) {
         root_file->cd();
         gPad->Write();
      }

      //--- histo of boosts for the population of halos (pick up only 4 values on x-axis values)
      if (is_subs_all && switch_y != 0 && gPP_DM_IS_ANNIHIL_OR_DECAY && n_obj > 10) {
         double boost_min = multi[2]->GetYaxis()->GetXmin();
         double boost_max = multi[2]->GetYaxis()->GetXmax();
         const int n_x_histo = 4;
         int x_indices[n_x_histo] = {0, n_x / 3, 2 * n_x / 3, n_x - 1};
         //-------------
         // DAVID
         // Higher-level option to choose values for which to draw boost
         //const int n_x_histo = 4;
         //double alpha_boost[n_x_histo] = {0.01, 0.05, 0.1, 0.5};
         //int x_indices[n_x_histo];
         //for (int i =0; i<n_x_histo; ++i)
         //   x_indices[i] = TMath::BinarySearch((int)x.size(), &x[0], alpha_boost[i]);
         //-------------
         histo_boost = new TH1D*[n_x_histo];
         vector<double> alphaint_or_theta;
         bool is_alphaint_or_theta = 1;
         if (switch_y == 2) is_alphaint_or_theta = 0;
         for (int j = 0; j < n_x_histo; ++j) {
            char tmp[100];
            sprintf(tmp, "pop_boost_%d", j);
            histo_boost[j] = new TH1D(tmp, tmp, max(n_obj / 10, 20), boost_min, boost_max);
            // DAVID
            // Useful to have same number of bins and range for comparison of several models.
            //histo_boost[j] = new TH1D(tmp,tmp,max(n_obj/10,20), 1., 200.);
            histo_boost[j]->SetName(tmp);
            histo_boost[j]->SetTitle("");
            histo_boost[j]->SetStats(kFALSE);
            histo_boost[j]->SetLineColor(rootcolor(j));
            histo_boost[j]->SetLineStyle(j + 1);
            histo_boost[j]->SetLineWidth(2);
            if (is_alphaint_or_theta)
               histo_boost[j]->GetXaxis()->SetTitle("Boost(#alpha_{int})#equiv[J_{sm}+<J_{subs}>+J_{crossprod}]/J_{tot}");
            else
               histo_boost[j]->GetXaxis()->SetTitle("Boost(#theta)#equiv[J_{sm}+<J_{subs}>+J_{crossprod}]/J_{tot}");
            histo_boost[j]->GetYaxis()->SetTitle("N");
            alphaint_or_theta.push_back(x[x_indices[j]]);
            for (int i = 0; i < n_obj; ++i)
               histo_boost[j]->Fill(y_boost[i * n_x + x_indices[j]]);
            if (gSIM_IS_WRITE_ROOTFILES) {
               root_file->cd();
               histo_boost[j]->Write();
            }
         }

         c_boost = new TCanvas("histo_boost", "histo_boost", 0, 550, 300, 300);
         c_boost->SetTickx(1);
         c_boost->SetTicky(1);
         c_boost->Range(-0.5, -4.5, 2.0, -0.15);
         c_boost->SetLogx(0);
         c_boost->SetLogy(1);
         c_boost->SetGridx(0);
         c_boost->SetGridy(0);

         leg_boost = new TLegend(0.6, 0.6, 0.95, 0.95);
         leg_boost->SetFillColor(kWhite);
         leg_boost->SetTextSize(0.05);
         leg_boost->SetTextFont(132);
         leg_boost->SetBorderSize(0);
         leg_boost->SetFillStyle(0);

         for (int j = 0; j < (int)alphaint_or_theta.size(); ++j) {
            char tmp_alpha[200];
            if (is_alphaint_or_theta)
               sprintf(tmp_alpha, "#alpha_{int} = %.3f #circ", alphaint_or_theta[j]);
            else
               sprintf(tmp_alpha, "#theta = %.3f #circ", alphaint_or_theta[j]);
            leg_boost->AddEntry(histo_boost[j], tmp_alpha, "L");
            if (j == 0) histo_boost[j]->Draw();
            else histo_boost[j]->Draw("SAME");
            gSIM_CLUMPYAD->Draw();
         }
         leg_boost->Draw("SAME");
         c_boost->Update();
         c_boost->Modified();
         root_file->cd();
         gPad->Write();
      }

      if (gSIM_IS_WRITE_ROOTFILES) {
         cout << " ... output [use ROOT TBrowser] written in: " << f_root << endl;
      }
      if (gSIM_IS_PRINT)
         cout << "_______________________" << endl << endl;

      if (gSIM_IS_DISPLAY)
         gSIM_ROOTAPP->Run(kTRUE);

      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->Close(); // closing ROOT file before display creates crash.
         delete root_file;
      }
      root_file = NULL;

      // Free memory
      if (leg_boost) delete leg_boost;
      leg_boost = NULL;
      if (c_boost) delete c_boost;
      c_boost = NULL;
      if (histo_boost) delete[] histo_boost;
      histo_boost = NULL;

      if (leg) delete[] leg;
      leg = NULL;
      if (multi) delete[] multi;
      multi = NULL;

      if (c_list) {
         for (Int_t cc = 0; cc < n_cvs; ++cc) {
            Int_t c;
            if (!is_subs_all && switch_y == 1 && cc == 2) c = 3;
            else if (switch_y == 2 && cc == 2) c = 3;
            else c = cc;
            delete c_list[c];
            c_list[c] = NULL;
         }
         delete[] c_list;
      }
      c_list = NULL;
      // Free memory for fluxes
      if (gr_gamma) delete[] gr_gamma;
      if (gr_nu) delete[] gr_nu;
      if (c_fluxes) delete c_fluxes;
      if (leg_fluxes) delete leg_fluxes;
      if (gr_boost && is_subs_all) delete[] gr_boost;
   }
#endif
   gSIM_ALPHAINT = old_alpha_aperture;
}

//______________________________________________________________________________
void halo_jeans(vector<double> const &x, int switch_y)
{
   //--- Prints/Plots y(x) and y(x)/y(x[0]) for a halo with Jeans analysis parameters.
   //
   //  x              Grid of x-axis values
   //  switch_y       Quantity to display
   //                    0 => sigma_p(R_kpc)
   //                    1 => I(R_kpc)
   //                    2 => beta_ani(r_kpc)

   // Load CLUMPY parameters
   vector<struct gStructHalo> list_halos;

   // load Jeans parameters
   halo_load_list4jeans(gLIST_HALOES_JEANS, list_halos, switch_y);

   // Set ROOT style
#if IS_ROOT
   rootstyle_set2CLUMPY();
#endif

   int n_x = x.size();
   int n_obj = (int)list_halos.size();

   // What is going to be calculated (for prints and plots)
   //  => 3 values or y (switch_y=0, 1, or 2)
   string cvs_name[3] = {"sigmapR", "IR", "Betar"};
   string x_name[3] = {"R [kpc]", "R [kpc]", "r [kpc]"};
   string y_name[3] = {"#sigma_{p} [km s^{-1}]", "I [L_{#odot} kpc^{-2}]", "#beta_{ani}"};
   string y_norm_name[3] = {"Normalised #sigma_{p}(R)", "Normalised I(R)", "Normalised #beta_{ani}(r)"};

   cout << ">>>>> " << y_name[switch_y] << " for all halos in " << gLIST_HALOES_JEANS << endl;

   if (switch_y == 0 || switch_y == 1)
      cout << "  => Relative precision: " << gSIM_EPS << endl;

   if (n_obj <= 0) {
      cout << "...... no haloes found in the list: nothing to do" << endl;
      return;
   }

   // for plotting the associated data [optional]
   vector<vector <double> > x_data;
   vector<vector <double> > dx_data;
   vector<vector <double> > y_data;
   vector<vector <double> > dy_data;
   vector<string> name_files;
   bool isData = 0; // Are there data to plot?

   //--- Storage variables
   vector<string> names, namesdata, legend, legenddata;
   vector<double> y(n_obj * n_x, 0.);
   vector<double> y_norm(n_obj * n_x, 0.);

   string f_halo_name = gLIST_HALOES_JEANS;

   FILE *fp[2] = {NULL, NULL};
   FILE *fp_current;
   unsigned found = f_halo_name.find_last_of("/"); // if the file is in a directory, keep only the file name without the path
   if ((int)found != -1) // a "/" was found
      f_halo_name = f_halo_name.substr(found + 1);

   string f_name = gSIM_OUTPUT_DIR + f_halo_name + "." + cvs_name[switch_y] + ".output";
   fp[0] = fopen(f_name.c_str(), "w");
   fp[1] = stdout;

   //vector<string> list_type;
   vector<string> type_data;

   // Calculate and fill params for all haloes
   for (int i = 0; i < n_obj ; ++i) {
      // Update par and store graph names
      const int npar = 20;
      double par_jeans[npar];
      halo_set_parjeans(i, par_jeans, list_halos); // set the Jeans parameters
      double Rmax = par_jeans[6];
      par_jeans[6] = gSIM_JEANS_RMAX; // Set the maximum radius of integration
      par_jeans[14] = gSIM_EPS; // Set precision

      string tmp_name = halo_get_name(i, list_halos) + "_" + halo_get_type(i, list_halos);
      string tmp_name_data = halo_get_name(i, list_halos) + "_" + halo_get_type(i, list_halos) + "_data";
      names.push_back(tmp_name);
      namesdata.push_back(tmp_name_data);
      legend.push_back(list_halos[i].Name + " (" + list_halos[i].Type + ")");
      legenddata.push_back(list_halos[i].Name + " (" + list_halos[i].Type + ")" + " - Data ");

      name_files.push_back(list_halos[i].JeansData.FileName);

      vector<double> xdata;
      vector<double> dxdata;
      vector<double> ydata;
      vector<double> dydata;
      int xdatasize = list_halos[i].JeansData.Radius.size();
      if (xdatasize != 0) { // There are data to plot
         isData = 1;
         type_data.push_back(list_halos[i].JeansData.TypeData);
         if (type_data.size() > 1) {
            if (type_data[type_data.size() - 1] != type_data[type_data.size() - 2]) { // Two data files have different TypeData keyword, abort
               printf("\n====> ERROR: halo_jeans() in janalysis.cc");
               printf("\n             At least two data files have different data types (e.g., Sigmap and Sigmap2).");
               printf("\n             => abort()\n\n");
               abort();
            }
         }
         if (list_halos[i].JeansData.TypeData  == "Vel") { // One data file has Vel TypeData keyword (unbinned data), abort
            printf("\n====> ERROR: halo_jeans() in janalysis.cc");
            printf("\n             At least one file contains binned velocities (keyword Vel => expect unbinned).");
            printf("\n             => abort()\n\n");
            abort();
         }
         if (list_halos[i].JeansData.TypeData  == "Sigmap2") { // draw sigmap2 instead of sigmap
            print_warning("janalysis.cc", "halo_jeans()", "At least one file contains velocity dispersions square => Draw sigmap^2.");
            cvs_name[0] = "sigmap2R";
            y_name[0] = "#sigma_{p}^{2} [km^{2} s^{-2}]";
            y_norm_name[0] = "Normalised #sigma_{p}^{2}(R)";
            f_name = gSIM_OUTPUT_DIR + f_halo_name + "." + cvs_name[switch_y] + ".output";
         }
      }

      if (isData) { // Fill the data
         for (int m = 0; m < xdatasize; m++) {
            xdata.push_back(list_halos[i].JeansData.Radius[m]);
            dxdata.push_back(list_halos[i].JeansData.ErrRadius[m]);
            if (switch_y == 0) {
               ydata.push_back(list_halos[i].JeansData.VelocData[m]);
               dydata.push_back(list_halos[i].JeansData.ErrVelocData[m]);
            } else if (switch_y == 1) {
               ydata.push_back(list_halos[i].JeansData.LightData[m]);
               dydata.push_back(list_halos[i].JeansData.ErrLightData[m]);
            }
         }
      }

      // Fill the vector of data
      x_data.push_back(xdata);
      dx_data.push_back(dxdata);
      y_data.push_back(ydata);
      dy_data.push_back(dydata);


      // Print header
      if (gSIM_IS_PRINT) {
         // Print header
         for (int ff = 0; ff < 2; ++ff) {
            fp_current = fp[ff];
            if (ff == 0) fprintf(fp_current, "\n   ... processing %s ...\n", names[i].c_str());
            fprintf(fp_current, "x= %s ,  ", x_name[switch_y].c_str());
            fprintf(fp_current, "y= %s\n", y_name[switch_y].c_str());
            if (switch_y != 1)
               fprintf(fp_current, "       x              y\n");
            else
               fprintf(fp_current, "       x              y            y/y_norm\n");
         }
      }


      // Calculate y
      for (int k = 0; k < n_x; ++k) {
         double tmp_x = x[k];
         if (switch_y == 0) { // 0. sigma_p(R)
            par_jeans[13] = tmp_x; // Radius considered
            if (list_halos[i].JeansData.TypeData  == "Sigmap2") // compute sigmap2
               y[i * n_x + k] = jeans_sigmap2(tmp_x, par_jeans);
            else
               y[i * n_x + k] = sqrt(jeans_sigmap2(tmp_x, par_jeans));
         } else if (switch_y == 1) { // 1. I(R)
            double temp = par_jeans[13];
            par_jeans[13] = gSIM_JEANS_RMAX; // Set the maximum radius of integration
            y[i * n_x + k] = light_i(tmp_x, &par_jeans[7]);
            par_jeans[13] = temp; // set back par_jeans[13] to its initial value
         } else if (switch_y == 2) { // 2. beta_ani(r)
            y[i * n_x + k] = beta_anisotropy(tmp_x, &par_jeans[15]);
         }
         if (abs(y[i * n_x + 0]) > 1.e-30)
            y_norm[i * n_x + k] = y[i * n_x + k] / y[i * n_x + 0];
         else {
            cout << y_name[switch_y].c_str() << " is close to 0 for first radius considered: y/y_norm is set to 1 " << endl;
            y_norm[i * n_x + k] = 1.;
         }
         // Print on screen and in file)
         if (gSIM_IS_PRINT) {
            for (int ff = 0; ff < 2; ++ff) {
               fp_current = fp[ff];
               if (switch_y != 1)
                  fprintf(fp_current, "  %.*le   %.*le\n", gSIM_SIGDIGITS + 2,  x[k],  gSIM_SIGDIGITS, y[i * n_x + k]);
               else
                  fprintf(fp_current, "  %.*le   %.*le   %.*le\n",
                          gSIM_SIGDIGITS + 2, x[k],  gSIM_SIGDIGITS, y[i * n_x + k],  gSIM_SIGDIGITS, y_norm[i * n_x + k]);
            }
         }
      }
      par_jeans[6] = Rmax; // set back par_jeans[6] to its initial value
   }

   vector<int> indexes;
   indexes.push_back(0);

   for (int i = 0; i < (int)name_files.size(); i++) {
      bool isequal = 0;
      for (int k = 0; k < (int)indexes.size(); k++) {
         if (name_files[i] == name_files[indexes[k]])
            isequal = 1;
      }
      if (isequal == 0)
         indexes.push_back(i);
   }

   if (gSIM_IS_PRINT) {
      fclose(fp[0]);
      cout << endl;
      cout << "______________________________________________" << endl << endl;
      cout << " ... output [ASCII] written in: " << f_name << endl;
      if (!gSIM_IS_WRITE_ROOTFILES)
         cout << "______________________________________________" << endl << endl;
   }

   // Displays  and/or save .root format
#if IS_ROOT
   string f_root = gSIM_OUTPUT_DIR + f_halo_name + "." + cvs_name[switch_y] + ".root";
   TFile *root_file = NULL;
   if (gSIM_IS_WRITE_ROOTFILES)
      root_file = new TFile(f_root.c_str(), "recreate");
   if (gSIM_IS_DISPLAY || gSIM_IS_WRITE_ROOTFILES) {
      // TGraph related
      bool is_logx = gSIM_IS_XLOG;
      bool is_logy = true;
      if (switch_y == 2)
         is_logy = false;

      // Tlegend related
      double leg_xy[4] = {0., 0., 0., 0.};
      string leg_header = "Displayed halos:";
      // TText related
      vector<string> text;
      bool is_text = false;

      // Set graph properties depending on switch_y
      //  1. rho(r)
      if (switch_y == 0) {
         leg_xy[0] = 0.65;
         leg_xy[1] = 0.58;
         leg_xy[2] = 0.95;
         leg_xy[3] = 0.92;

         //  2. J(alpha_int)
      } else if (switch_y == 1) {
         leg_xy[0] = 0.42;
         leg_xy[1] = 0.38;
         leg_xy[2] = 0.95;
         leg_xy[3] = 0.72;
         if (x[0] <= 1.e-40) is_logy = false;

         //  3. J(theta)
      } else if (switch_y == 2) {
         leg_xy[0] = 0.18;
         leg_xy[1] = 0.18;
         leg_xy[2] = 0.41;
         leg_xy[3] = 0.52;
      }

      int n_cvs = 2;
      TMultiGraph **multi = new TMultiGraph*[n_cvs];
      TLegend **leg = new TLegend*[n_cvs];
      TCanvas **c_list = new TCanvas*[n_cvs];
      TGraph **gr =  new TGraph*[2 * n_obj];
      TGraphErrors **grdata =  new TGraphErrors*[2 * n_obj];

      vector<double> xd;
      for (int k = 0; k < n_x; ++k)
         xd.push_back(x[k]);

      for (int c = 0; c < n_cvs; ++c) {
         // No normalisation for beta(r)
         if (switch_y != 1 && c == 1)
            continue;

         // Fill graphs
         multi[c] = new TMultiGraph();
         if (c == 2) {
            leg_xy[0] = 0.42;
            leg_xy[1] = 0.1;
            leg_xy[2] = 0.95;
            leg_xy[3] = 0.6;
         };
         leg[c] = new TLegend(leg_xy[0], leg_xy[1], leg_xy[2], leg_xy[3]);
         leg[c]->SetFillColor(kWhite);
         leg[c]->SetTextSize(0.03);
         leg[c]->SetBorderSize(0);
         leg[c]->SetHeader(leg_header.c_str());
         leg[c]->SetFillStyle(0);


         for (int i = 0; i < n_obj; ++i) {
            // Fill and set graph properties!
            if (c == 0 || c == 1) {
               string name = names[i];
               if (c == 0)
                  gr[c * n_obj + i] = new TGraph(n_x, &xd[0], &y[i * n_x + 0]);
               else if (c == 1) {
                  name = name + "_norm";
                  gr[c * n_obj + i] = new TGraph(n_x, &xd[0], &y_norm[i * n_x + 0]);
               }
               if (c == 1)
                  gr[c * n_obj + i]->SetName(names[i].c_str());
               gr[c * n_obj + i]->SetTitle(names[i].c_str());
               gr[c * n_obj + i]->GetXaxis()->SetTitle(x_name[switch_y].c_str());
               gr[c * n_obj + i]->GetYaxis()->SetTitle(y_name[switch_y].c_str());
               gr[c * n_obj + i]->SetLineColor(rootcolor(i));
               gr[c * n_obj + i]->SetLineStyle(i % 9 + 1);
               gr[c * n_obj + i]->SetLineWidth(2);
               if (gSIM_IS_WRITE_ROOTFILES)
                  gr[c * n_obj + i]->Write();
            }
            if (c == 0) {
               grdata[c * n_obj + i] = new TGraphErrors(x_data[i].size(), &x_data[i][0], &y_data[i][0], &dx_data[i][0], &dy_data[i][0]);
               grdata[c * n_obj + i]->SetName(namesdata[i].c_str());
               grdata[c * n_obj + i]->SetTitle(namesdata[i].c_str());
               grdata[c * n_obj + i]->SetMarkerColor(rootcolor(i));
               grdata[c * n_obj + i]->SetMarkerStyle(rootmarker(i));
               grdata[c * n_obj + i]->GetXaxis()->SetTitle(x_name[switch_y].c_str());
               grdata[c * n_obj + i]->GetYaxis()->SetTitle(y_name[switch_y].c_str());
               if (gSIM_IS_WRITE_ROOTFILES)
                  grdata[c * n_obj + i]->Write();
            }
            // Add in multi and set legends
            multi[c]->Add(gr[c * n_obj + i], "L");
            leg[c]->AddEntry(gr[c * n_obj + i], legend[i].c_str(), "L");
         }

         // Add the data to the canvas; if two data files have the same name, the data set is drawn only once
         for (int k = 0; k < (int)indexes.size(); k++) {
            if (c == 0 && x_data[indexes[k]].size() != 0) {
               multi[c]->Add(grdata[c * n_obj + indexes[k]], "p");
               leg[c]->AddEntry(grdata[c * n_obj + indexes[k]], legenddata[indexes[k]].c_str(), "p");
            }
         }

         // Draw canvas
         string tmp_cvs = cvs_name[switch_y];
         if (c == 1) tmp_cvs = cvs_name[switch_y] + "_norm";
         if (gSIM_IS_DISPLAY) {
            c_list[c] = new TCanvas(tmp_cvs.c_str(), tmp_cvs.c_str(), c * 650, 0, 650, 500);
            c_list[c]->SetLogx(is_logx);
            c_list[c]->SetLogy(is_logy);


            // Plot graphs and legends
            gSIM_CLUMPYAD->Draw();
            multi[c]->Draw("A");
            multi[c]->GetXaxis()->SetTitle(x_name[switch_y].c_str());
            multi[c]->GetYaxis()->SetTitle(y_name[switch_y].c_str());
            if (c == 1) multi[c]->GetYaxis()->SetTitle(y_norm_name[switch_y].c_str());
            leg[c]->Draw("SAME");

            // Text if required
            TPaveText *txt = new TPaveText(0.3, 0.8, 0.5, 0.9, "NDC");
            txt->SetTextSize(0.05);
            txt->SetTextFont(132);
            txt->SetFillColor(0);
            txt->SetBorderSize(0);
            txt->SetTextColor(kGray + 2);
            txt->SetFillStyle(0);
            txt->SetTextAlign(12);
            for (int ii = 0; ii < (int)text.size(); ++ii)
               txt->AddText(text[ii].c_str());
            if (is_text) txt->Draw();

            c_list[c]->Update();
            c_list[c]->Modified();

            if (gSIM_IS_WRITE_ROOTFILES) {
               root_file->cd();
               gPad->Write();
            }
         }
      }
      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->Close();
         delete root_file;
         cout << " ... output [use ROOT TBrowser] written in: " << f_root << endl;
      }
      if (gSIM_IS_PRINT)
         cout << "_______________________" << endl << endl;

      if (gSIM_IS_DISPLAY)
         gSIM_ROOTAPP->Run(kTRUE);

      // Free memory
      delete[] leg;
      delete[] multi;
      if (c_list)
         delete[] c_list;
      if (grdata) delete[] grdata;
      if (gr) delete[] gr;
   }
#endif
}

//______________________________________________________________________________
void halo_j2D(string const &name_in_list, double const &fov_diam,
              bool is_subs_drawn, double const &user_rse)
{
   //--- Print/Plot 2D skymap (fov) for the halos [smooth + subcl].
   //
   //  name_in_list   Name of the halo selected
   //  fov_diam       FOV diameter of the instrument (default is spherical FOV) [rad]
   //  is_subs_drawn  true => draw sub-clump, false => uses <Jsubcl>
   //  user_rse       Relative standard error allowed (subclumps drawn vs mean value)

   // Load CLUMPY parameters
   vector<struct gStructHalo> list_halos;
   halo_load_list(gLIST_HALOES, list_halos);


   char j_or_d = 'D';
   if (gPP_DM_IS_ANNIHIL_OR_DECAY)
      j_or_d = 'J';

   // Update par (for smooth and sub) and store graph names
   const int n_pardpdv = 10;
   const int n_parsubs = 25;
   double par_tot[n_pardpdv + 1];
   double par_dpdv[n_parsubs]; // Two last parameters are psi(halo_centre) and theta(halo_centre)
   double par_dpdv_modif[n_parsubs]; // Two last parameters are psi_los and theta_los
   double par_subs[n_parsubs];

   // Find name_in_list in list
   struct gStructHalo halo;
   bool is_found = false;
   string halo_name;

   for (int i = 0; i < (int)list_halos.size(); ++i) {
      if (upper_case(list_halos[i].Name) == upper_case(name_in_list)) {
         halo = list_halos[i];

         halo_set_partot(i, par_tot, list_halos, true);
         halo_set_triaxiality(i, list_halos);
         halo_set_pardpdv(i, par_dpdv, list_halos, true);
         halo_set_pardpdv(i, par_dpdv_modif, list_halos, true);
         halo_set_parsubs(i, par_subs, list_halos);
         is_found = true;
         halo_name = halo_get_name(i, list_halos);

         // switch to full 2D calculation of smooth contributions, if triaxial halo is chosen:
         if (halo.Triaxial_Is) gSIM_IS_CIRCULAR_SYMMETRY = false;

         // Set properly the two last parameters of par_dpdv_modif to psi_los and theta_los (in rad)
         par_dpdv_modif[8] = 0.;
         par_dpdv_modif[9] = 0.;
         break;
      }
   }

   if (!is_found) {
      print_warning("janalysis.cc", "halo_j2D()", "Halo name '" + name_in_list + "' not found in the list of loaded halos.");
      return;
   }

   // fill global variables for FITS output:
   gSIM_PSI_OBS = halo.PsiDeg * DEG_to_RAD;
   char char_tmp[512];
   sprintf(char_tmp, "%g", halo.ThetaDeg);
   gSIM_THETA_OBS_DEG = string(char_tmp);
   gSIM_THETA_ORTH_SIZE_DEG = "d";
   gSIM_REDSHIFT = halo.z;
   gLIST_HALO_DISTANCE = halo.l;
   gLIST_HALO_RSCALE = halo.Rscale * (1 + halo.z);
   gLIST_HALO_RHOSCALE = halo.Rhos / pow(1 + halo.z, 3);
   if (halo.HaloProfile == kZHAO) gLIST_HALO_RHOSCALE /= pow(2, (halo.ShapeParam2 - halo.ShapeParam3) / halo.ShapeParam1);
   gLIST_HALO_RDELTA = halo.Rvir * (1 + halo.z);
   gHALO_SUBS_FLAG_PROFILE[kEXTRAGAL] = halo.HaloProfile;
   gHALO_SUBS_SHAPE_PARAMS[kEXTRAGAL][0] = halo.ShapeParam1;
   gHALO_SUBS_SHAPE_PARAMS[kEXTRAGAL][1] = halo.ShapeParam2;
   gHALO_SUBS_SHAPE_PARAMS[kEXTRAGAL][2] = halo.ShapeParam3;
   // update gSIM_INPUTPARAM_VALUESTRING to get correct halo coordinates
   bool tmp_dirty0 = false;
   if (gSIM_INPUTPARAM_VALUESTRING[kSIM_EPS] == "998") tmp_dirty0 = true;
   gSIM_INPUTPARAM_VALUESTRING = inputparameters_globalparams2string(gSIM_FLAG_MODE);
   if (tmp_dirty0) gSIM_INPUTPARAM_VALUESTRING[kSIM_EPS] = "998";

   string out_name = halo_name + "2D";
   if (gPP_DM_IS_ANNIHIL_OR_DECAY)
      out_name = "annihil_" + out_name;
   else
      out_name = "decay_" + out_name;

   double f_dm = halo.Subs_mfrac;
   double mtot_subs = halo.Mtot * f_dm;
   double mmin_subs = gDM_SUBS_MMIN;
   double mmax_subs = halo.Subs_Mmax;
   double Delta_c = delta_x_to_delta_crit(gCOSMO_DELTA0, gCOSMO_FLAG_DELTA_REF, par_subs[6]);

   bool is_subs = true;
   if (f_dm < 1.e-3)
      is_subs = false;
   if (!is_subs)
      is_subs_drawn = false;

   if (is_subs_drawn) {
#if !IS_ROOT
      printf("\n====> ERROR: halo_j2D() in janalysis.cc");
      printf("\n             You need ROOT to be installed to draw random clumps");
      printf("\n             from a multivariate probability distribution.");
      printf("\n             => abort()\n\n");
      abort();
#endif
   }

   double ntot_subs =  nsubtot_from_msubtot(mtot_subs, mmin_subs, mmax_subs, &par_subs[8], gSIM_EPS);

   double psi_los = 0.; // we calculate everything at the coordinate center
   double theta_los = 0.; // we calculate everything at the coordinate center
   double lmin = max(0., halo.l - halo.Rvir);
   double lmax = halo.l + halo.Rvir;


   /********************** INITIALIZE HEALPIX GRID/FOV  ***********************/

   int n_pix;          // number of pixels on the whole sphere
   double delta_omega; // surface area of each pixel on the sphere = J-factor integration area
   int smooth_gamma_or_nu_or_both;  // will the output be smoothed by an instrumental beam
   // and if yes, by which one?
   hp_set_resolution(n_pix, delta_omega, smooth_gamma_or_nu_or_both);

   //--- Store some values read from the parfile
   double alphaint_orig = gSIM_ALPHAINT;
   double fov_deg = fov_diam * RAD_to_DEG;

   //--- print information about map dimensions
   cout << endl;
   printf("            ______________________________________________________\n\n");
   cout << "                         2D skymap for: " << halo.Name << endl;
   printf("                   circular FOV with diameter = %.1f [deg].\n", fov_deg);
   if (fabs(fov_deg) > 360.) {
      printf("\n====> ERROR: halo_j2D() in janalysis.cc");
      printf("\n             FOV diameter |d|>360 [deg] makes no sense.");
      printf("\n             => abort()\n\n");
      abort();
   }
   if (fov_deg < 0.) {
      printf("\n====> ERROR: halo_j2D() in janalysis.cc");
      printf("\n             Inverse mode (negative FOV diameter) not supported in halo mode.");
      printf("\n             => abort()\n\n");
      abort();
   }
   if (!is_subs)
      cout << "                   [centered on the halo position (smooth)]" << endl;
   else if (is_subs_drawn)
      cout << "            [centered on the halo position (continuum + drawn cl)]" << endl;
   else
      cout << "                 [centered on the halo position (continuum)]" << endl;
   cout << "                      - Integration angle: " << alphaint_orig *RAD_to_DEG << " deg" << endl;
   cout << "                      - Relative precision: " << gSIM_EPS << endl;
   printf("            ______________________________________________________\n\n");

   // the following function now creates the FOV:
   string grid_mode = "DISK" ;
   rangeset<int> rs_pix_fov = hp_set_pix_fov(grid_mode, psi_los,  theta_los, fov_diam,
                              fov_diam, gSIM_HEALPIX_NSIDE, gSIM_HEALPIX_SCHEME) ;

   vector<int> v_pix_fov;
   rs_pix_fov.toVector(v_pix_fov);
   int n_pix_fov = rs_pix_fov.nval(); // number of pixels in the FOV.
   T_Healpix_Base<int> hp_gridprop(gSIM_HEALPIX_NSIDE, gSIM_HEALPIX_SCHEME, SET_NSIDE) ;

   if (smooth_gamma_or_nu_or_both == 0) {
      cout << "      No smoothing of map by instrumental beam is applied.\n" << endl;
   } else if (smooth_gamma_or_nu_or_both == 1) {
      cout << "      Output map will be smoothed by gSIM_GAUSSBEAM_GAMMA_FWHM_DEG = " << gSIM_GAUSSBEAM_GAMMA_FWHM *RAD_to_DEG << " [deg]," << endl;
      cout << "      and the result will be stored in the 3rd extension of the output FITS file.\n" << endl;
   } else if (smooth_gamma_or_nu_or_both == 2) {
      cout << "      Output map will be smoothed by gSIM_GAUSSBEAM_NEUTRINO_FWHM_DEG = " << gSIM_GAUSSBEAM_NEUTRINO_FWHM *RAD_to_DEG << " [deg]," << endl;
      cout << "      and the result will be stored in the 3rd extension of the output FITS file.\n" << endl;
   } else if (smooth_gamma_or_nu_or_both == 3) {
      cout << "      Output map will be smoothed by gSIM_GAUSSBEAM_GAMMA_FWHM_DEG = " << gSIM_GAUSSBEAM_GAMMA_FWHM *RAD_to_DEG << " [deg]" << endl;
      cout << "      and separately smoothed by gSIM_GAUSSBEAM_NEUTRINO_FWHM_DEG = " << gSIM_GAUSSBEAM_NEUTRINO_FWHM *RAD_to_DEG << " [deg]," << endl;
      cout << "      and both results will be stored in the 3rd extension of the output FITS file.\n" << endl;
      if (gSIM_GAUSSBEAM_GAMMA_FWHM > gSIM_GAUSSBEAM_NEUTRINO_FWHM) {
         sprintf(char_tmp, "Your neutrino instrument resolution (%.2le deg) is better "
                 "than your gamma resolution (%.2le deg) => Is this really what you want?",
                 gSIM_GAUSSBEAM_NEUTRINO_FWHM * RAD_to_DEG, gSIM_GAUSSBEAM_GAMMA_FWHM * RAD_to_DEG);
         print_warning("janalysis.cc", "halo_j2D()", string(char_tmp));
      }
   }
   cout << ">>>>> N.B.: FOV diameter = " << fov_deg << " deg, with NSIDE = " << gSIM_HEALPIX_NSIDE << "   =>   " << n_pix_fov << " pixels." << endl;
   cout << "   => surface of the FOV: " << (float)n_pix_fov *delta_omega << " sr." << endl;
   cout << "   => fraction of sky covered: " << (float)n_pix_fov / (float)n_pix * 100. << "%\n" << endl;

   // if there will be a smoothing of the maps,
   // additionally create jgal_smooth/subs/cross_continuum on a bigger grid:
   double fov_rad_extended ;
   rangeset<int> rs_pix_fov_extended;
   vector<int> v_pix_fov_extended ;
   int n_pix_fov_extended = 0;
   string grid_mode_extended = grid_mode ;

   if (smooth_gamma_or_nu_or_both != 0 && n_pix_fov != n_pix) {
      double grid_extension = 5. * max(gSIM_GAUSSBEAM_GAMMA_FWHM, gSIM_GAUSSBEAM_NEUTRINO_FWHM);
      do_safegridextension(fov_diam, fov_diam, grid_extension, grid_mode_extended,
                           fov_rad_extended, fov_rad_extended) ;

      rs_pix_fov_extended = hp_set_pix_fov(grid_mode_extended, psi_los,  theta_los,  fov_rad_extended,  fov_rad_extended,
                                           gSIM_HEALPIX_NSIDE, gSIM_HEALPIX_SCHEME) ;
      rs_pix_fov_extended.toVector(v_pix_fov_extended);
      n_pix_fov_extended = rs_pix_fov_extended.nval(); // number of pixels in the FOV.
      cout << "   => size of extended grid for proper smoothing: " << n_pix_fov_extended << " pixels.\n" << endl;
   }


   /************************* GET GRID DIMENSIONS  ****************************/
   // (it is easy here:)
   double psi_min = - fov_diam / 2.;
   double theta_min = - fov_diam / 2.;

   /********************* OUTPUT FILE CREATION AND CHECK  *********************/

   string filename_core_str ;
   char f_name[500];

   if (is_subs_drawn)
      sprintf(f_name, "%s_FOVdiameter%.1fdeg_nside%d",
              out_name.c_str(), fov_deg, gSIM_HEALPIX_NSIDE);
   else
      sprintf(f_name, "%s_FOVdiameter%.1fdeg_nside%d",
              out_name.c_str(), fov_deg, gSIM_HEALPIX_NSIDE);

   filename_core_str.append(f_name) ;

   string filename_fits_str = gSIM_OUTPUT_DIR + filename_core_str + ".fits";
   string filename_root_str = gSIM_OUTPUT_DIR + filename_core_str + ".root";
   string filename_drawn_str = gSIM_OUTPUT_DIR + filename_core_str + ".drawn";

   fitshandle fh_file_out;
   const char *filename_fits_char = filename_fits_str.c_str();

   if (gSIM_IS_PRINT) {
      ifstream isfile(filename_fits_char);
      if (isfile) {
         print_warning("janalysis.cc", "halo_j2D()", "Output file " + filename_fits_str + " already exists and "
                       "will be overwritten. You have 5 seconds to kill the process with CTRL + C");
         usleep(5e6);
         remove(filename_fits_char);
      }
      fh_file_out.create(filename_fits_str);
   }


   /****************** FILL ARRAYS AND START TO CALCULATE!  *******************/

   // N.B.: no subclumps => mthresh = gDSPH_CLUMPS_MMAX
   //       otherwise: find mthresh
   double mthresh = mmax_subs;
   if (is_subs_drawn) {
      double jgal_bkd = 0.;
      double tol = max(1.e-2, gSIM_EPS);
      mthresh = find_mthresh_fov(par_tot, f_dm, par_dpdv, fov_deg, lmin, lmax, par_subs,
                                 mmin_subs, mmax_subs, user_rse, tol, jgal_bkd);
   }

   vector<double> j_tot(n_pix_fov, -1.e-40);
   vector<double> j_sm(n_pix_fov, -1.e-40);
   vector<double> j_subs_mthresh;
   vector<double> j_crossprod;
   vector<double> j_drawn;

   // when choosing a smoothing, these quantities are calculated on a bigger grid
   vector<double> j_tot_beam; // (will be initialised later to save memory.)
   vector<double> j_sm_beam;
   vector<double> j_subs_mthresh_beam;
   vector<double> j_crossprod_beam;

   // Create drawn clumps arrays, if required
   if (is_subs) {
      j_subs_mthresh.assign(n_pix_fov, -1.e-40);
      if (gPP_DM_IS_ANNIHIL_OR_DECAY)
         j_crossprod.assign(n_pix_fov, -1.e-40);
      if (is_subs_drawn)
         j_drawn.assign(n_pix_fov, -1.e-40);
   }

   if (smooth_gamma_or_nu_or_both != 0 && n_pix_fov != n_pix) {
      // Create smooth maps in case there will be an instrumental beam smoothin later:
      j_sm_beam.assign(n_pix_fov_extended, -1.e-40);
      if (is_subs) {
         j_subs_mthresh_beam.assign(n_pix_fov_extended, -1.e-40);
         if (gPP_DM_IS_ANNIHIL_OR_DECAY)
            j_crossprod_beam.assign(n_pix_fov_extended, -1.e-40);
      }
   }

   // Calculate smooth
   //------------------
   // (using interpolation to speed-up calculation when required)
   cout << "    * smooth halo contribution" << endl;
   if (smooth_gamma_or_nu_or_both == 0 || n_pix_fov == n_pix) {
      j_interpolation(&j_sm[0], rs_pix_fov, grid_mode, psi_los, theta_los, fov_diam, fov_diam,
                      0, halo.Mtot, par_tot, gSIM_EPS, halo.Subs_mfrac, par_dpdv, par_subs);
   } else {
      j_interpolation(&j_sm_beam[0], rs_pix_fov_extended, grid_mode_extended, psi_los, theta_los, fov_rad_extended,
                      fov_rad_extended, 0, halo.Mtot, par_tot, gSIM_EPS, halo.Subs_mfrac, par_dpdv, par_subs);
      // copy values on extended grid into jgal_sm array:
      for (int i = 0; i < n_pix_fov; ++i) {
         int hp_index = v_pix_fov[i] ;
         int i_extended = binary_search(v_pix_fov_extended, hp_index);
         j_sm[i] = j_sm_beam[i_extended];
      }
   }

   if (is_subs) {
      cout << "    * <subs> contribution (below m_thresh)" << endl;
      // Dummy kmax, l_crit, m1 and m2 values formatted as required by j_interp
      vector<double> l_crit, m1, m2;
      int kmax = 1;
      l_crit.push_back(lmin);
      m1.push_back(mmin_subs);
      m2.push_back(mthresh);
      if (smooth_gamma_or_nu_or_both == 0 || n_pix_fov == n_pix) {
         j_interpolation(&j_subs_mthresh[0], rs_pix_fov, grid_mode, psi_los, theta_los, fov_diam, fov_diam,
                         1, halo.Mtot, par_tot, gSIM_EPS, halo.Subs_mfrac, par_dpdv,
                         par_subs, ntot_subs, kmax, &l_crit[0], &m1[0], &m2[0]);
      } else {
         j_interpolation(&j_subs_mthresh_beam[0], rs_pix_fov_extended, grid_mode_extended, psi_los, theta_los, fov_rad_extended, fov_rad_extended,
                         1, halo.Mtot, par_tot, gSIM_EPS, halo.Subs_mfrac, par_dpdv,
                         par_subs, ntot_subs, kmax, &l_crit[0], &m1[0], &m2[0]);
         // copy values on extended grid into jgal_sub_continuum array:
         for (int i = 0; i < n_pix_fov; ++i) {
            int hp_index = v_pix_fov[i] ;
            int i_extended = binary_search(v_pix_fov_extended, hp_index);
            j_subs_mthresh[i] = j_subs_mthresh_beam[i_extended];
         }
      }
      if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
         printf("    * cross-product contribution\n");
         if (smooth_gamma_or_nu_or_both == 0 || n_pix_fov == n_pix) {
            j_interpolation(&j_crossprod[0], rs_pix_fov, grid_mode, psi_los, theta_los, fov_diam, fov_diam,
                            2, halo.Mtot, par_tot, gSIM_EPS, halo.Subs_mfrac, par_dpdv,
                            par_subs, ntot_subs, kmax, &l_crit[0], &m1[0], &m2[0]);
         } else {
            j_interpolation(&j_crossprod_beam[0], rs_pix_fov_extended, grid_mode_extended, psi_los, theta_los, fov_rad_extended, fov_rad_extended,
                            2, halo.Mtot, par_tot, gSIM_EPS, halo.Subs_mfrac, par_dpdv,
                            par_subs, ntot_subs, kmax, &l_crit[0], &m1[0], &m2[0]);
            // copy values on extended grid into jgal_crossprod array:
            for (int i = 0; i < n_pix_fov; ++i) {
               int hp_index = v_pix_fov[i] ;
               int i_extended = binary_search(v_pix_fov_extended, hp_index);
               j_crossprod[i] = j_crossprod_beam[i_extended];
            }
         }
      }
   }

   string unit = units_or_canvasname_1D(1, 2, 0);
   string to_replace = "#alpha_{int}";
   char tmp_unit[100];
   sprintf(tmp_unit, "%.4f^{#circ}", gSIM_ALPHAINT * RAD_to_DEG);
   string replace_with = string(tmp_unit);
   replace_substring(unit, to_replace, replace_with);

   // Do calculation for drawn clumps
   if (is_subs_drawn) {

      //--- Set Seed
      printf("========> SEED=%d\n", gSIM_SEED);
#if IS_ROOT
      gRandom->SetSeed(gSIM_SEED);
#endif

      // Total number of clumps in the FOV of the instrument
      // N.B.: the drawing is based on a solid angle whereas the skymap is calculated
      // on a square (on the sky). To ensure that there is no faulty counting
      // of the number of clumps, we draw them for an angle = fov*sqrt(2)
      // (which corresponds to the edge of the diagonal), and afterward
      // exclude those falling outside of the fov (this overestimates a bit
      // the number of clumps to draw, but that's the best we came out with).

      gSIM_ALPHAINT = fov_diam / 2. + gSIM_RESOLUTION;// radius = 0.5 * diameter + some extension to be on the safe side.;
      double ntot_foi = ntot_subs * frac_nsubs_in_foi(par_dpdv, psi_los, theta_los, lmin, lmax, gSIM_EPS);
      double nsub_drawn = ntot_foi * frac_nsubs_in_m1m2(&par_subs[8], mthresh, mmax_subs, gSIM_EPS);

      // Reset the integration to alpha_int
      gSIM_ALPHAINT = alphaint_orig;


      printf("    * Summary: \n");
      printf("       Mtot=%le Msol     Msubcl=%le Msol\n", halo.Mtot, mtot_subs);
      printf("       Nsubcl_tot=%le    Nsubl_fov=%le\n", ntot_subs, ntot_foi);
      printf("       => %.0f clumps to draw in the FOV (for Msubcl>%.2le Msol)\n\n", nsub_drawn, mthresh);


      //++++++++++++++++//
      // Draw subclumps //
      //++++++++++++++++//

      // Draw the clumps from mthresh to gDSPH_CLUMPS_MMAX
#if IS_ROOT
      TF1 *f_mass = new TF1("f_mass", dpdlogm, log10(mthresh), log10(mmax_subs), 2);
      //  par_subs[8] = dPdM normalisation
      //  par_subs[9] = dPdM slope alphaM
      f_mass->SetNpx(100);
      f_mass->SetParameter(0, 1.);
      f_mass->SetParameter(1, par_subs[9]);
#endif

      //--- Open file to store J for all drawn clumps
      FILE *fp_drawn = NULL;

      if (gSIM_IS_PRINT) {
         fp_drawn = fopen(filename_drawn_str.c_str(), "w");
         fprintf(fp_drawn, "# This file lists the properties and %ccl(centre) for all drawn subclumps in host halo %s at (l,b) = (%g,%g) deg.\n", j_or_d, halo_name.c_str(), halo.PsiDeg, halo.ThetaDeg);
         fprintf(fp_drawn, "# Name           Type    long    lat      d     z   Rdelta   |     rhos       rs     prof.   #1   #2   #3   |      %c         %c/%ccontinuum |  Mdelta  |  long    lat  (w.r.t. halo center)\n",
                 j_or_d, j_or_d, j_or_d);
         fprintf(fp_drawn, "#  -               -     [deg]   [deg]  [kpc]   -    [kpc]   |  [Msol/kpc3]  [kpc]  [enum]    -    -    -   | %s   -   |  [Msol]  |  [deg]   [deg]\n", unit.c_str());
         fprintf(fp_drawn, "#\n");
      }


      // Define the clump spatial distribution
      // N.B.: f_lalphabeta defines a cone whereas the skymap is calculated
      // on a square (on the sky). To ensure that there is no faulty counting
      // of the number of clumps, we draw them for an angle = fov/sqrt(2)
      // (which corresponds to the edge of the diagonal, see above).
#if IS_ROOT
      TF3 *f_lalphabeta = new TF3("f_lalphabeta", dpdv_lcosalphabeta, lmin, lmax, MY_COS(fov_diam / sqrt(2.)), 1., 0., 2 * PI, 10);
      f_lalphabeta->SetNpx(100);
      f_lalphabeta->SetNpy(1000);
      f_lalphabeta->SetNpz(100);
      f_lalphabeta->SetParameters(par_dpdv_modif);
#endif
      int n_print = max(25, (int)nsub_drawn / 100);

      // Draw clumps to be drawn and calculate/store their J
      for (int n = 1; n <= (int)nsub_drawn; ++n) {
         int nn = n / n_print;
         if (n % n_print == 1) {
            int n_last = min((nn + 1) * n_print, (int)nsub_drawn);
            printf("        ... processing clumps %d to %d ...\n",
                   nn * n_print + 1, n_last);
         }

         // Draw position
         double l_cl = 0.;
         double cos_alpha_cl = 0.;
         double beta_cl = 0;
         double psi_cl, theta_cl;
         double psi_cl_orig, theta_cl_orig, r_cl, x_cl; // we have had centered the host halo at (l,b,) = (0,0). These coordinates will give the subclump position for the host clump at the original Gal. coordinates.

#if IS_ROOT
         f_lalphabeta->GetRandom3(l_cl, cos_alpha_cl, beta_cl);
#endif

         double par_tmp[2] = {0., 0.};
         double par_tmp_orig[2] = {halo.PsiDeg * DEG_to_RAD, halo.ThetaDeg * DEG_to_RAD};
         double x[3] = {l_cl, acos(cos_alpha_cl), beta_cl};
         double x_orig[3] = {l_cl, acos(cos_alpha_cl), beta_cl};
         lalphabeta_to_lpsitheta(x, par_tmp);
         lalphabeta_to_lpsitheta(x_orig, par_tmp_orig);
         psi_cl = x[1];
         check_psi(psi_cl);
         theta_cl = x[2];
         x[1] = acos(cos_alpha_cl);
         x[2] = beta_cl;
         lalphabeta_to_rpsitheta(x, par_tmp);
         r_cl = x[0];
         x_cl = r_cl / halo.Rvir;

         psi_cl_orig = x_orig[1];
         theta_cl_orig = x_orig[2];

         // Draw mass
         double log_mass_cl = 0.;
#if IS_ROOT
         log_mass_cl = f_mass->GetRandom();
#endif

         // sometimes, nan values are thrown...
         if (std::isinf(float(log_mass_cl)) || isnan(float(log_mass_cl))) {
            print_warning("janalysis.cc", "halo_j2D()", "Found nan-value for log_mass_cl. Throw random generator again...");
            int try_n_times = 0;
            while (std::isinf(float(log_mass_cl)) || isnan(float(log_mass_cl))) {
#if IS_ROOT
               log_mass_cl = f_mass->GetRandom();
#endif
               try_n_times++;
               if (try_n_times == 100) break;
            }
            if (try_n_times == 100) {
               print_warning("janalysis.cc", "halo_j2D()", "Rethrowing random generator failed. Discard clump...");
               continue;
            }
         }

         double mass_cl = pow(10., log_mass_cl);

         double c = 0.;

         if (gDM_FLAG_CDELTA_DIST != kDIRAC) { //we draw the concentration from its distribution if gMW_SUBS_FLAG_CDELTAMDELTA is not kDIRAC
            double cvir_mean = mdelta_to_cdelta(mass_cl, Delta_c, (int)par_subs[10], par_subs, par_subs[6], x_cl);
            double par_conc[3] = { //define the concentration distribution properties
               cvir_mean, gDM_LOGCDELTA_STDDEV, (double)gDM_FLAG_CDELTA_DIST
            };
#if IS_ROOT
            TF1 *c_dist = new TF1("c_dist", dpdc, 0.01 * cvir_mean, 100.*cvir_mean, 3);
            c_dist->SetNpx(100);
            c_dist->SetParameters(par_conc);

            c = c_dist->GetRandom();
            delete c_dist;
            c_dist = NULL;
#endif
         }

         // Fill par
         const int npar = 10;
         double par_cl[npar + 1];
         mdelta_to_par(par_cl, mass_cl, Delta_c, (int)par_subs[10] /*card cdelta-mdelta*/, &par_subs[0],
                       gSIM_EPS, par_subs[6] /*redshift*/, c, x_cl);
         par_cl[7] = l_cl;
         par_cl[8] = psi_cl;
         par_cl[9] = theta_cl;
         par_cl[10] = par_subs[6];

         bool is_subsubs = false;
         double par_subsub[n_parsubs];
         const int n_pardpdv = 10;
         double par_subs_dpdv[n_pardpdv];
         double nsubsub_tot = 0;
         double mmax;

         if (par_subs[15] > 1) {
            mmax = gDM_SUBS_MMAXFRAC * mass_cl; // maximum mass of subclumps in mdelta
            double msub_tot = halo.Subs_mfrac * mass_cl;
            if (mmax > gDM_SUBS_MMIN && msub_tot > gDM_SUBS_MMIN) {
               is_subsubs = true;
               for (int i = 0; i < n_parsubs; ++i) {
                  par_subsub[i] = par_subs[i];
               }
               par_subsub[4] = par_cl[6];
               par_subsub[7] = mass_cl;
               dpdm_setnormprob(&par_subsub[8], gDM_SUBS_MMIN, mmax, gSIM_EPS);
               par_subsub[14] = halo.Subs_mfrac;
               par_subsub[15] = par_subs[15] - 1; // reduce nlevel of substructures

               for (int i = 0; i < n_pardpdv; ++i) {
                  par_subs_dpdv[i] = par_dpdv[i];
               }
               par_subs_dpdv[1] = par_cl[1] * par_subs[23];
               par_subs_dpdv[6] = par_cl[6];
               par_subs_dpdv[7] = l_cl;
               par_subs_dpdv[8] = psi_cl;
               par_subs_dpdv[9] = theta_cl;
               dpdv_setnormprob(par_subs_dpdv, gSIM_EPS);
               for (int j = 0; j < 7; ++j)
                  par_subsub[j + 16] = par_subs_dpdv[j];
               par_subsub[23] = halo.Subs_dPdV_Rs_to_Rshost;
               nsubsub_tot = nsubtot_from_msubtot(msub_tot, gDM_SUBS_MMIN, mmax, &par_subsub[8], gSIM_EPS);
            }
         }

         // Find indices psi and theta in the grid for the clump position
         // to get the continuum at this position

         pointing ptg_cl(PI / 2 - theta_cl, psi_cl) ;
         // find the closest Pixel to the clump centre
         int i_pix_closest = hp_gridprop.ang2pix(ptg_cl) ;

         //pointing ptg_i_pix_closest = hp_gridprop.pix2ang(i_pix_closest);
         //double psi_ipix_closest = ptg_i_pix_closest.phi;
         //double theta_i_pix_closest = PI / 2 - ptg_i_pix_closest.theta;
         //check_psi(psi_ipix_closest);

         if (!rs_pix_fov.contains(i_pix_closest)) continue;

         // add contribution from this clump (it's not a single pixel!)
         double j_cl_centre;
         if (halo.Subs_mfrac > 1.e-3 && is_subsubs)
            j_cl_centre = jsmooth_mix(mass_cl, par_cl, psi_cl, theta_cl, gSIM_EPS, halo.Subs_mfrac, par_subs_dpdv);
         else
            j_cl_centre = jsmooth(par_cl, psi_cl, theta_cl, gSIM_EPS);
         if (is_subsubs) {
            double l1 = max(0., par_cl[7] - par_cl[6]);
            double l2 = par_cl[7] + par_cl[6];
            j_cl_centre += jsub_continuum(nsubsub_tot, par_subs_dpdv, psi_cl, theta_cl, l1, l2, par_subsub, gDM_SUBS_MMIN, mmax);
            if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
               j_cl_centre  += jcrossprod_continuum(mass_cl, par_cl, psi_cl, theta_cl, gSIM_EPS, halo.Subs_mfrac, par_subs_dpdv);
            }
         }

         // find vector index of i_pix_closest
         int index_i_pix_closest = binary_search(v_pix_fov, i_pix_closest);

         double j_continuum = j_sm[index_i_pix_closest] + j_subs_mthresh[index_i_pix_closest];
         if (gPP_DM_IS_ANNIHIL_OR_DECAY)
            j_continuum += j_crossprod[index_i_pix_closest];

         double rhos_out = par_cl[0];
         // convert rho_0  to rho_s as used in profiles.cc for ZHAO profile:
         if (int(par_cl[5]) == kZHAO) rhos_out /= pow(2, (par_cl[3] - par_cl[4]) / par_cl[2]);

         // print in file
         if (gSIM_IS_PRINT) {
            fprintf(fp_drawn, " %7d         DSPH  %+7.2f %+7.2f  %.*le %.f %.*le    %.*le %.*le  k%s  %g   %g   %g    %.*le %.*le   %.*le  %+7.2f %+7.2f\n",
                    int(n), psi_cl_orig * RAD_to_DEG, theta_cl_orig * RAD_to_DEG,
                    gSIM_SIGDIGITS,  l_cl, -1., gSIM_SIGDIGITS, par_cl[6],
                    gSIM_SIGDIGITS, rhos_out, gSIM_SIGDIGITS, par_cl[1],
                    gNAMES_PROFILE[int(par_cl[5])], par_cl[2], par_cl[3], par_cl[4],
                    gSIM_SIGDIGITS, j_cl_centre, gSIM_SIGDIGITS,  j_cl_centre / j_continuum,
                    gSIM_SIGDIGITS, mass_cl, psi_cl * RAD_to_DEG, theta_cl * RAD_to_DEG);
         }

         // Add all pixels in the map:
         // Force to gSIM_EPS_DRAWN relative precision for subclumps, otherwise too slow
         if (is_subsubs) {
            add_halo_in_map(par_cl, gSIM_EPS_DRAWN, j_drawn, j_continuum, rs_pix_fov, true,
                            mass_cl, halo.Subs_mfrac,
                            par_subs_dpdv, par_subsub, nsubsub_tot);
         } else {
            add_halo_in_map(par_cl, gSIM_EPS_DRAWN, j_drawn, j_continuum, rs_pix_fov, false);
         }
      }
#if IS_ROOT
      delete f_mass;
      f_mass = NULL;
      delete f_lalphabeta;
      f_lalphabeta = NULL;
#endif
      if (gSIM_IS_PRINT) fclose(fp_drawn);
   }

   /******************* CONVERT ALL ARRAYS TO CORRECT UNITS ********************/

   convert_to_PP_units(1, n_pix_fov, &j_sm[0]);
   if (is_subs) {
      convert_to_PP_units(1, n_pix_fov, &j_subs_mthresh[0]);
      if (gPP_DM_IS_ANNIHIL_OR_DECAY)
         convert_to_PP_units(1, n_pix_fov, &j_crossprod[0]);
      if (is_subs_drawn)
         convert_to_PP_units(1, n_pix_fov, &j_drawn[0]);
   }
   // with instrumental beam:
   if (smooth_gamma_or_nu_or_both != 0  && n_pix_fov != n_pix) {
      convert_to_PP_units(1, n_pix_fov_extended, &j_sm_beam[0]);
      if (is_subs) {
         convert_to_PP_units(1, n_pix_fov_extended, &j_subs_mthresh_beam[0]);
         if (gPP_DM_IS_ANNIHIL_OR_DECAY)
            convert_to_PP_units(1, n_pix_fov_extended, &j_crossprod_beam[0]);
      }
   }

   /******************* WRITE FIRST TWO EXTENSIONS TO FILE  ********************/

   cout << "" << endl;
   if (is_subs_drawn  && gSIM_IS_PRINT)
      cout << "  ... drawn clumps [ASCII] written in: " << filename_drawn_str << endl;

   // write first extension (this information will also be used for ROOT plotting):
   // first, fill all arrays:
   for (int i = 0; i < n_pix_fov ; ++i) {
      j_tot[i] = j_sm[i];
      if (is_subs) {
         if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
            j_tot[i] += j_subs_mthresh[i] + j_crossprod[i];
            if (is_subs_drawn) j_tot[i] += j_drawn[i];
         } else {
            j_tot[i] += j_subs_mthresh[i];
            if (is_subs_drawn) j_tot[i] += j_drawn[i];
         }
      }
   }

   double int_area = 2. * PI * (1 - cos(gSIM_ALPHAINT)) ;
   double jrescalefac = gSIM_HEALPIX_DELTAOMEGA / int_area;

   // rescale J-factors to pixel area:
   for (int i = 0; i < n_pix_fov; ++i) {
      if (j_sm[i] > 1.e-40) {
         j_sm[i] *= jrescalefac ;
         j_tot[i] *= jrescalefac ;
         if (is_subs) {
            j_subs_mthresh[i] *= jrescalefac ;
            if (gPP_DM_IS_ANNIHIL_OR_DECAY)
               j_crossprod[i] *= jrescalefac ;
            if (is_subs_drawn)
               j_drawn[i] *= jrescalefac ;
         }
      }
   }

   if (smooth_gamma_or_nu_or_both != 0 && n_pix_fov != n_pix) {
      for (int i = 0; i < n_pix_fov_extended; ++i) {
         if (j_sm[i] > 1.e-40) {
            j_sm_beam[i] *= jrescalefac ;
            if (is_subs) {
               j_subs_mthresh_beam[i] *= jrescalefac ;
               if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                  j_crossprod_beam[i] *= jrescalefac ;
            }
         }
      }
   }


   //--- Array declarations and initialisation for 2nd extension:
   //    All values divided by the pixel size -> Jfactor per steradian
   vector<double> j_tot_sr;
   vector<double> j_sm_sr;
   vector<double> j_subs_mthresh_sr;
   vector<double> j_crossprod_sr;
   vector<double> j_drawn_sr;

   if (gSIM_IS_PRINT) {

      // fill arrays for second extension only in case of gSIM_IS_PRINT:
      j_tot_sr.assign(n_pix_fov, -1.e-40);
      j_sm_sr.assign(n_pix_fov, -1.e-40);

      // Create additional arrays only if required
      if (is_subs) {
         j_subs_mthresh_sr.assign(n_pix_fov, -1.e-40);
         if (gPP_DM_IS_ANNIHIL_OR_DECAY)
            j_crossprod_sr.assign(n_pix_fov, -1.e-40);
         if (is_subs_drawn)
            j_drawn_sr.assign(n_pix_fov, -1.e-40);
      }

      for (int i = 0; i < n_pix_fov; ++i) {
         j_sm_sr[i] = j_sm[i] / gSIM_HEALPIX_DELTAOMEGA ;
         j_tot_sr[i] = j_tot[i] / gSIM_HEALPIX_DELTAOMEGA ;
         if (is_subs) {
            j_subs_mthresh_sr[i] = j_subs_mthresh[i] / gSIM_HEALPIX_DELTAOMEGA ;
            if (gPP_DM_IS_ANNIHIL_OR_DECAY)
               j_crossprod_sr[i] = j_crossprod[i] / gSIM_HEALPIX_DELTAOMEGA ;
            if (is_subs_drawn)
               j_drawn_sr[i] = j_drawn[i] / gSIM_HEALPIX_DELTAOMEGA ;
         }
      }
   }

   // set all negative values in 1st extension to positive 1e-40 (alternatively, to healpix blind value)
   for (int i = 0; i < n_pix_fov; ++i) {
      if (j_tot[i] <= 3.e-40) j_tot[i] = 1.e-40;
      if (j_sm[i] < 1e-40) j_sm[i] = 1.e-40;
      if (is_subs && j_subs_mthresh[i] < 1e-40) j_subs_mthresh[i] = 1e-40;
      if (is_subs && gPP_DM_IS_ANNIHIL_OR_DECAY && j_crossprod[i] < 1e-40) j_crossprod[i] = 1e-40;
      if (is_subs && is_subs_drawn && j_drawn[i] < 1e-40) j_drawn[i] = 1e-40;
   }

   // set correct units for all columns:
   string j_units;
   string j_units_per_sr;
   if (gPP_DM_IS_ANNIHIL_OR_DECAY and gSIM_IS_ASTRO_OR_PP_UNITS) {
      j_units = "Msun^2 kpc^-5";
      j_units_per_sr = "Msun^2 kpc^-5 sr^-1";
   } else if (gPP_DM_IS_ANNIHIL_OR_DECAY and !gSIM_IS_ASTRO_OR_PP_UNITS) {
      j_units = "GeV^2 cm^-5";
      j_units_per_sr = "GeV^2 cm^-5 sr^-1";
   } else if (!gPP_DM_IS_ANNIHIL_OR_DECAY and gSIM_IS_ASTRO_OR_PP_UNITS) {
      j_units = "Msun kpc^-2";
      j_units_per_sr = "Msun kpc^-2 sr^-1";
   } else if (!gPP_DM_IS_ANNIHIL_OR_DECAY and !gSIM_IS_ASTRO_OR_PP_UNITS) {
      j_units = "GeV cm^-2";
      j_units_per_sr = "GeV cm^-2 sr^-1";
   } else {
      j_units = "Unknown (some error)";
      j_units_per_sr = "Unknown (some error)";
   }

   if (gSIM_IS_PRINT) {

      // set all negative values in 2nd extension to positive 1e-40 (alternatively, to healpix blind value)
      for (int i = 0; i < n_pix_fov; ++i) {
         if (j_tot_sr[i] < 1.e-30) j_tot_sr[i] = 1e-40;
         if (j_sm_sr[i] < 1.e-30) j_sm_sr[i] = 1e-40;
         if (is_subs && j_subs_mthresh_sr[i] < 1.e-30) j_subs_mthresh_sr[i] = 1e-40;
         if (is_subs && gPP_DM_IS_ANNIHIL_OR_DECAY && j_crossprod_sr[i] < 1.e-30) j_crossprod_sr[i] = 1e-40;
         if (is_subs && is_subs_drawn && j_drawn_sr[i] < 1.e-30) j_drawn_sr[i] = 1e-40;;
      }

      // secondly, write arrays into fits file according to the performed simulation:
      if (!is_subs) {
         fits_write_clumpymaps(fh_file_out, v_pix_fov, j_sm) ;
         if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
            fh_file_out.set_key("TTYPE2", string("Jtot"),    "label for field 2");
            fh_file_out.set_key("EXTNAME", string("JFACTOR"), "name of this binary table extension");
         } else {
            fh_file_out.set_key("TTYPE2", string("Dtot"),    "label for field 2");
            fh_file_out.set_key("EXTNAME", string("DFACTOR"), "name of this binary table extension");
         }
      } else {
         if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
            if (!is_subs_drawn) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot, j_sm, j_subs_mthresh, j_crossprod);
               fh_file_out.set_key("TTYPE2", string("Jtot"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jsmooth"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Jsub"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Jcrossp"), "label for field 5");

            } else {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot, j_sm, j_subs_mthresh, j_crossprod, j_drawn);
               fh_file_out.set_key("TTYPE2", string("Jtot"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jsmooth"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Jsub"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Jcrossp"), "label for field 5");
               fh_file_out.set_key("TTYPE6", string("Jdrawn"),  "label for field 6");
            }
            fh_file_out.set_key("EXTNAME", string("JFACTOR"), "name of this binary table extension");
         } else {
            if (!is_subs_drawn) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot, j_sm, j_subs_mthresh);
               fh_file_out.set_key("TTYPE2", string("Dtot"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dsmooth"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Dsub"),    "label for field 4");

            } else {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot, j_sm, j_subs_mthresh, j_drawn);
               fh_file_out.set_key("TTYPE2", string("Dtot"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dsmooth"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Dsub"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Ddrawn"),  "label for field 5");
            }
            fh_file_out.set_key("EXTNAME", string("DFACTOR"), "name of this binary table extension");
         }
      }
      // set correct units:
      // get number of columns finally written:
      int ncolumns = fh_file_out.ncols();
      for (int i = 2; i < ncolumns + 1; ++i) {
         sprintf(char_tmp, "TUNIT%d", i);
         fh_file_out.set_key(char_tmp, j_units, "physical unit of field");
      }

      // write second extension: All values divided by the pixel size -> Jfactor per steradian
      // write arrays into fits file according to the performed simulation:
      if (!is_subs) {
         fits_write_clumpymaps(fh_file_out, v_pix_fov, j_sm_sr) ;
         if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
            fh_file_out.set_key("TTYPE2", string("Jtot_per_sr"),    "label for field 2");
            fh_file_out.set_key("EXTNAME", string("JFACTOR_PER_SR"), "name of this binary table extension");
         } else {
            fh_file_out.set_key("TTYPE2", string("Dtot_per_sr"),    "label for field 2");
            fh_file_out.set_key("EXTNAME", string("DFACTOR_PER_SR"), "name of this binary table extension");
         }
      } else {
         if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
            if (!is_subs_drawn) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_sr, j_sm_sr, j_subs_mthresh_sr, j_crossprod_sr) ;
               fh_file_out.set_key("TTYPE2", string("Jtot_per_sr"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jsmooth_per_sr"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Jsub_per_sr"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Jcrossp_per_sr"), "label for field 5");
            } else {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_sr, j_sm_sr, j_subs_mthresh_sr, j_crossprod_sr, j_drawn_sr) ;
               fh_file_out.set_key("TTYPE2", string("Jtot_per_sr"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jsmooth_per_sr"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Jsub_per_sr"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Jcrossp_per_sr"), "label for field 5");
               fh_file_out.set_key("TTYPE6", string("Jdrawn_per_sr"),  "label for field 6");
            }
            fh_file_out.set_key("EXTNAME", string("JFACTOR_PER_SR"), "name of this binary table extension");
         } else {
            if (!is_subs_drawn) {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_sr, j_sm_sr, j_subs_mthresh_sr) ;
               fh_file_out.set_key("TTYPE2", string("Dtot_per_sr"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dsmooth_per_sr"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Dsub_per_sr"),    "label for field 4");
            } else {
               fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_sr, j_sm_sr, j_subs_mthresh_sr, j_drawn_sr) ;
               fh_file_out.set_key("TTYPE2", string("Dtot_per_sr"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dsmooth_per_sr"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Dsub_per_sr"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Ddrawn_per_sr"),  "label for field 5");
            }
            fh_file_out.set_key("EXTNAME", string("DFACTOR_PER_SR"), "name of this binary table extension");
         }
      }
      // set correct units for J per sr extension:
      for (int i = 2; i < ncolumns + 1; ++i) {
         sprintf(char_tmp, "TUNIT%d", i);
         fh_file_out.set_key(char_tmp, j_units_per_sr, "physical unit of field");
      }

      // clear memory:
      vector<double>().swap(j_tot_sr);
      vector<double>().swap(j_sm_sr);
      if (is_subs) {
         vector<double>().swap(j_subs_mthresh_sr);
         if (gPP_DM_IS_ANNIHIL_OR_DECAY)
            vector<double>().swap(j_crossprod_sr);
         if (is_subs_drawn)
            vector<double>().swap(j_drawn_sr);
      }
   }

   /******************* SMOOTH MAPS IF BEAM WIDTHS ARE GIVEN ******************/

   vector<double> j_tot_gammasmoothed ;
   vector<double> j_tot_neutrinosmoothed ;
   vector<double> j_tot_gammasmoothed_sr ;
   vector<double> j_tot_neutrinosmoothed_sr ;

   if (smooth_gamma_or_nu_or_both != 0) {
      // write Healpix map and output whole sky fits file:
      arr<double> arrhp_fullsky(n_pix, 0.);  // this takes a lot of memory...
      T_Healpix_Base<int> hp_gridprop_fullsky;
      hp_gridprop_fullsky.SetNside(gSIM_HEALPIX_NSIDE, RING);

      if (n_pix_fov != n_pix) {
         j_tot_beam.assign(n_pix_fov_extended, 1.e-40);
         // stack up smooth contributions on extended grid:
         for (int i = 0; i < n_pix_fov_extended; ++i) {
            j_tot_beam[i] = j_sm_beam[i] ;
            if (is_subs) {
               j_tot_beam[i] += j_subs_mthresh_beam[i];
               if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                  j_tot_beam[i] += j_crossprod_beam[i];
            }
         }
         // fill drawn clumps and/or from list onto smooth contributions on extended grid:
         for (int i = 0; i < n_pix_fov; ++i) {
            int hp_index = v_pix_fov[i] ;
            int i_extended = binary_search(v_pix_fov_extended, hp_index);
            if (is_subs_drawn) j_tot_beam[i_extended] += j_drawn[i] ;
         }
         // write everything into a full-sky map:
         for (int i_extended = 0; i_extended < n_pix_fov_extended; ++i_extended) {
            if (gSIM_HEALPIX_SCHEME == NEST) {
               arrhp_fullsky[hp_gridprop_fullsky.nest2ring(v_pix_fov_extended[i_extended])] = j_tot_beam[i_extended];
               //cout << j_tot_beam[i]<< endl;
            } else arrhp_fullsky[v_pix_fov_extended[i_extended]] = j_tot_beam[i_extended];
         }
      } else {
         // for the fullsky, in the RING scheme, j_tot is already identical to arrhp_fullsky.
         // so switch only from NESTED to RING scheme and from double to single precision.
         for (int i = 0; i < n_pix_fov; ++i) {
            if (gSIM_HEALPIX_SCHEME == NEST) arrhp_fullsky[hp_gridprop_fullsky.nest2ring(v_pix_fov[i])] = j_tot[i];
            else arrhp_fullsky[i] = j_tot[i];
         }
      }

      string weights_dir = gSIM_HEALPIX_RING_WEIGHTS_DIR ;
      string powspecfile_str = "" ;
      if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3) {

         Healpix_Map<double> hp_map_gamma(arrhp_fullsky, RING);

         if (std::isinf(hp_map_gamma.average()) || isnan(float(hp_map_gamma.average()))) {
            printf("\n====> ERROR: halo_j2D() in janalysis.cc");
            printf("\n             Map for smoothing contains NaN values..");
            printf("\n             => abort()\n\n");
            abort();
         }

         cout << "\n>>>>> now smooth total " << j_or_d << "-factor skymap with instrumental beam of Gamma-ray telescope ..." << endl;

         j_tot_gammasmoothed.assign(n_pix_fov, 1.e-40);

         hp_smooth_map(hp_map_gamma, gSIM_GAUSSBEAM_GAMMA_FWHM, weights_dir) ;

         // Now, cut off the edges of the extended grid and write original FOV to file:
         for (int i = 0; i < n_pix_fov; ++i) {
            if (gSIM_HEALPIX_SCHEME == NEST) j_tot_gammasmoothed[i] = hp_map_gamma[hp_gridprop_fullsky.nest2ring(v_pix_fov[i])];
            else j_tot_gammasmoothed[i] = hp_map_gamma[v_pix_fov[i]];
            // remove artefacts:
            if (j_tot_gammasmoothed[i] < SMALL_NUMBER) j_tot_gammasmoothed[i] = 1e-40;
         }
      }

      if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3) {

         Healpix_Map<double> hp_map_neutrino(arrhp_fullsky, RING);
         cout << "\n>>>>> now smooth total " << j_or_d << "-factor skymap with instrumental beam of neutrino telescope ..." << endl;

         j_tot_neutrinosmoothed.assign(n_pix_fov, 1.e-40);

         hp_smooth_map(hp_map_neutrino, gSIM_GAUSSBEAM_NEUTRINO_FWHM, weights_dir) ;

         // Now, cut off the edges of the extended grid and write original FOV to file:
         for (int i = 0; i < n_pix_fov; ++i) {
            if (gSIM_HEALPIX_SCHEME == NEST) j_tot_neutrinosmoothed[i] = hp_map_neutrino[hp_gridprop_fullsky.nest2ring(v_pix_fov[i])];
            else j_tot_neutrinosmoothed[i] = hp_map_neutrino[v_pix_fov[i]];
            if (j_tot_neutrinosmoothed[i] < SMALL_NUMBER) j_tot_neutrinosmoothed[i] = 1e-40;
         }
      }


      // deallocate memory:
      arr<double>().swap(arrhp_fullsky); // deallocate memory
      // hp_map object?

      if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3) {
         j_tot_gammasmoothed_sr.assign(n_pix_fov, 1.e-40);
      }
      if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3) {
         j_tot_neutrinosmoothed_sr.assign(n_pix_fov, 1.e-40);
      }

      for (int i = 0; i < n_pix_fov; ++i) {
         if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3)
            j_tot_gammasmoothed_sr[i] = j_tot_gammasmoothed[i] / gSIM_HEALPIX_DELTAOMEGA ;
         if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3)
            j_tot_neutrinosmoothed_sr[i] = j_tot_neutrinosmoothed[i] / gSIM_HEALPIX_DELTAOMEGA ;
      }

      // write to fits file:
      cout << "" << endl;
      if (gSIM_IS_PRINT) {
         if (smooth_gamma_or_nu_or_both == 1) {
            fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_gammasmoothed, j_tot_gammasmoothed_sr) ;
            if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
               fh_file_out.set_key("TTYPE2", string("Jtot_gamma"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jtot_gamma_per_sr"), "label for field 3");
            } else {
               fh_file_out.set_key("TTYPE2", string("Dtot_gamma"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dtot_gamma_per_sr"), "label for field 3");

            }
         } else if (smooth_gamma_or_nu_or_both == 2) {
            fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_neutrinosmoothed, j_tot_neutrinosmoothed_sr) ;
            if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
               fh_file_out.set_key("TTYPE2", string("Jtot_neutrino"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jtot_neutrino_per_sr"), "label for field 3");
            } else {
               fh_file_out.set_key("TTYPE2", string("Dtot_neutrino"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dtot_neutrino_per_sr"), "label for field 3");
            }
            // set correct units for J per sr extension:
            fh_file_out.set_key("TUNIT2", j_units, "physical unit of field");
            fh_file_out.set_key("TUNIT3", j_units_per_sr, "physical unit of field");
         } else if (smooth_gamma_or_nu_or_both == 3) {
            fits_write_clumpymaps(fh_file_out, v_pix_fov, j_tot_gammasmoothed, j_tot_gammasmoothed_sr, j_tot_neutrinosmoothed, j_tot_neutrinosmoothed_sr) ;
            if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
               fh_file_out.set_key("TTYPE2", string("Jtot_gamma"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Jtot_gamma_per_sr"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Jtot_neutrino"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Jtotneutrino_per_sr"), "label for field 5");
            } else {
               fh_file_out.set_key("TTYPE2", string("Dtot_gamma"),    "label for field 2");
               fh_file_out.set_key("TTYPE3", string("Dtot_gamma_per_sr"), "label for field 3");
               fh_file_out.set_key("TTYPE4", string("Dtot_neutrino"),    "label for field 4");
               fh_file_out.set_key("TTYPE5", string("Dtot_neutrino_per_sr"), "label for field 5");
            }
            // set correct units for J per sr extension:
            fh_file_out.set_key("TUNIT2", j_units, "physical unit of field");
            fh_file_out.set_key("TUNIT3", j_units_per_sr, "physical unit of field");
            fh_file_out.set_key("TUNIT4", j_units, "physical unit of field");
            fh_file_out.set_key("TUNIT5", j_units_per_sr, "physical unit of field");
         }
         // set correct extension name:
         if (gPP_DM_IS_ANNIHIL_OR_DECAY) fh_file_out.set_key("EXTNAME", string("JTOT_SMOOTHED"), "name of this binary table extension");
         else fh_file_out.set_key("EXTNAME", string("DTOT_SMOOTHED"), "name of this binary table extension");
      }
   }

   // delete values per sr arrays from third extension, as they are not needed anymore
   if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3)
      j_tot_gammasmoothed_sr.clear();
   if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3)
      j_tot_neutrinosmoothed_sr.clear();

   /******************* CALCULATE FLUX- AND INTENSITY MAPS ********************/

   // Is flux?
   bool is_flux = true;
   if (gPP_DM_MASS_GEV < gSIM_FLUX_AT_E_GEV) {
      is_flux = false;
      print_warning("janalysis.cc", "halo_j2D()", "No fluxes are calculated because pivot energy is bigger than DM particle mass.");
   }

   if (gSIM_IS_WRITE_FLUXMAPS && is_flux) {
      cout << "\n>>>>> Calculate and write flux and intensity map(s) for given particle physics model..." << endl;

      if (smooth_gamma_or_nu_or_both == 0) {
         fits_write_fluxmaps(v_pix_fov, j_tot, fh_file_out);
      } else if (smooth_gamma_or_nu_or_both == 1) {
         fits_write_fluxmaps(v_pix_fov, j_tot, j_tot_gammasmoothed, smooth_gamma_or_nu_or_both, fh_file_out);
      } else if (smooth_gamma_or_nu_or_both == 2) {
         fits_write_fluxmaps(v_pix_fov, j_tot, j_tot_neutrinosmoothed, smooth_gamma_or_nu_or_both, fh_file_out);
      } else if (smooth_gamma_or_nu_or_both == 3) {
         fits_write_fluxmaps(v_pix_fov, j_tot, j_tot_gammasmoothed, j_tot_neutrinosmoothed, smooth_gamma_or_nu_or_both, fh_file_out);
      }
   }

   /************************ CLOSING OUTPUT FILE WRITING **********************/

   if (gSIM_IS_PRINT) {

      // delete flux related keywords from first two extensions:
      if (gSIM_IS_WRITE_FLUXMAPS) {
         vector<string> keylist;
         for (int k = 2; k < 4; ++k) { // loop through first two data extensions
            fh_file_out.goto_hdu(k);
            fh_file_out.get_all_keys(keylist);
            int delete_flux_keys[] = {kPP_DM_ANNIHIL_DELTA, kPP_DM_ANNIHIL_SIGMAV_CM3PERS, kPP_DM_DECAY_LIFETIME_S,
                                      kSIM_FLUX_IS_INTEG_OR_DIFF, kSIM_FLUX_FLAG_NUFLAVOUR, kSIM_FLUX_FLAG_FINALSTATE,
                                      kPP_DM_MASS_GEV, kPP_FLAG_SPECTRUMMODEL,
                                      kSIM_FLUX_EMIN_GEV, kSIM_FLUX_EMAX_GEV, kSIM_FLUX_AT_E_GEV,
                                      kPP_NUMIXING_THETA12_DEG, kPP_NUMIXING_THETA13_DEG, kPP_NUMIXING_THETA23_DEG,
                                     };
            for (int i = 0; i < int(sizeof(delete_flux_keys) / sizeof(delete_flux_keys[0])); ++i) {
               if (find(keylist.begin(), keylist.end(), gSIM_INPUTPARAMS[delete_flux_keys[i]][kSIM_INPUTPARAM_FITSNAME]) != keylist.end()) {
                  fh_file_out.delete_key(gSIM_INPUTPARAMS[delete_flux_keys[i]][kSIM_INPUTPARAM_FITSNAME]);
               }
            }
            for (int i = 0; i < (int)keylist.size(); ++i) { // delete branching ratios
               if (keylist[i].substr(0, 2) == "BR") {
                  fh_file_out.delete_key(keylist[i]);
               }
            }
            fh_file_out.set_key(gSIM_INPUTPARAMS[kSIM_IS_WRITE_FLUXMAPS][kSIM_INPUTPARAM_FITSNAME], false, gSIM_INPUTPARAMS[kSIM_IS_WRITE_FLUXMAPS][kSIM_INPUTPARAM_DESCRIPTION]);
         }
      }

      fh_file_out.close();
      cout << "_______________________" << endl << endl;
      printf("      Output files successfully written.\n\n") ;
      printf("      We recommend Aladin (http://aladin.u-strasbg.fr/) for visualizing and\n") ;
      printf("      browsing the FITS file. Alternative, less optimised but more widely \n") ;
      printf("      readable output formats can be generated via the ./bin/clumpy -o options.\n") ;
      if (gSIM_HEALPIX_NSIDE % 2 != 0) {
         print_warning("janalysis.cc", "halo_j2D()", "Some FITS viewers won't work for odd NSIDE, as chosen in this simulation.");
      }
      printf("\n") ;

      cout << "_______________________" << endl << endl;
   }


   /***************************  ROOT QUICK LOOK  *****************************/
#if IS_ROOT
   if (gSIM_IS_DISPLAY || gSIM_IS_WRITE_ROOTFILES) {
      printf("    * ROOT displays and/or output\n");


      // #graph = smooth, <sub>, drawn (optional), tot
      TCanvas *c_halocomp = NULL;
      TCanvas *c_halotot = NULL;
      TCanvas *c_halotot_smoothed_gamma = NULL;
      TCanvas *c_halotot_smoothed_neutrino = NULL;
      TH2D **h2d = NULL;
      THStack *h_stack = NULL;

      // Save .root format?
      TFile *root_file = NULL;
      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file = new TFile(filename_root_str.c_str(), "recreate");
         root_file->cd();
      }

      //--- Set binning:
      double delta_psi = sqrt(delta_omega) ;
      double delta_theta = sqrt(delta_omega) ;

      int n_psi ;
      int n_theta ;
      if (gSIM_HEALPIX_NSIDE % 2 == 0) {
         // force n_psi, n_theta to be even, as it fits better to the actual Healpix grid data
         // (for even NSIDE, no pixel with center at (0,0))
         n_psi = 2 * int(ceil(fov_diam / delta_psi) / 2.) + 2; // # of bins in psi, force to be even
         n_theta = 2 * int(ceil(fov_diam / delta_theta) / 2.) + 2 ; // # of bins in theta, force to be even
      } else {
         n_psi = 2 * int(ceil(fov_diam / delta_psi) / 2.) + 1; // # of bins in psi, force to be odd
         n_theta = 2 * int(ceil(fov_diam / delta_theta) / 2.) + 1 ;
      }
      delta_theta = fov_diam / (n_theta - 1) ; // correct delta_theta, accounted for the forced n_theta
      delta_psi = fov_diam / (n_psi - 1) ;

      //--- Array declarations and initialisation for rectangular ROOT plots
      vector<double> j_tot_plot(n_psi * n_theta, 1.e-40);
      vector<double> j_sm_plot(n_psi * n_theta, 1.e-40);
      vector<double> j_subs_mthresh_plot;
      vector<double> j_crossprod_plot;
      vector<double> j_drawn_plot;

      vector<double> j_tot_gammasmoothed_plot ;
      vector<double> j_tot_neutrinosmoothed_plot ;
      if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3)
         j_tot_gammasmoothed_plot.assign(n_psi * n_theta, 1.e-40);
      if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3)
         j_tot_neutrinosmoothed_plot.assign(n_psi * n_theta, 1.e-40);
      vector<double> psi_tab;
      vector<double> theta_tab;


      // Create Jdrawn for rectangular ROOT plot if required
      if (is_subs) {
         j_subs_mthresh_plot.assign(n_psi * n_theta, 1.e-40);
         if (gPP_DM_IS_ANNIHIL_OR_DECAY)
            j_crossprod_plot.assign(n_psi * n_theta, 1.e-40);
         if (is_subs_drawn)
            j_drawn_plot.assign(n_psi * n_theta, 1.e-40);
      }

      // find plot ranges:
      double j_tot_min = 1.e40;
      for (int i = 0; i < n_pix_fov; ++i) {
         if (j_tot[i]  < j_tot_min && j_tot[i] > 1e-2) {
            j_tot_min = j_tot[i] ;
         }
      }
      double j_sm_min = 1.e40;
      for (int i = 0; i < n_pix_fov; ++i) {
         if (j_sm[i]  < j_sm_min && j_sm[i] > 1e-2) {
            j_sm_min = j_sm[i] ;
         }
      }

      // fill arrays for plotting:
      for (int j = 0; j < n_theta; ++j)
         theta_tab.push_back(theta_min + j * delta_theta);
      for (int i = 0; i < n_psi; ++i) {
         psi_tab.push_back(psi_min + i * delta_psi);
         for (int j = 0; j < n_theta; ++j) {
            pointing ptg_angles_plot(PI / 2 - theta_min - j * delta_theta, psi_min + i * delta_psi) ;
            // find the pixel to this angle:
            int i_pix = hp_gridprop.ang2pix(ptg_angles_plot) ;
            // if Healpix map contains a value at this point, fill it in the plotting map:
            if (rs_pix_fov.contains(i_pix)) {
               int index_i_pix = binary_search(v_pix_fov, i_pix);
               j_sm_plot[i * n_theta + j] = j_sm[index_i_pix] ;
               j_tot_plot[i * n_theta + j] = j_tot[index_i_pix]  ;
               if (is_subs) {

                  j_subs_mthresh_plot[i * n_theta + j] = j_subs_mthresh[index_i_pix] ;
                  if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                     j_crossprod_plot[i * n_theta + j] = j_crossprod[index_i_pix] ;
                  if (is_subs_drawn)
                     j_drawn_plot[i * n_theta + j] = j_drawn[index_i_pix] ;
               }

               if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3)
                  j_tot_gammasmoothed_plot[i * n_theta + j] = j_tot_gammasmoothed[index_i_pix] ;
               if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3)
                  j_tot_neutrinosmoothed_plot[i * n_theta + j] = j_tot_neutrinosmoothed[index_i_pix] ;
            }

            // else, fill with 1.e-40 to avoid crash of display in log
            else {
               j_sm_plot[i * n_theta + j] = 1.e-40;
               j_tot_plot[i * n_theta + j] = 1.e-40;
               if (is_subs) {
                  j_subs_mthresh_plot[i * n_theta + j] = 1.e-40;
                  if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                     j_crossprod_plot[i * n_theta + j] = 1.e-40;
                  if (is_subs_drawn)
                     j_drawn_plot[i * n_theta + j] = 1.e-40;
               }
               if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3)
                  j_tot_gammasmoothed_plot[i * n_theta + j] = 1.e-40;
               if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3)
                  j_tot_neutrinosmoothed_plot[i * n_theta + j] = 1.e-40;
            }
         }
      }


      vector<string> gr_name;
      vector<int> colors;
      int n_g = 2;
      gr_name.push_back("gr_tot");
      colors.push_back(kBlue);
      gr_name.push_back("gr_sm");
      colors.push_back(kBlue);
      if (is_subs) {
         if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
            gr_name.push_back("gr_crossprod");
            colors.push_back(kGreen + 1);
            ++n_g;
         }
         gr_name.push_back("gr_mean");
         colors.push_back(kRed);
         ++n_g;

         if (is_subs_drawn) {
            gr_name.push_back("gr_drawn");
            colors.push_back(kYellow);
            ++n_g;
         }
      }

      if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3) {
         gr_name.push_back("gr_gaussbeam_gamma");
         colors.push_back(kBlue);
         ++n_g;
      }

      if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3) {
         gr_name.push_back("gr_gaussbeam_neutrino");
         colors.push_back(kBlue);
         ++n_g;
      }

      h2d = new TH2D*[n_g];

      // Create and fill 2D histos
      double angle_max_deg = fov_diam / 2. * RAD_to_DEG;
      for (int g = 0; g < n_g; ++g) {
         h2d[g] = new TH2D(gr_name[g].c_str(), "",
                           n_psi, -angle_max_deg, angle_max_deg,
                           n_theta, -angle_max_deg, angle_max_deg);
      }

      for (int j = 0; j < n_psi ; ++j) {
         for (int k = 0; k < n_theta ; ++k) {
            h2d[0]->SetBinContent(j + 1, k + 1, j_tot_plot[j * n_theta + k]);
            h2d[1]->SetBinContent(j + 1, k + 1, j_sm_plot[j * n_theta + k]);

            if (smooth_gamma_or_nu_or_both == 1) {
               h2d[n_g - 1]->SetBinContent(j + 1, k + 1, j_tot_gammasmoothed_plot[j * n_theta + k]);
            } else if (smooth_gamma_or_nu_or_both == 2) {
               h2d[n_g - 1]->SetBinContent(j + 1, k + 1, j_tot_neutrinosmoothed_plot[j * n_theta + k]);
            } else if (smooth_gamma_or_nu_or_both == 3) {
               h2d[n_g - 2]->SetBinContent(j + 1, k + 1, j_tot_gammasmoothed_plot[j * n_theta + k]);
               h2d[n_g - 1]->SetBinContent(j + 1, k + 1, j_tot_neutrinosmoothed_plot[j * n_theta + k]);
            }

            // Fill histo for all other components
            if (is_subs) {
               if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
                  h2d[2]->SetBinContent(j + 1, k + 1, j_crossprod_plot[j * n_theta + k]);
                  h2d[3]->SetBinContent(j + 1, k + 1, j_subs_mthresh_plot[j * n_theta + k]);
                  if (is_subs_drawn)
                     h2d[4]->SetBinContent(j + 1, k + 1, j_drawn_plot[j * n_theta + k]);
               } else {
                  h2d[2]->SetBinContent(j + 1, k + 1, j_subs_mthresh_plot[j * n_theta + k]);
                  if (is_subs_drawn)
                     h2d[3]->SetBinContent(j + 1, k + 1, j_drawn_plot[j * n_theta + k]);
               }
            }
         }
      }

      // Create and fill 2D stack
      char integr[200];
      sprintf(integr, "%s (#Delta#Omega=%.2le sr)", (halo.Name).c_str(), gSIM_HEALPIX_DELTAOMEGA);
      string histo_legend;
      if (!is_subs) {
         if (gPP_DM_IS_ANNIHIL_OR_DECAY)
            histo_legend = string(integr) + ": J_{sm} (blue)";
         else
            histo_legend = string(integr) + ": D_{sm} (blue)";
      } else {
         if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
            if (is_subs_drawn)
               histo_legend = string(integr) + ": J_{sm} (blue) + J_{cross-prod} (green) + J_{<sub>} (red) + J_{drawn} (yellow)";
            else
               histo_legend = string(integr) + ": J_{sm} (blue) + J_{cross-prod} (green) + J_{<sub>} (red)";
         } else {
            if (is_subs_drawn)
               histo_legend = string(integr) + ": D_{sm} (blue) + D_{<sub>} (red) + D_{drawn} (yellow)";
            else
               histo_legend = string(integr) + ": D_{sm} (blue) + D_{<sub>} (red)";
         }
      }


      // If too many pixels, skip plot of stack to save time (anyway, details not visile in that case).
      bool is_stack = true;
      if (n_pix_fov > 50000) {
         is_stack = false;
         cout << "      N.B.: n_pix_fov=" << n_pix_fov << "(>50000) => do not plot stacked contributions (time consuming)" << endl;
      }
      h_stack = new THStack("h_stack", "h_stack");

      string  z_axis_name;
      if (gPP_DM_IS_ANNIHIL_OR_DECAY && gSIM_IS_ASTRO_OR_PP_UNITS)
         z_axis_name = "J_{halo} [M_{#odot}^{2} kpc^{-5}]";
      else if (!gPP_DM_IS_ANNIHIL_OR_DECAY && gSIM_IS_ASTRO_OR_PP_UNITS)
         z_axis_name = "D_{halo} [M_{#odot} kpc^{-2}]";
      else if (gPP_DM_IS_ANNIHIL_OR_DECAY && !gSIM_IS_ASTRO_OR_PP_UNITS)
         z_axis_name = "J_{halo} [GeV^{2} cm^{-5}]";
      else if (!gPP_DM_IS_ANNIHIL_OR_DECAY && !gSIM_IS_ASTRO_OR_PP_UNITS)
         z_axis_name = "D_{halo} [GeV cm^{-2}]";

      for (int g = 0; g < n_g; ++g) {
         if (g == 0 || (smooth_gamma_or_nu_or_both == 3 && g >= n_g - 2)
               || ((smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 2) && g >= n_g - 1)) {
            h2d[g]->GetXaxis()->CenterTitle(1);
            h2d[g]->GetYaxis()->CenterTitle(1);
            h2d[g]->GetZaxis()->CenterTitle(1);
            h2d[g]->GetXaxis()->SetTitle("#psi_{halo} [deg]");
            h2d[g]->GetYaxis()->SetTitle("#theta_{halo} [deg]");
            h2d[g]->GetZaxis()->SetTitle(z_axis_name.c_str());
            h2d[g]->GetXaxis()->SetTitleOffset(1.5);
            h2d[g]->GetXaxis()->SetTitleFont(132);
            h2d[g]->GetXaxis()->SetTitleSize(0.05);
            h2d[g]->GetXaxis()->SetLabelFont(132);
            h2d[g]->GetXaxis()->SetLabelSize(0.04);
            h2d[g]->GetYaxis()->SetTitleOffset(1.9);
            h2d[g]->GetYaxis()->SetTitleFont(132);
            h2d[g]->GetYaxis()->SetTitleSize(0.05);
            h2d[g]->GetYaxis()->SetLabelFont(132);
            h2d[g]->GetYaxis()->SetLabelSize(0.04);
            h2d[g]->GetZaxis()->SetTitleOffset(0.95);
            h2d[g]->GetZaxis()->SetTitleFont(132);
            h2d[g]->GetZaxis()->SetTitleSize(0.05);
            h2d[g]->GetZaxis()->SetLabelFont(132);
            h2d[g]->GetZaxis()->SetLabelSize(0.05);
            h2d[g]->SetStats(0);
         } else  {
            h2d[g]->SetFillColor(colors[g]);
            if (is_stack) {
               if (smooth_gamma_or_nu_or_both == 3 && g < n_g - 2)
                  h_stack->Add(h2d[g]);
               if ((smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 2) && g < n_g - 1)
                  h_stack->Add(h2d[g]);
               if (smooth_gamma_or_nu_or_both == 0)
                  h_stack->Add(h2d[g]);
            }
         }
      }

      if (is_stack) {
         if (is_subs)
            h_stack->SetMinimum(j_sm_min / 2.);
         else
            h_stack->SetMinimum(j_sm_min);
         h_stack->SetMaximum(h2d[0]->GetMaximum());
      }
      h2d[0]->SetMinimum(j_tot_min);
      h2d[0]->SetMaximum(h2d[0]->GetMaximum());
      if (smooth_gamma_or_nu_or_both != 0) {
         h2d[n_g - 1]->SetMinimum(j_tot_min);
         h2d[n_g - 1]->SetMaximum(h2d[0]->GetMaximum()); // same scale as unsmoothed
      }
      if (smooth_gamma_or_nu_or_both == 3) {
         h2d[n_g - 2]->SetMinimum(j_tot_min);
         h2d[n_g - 2]->SetMaximum(h2d[0]->GetMaximum()); // same scale as unsmoothed
      }

      if (gSIM_IS_WRITE_ROOTFILES) {
         for (int g = 0; g < n_g; ++g) h2d[g]->Write();
      }

      const char *title;
      if (smooth_gamma_or_nu_or_both != 0) title = "c_halocomp (unsmoothed)";
      else title = "c_halocomp";
      if (is_stack) {
         c_halocomp = new TCanvas("c_halocomp", title, 0, 0, 650, 500);
         gStyle->SetTitleY(0.96); // histos are redrawn: necessary to force title position
         c_halocomp->SetLogx(0);
         c_halocomp->SetLogy(0);
         c_halocomp->SetLogz(1);
         c_halocomp->SetGridx(0);
         c_halocomp->SetGridy(0);
         gStyle->SetPalette(1);
         h_stack->Draw();
         gSIM_CLUMPYAD->Draw();
         h_stack->SetTitle(histo_legend.c_str());
         h_stack->GetXaxis()->CenterTitle(1);
         h_stack->GetYaxis()->CenterTitle(1);
         h_stack->GetXaxis()->SetTitle("#psi_{halo} [deg]");
         h_stack->GetYaxis()->SetTitle("#theta_{halo} [deg]");
         h_stack->GetXaxis()->SetTitleOffset(1.5);
         h_stack->GetXaxis()->SetTitleFont(132);
         h_stack->GetXaxis()->SetTitleSize(0.05);
         h_stack->GetXaxis()->SetLabelFont(132);
         h_stack->GetXaxis()->SetLabelSize(0.04);
         h_stack->GetYaxis()->SetTitleOffset(1.9);
         h_stack->GetYaxis()->SetTitleFont(132);
         h_stack->GetYaxis()->SetTitleSize(0.05);
         h_stack->GetYaxis()->SetLabelFont(132);
         h_stack->GetYaxis()->SetLabelSize(0.04);
         h_stack->GetHistogram()->GetZaxis()->CenterTitle(1);
         h_stack->GetHistogram()->GetZaxis()->SetTitle(z_axis_name.c_str());
         h_stack->GetHistogram()->GetZaxis()->SetTitleOffset(0.95);
         h_stack->GetHistogram()->GetZaxis()->SetTitleFont(132);
         h_stack->GetHistogram()->GetZaxis()->SetTitleSize(0.05);
         h_stack->GetHistogram()->GetZaxis()->SetLabelFont(132);
         h_stack->GetHistogram()->GetZaxis()->SetLabelSize(0.05);
         h_stack->GetHistogram()->SetTitleFont(132);
         gPad->Update(); //to force the generation of the title
         TPaveText *h_title = (TPaveText *)gPad->GetPrimitive("title");
         //h_title->SetBorderSize(0);
         h_title->SetFillColor(0);
         c_halocomp->Modified();
         c_halocomp->Update();

         if (gSIM_IS_WRITE_ROOTFILES) c_halocomp->Write();

      }

      if (smooth_gamma_or_nu_or_both != 0) title = "c_halotot (unsmoothed)";
      else title = "c_halotot";
      if (is_stack)
         c_halotot = new TCanvas("c_halotot", "c_halotot", 500, 0, 650, 500);
      else
         c_halotot = new TCanvas("c_halotot", "c_halotot", 0, 0, 650, 500);
      c_halotot->SetLogx(0);
      c_halotot->SetLogy(0);
      c_halotot->SetLogz(1);
      c_halotot->SetGridx(0);
      c_halotot->SetGridy(0);
      h2d[0]->SetTitle(integr);
      gStyle->SetTitleY(0.96); // histos are redrawn: necessary to force title position
      h2d[0]->SetTitleFont(132);
      h2d[0]->SetTitleSize(0.02);
      h2d[0]->Draw("surf3");
      gSIM_CLUMPYAD->Draw();
      c_halotot->Modified();
      c_halotot->Update();

      if (gSIM_IS_WRITE_ROOTFILES) c_halotot->Write();

      if (smooth_gamma_or_nu_or_both == 1 || smooth_gamma_or_nu_or_both == 3) {
         c_halotot_smoothed_gamma = new TCanvas("c_halotot_smoothed_gamma", "c_halotot smoothed by Gamma resolution", 0, 500, 650, 500);
         c_halotot_smoothed_gamma->SetLogx(0);
         c_halotot_smoothed_gamma->SetLogy(0);
         c_halotot_smoothed_gamma->SetLogz(1);
         c_halotot_smoothed_gamma->SetGridx(0);
         c_halotot_smoothed_gamma->SetGridy(0);
         char smoot[200];
         sprintf(smoot, "Smoothing FWHM = %.2le [deg]", gSIM_GAUSSBEAM_GAMMA_FWHM * RAD_to_DEG);
         if (smooth_gamma_or_nu_or_both == 1) {
            h2d[n_g - 1]->SetTitle(smoot);
            h2d[n_g - 1]->SetTitleFont(132);
            h2d[n_g - 1]->SetTitleSize(0.02);
            h2d[n_g - 1]->Draw("surf3");
         } else {
            h2d[n_g - 2]->SetTitle(smoot);
            h2d[n_g - 2]->SetTitleFont(132);
            h2d[n_g - 2]->SetTitleSize(0.02);
            h2d[n_g - 2]->Draw("surf3");
         }
         gSIM_CLUMPYAD->Draw();
         c_halotot_smoothed_gamma->Modified();
         c_halotot_smoothed_gamma->Update();
         if (gSIM_IS_WRITE_ROOTFILES) c_halotot_smoothed_gamma->Write();
      }

      if (smooth_gamma_or_nu_or_both == 2 || smooth_gamma_or_nu_or_both == 3) {
         c_halotot_smoothed_neutrino = new TCanvas("c_halotot_smoothed_neutrino", "c_halotot smoothed by neutrino resolution", 650, 500, 650, 500);
         c_halotot_smoothed_neutrino->SetLogx(0);
         c_halotot_smoothed_neutrino->SetLogy(0);
         c_halotot_smoothed_neutrino->SetLogz(1);
         c_halotot_smoothed_neutrino->SetGridx(0);
         c_halotot_smoothed_neutrino->SetGridy(0);
         char smoot[200];
         sprintf(smoot, "Smoothing FWHM = %.2le [deg]", gSIM_GAUSSBEAM_NEUTRINO_FWHM * RAD_to_DEG);
         h2d[n_g - 1]->SetTitle(smoot);
         h2d[n_g - 1]->SetTitleFont(132);
         h2d[n_g - 1]->SetTitleSize(0.02);
         h2d[n_g - 1]->Draw("surf3");
         gSIM_CLUMPYAD->Draw();
         c_halotot_smoothed_neutrino->Modified();
         c_halotot_smoothed_neutrino->Update();
         if (gSIM_IS_WRITE_ROOTFILES) c_halotot_smoothed_neutrino->Write();
      }

      if (gSIM_IS_WRITE_ROOTFILES) {
         cout << " ... ROOT plots [use ROOT TBrowser] written in: " << filename_root_str << endl;
         cout << "_______________________" << endl << endl;

      }

      if (gSIM_IS_DISPLAY)
         gSIM_ROOTAPP->Run(kTRUE);

      if (root_file) {
         root_file->Close(); // closing ROOT file before display creates crash.
         delete root_file;
      }
      root_file = NULL;

      // Clean memory
      delete[] h2d;
      h2d = NULL;
      if (h_stack) delete h_stack;
      h_stack = NULL;
      delete c_halocomp;
      c_halocomp = NULL;
      delete c_halotot;
      c_halotot = NULL;
   }
#endif
   // deallocate memory
   rs_pix_fov.clear() ;
   vector<int>().swap(v_pix_fov);

   if (smooth_gamma_or_nu_or_both != 0) {
      rs_pix_fov_extended.clear() ;
      vector<int>().swap(v_pix_fov_extended); // deallocate memory
   }
}

//______________________________________________________________________________
double halo_jtot(int i, vector<struct gStructHalo> &list_halos, double const &psi_los,
                 double const &theta_los, double const &eps, bool is_halo_set_to_0_0)
{
   //--- Returns Jtot = Jsmooth + Jsubs + Jcross-prod for the i-th halo in list.
   //
   //  i              i-th element of list_halos
   //  list_halos     Vector of halos
   //  psi_los        Longitude of the line of sight [rad]
   //  theta_los      Latitude of the line of sight [rad]
   //  eps            Relative precision sought for integrations

   double jsm = 0.;
   double jsubs = 0.;
   double jcrossprod = 0.;
   return halo_jtot(i, list_halos, psi_los, theta_los, eps, is_halo_set_to_0_0, jsm, jsubs, jcrossprod);
}

//______________________________________________________________________________
double halo_jtot(int i, vector<struct gStructHalo> &list_halos,
                 double const &psi_los, double const &theta_los, double const &eps,
                 bool is_halo_set_to_0_0, double &jsm, double &jsubs, double &jcrossprod)
{
   //--- Returns Jtot = Jsmooth + Jsubs + Jcross-prod for the i-th halo in list
   //    but also the separate contributions if needed.
   //
   // INPUTS:
   //  i              i-th element
   //  list_halos     Vector of halos
   //  psi_los        Longitude of the line of sight [rad]
   //  theta_los      Latitude of the line of sight [rad]
   //  eps            Relative precision sought for integrations
   // OUTPUTS:
   //  jsm            Smooth contribution only
   //  jsubs          Sub-haloes mean contribution only (if exists)
   //  jcrossprod     Cross-product mean (if not null)


   // Set halo parameters (and triaxiality)
   const int npar1 = 10;
   double par_halotot[npar1 + 1];
   halo_set_partot(i, par_halotot, list_halos, is_halo_set_to_0_0);
   halo_set_triaxiality(i, list_halos);

   double alphaint_orig = gSIM_ALPHAINT;
   if (list_halos[i].z > ZMIN_EXTRAGAL) gSIM_ALPHAINT = atan(1. / (1 + list_halos[i].z) * MY_TAN(gSIM_ALPHAINT));

   jsm = 0.;
   jsubs = 0.;
   jcrossprod = 0.;
   // If subhalos
   if (list_halos[i].Subs_mfrac > 1.e-5) {
      const int npar_dpdv = 10;
      double par_halodpdv[npar_dpdv];
      const int npar_subs = 24;
      double par_halosubs[npar_subs];
      halo_set_pardpdv(i, par_halodpdv, list_halos, is_halo_set_to_0_0);
      halo_set_parsubs(i, par_halosubs, list_halos);
      double lhalomin = max(0., list_halos[i].l - list_halos[i].Rvir);
      double lhalomax = list_halos[i].l + list_halos[i].Rvir;

      // Add contrib. for smooth
      jsm = jsmooth_mix(list_halos[i].Mtot, par_halotot, psi_los, theta_los,
                        lhalomin, lhalomax, eps, list_halos[i].Subs_mfrac, par_halodpdv);
      // Add contrib. from subclumps
      double mhalotot_subs = list_halos[i].Mtot * list_halos[i].Subs_mfrac;
      double nhalotot_subs = nsubtot_from_msubtot(mhalotot_subs, gDM_SUBS_MMIN, list_halos[i].Subs_Mmax, &par_halosubs[8], eps);
      jsubs = jsub_continuum(nhalotot_subs, par_halodpdv, psi_los, theta_los,
                             lhalomin, lhalomax, par_halosubs, gDM_SUBS_MMIN, list_halos[i].Subs_Mmax);
      // Add cross-product
      if (gPP_DM_IS_ANNIHIL_OR_DECAY)
         jcrossprod = jcrossprod_continuum(list_halos[i].Mtot, par_halotot, psi_los, theta_los,
                                           lhalomin, lhalomax, eps, list_halos[i].Subs_mfrac, par_halodpdv);
      gSIM_ALPHAINT = alphaint_orig;
      return (jsm + jsubs + jcrossprod);
   } else {
      jsm = jsmooth(par_halotot, psi_los, theta_los, eps);
      gSIM_ALPHAINT = alphaint_orig;
      return jsm;
   }
   gSIM_ALPHAINT = alphaint_orig;
}

//______________________________________________________________________________
void halo_load_data4jeans(string const  &data_file, struct gStructJeansData &data_halo, bool is_verbose)
{
   //--- Reads a Jeans analysis data file (kinematics or surface brightness)
   //    The format must be as in data/dataSigmap.txt or data/dataLight.txt
   //    Data are stored in the gStructJeansData data_halo.
   //    N.B.: a Jeans analysis kinematic data file must be formatted as follows
   //   (see data/dataVel.txt and data/dataSigmap.txt):
   //       TypeData
   //       R[kpc]  dR[kpc]  V[km/s_or_km^2/s^2] dV[km/s_or_km^2/s^2] (if use of Vel TypeData, you can add MembershipProba, RA[deg], Dec[deg])
   //    N.B.: a Jeans analysis surface brightness data file must be formatted
   //    as follows (see data/dataLight.txt):
   //      TypeData
   //      R[kpc]  dR[kpc]  I[L/kpc^2] dI[L/kpc^2]
   //
   // INPUTS:
   //  data_file     File to be read
   //  data_halo     gStructJeansData structure to store the data
   //  is_verbose    Chatter or not...
   // OUTPUT:
   //  data_halo     gStructJeansData structure to store the data


   if (is_verbose) {
      cout << ">>>>> Read and load " << data_file << endl;
      cout << "   N.B.: a Jeans analysis kinematic data file must be formatted as follows "
           "(see data/dataVel.txt and data/dataSigmap.txt):" << endl;
      cout << "     TypeData" << endl;
      cout << "     R[kpc]  dR[kpc]  V[km/s_or_km^2/s^2] dV[km/s_or_km^2/s^2]"
           "  (if use of Vel TypeData, you can add MembershipProba, RA[deg], Dec[deg])" << endl;
      cout << "   N.B.: a Jeans analysis surface brightness data file must be "
           "formatted as follows (see data/dataLight.txt):" << endl;
      cout << "     TypeData" << endl;
      cout << "     R[kpc]  dR[kpc]  I[L/kpc^2] dI[L/kpc^2]" << endl;

   }

   // Open file and check if exists
   ifstream f_mcmc(data_file.c_str());
   if (!f_mcmc) {
      printf("\n====> ERROR: halo_load_data4jeans() in janalysis.cc");
      printf("\n             Cannot open (and read) file %s", data_file.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }
   // Get file name
   data_halo.FileName = data_file;
   // List of keywords
   const int n_typedata = 4;
   const char keyword_typedata[n_typedata][50] = {"Vel", "Sigmap2", "Sigmap", "SB"};

   const int n_keyword_JeansData = 9;
   const char keyword_JeansData [n_keyword_JeansData][100] = {
      "R[kpc]", "dR[kpc]", "V[km/s_or_km^2/s^2]",
      "dV[km/s_or_km^2/s^2]", "MembershipProba", "I[L/kpc^2]",
      "dI[L/kpc^2]", "RA[deg]", "Dec[deg]"
   };

   string names_line;
   int iuse = 0;
   int i = 0;
   bool is_angles = 0;
   bool is_membership = 0;

   // Loop over the header - find the keywords
   while (iuse < 3) {
      getline(f_mcmc, names_line);
      names_line.erase(remove_if(names_line.begin(), names_line.end(), isNotASCII), names_line.end());

      // if it is a comment (start with #) or a blanck line, skip it
      string line = removeblanks_from_startstop(names_line);
      if (line[0] == '#' || line.empty()) {
         i = i + 1;
         continue;
      }
      if (iuse == 1) { // 2nd non-comment or non-empty line: "Vel" or "Sigmap2" or "Sigmap" or "SB"
         vector<string> type_params;
         replace_substring(line, "\t", " ");
         string2list(line, " ", type_params);
         if (type_params[0] != keyword_typedata[0] && type_params[0] != keyword_typedata[1]
               && type_params[0] != keyword_typedata[2] && type_params[0] != keyword_typedata[3]) {
            cout << type_params[0] << endl;
            printf("\n====> ERROR: halo_load_data4jeans() in janalysis.cc");
            printf("\n             TypeData keyword must be \"Vel\", \"Sigmap2\", \"Sigmap \", or \"SB\"");
            printf("\n             => abort()\n\n");
            abort();
         }
         data_halo.TypeData = type_params[0];
      }
      if (iuse == 2) { // 3nd non-comment or non-empty line: R[kpc]  dR[kpc]   V[km/s_or_km^2/s^2]
         // (or I[#/kpc^2])  dV[km/s_or_km^2/s^2] (or dI[#/kpc^2])  MembershipProba
         // (if use of Vel TypeData) RA[deg] [optionnal] Dec[deg] [optionnal]
         vector<string> read_params;
         replace_substring(line, "\t", " ");
         string2list(line, " ", read_params);
         if (data_halo.TypeData == keyword_typedata[0]) { // Velocities
            if (read_params.size() < 4) {
               printf("\n====> ERROR: halo_load_data4jeans() in janalysis.cc");
               printf("\n             Wrong number (=%d) of kinematic data keywords, you must use at least (in that order)",
                      (int)read_params.size());
               printf("\n             \"R[kpc]\"  \"dR[kpc]\"   \"V[km/s_or_km^2/s^2]\"  \"dV[km/s_or_km^2/s^2]\"");
               printf("\n             => abort()\n\n");
               abort();
            }
            if (read_params.size() == 4) { //no Membership, Ra and Dec
               for (int m = 0; m < 4; m++) {
                  if (read_params[m] != keyword_JeansData[m]) {
                     printf("\n====> ERROR: halo_load_data4jeans() in janalysis.cc");
                     printf("\n             Wrong names for kinematic data keywords, there should be (in that order)");
                     printf("\n             \"R[kpc]\"  \"dR[kpc]\"   \"V[km/s_or_km^2/s^2]\"  \"dV[km/s_or_km^2/s^2]\"");
                     printf("\n             => abort()\n\n");
                     abort();
                  }
               }
            } else if (read_params.size() == 5) { //Membership probability but no Ra and Dec
               is_membership = 1;
               for (int m = 0; m < 5; m++) {
                  if (read_params[m] != keyword_JeansData[m]) {
                     cout << "\n====> ERROR: in the kinematic data keywords, you must use \"R[kpc]\"  \"dR[kpc]\"   \"V[km/s_or_km^2/s^2]\"  \"dV[km/s_or_km^2/s^2]\" [+ optional MembershipProba RA[deg] Dec[deg]] in that order" << endl;
                     return;
                  }
               }
            } else if (read_params.size() == 6) { // Ra and Dec but no membership probabilities
               is_angles = 1;
               for (int m = 0; m < 4; m++) {
                  if (read_params[m] != keyword_JeansData[m]) {
                     cout << "\n====> ERROR: in the kinematic data keywords, you must use \"R[kpc]\"  \"dR[kpc]\"   \"V[km/s_or_km^2/s^2]\"  \"dV[km/s_or_km^2/s^2]\" [+ optional MembershipProba RA[deg] Dec[deg]] in that order" << endl;
                     return;
                  }
               }
               if (read_params[4] != keyword_JeansData[7] || read_params[5] != keyword_JeansData[8]) {
                  cout << "\n====> ERROR: in the kinematic data keywords, you must use \"R[kpc]\"  \"dR[kpc]\"   \"V[km/s_or_km^2/s^2]\"  \"dV[km/s_or_km^2/s^2]\"  [+ optional MembershipProba RA[deg] Dec[deg]] in that order" << endl;
                  return;
               }
            } else if (read_params.size() == 7) { // Membership probability, Ra, and Dec
               is_angles = 1;
               is_membership = 1;
               for (int m = 0; m < 5; m++) {
                  if (read_params[m] != keyword_JeansData[m]) {
                     cout << "\n====> ERROR: in the kinematic data keywords, you must use \"R[kpc]\"  \"dR[kpc]\"   \"V[km/s_or_km^2/s^2]\"  \"dV[km/s_or_km^2/s^2]\" [+ optional MembershipProba RA[deg] Dec[deg]] in that order" << endl;
                     return;
                  }
               }
               if (read_params[5] != keyword_JeansData[7] || read_params[6] != keyword_JeansData[8]) {
                  cout << "\n====> ERROR: in the kinematic data keywords,  you must use \"R[kpc]\"  \"dR[kpc]\"   \"V[km/s_or_km^2/s^2]\"  \"dV[km/s_or_km^2/s^2]\"  [+ optional MembershipProba RA[deg] Dec[deg]] in that order" << endl;
                  return;
               }
            }
         } else if (data_halo.TypeData == keyword_typedata[1] || data_halo.TypeData == keyword_typedata[2]) { // Velocity dispersions

            if (read_params.size() != 4) {
               cout << "\n====> ERROR: wrong number of kinematic data keywords, you must use \"R[kpc]\"  \"dR[kpc]\"   \"V[km/s_or_km^2/s^2]\"  \"dV[km/s_or_km^2/s^2]\" in that order" << endl;
               return;
            }
            for (int m = 0; m < 4; m++) {
               if (read_params[m] != keyword_JeansData[m]) {
                  cout << "\n====> ERROR: in the kinematic data keywords, you must use \"R[kpc]\"  \"dR[kpc]\"   \"V[km/s_or_km^2/s^2]\"  \"dV[km/s_or_km^2/s^2]\" in that order" << endl;
                  return;
               }
            }
         } else if (data_halo.TypeData == keyword_typedata[3]) { // Surface brightness
            if (read_params.size() != 4) {
               cout << "\n====> ERROR: wrong number of Light Data Keyword, you must use \"R[kpc]\"  \"dR[kpc]\"   \"I[L/kpc^2]\"  \"dI[L/kpc^2]\" in that order" << endl;
               return;
            }

            if (read_params[0] != keyword_JeansData[0] || read_params[1] != keyword_JeansData[1] || read_params[2] != keyword_JeansData[5] || read_params[3] !=
                  keyword_JeansData[6]) {
               cout << "\n====> ERROR: in the Light Data Keyword, you must use \"R[kpc]\"  \"dR[kpc]\"   \"I[L/kpc^2]\"  \"dI[L/kpc^2]\" in that order" << endl;
               return;
            }
         }
      }
      i = i + 1;
      iuse = iuse + 1;
   }
   f_mcmc.close();

   // Fill data_halo with data
   ifstream f_data(data_file.c_str());
   string line_input;
   int k = 0;
   while (getline(f_data, line_input).good()) {
      line_input.erase(remove_if(line_input.begin(), line_input.end(), isNotASCII), line_input.end());
      if (k < i) {
         k = k + 1;
      } else {
         // if it is a comment (start with #) or a blanck line, skip it
         string line = removeblanks_from_startstop(line_input);
         if (line[0] == '#' || line.empty()) {
            continue;
         }
         vector<double> params;
         replace_substring(line, "\t", " ");
         string2list(line, " ", params);
         if (data_halo.TypeData == keyword_typedata[0]) { // Unbinned kinematic data file (velocities)
            data_halo.Radius.push_back(params[0]);
            data_halo.ErrRadius.push_back(params[1]);
            data_halo.VelocData.push_back(params[2]);
            data_halo.ErrVelocData.push_back(params[3]);
            if (is_membership && is_angles) {   // membership probabilities and angles
               data_halo.MembershipProb.push_back(params[4]);
               data_halo.RA.push_back(params[5]);
               data_halo.Dec.push_back(params[6]);
            } else if (is_membership) // just membership
               data_halo.MembershipProb.push_back(params[4]);
            else if (is_angles) { // just angles
               data_halo.RA.push_back(params[4]);
               data_halo.Dec.push_back(params[5]);
               data_halo.MembershipProb.push_back(1.); // in case of no membership probabilities, set them to 1
            } else // no membership and no angles
               data_halo.MembershipProb.push_back(1.); // in case of no membership probabilities, set them to 1
         } else if (data_halo.TypeData == keyword_typedata[1] || data_halo.TypeData == keyword_typedata[2]) { // Binned kinematic data file (velocity dispersion)
            data_halo.Radius.push_back(params[0]);
            data_halo.ErrRadius.push_back(params[1]);
            data_halo.VelocData.push_back(params[2]);
            data_halo.ErrVelocData.push_back(params[3]);
         } else if (data_halo.TypeData == keyword_typedata[3]) { // Surface brightness data
            data_halo.Radius.push_back(params[0]);
            data_halo.ErrRadius.push_back(params[1]);
            data_halo.LightData.push_back(params[2]);
            data_halo.ErrLightData.push_back(params[3]);
         }
      }
   }

   f_data.close();
   return ;
}

//______________________________________________________________________________
void halo_load_data4jeans(string const &data_file, struct gStructHalo &stat_halo, bool is_verbose)
{
   //--- Reads a Jeans analysis data file (kinematics or surface brightness).
   //    The format must be as in data/dataSigmap.txt, data/dataVel.txt or data/dataLight.txt
   //    Data are stored in the gStructHalo stat_halo.
   //    N.B.: a Jeans analysis kinematic data file must be formatted as follows
   //    (see data/dataVel.txt and data/dataSigmap.txt):
   //       TypeData
   //       R[kpc]  dR[kpc]  V[km/s_or_km^2/s^2] dV[km/s_or_km^2/s^2]
   //         (if use of Vel TypeData, you can add MembershipProba, RA[deg], Dec[deg])
   //    N.B.: a Jeans analysis surface brightness data file must be formatted
   //    as follows (see data/dataLight.txt):
   //       TypeData
   //       R[kpc]  dR[kpc]  I[#/kpc^2] dI[#/kpc^2]
   //
   // INPUTS:
   //  data_file     File to be read
   //  stat_halo     gStructHalo structure to store the data
   //  is_verbose    Chatter or not...
   // OUTPUTS:
   //  stat_halo     gStructHalo structure to store the data

   halo_load_data4jeans(data_file, stat_halo.JeansData, is_verbose);
}

//______________________________________________________________________________
void halo_load_list(string const &file_halos, vector<struct gStructHalo> &list_halos,
                    bool is_clear)
{
   //--- Reads list of halos (name, position, distance...). The format must be as in
   //    data/generic_list.txt or data/generic_triaxial_list.txt (for triaxial halos).
   //
   //  file_halos     File to be read
   //  list_halos     Vector of gStructHalo containing halo params
   //  is_clear       Mode to load new objects from list
   //                    - true: clears any pre-loaded file (default call)
   //                    - queues the objects with some previously loaded ones (is_clear=false)
   //                      [checks that the object to be added is not already loaded]

   string file_halos_plain = file_halos;
   resolve_envvar(file_halos_plain);

   char char_tmp[512];

   // Open file and check if exists
   ifstream f_list(file_halos_plain.c_str());
   if (!f_list) {
      print_error("janalysis.cc", "halo_load_list()", "Cannot find and open file '" + file_halos_plain + "'");
   }
   cout << ">>>>> Load " << file_halos_plain << endl;

   struct gStructHalo tmp;
   if (is_clear) list_halos.clear();

   // Loop over lines: skip empty ones or those starting with #
   string line;
   int n_params = 0;
   while (getline(f_list, line)) {

      line.erase(remove_if(line.begin(), line.end(), isNotASCII), line.end());
      replace_substring(line, "\t", " ");

      size_t pos = line.find_first_not_of(" ");
      if (line.empty() || pos == string::npos || line[pos] == '#') continue;

      vector<string> params;
      string2list(line, " ", params);

      // n_params = 13: Spherical halo list input
      // n_params = 16: list output
      // n_params = 18: Halo mode subhalos output
      // n_params = 20: Triaxial halo list input
      // n_params = 21: Galactic subhalos output

      int n_params_line = int(params.size());
      if (n_params_line != 13 && n_params_line != 20 && n_params_line != 21 && n_params_line != 16 && n_params_line != 18) {
         printf("\n====> ERROR: halo_load_list() in janalysis.cc");
         printf("\n             Wrong number of columns (=%d) instead of 13 (or 20 for triaxial models).", (int)params.size());
         printf("\n             => abort()\n\n");
         abort();
      }

      if (n_params_line != 20 && n_params_line > 13) {
         for (int ii = 13; ii < int(params.size()); ++ii) params[ii] = 0.;
         n_params_line = 13.;
      }

      n_params = max(n_params, n_params_line);

      // Fill parameters
      params[0].erase(remove(params[0].begin(), params[0].end(), '\t'), params[0].end());
      params[1].erase(remove(params[1].begin(), params[1].end(), '\t'), params[1].end());
      params[9].erase(remove(params[9].begin(), params[9].end(), '\t'), params[9].end());
      tmp.Name = params[0];
      tmp.Type = params[1];
      tmp.PsiDeg = atof(params[2].c_str());
      tmp.ThetaDeg = atof(params[3].c_str());

      tmp.l = atof(params[4].c_str());
      tmp.z = atof(params[5].c_str());
      // Check consistency of given distance or redshift
      if (fabs(tmp.l + 1) > SMALL_NUMBER and fabs(tmp.z + 1) > SMALL_NUMBER) {
         printf("\n====> ERROR: halo_load_list() in janalysis.cc");
         printf("\n             Halo distance must be specified either by the proper distance");
         printf("\n             or by the redshift. The unused specifier must be set to -1.");
         printf("\n             => abort()\n\n");
         abort();
      } else if (fabs(tmp.l + 1) < SMALL_NUMBER and fabs(tmp.z + 1) < SMALL_NUMBER) {
         printf("\n====> ERROR: halo_load_list() in janalysis.cc");
         printf("\n             Halo distance must be specified either by the proper distance");
         printf("\n             or by the redshift. One and only one of the specifiers must not be set to -1.");
         printf("\n             => abort()\n\n");
         abort();
      } else if (fabs(tmp.z + 1) < SMALL_NUMBER) {
         double dc_Mpc = tmp.l * KPC_to_MPC;
         tmp.z =  d_to_z(dc_Mpc, 0);
      } else if (fabs(tmp.l + 1) < SMALL_NUMBER) {
         tmp.l =  dh_c(tmp.z) / gCOSMO_HUBBLE / KPC_to_MPC;
      }
      if (tmp.z < ZMIN_EXTRAGAL) tmp.z = 0.; // no cosmic redshift for z<1e-3: proper motion redshift probably bigger...

      tmp.Rvir = atof(params[6].c_str()) / (1 + tmp.z);
      if (fabs((tmp.l - tmp.Rvir) / tmp.l) < SMALL_NUMBER) {
         tmp.Rvir -= tmp.Rvir * SMALL_NUMBER;
      }
      if (tmp.HaloProfile == kNODES and atof(params[7].c_str()) == -1) {
         tmp.Rhos = gHALO_NODES_INPUT_RHOSCALE[atoi(params[10].c_str())] * pow(1 + tmp.z, 3.);
         tmp.Rscale = gHALO_NODES_INPUT_RSCALE[atoi(params[10].c_str())] / (1 + tmp.z);
      } else {
         tmp.Rhos = atof(params[7].c_str()) * pow(1 + tmp.z, 3.);
         tmp.Rscale = atof(params[8].c_str()) / (1 + tmp.z);
      }
      tmp.HaloProfile = string_to_enum("FLAG_PROFILE", params[9]);
      tmp.ShapeParam1 = atof(params[10].c_str());
      tmp.ShapeParam2 = atof(params[11].c_str());
      tmp.ShapeParam3 = atof(params[12].c_str());

      // convert rho_s to rho_0 as used in profiles.cc for ZHAO profile:
      if (tmp.HaloProfile == kZHAO) tmp.Rhos *= pow(2, (tmp.ShapeParam2 - tmp.ShapeParam3) / tmp.ShapeParam1);
      else if (tmp.HaloProfile == kNODES) {
         // check if node data is loaded:
         if (gHALO_NODES_X_GRID.size() == 1) {
            int standardparam_length = string(gSIM_STANDARD_INPUTPARAMS[kLIST_HALOES_NODES]).length() + 1;

            cout << "\n====> Your choice of kNODES halo profile in " << file_halos_plain << " requires the additional input parameter:" << endl << endl;
            cout << string_fixlength("# Variable name", 40) << string_fixlength("Unit", gSIM_PARAMLENGTH_MAX) << string_fixlength("Standard Value", gSIM_PARAMLENGTH_MAX + 3)
                 << string_fixlength("(Format)", gSIM_PARAMLENGTH_MAX + 3) << "(Comment)" << endl << endl;
            cout << string_fixlength(gSIM_INPUTPARAMS[kLIST_HALOES_NODES][kSIM_INPUTPARAM_VARNAME], 40)
                 << string_fixlength(gSIM_INPUTPARAMS[kLIST_HALOES_NODES][kSIM_INPUTPARAM_UNIT], gSIM_PARAMLENGTH_MAX)
                 << string_fixlength(gSIM_STANDARD_INPUTPARAMS[kLIST_HALOES_NODES], standardparam_length) << "   "
                 << string_fixlength("<" + string(gSIM_INPUTPARAMS[kLIST_HALOES_NODES][kSIM_INPUTPARAM_DATATYPE]) + ">", gSIM_PARAMLENGTH_MAX + 3)
                 << gSIM_INPUTPARAMS[kLIST_HALOES_NODES][kSIM_INPUTPARAM_DESCRIPTION] << endl;
            cout << endl;
            cout         << "      Please add it to the command line or your input parameter file " <<  endl;
            cout       << "      and restart the program."  << endl <<  endl;
            abort();
         }
      }

      if (params.size() > 13) {
         tmp.Triaxial_Is = atoi(params[13].c_str());
         if (tmp.Triaxial_Is == 1 and params.size() == 20) {
            tmp.Triaxial_a = atof(params[14].c_str());
            tmp.Triaxial_b = atof(params[15].c_str());
            tmp.Triaxial_c = atof(params[16].c_str());
            tmp.Triaxial_rotalpha = atof(params[17].c_str());
            tmp.Triaxial_rotbeta = atof(params[18].c_str());
            tmp.Triaxial_rotgamma = atof(params[19].c_str());
         } else if (tmp.Triaxial_Is == 1 and params.size() != 20) {
            printf("\n====> ERROR: halo_load_list() in janalysis.cc");
            printf("\n             Wrong number of columns (=%d) instead of 20 for triaxial model.", (int)params.size());
            printf("\n             => abort()\n\n");
            abort();
         }
      } else {
         tmp.Triaxial_Is = false;
         tmp.Triaxial_a = 1.;
         tmp.Triaxial_b = 1.;
         tmp.Triaxial_c = 1.;
         tmp.Triaxial_rotalpha = 0.;
         tmp.Triaxial_rotbeta = 0.;
         tmp.Triaxial_rotgamma = 0.;
      }

      const int npar = 7;
      double par_tot[npar] = {tmp.Rhos, tmp.Rscale, tmp.ShapeParam1, tmp.ShapeParam2, tmp.ShapeParam3, (double)tmp.HaloProfile, tmp.Rvir};

      // assign Rvir to Rdelta if set to -1
      if (fabs(tmp.Rvir + 1 / (1 + tmp.z)) < SMALL_NUMBER) {
         double Delta_c = delta_x_to_delta_crit(gCOSMO_DELTA0, gCOSMO_FLAG_DELTA_REF, tmp.z);
         tmp.Rvir = shapeparams_to_Rdelta(par_tot, Delta_c, tmp.z);
         par_tot[6] = tmp.Rvir;
      }

      tmp.Mtot = mass_singlehalo(par_tot, gSIM_EPS);

      // Find matching type for halo
      int i_type = -1;
      for (int ii = 0; ii < gN_TYPEHALOES - 1; ++ii) {
         if (tmp.Type == gNAMES_TYPEHALOES[ii]) {
            i_type = ii;
            break;
         }
      }
      if (i_type == -1) {
         printf("\n====> ERROR: halo_load_list() in janalysis.cc");
         printf("\n             Type=%s (for Name = %s) found in %s does not match any existing type",
                tmp.Type.c_str(), tmp.Name.c_str(), gLIST_HALOES.c_str());
         printf("\n             Available types are: ");
         for (int ii = 0; ii < gN_TYPEHALOES - 1; ++ii)
            printf("%s ", gNAMES_TYPEHALOES[ii]);
         printf("\n             => abort()\n\n");
         abort();
      }

      tmp.Subs_mfrac = gHALO_SUBS_MASSFRACTION[i_type];
      tmp.Subs_dPdM_Slope = gHALO_SUBS_DPDM_SLOPE[i_type];

      // Assign correct values for kHOST flag for halo parameters:

      bool is_shapeparams_likehost_error = false;
      // if kHOST selected for dp/dv profile
      if (gHALO_SUBS_DPDV_FLAG_PROFILE[i_type] == kHOST) tmp.Subs_dPdV_Profile = tmp.HaloProfile;
      else tmp.Subs_dPdV_Profile = gHALO_SUBS_DPDV_FLAG_PROFILE[i_type];

      // if kHOST selected for dp/dv profile shape parameters
      if (int(gHALO_SUBS_DPDV_SHAPE_PARAMS[i_type][0]) == kHOST) {
         if (tmp.ShapeParam1 == -1 && tmp.Subs_dPdV_Profile != kBURKERT && tmp.Subs_dPdV_Profile != kDPDV_PIERI11) is_shapeparams_likehost_error = true;
         tmp.Subs_dPdV_ShapeParam1 = tmp.ShapeParam1;
      } else tmp.Subs_dPdV_ShapeParam1 = gHALO_SUBS_DPDV_SHAPE_PARAMS[i_type][0];

      if (int(gHALO_SUBS_DPDV_SHAPE_PARAMS[i_type][1]) == kHOST) {
         if (tmp.ShapeParam2 == -1
               && tmp.Subs_dPdV_Profile != kBURKERT
               && tmp.Subs_dPdV_Profile != kDPDV_PIERI11
               && tmp.Subs_dPdV_Profile != kNODES
               && tmp.Subs_dPdV_Profile != kDPDV_SPRINGEL08_ANTIBIASED
               && tmp.Subs_dPdV_Profile != kEINASTO
               && tmp.Subs_dPdV_Profile != kEINASTO_N) is_shapeparams_likehost_error = true;
         tmp.Subs_dPdV_ShapeParam2 = tmp.ShapeParam2;
      } else tmp.Subs_dPdV_ShapeParam2 = gHALO_SUBS_DPDV_SHAPE_PARAMS[i_type][1];

      if (int(gHALO_SUBS_DPDV_SHAPE_PARAMS[i_type][2]) == kHOST) {
         if (tmp.ShapeParam3 == -1
               && tmp.Subs_dPdV_Profile != kBURKERT
               && tmp.Subs_dPdV_Profile != kDPDV_PIERI11
               && tmp.Subs_dPdV_Profile != kNODES
               && tmp.Subs_dPdV_Profile != kDPDV_SPRINGEL08_ANTIBIASED
               && tmp.Subs_dPdV_Profile != kEINASTO
               && tmp.Subs_dPdV_Profile != kEINASTO_N) is_shapeparams_likehost_error = true;
         tmp.Subs_dPdV_ShapeParam3 = tmp.ShapeParam3;
      } else tmp.Subs_dPdV_ShapeParam3 = gHALO_SUBS_DPDV_SHAPE_PARAMS[i_type][2];

      // if kHOST selected for dp/dv rscale profile
      if (gHALO_SUBS_DPDV_RSCALE_TO_RS_HOST[i_type] < 0.) {
         printf("\n====> ERROR: halo_load_list() in janalysis.cc");
         printf("\n             Ratio of dPdV scale radius to total scale radius must be positive.");
         printf("\n             => abort()\n\n");
         abort();
      } else
         tmp.Subs_dPdV_Rscale = gHALO_SUBS_DPDV_RSCALE_TO_RS_HOST[i_type] * tmp.Rscale;
      tmp.Subs_dPdV_Rs_to_Rshost = gHALO_SUBS_DPDV_RSCALE_TO_RS_HOST[i_type];

      if (tmp.Subs_dPdV_Profile == kDPDV_PIERI11 and tmp.Subs_mfrac > SMALL_NUMBER) {
         // update r_s and shape parameter:
         gsl_function F;
         F.function = &solve_rbias;
         rbias_params_for_rootfinding params = {tmp.Mtot, tmp.Rvir, tmp.Rhos, tmp.Rscale,
                                                tmp.ShapeParam1, tmp.ShapeParam2, tmp.ShapeParam3, tmp.HaloProfile, tmp.Subs_mfrac
                                               };
         F.params = &params;
         double rmin = 1e-5;
         double rmax = 1e5;
         int return_status = 0;
         double rbias = rootsolver_gsl(gsl_root_fsolver_falsepos, F, rmin, rmax, gSIM_EPS, return_status); // faster than gsl_root_fsolver_brent and gsl_root_fsolver_bisection
         tmp.Subs_dPdV_Rscale = tmp.Rscale;
         tmp.Subs_dPdV_ShapeParam1 = rbias;
         tmp.Subs_dPdV_ShapeParam2 = 0;
         tmp.Subs_dPdV_ShapeParam3 = 0;
      }

      // if kHOST selected for inner clump profile
      if (gHALO_SUBS_FLAG_PROFILE[i_type] == kHOST) tmp.Subs_Inner_Profile = tmp.HaloProfile;
      else tmp.Subs_Inner_Profile = gHALO_SUBS_FLAG_PROFILE[i_type];

      // if kHOST selected for inner clump profile shape parameters
      if (int(gHALO_SUBS_SHAPE_PARAMS[i_type][0]) == kHOST) {
         if (tmp.ShapeParam1 == -1 && tmp.Subs_Inner_Profile != kBURKERT && tmp.Subs_Inner_Profile != kDPDV_PIERI11) is_shapeparams_likehost_error = true;
         tmp.Subs_Inner_ShapeParam1 = tmp.ShapeParam1;
      } else tmp.Subs_Inner_ShapeParam1 = gHALO_SUBS_SHAPE_PARAMS[i_type][0];

      if (int(gHALO_SUBS_SHAPE_PARAMS[i_type][1]) == kHOST) {
         if (tmp.ShapeParam2 == -1
               && tmp.Subs_Inner_Profile != kBURKERT
               && tmp.Subs_Inner_Profile != kDPDV_PIERI11
               && tmp.Subs_Inner_Profile != kNODES
               && tmp.Subs_Inner_Profile != kDPDV_SPRINGEL08_ANTIBIASED
               && tmp.Subs_Inner_Profile != kEINASTO
               && tmp.Subs_Inner_Profile != kEINASTO_N) is_shapeparams_likehost_error = true;
         tmp.Subs_Inner_ShapeParam2 = tmp.ShapeParam2;
      } else tmp.Subs_Inner_ShapeParam2 = gHALO_SUBS_SHAPE_PARAMS[i_type][1];

      if (int(gHALO_SUBS_SHAPE_PARAMS[i_type][2]) == kHOST) {
         if (tmp.ShapeParam3 == -1
               && tmp.Subs_Inner_Profile != kBURKERT
               && tmp.Subs_Inner_Profile != kDPDV_PIERI11
               && tmp.Subs_Inner_Profile != kNODES
               && tmp.Subs_Inner_Profile != kDPDV_SPRINGEL08_ANTIBIASED
               && tmp.Subs_Inner_Profile != kEINASTO
               && tmp.Subs_Inner_Profile != kEINASTO_N) is_shapeparams_likehost_error = true;
         tmp.Subs_Inner_ShapeParam3 = tmp.ShapeParam3;
      } else  tmp.Subs_Inner_ShapeParam3 = gHALO_SUBS_SHAPE_PARAMS[i_type][2];

      if (is_shapeparams_likehost_error and gDM_SUBS_NUMBEROFLEVELS > 0) {
         printf("\n====> ERROR: halo_load_list() in janalysis.cc");
         printf("\n             1. You have set value 'kHOST' in substructure shape/dPdV parameter description");
         printf("\n                and the corresponding shape parameter does not exist for the host halo. ");
         printf("\n             OR");
         printf("\n             2. Some shape parameters are missing for the substructure description.");
         printf("\n             Please specify explicit value(s) and/or don't use 'kHOST'.");
         printf("\n             => abort()\n\n");
         abort();
      }


      tmp.Subs_Inner_CDELTAMDELTA = gHALO_SUBS_FLAG_CDELTAMDELTA[i_type];
      tmp.Subs_Mmax = gDM_SUBS_MMAXFRAC * tmp.Mtot;

      // If old list was not cleared, check if new list is really new!
      if (is_clear == false) {
         for (int i = 0; i < (int)list_halos.size(); ++i) {
            if (upper_case(tmp.Name) == upper_case(list_halos[i].Name) &&  upper_case(tmp.Type) == upper_case(list_halos[i].Type)) {
               sprintf(char_tmp, "Name = %s and Type = %s already loaded => Overrides old values with those read in %s.", (tmp.Name).c_str(), (tmp.Type).c_str(), file_halos_plain.c_str());
               print_warning("janalysis.cc", "halo_load_list()", string(char_tmp));
               list_halos[i] = tmp;
            }
         }
      } else list_halos.push_back(tmp);
   }

   printf("  - List of halos loaded from %s\n", file_halos_plain.c_str());
   if (n_params == 13) {
      printf("#**********************************************************************************************************#\n");
      printf("#                  [OBJECT LOCATION AND SIZE]                |            DM DISTRIBUTION (RHO_TOT)        #\n");
      printf("# Name           Type      l       b      d      z   Rdelta  |     rhos       rs    prof.   #1   #2   #3   #\n");
      printf("#  -               -     [deg]   [deg]  [kpc]    -   [kpc]   |  [Msol/kpc3]  [kpc] [enum]    -    -    -   #\n");
      printf("#**********************************************************************************************************#\n");
      for (int i = 0; i < (int)list_halos.size(); ++i) {
         double rhos_disp = list_halos[i].Rhos / pow(1 + list_halos[i].z, 3);
         if (list_halos[i].HaloProfile == kZHAO) rhos_disp /= pow(2, (list_halos[i].ShapeParam2 - list_halos[i].ShapeParam3) / list_halos[i].ShapeParam1);
         printf("%-15s k%-7s %+6.1lf %+6.1lf %.6g %.6g %.6g       %.6g    %.6g   k%-6s %.4g %.4g %4g\n",
                (list_halos[i].Name).c_str(), (list_halos[i].Type).c_str(), list_halos[i].PsiDeg, list_halos[i].ThetaDeg,
                list_halos[i].l, list_halos[i].z, list_halos[i].Rvir * (1 + list_halos[i].z),
                rhos_disp, list_halos[i].Rscale * (1 + list_halos[i].z), gNAMES_PROFILE[(int)list_halos[i].HaloProfile],
                list_halos[i].ShapeParam1, list_halos[i].ShapeParam2, list_halos[i].ShapeParam3);

         //cout << gNAMES_PROFILE[(int)list_halos[i].Subs_dPdV_Profile] << " " << list_halos[i].Subs_dPdV_ShapeParam1
         //     << " " << list_halos[i].Rscale << " vs " << list_halos[i].Subs_dPdV_Rscale << endl;
      }
      printf("#**********************************************************************************************************#\n");
   } else if (n_params == 20) {
      printf("#********************************************************************************************************************************************************#\n");
      printf("#                  [OBJECT LOCATION AND SIZE]                |            DM DISTRIBUTION (RHO_TOT)                      [TRIAXIALITY]                   #\n");
      printf("# Name           Type      l       b      d      z   Rdelta  |     rhos       rs    prof.   #1   #2   #3       IsTriaxial  a   b   c   rotx  roty  rotz  #\n");
      printf("#  -               -     [deg]   [deg]  [kpc]    -   [kpc]   |  [Msol/kpc3]  [kpc] [enum]    -    -    -           -       -   -   -   [deg] [deg] [deg] #\n");
      printf("#********************************************************************************************************************************************************#\n");
      for (int i = 0; i < (int)list_halos.size(); ++i) {
         double rhos_disp = list_halos[i].Rhos / pow(1 + list_halos[i].z, 3);
         if (list_halos[i].HaloProfile == kZHAO) rhos_disp /= pow(2, (list_halos[i].ShapeParam2 - list_halos[i].ShapeParam3) / list_halos[i].ShapeParam1);
         printf("%-15s k%-7s %+6.1lf %+6.1lf %.6g %.6g  %6g       %.6g    %.6g   k%-6s %.4g %.4g %.4g       %d   %.2lf %.2lf %.2lf  %.0lf %.0lf %.0lf\n",
                (list_halos[i].Name).c_str(), (list_halos[i].Type).c_str(), list_halos[i].PsiDeg, list_halos[i].ThetaDeg,
                list_halos[i].l, list_halos[i].z, list_halos[i].Rvir * (1 + list_halos[i].z),
                rhos_disp, list_halos[i].Rscale * (1 + list_halos[i].z), gNAMES_PROFILE[(int)list_halos[i].HaloProfile],
                list_halos[i].ShapeParam1, list_halos[i].ShapeParam2, list_halos[i].ShapeParam3,
                list_halos[i].Triaxial_Is, list_halos[i].Triaxial_a, list_halos[i].Triaxial_b, list_halos[i].Triaxial_c,
                list_halos[i].Triaxial_rotalpha, list_halos[i].Triaxial_rotbeta, list_halos[i].Triaxial_rotgamma);

         //cout << gNAMES_PROFILE[(int)list_halos[i].Subs_dPdV_Profile] << " " << list_halos[i].Subs_dPdV_ShapeParam1
         //     << " " << list_halos[i].Rscale << " vs " << list_halos[i].Subs_dPdV_Rscale << endl;
      }
      printf("#*********************************************************************************************************************************************************#\n");
   }

   return;
}

//______________________________________________________________________________
void halo_load_list4jeans(string const &file_halos, vector<struct gStructHalo> &list_halos,
                          int switch_y, bool is_clear)
{
   //--- Reads list of haloes for Jeans analysis. The format must be as in
   //    data/list_generic_jeans.txt.
   //
   //  file_halos     File to be read
   //  list_halos     Vector of gStructHalo containing halo params (filled with halo_load_list())
   //  switch_y       0->load JeansData velocity (sigmap); 1->load JeansData light (I)
   //  is_clear       Mode to load new objects from list
   //                    - true: clears any pre-loaded file (default call)
   //                    - queues the objects with some previously loaded ones (is_clear=false)
   //                      [checks that the object to be added is not already loaded]

   string file_halos_plain = file_halos;
   resolve_envvar(file_halos_plain);
   char char_tmp[512];

   // Open file and check if exists
   ifstream f_list(file_halos_plain.c_str());
   if (!f_list) {
      printf("\n====> ERROR: halo_load_list4jeans() in janalysis.cc");
      printf("\n             Cannot find and open file %s", file_halos_plain.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }
   cout << ">>>>> Load " << file_halos_plain << endl;

   // get directory of file:
   string file_halos_dir = "";
   if (file_halos_plain.find_last_of("\\/") != string::npos) {
      file_halos_dir = file_halos_plain.substr(0, file_halos_plain.find_last_of("\\/"));
      file_halos_dir += "/";
   }

   vector<string> VelData; // name of data file
   vector<string> LightData;

   if (is_clear) list_halos.clear();

   // Loop over lines: skip empty ones or those starting with #
   string line;
   int n_params = 0;
   while (getline(f_list, line)) {

      line.erase(remove_if(line.begin(), line.end(), isNotASCII), line.end());
      replace_substring(line, "\t", " ");

      struct gStructHalo tmp; // temporary structure to store the parameters and data
      size_t pos = line.find_first_not_of(" ");
      if (line.empty() || pos == string::npos || line[pos] == '#') continue;

      vector<string> params;
      string2list(line, " ", params);

      if (params.size() != 23) {
         printf("\n====> ERROR: halo_load_list4jeans() in janalysis.cc");
         printf("\n             Wrong number of columns (=%d) instead of 23.", (int)params.size());
         printf("\n             => abort()\n\n");
         abort();
      }
      n_params = max(n_params, (int)params.size());

      // Fill parameters


      params[0].erase(remove(params[0].begin(), params[0].end(), '\t'), params[0].end());
      params[1].erase(remove(params[1].begin(), params[1].end(), '\t'), params[1].end());
      params[3].erase(remove(params[3].begin(), params[3].end(), '\t'), params[3].end());
      params[4].erase(remove(params[4].begin(), params[4].end(), '\t'), params[4].end());
      params[5].erase(remove(params[5].begin(), params[5].end(), '\t'), params[5].end());

      if (params[4][0] != '/') params[4] = file_halos_dir + params[4];
      if (params[5][0] != '/') params[5] = file_halos_dir + params[5];

      params[7].erase(remove(params[7].begin(), params[7].end(), '\t'), params[7].end());
      params[8].erase(remove(params[8].begin(), params[8].end(), '\t'), params[8].end());
      params[13].erase(remove(params[13].begin(), params[13].end(), '\t'), params[13].end());
      params[14].erase(remove(params[14].begin(), params[14].end(), '\t'), params[14].end());
      params[19].erase(remove(params[19].begin(), params[19].end(), '\t'), params[19].end());
      params[20].erase(remove(params[20].begin(), params[20].end(), '\t'), params[20].end());

      tmp.Name = params[0];
      tmp.Type = params[1];
      tmp.Rvir = atof(params[2].c_str());
      gSIM_JEANS_RMAX = atof(params[3].c_str());
      VelData.push_back(params[4]);
      LightData.push_back(params[5]);
      if (switch_y == 0) { // Velocity data
         if (params[4] != "-")
            halo_load_data4jeans(params[4], tmp, 0);
      } else if (switch_y == 1) {
         if (params[5] != "-")
            halo_load_data4jeans(params[5], tmp, 0);
      }
      if (tmp.HaloProfile == kNODES and atof(params[6].c_str()) == -1) {
         tmp.Rhos = gHALO_NODES_INPUT_RHOSCALE[atoi(params[9].c_str())];
         tmp.Rscale = gHALO_NODES_INPUT_RSCALE[atoi(params[9].c_str())];
      } else {
         tmp.Rhos = atof(params[6].c_str());
         tmp.Rscale = atof(params[7].c_str());
      }
      tmp.HaloProfile = string_to_enum("FLAG_PROFILE", params[8]);
      tmp.ShapeParam1 = atof(params[9].c_str());
      tmp.ShapeParam2 = atof(params[10].c_str());
      tmp.ShapeParam3 = atof(params[11].c_str());

      // convert rho_s to rho_0 as used in profiles.cc for ZHAO profile:
      if (tmp.HaloProfile == kZHAO) tmp.Rhos *= pow(2, (tmp.ShapeParam2 - tmp.ShapeParam3) / tmp.ShapeParam1);
      else if (tmp.HaloProfile == kNODES) {
         // check if node data is loaded:
         if (gHALO_NODES_X_GRID.size() == 1) {
            int standardparam_length = string(gSIM_STANDARD_INPUTPARAMS[kLIST_HALOES_NODES]).length() + 1;

            cout << "\n====> Your choice of kNODES halo profile in " << file_halos_plain << " requires the additional input parameter:" << endl << endl;
            cout << string_fixlength("# Variable name", 40) << string_fixlength("Unit", gSIM_PARAMLENGTH_MAX) << string_fixlength("Standard Value", gSIM_PARAMLENGTH_MAX + 3)
                 << string_fixlength("(Format)", gSIM_PARAMLENGTH_MAX + 3) << "(Comment)" << endl << endl;
            cout << string_fixlength(gSIM_INPUTPARAMS[kLIST_HALOES_NODES][kSIM_INPUTPARAM_VARNAME], 40)
                 << string_fixlength(gSIM_INPUTPARAMS[kLIST_HALOES_NODES][kSIM_INPUTPARAM_UNIT], gSIM_PARAMLENGTH_MAX)
                 << string_fixlength(gSIM_STANDARD_INPUTPARAMS[kLIST_HALOES_NODES], standardparam_length) << "   "
                 << string_fixlength("<" + string(gSIM_INPUTPARAMS[kLIST_HALOES_NODES][kSIM_INPUTPARAM_DATATYPE]) + ">", gSIM_PARAMLENGTH_MAX + 3)
                 << gSIM_INPUTPARAMS[kLIST_HALOES_NODES][kSIM_INPUTPARAM_DESCRIPTION] << endl;
            cout << endl;
            cout         << "      Please add it to the command line or your input parameter file " <<  endl;
            cout       << "      and restart the program."  << endl <<  endl;
            abort();
         }
      }
      tmp.L =  atof(params[12].c_str());
      tmp.RLight =  atof(params[13].c_str());
      tmp.LightProfile = string_to_enum("FLAG_LIGHTPROFILE", params[14]);
      tmp.LightShapeParam1 = atof(params[15].c_str());
      tmp.LightShapeParam2 = atof(params[16].c_str());
      tmp.LightShapeParam3 = atof(params[17].c_str());
      tmp.Beta_Aniso_0 = atof(params[18].c_str());
      tmp.Beta_Aniso_Inf = atof(params[19].c_str());
      tmp.AnisoProfile = string_to_enum("FLAG_ANISOTROPYPROFILE", params[20]);
      tmp.RAniso = atof(params[21].c_str());
      tmp.AnisoShapeParam = atof(params[22].c_str());

      // Find matching type for halo
      int i_type = -1;
      for (int ii = 0; ii < gN_TYPEHALOES; ++ii) {
         if (tmp.Type == gNAMES_TYPEHALOES[ii]) {
            i_type = ii;
            break;
         }
      }
      if (i_type == -1) {
         printf("\n====> ERROR: halo_load_list4jeans() in janalysis.cc");
         printf("\n             Type=%s (for Name=%s) found in %s does not match any existing type",
                tmp.Type.c_str(), tmp.Name.c_str(), gLIST_HALOES.c_str());
         printf("\n             Available types are: ");
         for (int ii = 0; ii < gN_TYPEHALOES; ++ii)
            printf("%s ", gNAMES_TYPEHALOES[ii]);
         printf("\n             => abort()\n\n");
         abort();
      }

      // If old list was not cleared, check if new list is really new!
      if (is_clear == false) {
         for (int i = 0; i < (int)list_halos.size(); ++i) {
            if (upper_case(tmp.Name) == upper_case(list_halos[i].Name) &&  upper_case(tmp.Type) == upper_case(list_halos[i].Type)) {
               sprintf(char_tmp, "Name = %s and Type = %s already loaded => Overrides old values with those read in %s.", (tmp.Name).c_str(), (tmp.Type).c_str(), file_halos_plain.c_str());
               print_warning("janalysis.cc", "halo_load_list4jeans()", string(char_tmp));
               list_halos[i] = tmp;
            }
         }
      } else list_halos.push_back(tmp);
   }

   printf("  - List of halos loaded from %s\n", file_halos_plain.c_str());
   printf("#*************************************************************************************************************************************************************************************#\n");
   printf("#     [OBJECT SIZE]          |           [DATA]           |         DM DISTRIBUTION (RHO_TOT)        |             Light Profile                |        Anisotropy  Profile         #\n");
   printf("# Name    Type    Rvir  Rmax |    Vel            Light    |  rhos       rs     prof.   #1   #2   #3  |     L        rs*    prof.   #1   #2   #3 | beta_0 beta_inf  prof.   ra   eta  #\n");
   printf("#  -        -    [kpc] [kpc] |     -               -      |[Msol/kpc3]  [kpc]  [enum]    -    -    - |[Lsol/kpc3]  [kpc]  [enum]    -    -    - |   -       -     [enum]   kpc   -   #\n");
   printf("#*************************************************************************************************************************************************************************************#\n");
   for (int i = 0; i < (int)list_halos.size(); ++i) {
      printf("%-5s %-8s %.1le %le %-8s %-8s   %5.1le   %5.1le  %-5s %.2lf %.2lf %.2lf    %5.1le   %5.1le  %-5s %.2lf %.2lf %.2lf   %.2lf %.2lf %-5s  %5.1le %.2lf \n",
             (list_halos[i].Name).c_str(), (list_halos[i].Type).c_str(), list_halos[i].Rvir, gSIM_JEANS_RMAX,
             VelData[i].c_str(), LightData[i].c_str(), list_halos[i].Rhos, list_halos[i].Rscale, gNAMES_PROFILE[(int)list_halos[i].HaloProfile],
             list_halos[i].ShapeParam1, list_halos[i].ShapeParam2, list_halos[i].ShapeParam3,
             list_halos[i].L, list_halos[i].RLight, gNAMES_LIGHTPROFILE[(int)list_halos[i].LightProfile],
             list_halos[i].LightShapeParam1, list_halos[i].LightShapeParam2, list_halos[i].LightShapeParam3,
             list_halos[i].Beta_Aniso_0, list_halos[i].Beta_Aniso_Inf, gNAMES_ANISOTROPYPROFILE[(int)list_halos[i].AnisoProfile],
             list_halos[i].RAniso, list_halos[i].AnisoShapeParam);
   }
   printf("#*************************************************************************************************************************************************************************************#\n");
}

//______________________________________________________________________________
void halo_set_pardpdv(int i, double par_dpdv[10], vector<struct gStructHalo> &list_halos,
                      bool is_halo_set_to_0_0)
{
   //--- Fills parameters for the spatial distribution of clump in their host halo.
   //
   // INPUTS
   //  i              Index of the halo to use in list_halos
   //  list_halos     Vector of gStructHalo containing halo params (filled with halo_load_list())
   //  is_halo_set_to_0_0  If true, sets the halo to (theta,psi)=(0,0)
   //                 [used for most calculations, avoids coord. issues if |b_halo|~PI/2]
   // OUTPUTS:
   //  par_dpdv[0]    dpdv normalisation [kpc^{-3}]
   //  par_dpdv[1]    dpdv scale radius [kpc]
   //  par_dpdv[2]    dpdv shape parameter #1
   //  par_dpdv[3]    dpdv shape parameter #2
   //  par_dpdv[4]    dpdv shape parameter #3
   //  par_dpdv[5]    dpdv card_profile [gENUM_PROFILE]
   //  par_dpdv[6]    Radius of dpdv [kpc]
   //  par_dpdv[7]    Distance observer - host halo centre [kpc]
   //  par_dpdv[8]    Longitude (psi) of host halo [rad]
   //  par_dpdv[9]    Latitude (theta) of host halo [rad]


   if (i < 0 || i >= (int)list_halos.size()) {
      printf("\n====> ERROR: halo_set_pardpdv() in janalysis.cc");
      printf("\n             Index i=%d is out of range", i);
      printf("\n             => abort()\n\n");
      abort();
   }

   double psi_halo = 0.;
   double theta_halo = 0.;
   // N.B.: if is_halo_set_to_0_0=true, the halo is set to (d,0,0)
   //       to avoid coordinates problems in integrations if ever
   //       |b_halo|~PI/2" << endl;
   if (!is_halo_set_to_0_0) {
      psi_halo = list_halos[i].PsiDeg * DEG_to_RAD;
      check_psi(psi_halo);
      theta_halo = list_halos[i].ThetaDeg * DEG_to_RAD;
   }

   par_dpdv[1] = list_halos[i].Subs_dPdV_Rscale;
   par_dpdv[2] = list_halos[i].Subs_dPdV_ShapeParam1;
   par_dpdv[3] = list_halos[i].Subs_dPdV_ShapeParam2;
   par_dpdv[4] = list_halos[i].Subs_dPdV_ShapeParam3;
   par_dpdv[5] = list_halos[i].Subs_dPdV_Profile;
   par_dpdv[6] = list_halos[i].Rvir;
   par_dpdv[7] = list_halos[i].l;
   par_dpdv[8] = psi_halo;
   par_dpdv[9] = theta_halo;

   if (par_dpdv[5] == kDPDV_PIERI11) {
      gsl_function F;
      F.function = &solve_rbias;
      double mdelta = list_halos[i].Mtot;
      double fsub = list_halos[i].Subs_mfrac;
      rbias_params_for_rootfinding params = {mdelta, list_halos[i].Rvir, list_halos[i].Rhos, list_halos[i].Rscale,
                                             list_halos[i].ShapeParam1, list_halos[i].ShapeParam2, list_halos[i].ShapeParam3, int(list_halos[i].HaloProfile), fsub
                                            };
      F.params = &params;
      double rmin = 1e-5;
      double rmax = 1e5;
      int return_status = 0;
      double rbias = rootsolver_gsl(gsl_root_fsolver_falsepos, F, rmin, rmax, gSIM_EPS, return_status); // faster than gsl_root_fsolver_brent and gsl_root_fsolver_bisection
      par_dpdv[1] = list_halos[i].Rscale;
      par_dpdv[2] = rbias;
      par_dpdv[3] = 0;
      par_dpdv[4] = 0;
   }

   // Find proper normalisation for dpdv to be a probability, i.e. int_0^rvir dpdv = 1
   dpdv_setnormprob(par_dpdv, gSIM_EPS);
}

//______________________________________________________________________________
void halo_set_parsmooth(int i, double par_smooth[21], vector<struct gStructHalo> &list_halos,
                        bool is_halo_set_to_0_0)
{
   //--- Fills smooth parameters for a host halo.
   //
   // INPUTS:
   //  i              Index of the halo to use in list_halos
   //  list_halos     Vector of gStructHalo containing halo params (filled with halo_load_list())
   //  is_halo_set_to_0_0  If true, sets the halo to (theta,psi)=(0,0)
   //                 [used for most calculations, avoids coord. issues if |b_halo|~PI/2]
   // OUTPUTS:
   //  par_smooth[0]  rho_1(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par_smooth[1]  rho_1(r) scale radius [kpc]
   //  par_smooth[2]  rho_1(r) shape parameter #1
   //  par_smooth[3]  rho_1(r) shape parameter #2
   //  par_smooth[4]  rho_1(r) shape parameter #3
   //  par_smooth[5]  rho_1(r) card_profile [gENUM_PROFILE]
   //  par_smooth[6]  rho_1(r) effective radius where to stop integration [kpc]
   //  par_smooth[7]  rho_1(r) distance observer - clump centre [kpc]
   //  par_smooth[8]  rho_1(r) psi_cl to clump centre [rad]
   //  par_smooth[9]  rho_1(r) theta_cl to clump centre [rad]
   //  par_smooth[10] switch_rho: selects which combination of rho_1 and rho_2 to use
   //                    0 -> rho(r) = rho1(r)
   //                    1 -> rho(r) = rho1(r)-rho2(r)
   //                    2 -> rho(r) = rho1(r)*rho2(r)
   //  par_smooth[11] rho_2(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par_smooth[12] rho_2(r) scale radius [kpc]
   //  par_smooth[13] rho_2(r) shape parameter #1
   //  par_smooth[14] rho_2(r) shape parameter #2
   //  par_smooth[15] rho_2(r) shape parameter #3
   //  par_smooth[16] rho_2(r) card_profile [gENUM_PROFILE]
   //  par_smooth[17] rho_2(r) effective radius where to stop integration [kpc]
   //  par_smooth[18] rho_2(r) distance observer - clump centre [kpc]
   //  par_smooth[19] rho_2(r) psi_cl to clump centre [rad]
   //  par_smooth[20] rho_2(r) theta_cl to clump centre [rad]

   if (i < 0 || i >= (int)list_halos.size()) {
      printf("\n====> ERROR: halo_set_parsmooth() in janalysis.cc");
      printf("\n             Index i=%d is out of range", i);
      printf("\n             => abort()\n\n");
      abort();
   }

   const int npar = 10;
   double par_halotot[npar + 1];
   double par_halodpdv[npar];
   halo_set_partot(i, par_halotot, list_halos, is_halo_set_to_0_0);
   halo_set_triaxiality(i, list_halos);

   halo_set_pardpdv(i, par_halodpdv, list_halos, is_halo_set_to_0_0);

   for (int j = 0; j < 10; j++) par_smooth[j] = par_halotot[j];
   par_smooth[10] = 1;
   for (int j = 11; j < 21; j++) par_smooth[j] = par_halodpdv[j - 11];
   par_smooth[11] *= list_halos[i].Subs_mfrac * list_halos[i].Mtot;

}

//______________________________________________________________________________
void halo_set_parsubs(int i, double par_subs[25], vector<struct gStructHalo> &list_halos)
{
   //--- Fills parameters for the mass distribution, number and profiles for the
   //    clumps in host halo.
   //
   // INPTUT:
   //  i              Index of the halo to use in list_halos
   //  list_halos     Vector of gStructHalo structures containing halo parameters (filled with halo_load_list())
   // OUTPUTS:
   //  par_subs[0]    rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_subs[1]    rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_subs[2]    rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_subs[3]    rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]    host halo outer radius where to stop integration [kpc]
   //  par_subs[5]    eps: relative precision sought L calculation
   //  par_subs[6]    z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]    mdelta: mass of the subclump (UNUSED in this function)
   //  par_subs[8]    subhalo dPdM normalisation [1/Msol]
   //  par_subs[9]    subhalo dPdM slope alphaM
   //  par_subs[10]   subhalo dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_subs[11]   dPdc mean concentration <c>   (UNUSED in this function)
   //  par_subs[12]   subhalo dPdc standard deviation       [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[13]   subhalo dPdc card_profile [gENUM_CDELTA_DIST]
   //  par_subs[14]   f: mass fraction of substructures     [only for ANNIHILATION and nlevel>1]
   //  par_subs[15]   nlevel: sub-sub...halos (1=no sub)    [only for ANNIHILATION and nlevel>1]
   //  par_subs[16]   dPdV normalisation  [kpc^{-3}]        [only for ANNIHILATION and nlevel>1]
   //  par_subs[17]   dPdV scale radius [kpc]               [only for ANNIHILATION and nlevel>1]
   //  par_subs[18]   dPdV shape parameter #1               [only for ANNIHILATION and nlevel>1]
   //  par_subs[19]   dPdV shape parameter #2               [only for ANNIHILATION and nlevel>1]
   //  par_subs[20]   dPdV shape parameter #3               [only for ANNIHILATION and nlevel>1]
   //  par_subs[21]   dPdV card_profile [gENUM_PROFILE]     [only for ANNIHILATION and nlevel>1]
   //  par_subs[22]   host halo outer radius (unused)       [only for ANNIHILATION and nlevel>1]
   //  par_subs[23]   ratio rs dPdV to rs_cl                [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]   R_host (corresp. to current dPdV)     [only for ANNIHILATION and nlevel>1]

   if (i < 0 || i >= (int)list_halos.size()) {
      printf("\n====> ERROR: halo_set_parsubs() in janalysis.cc");
      printf("\n             Index i=%d is out of range", i);
      printf("\n             => abort()\n\n");
      abort();
   }

   // Need par_tot and par_dpdv!
   const int npar = 10;
   double par_dpdv[npar];
   halo_set_pardpdv(i, par_dpdv, list_halos, true); //gal_set_pardpdv(par_dpdv);

   par_subs[0] = list_halos[i].Subs_Inner_ShapeParam1;
   par_subs[1] = list_halos[i].Subs_Inner_ShapeParam2;
   par_subs[2] = list_halos[i].Subs_Inner_ShapeParam3;
   par_subs[3] = list_halos[i].Subs_Inner_Profile;
   par_subs[4] = list_halos[i].Rvir;
   par_subs[5] = gSIM_EPS;
   par_subs[6] = list_halos[i].z;
   //par_subs[7] = list_halos[i].Mtot; // check!
   par_subs[7] = 0.;  // will only be used - and updated later - for nlevel > 0 substructures.
   par_subs[8] = 1.;
   par_subs[9] = list_halos[i].Subs_dPdM_Slope;
   par_subs[10] = list_halos[i].Subs_Inner_CDELTAMDELTA;
   par_subs[11] = 0.; // will only be used - and updated later - for nlevel > 0 substructures.
   par_subs[12] = gDM_LOGCDELTA_STDDEV;
   par_subs[13] = gDM_FLAG_CDELTA_DIST;

   for (int j = 0; j < 7; ++j)
      par_subs[j + 16] = par_dpdv[j];
   par_subs[23] = list_halos[i].Subs_dPdV_Rs_to_Rshost;

   // Find proper normalisation for dpdm to be a probability, i.e. int_Mmin^Mmax dpdm dM = 1
   dpdm_setnormprob(&par_subs[8], gDM_SUBS_MMIN, list_halos[i].Subs_Mmax, gSIM_EPS);
   if (gDM_SUBS_NUMBEROFLEVELS > 0 or list_halos[i].Subs_mfrac > 0) {
      par_subs[14] = list_halos[i].Subs_mfrac;
      par_subs[15] = gDM_SUBS_NUMBEROFLEVELS;
   } else {
      par_subs[14] = 0; //  f: mass fraction of substructures = 0
      par_subs[15] = 1; //  dummy value to avoid code to crash.
   }
}

//______________________________________________________________________________
void halo_set_partot(int i, double par_tot[11], vector<struct gStructHalo> &list_halos,
                     bool is_halo_set_to_0_0)
{
   //--- Fills parameters for the total DM halo of the i-th halo in list.
   //    N.B.: Quantities are given in proper coordinates.
   //
   // INPUTS:
   //  i              Index of the halo to use in list_halos
   //  list_halos     Vector of gStructHalo structures containing halo parameters (filled with halo_load_list())
   //  is_halo_set_to_0_0 If true, sets the halo to (theta,psi)=(0,0)
   //                 [used for most calculations, avoids coord. issues if |b_halo|~PI/2]
   // OUTPUTS:
   //  par_tot[0]     rho_tot normalisation [Msol kpc^{-3}]
   //  par_tot[1]     rho_tot scale radius [kpc]
   //  par_tot[2]     rho_tot shape parameter #1
   //  par_tot[3]     rho_tot shape parameter #2
   //  par_tot[4]     rho_tot shape parameter #3
   //  par_tot[5]     rho_tot card_profile [gENUM_PROFILE]
   //  par_tot[6]     Radius of rho_tot [kpc]
   //  par_tot[7]     Distance observer - host halo centre [kpc]
   //  par_tot[8]     Longitude (psi) of host halo [rad]
   //  par_tot[9]     Latitude (theta) of host halo [rad]
   //  par_tot[10]    Redshift z

   if (i < 0 || i >= (int)list_halos.size()) {
      printf("\n====> ERROR: halo_set_partot() in janalysis.cc");
      printf("\n             Index i=%d is out of range", i);
      printf("\n             => abort()\n\n");
      abort();
   }

   double psi_halo = 0.;
   double theta_halo = 0.;
   // N.B.: if is_halo_set_to_0_0=true, the halo is set to (d,0,0)
   //       to avoid coordinates problems in integrations if ever
   //       |b_halo|~PI/2" << endl;
   if (!is_halo_set_to_0_0) {
      psi_halo = list_halos[i].PsiDeg * DEG_to_RAD;
      check_psi(psi_halo);
      theta_halo = list_halos[i].ThetaDeg * DEG_to_RAD;
   }
   par_tot[0] = list_halos[i].Rhos;
   par_tot[1] = list_halos[i].Rscale;
   par_tot[2] = list_halos[i].ShapeParam1;
   par_tot[3] = list_halos[i].ShapeParam2;
   par_tot[4] = list_halos[i].ShapeParam3;
   par_tot[5] = list_halos[i].HaloProfile;
   par_tot[6] = list_halos[i].Rvir;
   par_tot[7] = list_halos[i].l;
   par_tot[8] = psi_halo;
   par_tot[9] = theta_halo;
   par_tot[10] = list_halos[i].z;
}

//______________________________________________________________________________
void halo_set_parjeans(int i, double par_jeans[20], vector<struct gStructHalo> &list_halos)
{
   //--- Fills parameters for the total DM halo of the i-th halo in list,
   //    N.B: eps and par_jeans[13] are set at a default value which will is changed
   //    during the call of a jeans analysis-related function.
   //
   // INPUTS:
   //  i              Index of the halo to use in list_halos
   //  list_halos     Vector of gStructHalo containing halo params (filled with halo_load_list())
   // OUTPUTS:
   //  par_jeans[0]   Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]   Dark matter profile scale radius [kpc]
   //  par_jeans[2]   Dark matter shape parameter #1
   //  par_jeans[3]   Dark matter shape parameter #2
   //  par_jeans[4]   Dark matter shape parameter #3
   //  par_jeans[5]   Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]   Max radius of integration
   //  par_jeans[7]   Light profile normalisation [Lsol]
   //  par_jeans[8]   Light profile scale radius [kpc]
   //  par_jeans[9]   Light profile shape parameter #1
   //  par_jeans[10]  Light profile shape parameter #2
   //  par_jeans[11]  Light profile shape parameter #3
   //  par_jeans[12]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13]  R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14]  eps: precision sought for integration
   //  par_jeans[15]  Anisotropy parameter #1
   //  par_jeans[16]  Anisotropy parameter #2
   //  par_jeans[17]  Anisotropy parameter #3
   //  par_jeans[18]  Anisotropy parameter #4
   //  par_jeans[19]  Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)


   if (i < 0 || i >= (int)list_halos.size()) {
      printf("\n====> ERROR: halo_set_partot() in janalysis.cc");
      printf("\n             Index i=%d is out of range", i);
      printf("\n             => abort()\n\n");
      abort();
   }

   // These values are arbitraty: they have to be modified after
   // the call of the function
   double eps = 0.01;
   double R = 1.;

   // Fill parameters from list_halos

   par_jeans[0] = list_halos[i].Rhos;
   par_jeans[1] = list_halos[i].Rscale;
   par_jeans[2] = list_halos[i].ShapeParam1;
   par_jeans[3] = list_halos[i].ShapeParam2;
   par_jeans[4] = list_halos[i].ShapeParam3;
   par_jeans[5] = list_halos[i].HaloProfile;
   par_jeans[6] = list_halos[i].Rvir;
   par_jeans[7] = list_halos[i].L;
   par_jeans[8] = list_halos[i].RLight;
   par_jeans[9] = list_halos[i].LightShapeParam1;
   par_jeans[10] = list_halos[i].LightShapeParam2;
   par_jeans[11] = list_halos[i].LightShapeParam3;
   par_jeans[12] = list_halos[i].LightProfile;
   par_jeans[13]  = R;
   par_jeans[14]  = eps;
   par_jeans[15] = list_halos[i].Beta_Aniso_0;
   par_jeans[16] = list_halos[i].Beta_Aniso_Inf;
   par_jeans[17] = list_halos[i].RAniso;
   par_jeans[18] = list_halos[i].AnisoShapeParam;
   par_jeans[19] = list_halos[i].AnisoProfile;
}

//______________________________________________________________________________
void halo_set_triaxiality(int i, vector<struct gStructHalo> &list_halos)
{
   //--- Fills global triaxiality parameters (gHALO_TRIAXIAL_*) from the parameter
   //    values found in the i-th halo of the list (of halos).
   //
   // INPUTS:
   //  i              Index of the halo to use in list_halos
   //  list_halos     Vector of halo parameters (filled with halo_load_list())

   if (i < 0 || i >= (int)list_halos.size()) {
      printf("\n====> ERROR: halo_set_triaxiality() in janalysis.cc");
      printf("\n             Index i=%d is out of range", i);
      printf("\n             => abort()\n\n");
      abort();
   }

   // Fills global parameters
   gHALO_TRIAXIAL_AXES[0] = list_halos[i].Triaxial_a;
   gHALO_TRIAXIAL_AXES[1] = list_halos[i].Triaxial_b;
   gHALO_TRIAXIAL_AXES[2] = list_halos[i].Triaxial_c;
   gHALO_TRIAXIAL_IS = list_halos[i].Triaxial_Is;
   gHALO_TRIAXIAL_ROTANGLES[0] = list_halos[i].Triaxial_rotalpha;
   gHALO_TRIAXIAL_ROTANGLES[1] = list_halos[i].Triaxial_rotbeta;
   gHALO_TRIAXIAL_ROTANGLES[2] = list_halos[i].Triaxial_rotgamma;
}

//______________________________________________________________________________
void plot_c_lum_boost_vs_m(double mmin, double mmax, int n_m,
                           string const &list_cvir, double mmin_pp, double frac_max, double f_dm,
                           string const &list_alpham, int nlevel, double sigma_cvir)
{
   //--- Plots cDelta-mDelta relationships, lum, and boost in a large mass range,
   //    for parameters in 'param_file' (some are superseded by parameters directly
   //    passed to the function).
   //
   //  param_file     CLUMPY parameter file
   //  mmin           Minimal mass to plot
   //  mmax           Maximal mass to plot
   //  n_m            Number of mass bins to plot
   //  list_cvir      Comma-separated list of cdelta-mdelta to plot (ALL to display all)
   //  mmin_pp        Minimal mass of the DM substructures
   //  frac_mmax      Mass fraction of the heaviest halo in host (e.g., 0.1)
   //  f_dm           Fraction of DM in substructures at each level (e.g., 0.2)
   //  list_alpham    Comma-separated list of DM slopes (dP/dM~M^(-alpha_m} in [1.8-2])
   //  nlevel         Number of level of sub-sub-sub... to consider (1= no substructures)
   //  sigma_cvir     If non-null, use log-norm distribution of c around <c> of width sigma_cvir (e.g., 0.14)

   // Set clumpy style for plots
#if IS_ROOT
   rootstyle_set2CLUMPY();
#endif

   // Minimal mass of subhalos + maximal mass fraction of a subhalo in host
   gDM_SUBS_MMIN = mmin_pp;
   gDM_SUBS_MMAXFRAC = frac_max;
   if (gDM_SUBS_MMAXFRAC > 1) {
      printf("\n====> ERROR: plot_c_lum_boost_vs_m() in janalysis.cc");
      printf("\n             frac_max=%.2le must be smaller than 1.", frac_max);
      printf("\n             => abort()\n\n");
      abort();
   }

   // alpha_m slopes
   vector<double> v_alpham;
   string2list(list_alpham, ",", v_alpham);
   int n_alpha = v_alpham.size();
   if (gDM_SUBS_NUMBEROFLEVELS == 0) n_alpha = 1;

   // cvir parametrisations
   vector<int> v_cvir;
   if (list_cvir != "ALL") {
      vector<string> v_cvir_str;
      string2list(list_cvir, ",", v_cvir_str);
      for (int i = 0; i < (int)v_cvir_str.size(); ++i) {
         int c = string_to_enum("FLAG_CDELTAMDELTA", v_cvir_str[i]);
         if (c >= 0)
            v_cvir.push_back(c);
      }
   } else {
      for (int i = 0; i < gN_CDELTAMDELTA; ++i)
         v_cvir.push_back(i);
   }
   if (v_cvir.size() == 0) {
      printf("\n====> ERROR: plot_c_lum_boost_vs_m() in janalysis.cc");
      printf("\n             Empty list of cdelta-mdelta relationships to plot.");
      printf("\n             => abort()\n\n");
      abort();
   }


   // cvir distribution
   gDM_LOGCDELTA_STDDEV = sigma_cvir;
   if (gDM_LOGCDELTA_STDDEV > 1.e-5)
      gDM_FLAG_CDELTA_DIST = kLOGNORM;
   else
      gDM_FLAG_CDELTA_DIST = kDIRAC;

   nlevel += 1; // second level is substructure of each halo.
   // Boost level
   gDM_SUBS_NUMBEROFLEVELS = nlevel;

   // Uncomment to run all these profiles
   //const int n_prof = 4;
   //char name_prof[n_prof][20] = {"M98", "NFW97", "EINASTO_N04", "BURKERT"};
   //double par_prof[n_prof][4] = { {1.5, 3., 1.5, 0}, {1., 3., 1, 0}, {0.17, 0., 0., 1}, {0., 0., 0., 4}};
   const int n_prof = 1;
   char name_prof[n_prof][20] = {"CosmicHalo"};
   double par_prof[n_prof][10];

   par_prof[0][2] = gHALO_SUBS_SHAPE_PARAMS[kEXTRAGAL][0];
   par_prof[0][3] = gHALO_SUBS_SHAPE_PARAMS[kEXTRAGAL][1];
   par_prof[0][4] = gHALO_SUBS_SHAPE_PARAMS[kEXTRAGAL][2];
   par_prof[0][5] = gHALO_SUBS_FLAG_PROFILE[kEXTRAGAL];

   double z = gSIM_REDSHIFT;
   double Delta_c = delta_x_to_delta_crit(gCOSMO_DELTA0, gCOSMO_FLAG_DELTA_REF, z);

   const int n_cm = v_cvir.size();
   double step = pow(mmax / mmin, 1. / (double)(n_m - 1));

   const int nlev_test = 6;
   double m[n_m];
   double cdelta[n_cm][n_m];
   double lum_nosubs[n_prof][n_cm][n_m];
   double lum_nosubs_NFW_analytic[n_cm][n_m];
   double lum_subs[n_alpha][n_prof][n_cm][n_m];
   double lum_dpdlogm[n_alpha][n_prof][n_cm][n_m];
   double boost[n_alpha][n_prof][n_cm][n_m];
   double boost_rel[nlev_test][n_alpha][n_prof][n_cm][n_m];
   double lum_rel[n_alpha][n_prof][n_cm][n_m];
   const int n_cmean = 2;
   vector <double> cvir_val[n_cmean], dpdc_val[n_cmean], dpdclum_val[n_cmean], lum_val[n_cmean];
   double cvir_mean[n_cmean];
   int j_cvir_mean = -1;
   int i_mean = -1;

   // ASCII Outputs/prints
   int n_files = 2;
   if (gDM_SUBS_NUMBEROFLEVELS == 1) n_files = 1;

   FILE *fp[2] = {NULL, NULL};
   FILE *fp_current;
   string f_name1 = gSIM_OUTPUT_DIR + "c_lum_m.output";
   string f_name2 = gSIM_OUTPUT_DIR + "boost_m_alpha.output";
   fp[0] = fopen(f_name1.c_str(), "w");
   if (gDM_SUBS_NUMBEROFLEVELS > 1) fp[1] = fopen(f_name2.c_str(), "w");

   if (gSIM_IS_PRINT) {
      // Print header
      fp_current = fp[0];
      fprintf(fp_current, "\n");
      fprintf(fp_current, "# mass mdelta ");
      for (int j = 0; j < n_cm; ++j) {
         fprintf(fp_current, "cdelta-mdelta[%s] ", gNAMES_CDELTAMDELTA[v_cvir[j]]);
         for (int k = 0; k < n_prof; ++k) {
            for (int a = 0; a < n_alpha; ++a) {
               if (gDM_SUBS_NUMBEROFLEVELS == 1) {
                  if (gPP_DM_IS_ANNIHIL_OR_DECAY && z > SMALL_NUMBER) fprintf(fp_current, "Proper luminosity[%s,DMmodel%1d] ", gNAMES_CDELTAMDELTA[v_cvir[j]], k);
                  else fprintf(fp_current, "luminosity[%s,DMmodel%1d] ", gNAMES_CDELTAMDELTA[v_cvir[j]], k);
               } else {
                  if (gPP_DM_IS_ANNIHIL_OR_DECAY && z > SMALL_NUMBER) fprintf(fp_current, "Proper luminosity[%s,DMmodel%1d,alpham=%.1f] ", gNAMES_CDELTAMDELTA[v_cvir[j]], k, v_alpham[a]);
                  else  fprintf(fp_current, "luminosity[%s,DMmodel%1d,alpham=%.1f] ", gNAMES_CDELTAMDELTA[v_cvir[j]], k, v_alpham[a]);
               }
            }
         }
      }
      fprintf(fp_current, "\n");
      fprintf(fp_current, "# [Msol]     ");
      for (int j = 0; j < n_cm; ++j) {
         fprintf(fp_current, "[-]         ");
         for (int k = 0; k < n_prof; ++k) {
            for (int a = 0; a < n_alpha; ++a) {
               if (gPP_DM_IS_ANNIHIL_OR_DECAY) fprintf(fp_current, "L/mdelta/rho_0m [-] ");
               else fprintf(fp_current, "L/mdelta [-]      ");
            }
         }
      }
      fprintf(fp_current, "\n");

      if (gDM_SUBS_NUMBEROFLEVELS > 1) {
         fp_current = fp[1];
         fprintf(fp_current, "\n");
         fprintf(fp_current, "# mass   ");
         for (int j = 0; j < n_cm; ++j) {
            for (int k = 0; k < n_prof; ++k) {
               for (int a = 0; a < n_alpha; ++a) {
                  fprintf(fp_current, "boost[%s,DMmodel%1d,alpham=%.1f] ", gNAMES_CDELTAMDELTA[v_cvir[j]], k, v_alpham[a]);
               }
            }
         }
         fprintf(fp_current, "\n");
         fprintf(fp_current, "# [Msol]        ");
         for (int j = 0; j < n_cm; ++j) {
            for (int k = 0; k < n_prof; ++k) {
               for (int a = 0; a < n_alpha; ++a) {
                  fprintf(fp_current, "[-]           ");
               }
            }
         }
         fprintf(fp_current, "\n");
      }
   }

   // Calculate cvir and lum
   cout << ">>>>> cdelta(M) and L(M) for various cdelta-mdelta and profiles" << endl;

   // Loop on masses
   for (int i = 0; i < n_m; ++i) {
      m[i] = mmin * pow(step, i);
      cout << "  ... processing mass = " << m[i] << " Msol ..." << endl;

      if (gSIM_IS_PRINT) {
         for (int ff = 0; ff < n_files; ++ff) {
            fp_current = fp[ff];
            fprintf(fp_current, "%.*le    ", gSIM_SIGDIGITS + 2, m[i]);
         }
      }
      // Loop on concentrations
      for (int j = 0; j < n_cm; ++j) {
         int j_cvir = v_cvir[j];
         gHALO_SUBS_FLAG_CDELTAMDELTA[kEXTRAGAL] = j_cvir;
         //printf("  - Calculate for c-m[%d]=%s\n", j_cvir, gNAMES_CDELTAMDELTA[j_cvir]);
         cdelta[j][i] = mdelta_to_cdelta(m[i], Delta_c, j_cvir, &par_prof[0][2], z);
         //cout << "c_delta: " << cdelta[j][i] << ", c_vir: " << c_vir << endl;

         if (gSIM_IS_PRINT) {
            fprintf(fp[0], "%.*le    ", gSIM_SIGDIGITS, cdelta[j][i]);
         }

         // Loop on DM profiles
         for (int k = 0; k < n_prof; ++k) {
            //printf("      -> profile[%d]=%s\n", k, name_prof[k]);

            // Calculate halo parameters (from mass)
            const int n_par = 7;
            double par_host[n_par];
            //  par_host[0]   Halo density normalisation [kpc/Myr^{-3}]
            //  par_host[1]   Halo scale radius, based on mdelta_to_rscale() [kpc]
            //  par_host[2-4] Shape parameter #1,2,3 (=par_prof[0,3])
            //  par_host[5]   card_profile (=par_prof[3])
            //  par_host[6]   Virial radius, based on mdelta_to_Rdelta() [kpc]

            mdelta_to_par(par_host, m[i], Delta_c, j_cvir, &par_prof[k][2], gSIM_EPS, z);

            double rho_core;
            double r0 = 0.;
            rho(r0, par_host, rho_core);
            if (j_cvir == kROCHA13_SIDM_VIR) {
               cout << "      k" << gNAMES_CDELTAMDELTA[j_cvir] << ":  \tSIDM rho_core = " << rho_core * 1e-9 << " Msol/pc^3, R_s = "  << par_host[1] << " kpc" << endl;
            }
            // Calculate luminosity (from mass)
            const int n_par_tot = 21;
            double par_mix[n_par_tot];
            for (int n = 0; n < 7; ++n)
               par_mix[n] = par_host[n];
            for (int n = 7; n < n_par_tot; ++n)
               par_mix[n] = 0.;
            //  par[0-6]     rho_1(r): norm [Msol/kpc^{-3}] + rs [kpc] + shape #(1,2,3) + gENUM_PROFILE + Rmax
            //  par[7-9]     l_HC,psi_HC,theta_HC: distance, longitude, and latitude to rho_1 centre [kpc,rad,rad]
            //  par[10]      switch_rho: selects which combination of rho_1 and rho_2 to use
            //                  0 -> rho(r) = rho_1(r)
            //                  1 -> rho(r) = rho_1(r)-rho_2(r)
            //                  2 -> rho(r) = rho_1(r)*rho_2(r)
            //  par[11-17]   rho_2(r): norm [Msol/kpc^{-3}] + rs [kpc] + shape #(1,2,3) + gENUM_PROFILE + Rmax
            //  par[18-20]   l_HC,psi_HC,theta_HC: distance, longitude, and latitude to rho_2 centre [kpc,rad,rad]
            lum_nosubs[k][j][i] = lum_singlehalo_nosubs_mix(par_mix, 0.01 * gSIM_EPS);

            // cross-check luminosity for NFW profile with analytic expression:
            if (par_prof[k][5] == kZHAO and par_prof[k][2] == 1 and par_prof[k][3] == 3 and par_prof[k][4] == 1) {
               double f_c = 1. / 9 * pow(cdelta[j][i], 3.) * (1 - pow(1 + cdelta[j][i], -3)) /
                            pow(log1p(cdelta[j][i]) - cdelta[j][i] / (1 + cdelta[j][i]), 2);
               lum_nosubs_NFW_analytic[j][i] = m[i] * Delta_c * rho_crit(z) * f_c;
               cout << "      Proper luminosity numerical: " << lum_nosubs[k][j][i] << " analytical: " << lum_nosubs_NFW_analytic[j][i] << endl;
            }

            // Calculate multi-level boost (from mass)
            // loop on dP/dM slope
            for (int a = 0; a < n_alpha; ++a) {

               // Fill par_subs for mean1cl_lumn_r_m() calls!
               const int n_subs = 24;
               double par_subs[n_subs];
               //  par_subs[0-4]   rho_cl(r): shape #(1,2,3) + gENUM_PROFILE + Rmax
               //  par_subs[5]     eps: relative precision sought L calculation
               //  par_subs[6]     z: redshift of the halo hosting the sub-clumps
               //  par_subs[7]     mdelta: mass of rho_cl(r)
               //  par_subs[8]     dPdM normalisation [1/Msol]
               //  par_subs[9]     dPdM slope alphaM
               //  par_subs[10]    dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)      [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
               //  par_subs[11]    dPdc mean concentration <c>          [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
               //  par_subs[12]    dPdc standard deviation              [only for ANNIHILATION and nlevel>1]
               //  par_subs[13]    dPdc card_profile [gENUM_CDELTA_DIST]  [only for ANNIHILATION and nlevel>1]
               //  par_subs[14]    f: mass fraction of substructures    [only for ANNIHILATION and nlevel>1]
               //  par_subs[15]    nlevel: sub-sub...halos (1=no sub)   [only for ANNIHILATION and nlevel>1]
               //  par_subs[16-23] dPdV: norm [kpc^{-3}] + rs [kpc] + shape #(1,2,3) + gENUM_PROFILE + Rmax
               for (int g = 0; g < 5; ++g)
                  // subhalo properties same as host halo properties
                  par_subs[g] = par_host[g + 2];
               par_subs[5] = gSIM_EPS;
               par_subs[6] = z;
               par_subs[7] = m[i];
               par_subs[8] = 1.;
               par_subs[9] = v_alpham[a];
               double msub_min = gDM_SUBS_MMIN;
               double msub_max = m[i] * gDM_SUBS_MMAXFRAC;
               dpdm_setnormprob(&par_subs[8], msub_min, msub_max, gSIM_EPS);
               par_subs[10] = j_cvir;
               par_subs[11] = mdelta_to_cdelta(m[i], Delta_c, j_cvir, &par_prof[k][2], z);// concentration of host halo
               par_subs[12] = gDM_LOGCDELTA_STDDEV;
               par_subs[13] = gDM_FLAG_CDELTA_DIST;
               par_subs[14] = f_dm;
               par_subs[15] = nlevel;
               // dPdV properties
               // par_subs[16] will be set below
               par_subs[17] = gHALO_SUBS_DPDV_RSCALE_TO_RS_HOST[kEXTRAGAL] * par_host[1]; // scale radius
               par_subs[18] = gHALO_SUBS_DPDV_SHAPE_PARAMS[kEXTRAGAL][0];
               par_subs[19] = gHALO_SUBS_DPDV_SHAPE_PARAMS[kEXTRAGAL][1];
               par_subs[20] = gHALO_SUBS_DPDV_SHAPE_PARAMS[kEXTRAGAL][2];

               if (gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] == kDPDV_PIERI11 and par_subs[14] > SMALL_NUMBER) {
                  // update r_s and shape parameter:
                  gsl_function F;
                  F.function = &solve_rbias;
                  rbias_params_for_rootfinding params = {par_host[9], par_host[6], par_host[0], par_host[1],
                                                         par_host[2], par_host[3], par_host[4], int(par_host[5]), par_subs[14]
                                                        };
                  F.params = &params;
                  double rmin = 1e-5;
                  double rmax = 1e5;
                  int return_status = 0;
                  double rbias = rootsolver_gsl(gsl_root_fsolver_falsepos, F, rmin, rmax, gSIM_EPS, return_status); // faster than gsl_root_fsolver_brent and gsl_root_fsolver_bisection
                  par_subs[17] = par_host[1];
                  par_subs[18] = rbias;
                  par_subs[19] = 0;
                  par_subs[20] = 0;
               }

               par_subs[21] = gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL];
               par_subs[22] = par_host[6]; // Rvir
               par_subs[23] = gHALO_SUBS_DPDV_RSCALE_TO_RS_HOST[kEXTRAGAL];

               dpdv_setnormprob(&par_subs[16], gSIM_EPS);

               // Fill par_host for lum_singlehalo_subs() calls!
               // N.B.: lum_singlehalo_subs() and  mean1cl_lumn_r_m() are not equivalent as the former
               // calculate L for a single host and its subhalos population, whereas the latter
               // calculate L for a distribution of haloes of mass M with dP/dc (and possibly
               // sub-halos)
               //  par_host[0-6]   rho_cl(r): norm + rs + shape #(1,2,3) + gENUM_PROFILE + Rmax
               //  par_host[7]     eps: relative precision sought L calculation
               //  par_host[8]     z: redshift of the halo hosting the sub-clumps
               //  par_host[9]     mdelta: mass of rho_cl(r)
               //  par_host[10]    dPdM normalisation [1/Msol]
               //  par_host[11]    dPdM slope alphaM
               //  par_host[12]    dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)      [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
               //  par_host[13]    dPdc mean concentration <c>          [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
               //  par_host[14]    dPdc standard deviation              [only for ANNIHILATION and nlevel>1]
               //  par_host[15]    dPdc card_profile [gENUM_CDELTA_DIST]  [only for ANNIHILATION and nlevel>1]
               //  par_host[16]    f: mass fraction of substructures    [only for ANNIHILATION and nlevel>1]
               //  par_host[17]    nlevel: sub-sub...halos (1=no sub)   [only for ANNIHILATION and nlevel>1]
               //  par_host[18-24] dPdV: norm [kpc^{-3}] + rs [kpc] + shape #(1,2,3) + gENUM_PROFILE + Rmax
               const int n_parhost = 26;
               double par_host_alpha[n_parhost];
               for (int g = 0; g < 24; ++g)
                  par_host_alpha[g + 2] = par_subs[g];
               mdelta_to_par(par_host_alpha, m[i], Delta_c, j_cvir, &par_subs[0], gSIM_EPS, z, par_subs[11] /*cdelta already calculated*/);
               // Calling one or the other is equivalent (one level difference)!
               par_host_alpha[17] = nlevel;

               // Calculate total boost
               lum_subs[a][k][j][i] = lum_singlehalo_subs(par_host_alpha);
               if (gDM_SUBS_NUMBEROFLEVELS > 1 and par_subs[14] > 1e-5)
                  boost[a][k][j][i] = lum_subs[a][k][j][i] / lum_nosubs[k][j][i];
               else boost[a][k][j][i] = 1.;

               if (gSIM_IS_PRINT) {
                  if (gPP_DM_IS_ANNIHIL_OR_DECAY) fprintf(fp[0], "%.*le     ", gSIM_SIGDIGITS, lum_subs[a][k][j][i] / m[i] / gCOSMO_RHO0_C / gCOSMO_OMEGA0_M);
                  else fprintf(fp[0], "%.*le     ", gSIM_SIGDIGITS, lum_subs[a][k][j][i] / m[i]);
                  if (gDM_SUBS_NUMBEROFLEVELS > 1) fprintf(fp[1], "%.*le  ", gSIM_SIGDIGITS, boost[a][k][j][i]);
               }

               // Calculate relative boost contributions of various levels
               // (simplify for no dPdc for speed)
               double sdtdev_ref = gDM_LOGCDELTA_STDDEV;
               int flag_dpdc_ref = gDM_FLAG_CDELTA_DIST;
               gDM_LOGCDELTA_STDDEV = 0.;
               gDM_FLAG_CDELTA_DIST = kDIRAC;
               for (int bb = 3; bb < nlev_test; ++bb) {
                  static bool is_print_warning = false;
                  double boost_ref, boost_current;
                  if (gDM_SUBS_NUMBEROFLEVELS > 1 && (gSIM_IS_DISPLAY || gSIM_IS_WRITE_ROOTFILES)) {
                     // Ref is calculation from previous level
                     par_host_alpha[17] = bb - 1;
                     boost_ref = lum_singlehalo_subs(par_host_alpha);
                     // Current calculation
                     par_host_alpha[17] = bb;
                     boost_current = lum_singlehalo_subs(par_host_alpha);
                     if (!is_print_warning) {
                        print_warning("janalysis.cc", "plot_c_lum_boost_vs_m()",
                                      "Highly time-consuming multi-substructure computation is done. "
                                      "You can bypass it by neither displaying ROOT plots (use -p flag) "
                                      "nor writing ROOT files (use --gSIM_IS_WRITE_ROOTFILES=0 flag).");
                        is_print_warning = true;
                     }
                  } else {
                     boost_ref = 1.;
                     boost_current = 1.;
                     if (!is_print_warning) {
                        print_warning("janalysis.cc", "plot_c_lum_boost_vs_m()",
                                      "No multi-level boost calculation done. Switched off because either "
                                      "gDM_SUBS_NUMBEROFLEVELS=0 is chosen"
                                      " or '-p' flag and gSIM_IS_WRITE_ROOTFILES=0 are chosen.");
                        is_print_warning = true;
                     }
                  }
                  // Relative difference from one level to another
                  boost_rel[bb][a][k][j][i] = (boost_current - boost_ref) / boost_ref * 100.;
                  //cout << "nlevel=" << bb << " boost_ref=" << boost_ref << " boost_current=" << boost_current
                  //     << "   rel-diff=" << boost_rel[bb][a][k][j][i] << endl;
               }
               gDM_LOGCDELTA_STDDEV = sdtdev_ref;
               gDM_FLAG_CDELTA_DIST = flag_dpdc_ref;

               // Calculate relative effect of dP/dc distribution
               if (gDM_LOGCDELTA_STDDEV > 1.e-5) {
                  // Calculate a 'zero' level of subs
                  par_subs[15] = 1;
                  double res_mean1m = mean1cl_lumn_r_m(par_subs, 1);
                  lum_rel[a][k][j][i] = (res_mean1m - lum_nosubs[k][j][i]) / lum_nosubs[k][j][i] * 100.;
               }


               // Calculate integrand lum*dp/dlogM
               double res_lumdpdlogm = 0.;
               dpdm_lum_r_m(m[i], par_subs, res_lumdpdlogm);
               lum_dpdlogm[a][k][j][i] = res_lumdpdlogm * m[i];


               // Calculate concentration
               if ((i == 0 || i == (n_m - 1)) && i_mean < n_cmean - 1 && k == 0 && a == 0 && sigma_cvir > 1.e-5) {
                  ++i_mean;
                  j_cvir_mean = j_cvir;

                  double dpdc_lum_cmean = 0., dpdc_cmean = 0.;
                  dpdc_lum(par_subs[11], par_subs, dpdc_lum_cmean);
                  dpdc(par_subs[11], &par_subs[11], dpdc_cmean);
                  double lum_cmean = lum_singlehalo_nosubs(par_host_alpha, gSIM_EPS);

                  cvir_mean[i_mean] = par_subs[11];
                  double cvir_min = 0.1 * par_subs[11];
                  double cvir_max = 10.* par_subs[11];
                  cvir_val[i_mean].clear();
                  dpdclum_val[i_mean].clear();
                  dpdc_val[i_mean].clear();
                  lum_val[i_mean].clear();
                  for (double cvir = cvir_min; cvir < cvir_max; cvir *= 1.02) {
                     cvir_val[i_mean].push_back(cvir);
                     double tmp_res = 0.;
                     dpdc_lum(cvir, par_subs, tmp_res);
                     dpdclum_val[i_mean].push_back(tmp_res / dpdc_lum_cmean);
                     dpdc(cvir, &par_subs[11], tmp_res);
                     dpdc_val[i_mean].push_back(tmp_res / dpdc_cmean);

                     for (int iii = 0; iii < n_parhost - 2; ++iii)
                        par_host_alpha[iii + 2] = par_subs[iii];
                     mdelta_to_par(par_host_alpha, m[i], Delta_c, j_cvir, &par_subs[0], gSIM_EPS, z, cvir /*cdelta already calculated*/);
                     double tmp_lum = lum_singlehalo_nosubs(par_host_alpha, gSIM_EPS);
                     //lum = lum_singlehalo_subs(par_host);
                     lum_val[i_mean].push_back(tmp_lum / lum_cmean);
                  }
               }
            }
         }
      }
      if (gSIM_IS_PRINT) {
         for (int ff = 0; ff < n_files; ++ff) {
            fp_current = fp[ff];
            fprintf(fp_current, "\n");
         }
      }
   }

   if (gSIM_IS_PRINT) {
      for (int ff = 0; ff < n_files; ++ff) {
         fp_current = fp[ff];
         fclose(fp_current);
      }
      cout << endl;
      cout << "_______________________" << endl << endl;
      cout << " ... output [ASCII] written in: " << endl;
      cout << "       " << f_name1 << endl;
      if (gDM_SUBS_NUMBEROFLEVELS > 1) cout << "       " << f_name2 << endl << endl;
   }

#if IS_ROOT
   // Displays
   if (gSIM_IS_DISPLAY or gSIM_IS_WRITE_ROOTFILES) {

      // Save .root format?
      string f_root = gSIM_OUTPUT_DIR + "c_lum_boost.root";
      TFile *root_file = NULL;
      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file = new TFile(f_root.c_str(), "recreate");
      }

      // cdelta-mdelta
      TMultiGraph *multi_cvir = new TMultiGraph();
      TLegend *leg_cvir = new TLegend(0.65, 0.5, 0.99, 0.92);
      leg_cvir->SetHeader("c_{#Delta}-M_{#Delta} flag");

      // lum(m)
      TMultiGraph *multi_lum = new TMultiGraph();
      TLegend *leg_lumprof = new TLegend(0.2, 0.6, 0.4, 0.92);
      //->SetHeader("DM profile");
      TLegend *leg_lumcvir = new TLegend(0.49, 0.15, 0.95, 0.57);
      leg_lumcvir->SetHeader("c_{#Delta}-M_{#Delta} flag");

      // lum_dpdlogm(m)
      TMultiGraph *multi_lumdpdlogm = new TMultiGraph();

      // boost(m)
      TMultiGraph *multi_boost = new TMultiGraph();
      TLegend *leg_lumcvir_red = new TLegend(0.15, 0.15, 0.75, 0.5);
      char header[500];
      sprintf(header, "#alpha_{M}=(%s), n_level=%d, %s (#sigma_{log c}=%.2f)", list_alpham.c_str(), nlevel, gNAMES_CVIR_DIST[kLOGNORM], sigma_cvir);
      leg_lumcvir_red->SetHeader(header);

      // boost_rel(m)
      TMultiGraph *multi_boostrel = new TMultiGraph();
      TLegend *leg_boostrel = new TLegend(0.45, 0.35, 0.95, 0.65);

      TGraph **gr_cvir = new TGraph*[n_cm];
      TGraph **gr_lum = new TGraph*[n_alpha * n_prof * n_cm];
      TGraph **gr_lumdpdm = new TGraph*[n_alpha * n_prof * n_cm];
      TGraph **gr_boost = new TGraph*[n_alpha * n_prof * n_cm];
      TGraph **gr_boostrel = new TGraph*[nlev_test * n_alpha * n_prof * n_cm];
      for (int j = 0; j < n_cm; ++j) {
         int j_cvir = v_cvir[j];
         // cdelta-mdelta
         gr_cvir[j] = new TGraph();
         char tmp[200];
         sprintf(tmp, "gr_cvir_%s", gNAMES_CDELTAMDELTA[j_cvir]);
         gr_cvir[j]->SetName(tmp);
         int i_used = 0;
         for (int i = 0; i < n_m; ++i) {
            if ((j_cvir == kNETO07_200 || j_cvir == kDUFFY08F_VIR || j_cvir == kDUFFY08F_200 || j_cvir == kDUFFY08F_MEAN
                  || j_cvir == kETTORI10_200 || j_cvir == kPRADA12_200) && m[i] < 1.e8) {
               static bool is_print_warning_cdelta = false;
               if (!is_print_warning_cdelta) {
                  print_warning("janalysis.cc", "plot_c_lum_boost_vs_m()",
                                "profile " + string(gNAMES_CDELTAMDELTA[j_cvir]) + " only valid for masses > 10^8 Msol.");
                  is_print_warning_cdelta = true;
               }
               //continue;
            }
            gr_cvir[j]->SetPoint(i_used, m[i], cdelta[j][i]);
            ++i_used;
         }
         gr_cvir[j]->SetLineColor(rootcolor(j_cvir));
         gr_cvir[j]->SetLineWidth(3);
         gr_cvir[j]->SetLineStyle(j_cvir + 1);
         multi_cvir->Add(gr_cvir[j], "L");

         sprintf(tmp, "%g", z);
         string label_cvir = string(gNAMES_CDELTAMDELTA[j_cvir]) + ", z = " + string(tmp);

         TLegendEntry *entry = leg_cvir->AddEntry(gr_cvir[j], label_cvir.c_str(), "L");
         entry->SetTextColor(rootcolor(j_cvir));

         if (gSIM_IS_WRITE_ROOTFILES) {
            root_file->cd();
            gr_cvir[j]->Write();
         }

         // lum(m)
         int n_prof_start = 0;
         int n_prof_stop = n_prof;
         for (int k = n_prof_start; k < n_prof_stop; ++k) {
            for (int a = 0; a < n_alpha; ++a) {

               sprintf(tmp, "%g", z);
               string label_lum = string(gNAMES_CDELTAMDELTA[j_cvir]) + ", z = " + string(tmp)  + ", #alpha_{M} = ";
               sprintf(tmp, "%g", v_alpham[a]);
               label_lum += string(tmp);

               // dP/dM (hence lumdpdlogm) for m_host such as gDM_SUBS_MMIN> m_host*gDM_SUBS_MMAXFRAC is null
               int i_start = 0;
               while (m[i_start]*gDM_SUBS_MMAXFRAC < gDM_SUBS_MMIN)
                  ++i_start;
               int i_gen = a * n_prof * n_cm + k * n_cm + j;

               gr_lum[i_gen] = new TGraph();
               int ii_used = 0;
               for (int i = 0; i < n_m; ++i) {
                  if ((j_cvir == kNETO07_200 || j_cvir == kDUFFY08F_VIR || j_cvir == kDUFFY08F_200 || j_cvir == kDUFFY08F_MEAN
                        || j_cvir == kETTORI10_200) && m[i] < 1.e8)
                     continue;
                  if (gPP_DM_IS_ANNIHIL_OR_DECAY) gr_lum[i_gen]->SetPoint(ii_used, m[i], lum_subs[a][k][j][i] / m[i] / gCOSMO_RHO0_C / gCOSMO_OMEGA0_M);
                  else gr_lum[i_gen]->SetPoint(ii_used, m[i], lum_subs[a][k][j][i] / m[i]);
                  ++ii_used;
               }
               gr_lum[i_gen]->SetLineColor(rootcolor(j_cvir));
               gr_lum[i_gen]->SetLineWidth(a * 2 + 2);
               gr_lum[i_gen]->SetLineStyle(k + 1);
               char tmp2[500];
               sprintf(tmp2, "gr_lum_%2d_%s_%s", int(v_alpham[a] * 10.), gNAMES_CDELTAMDELTA[j_cvir], name_prof[k]);
               gr_lum[i_gen]->SetName(tmp2);
               multi_lum->Add(gr_lum[i_gen], "L");
               //if (j == 0)
               //leg_lumprof->AddEntry(gr_lum[k * n_cm + j], name_prof[k], "L");
               if (k == 0) {
                  entry = leg_lumcvir->AddEntry(gr_lum[i_gen], label_lum.c_str(), "L");
                  entry->SetTextColor(rootcolor(j_cvir));
               }

               if (gSIM_IS_WRITE_ROOTFILES) {
                  root_file->cd();
                  gr_lum[i_gen]->Write();
               }

               // lum_dpdlogm(m), boost(m), and boostrel(m)
               if (j_cvir == kNETO07_200 || j_cvir == kDUFFY08F_VIR || j_cvir == kDUFFY08F_200 || j_cvir == kDUFFY08F_MEAN
                     || j_cvir == kETTORI10_200)
                  continue;
               // lumdpdlogm

               gr_lumdpdm[i_gen] = new TGraph(n_m - i_start, &m[i_start], &lum_dpdlogm[a][k][j][i_start]);
               gr_lumdpdm[i_gen]->SetLineColor(rootcolor(j_cvir));
               gr_lumdpdm[i_gen]->SetLineWidth(a * 2 + 2);
               gr_lumdpdm[i_gen]->SetLineStyle(k + 1);
               char tmp3[500];
               sprintf(tmp3, "gr_lumdpdlogm_%2d_%s_%s", int(v_alpham[a] * 10.), gNAMES_CDELTAMDELTA[j_cvir], name_prof[k]);
               gr_lumdpdm[i_gen] ->SetName(tmp3);
               multi_lumdpdlogm->Add(gr_lumdpdm[i_gen], "L");
               if (gSIM_IS_WRITE_ROOTFILES) {
                  root_file->cd();
                  gr_lumdpdm[i_gen]->Write();
               }

               // boost
               gr_boost[i_gen] = new TGraph(n_m, m, boost[a][k][j]);
               gr_boost[i_gen]->SetLineColor(rootcolor(j_cvir));
               gr_boost[i_gen]->SetLineWidth(a * 2 + 2);
               gr_boost[i_gen]->SetLineStyle(k + 1);
               sprintf(tmp3, "gr_boost_%2d_%s_%s", int(v_alpham[a] * 10.), gNAMES_CDELTAMDELTA[j_cvir], name_prof[k]);
               gr_boost[i_gen] ->SetName(tmp3);
               multi_boost->Add(gr_boost[i_gen], "L");

               if (k == 0) {
                  entry = leg_lumcvir_red->AddEntry(gr_boost[i_gen], label_lum.c_str(), "L");
                  entry->SetTextColor(rootcolor(j_cvir));
               }

               if (gSIM_IS_WRITE_ROOTFILES) {
                  root_file->cd();
                  gr_boost[i_gen]->Write();
               }

               // boostrel
               for (int bb = 2; bb < nlev_test; ++bb) {
                  int i_gen2 = bb * n_alpha * n_prof * n_cm + a * n_prof * n_cm + k * n_cm + j;
                  // Use gr_boostrel for bb=0 to add this graph!
                  if (bb == 2) {
                     if (gDM_LOGCDELTA_STDDEV < 1.e-5)
                        continue;
                     else
                        gr_boostrel[i_gen2] = new TGraph(n_m, m, lum_rel[a][k][j]);
                  } else
                     gr_boostrel[i_gen2] = new TGraph(n_m, m, boost_rel[bb][a][k][j]);
                  gr_boostrel[i_gen2]->SetLineColor(rootcolor(bb));
                  gr_boostrel[i_gen2]->SetLineWidth(a * 2 + 2);
                  gr_boostrel[i_gen2]->SetLineStyle(k + 1);
                  sprintf(tmp3, "gr_boostrel_%2d_%s_%s_lev%d", int(v_alpham[a] * 10.), gNAMES_CDELTAMDELTA[j_cvir], name_prof[k], bb - 1);
                  gr_boostrel[i_gen2] ->SetName(tmp3);
                  multi_boostrel->Add(gr_boostrel[i_gen2], "L");

                  if (k == 0 && a == 0) {
                     char levels[200];
                     if (bb == 2)
                        sprintf(levels, "Effect of #sigma_{log c}=%.2f", sigma_cvir);
                     else
                        sprintf(levels, "Effect of level %d (subs)", bb - 1);
                     entry = leg_boostrel->AddEntry(gr_boostrel[i_gen2], levels, "L");
                     entry->SetTextColor(rootcolor(bb));
                  }

                  if (gSIM_IS_WRITE_ROOTFILES) {
                     root_file->cd();
                     gr_boostrel[i_gen2]->Write();
                  }
               }
            }
         }
      }
      //multi_cvir->Print();

      string Delta_str = legend_for_delta();

      //--- c_cvir for halos
      TGraph *gr_c_comp = new TGraph[1];
      // add comparison data:
      for (int j = 0; j < n_cm; ++j) {
         int j_cvir = v_cvir[j];
         string label_literature;
         if (j_cvir == kLUDLOW16_200) {
            string label_base = "Ludlow et al. (2016)";
            string Delta_lit;
            if (gCOSMO_FLAG_DELTA_REF == kRHO_MEAN) {
               char Delta_c_char[200];
               sprintf(Delta_c_char, "%d", int(round(delta_crit_to_delta_x(200, kRHO_MEAN, z))));
               Delta_lit = "#Delta_{m} = " + string(Delta_c_char);
            } else {
               Delta_lit = "#Delta_{c} = 200";
            }

            vector<double> logMh_base; // [Msol/h]
            vector<double> logc_base;
            int n_points;
            if (z < 0.75) {
               n_points = 16;
               double ludlow16_figC1_arr_z0[16][2] = {
                  { -6.96, 1.6219},
                  { -4.57, 1.5844},
                  { -2.62, 1.5469},
                  { -0.72, 1.5047},
                  { 2.02, 1.4359},
                  { 4.81, 1.3484},
                  { 6.74, 1.2750},
                  { 8.64, 1.1813},
                  {10.50, 1.0656},
                  {12.27, 0.9219},
                  {13.41, 0.7984},
                  {14.26, 0.6875},
                  {15.16, 0.5688},
                  {16.05, 0.4594},
                  {16.55, 0.3969},
                  {17.02, 0.3406},
               };
               for (int i = 0; i < n_points; ++i) {
                  logMh_base.push_back(ludlow16_figC1_arr_z0[i][0]);
                  logc_base.push_back(ludlow16_figC1_arr_z0[i][1]);
               }
               label_literature = (label_base + ", z = 0, " + Delta_lit);
            }  else if (z < 1.5 and z >= 0.75) {
               n_points = 16;
               double ludlow16_figC1_arr_z1[16][2] = {
                  { -6.96, 1.4438},
                  { -4.63, 1.4094},
                  { -2.60, 1.375 },
                  { -0.52, 1.3313},
                  { 1.30, 1.2859},
                  { 3.91, 1.2125},
                  { 5.87, 1.1453},
                  { 7.79, 1.0625},
                  { 9.55, 0.9688},
                  {11.53, 0.8359},
                  {12.98, 0.7063},
                  {14.28, 0.5750},
                  {15.10, 0.4922},
                  {15.70, 0.4438},
                  {16.20, 0.4000},
                  {17.09, 0.3359},
               };
               for (int i = 0; i < n_points; ++i) {
                  logMh_base.push_back(ludlow16_figC1_arr_z1[i][0]);
                  logc_base.push_back(ludlow16_figC1_arr_z1[i][1]);
               }
               label_literature = (label_base + ", z = 1, " + Delta_lit);
            } else if (z < 2.5 and z >= 1.5) {
               n_points = 12;
               double ludlow16_figC1_arr_z2[12][2] = {
                  { -6.96, 1.2984},
                  { -3.55, 1.2391},
                  { 0.08, 1.1688},
                  { 3.06, 1.0906},
                  { 5.91, 1.0016},
                  { 8.72, 0.8797},
                  {10.70, 0.7656},
                  {12.44, 0.6406},
                  {14.11, 0.5063},
                  {15.35, 0.4156},
                  {16.34, 0.3641},
                  {17.09, 0.3219},
               };
               for (int i = 0; i < n_points; ++i) {
                  logMh_base.push_back(ludlow16_figC1_arr_z2[i][0]);
                  logc_base.push_back(ludlow16_figC1_arr_z2[i][1]);
               }
               label_literature = (label_base + ", z = 2, " + Delta_lit);
            } else if (z < 3.5 and z >= 2.5) {
               n_points = 12;
               double ludlow16_figC1_arr_z3[12][2] = {
                  { -6.98, 1.1594},
                  { -3.64, 1.1063},
                  { -0.79, 1.0531},
                  { 1.92, 0.9891},
                  { 4.79, 0.9078},
                  { 7.64, 0.8031},
                  { 9.61, 0.7141},
                  {11.63, 0.5984},
                  {13.26, 0.4969},
                  {15.16, 0.3891},
                  {16.10, 0.3438},
                  {17.17, 0.3031},
               };
               for (int i = 0; i < n_points; ++i) {
                  logMh_base.push_back(ludlow16_figC1_arr_z3[i][0]);
                  logc_base.push_back(ludlow16_figC1_arr_z3[i][1]);
               }
               label_literature = (label_base + ", z = 3, " + Delta_lit);
            } else if (z < 6 and z >= 3.5) {
               n_points = 11;
               double ludlow16_figC1_arr_z5[11][2] = {
                  { -6.90, 0.9844},
                  { -3.62, 0.9391},
                  { -0.81, 0.8875},
                  { 2.07, 0.8266},
                  { 4.96, 0.7500},
                  { 7.71, 0.6625},
                  { 9.65, 0.5875},
                  {11.47, 0.5063},
                  {13.22, 0.4281},
                  {15.12, 0.3500},
                  {17.13, 0.2844},
               };
               for (int i = 0; i < n_points; ++i) {
                  logMh_base.push_back(ludlow16_figC1_arr_z5[i][0]);
                  logc_base.push_back(ludlow16_figC1_arr_z5[i][1]);
               }
               label_literature = (label_base + ", z = 5, " + Delta_lit);
            } else if (z >= 6) {
               n_points = 11;
               double ludlow16_figC1_arr_z7[11][2] = {
                  { -6.94, 0.8563},
                  { -4.65, 0.8250},
                  { -1.61, 0.7797},
                  { 1.07, 0.7313},
                  { 3.14, 0.6875},
                  { 5.99, 0.6172},
                  { 8.72, 0.5406},
                  {11.45, 0.4453},
                  {14.22, 0.3516},
                  {15.47, 0.3094},
                  {17.03, 0.2672},
               };
               for (int i = 0; i < n_points; ++i) {
                  logMh_base.push_back(ludlow16_figC1_arr_z7[i][0]);
                  logc_base.push_back(ludlow16_figC1_arr_z7[i][1]);
               }
               label_literature = (label_base + ", z = 7, " + Delta_lit);
            }

            char tmp[500];
            sprintf(tmp, "gr_cvir_comp%s", gNAMES_CDELTAMDELTA[j_cvir]);

            int n_used = 0;
            vector<double> m_used;
            vector<double> c_used;
            for (int i = 1; i < n_m; ++i) {
               double logmh_var = log10(m[i] * gCOSMO_HUBBLE);
               double logcinterpol = interp1D(logmh_var, logMh_base, logc_base, kLINLIN, true);
               if (logcinterpol > HPX_BLIND_VALUE) {
                  m_used.push_back(m[i]);
                  c_used.push_back(pow(10, logcinterpol));
                  ++n_used;
               }
            }
            gr_c_comp = new TGraph(n_used, &m_used[0], &c_used[0]);
            gr_c_comp->SetName(tmp);
            gr_c_comp->SetLineColor(rootcolor(j_cvir + 10));
            gr_c_comp->SetLineWidth(5);
            gr_c_comp->SetLineStyle(j_cvir + 1);
            multi_cvir->Add(gr_c_comp, "L");

            TLegendEntry *entry = leg_cvir->AddEntry(gr_c_comp, label_literature.c_str(), "L");
            entry->SetTextColor(rootcolor(j_cvir));
         } else if (j_cvir == kPRADA12_200) {
            string label_base = "Prada et al. (2012)";
            string Delta_lit;
            if (gCOSMO_FLAG_DELTA_REF == kRHO_CRIT) {
               char Delta_c_char[200];
               sprintf(Delta_c_char, "%d", int(round(delta_crit_to_delta_x(200, kRHO_CRIT, z))));
               Delta_lit = "#Delta_{c} = " + string(Delta_c_char);
            } else {
               Delta_lit = "#Delta_{c} = 200";
            }

            vector<double> logMh_base; // [Msol/h]
            vector<double> logc_base;
            int n_points;
            if (z < 0.25) {
               n_points = 11;
               double prada12_fig12_arr_z0[11][2] = {
                  {10.80, 0.9801},
                  {11.85, 0.8989},
                  {12.73, 0.8257},
                  {13.39, 0.7714},
                  {13.78, 0.7413},
                  {14.08, 0.7225},
                  {14.34, 0.7087},
                  {14.62, 0.7   },
                  {14.74, 0.6986},
                  {14.81, 0.6986},
                  {14.89, 0.7   },
               };
               for (int i = 0; i < n_points; ++i) {
                  logMh_base.push_back(prada12_fig12_arr_z0[i][0]);
                  logc_base.push_back(prada12_fig12_arr_z0[i][1]);
               }
               label_literature = (label_base + ", z = 0, " + Delta_lit);
            }  else if (z < 0.75 and z >= 0.25) {
               n_points = 14;
               double prada12_fig12_arr_z05[14][2] = {
                  {10.82, 0.8938},
                  {11.68, 0.8348},
                  {12.45, 0.7819},
                  {13.04, 0.7431},
                  {13.43, 0.7199},
                  {13.69, 0.7072},
                  {13.92, 0.6989},
                  {14.07, 0.6953},
                  {14.22, 0.6938},
                  {14.37, 0.6935},
                  {14.51, 0.6964},
                  {14.60, 0.7   },
                  {14.67, 0.7036},
                  {14.74, 0.7083},
               };
               for (int i = 0; i < n_points; ++i) {
                  logMh_base.push_back(prada12_fig12_arr_z05[i][0]);
                  logc_base.push_back(prada12_fig12_arr_z05[i][1]);
               }
               label_literature = (label_base + ", z = 0.5, " + Delta_lit);
            } else if (z < 1.5 and z >= 0.75) {
               n_points = 15;
               double prada12_fig12_arr_z1[15][2] = {
                  {10.81, 0.8207},
                  {11.56, 0.7775},
                  {12.29, 0.7373},
                  {12.79, 0.7123},
                  {13.15, 0.6978},
                  {13.42, 0.6891},
                  {13.61, 0.6870},
                  {13.79, 0.6855},
                  {13.94, 0.6866},
                  {14.06, 0.6902},
                  {14.20, 0.6960},
                  {14.31, 0.7025},
                  {14.38, 0.7087},
                  {14.46, 0.7152},
                  {14.55, 0.7257},
               };
               for (int i = 0; i < n_points; ++i) {
                  logMh_base.push_back(prada12_fig12_arr_z1[i][0]);
                  logc_base.push_back(prada12_fig12_arr_z1[i][1]);
               }
               label_literature = (label_base + ", z = 1, " + Delta_lit);
            } else if (z < 3.5 and z >= 1.5) {
               n_points = 14;
               double prada12_fig12_arr_z2[14][2] = {
                  {10.817, 0.6775},
                  {11.317, 0.6630},
                  {11.833, 0.6529},
                  {12.173, 0.6496},
                  {12.300, 0.6500},
                  {12.550, 0.6518},
                  {12.793, 0.6562},
                  {13.078, 0.6681},
                  {13.267, 0.6801},
                  {13.423, 0.6938},
                  {13.586, 0.7127},
                  {13.702, 0.7326},
                  {13.817, 0.7493},
                  {13.913, 0.7721},
               };
               for (int i = 0; i < n_points; ++i) {
                  logMh_base.push_back(prada12_fig12_arr_z2[i][0]);
                  logc_base.push_back(prada12_fig12_arr_z2[i][1]);
               }
               label_literature = (label_base + ", z = 2, " + Delta_lit);
            } else if (z < 5 and z >= 3.5) {
               n_points = 10;
               double prada12_fig12_arr_z4[10][2] = {
                  {10.80, 0.6105},
                  {11.27, 0.6214},
                  {11.62, 0.6370},
                  {11.94, 0.6580},
                  {12.17, 0.6783},
                  {12.36, 0.7   },
                  {12.52, 0.7236},
                  {12.66, 0.7449},
                  {12.77, 0.7710},
                  {12.90, 0.7935},
               };
               for (int i = 0; i < n_points; ++i) {
                  logMh_base.push_back(prada12_fig12_arr_z4[i][0]);
                  logc_base.push_back(prada12_fig12_arr_z4[i][1]);
               }
               label_literature = (label_base + ", z = 4, " + Delta_lit);
            } else if (z >= 5) {
               n_points = 5;
               double prada12_fig12_arr_z6[5][2] = {
                  {10.80, 0.6547},
                  {11.02, 0.6728},
                  {11.19, 0.6877},
                  {11.37, 0.7062},
                  {11.59, 0.7351},
               };
               for (int i = 0; i < n_points; ++i) {
                  logMh_base.push_back(prada12_fig12_arr_z6[i][0]);
                  logc_base.push_back(prada12_fig12_arr_z6[i][1]);
               }
               label_literature = (label_base + ", z = 6, " + Delta_lit);
            }

            char tmp[500];
            sprintf(tmp, "gr_cvir_comp%s", gNAMES_CDELTAMDELTA[j_cvir]);
            int n_used = 0;
            vector<double> m_used;
            vector<double> c_used;
            for (int i = 1; i < n_m; ++i) {
               double logmh_var = log10(m[i] * gCOSMO_HUBBLE);
               double logcinterpol = interp1D(logmh_var, logMh_base, logc_base, kLINLIN, true);
               if (logcinterpol > HPX_BLIND_VALUE) {
                  m_used.push_back(m[i]);
                  c_used.push_back(pow(10, logcinterpol));
                  ++n_used;
               }
            }
            gr_c_comp = new TGraph(n_used, &m_used[0], &c_used[0]);
            gr_c_comp->SetName(tmp);
            gr_c_comp->SetLineColor(rootcolor(j_cvir + 10));
            gr_c_comp->SetLineWidth(5);
            gr_c_comp->SetLineStyle(j_cvir + 1);
            multi_cvir->Add(gr_c_comp, "L");

            TLegendEntry *entry = leg_cvir->AddEntry(gr_c_comp, label_literature.c_str(), "L");
            entry->SetTextColor(rootcolor(j_cvir));
         }
      }


      TCanvas *c_cvir = new TCanvas("c_cvir", "c_cvir", 0, 0, 650, 500);
      c_cvir->SetLogx(1);
      c_cvir->SetLogy(1);
      multi_cvir->Draw("A");
      gSIM_CLUMPYAD->Draw();
      string label_tmp = "M_{" + Delta_str + "}  [M_{#odot}]";
      multi_cvir->GetXaxis()->SetTitle(label_tmp.c_str());
      label_tmp = "Concentration  c_{" + Delta_str + "}";
      multi_cvir->GetYaxis()->SetTitle(label_tmp.c_str());
      multi_cvir->SetMinimum(multi_cvir->GetYaxis()->GetXmin());
      multi_cvir->SetMaximum(multi_cvir->GetYaxis()->GetXmax());
      leg_cvir->Draw("SAME");
      c_cvir->Update();
      c_cvir->Modified();
      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->cd();
         gPad->Write();
      }

      //--- c_lum for halos
      TCanvas *c_lum = new TCanvas("c_lum", "c_lum", 660, 0, 650, 500);
      c_lum->SetLogx(1);
      c_lum->SetLogy(1);
      multi_lum->Draw("A");
      gSIM_CLUMPYAD->Draw();
      label_tmp = "M_{" + Delta_str + "}  [M_{#odot}]";
      multi_lum->GetXaxis()->SetTitle(label_tmp.c_str());
      if (gPP_DM_IS_ANNIHIL_OR_DECAY)
         if (z > SMALL_NUMBER) multi_lum->GetYaxis()->SetTitle("Proper luminosity / (M #times #rho_{m,0})  [-]");
         else multi_lum->GetYaxis()->SetTitle("Luminosity / (M #times #rho_{m,0})  [-]");
      else
         multi_lum->GetYaxis()->SetTitle("Luminosity / M [-]");
      multi_lum->SetMinimum(multi_lum->GetYaxis()->GetXmin());
      multi_lum->SetMaximum(multi_lum->GetYaxis()->GetXmax());
      leg_lumprof->Draw("SAME");
      leg_lumcvir->Draw("SAME");
      c_lum->Update();
      c_lum->Modified();
      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->cd();
         gPad->Write();
      }

      //--- c_lumdpdlogm for halos
      TCanvas *c_lumdpdlogm = new TCanvas("c_lumdpdlogm", "c_lumdpdlogm", 0, 500, 650, 500);
      c_lumdpdlogm->SetLogx(1);
      c_lumdpdlogm->SetLogy(0);
      multi_lumdpdlogm->Draw("A");
      gSIM_CLUMPYAD->Draw();
      multi_lumdpdlogm->GetXaxis()->SetTitle(label_tmp.c_str());
      if (gPP_DM_IS_ANNIHIL_OR_DECAY)
         multi_lumdpdlogm->GetYaxis()->SetTitle("L #times dP/dLogM [M_{#odot}^{2} kpc^{-3}]");
      else
         multi_lumdpdlogm->GetYaxis()->SetTitle("L #times dP/dLogM [M_{#odot}]");
      multi_lumdpdlogm->SetMinimum(multi_lumdpdlogm->GetYaxis()->GetXmin());
      multi_lumdpdlogm->SetMaximum(multi_lumdpdlogm->GetYaxis()->GetXmax());
      leg_lumprof->Draw("SAME");
      leg_lumcvir_red->Draw("SAME");
      c_lumdpdlogm->Update();
      c_lumdpdlogm->Modified();
      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->cd();
         gPad->Write();
      }

      //--- c_boost for halos
      TCanvas *c_boost = new TCanvas("c_boost", "c_boost", 1200, 0, 650, 500);
      c_boost->SetLogx(1);
      c_boost->SetLogy(0);
      multi_boost->Draw("A");
      gSIM_CLUMPYAD->Draw();
      multi_boost->GetXaxis()->SetTitle(label_tmp.c_str());
      multi_boost->GetYaxis()->SetTitle("Boost  [-]");
      multi_boost->SetMinimum(multi_boost->GetYaxis()->GetXmin());
      multi_boost->SetMaximum(multi_boost->GetYaxis()->GetXmax());
      leg_lumprof->Draw("SAME");
      leg_lumcvir_red->Draw("SAME");
      c_boost->Update();
      c_boost->Modified();
      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->cd();
         gPad->Write();
      }

      //--- c_boost_rel for halos
      TCanvas *c_boostrel = new TCanvas("c_boostrel", "c_boostrel", 1200, 500, 650, 500);
      c_boostrel->SetLogx(1);
      c_boostrel->SetLogy(0);
      multi_boostrel->Draw("A");
      gSIM_CLUMPYAD->Draw();
      multi_boostrel->GetXaxis()->SetTitle(label_tmp.c_str());
      multi_boostrel->GetYaxis()->SetTitle("Relative difference [%]");
      multi_boostrel->SetMinimum(multi_boostrel->GetYaxis()->GetXmin());
      multi_boostrel->SetMaximum(multi_boostrel->GetYaxis()->GetXmax());
      leg_lumprof->Draw("SAME");
      leg_lumcvir_red->Draw("SAME");
      leg_boostrel->Draw("SAME");
      c_boostrel->Update();
      c_boostrel->Modified();
      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->cd();
         gPad->Write();
      }

      //--- c_dpdclum
      TMultiGraph *multi_dpdclum = NULL;
      TLegend *leg_dpdclum = NULL;
      TCanvas *c_dpdclum = NULL;
      TGraph *gr_dpdclum[n_cmean];
      TGraph *gr_dpdc[n_cmean];
      TGraph *gr_lumc[n_cmean];
      TText *txt[n_cmean];
      if (sigma_cvir > 1.e-5 && j_cvir_mean >= 0) {
         multi_dpdclum = new TMultiGraph();
         leg_dpdclum = new TLegend(0.2, 0.65, 0.7, 0.95);
         char tmp_header[500];
         sprintf(tmp_header, "Y (#alpha_{M}=%.2f, %s)", v_alpham[0], gNAMES_CDELTAMDELTA[j_cvir_mean]);
         leg_dpdclum->SetHeader(tmp_header);
         c_dpdclum = new TCanvas("c_dpdclum", "c_dpdclum", 600, 500, 650, 500);
         c_dpdclum->SetLogx(1);
         c_dpdclum->SetLogy(1);
         // Loop on <c> values
         for (int cc = 0; cc < n_cmean; ++cc) {
            char tmp_cc[500];
            sprintf(tmp_cc, "dpdc_lum_%d", cc);
            gr_dpdclum[cc] = new TGraph(cvir_val[cc].size(), &cvir_val[cc][0], &dpdclum_val[cc][0]);
            gr_dpdclum[cc]->SetLineColor(kRed);
            gr_dpdclum[cc]->SetLineWidth(3);
            gr_dpdclum[cc]->SetLineStyle(1);
            gr_dpdclum[cc]->SetName(tmp_cc);
            multi_dpdclum->Add(gr_dpdclum[cc], "L");
            if (cc == 0) {
               TLegendEntry *entry = leg_dpdclum->AddEntry(gr_dpdclum[cc], "dP/dc #times L (integrand)", "L");
               entry->SetTextColor(kRed);
            }
            //
            sprintf(tmp_cc, "dpdc_%d", cc);
            gr_dpdc[cc] = new TGraph(cvir_val[cc].size(), &cvir_val[cc][0], &dpdc_val[cc][0]);
            gr_dpdc[cc]->SetLineColor(kBlue);
            gr_dpdc[cc]->SetLineWidth(2);
            gr_dpdc[cc]->SetLineStyle(3);
            gr_dpdc[cc]->SetName(tmp_cc);
            multi_dpdclum->Add(gr_dpdc[cc], "L");
            if (cc == 0) {
               TLegendEntry *entry = leg_dpdclum->AddEntry(gr_dpdc[cc], "dP/dc", "L");
               entry->SetTextColor(kBlue);
            }
            //
            sprintf(tmp_cc, "lum_%d", cc);
            gr_lumc[cc] = new TGraph(cvir_val[cc].size(), &cvir_val[cc][0], &lum_val[cc][0]);
            gr_lumc[cc]->SetLineColor(kMagenta);
            gr_lumc[cc]->SetLineWidth(2);
            gr_lumc[cc]->SetLineStyle(5);
            gr_lumc[cc]->SetName(tmp_cc);
            multi_dpdclum->Add(gr_lumc[cc], "L");
            if (cc == 0) {
               TLegendEntry *entry = leg_dpdclum->AddEntry(gr_lumc[cc], "L", "L");
               entry->SetTextColor(kMagenta);
            }
            char txt_cmean[500];
            sprintf(txt_cmean, "<c>=%.1f", cvir_mean[cc]);
            txt[cc] = new TText(0.7 * cvir_mean[cc], 0.01, txt_cmean);
            txt[cc]->SetTextFont(132);
            txt[cc]->SetTextSize(0.04);
         }
         //
         multi_dpdclum->Draw("A");
         gSIM_CLUMPYAD->Draw();
         label_tmp = "c_{" + Delta_str + "}  [-]";
         multi_dpdclum->GetXaxis()->SetTitle(label_tmp.c_str());
         multi_dpdclum->GetYaxis()->SetTitle("Y(c) / Y(<c>)");
         multi_dpdclum->SetMinimum(1.e-3);
         multi_dpdclum->SetMaximum(1.e3);
         leg_dpdclum->Draw("SAME");
         for (int cc = 0; cc < n_cmean; ++cc)
            txt[cc]->Draw();
         c_dpdclum->Update();
         c_dpdclum->Modified();

         if (gSIM_IS_WRITE_ROOTFILES) {
            root_file->cd();
            gPad->Write();
         }
      }

      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->Close();
         delete root_file;
         cout << " ... output [use ROOT TBrowser] written in: " << f_root << endl;
      }
      if (gSIM_IS_PRINT)
         cout << "_______________________" << endl << endl;

      if (gSIM_IS_DISPLAY)
         gSIM_ROOTAPP->Run(kTRUE);

      delete leg_cvir;
      delete multi_cvir;
      delete c_cvir;

      delete leg_lumprof;
      delete leg_lumcvir;
      delete multi_lum;
      delete c_lum;

      delete leg_lumcvir_red;
      delete multi_boost;
      delete c_boost;

      delete leg_boostrel;
      delete multi_boostrel;
      delete c_boostrel;

      delete multi_lumdpdlogm;
      delete c_lumdpdlogm;

      if (multi_dpdclum)
         delete multi_dpdclum;
      if (leg_dpdclum)
         delete leg_dpdclum;
      if (c_dpdclum)
         delete c_dpdclum;
      if (gr_lum) delete[] gr_lum;
      if (gr_lumdpdm) delete[] gr_lumdpdm;
      if (gr_cvir) delete[] gr_cvir;
      if (gr_boost) delete[] gr_boost;
      if (gr_boostrel) delete[] gr_boostrel;
   }
#endif
}

#if IS_ROOT
//______________________________________________________________________________
void pop_study_plots(vector<struct gStructHalo> &list_halos, vector<pair<double, int> > &list_j,
                     vector<pair<double, int> > &list_contrast, vector<pair<double, int> > &list_boost,
                     TCanvas **c_pop, TH1D **h_pop, TGraphAsymmErrors **gr_pop,
                     double const &phi_cut_deg, string const &pop_name, string const &filename_root_str)
{
   //--- Plots for population studies.
   //
   // INPUTS:
   //  list_halos     Vector of gStructHalo loaded from halo_load_list()
   //  list_j         Vector of pairs of <Jhalo(alpha_int),index> values (for each halo)
   //  list_contrast  Vector of pairs of <Jhalo(alpha_int)/JGal(alpha_int),index> values (for each halo)
   //  list_boost     Vector of pairs <Boost=(Jtot_withsubs/Jtot_nosubs),index> values (for each halo)
   //  c_pop          Root canvas
   //  h_pop          Root histos for displays
   //  gr_pop         Root graph for displays
   //  phi_cut_deg    If angle of the object (w.r.t. Gal. centre) < phi_cut_deg, the object is discarded
   //  pop_name       Name added to graphs & canvas names (if function called several times)
   // OUTPUTS:
   //  list_halos     Sorted vector of gStructHalo loaded from halo_load_list()
   //  list_j         Sorted Vector of pairs of <Jhalo(alpha_int),index> values (for each halo)
   //  list_contrast  Sorted Vector of pairs of <Jhalo(alpha_int)/JGal(alpha_int),index> values (for each halo)
   //  list_boost     Sorted Vector of pairs <Boost=(Jtot_withsubs/Jtot_nosubs),index> values (for each halo)
   //  c_pop          Filled Root canvas
   //  h_pop          Filled Root histos for displays
   //  gr_pop         Filled Root graph for displays

   // If list from population too small, does nothing!
   if ((int)list_halos.size() < 20)
      return;

   // Ensure list_j and list_contrast vectors are sorted
   double dummy = 1.;
   pop_study_sort_j(list_halos, list_j, list_contrast, list_boost, dummy, false);

   int n_j = list_halos.size();
   vector<double> alpha_s;
   for (int i = 0; i < n_j; ++i)
      alpha_s.push_back(asin(list_halos[i].Rscale / list_halos[i].l) * RAD_to_DEG);
   sort(alpha_s.begin(), alpha_s.end());

   // Draw canvas, histos, and graphs
   int n_h = 3;
   if (list_boost.size() > 0) n_h += 1;
   int nfig = 5 + n_h + 1; // last one is J-M-d 3D cube
   const int n_gr = 7; // number of graphs: no histos, two graphs in sumjbin_logj and cumulsupj_logj 3 + 2 * 2
   c_pop = new TCanvas*[nfig];
   h_pop = new TH1D*[n_h];
   gr_pop = new TGraphAsymmErrors*[n_gr];
   TLegend **leg_cumul = new TLegend*[n_gr];
   TLegend *leg_boost = NULL;
   char header[500];
   sprintf(header, "#theta_{cut}=%.1f#circ", phi_cut_deg);
   vector<string> c_name;
   // c_name.reserve(nfig);
   c_name.push_back("logn_logalphas");     // fig #1, histo #1
   c_name.push_back("logn_logj");          // fig #2, histo #2
   c_name.push_back("logn_logj_contrast"); // fig #3, histo #3
   c_name.push_back("j_alphas");           // fig #4
   c_name.push_back("j_md2");              // fig #5
   c_name.push_back("j_m");                // fig #6
   c_name.push_back("sumjbin_logj");       // fig #7
   c_name.push_back("cumulsupj_logj");     // fig #8
   c_name.push_back("j_m_d_cube");         // fig #9

   if (list_boost.size() > 0) c_name.push_back("logn_logboost"); // extra histo
   for (int i = 0; i < nfig; ++i)
      c_name[i] = "c_" + pop_name + "_" + c_name[i];

   int x_pos[nfig], y_pos[nfig];
   for (int i = 0; i < nfig; ++i) {
      x_pos[i] = 675 + (i % 3) * 310;
      y_pos[i] = 20 + (i / 3) * 320;
   }


   int i_h = 0, i_gr = 0;
   int ih_to_c[n_h];
   ih_to_c[0] = 0;
   ih_to_c[1] = 1;
   ih_to_c[2] = 2;
   if (list_boost.size() > 0) ih_to_c[3] = 8;

   int igr_to_c[n_gr] = {3, 4, 5, 6, 6, 7, 7};
   for (int c = 0; c < nfig; ++c) {
      c_pop[c] = new TCanvas(c_name[c].c_str(), c_name[c].c_str(), x_pos[c], y_pos[c], 300, 300);
      c_pop[c]->SetHighLightColor(kWhite);
      c_pop[c]->SetFillColor(kWhite);
      c_pop[c]->SetBorderMode(0);
      c_pop[c]->SetBorderSize(0);
      c_pop[c]->SetLeftMargin(0.15);
      c_pop[c]->SetRightMargin(0.05);
      c_pop[c]->SetTopMargin(0.05);
      c_pop[c]->SetBottomMargin(0.17);
      c_pop[c]->SetFrameBorderMode(0);
      c_pop[c]->SetFrameFillColor(kWhite);
      c_pop[c]->SetTickx(1);
      c_pop[c]->SetTicky(1);
      c_pop[c]->Range(-0.5, -4.5, 2.0, -0.15);
      c_pop[c]->SetLogx(0);
      c_pop[c]->SetLogy(0);
      c_pop[c]->SetGridx(0);
      c_pop[c]->SetGridy(0);
   }

   // DAVID
   // Higher-level option (to plot normalised histograms)
   bool is_normtomax = false;

   //-------------------------------
   //--- Set and plot histograms ---
   //-------------------------------
   string h_name[3] = {"lognlogalphas", "lognlogj", "lognlogj_contrast"};
   for (int j = i_h; j < n_h; ++j) {
      int c = ih_to_c[j];
      c_pop[c]->cd();
      if (j == 3) {
         vector<double> boosts;
         for (int k = 0; k < n_j; ++k)
            boosts.push_back(list_boost[k].first);
         sort(boosts.begin(), boosts.end());
         //h_pop[j] = new TH1D("pop_boost", "pop_boost", max(3,min(n_j/10,50)), boosts[0], boosts[n_j-1]);
         // DAVID
         // Useful to have same number of bins and range for comparison of several models.
         h_pop[j] = new TH1D("pop_boost", "pop_boost", max(3, min(n_j / 10, 50)), log10(1), log10(200));
         h_pop[j]->SetName("pop_boost");
         h_pop[j]->SetTitle("");
         for (int k = 0; k < n_j; ++k)
            h_pop[j]->Fill(log10(boosts[k]));
         TH1D **histo_boost = new TH1D*[1];
         histo_boost[0] = h_pop[j];
         vector<double> alphaint_or_theta;
         alphaint_or_theta.push_back(gSIM_ALPHAINT * RAD_to_DEG);
         bool is_alphaint_or_theta = 1;

         c_pop[c]->SetTickx(1);
         c_pop[c]->SetTicky(1);
         c_pop[c]->Range(-0.5, -4.5, 2.0, -0.15);
         c_pop[c]->SetLogx(0);
         c_pop[c]->SetLogy(1);
         c_pop[c]->SetGridx(0);
         c_pop[c]->SetGridy(0);

         leg_boost = new TLegend(0.6, 0.6, 0.95, 0.95);
         leg_boost->SetFillColor(kWhite);
         leg_boost->SetTextSize(0.05);
         leg_boost->SetTextFont(132);
         leg_boost->SetBorderSize(0);
         leg_boost->SetFillStyle(0);

         for (int jj = 0; jj < (int)alphaint_or_theta.size(); ++jj) {
            histo_boost[jj]->SetStats(kFALSE);
            histo_boost[jj]->SetLineColor(rootcolor(jj));
            histo_boost[jj]->SetLineStyle(jj + 1);
            histo_boost[jj]->SetLineWidth(2);
            if (is_alphaint_or_theta)
               histo_boost[jj]->GetXaxis()->SetTitle("Boost(#alpha_{int})#equiv[J_{sm}+<J_{subs}>+J_{crossprod}]/J_{tot}");
            else
               histo_boost[jj]->GetXaxis()->SetTitle("Boost(#theta)#equiv[J_{sm}+<J_{subs}>+J_{crossprod}]/J_{tot}");
            histo_boost[jj]->GetYaxis()->SetTitle("N");
            char tmp_alpha[200];
            if (is_alphaint_or_theta)
               sprintf(tmp_alpha, "#alpha_{int} = %.3f #circ", alphaint_or_theta[jj]);
            else
               sprintf(tmp_alpha, "#theta = %.3f #circ", alphaint_or_theta[jj]);
            leg_boost->AddEntry(histo_boost[jj], tmp_alpha, "L");
            if (jj == 0) histo_boost[jj]->Draw();
            else histo_boost[jj]->Draw("SAME");
            gSIM_CLUMPYAD->Draw();
         }

         leg_boost->Draw("SAME");
         histo_boost[0] = NULL;
         delete[] histo_boost;
      } else {
         h_name[j] = "h_" + pop_name + "_" + h_name[j];
         c_pop[c]->SetLogx(0);
         c_pop[c]->SetLogy(1);
         if (j == 0) {
            h_pop[j] = new TH1D(h_name[j].c_str(), h_name[j].c_str(), max(3, min(n_j / 10, 50)),
                                log10(alpha_s[0]), log10(alpha_s[n_j - 1]) * 1.1);
            //h_pop[j] = new TH1D(h_name[j].c_str(), h_name[j].c_str(), 50, -2.5, 1.0);
            for (int k = 0; k < n_j; ++k)
               h_pop[j]->Fill(log10(alpha_s[k]));
            h_pop[j]->GetXaxis()->SetTitle("log_{10}(#alpha_{s} / 1#circ)");
         } else if (j == 1) {
            if (!is_normtomax) {
               h_pop[j] = new TH1D(h_name[j].c_str(), h_name[j].c_str(), max(3, min(n_j / 10, 50)),
                                   log10(list_j[0].first), log10(list_j[n_j - 1].first) * 1.1);
               // DAVID
               // Useful to have same number of bins and range for comparison of several models.
               //h_pop[j] = new TH1D(h_name[j].c_str(), h_name[j].c_str(), 50, 7.5, 12.5);
               for (int k = 0; k < n_j; ++k)
                  h_pop[j]->Fill(log10(list_j[k].first));
               if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
                  if (gSIM_IS_ASTRO_OR_PP_UNITS)
                     h_pop[j]->GetXaxis()->SetTitle("log_{10}(J / M_{#odot}^{2} kpc^{-5})");
                  else
                     h_pop[j]->GetXaxis()->SetTitle("log_{10}(J / GeV^{2} cm^{-5})");
               } else {
                  if (gSIM_IS_ASTRO_OR_PP_UNITS)
                     h_pop[j]->GetXaxis()->SetTitle("log_{10}(D / M_{#odot} kpc^{-2})");
                  else
                     h_pop[j]->GetXaxis()->SetTitle("log_{10}(D / GeV cm^{-2})");
               }
            } else {
               //h_pop[j] = new TH1D(h_name[j].c_str(), h_name[j].c_str(), max(3,min(n_j/10,50)), -3., 0.1);
               h_pop[j] = new TH1D(h_name[j].c_str(), h_name[j].c_str(), 50, -3., 0.1);
               for (int k = 0; k < n_j; ++k)
                  h_pop[j]->Fill(log10(list_j[k].first / list_j[n_j - 1].first));
               if (gPP_DM_IS_ANNIHIL_OR_DECAY)
                  h_pop[j]->GetXaxis()->SetTitle("log_{10}(J / J_{max})");
               else
                  h_pop[j]->GetXaxis()->SetTitle("log_{10}(D / D_{max})");
            }
         } else if (j == 2) {
            h_pop[j] = new TH1D(h_name[j].c_str(), h_name[j].c_str(), max(3, min(n_j / 10, 50)),
                                log10(list_contrast[0].first), log10(list_contrast[n_j - 1].first) * 1.1);
            for (int k = 0; k < n_j; ++k)
               h_pop[j]->Fill(log10(list_contrast[k].first));
            if (gPP_DM_IS_ANNIHIL_OR_DECAY)
               h_pop[j]->GetXaxis()->SetTitle("log_{10}(J / J_{Gal.bkd})");
            else
               h_pop[j]->GetXaxis()->SetTitle("log_{10}(D / D_{Gal.bkd})");
         }
         h_pop[j]->SetStats(0);
         h_pop[j]->SetLineColor(kBlack);
         h_pop[j]->SetLineStyle(1);
         h_pop[j]->SetLineWidth(2);
         h_pop[j]->SetTitle("");
         h_pop[j]->GetXaxis()->SetNdivisions(504);
         h_pop[j]->GetXaxis()->SetLabelOffset(0.01);
         h_pop[j]->GetXaxis()->SetLabelSize(0.065);
         h_pop[j]->GetXaxis()->SetLabelFont(132);
         h_pop[j]->GetXaxis()->SetTitleSize(0.075);
         h_pop[j]->GetXaxis()->SetTickLength(0.08);
         h_pop[j]->GetXaxis()->SetTitleOffset(1.);
         h_pop[j]->GetXaxis()->SetTitleFont(132);
         h_pop[j]->GetYaxis()->SetNdivisions(504);
         h_pop[j]->GetYaxis()->SetLabelOffset(0.);
         h_pop[j]->GetYaxis()->SetLabelSize(0.065);
         h_pop[j]->GetYaxis()->SetLabelFont(132);
         h_pop[j]->GetYaxis()->SetTitleSize(0.075);
         h_pop[j]->GetYaxis()->SetTickLength(0.08);
         h_pop[j]->GetYaxis()->SetTitleOffset(0.9);
         h_pop[j]->GetYaxis()->SetTitleFont(132);
         h_pop[j]->GetYaxis()->SetTitle("N");
         h_pop[j]->SetMinimum(.5);
         h_pop[j]->Draw();
         gSIM_CLUMPYAD->Draw();
      }
   }

   //---------------------------
   //--- Set and plot graphs ---
   //---------------------------
   //--- Create vector of Jpop bin values and fill Jbins (for third and fourth graph)

   // Choose j bin sizes (jbins)
   vector<double> jbins;
   double jbin_min = list_j[0].first;
   double jbin_max = list_j[n_j - 1].first;
   int nbins = min(20, n_j / 5);
   double step = pow(jbin_max / jbin_min, 1. / (double)nbins);
   jbins.push_back(jbin_min);
   for (int k = 1; k < nbins; ++k)
      jbins.push_back(jbin_min * pow(step, k));
   jbins.push_back(jbin_max);
   vector<int> n_halos_bin(nbins, 0);

   // Reserve for cumul (for pop. objects, but also DM Gal. Bkgd)
   // N.B.: cumul_xxx is for the cumulated of n_k objects with j-value in [jbins[k],jbins[k+1]]
   //       cumulsupj_xxx is for the cumulated of m_k objects with j-value > jbins[k]
   vector<double> cumul_jbins(nbins, 0.);
   vector<double> cumul_jgalbins(nbins, 0.);
   vector<double> cumulsupj_jbins(nbins, 0.);
   vector<double> cumulsupj_jgalbins(nbins, 0.);
   vector<int> n_cumulhalos(nbins, 0);
   int i_start = 0;
   // Loop on jbins
   for (int k = 0; k < nbins; ++k) {
      int n_j_in_bin = 0;
      // For each bin k, search for haloes the j of which falls in [jbins[k],jbins[k+1]]
      for (int i_sorted = i_start; i_sorted < n_j; ++i_sorted) {
         // N.B.: we need to find correspondance between list j sorted by j/jgal and sorted by j
         // Find index in contrast_j
         int l = 0;
         for (int m = 0; m < (int)list_contrast.size(); ++m) {
            l = m;
            if (list_contrast[m].second == list_j[i_sorted].second)
               break;
         }

         // If does not pass phi_cut (angle between GC and halo), discards
         int  i_list = list_j[i_sorted].second;
         double psi_halo = list_halos[i_list].PsiDeg * DEG_to_RAD;
         check_psi(psi_halo);
         double theta_halo = list_halos[i_list].ThetaDeg * DEG_to_RAD;
         double phi_deg = psitheta_to_phi(psi_halo, theta_halo) * RAD_to_DEG;
         if (phi_deg < phi_cut_deg)
            continue;

         if (list_j[i_sorted].first <= jbins[k + 1]) {
            ++n_j_in_bin;
            cumul_jbins[k] +=  list_j[i_sorted].first;
            cumul_jgalbins[k] +=  list_j[i_sorted].first / list_contrast[l].first;
            if (i_sorted == n_j - 1 && n_j_in_bin > 0)
               n_halos_bin[k] = n_j_in_bin;
         } else {
            i_start = i_sorted;
            if (cumul_jbins[k] > 0. && n_j_in_bin > 0) {
               n_halos_bin[k] = n_j_in_bin;
            }
            break;
         }
      }
   }

   // Calculate cumusupj_xxx from cumul_xxx
   for (int k = 0; k < nbins; ++k) {
      cumulsupj_jbins[k] = cumul_jbins[k];
      cumulsupj_jgalbins[k] = cumul_jgalbins[k];
      n_cumulhalos[k] = n_halos_bin[k];
   }
   for (int k = nbins - 2; k >= 0; --k) {
      cumulsupj_jbins[k] += cumulsupj_jbins[k + 1];
      cumulsupj_jgalbins[k] += cumulsupj_jgalbins[k + 1];
      n_cumulhalos[k] += n_cumulhalos[k + 1];
   }

   // Graph caracteristics
   char x_title[n_gr][2][100] = {
      {"#alpha_{s}=asin(r_{s}/d)  [deg]", "#alpha_{s}=asin(r_{s}/d)  [deg]"},
      {"M/d^{2}  [M_{#odot} kpc^{-2}]", "M/d^{2}  [M_{#odot} kpc^{-2}]"},
      {"M [M_{#odot}]", "M [M_{#odot}]"},
      {"D bins ", "J bins "},
      {"D bins ", "J bins "},
      {"D bins ", "J bins "},
      {"D bins ", "J bins "}
   };
   for (int ii = 3; ii < n_gr; ++ii) {
      string tmp0 = x_title[ii][0];
      string tmp1 = x_title[ii][1];
      if (gSIM_IS_ASTRO_OR_PP_UNITS) {
         sprintf(x_title[ii][0], "%s [M_{#odot} kpc^{-2}]", tmp0.c_str());
         sprintf(x_title[ii][1], "%s [M_{#odot}^{2} kpc^{-5}]", tmp1.c_str());
      } else {
         sprintf(x_title[ii][0], "%s [GeV cm^{-2}]", tmp0.c_str());
         sprintf(x_title[ii][1], "%s [GeV^{2} cm^{-5}]", tmp1.c_str());
      }
   }

   char y_title[n_gr][2][100] = {
      {"D ", "J "},
      {"D ", "J "},
      {"D ", "J "},
      {"Sum Dk (k#in D bin)  ", "Sum J_{k} (k#in J_{bin})  "},
      {"Sum Dk (k#in D bin)  ", "Sum J_{k} (k#in J_{bin})  "},
      {"Cumulative (> D)   ", "Cumulative  (>J)  "},
      {"Cumulative (> D)   ", "Cumulative  (>J)  "}
   };
   for (int ii = 0; ii < n_gr; ++ii) {
      string tmp0 = y_title[ii][0];
      string tmp1 = y_title[ii][1];
      if (gSIM_IS_ASTRO_OR_PP_UNITS) {
         sprintf(y_title[ii][0], "%s [M_{#odot} kpc^{-2}]", tmp0.c_str());
         sprintf(y_title[ii][1], "%s [M_{#odot}^{2} kpc^{-5}]", tmp1.c_str());
      } else {
         sprintf(y_title[ii][0], "%s [GeV cm^{-2}]", tmp0.c_str());
         sprintf(y_title[ii][1], "%s [GeV^{2} cm^{-5}]", tmp1.c_str());
      }
   }


   int marker_color[n_gr] = {kRed, kRed, kRed, kBlue, kRed, kBlue, kRed};
   int marker_style[n_gr] = {24, 24, 24, 7, 2, 7, 2};
   double marker_size[n_gr] = {0.6, 0.6, 0.6, 0.9, 0.9, 0.9, 0.9};
   int line_color[n_gr] = {kRed, kRed, kRed, kBlue, kRed, kBlue, kRed};
   int line_style[n_gr] = {1, 1, 1, 1, 1, 1, 1};
   int line_width[n_gr] = {1, 1, 1, 1, 1, 1, 1};


   string gr_name[n_gr] = {"jfactor_vs_alphas", "jfactor_vs_mtod2", "jfactor_vs_m", "sumj_vs_jbin", "cumulj_vs_j"};
   for (int j = i_gr; j < n_gr; ++j)
      gr_name[j] = "gr_" + pop_name + "_" + gr_name[j];

   for (int j = i_gr; j < n_gr; ++j) {
      if (j == 4 || j == 6) continue;
      int c = igr_to_c[j];
      c_pop[c]->cd();
      c_pop[c]->SetLogx(1);
      c_pop[c]->SetLogy(1);
      gr_pop[j] = new TGraphAsymmErrors();
      gr_pop[j]->SetName(gr_name[j].c_str());
      gr_pop[j]->SetTitle(gr_name[j].c_str());
      leg_cumul[j] = new TLegend(0.4, 0.2, 0.6, 0.4);
      if (j == 3 || j == 5) {
         gr_pop[j + 1] = new TGraphAsymmErrors();
         gr_pop[j + 1]->SetName(gr_name[j + 1].c_str());
         gr_pop[j + 1]->SetTitle(gr_name[j + 1].c_str());
      }

      // Fill graphs
      if (j == 0) {
         // J over alpha_s plot
         for (int k = 0; k < n_j; ++k) {
            int l = list_j[k].second;
            gr_pop[j]->SetPoint(k, asin(list_halos[l].Rscale / list_halos[l].l) * RAD_to_DEG, list_j[k].first);
         }
      } else if (j == 1) {
         // J over M/d^2 plot
         for (int k = 0; k < n_j; ++k) {
            int l = list_j[k].second;
            gr_pop[j]->SetPoint(k, list_halos[l].Mtot / (list_halos[l].l * list_halos[l].l), list_j[k].first);
         }
      } else if (j == 2) {
         // J over M plot
         for (int k = 0; k < n_j; ++k) {
            int l = list_j[k].second;
            gr_pop[j]->SetPoint(k, list_halos[l].Mtot, list_j[k].first);
         }
      } else if (j == 3) {
         // sum J over J bins plot
         int kk = 0;
         for (int k = 0; k < nbins; ++k) {
            double x_current = sqrt(jbins[k] * jbins[k + 1]);
            // Skip empty bins
            if (cumul_jbins[k] > 0. && n_halos_bin[k] > 0) {
               gr_pop[j]->SetPoint(kk, x_current, cumul_jbins[k]);
               gr_pop[j]->SetPointError(kk, x_current - jbins[k], jbins[k + 1] - x_current, 0., 0.);
               gr_pop[j + 1]->SetPoint(kk, x_current, cumul_jgalbins[k]);
               //gr_pop[j+1]->SetPointError(kk, x_current-jbins[k], jbins[k+1]-x_current, 0., 0.);
               ++kk;
            }
         }
      } else if (j == 5) {
         // cumul J over J bins plot
         for (int k = 0; k < nbins; ++k) {
            double x_current = jbins[k];
            gr_pop[j]->SetPoint(k, x_current, cumulsupj_jbins[k]);
            //gr_pop[j]->SetPointError(k, 0, jbins[k+1]-x_current, 0., 0.);
            gr_pop[j + 1]->SetPoint(k, x_current, cumulsupj_jgalbins[k]);
            //gr_pop[j+1]->SetPointError(k, x_current-jbins[k], jbins[k+1]-x_current, 0., 0.);
         }
      }

      gr_pop[j]->SetTitle("");
      gr_pop[j]->SetMarkerColor(marker_color[j]);
      gr_pop[j]->SetMarkerStyle(marker_style[j]);
      gr_pop[j]->SetMarkerSize(marker_size[j]);
      gr_pop[j]->SetLineColor(line_color[j]);
      gr_pop[j]->SetLineStyle(line_style[j]);
      gr_pop[j]->SetLineWidth(line_width[j]);
      if (j == 3 || j == 5) {
         gr_pop[j + 1]->SetTitle("");
         gr_pop[j + 1]->SetMarkerColor(marker_color[j + 1]);
         gr_pop[j + 1]->SetMarkerStyle(marker_style[j + 1]);
         gr_pop[j + 1]->SetMarkerSize(marker_size[j + 1]);
         gr_pop[j + 1]->SetLineColor(line_color[j + 1]);
         gr_pop[j + 1]->SetLineStyle(line_style[j + 1]);
         gr_pop[j + 1]->SetLineWidth(line_width[j + 1]);
      }

      gr_pop[j]->GetXaxis()->SetTitle(x_title[j][gPP_DM_IS_ANNIHIL_OR_DECAY]);
      gr_pop[j]->GetXaxis()->SetNdivisions(504);
      gr_pop[j]->GetXaxis()->SetLabelOffset(0.0);
      gr_pop[j]->GetXaxis()->SetLabelSize(0.065);
      gr_pop[j]->GetXaxis()->SetLabelFont(132);
      gr_pop[j]->GetXaxis()->SetTitleSize(0.075);
      gr_pop[j]->GetXaxis()->SetTickLength(0.08);
      gr_pop[j]->GetXaxis()->SetTitleOffset(0.95);
      gr_pop[j]->GetXaxis()->SetTitleFont(132);

      gr_pop[j]->GetYaxis()->SetTitle(y_title[j][gPP_DM_IS_ANNIHIL_OR_DECAY]);
      gr_pop[j]->GetYaxis()->SetNdivisions(504);
      gr_pop[j]->GetYaxis()->SetLabelOffset(0.);
      gr_pop[j]->GetYaxis()->SetLabelSize(0.065);
      gr_pop[j]->GetYaxis()->SetLabelFont(132);
      gr_pop[j]->GetYaxis()->SetTitleSize(0.07);
      gr_pop[j]->GetYaxis()->SetTickLength(0.08);
      gr_pop[j]->GetYaxis()->SetTitleOffset(0.9);
      gr_pop[j]->GetYaxis()->SetTitleFont(132);

      gr_pop[j]->Draw("APE0");
      gSIM_CLUMPYAD->Draw();

      if (j == 3 || j == 5) {
         gr_pop[j + 1]->Draw("LPSAME");
         char tmp_pop[200];
         sprintf(tmp_pop, "Pop. - #Delta#Omega=%.2le sr", gSIM_HEALPIX_DELTAOMEGA);
         char tmp_gal[200];
         sprintf(tmp_gal, "Gal. - #Delta#Omega=%.2le sr", gSIM_HEALPIX_DELTAOMEGA);
         leg_cumul[j]->SetFillColor(kWhite);
         leg_cumul[j]->SetTextFont(132);
         leg_cumul[j]->SetBorderSize(0);
         leg_cumul[j]->SetFillStyle(0);
         leg_cumul[j]->SetHeader(header);
         leg_cumul[j]->AddEntry(gr_pop[j], tmp_pop, "BP");
         leg_cumul[j]->AddEntry(gr_pop[j + 1], tmp_gal, "LP");
         leg_cumul[j]->Draw();
         int kk = 0;
         for (int k = 0; k < nbins; ++k) {
            char tmp[50];
            TText *text = NULL;
            TArrow *ar = NULL;
            if (j == 3) {
               if (n_halos_bin[k] <= 0) continue;
               double x = sqrt(jbins[k] * jbins[k + 1]);
               double y = cumul_jbins[k];
               sprintf(tmp, "%d", n_halos_bin[k]);
               text = new TText(x, y * 1.3, tmp);
               text->SetTextSize(0.041);
               leg_cumul[j]->SetTextSize(0.045);
               ++kk;
            } else if (j == 5) {
               double x = jbins[k];
               double y = cumulsupj_jbins[k];
               ar = new TArrow(x, y, x * jbins[1] / jbins[0], y, 0.015, "|->");
               ar->Draw("SAME");
               ar->SetLineWidth(1);
               ar->SetLineColor(kBlue);
               ar->SetFillColor(kBlue);
               ar->Draw();
               sprintf(tmp, "%d", n_cumulhalos[k]);
               text = new TText(x * sqrt(jbins[1] / jbins[0]), y * 1.1, tmp);
               text->SetTextSize(0.035);
               leg_cumul[j]->SetTextSize(0.04);
            }

            text->Draw();
            text->SetTextFont(132);
            text->SetTextAlign(22);
            text->SetTextColor(kBlack);
         }
      }
   }

   TNtuple *nt = new TNtuple("j_m_d_cube", "j_m_d_cube", "logJ:logM:logd");
   for (int k = 0; k < n_j; ++k) {
      int l = list_j[k].second;
      if (list_j[k].first > 0. && list_halos[l].Mtot > 0 && list_halos[l].l > 0.)
         nt->Fill(log10(list_j[k].first), log10(list_halos[l].Mtot), log10(list_halos[l].l));
   }
   c_pop[8]->cd();

   c_pop[8]->SetTickx(1);
   c_pop[8]->SetTicky(1);
   c_pop[8]->SetGridx(1);
   c_pop[8]->SetGridy(1);

   nt->SetMarkerColor(marker_color[1]);
   nt->SetMarkerStyle(marker_style[1]);
   nt->SetMarkerSize(marker_size[1]);
   nt->SetLineColor(line_color[1]);
   nt->SetLineStyle(line_style[1]);
   nt->SetLineWidth(line_width[1]);


   nt->Draw("logJ:logM:logd");
   gSIM_CLUMPYAD->Draw();

   // Save .root format?
   TFile *root_file_pop = NULL;
   if (filename_root_str != "NULL") {
      root_file_pop = new TFile(filename_root_str.c_str(), "recreate");
      root_file_pop->cd();
      for (int j = i_gr; j < nfig; ++j) c_pop[j]->Write();
      root_file_pop->Close();
      cout << " ... Population study plots [use ROOT TBrowser] written in: " << filename_root_str << endl;
   }

   delete[] h_pop;
   delete[] gr_pop;
   delete[] leg_cumul;
   delete[] c_pop;
}
#endif

//______________________________________________________________________________
void pop_study_sort_j(vector<struct gStructHalo> &list_halos, vector<pair<double, int> > &list_j,
                      vector<pair<double, int> > &list_contrast, vector<pair<double, int> > &list_boost,
                      double const &contrast_thresh, bool is_print)
{
   //--- Prints on screen the brightest halos sorted according to their J value
   //    and also to their contrast w.r.t. the Gal. background taken at the same
   //    position (i.e. J_halo/J_Gal.bkd > contrast_thresh).
   //
   // INPUTS:
   //  list_halos     vector<struct gStructHalo> of halos loaded from halo_load_list()
   //  list_j         Vector of pairs <Jhalo(alpha_int),index> values (for each halo)
   //  list_contrast  Vector of pairs <Jhalo(alpha_int)/JGal.bkg(alpha_int),index> values (for each halo)
   //  list_boost     Vector of pairs <Boost=(Jtot_withsubs/Jtot_nosubs),index> values (for each halo)
   //  contrast_thresh Sort and print (if is_print==true) halos for which Jhalo/JGal.bkd>contrast_thresh
   //  is_print       Switch to print [default] or only sort
   // OUTPUTS:
   //  list_j         Sorted according to the j values (faintest first, brightest last)
   //  list_contrast  Sorted according to the contrast values (faintest first, brightest last)


   // Sort according to j (form pairs to keep track of original indices)
   sort(list_j.begin(), list_j.end());

   // Sort according to contrast (form pairs to keep track of original indices)
   sort(list_contrast.begin(), list_contrast.end());

   if (!is_print)
      return;

   // Find number with contrast > 1
   int n_good = 0;
   for (int i = list_contrast.size() - 1; i >= 0 ; --i) {
      if (list_contrast[i].first > contrast_thresh)
         ++n_good;
      else
         break;
   }

   // DAVID
   // Print options:
   //   0: standard print
   //   1: more info than 0
   //   2: print format a la Combet et al. study
   int switch_print = 1;

   // Sorted by Jmax
   string unit = units_or_canvasname_1D(1, 2, 0);

   printf("\n");
   printf("         --------------------------------------\n");
   printf("       > Sorted by j_halo values (%d brightest):\n", n_good);
   printf("         --------------------------------------\n");
   if (switch_print == 0) {
      printf("                  halo              #in list       l      b     phi  mass m_%s   d      j_halo    j_gal   j_halo/j_gal log10(j_halo) log10(j_gal)  alpha_s  alpha_int/alpha_s", legend_for_delta().c_str());
      if (gPP_DM_IS_ANNIHIL_OR_DECAY && list_boost.size() > 0)
         printf("    boost");
      printf("\n                                                 [deg]  [deg]  [deg]    [Msol]     [kpc]     %s        -    [deg]     -\n", unit.c_str());
   } else if (switch_print == 1) {
      printf("                  halo              #in list       l      b     phi  mass m_%s   d      j_halo    j_gal   j_halo/j_gal log10(j_halo) log10(j_gal)", legend_for_delta().c_str());
      if (gPP_DM_IS_ANNIHIL_OR_DECAY && list_boost.size() > 0)
         printf("    boost");
      printf("\n                                                 [deg]  [deg]  [deg]    [Msol]     [kpc]     %s         -    \n", unit.c_str());
   } else if (switch_print == 2) {
      printf("                  halo              #in list       l      b        d      alpha_s   j_halo");
      if (gPP_DM_IS_ANNIHIL_OR_DECAY && list_boost.size() > 0)
         printf("    boost");
      printf("\n                                                 [deg]  [deg]      [kpc]   [deg]    %s\n", unit.c_str());
   }
   for (int i = list_j.size() - 1; i >= (int)list_j.size() - n_good; --i) {
      // Index k in list of haloes
      int k = list_j[i].second;
      // Find corresponding index l in list_contrast
      int l = 0;
      for (int m = 0; m < (int)list_contrast.size(); ++m) {
         l = m;
         if (list_contrast[m].second == k)
            break;
      }
      string tmp = list_halos[k].Name + " (" + list_halos[k].Type + ")";
      double phi = psitheta_to_phi(list_halos[k].PsiDeg * DEG_to_RAD, list_halos[k].ThetaDeg * DEG_to_RAD);
      if (list_boost.size() == 0) {
         if (switch_print == 0)
            printf("      => %-30s  %-6d %+5.1f  %+5.1f  %5.1f   %.2le  %.2le  %.2le  %.2le   %.2le      %.2f        %.2f    %.2le   %.2le\n",
                   tmp.c_str(), k + 1, list_halos[k].PsiDeg, list_halos[k].ThetaDeg, phi * RAD_to_DEG,
                   list_halos[k].Mtot, list_halos[k].l, list_j[i].first, list_j[i].first / list_contrast[l].first,
                   list_contrast[l].first, log10(list_j[i].first), log10(list_j[i].first / list_contrast[l].first),
                   asin(list_halos[k].Rscale / list_halos[k].l) * RAD_to_DEG, gSIM_ALPHAINT / asin(list_halos[k].Rscale / list_halos[k].l));
         else if (switch_print == 1)
            printf("      => %-30s  %-6d %+5.1f  %+5.1f  %5.1f   %.2le  %.2le  %.2le  %.2le   %.2le      %.2f        %.2f\n",
                   tmp.c_str(), k + 1, list_halos[k].PsiDeg, list_halos[k].ThetaDeg, phi * RAD_to_DEG,
                   list_halos[k].Mtot, list_halos[k].l, list_j[i].first, list_j[i].first / list_contrast[l].first,
                   list_contrast[l].first, log10(list_j[i].first), log10(list_j[i].first / list_contrast[l].first));
         else if (switch_print == 2)
            printf("      => %-30s  %-6d %+5.1f  %+5.1f   %.2le  %.2le  %.2le\n",
                   tmp.c_str(), k + 1, list_halos[k].PsiDeg, list_halos[k].ThetaDeg, list_halos[k].l,
                   asin(list_halos[k].Rscale / list_halos[k].l) * RAD_to_DEG, list_j[i].first);
      } else {
         if (switch_print == 0)
            printf("      => %-30s  %-6d %+5.1f  %+5.1f  %5.1f   %.2le  %.2le  %.2le  %.2le   %.2le      %.2f        %.2f    %.2le   %.2le        %.2le\n",
                   tmp.c_str(), k + 1, list_halos[k].PsiDeg, list_halos[k].ThetaDeg, phi * RAD_to_DEG,
                   list_halos[k].Mtot, list_halos[k].l, list_j[i].first, list_j[i].first / list_contrast[l].first,
                   list_contrast[l].first, log10(list_j[i].first), log10(list_j[i].first / list_contrast[l].first),
                   asin(list_halos[k].Rscale / list_halos[k].l) * RAD_to_DEG, gSIM_ALPHAINT / asin(list_halos[k].Rscale / list_halos[k].l), list_boost[i].first);
         else if (switch_print == 1)
            printf("      => %-30s  %-6d %+5.1f  %+5.1f  %5.1f   %.2le  %.2le  %.2le  %.2le   %.2le      %.2f        %.2f     %.2le\n",
                   tmp.c_str(), k + 1, list_halos[k].PsiDeg, list_halos[k].ThetaDeg, phi * RAD_to_DEG,
                   list_halos[k].Mtot, list_halos[k].l, list_j[i].first, list_j[i].first / list_contrast[l].first,
                   list_contrast[l].first, log10(list_j[i].first), log10(list_j[i].first / list_contrast[l].first), list_boost[i].first);
         else if (switch_print == 2)
            printf("      => %-30s  %-6d %+5.1f  %+5.1f   %.2le  %.2le  %.2le     %.2le\n",
                   tmp.c_str(), k + 1, list_halos[k].PsiDeg, list_halos[k].ThetaDeg, list_halos[k].l,
                   asin(list_halos[k].Rscale / list_halos[k].l) * RAD_to_DEG, list_j[i].first, list_boost[i].first);

      }
   }


   // Sorted by contrast
   printf("\n");
   printf("         ---------------------------------------------------\n");
   printf("       > Sorted by j_halo/j_gal values (only those >%.2le):\n", contrast_thresh);
   printf("         ---------------------------------------------------\n");
   if (switch_print == 0) {
      printf("                  halo              #in list       l      b     phi  mass m_%s   d      j_halo    j_gal   j_halo/j_gal log10(j_halo) log10(j_gal) alpha_s  alpha_int/alpha_s", legend_for_delta().c_str());
      if (gPP_DM_IS_ANNIHIL_OR_DECAY && list_boost.size() > 0)
         printf("    boost");
      printf("\n                                                 [deg]  [deg]  [deg]    [Msol]     [kpc]     %s        -    [deg]    -\n", unit.c_str());
   } else if (switch_print == 1) {
      printf("                  halo              #in list        l     b     phi  mass m_%s   d      j_halo    j_gal   j_halo/j_gal log10(j_halo) log10(j_gal)", legend_for_delta().c_str());
      if (gPP_DM_IS_ANNIHIL_OR_DECAY && list_boost.size() > 0)
         printf("    boost");
      printf("\n                                                 [deg]  [deg]  [deg]    [Msol]     [kpc]     %s        -    \n", unit.c_str());
   } else if (switch_print == 2) {
      if (list_boost.size() > 0) printf("    boost\n");
      else printf("\n");
      printf("                  halo              #in list       l     b        d      alpha_s   j_halo");
      if (gPP_DM_IS_ANNIHIL_OR_DECAY && list_boost.size() > 0)
         printf("    boost");
      printf("                                             [deg] [deg]      [kpc]   [deg]    %s\n", unit.c_str());
   }
   for (int i = list_contrast.size() - 1; i >= (int)list_contrast.size() - n_good; --i) {
      // Index k in list of haloes
      int k = list_contrast[i].second;
      // Find corresponding index l in list_j
      int l = 0;
      for (int m = 0; m < (int)list_j.size(); ++m) {
         l = m;
         if (list_j[m].second == k)
            break;
      }

      string tmp = list_halos[k].Name + " (" + list_halos[k].Type + ")";
      double phi = psitheta_to_phi(list_halos[k].PsiDeg * DEG_to_RAD, list_halos[k].ThetaDeg * DEG_to_RAD);
      if (list_boost.size() == 0) {
         if (switch_print == 0)
            printf("      => %-30s  %-6d %+5.1f  %+5.1f  %5.1f   %.2le  %.2le  %.2le  %.2le   %.2le      %.2f        %.2f    %.2le   %.2le\n",
                   tmp.c_str(), k + 1, list_halos[k].PsiDeg, list_halos[k].ThetaDeg, phi * RAD_to_DEG,
                   list_halos[k].Mtot, list_halos[k].l, list_j[l].first, list_j[l].first / list_contrast[i].first,
                   list_contrast[i].first, log10(list_j[l].first), log10(list_j[l].first / list_contrast[i].first),
                   asin(list_halos[k].Rscale / list_halos[k].l) * RAD_to_DEG, gSIM_ALPHAINT / asin(list_halos[k].Rscale / list_halos[k].l));
         else if (switch_print == 1)
            printf("      => %-30s  %-6d %+5.1f  %+5.1f  %5.1f   %.2le  %.2le  %.2le  %.2le   %.2le      %.2f        %.2f\n",
                   tmp.c_str(), k + 1, list_halos[k].PsiDeg, list_halos[k].ThetaDeg, phi * RAD_to_DEG,
                   list_halos[k].Mtot, list_halos[k].l, list_j[l].first, list_j[l].first / list_contrast[i].first,
                   list_contrast[i].first, log10(list_j[l].first), log10(list_j[l].first / list_contrast[i].first));
         else if (switch_print == 2)
            printf("      => %-30s  %-6d %+5.1f  %+5.1f   %.2le  %.2le  %.2le\n",
                   tmp.c_str(), k + 1, list_halos[k].PsiDeg, list_halos[k].ThetaDeg,
                   list_halos[k].l, asin(list_halos[k].Rscale / list_halos[k].l) * RAD_to_DEG, list_j[l].first);
      } else {
         if (switch_print == 0)
            printf("      => %-30s  %-6d %+5.1f  %+5.1f  %5.1f   %.2le  %.2le  %.2le  %.2le   %.2le      %.2f        %.2f    %.2le   %.2le        %.2le\n",
                   tmp.c_str(), k + 1, list_halos[k].PsiDeg, list_halos[k].ThetaDeg, phi * RAD_to_DEG,
                   list_halos[k].Mtot, list_halos[k].l, list_j[l].first, list_j[l].first / list_contrast[i].first,
                   list_contrast[i].first, log10(list_j[l].first), log10(list_j[l].first / list_contrast[i].first),
                   asin(list_halos[k].Rscale / list_halos[k].l) * RAD_to_DEG, gSIM_ALPHAINT / asin(list_halos[k].Rscale / list_halos[k].l), list_boost[k].first);
         else if (switch_print == 1)
            printf("      => %-30s  %-6d %+5.1f  %+5.1f %5.1f   %.2le  %.2le  %.2le  %.2le   %.2le      %.2f        %.2f     %.2le\n",
                   tmp.c_str(), k + 1, list_halos[k].PsiDeg, list_halos[k].ThetaDeg, phi * RAD_to_DEG,
                   list_halos[k].Mtot, list_halos[k].l, list_j[l].first, list_j[l].first / list_contrast[i].first,
                   list_contrast[i].first, log10(list_j[l].first), log10(list_j[l].first / list_contrast[i].first), list_boost[k].first);
         else if (switch_print == 2)
            printf("      => %-30s  %-6d %+5.1f  %+5.1f   %.2le  %.2le  %.2le     %.2le\n",
                   tmp.c_str(), k + 1, list_halos[k].PsiDeg, list_halos[k].ThetaDeg,
                   list_halos[k].l, asin(list_halos[k].Rscale / list_halos[k].l) * RAD_to_DEG, list_j[l].first, list_boost[k].first);
      }
   }
}

//______________________________________________________________________________
void stat_CLs(vector<double> const &x, string const &stat_files,
              vector<double> const &cls, int switch_y,
              double const &rho_sat, double const &eps,
              double const &alphaint, int switch_stat, string const &data_files)
{
   //--- Draws CLs for the quantity y(x) and 1/2*(|y(x)|-|y_ref(x)|), where x is in log scale.
   //    N.B.: a stat. analysis file must be formatted as described in stat_load_list() and
   //    a data file as described in halo_load_data4jeans(). If both a list of stat. and data files
   //    are provided, they must correspond to the same object.
   //
   //  x              Grid of x-axis values
   //  stat_files     Stat. analysis files from which the CL are obtained
   //  cls            List of x% CL to be drawn
   //  rho_sat        Saturation density (in Msol/kpc3)
   //  switch_y       Quantity to display [default = 0]
   //                    0->rho(r_kpc)
   //                    1->J(alphaint_deg)
   //                    2->J(theta_deg)
   //                    3->sigma_p(R_kpc) or sigma_p2(R_kpc) with associated data [optional]
   //                    4->vr2(r_kpc) (unprojected solution of jeans equation (velocity dispersion squared))
   //                    5->beta_anis(r_kpc)
   //                    6->nu(r_kpc) (light profile)
   //                    7->I(R_kpc)  (projected light profile - surface brightness)
   //                    8->M(r_kpc)
   //  eps[1.e-3]     Relative precision demanded for the calculation (non-applicable for switch_y=0, 5)
   //  switch_stat    Stat. mode [default = 0]
   //                    0->uses the PDF CL method,
   //                    1->uses the chi2 CL method,
   //                    2->uses both methods
   //                    3->uses mean and dispersion (e.g., for bootstrap)
   //  data_files     Data (velocity dispersion, surface brightness) on which to superimpose the median + CLs

   double old_alpha_aperture = gSIM_ALPHAINT;

#if IS_ROOT
   if ((int)cls.size() == 0) {
      cout << "   No CL found => return" << endl;
      return;
   }

   // Set ROOT style
   rootstyle_set2CLUMPY();


   // list of possible TypeData keywords
   const int n_typedata = 4;
   const char keyword_typedata[n_typedata][50] = {"Vel", "Sigmap2", "Sigmap", "SB"};

   vector<string> files;
   stat_file2files(stat_files, files);
   const int n_files = files.size();
   if (n_files > 1 && switch_stat == 2) {
      cout << "   You are not allowed to use switch_stat=2 with more than 1 stat_file!" << endl;
      return;
   }

   gDM_RHOSAT = rho_sat;
   int n_x = x.size();
   vector<double> x_cl = cls;
   sort(x_cl.begin(), x_cl.end());  // sort in growing order
   int n_cls = x_cl.size();
   if (switch_stat == 3)
      n_cls = 1;

   bool is_sigmap2 = 0;
   bool is_sigmap = 0;
   bool is_surf_brightness = 0;
   bool is_data_files = 0;

   if (data_files != "unused") // Is there a data file?
      is_data_files = 1;

   // What is going to be calculated (for prints and plots)
   //  => 3 values for stat (switch_stat=0, 1, or 2)
   const int n_st = 4, n_switch = 9, n_dm = 2, n_units = 2;
   string stat[n_st] = {"PDF method", "chi2 method", "both stat. methods", "average+dispersion"};
   //  => 3 values or y (switch_y=0, 1, or 2)
   string x_name[n_switch] = {"r", "#alpha_{int}", "#theta from halo centre", "R", "r", "r", "r", "R", "r"};
   string x_unit[n_switch] = {"[kpc]", "[deg]", "[deg]", "[kpc]", "[kpc]", "[kpc]", "[kpc]", "[kpc]", "[kpc]"};
   //  => 2 values for annihilation or decay (set by gPP_DM_IS_ANNIHIL_OR_DECAY)
   string y_name[n_dm][n_switch] = { {"#rho(r)", "D(#alpha_{int})", "D(#theta)", "#sigma_{p}", "v_{r}^{2}(r)", "#beta_{ani}(r)", "#nu(r)", "I(R)", "M(r)"},
      {"#rho(r)", "J(#alpha_{int})", "J(#theta)", "#sigma_{p}", "v_{r}^{2}(r)",  "#beta_{ani}(r)", "#nu(r)", "I(R)", "M(r)"}
   };
   string y_name_canvas[n_dm][n_switch] = { {"rhor", "Dalphaint", "Dtheta", "sigma_p", "vr2", "beta", "nu", "I", "Mr"}, {"rhor", "Jalphaint", "Jtheta", "sigma_p", "vr2", "beta", "nu", "I", "Mr"}};
   string y_unit[n_units][n_dm][n_switch] = {
      {  {"[GeV cm^{-3}]", "[GeV cm^{-2}]", "[GeV cm^{-2}]", "[km s^{-1}]", "[km^{2} s^{-2}]", "", "[L_{#odot} cm^{-3}]", "[L_{#odot} cm^{-2}]", "[M_{#odot}]"},
         {"[GeV cm^{-3}]", "[GeV^{2} cm^{-5}]", "[GeV^{2} cm^{-5}]", "[km s^{-1}]", "[km^{2} s^{-2}]", "", "[L_{#odot} cm^{-3}]", "[L_{#odot} cm^{-2}]", "[M_{#odot}]"}
      },
      {  {"[M_{#odot} kpc^{-3}]", "[M_{#odot} kpc^{-2}]", "[M_{#odot} kpc^{-2}]", "[km s^{-1}]", "[km^{2} s^{-2}]", "", "[L_{#odot} kpc^{-3}]", "[L_{#odot} kpc^{-2}]", "[M_{#odot}]"},
         {"[M_{#odot} kpc^{-3}]", "[M_{#odot}^{2} kpc^{-5}]", "[M_{#odot}^{2} kpc^{-5}]", "[km s^{-1}]", "[km^{2} s^{-2}]", "", "[L_{#odot} kpc^{-3}]", "[L_{#odot} kpc^{-2}]", "[M_{#odot}]"}
      },
   };

   string x_axis[n_switch] = {"log_{10}(r/kpc)", "log_{10}(#alpha_{int}/deg)", "log_{10}(#theta)/deg", "R/kpc", "r/kpc", "log_{10}(r/kpc)", "log_{10}(r/kpc)", "log_{10}(R/kpc)", "log_{10}(r/kpc)"};
   string y_axis[n_units][n_dm][n_switch] = {
      {
         {
            "log_{10}(#rho(r)/ (GeV cm^{-3}))", "log_{10}(D(#alpha_{int})/ (GeV cm^{-2}))", "log_{10}(D(#theta)/ (GeV cm^{-2}))",
            "log_{10}(#sigma_{p}/(km s^{-1}))", "log_{10}(v_{r}^{2})/(km^{2} s^{-2})", "#beta_{ani}", "log_{10}(#nu(r)/(L_{#odot} cm^{-3})", "log_{10}(I(R)/(L_{#odot} cm^{-2})", "log_{10}( M(r)/ M_{#odot} )"
         },
         {
            "log_{10}(#rho(r)/(GeV cm^{-3}))", "log_{10}(J(#alpha_{int})/ (GeV^{2} cm^{-5}))", "log_{10}(J(#theta)/ (GeV^{2} cm^{-5}))",
            "log_{10}(#sigma_{p}/(km s^{-1}))", "log_{10}(v_{r}^{2})/(km^{2} s^{-2})",  "#beta_{ani}", "log_{10}(#nu(r)/(L_{#odot} cm^{-3})", "log_{10}(I(R)/(L_{#odot} cm^{-2})", "log_{10}( M(r)/ M_{#odot} )"
         }
      },
      {
         {
            "log_{10}(#rho(r)/ (M_{#odot} kpc^{-3}))", "log_{10}(D(#alpha_{int})/ (M_{#odot} kpc^{-2}))", "log_{10}(D(#theta)/ (M_{#odot} kpc^{-2}))",
            "log_{10}(#sigma_{p}/(km s^{-1}))", "log_{10}(v_{r}^{2})/(km^{2} s^{-2})", "#beta_{ani}", "log_{10}(#nu(r)/(L_{#odot} kpc^{-3})", "log_{10}(I(R)/(L_{#odot} kpc^{-2})", "log_{10}( M(r)/ M_{#odot} )"
         },
         {
            "log_{10}(#rho(r)/(M_{#odot} kpc^{-3}))", "log_{10}(J(#alpha_{int})/ (M_{#odot}^{2} kpc^{-5}))", "log_{10}(J(#theta)/ (M_{#odot}^{2} kpc^{-5}))",
            "log_{10}(#sigma_{p}/(km s^{-1}))", "log_{10}(v_{r}^{2})/(km^{2} s^{-2})",  "#beta_{ani}", "log_{10}(#nu(r)/(L_{#odot} kpc^{-3})", "log_{10}(I(R)/(L_{#odot} kpc^{-2})", "log_{10}( M(r)/ M_{#odot} )"
         }
      }
   };
   string y_axis_centered[n_dm][n_switch] = { {"log_{10}(#rho/#rho", "log_{10}(D/D", "log_{10}(D/D", "log_{10}(#sigma_{p}/#sigma_{p}",
         "log_{10}(v_{r}^{2}/v_{r}^{2}", "#beta_{ani}/#beta_{ani}", "log_{10}(#nu/#nu", "log_{10}(I/I", "log_{10}(M/M"
      },
      {
         "log_{10}(#rho/#rho", "log_{10}(J/J", "log_{10}(J/J", "log_{10}(#sigma_{p}/#sigma_{p}",
         "log_{10}(v_{r}^{2}/v_{r}^{2}", "#beta_{ani}/#beta_{ani}", "log_{10}(#nu/#nu", "log_{10}(I/I", "log_{10}(M/M"
      }
   };
   if (switch_stat == 1  && switch_y != 5)
      y_axis_centered[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y] += "_{best}";
   else if (switch_stat == 0  && switch_y != 5)
      y_axis_centered[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y] += "_{med})";
   else if (switch_stat == 1 && switch_y == 5)
      y_axis_centered[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y] += "_{best}";
   else if (switch_stat == 0 && switch_y == 5)
      y_axis_centered[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y] += "_{med}";


   cout << ">>>>> " << y_axis[gSIM_IS_ASTRO_OR_PP_UNITS][gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y] << " CLs from the stat. analysis [file=" << stat_files << "]" << endl;
   cout << "      => " << stat[switch_stat] << endl;
   if (switch_y == 1 || switch_y == 2 || switch_y == 3 || switch_y == 6 || switch_y == 7 || switch_y == 8) {
      cout << "  - Relative precision: " << eps << endl;
      cout << "  - Confidence levels: ";
      for (int i = 0; i < (int)cls.size(); ++i) {
         if (i == (int)cls.size() - 1) cout << cls[i] << endl;
         else cout << cls[i] << ", ";
      }
   }

   //--- Storage quantities

   vector< vector <double> > x_data;
   vector< vector <double> > dx_data;
   vector< vector <double> > y_data;
   vector< vector <double> > dy_data;
   vector<int> i_best;
   vector<string> names;
   vector<int> n_stat;
   vector<double> chi2_xcl(n_files * n_cls, 0.);
   vector<double> logy_mean(n_files * n_x, 0.);
   vector<double> logy_median(n_files * n_x, 0.);
   vector<double> logy_mostlikely(n_files * n_x, 0.);
   vector<double> logy_bestchi2(n_files * n_x, 0.);

   vector<double> logy_pdf_cl_up(n_files * n_cls * n_x, 0.);
   vector<double> logy_pdf_cl_lo(n_files * n_cls * n_x, 0.);
   vector<double> logy_pdf_centered_cl_up(n_files * n_cls * n_x, 0.);
   vector<double> logy_pdf_centered_cl_lo(n_files * n_cls * n_x, 0.);
   vector<double> logy_chi2_cl_up(n_files * n_cls * n_x, 0.);
   vector<double> logy_chi2_cl_lo(n_files * n_cls * n_x, 0.);
   vector<double> logy_chi2_centered_cl_up(n_files * n_cls * n_x, 0.);
   vector<double> logy_chi2_centered_cl_lo(n_files * n_cls * n_x, 0.);

   TH2D *h2d_pdf = NULL;
   TH2D *h2d_pdf_centered = NULL;

   vector<string> datafiles;
   if (is_data_files) {
      halo_files2files_data(data_files, datafiles); // fill a vector of data files
      int n_datafiles = datafiles.size(); // number of data files
      if (n_datafiles != n_files) {
         cout << " Number of stat files and data files are different: abort " << endl;
         return;
      }
   }

   //--- Loop on all stat. analysis files: calculate y(x) for all models in a file
   for (int i = 0; i < n_files; ++i) {
      // Load and store the i-th stat. analysis file
      struct gStructHalo stat_halo;
      stat_load_list(files[i], stat_halo, false);
      if (switch_stat == 3)
         i_best.push_back(stat_find_bestmodel(files[i], 1)); // best chi2 or likelihood of file i
      else
         i_best.push_back(stat_find_bestmodel(files[i], switch_stat)); // best chi2 or likelihood of file i

      names.push_back(stat_halo.Name);
      n_stat.push_back(stat_halo.StatChi2.size());
      cout << "   ... processing " << files[i] << " ..." << endl;

      if (is_data_files) {
         struct gStructJeansData data_halo;
         halo_load_data4jeans(datafiles[i], data_halo, 0); // load data file
         stat_halo.JeansData = data_halo;
         if (stat_halo.JeansData.TypeData == keyword_typedata[0]) {
            printf("\n====> ERROR: stat_CLs() in janalysis.cc");
            printf("\n             At least one file uses Velocity data instead of Velocity dispersion");
            printf("\n             => abort()\n\n");
            abort();
         } else if (stat_halo.JeansData.TypeData == keyword_typedata[1]) // Data: Sigmap2
            is_sigmap2 = 1;
         else if (stat_halo.JeansData.TypeData == keyword_typedata[2]) // Data: Sigmap
            is_sigmap = 1;
         else if (stat_halo.JeansData.TypeData == keyword_typedata[3]) // Data: Surface brightness
            is_surf_brightness = 1;

         if ((is_sigmap && is_sigmap2) || (is_sigmap && is_surf_brightness) || (is_sigmap2 && is_surf_brightness)) {
            printf("\n====> ERROR: stat_CLs() in janalysis.cc");
            printf("\n             At least two data files do not share the same TypeData Keyword");
            printf("\n             => abort()\n\n");
            abort();
         }

         if (is_sigmap2) { // Data: Sigmap2
            y_name[0][3] = "#sigma_{p}^{2}";
            y_name[1][3] = "#sigma_{p}^{2}";
            y_name_canvas[0][3] = "sigma_p2";
            y_name_canvas[1][3] = "sigma_p2";
            y_unit[0][0][3] = "[km^{2} s^{-2}]";
            y_unit[0][1][3] = "[km^{2} s^{-2}]";
            y_axis[0][0][3] = "log_{10}(#sigma_{p}^{2}}/(km^{2} s^{-2}))";
            y_axis[0][1][3] = "log_{10}(#sigma_{p}^{2}}/(km^{2} s^{-2}))";
            y_unit[1][0][3] = "[km^{2} s^{-2}]";
            y_unit[1][1][3] = "[km^{2} s^{-2}]";
            y_axis[1][0][3] = "log_{10}(#sigma_{p}^{2}}/(km^{2} s^{-2}))";
            y_axis[1][1][3] = "log_{10}(#sigma_{p}^{2}}/(km^{2} s^{-2}))";
            y_axis_centered[0][3] = "log_{10}(#sigma_{p}^{2}/#sigma_{p}^{2}";
            y_axis_centered[1][3] = "log_{10}(#sigma_{p}^{2}/#sigma_{p}^{2}";

            if (switch_stat == 1  && switch_y != 5)
               y_axis_centered[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y] += "_{best}";
            else if (switch_stat == 0  && switch_y != 5)
               y_axis_centered[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y] += "_{med})";
            else if (switch_stat == 1 && switch_y == 5)
               y_axis_centered[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y] += "_{best}";
            else if (switch_stat == 0 && switch_y == 5)
               y_axis_centered[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y] += "_{med}";

         }

         if (is_surf_brightness) { // Data: surface brightness
            y_name[0][3] = "Surface Brightness";
            y_name[1][3] = "Surface Brightness";
            y_name_canvas[0][3] = "SB";
            y_name_canvas[1][3] = "SB";
            y_unit[0][0][3] = "[L_{#odot} kpc^{-2}]";
            y_unit[0][1][3] = "[L_{#odot} kpc^{-2}]";
            y_axis[0][0][3] = "log_{10}(I/(L_{#odot} kpc^{-2}))";
            y_axis[0][1][3] = "log_{10}(I/(L_{#odot} kpc^{-2}))";
            y_unit[1][0][3] = "[L_{#odot} kpc^{-2}]";
            y_unit[1][1][3] = "[L_{#odot} kpc^{-2}]";
            y_axis[1][0][3] = "log_{10}(I/(L_{#odot} kpc^{-2}))";
            y_axis[1][1][3] = "log_{10}(I/(L_{#odot} kpc^{-2}))";
            y_axis_centered[0][3] = "log_{10}(I/I";
            y_axis_centered[1][3] = "log_{10}(I/I";

            if (switch_stat == 1  && switch_y != 5)
               y_axis_centered[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y] += "_{best}";
            else if (switch_stat == 0  && switch_y != 5)
               y_axis_centered[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y] += "_{med})";
            else if (switch_stat == 1 && switch_y == 5)
               y_axis_centered[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y] += "_{best}";
            else if (switch_stat == 0 && switch_y == 5)
               y_axis_centered[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y] += "_{med}";
         }
         // Vector containing the data
         vector<double> xdata;
         vector<double> dxdata;
         vector<double> ydata;
         vector<double> dydata;
         int xdatasize = data_halo.Radius.size();

         for (int m = 0; m < xdatasize; m++) { // Fill the data
            xdata.push_back(stat_halo.JeansData.Radius[m]);
            dxdata.push_back(stat_halo.JeansData.ErrRadius[m]);
            if (is_sigmap || is_sigmap2) {
               ydata.push_back(stat_halo.JeansData.VelocData[m]);
               dydata.push_back(stat_halo.JeansData.ErrVelocData[m]);
            } else {
               ydata.push_back(stat_halo.JeansData.LightData[m]);
               dydata.push_back(stat_halo.JeansData.ErrLightData[m]);
            }
         }
         x_data.push_back(xdata);
         dx_data.push_back(dxdata);
         y_data.push_back(ydata);
         dy_data.push_back(dydata);
      }

      // Loop on file entries and calculate/store y(x)
      vector<vector<double> > y(n_stat[i], vector<double>(n_x, 0.));

      const int npar = 10;
      double par[npar];
      double psi_los = 0.;
      double theta_los = 0.;

      // Jeans parameters
      const int nparjeans = 20;
      double par_jeans[nparjeans];
      // Light parameters
      const int npar_light = 8;
      double par_light[npar_light];
      // Anisotropy parameters
      const int npar_anis = 5;
      double par_anis[npar_anis];

      for (int j = 0; j < n_stat[i]; ++j) {
         // Update content of par[]
         // Switch: to calculate M(r)
         //----------------------------
         if (switch_y == 8) {
            stat_set_partot(j, par, stat_halo, true);
            double rvir = par[6];
            for (int k = 0; k < n_x; ++k) {
               // If x>Rvir, stop integration at Rvir nonetheless
               par[6] = min(x[k], rvir);
               y[j][k] = mass_singlehalo(par, eps);
               if (y[j][k] < 1.e-40) y[j][k] = 1.e-40;
            }
            // Switch: to calculate rho(r)
            //----------------------------
         } else if (switch_y == 0) {
            stat_set_partot(j, par, stat_halo, true);
            for (int k = 0; k < n_x; ++k) {
               double tmp_x = x[k];
               y[j][k] = 0.;
               rho(tmp_x, par, y[j][k]);
               convert_to_PP_units(0, y[j][k]);
               if (y[j][k] < 1.e-40) y[j][k] = 1.e-40;
            }
            // Switch: to calculate J(alpha_int) [or D]
            //-----------------------------------------
         } else if (switch_y == 1) {
            stat_set_partot(j, par, stat_halo, true);
            //print_partot(par);
            for (int k = 0; k < n_x; ++k) {
               gSIM_ALPHAINT = x[k] * DEG_to_RAD;
               y[j][k] = 0.;
               if (gSIM_ALPHAINT < 1.e-30) y[j][k] = 0.;
               else y[j][k] = jsmooth(par, psi_los, theta_los, eps);
               convert_to_PP_units(1, y[j][k]);
            }
            if (j % 100 == 0)
               cout << "   -> " << j + 1 << " models treated (out of " << n_stat[i] << " configurations)" << endl;
            // Switch: to calculate J(theta) [or D]
            //-------------------------------------
         } else if (switch_y == 2) {
            stat_set_partot(j, par, stat_halo, true);
            gSIM_ALPHAINT = alphaint;
            for (int k = 0; k < n_x; ++k) {
               psi_los = x[k] * DEG_to_RAD;
               y[j][k] = 0.;
               y[j][k] = jsmooth(par, psi_los, theta_los, eps);
               convert_to_PP_units(1, y[j][k]);
               if (y[j][k] < 1.e-40) y[j][k] = 1.e-40;
            }
            if (j % 100 == 0)
               cout << "   -> " << j + 1 << " models treated (out of " << n_stat[i] << " configurations)" << endl;
            // Switch: to calculate sigmap(R)
            //-------------------------------------
         } else if (switch_y == 3) {
            stat_set_partot(j, par, stat_halo, true); // set parameters
            // Print error message if needed parameters are missing
            if (stat_halo.StatHaloProfile.size() == 0) {
               printf("\n====> ERROR: stat_CLs() in janalysis.cc");
               printf("\n             No halo profile found in file %s", files[i].c_str());
               printf("\n             => abort()\n\n");
               abort();
            }
            if (stat_halo.StatLightProfile.size() == 0) {
               printf("\n====> ERROR: stat_CLs() in janalysis.cc");
               printf("\n             No light profile found in file %s", files[i].c_str());
               printf("\n             => abort()\n\n");
               abort();
            }
            if (stat_halo.StatAnisoProfile.size() == 0) {
               printf("\n====> ERROR: stat_CLs() in janalysis.cc");
               printf("\n             No anisotropy profile found in file %s", files[i].c_str());
               printf("\n             => abort()\n\n");
               abort();
            }
            stat_set_parjeans(j, par_jeans, stat_halo); // set the Jeans parameters
            par_jeans[6] = gSIM_JEANS_RMAX; // Set the maximum radius of integration
            par_jeans[14] = eps; // ! In stat_set_parjeans, eps (par_jeans[14]) is fixed to a default value of 0.01

            for (int k = 0; k < n_x; ++k) {
               par_jeans[13] = x[k]; // set the radius for computation of velocity dispersion
               double radius = x[k];
               y[j][k] = jeans_sigmap2(radius, par_jeans);
               if (!is_sigmap2)
                  y[j][k] = sqrt(y[j][k]);
               if (y[j][k] < 1.e-40) y[j][k] = 1.e-40;
            }
            if (j % 100 == 0)
               cout << "   -> " << j + 1 << " models treated (out of " << n_stat[i] << " configurations)" << endl;
            // Switch: to calculate vr2(r)
            //-------------------------------------
         } else if (switch_y == 4) {
            stat_set_partot(j, par, stat_halo, true); // set parameters
            // Print error message if needed parameters are missing
            if (stat_halo.StatHaloProfile.size() == 0) {
               printf("\n====> ERROR: stat_CLs() in janalysis.cc");
               printf("\n             No halo profile found in file %s", files[i].c_str());
               printf("\n             => abort()\n\n");
               abort();
            }
            if (stat_halo.StatLightProfile.size() == 0) {
               printf("\n====> ERROR: stat_CLs() in janalysis.cc");
               printf("\n             No light profile found in file %s", files[i].c_str());
               printf("\n             => abort()\n\n");
               abort();
            }
            if (stat_halo.StatAnisoProfile.size() == 0) {
               printf("\n====> ERROR: stat_CLs() in janalysis.cc");
               printf("\n             No anisotropy profile found in file %s", files[i].c_str());
               printf("\n             => abort()\n\n");
               abort();
            }
            stat_set_parjeans(j, par_jeans, stat_halo); // set the Jeans parameters
            par_jeans[6] = gSIM_JEANS_RMAX; // Set the maximum radius of integration
            par_jeans[14] = eps; // ! In stat_set_parjeans, eps (par_jeans[14]) is fixed to a default value

            for (int k = 0; k < n_x; ++k) {
               par_jeans[13] = x[k]; // set the radius for computation of velocity dispersion
               double radius = x[k];
               y[j][k] = jeans_nuvr2(radius, par_jeans);
               par_jeans[13] = par_jeans[6]; // set maximum radius of integration for nu (needed for some cases)
               double nu = light_nu(radius, &par_jeans[7]);
               if (nu < 1.e-20) {
                  cout << "The number density nu is 0 or negative at radius r = " << radius << ". nuvr2 set to 0" << endl;
                  y[j][k] = 0.;
                  continue;
               }
               y[j][k] *= NEWTON_CONSTANT_SI * M3perKG_to_KM2KPCperMSOL / nu; // Conversion to km^2/s^2
               if (y[j][k] < 1.e-40) y[j][k] = 1.e-40;
            }
            if (j % 100 == 0)
               cout << "   -> " << j + 1 << " models treated (out of " << n_stat[i] << " configurations)" << endl;
            // Switch: to calculate beta_ani(r)
            //----------------------------
         } else if (switch_y == 5) {
            stat_set_paranis(j, par_anis, stat_halo);
            for (int k = 0; k < n_x; ++k) {
               double radius = x[k]; // set the radius for computation of velocity anisotropy
               y[j][k] = beta_anisotropy(radius, par_anis);
               if (abs(y[j][k]) < 1.e-40) y[j][k] = 1.e-40;
            }
            if (j % 100 == 0)
               cout << "   -> " << j + 1 << " models treated (out of " << n_stat[i] << " configurations)" << endl;
            // Switch: to calculate nu(r) (lightprofile)
            //----------------------------
         } else if (switch_y == 6) {
            stat_set_parlight(j, par_light, stat_halo);
            par_light[6] = gSIM_JEANS_RMAX; // Set the maximum radius of integration
            par_light[7] = eps;
            for (int k = 0; k < n_x; ++k) {
               double radius = x[k]; // set the radius
               y[j][k] = light_nu(radius, par_light);
               if (y[j][k] < 1.e-40) y[j][k] = 1.e-40;
            }
            if (j % 100 == 0)
               cout << "   -> " << j + 1 << " models treated (out of " << n_stat[i] << " configurations)" << endl;
            // Switch: to calculate I(R) (projected light profile)
            //----------------------------
         } else if (switch_y == 7) {
            stat_set_parlight(j, par_light, stat_halo);
            par_light[6] = gSIM_JEANS_RMAX; // Set the maximum radius of integration
            par_light[7] = eps;
            for (int k = 0; k < n_x; ++k) {
               double radius = x[k]; // set the radius
               y[j][k] = light_i(radius, par_light);
               if (y[j][k] < 1.e-40) y[j][k] = 1.e-40;
            }
            if (j % 100 == 0)
               cout << "   -> " << j + 1 << " models treated (out of " << n_stat[i] << " configurations)" << endl;
         }
      }

      //--- Calculate CLs: switch_stat
      //  3. <y> and sigma_y from values
      if (switch_stat == 3) {
         for (int k = 0; k < n_x; ++k) {
            double mean_y = 0., sigma_y = 0.;

            // Calculate mean
            for (int j = 0; j < n_stat[i]; ++j) {
               if (switch_y == 5)
                  mean_y += y[j][k];
               else
                  mean_y += log10(y[j][k]);
            }
            mean_y /= n_stat[i];
            logy_mean[i * n_x + k] = mean_y;
            logy_median[i * n_x + k] = mean_y;
            logy_mostlikely[i * n_x + k] = mean_y;

            if (switch_y == 5)
               logy_bestchi2[i * n_x + k] = y[i_best[i]][k]; // beta_anisotropy can be negative
            else
               logy_bestchi2[i * n_x + k] = log10(y[i_best[i]][k]);

            // Calculate sigma
            for (int j = 0; j < n_stat[i]; ++j) {
               if (switch_y == 5)
                  sigma_y += (y[j][k] - mean_y) * (y[j][k] - mean_y);
               else
                  sigma_y += (log10(y[j][k]) - mean_y) * (log10(y[j][k]) - mean_y);
            }
            sigma_y /= (n_stat[i] - 1);
            sigma_y = sqrt(sigma_y);

            // PDF quantiles (calculated for each rho(r))
            for (int l = 0; l < n_cls; ++l) {
               logy_pdf_cl_lo[i * n_cls * n_x + l * n_x + k] = mean_y - sigma_y;
               logy_pdf_cl_up[i * n_cls * n_x + l * n_x + k] = mean_y + sigma_y;

               if (switch_y == 5) {
                  logy_pdf_centered_cl_lo[i * n_cls * n_x + l * n_x + k] = logy_pdf_cl_lo[i * n_cls * n_x + l * n_x + k] / logy_median[i * n_x + k];
                  logy_pdf_centered_cl_up[i * n_cls * n_x + l * n_x + k] = logy_pdf_cl_up[i * n_cls * n_x + l * n_x + k] / logy_median[i * n_x + k];
               } else {
                  logy_pdf_centered_cl_lo[i * n_cls * n_x + l * n_x + k] = logy_pdf_cl_lo[i * n_cls * n_x + l * n_x + k] - logy_median[i * n_x + k];
                  logy_pdf_centered_cl_up[i * n_cls * n_x + l * n_x + k] = logy_pdf_cl_up[i * n_cls * n_x + l * n_x + k] - logy_median[i * n_x + k];
               }
            }
         }
      } else {

         //  1. CLs from PDF method
         for (int k = 0; k < n_x; ++k) {
            double logy_min_old = 1.e40;
            double logy_max_old = -1.e40;

            for (int j = 0; j < n_stat[i]; ++j) { // find min and max
               double logy = 0.;
               if (switch_y == 5)
                  logy = y[j][k]; // beta_anisotropy can be negative
               else
                  logy = log10(y[j][k]);

               if (logy < logy_min_old) logy_min_old = logy;
               if (logy > logy_max_old) logy_max_old = logy;
            }

            double size_bin; // bin size of the histogram used to compute the CLs. It is automatically adapted.
            // initial values
            if (switch_y == 3 || switch_y == 4 || switch_y == 5)
               size_bin = 0.005; // typical values
            else // quantities with more spread
               size_bin = 0.01;  //typical values

            int nbins = int((logy_max_old - logy_min_old) / size_bin);
            int ny_histo = nbins;

            TH1D *h_logy_temp = new TH1D("", "", ny_histo, logy_min_old, logy_max_old); // temporary histogram for finding the optimal bin size
            for (int j = 0; j < n_stat[i]; ++j) {
               if (switch_y == 5) {
                  if (y[j][k] < logy_max_old && y[j][k] > logy_min_old)
                     h_logy_temp->Fill(y[j][k]);
               } else {
                  if (log10(y[j][k]) < logy_max_old && log10(y[j][k]) > logy_min_old)
                     h_logy_temp->Fill(log10(y[j][k]));
               }
            }
            // Median
            double x_h1d_temp[1] = {0.};
            double q_h1d_temp = 0.5;
            h_logy_temp->GetQuantiles(1, x_h1d_temp, &q_h1d_temp);

            // 1 sigma quantiles
            double ql_temp[2] = {(1. - 0.68) / 2., 0.68 + (1. - 0.68) / 2.};
            double xl_temp[2] = {0., 0.};
            h_logy_temp->GetQuantiles(2, xl_temp, ql_temp);


            double logy_min_new; // optimised values of min and max (5 sigma away from the median; CLs can be wrong if presence of a few points with very large/low values)
            double logy_max_new;

            if (logy_max_old > x_h1d_temp[0] + 5.*(xl_temp[1] - x_h1d_temp[0])) // 5 sigmas away from median
               logy_max_new = x_h1d_temp[0] + 5.*(xl_temp[1] - x_h1d_temp[0]);
            else
               logy_max_new = logy_max_old;

            if (logy_min_old < x_h1d_temp[0] - 5.*(x_h1d_temp[0] - xl_temp[0]))
               logy_min_new = x_h1d_temp[0] - 5.*(x_h1d_temp[0] - xl_temp[0]);
            else
               logy_min_new = logy_min_old;

            double dispersion = 0.5 * (xl_temp[1] - xl_temp[0]); // 0.5 * (68% up - 68% low)
            size_bin = dispersion / 25.; // optimised bin size, depends on the dispersion of the distribution
            nbins = int((logy_max_new - logy_min_new) / size_bin);

            ny_histo = nbins;

            TH1D *h_logy = new TH1D("h_logy", "h_logy", ny_histo, logy_min_new, logy_max_new); // real histo used for the CLs
            for (int j = 0; j < n_stat[i]; ++j) { // Fill histo
               if (switch_y == 5) {
                  if (y[j][k] < logy_max_new && y[j][k] > logy_min_new)
                     h_logy->Fill(y[j][k]);
               } else {
                  if (log10(y[j][k]) < logy_max_new && log10(y[j][k]) > logy_min_new)
                     h_logy->Fill(log10(y[j][k]));
               }
            }

            // Mean
            logy_mean[i * n_x + k] = h_logy->GetMean();

            // Median
            double q_h1d = 0.5;
            double x_h1d[1] = {0.};
            h_logy->GetQuantiles(1, x_h1d, &q_h1d);
            logy_median[i * n_x + k] = x_h1d[0];

            // Most likely
            double step_hlogy = (logy_max_new - logy_min_new) / (double)(ny_histo + 1);
            logy_mostlikely[i * n_x + k] = logy_min_new + h_logy->GetMaximumBin() * step_hlogy;

            // PDF quantiles (calculated for each rho(r))
            for (int l = 0; l < n_cls; ++l) {
               double ql[2] = {(1. - x_cl[l]) / 2., x_cl[l] + (1. - x_cl[l]) / 2.};
               double xl[2] = {0., 0.};
               h_logy->GetQuantiles(2, xl, ql);
               logy_pdf_cl_lo[i * n_cls * n_x + l * n_x + k] = xl[0];
               logy_pdf_cl_up[i * n_cls * n_x + l * n_x + k] = xl[1];
               if (switch_y == 5) {
                  logy_pdf_centered_cl_lo[i * n_cls * n_x + l * n_x + k] = logy_pdf_cl_lo[i * n_cls * n_x + l * n_x + k] / logy_median[i * n_x + k];
                  logy_pdf_centered_cl_up[i * n_cls * n_x + l * n_x + k] = logy_pdf_cl_up[i * n_cls * n_x + l * n_x + k] / logy_median[i * n_x + k];
               } else {
                  logy_pdf_centered_cl_lo[i * n_cls * n_x + l * n_x + k] = logy_pdf_cl_lo[i * n_cls * n_x + l * n_x + k] - logy_median[i * n_x + k];
                  logy_pdf_centered_cl_up[i * n_cls * n_x + l * n_x + k] = logy_pdf_cl_up[i * n_cls * n_x + l * n_x + k] - logy_median[i * n_x + k];
               }
            }
            delete h_logy;
            h_logy = NULL;
         }

         //  2. CLs from chi2 method
         // Store y(x) for best chi2
         for (int k = 0; k < n_x; ++k)
            if (switch_y == 5)
               logy_bestchi2[i * n_x + k] = y[i_best[i]][k]; // beta_anisotropy can be negative
            else
               logy_bestchi2[i * n_x + k] = log10(y[i_best[i]][k]);

         // Find chi2 min, max
         double chi2min = 1.e40, chi2max = -1.e40;
         for (int j = 0; j < n_stat[i]; ++j) {
            if (stat_halo.StatChi2[j] < chi2min) chi2min = stat_halo.StatChi2[j];
            if (stat_halo.StatChi2[j] > chi2max) chi2max = stat_halo.StatChi2[j];
         }
         // Fill a TH1D histo for chi2 to retrieve the quantiles
         TH1D *h_chi2 = new TH1D("chi2", "chi2", n_stat[i], chi2min, chi2max);
         for (int j = 0; j < n_stat[i]; ++j)
            h_chi2->Fill(stat_halo.StatChi2[j]);
         h_chi2->GetQuantiles((int)x_cl.size(), &chi2_xcl[i * n_cls + 0], &x_cl[0]);
         delete h_chi2;
         h_chi2 = NULL;

         // Loop on entries, x values and quantiles
         // but initialise first!
         for (int j = 0; j < n_stat[i]; ++j) {
            for (int k = 0; k < n_x; ++k) {
               for (int l = 0; l < n_cls; ++l) {
                  logy_chi2_cl_lo[i * n_cls * n_x + l * n_x + k] = 100.;
                  logy_chi2_cl_up[i * n_cls * n_x + l * n_x + k] = -100;
               }
            }
         }
         for (int j = 0; j < n_stat[i]; ++j) {
            for (int k = 0; k < n_x; ++k) {
               double logy;
               if (switch_y == 5)
                  logy = y[j][k]; //beta anisotropy can be negative
               else
                  logy = log10(y[j][k]);
               for (int l = 0; l < n_cls; ++l) {
                  if (stat_halo.StatChi2[j] < chi2_xcl[i * n_cls + l]) {
                     if (logy < logy_chi2_cl_lo[i * n_cls * n_x + l * n_x + k]) logy_chi2_cl_lo[i * n_cls * n_x + l * n_x + k] = logy;
                     if (logy > logy_chi2_cl_up[i * n_cls * n_x + l * n_x + k]) logy_chi2_cl_up[i * n_cls * n_x + l * n_x + k] = logy;
                  }
                  //if (k == 0 && j == 0)
                  //   cout << "      CL for " <<  x_cl[l] * 100. << "% of chi2 surface"
                  //        << " => chi2=" << chi2_xcl[i * n_cls + l]
                  //        << " (chi2/dof=" << chi2_xcl[i * n_cls + l] / (double)(stat_halo.Ndata - stat_halo.Npar) << ")" << endl;
               }
            }
         }
         for (int k = 0; k < n_x; ++k) {
            for (int l = 0; l < n_cls; ++l) {
               if (switch_y == 5) {
                  logy_chi2_centered_cl_lo[i * n_cls * n_x + l * n_x + k] = logy_chi2_cl_lo[i * n_cls * n_x + l * n_x + k] / logy_bestchi2[i * n_x + k];
                  logy_chi2_centered_cl_up[i * n_cls * n_x + l * n_x + k] = logy_chi2_cl_up[i * n_cls * n_x + l * n_x + k] / logy_bestchi2[i * n_x + k];
               } else { // log scale
                  logy_chi2_centered_cl_lo[i * n_cls * n_x + l * n_x + k] = logy_chi2_cl_lo[i * n_cls * n_x + l * n_x + k] - logy_bestchi2[i * n_x + k];
                  logy_chi2_centered_cl_up[i * n_cls * n_x + l * n_x + k] = logy_chi2_cl_up[i * n_cls * n_x + l * n_x + k] - logy_bestchi2[i * n_x + k];
               }
            }
         }

         //  3. For PDF only
         if (i == 0) {
            h2d_pdf = new TH2D("h2_pdf", "h2_pdf", n_x - 1, log10(x[0]), log10(x[n_x - 1]),
                               100, logy_pdf_cl_lo[i * n_cls * n_x + (n_cls - 1)*n_x + n_x - 1], logy_pdf_cl_up[i * n_cls * n_x + (n_cls - 1)*n_x + 0]);
            h2d_pdf_centered = new TH2D("h2_pdf_centered", "h2_pdf_centered", n_x - 1, log10(x[0]), log10(x[n_x - 1]),
                                        100, logy_pdf_centered_cl_lo[i * n_cls * n_x + (n_cls - 1)*n_x + n_x - 1], logy_pdf_centered_cl_up[i * n_cls * n_x + (n_cls - 1)*n_x + 0]);
            for (int k = 0; k < n_x; ++k) {
               for (int j = 0; j < n_stat[i]; ++j) {
                  if (switch_y == 5) {
                     h2d_pdf->Fill(log10(x[k]), y[j][k]);
                     h2d_pdf_centered->Fill(log10(x[k]), y[j][k] / logy_median[i * n_x + k]);
                  } else {
                     h2d_pdf->Fill(log10(x[k]), log10(y[j][k]));
                     h2d_pdf_centered->Fill(log10(x[k]), log10(y[j][k]) - logy_median[i * n_x + k]);
                  }
               }
            }
         }
      }


      //--- Print (both on screen and in a file)
      if (gSIM_IS_PRINT) {
         FILE *fp = NULL;
         string infilename = files[i].substr(files[i].find_last_of("\\/") + 1, files[i].size() - 1);
         string f_name = gSIM_OUTPUT_DIR + infilename + "." + y_name_canvas[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y] + "_cls.output";
         for (int ff = 0; ff < 2; ++ff) {
            if (ff == 1) fp = fopen(f_name.c_str(), "w");
            else fp = stdout;

            // Print header
            fprintf(fp, "\n");
            // fprintf(fp, "%s \n", ("   ... processing " + names[i] + " ... ").c_str());
            fprintf(fp, "x= %s %s ,  ", x_name[switch_y].c_str(), x_unit[switch_y].c_str());
            fprintf(fp, "y= %s %s\n", y_name[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y].c_str(),
                    y_unit[gSIM_IS_ASTRO_OR_PP_UNITS][gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y].c_str());
            if (switch_stat == 0 || switch_stat == 2 || switch_stat == 3)
               fprintf(fp, "     x       y_pdf_median  y_pdf_mean  y_mostlikely  y_chi2_best    CL_lo_PDF   CL_up_PDF\n");
            else
               fprintf(fp, "     x       y_pdf_median  y_pdf_mean  y_mostlikely  y_chi2_best    CL_lo_chi2  CL_up_chi2\n");
            fprintf(fp, "                                                                 ");
            for (int l = 0; l < n_cls; ++l)
               fprintf(fp, "|          %.1f%% CL         ", x_cl[l] * 100);
            fprintf(fp, " \n");

            // Print values
            for (int k = 0; k < n_x; ++k) {
               if (switch_y == 5) {
                  fprintf(fp, "%.*le %.*le %.*le %.*le %.*le ", gSIM_SIGDIGITS + 2, x[k], gSIM_SIGDIGITS, logy_median[i * n_x + k],
                          gSIM_SIGDIGITS, logy_mean[i * n_x + k], gSIM_SIGDIGITS, logy_mostlikely[i * n_x + k],
                          gSIM_SIGDIGITS, logy_bestchi2[i * n_x + k]);

                  if (switch_stat == 0 || switch_stat == 2 || switch_stat == 3) {
                     for (int l = 0; l < n_cls; ++l)
                        fprintf(fp, " %.*le %.*le ", gSIM_SIGDIGITS, logy_pdf_cl_lo[i * n_cls * n_x + l * n_x + k],
                                gSIM_SIGDIGITS, logy_pdf_cl_up[i * n_cls * n_x + l * n_x + k]);
                  } else if (switch_stat == 1) {
                     for (int l = 0; l < n_cls; ++l)
                        fprintf(fp, " %.*le %.*le ", gSIM_SIGDIGITS, logy_chi2_cl_lo[i * n_cls * n_x + l * n_x + k],
                                gSIM_SIGDIGITS, logy_chi2_cl_up[i * n_cls * n_x + l * n_x + k]);
                  }
                  fprintf(fp, " \n");
               } else {
                  fprintf(fp, "%.*le %.*le %.*le %.*le %.*le ", gSIM_SIGDIGITS + 2, x[k], gSIM_SIGDIGITS, pow(10., logy_median[i * n_x + k]),
                          gSIM_SIGDIGITS, pow(10., logy_mean[i * n_x + k]), gSIM_SIGDIGITS, pow(10., logy_mostlikely[i * n_x + k]),
                          gSIM_SIGDIGITS, pow(10., logy_bestchi2[i * n_x + k]));


                  if (switch_stat == 0 || switch_stat == 2 || switch_stat == 3) {
                     for (int l = 0; l < n_cls; ++l)
                        fprintf(fp, "  %.*le %.*le ", gSIM_SIGDIGITS, pow(10., logy_pdf_cl_lo[i * n_cls * n_x + l * n_x + k]),
                                gSIM_SIGDIGITS, pow(10., logy_pdf_cl_up[i * n_cls * n_x + l * n_x + k]));
                  } else {
                     for (int l = 0; l < n_cls; ++l)
                        fprintf(fp, "  %.*le %.*le ", gSIM_SIGDIGITS, pow(10., logy_chi2_cl_lo[i * n_cls * n_x + l * n_x + k]),
                                gSIM_SIGDIGITS, pow(10., logy_chi2_cl_up[i * n_cls * n_x + l * n_x + k]));
                  }
                  fprintf(fp, " \n");
               }
            }
            if (ff == 1) fclose(fp);
         }

         cout << "_______________________" << endl << endl;
         cout << " ... output [ASCII] CLs written in: " << f_name << endl;
         cout << "_______________________" << endl << endl;
      }
   } // end loop on files

   //--- Displays
   if (gSIM_IS_DISPLAY) {

      TGraphErrors **gr_data = new TGraphErrors*[n_files];

      // Allocate and Fill histograms and plots ([2] is for centered values, i.e. second plot)
      TGraph **gr_chi2_best = new TGraph*[n_files];
      TGraph **gr_chi2_cls_lo = new TGraph*[2 * n_cls * n_files];
      TGraph **gr_chi2_cls_up = new TGraph*[2 * n_cls * n_files];
      TGraph **gr_pdf_mean = new TGraph*[n_files];
      TGraph **gr_pdf_median = new TGraph*[n_files];
      TGraph **gr_pdf_mostlikely = new TGraph*[n_files];
      TGraph **gr_pdf_cls_lo = new TGraph*[2 * n_cls * n_files];
      TGraph **gr_pdf_cls_up = new TGraph*[2 * n_cls * n_files];
      TMultiGraph **multi = new TMultiGraph*[2];
      TLegend **leg = new TLegend*[2];

      // Initialisation of legend, histo, multigraph (for the 2 canvas)
      for (int c = 0; c < 2; ++c) {
         if (c == 0) leg[c] = new TLegend(0.18, 0.15, 0.53, 0.55);
         if (c == 1) leg[c] = new TLegend(0.4, 0.15, 0.6, 0.55);
         leg[c]->SetFillColor(kWhite);
         leg[c]->SetTextFont(132);
         leg[c]->SetTextSize(0.045);
         leg[c]->SetBorderSize(0);
         leg[c]->SetFillStyle(0);
         if (n_files == 1)
            leg[c]->SetHeader(names[0].c_str());
      }
      if (h2d_pdf) {
         h2d_pdf->SetMarkerStyle(25);
         h2d_pdf->SetMarkerSize(1.3);
         h2d_pdf->SetMarkerColor(1);
      }
      if (h2d_pdf_centered) {
         h2d_pdf_centered->SetMarkerStyle(25);
         h2d_pdf_centered->SetMarkerSize(1.3);
         h2d_pdf_centered->SetMarkerColor(1);
      }

      // Fill graphs
      for (int i = 0; i < n_files; ++i) {
         // Update/fill simple graphs (mean, median, etc)
         char gr_name[200];
         // Mean (PDF)
         sprintf(gr_name, "gr_pdf_mean_%s", names[i].c_str());
         gr_pdf_mean[i] = new TGraph(n_x - 1);
         gr_pdf_mean[i]->SetName(gr_name);
         gr_pdf_mean[i]->SetLineStyle(1);
         gr_pdf_mean[i]->SetLineColor(kYellow);
         gr_pdf_mean[i]->SetLineWidth(3);
         // Median (PDF)
         sprintf(gr_name, "gr_pdf_median_%s", names[i].c_str());
         gr_pdf_median[i] = new TGraph(n_x - 1);
         gr_pdf_median[i]->SetName(gr_name);
         gr_pdf_median[i]->SetLineStyle(1);
         gr_pdf_median[i]->SetLineColor(kGreen);
         gr_pdf_median[i]->SetLineWidth(3);
         // Most-likely
         sprintf(gr_name, "gr_pdf_mostlikely_%s", names[i].c_str());
         gr_pdf_mostlikely[i] = new TGraph(n_x - 1);
         gr_pdf_mostlikely[i]->SetName(gr_name);
         gr_pdf_mostlikely[i]->SetLineStyle(1);
         gr_pdf_mostlikely[i]->SetLineColor(kGray);
         gr_pdf_mostlikely[i]->SetLineWidth(2);
         // Chi2 best
         sprintf(gr_name, "gr_chi2_best_%s", names[i].c_str());
         gr_chi2_best[i] = new TGraph(n_x - 1);
         gr_chi2_best[i]->SetName(gr_name);
         gr_chi2_best[i]->SetLineStyle(1);
         gr_chi2_best[i]->SetLineColor(kRed);
         gr_chi2_best[i]->SetLineWidth(3);
         // Fill
         for (int k = 0; k < n_x; ++k) {
            double xval = x[k];
            if (switch_y == 5) {
               gr_pdf_mean[i]->SetPoint(k, xval, logy_mean[i * n_x + k]);
               gr_pdf_median[i]->SetPoint(k, xval, logy_median[i * n_x + k]);
               gr_pdf_mostlikely[i]->SetPoint(k, xval, logy_mostlikely[i * n_x + k]);
               gr_chi2_best[i]->SetPoint(k, xval, logy_bestchi2[i * n_x + k]);
            } else {
               gr_pdf_mean[i]->SetPoint(k, xval, pow(10., logy_mean[i * n_x + k]));
               gr_pdf_median[i]->SetPoint(k, xval, pow(10., logy_median[i * n_x + k]));
               gr_pdf_mostlikely[i]->SetPoint(k, xval, pow(10., logy_mostlikely[i * n_x + k]));
               gr_chi2_best[i]->SetPoint(k, xval, pow(10., logy_bestchi2[i * n_x + k]));
            }
            if (switch_stat == 2) {
               xval = log10(x[k]);
               gr_pdf_mean[i]->SetPoint(k, xval, logy_mean[i * n_x + k]);
               gr_pdf_median[i]->SetPoint(k, xval, logy_median[i * n_x + k]);
               gr_pdf_mostlikely[i]->SetPoint(k, xval, logy_mostlikely[i * n_x + k]);
               gr_chi2_best[i]->SetPoint(k, xval, logy_bestchi2[i * n_x + k]);

            }
         }

         // Update/fill CLs graphs
         for (int c = 0; c < 2; ++c) {
            for (int l = 0; l < n_cls; ++l) {
               char gr_cl_name[500];
               if (c == 0) sprintf(gr_cl_name, "gr_chi2_cls_lo_%s_%d", names[i].c_str(), l);
               else if (c == 1) sprintf(gr_cl_name, "gr_chi2_cls_lo_centered_%s_%d", names[i].c_str(), l);
               gr_chi2_cls_lo[(c * n_cls * n_files) + n_cls * i + l] = new TGraph(n_x);
               gr_chi2_cls_lo[(c * n_cls * n_files) + n_cls * i + l]->SetName(gr_cl_name);
               gr_chi2_cls_lo[(c * n_cls * n_files) + n_cls * i + l]->SetLineStyle(l + 2);
               gr_chi2_cls_lo[(c * n_cls * n_files) + n_cls * i + l]->SetLineColor(kRed);
               gr_chi2_cls_lo[(c * n_cls * n_files) + n_cls * i + l]->SetLineWidth(2);

               if (c == 0) sprintf(gr_cl_name, "gr_chi2_cls_up_%s_%d", names[i].c_str(), l);
               else if (c == 1) sprintf(gr_cl_name, "gr_chi2_cls_up_centered_%s_%d", names[i].c_str(), l);
               gr_chi2_cls_up[(c * n_cls * n_files) + n_cls * i + l] = new TGraph(n_x);
               gr_chi2_cls_up[(c * n_cls * n_files) + n_cls * i + l]->SetName(gr_cl_name);
               gr_chi2_cls_up[(c * n_cls * n_files) + n_cls * i + l]->SetLineStyle(l + 2);
               gr_chi2_cls_up[(c * n_cls * n_files) + n_cls * i + l]->SetLineColor(kRed);
               gr_chi2_cls_up[(c * n_cls * n_files) + n_cls * i + l]->SetLineWidth(2);

               if (c == 0) sprintf(gr_cl_name, "gr_pdf_cls_lo_%s_%d", names[i].c_str(), l);
               else if (c == 1) sprintf(gr_cl_name, "gr_pdf_cls_lo_centered_%s_%d", names[i].c_str(), l);
               gr_pdf_cls_lo[(c * n_cls * n_files) + n_cls * i + l] = new TGraph(n_x);
               gr_pdf_cls_lo[(c * n_cls * n_files) + n_cls * i + l]->SetName(gr_cl_name);
               gr_pdf_cls_lo[(c * n_cls * n_files) + n_cls * i + l]->SetLineStyle(l + 2);
               gr_pdf_cls_lo[(c * n_cls * n_files) + n_cls * i + l]->SetLineColor(kGreen + 1);
               gr_pdf_cls_lo[(c * n_cls * n_files) + n_cls * i + l]->SetLineWidth(3);

               if (c == 0) sprintf(gr_cl_name, "gr_pdf_cls_up_%s_%d", names[i].c_str(), l);
               else if (c == 1) sprintf(gr_cl_name, "gr_pdf_cls_up_centered_%s_%d", names[i].c_str(), l);
               gr_pdf_cls_up[(c * n_cls * n_files) + n_cls * i + l] = new TGraph(n_x);
               gr_pdf_cls_up[(c * n_cls * n_files) + n_cls * i + l]->SetName(gr_cl_name);
               gr_pdf_cls_up[(c * n_cls * n_files) + n_cls * i + l]->SetLineStyle(l + 2);
               gr_pdf_cls_up[(c * n_cls * n_files) + n_cls * i + l]->SetLineColor(kGreen + 1);
               gr_pdf_cls_up[(c * n_cls * n_files) + n_cls * i + l]->SetLineWidth(3);

               // Fill
               for (int k = 0; k < n_x; ++k) {
                  if (c == 0) {
                     if (switch_y == 5) {
                        gr_pdf_cls_lo[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, x[k], logy_pdf_cl_lo[i * n_cls * n_x + l * n_x + k]);
                        gr_pdf_cls_up[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, x[k], logy_pdf_cl_up[i * n_cls * n_x + l * n_x + k]);
                        gr_chi2_cls_lo[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, x[k], logy_chi2_cl_lo[i * n_cls * n_x + l * n_x + k]);
                        gr_chi2_cls_up[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, x[k], logy_chi2_cl_up[i * n_cls * n_x + l * n_x + k]);
                     } else {
                        gr_pdf_cls_lo[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, x[k], pow(10., logy_pdf_cl_lo[i * n_cls * n_x + l * n_x + k]));
                        gr_pdf_cls_up[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, x[k], pow(10., logy_pdf_cl_up[i * n_cls * n_x + l * n_x + k]));
                        gr_chi2_cls_lo[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, x[k], pow(10., logy_chi2_cl_lo[i * n_cls * n_x + l * n_x + k]));
                        gr_chi2_cls_up[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, x[k], pow(10., logy_chi2_cl_up[i * n_cls * n_x + l * n_x + k]));
                     }

                  } else if (c == 1) {
                     gr_pdf_cls_lo[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, x[k], logy_pdf_centered_cl_lo[i * n_cls * n_x + l * n_x + k]);
                     gr_pdf_cls_up[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, x[k], logy_pdf_centered_cl_up[i * n_cls * n_x + l * n_x + k]);
                     gr_chi2_cls_lo[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, x[k], logy_chi2_centered_cl_lo[i * n_cls * n_x + l * n_x + k]);
                     gr_chi2_cls_up[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, x[k], logy_chi2_centered_cl_up[i * n_cls * n_x + l * n_x + k]);
                  }
                  if (switch_stat == 2) {
                     double xval = log10(x[k]);
                     if (c == 0) {
                        gr_pdf_cls_lo[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, xval, logy_pdf_cl_lo[i * n_cls * n_x + l * n_x + k]);
                        gr_pdf_cls_up[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, xval, logy_pdf_cl_up[i * n_cls * n_x + l * n_x + k]);
                        gr_chi2_cls_lo[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, xval, logy_chi2_cl_lo[i * n_cls * n_x + l * n_x + k]);
                        gr_chi2_cls_up[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, xval, logy_chi2_cl_up[i * n_cls * n_x + l * n_x + k]);
                     } else if (c == 1) {
                        gr_pdf_cls_lo[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, xval, logy_pdf_centered_cl_lo[i * n_cls * n_x + l * n_x + k]);
                        gr_pdf_cls_up[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, xval, logy_pdf_centered_cl_up[i * n_cls * n_x + l * n_x + k]);
                        gr_chi2_cls_lo[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, xval, logy_chi2_centered_cl_lo[i * n_cls * n_x + l * n_x + k]);
                        gr_chi2_cls_up[(c * n_cls * n_files) + n_cls * i + l]->SetPoint(k, xval, logy_chi2_centered_cl_up[i * n_cls * n_x + l * n_x + k]);
                     }
                  }
               }
            }
         }
         // Kinematic Data
         if (is_data_files) {
            char data_gr_names[100];
            //vector <double> dummy(x_data[i].size(), 0.);
            //gr_data[i] = new TGraphErrors(x_data[i].size(), &x_data[i][0], &y_data[i][0], &dx_data[i][0], &dummy[0]);
            gr_data[i] = new TGraphErrors(x_data[i].size(), &x_data[i][0], &y_data[i][0], &dx_data[i][0], &dy_data[i][0]);
            sprintf(data_gr_names, "Data_%s", names[i].c_str());
            gr_data[i]->SetName(data_gr_names);
            gr_data[i]->SetMarkerStyle(21);

         }

      }

      TCanvas **c_mcmc =  new TCanvas*[2];
      TPaveText *pt = NULL;
      TH2 *th2 = NULL;
      vector<int> gr_color;
      for (int i = 0; i < n_files; ++i) {
         gr_color.push_back(rootcolor(i));
      }

      for (int c = 0; c < 2; ++c) {
         string c_name = y_name_canvas[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y];
         if (c == 0) c_name = "cl_" + c_name;
         else c_name = "cl_centered_" + c_name;
         c_mcmc[c] = new TCanvas(c_name.c_str(), c_name.c_str(), 650 * c, 0, 650, 500);
         c_mcmc[c]->SetBorderSize(0);
         c_mcmc[c]->SetLogx(1);
         if (switch_y == 3 || switch_y == 5)
            c_mcmc[c]->SetLogy(0);
         else
            c_mcmc[c]->SetLogy(1);
         if (c == 1) c_mcmc[c]->SetLogy(0);
         if (switch_stat == 2) {
            c_mcmc[c]->SetLogx(0);
            c_mcmc[c]->SetLogy(0);
         }
         c_mcmc[c]->SetLogz(0);
         c_mcmc[c]->SetTickx(1);
         c_mcmc[c]->SetTicky(1);
         c_mcmc[c]->SetGridx(0);
         c_mcmc[c]->SetGridy(0);
         multi[c] = new TMultiGraph();
         gStyle->SetPalette(1);

         // Legend and Multigraph according to switch_stat
         for (int i = 0; i < n_files; ++i) {

            //--- PDF methods
            if (switch_stat == 0) {
               string add_name = "";
               if (n_files > 1) add_name = names[i] + " ";
               if (c == 0) {
                  gr_pdf_median[i]->SetLineColor(gr_color[i]);
                  gr_pdf_median[i]->SetLineStyle(1);
                  multi[c]->Add(gr_pdf_median[i], "L");
                  leg[0]->AddEntry(gr_pdf_median[i], (add_name + "median").c_str(), "L");
               }
               for (int l = 0; l < n_cls; ++l) {
                  char quantile[100];
                  sprintf(quantile, "%.0f%% CL", x_cl[l] * 100.);
                  string q_name = add_name + quantile + " (from pdf)";
                  int index = (c * n_cls * n_files) + n_cls * i + l;
                  gr_pdf_cls_lo[index]->SetLineColor(gr_color[i]);
                  gr_pdf_cls_up[index]->SetLineColor(gr_color[i]);
                  gr_pdf_cls_lo[index]->SetLineStyle(l + 2);
                  gr_pdf_cls_up[index]->SetLineStyle(l + 2);
                  multi[c]->Add(gr_pdf_cls_lo[index], "L");
                  multi[c]->Add(gr_pdf_cls_up[index], "L");
                  leg[c]->AddEntry(gr_pdf_cls_lo[index], q_name.c_str(), "L");
               }

               //--- chi2 methods
            } else if (switch_stat == 1) {
               string add_name = "";
               if (n_files > 1) add_name = names[i] + " ";
               if (c == 0) {
                  gr_chi2_best[i]->SetLineColor(gr_color[i]);
                  gr_chi2_best[i]->SetLineStyle(1);
                  multi[c]->Add(gr_chi2_best[i], "L");
                  leg[0]->AddEntry(gr_chi2_best[i], (add_name + "#chi^{2}: best-fit").c_str(), "L");
               }
               for (int l = 0; l < n_cls; ++l) {
                  char quantile[100];
                  sprintf(quantile, "%.0f%% CL", x_cl[l] * 100.);
                  string q_name = add_name + quantile + " (from #chi^{2})";
                  int index = (c * n_cls * n_files) + n_cls * i + l;
                  gr_chi2_cls_lo[index]->SetLineColor(gr_color[i]);
                  gr_chi2_cls_up[index]->SetLineColor(gr_color[i]);
                  gr_chi2_cls_lo[index]->SetLineStyle(l + 2);
                  gr_chi2_cls_up[index]->SetLineStyle(l + 2);
                  multi[c]->Add(gr_chi2_cls_lo[index], "L");
                  multi[c]->Add(gr_chi2_cls_up[index], "L");
                  leg[c]->AddEntry(gr_chi2_cls_lo[index], q_name.c_str(), "L");
               }

               //--- <y>, sigma_y method
            } else if (switch_stat == 3) {
               string add_name = "";
               if (n_files > 1) add_name = names[i] + " ";
               if (c == 0) {
                  gr_pdf_median[i]->SetLineColor(gr_color[i]);
                  gr_pdf_median[i]->SetLineStyle(1);
                  multi[c]->Add(gr_pdf_mean[i], "L");
                  leg[0]->AddEntry(gr_pdf_mean[i], (add_name + "mean").c_str(), "L");
               }
               for (int l = 0; l < n_cls; ++l) {
                  char quantile[100];
                  sprintf(quantile, "sigma");
                  string q_name = add_name + quantile + " (dispersion)";
                  int index = (c * n_cls * n_files) + n_cls * i + l;
                  gr_pdf_cls_lo[index]->SetLineColor(gr_color[i]);
                  gr_pdf_cls_up[index]->SetLineColor(gr_color[i]);
                  gr_pdf_cls_lo[index]->SetLineStyle(l + 2);
                  gr_pdf_cls_up[index]->SetLineStyle(l + 2);
                  multi[c]->Add(gr_pdf_cls_lo[index], "L");
                  multi[c]->Add(gr_pdf_cls_up[index], "L");
                  leg[c]->AddEntry(gr_pdf_cls_lo[index], q_name.c_str(), "L");
               }


               //--- chi2 + pdf method
            } else if (switch_stat == 2) {
               string add_name = "";
               // add h2
               leg[c]->AddEntry(h2d_pdf, "Projected PDF", "P");
               // add mean, median... (only if non-centered)
               if (c == 0) {
                  multi[c]->Add(gr_pdf_mean[i], "L");
                  multi[c]->Add(gr_pdf_mostlikely[i], "L");
                  multi[c]->Add(gr_pdf_median[i], "L");
                  leg[0]->AddEntry(gr_pdf_mean[0], "PDF: mean", "L");
                  leg[0]->AddEntry(gr_pdf_mostlikely[0], "PDF: most probable", "L");
                  leg[0]->AddEntry(gr_pdf_median[0], "PDF: median", "L");
               }
               // add PDF CLs
               for (int l = 0; l < n_cls; ++l) {
                  char quantile[100];
                  sprintf(quantile, "PDF: %.0f%% CL", x_cl[l] * 100.);
                  string q_name = add_name + quantile;
                  int index = (c * n_cls * n_files) + n_cls * i + l;
                  multi[c]->Add(gr_pdf_cls_lo[index], "L");
                  multi[c]->Add(gr_pdf_cls_up[index], "L");
                  leg[c]->AddEntry(gr_pdf_cls_lo[index], q_name.c_str(), "L");
               }
               // add best-chi2 (only if non-centered)
               if (c == 0) {
                  multi[c]->Add(gr_chi2_best[i], "L");
                  leg[0]->AddEntry(gr_chi2_best[0], "#chi^{2}: best-fit", "L");
               }
               // add chi2 CLs
               for (int l = 0; l < n_cls; ++l) {
                  char quantile[100];
                  sprintf(quantile, "#chi^{2}: %.0f%% CL", x_cl[l] * 100.);
                  string q_name = add_name + quantile;
                  int index = (c * n_cls * n_files) + n_cls * i + l;
                  multi[c]->Add(gr_chi2_cls_lo[index], "L");
                  multi[c]->Add(gr_chi2_cls_up[index], "L");
                  leg[c]->AddEntry(gr_chi2_cls_lo[index], q_name.c_str(), "L");
               }
            }
            // If Kinematic data files
            if (is_data_files) {
               if (c == 0) {
                  gr_data[i]->SetLineColor(gr_color[i]);
                  gr_data[i]->SetMarkerColor(gr_color[i]);
                  multi[c]->Add(gr_data[i], "p");
               }
            }

         }

         if (switch_stat == 2) {
            if (c == 0)
               th2 = h2d_pdf;
            else if (c == 1)
               th2 = h2d_pdf_centered;
            th2->Draw("box");
            th2->SetStats(0);
            th2->SetTitle("");
            th2->GetXaxis()->SetNdivisions(510);
            th2->GetXaxis()->SetLabelOffset(0.005);
            th2->GetXaxis()->SetLabelSize(0.045);
            th2->GetXaxis()->SetLabelFont(132);
            th2->GetXaxis()->SetTitle(x_axis[switch_y].c_str());
            th2->GetXaxis()->SetTitleSize(0.045);
            th2->GetXaxis()->SetTickLength(0.05);
            th2->GetXaxis()->SetTitleOffset(1.0);
            th2->GetXaxis()->SetTitleFont(132);
            th2->GetYaxis()->SetNdivisions(510);
            th2->GetYaxis()->SetLabelOffset(0.005);
            th2->GetYaxis()->SetLabelSize(0.045);
            th2->GetYaxis()->SetLabelFont(132);
            th2->GetYaxis()->SetTitle(y_axis[gSIM_IS_ASTRO_OR_PP_UNITS][gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y].c_str());
            th2->GetYaxis()->SetTitleSize(0.05);
            th2->GetYaxis()->SetTickLength(0.05);
            th2->GetYaxis()->SetTitleOffset(.8);
            th2->GetYaxis()->SetTitleFont(132);
            multi[c]->Draw("L");
            gSIM_CLUMPYAD->Draw();
         } else {
            multi[c]->Draw("A");
            gSIM_CLUMPYAD->Draw();
            multi[c]->GetXaxis()->SetLimits(x[0], x[n_x - 1]);
            multi[c]->GetXaxis()->SetTitle((x_name[switch_y] + "  " + x_unit[switch_y]).c_str());
            if (c == 0)
               multi[c]->GetYaxis()->SetTitle((y_name[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y] + "  " + y_unit[gSIM_IS_ASTRO_OR_PP_UNITS][gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y]).c_str());
            else if (c == 1)
               multi[c]->GetYaxis()->SetTitle(y_axis_centered[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y].c_str());
         }

         leg[c]->Draw("SAME");
         gPad->SetTickx(1);
         gPad->SetTicky(1);

         char title[200];
         sprintf(title, "#alpha_{int} =%.3f^{o}", gSIM_ALPHAINT * RAD_to_DEG);
         pt = new TPaveText(0.3, 0.8, 0.5, 0.9, "blNDC");
         pt->SetBorderSize(0);
         pt->SetFillColor(kWhite);
         pt->SetLineColor(1);
         pt->SetLineWidth(1);
         pt->SetTextFont(132);
         pt->SetTextSize(0.05);
         pt->AddText(title);
         if (switch_y == 2)   pt->Draw();

         c_mcmc[c]->Update();
         c_mcmc[c]->Modified();
      }

      if (gSIM_IS_DISPLAY)
         gSIM_ROOTAPP->Run(kTRUE);
      th2 = NULL;

      // Free memory
      delete[] multi;
      multi = NULL;
      delete[] leg;
      leg = NULL;
      for (Int_t c = 0; c < 2; ++c) {
         delete c_mcmc[c];
         c_mcmc[c] = NULL;
      }
      delete[] c_mcmc;
      c_mcmc = NULL;
      delete pt;
      pt = NULL;

      if (gr_data) delete[] gr_data;
      gr_data = NULL;
      if (gr_chi2_best) delete[] gr_chi2_best;
      gr_chi2_best = NULL;
      if (gr_chi2_cls_lo) delete[] gr_chi2_cls_lo;
      gr_chi2_cls_lo = NULL;
      if (gr_chi2_cls_up) delete[] gr_chi2_cls_up;
      gr_chi2_cls_up = NULL;
      if (gr_pdf_mean) delete[] gr_pdf_mean;
      gr_pdf_mean = NULL;
      if (gr_pdf_median) delete[] gr_pdf_median;
      gr_pdf_median = NULL;
      if (gr_pdf_mostlikely) delete[] gr_pdf_mostlikely;
      gr_pdf_mostlikely = NULL;
      if (gr_pdf_cls_lo) delete[] gr_pdf_cls_lo;
      gr_pdf_cls_lo = NULL;
      if (gr_pdf_cls_up) delete[] gr_pdf_cls_up;
      gr_pdf_cls_up = NULL;
   }

   delete h2d_pdf;
   h2d_pdf = NULL;
   delete h2d_pdf_centered;
   h2d_pdf_centered = NULL;
#else
   printf("\n====> ERROR: stat_CLs() in janalysis.cc");
   printf("\n             You need ROOT to be installed to run this function.");
   printf("\n             => abort()\n\n");
   abort();
#endif
   // Reset value define in params.h
   gSIM_ALPHAINT = old_alpha_aperture;
}

//______________________________________________________________________________
void stat_draw_chi2(string const &stat_files, bool is_logl_or_chi2)
{
   //--- Draws chi2/dof and chi2 (or log-likelihood) distribution obtained from
   //    the stat. analysis files.
   //    N.B.: a stat. analysis file must be formatted as described in stat_load_list().
   //
   //  stat_files       File to read
   //  is_logl_or_chi2  File contains log-likelihood or chi2

   cout << ">>>>> Draw chi2/dof and chi2 distribution obtained from a list of stat. analysis files" << endl;
   cout << "   [N.B.: a stat. analysis file must be formatted as described in stat_load_list()]" << endl;
   cout << endl;

   // Check if one or multiples files
   vector<string> files;
   stat_file2files(stat_files, files);

   // Load and store files
   const int n = files.size();
   vector<struct gStructHalo> stat_halos;
   vector<int> n_stat(n, 0);
   vector<int> dof(n, 0);
   vector<int> i_best(n, 0);

   double chi2min[2] = {1.e40, 1.e40};
   double chi2max[2] = { -1.e40, -1.e40};
   for (int i = 0; i < n; ++i) {
      struct gStructHalo stat_halo_tmp;
      stat_load_list(files[i], stat_halo_tmp, false);
      stat_halos.push_back(stat_halo_tmp);

      n_stat[i] = stat_halos[i].StatChi2.size();
      dof[i] = stat_halos[i].Ndata - stat_halos[i].Npar;
      for (int j = 0; j < n_stat[i]; ++j) {
         if (stat_halos[i].StatChi2[j] < 0. && !is_logl_or_chi2) {
            printf("\n====> ERROR: stat_draw_chi2() in janalysis.cc");
            printf("\n             You indicated chi2 for %s, but contains negative values (log-likelihood?)", files[i].c_str());
            printf("\n             => abort()\n\n");
            abort();
         } else if (stat_halos[i].StatChi2[j] > 0. && is_logl_or_chi2) {
            printf("\n====> ERROR: stat_draw_chi2() in janalysis.cc");
            printf("\n             You indicated log-likelihood for %s, but contains positive values (chi2?)", files[i].c_str());
            printf("\n             => abort()\n\n");
            abort();
         }

         double chi2 =  fabs(stat_halos[i].StatChi2[j]);
         if (chi2 < chi2min[0])  {
            chi2min[0] = chi2;
            i_best[i] = j;
         }
         if (chi2 > chi2max[0])  chi2max[0] = chi2;
         if (chi2 / (double)dof[i] < chi2min[1])  chi2min[1] = chi2 / (double)dof[i];
         if (chi2 / (double)dof[i] > chi2max[1])  chi2max[1] = chi2 / (double)dof[i];
      }

      // If log-likelihood, multiply by "-1" and invert min and max
      if (is_logl_or_chi2) {
         double tmp = chi2min[0];
         chi2min[0] = -chi2max[0];
         chi2max[0] = -tmp;
      }

      if (is_logl_or_chi2)
         printf("    => best-fit model found for log-likelihood = %le\n", chi2max[0]);
      else
         printf("    => best-fit model found for chi2 = %le\n", chi2min[0]);
   }

#if IS_ROOT
   // Define histogram to be plotted
   TH1 **h_chi2 = new TH1*[2 * n];
   TLegend **leg = new TLegend*[2];
   TCanvas **c_chi2 = new TCanvas*[2];
   TF1 *f_chi2 = NULL;

   for (int l = 0; l < 2; ++l) {
      if (l == 1 && is_logl_or_chi2)
         continue;

      leg[l] = new TLegend(0.68, 0.58, 0.91, 0.92);
      leg[l]->SetFillColor(kWhite);
      leg[l]->SetTextSize(0.028);
      leg[l]->SetBorderSize(0);
      leg[l]->SetFillStyle(0);

      // Allocate and fill histos
      double max = 1.e-40;
      for (int i = 0; i < n; ++i) {
         string name = stat_halos[i].Name;
         if (l == 1 && i == 0) name = name + "_dof";
         h_chi2[i + l * n] = new TH1D(name.c_str(), name.c_str(), 200, chi2min[l], chi2max[l]);
         for (int j = 0; j < n_stat[i]; ++j) {
            double chi2 =  stat_halos[i].StatChi2[j];
            if (l == 1) chi2 /= (double)dof[i];
            h_chi2[i + l * n]->Fill(chi2);
         }
         leg[l]->AddEntry(h_chi2[i + l * n], name.c_str(), "PFL");

         // Histo for chi2
         h_chi2[i + l * n]->SetFillColor(rootcolor(i + 1));
         h_chi2[i + l * n]->SetLineColor(rootcolor(i + 1));
         h_chi2[i + l * n]->SetFillStyle(3001 + i);
         h_chi2[i + l * n]->SetDirectory(0);
         h_chi2[i + l * n]->SetStats(0);
         double norm = h_chi2[i + l * n]->Integral();
         h_chi2[i + l * n]->Scale(1. / norm);
         max = TMath::Max(max, h_chi2[i + l * n]->GetMaximum());
         h_chi2[0]->SetMaximum(max);
         if (is_logl_or_chi2) {
            h_chi2[i + l * n]->GetXaxis()->SetTitle("log-likelihood");
            h_chi2[i + l * n]->GetYaxis()->SetTitle("PDF(log-likelihood)");
         } else {
            h_chi2[i + l * n]->GetXaxis()->SetTitle("#chi^{2}");
            h_chi2[i + l * n]->GetYaxis()->SetTitle("PDF(#chi^{2})");
         }
         if (l == 1) {
            h_chi2[i + l * n]->GetXaxis()->SetTitle("#chi^{2}/d.o.f.");
            h_chi2[i + l * n]->GetYaxis()->SetTitle("PDF(#chi^{2}/d.o.f.)");
         }
      }

      //--- draw chi2
      if (l == 0) {
         if (is_logl_or_chi2)
            c_chi2[l] = new TCanvas("loglikelihood", "loglikelihood_pdf");
         else
            c_chi2[l] = new TCanvas("chi2", "chi2_pdf");
      } else c_chi2[l] = new TCanvas("chi2dof", "chi2dof pdf");
      gStyle->SetOptStat(0);
      for (int i = 0; i < n; ++i) {
         if (i == 0) h_chi2[i + l * n]->Draw();
         else h_chi2[i + n * l]->Draw("SAME");
         gSIM_CLUMPYAD->Draw();

      }
      leg[l]->Draw("SAME");
      gPad->SetTickx(1);
      gPad->SetTicky(1);
      c_chi2[l]->Update();
      c_chi2[l]->Modified();
   }

   if (gSIM_IS_DISPLAY)
      gSIM_ROOTAPP->Run(kTRUE);

   // Free memory
   delete[] leg;
   if (f_chi2) delete f_chi2;
   delete[] c_chi2;
   delete[] h_chi2;
#else
   printf("\n====> ERROR: stat_draw_chi2() in janalysis.cc");
   printf("\n             You need ROOT to be installed to display the results.");
   printf("\n             => abort()\n\n");
   abort();
#endif
}

//______________________________________________________________________________
void stat_draw_correlations(vector<int> const &ids, string const &stat_file,
                            double const &rkpc_for_mr, double const &rho_sat)
{
   //--- Draws PDF and correlation between the parameters of a stat. analysis file.
   //    N.B.: a stat. analysis file must be formatted as described in stat_load_list().
   //
   //  ids            IDs of parameters to plot
   //  stat_file      File to read
   //  rkpc_for_mr    If 1: plot M(rkpc_for_mr) instead of first ID parameter (if keyword "profile" is in stat_file)
   //  rho_sat        Saturation density [Msol/kpc3]

   cout << ">>>>> Draw PDF and correlations between the chosen parameters of a stat analysis file" << endl;
   cout << "   [N.B.: a stat. analysis file must be formatted as described in stat_load_list()]" << endl;
   cout << endl;
   cout << "  => Read and load " << stat_file << endl;
   cout << endl;

   char char_tmp[512];

   vector<string> files;
   stat_file2files(stat_file, files);
   if ((int)files.size() > 1) {
      print_warning("janalysis.cc", "stat_draw_correlations()",
                    stat_file + " contains several files, whereas only one allowed here.");
      return;
   }

   const int n_id_stat_params = 17;

   const char id_stat_names [n_id_stat_params][100] = {"Rhos", "Rscale", "Rvir", "ShapeParam1", "ShapeParam2", "ShapeParam3", "L", "RLight", "LightShapeParam1", "LightShapeParam2",
                                                       "LightShapeParam3", "Beta_Aniso_0", "Beta_Aniso_Inf", "RAniso", "AnisoShapeParam", "RA_cnt",
                                                       "Dec_cnt"
                                                      };
   string params[n_id_stat_params] = {"log10(#rho_{s}/M_{#odot}kpc^{-3})", "log10(r_{s}/kpc)", "log10(R_{vir}/kpc)", "#alpha", "#beta", "#gamma", "log10(L/L_{#odot})", "log10(r_{L}/kpc)", "#alpha*",
                                      "#beta*", "#gamma*", "#beta_{0}", "#beta_{#infty}", "log10(r_{a}/kpc)", "#eta", "RA_{cnt} [deg]", "Dec_{cnt} [deg]"
                                     };

   gDM_RHOSAT = rho_sat;

   // Load and store file
   struct gStructHalo stat_halo;
   stat_load_list(stat_file, stat_halo, false);
   string name = stat_halo.Name;


   bool is_issue_id = false;
   if (ids.size() == 0) {
      cout << "   *** No IDs given!" << endl;
      is_issue_id = true;
   } else if (int(ids.size()) > n_id_stat_params) {
      cout << "   *** Too many IDs given!" << endl;
      is_issue_id = true;
   } else {
      for (int i = 0; i < int(ids.size()); i++) {
         if (ids[i] < 0 || ids[i] > n_id_stat_params) {
            cout << "Wrong IDs given!" << endl;
            is_issue_id = true;
            break;
         }
      }
   }
   if (is_issue_id) {
      cout << "    => The list of free parameters in " << stat_file << " with their corresponding IDs are: " << endl;
      for (int n = 0; n < int(sizeof(stat_halo.StatIsFreeParam) / sizeof(stat_halo.StatIsFreeParam[0])); n++) {
         if (stat_halo.StatIsFreeParam[n])
            cout << "      " << id_stat_names[n] << ": ID is " << n + 1 << endl;
      }
      cout << endl;
      return;
   }


   if (stat_halo.StatHaloProfile.size() != 0 && stat_halo.StatHaloProfile[0] == 1) { // Einasto profile
      params[0] = "log10(#rho_{-2}/M_{#odot}kpc^{-3})";
      params[1] = "log10(r_{-2}/kpc)";
   }
   if (stat_halo.StatLightProfile.size() != 0) {
      if (stat_halo.StatLightProfile[0] == 0 || stat_halo.StatLightProfile[0] == 1) // Exponential
         params[7] = "log10(r_{c}/kpc)";
      else if (stat_halo.StatLightProfile[0] == 2) { // King profile
         params[7] = "log10(r_{c}/kpc)";
         params[8] = "log10(r_{lim}/kpc)";
      } else if (stat_halo.StatLightProfile[0] == 4) { // Sersic profile
         params[7] = "log10(r_{c}/kpc)";
         params[8] = "n";
      } else if (stat_halo.StatLightProfile[0] == 5) { // Zhao profile
         params[7] = "log10(r_{s}*/kpc)";
      }
   }

   for (int k = 0; k < int(ids.size()); k++) {
      if (stat_halo.StatParam[ids[k] - 1].size() == 0) {
         printf("\n====> ERROR: stat_draw_correlations() in janalysis.cc");
         printf("\n             No data in file %s for parameter %s with ID=%d",
                stat_file.c_str(), id_stat_names[ids[k] - 1], ids[k]);
         printf("\n             => abort()\n\n");
         abort();
      }
      if (stat_halo.StatIsFreeParam[ids[k] - 1] == 0) {
         sprintf(char_tmp, "In file %s, parameter %s with ID=%d is fixed",
                 stat_file.c_str(), id_stat_names[ids[k] - 1], ids[k]);
         print_warning("janalysis.cc", "stat_draw_correlations()", string(char_tmp));
      }
   }


   int n_stat = stat_halo.StatParam[ids[0] - 1].size(); // number of points in stat file
   const int n_par = ids.size(); // number of parameters

   bool is_mass = false;
   if (rkpc_for_mr > 1.e-50) {
      char rkpc[50];
      sprintf(rkpc, "%.1le", rkpc_for_mr);
      string mr = rkpc;
      mr = "log10(M_{" + mr + " kpc}/M_{#odot})";
      params[ids[0] - 1] = mr;
      is_mass = true;
   }

   if (is_mass && (stat_halo.StatHaloProfile.size() == 0)) {
      printf("\n====> ERROR: stat_draw_correlations() in janalysis.cc");
      printf("\n             No halo profile parameters found in file %s", stat_file.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }

   vector<vector<double> > val(n_par, vector<double>(n_stat, 1.e-40));
   vector<double> valmin(n_par, 1.e40);
   vector<double> valmax(n_par, -1.e40);
   for (int i = 0; i < n_stat; ++i) {
      for (int k = 0; k < n_par; k++) {
         if (ids[k] == 1 || ids[k] == 2 || ids[k] == 3 || ids[k] == 7 || ids[k] == 8 || ids[k] == 14) {
            val[k][i] = log10(stat_halo.StatParam[ids[k] - 1][i]);
         } else {
            val[k][i] = stat_halo.StatParam[ids[k] - 1][i];
         }
         if (stat_halo.StatLightProfile.size() != 0 && stat_halo.StatLightProfile[0] == 2) { // King
            if (ids[k] == 9)
               val[k][i] = log10(stat_halo.StatParam[ids[k] - 1][i]);
         }
      }

      // mr instead of first parameter
      if (is_mass) {
         const int npar = 7;
         double par[npar] = {stat_halo.StatParam[0][i], stat_halo.StatParam[1][i],
                             stat_halo.StatParam[3][i], stat_halo.StatParam[4][i], stat_halo.StatParam[5][i],
                             (double)stat_halo.StatHaloProfile[i], rkpc_for_mr
                            };
         val[0][i] = log10(mass_singlehalo(par, 1.e-3));
      }
   }

#if IS_ROOT
   // Draw canvas and histos.
   TCanvas *c_correl = new TCanvas("c_correl", "c_correl", 15, 46, 800, 800);
   c_correl->SetHighLightColor(0);
   c_correl->Range(0, 0, 1, 1);
   c_correl->SetFillColor(kWhite);
   c_correl->SetFrameFillColor(kWhite);
   c_correl->SetBorderMode(0);
   c_correl->SetBorderSize(0);
   c_correl->SetLeftMargin(0.2);
   c_correl->SetRightMargin(0.01);
   c_correl->SetTopMargin(0.01);
   c_correl->SetBottomMargin(0.2);
   c_correl->SetFrameBorderMode(0);
   c_correl->Divide(n_par, n_par);

   const int nbins = 50;

   // Quantities on-diagonal
   TH1D **h1D = new TH1D*[n_par];
   int n_val = val[0].size();
   for (int i = 0; i < n_par; ++i) {

      char name_h1[50];
      sprintf(name_h1, "correl%d%d", i, i);
      h1D[i] = new TH1D(name_h1, name_h1, nbins, valmin[i], valmax[i]);
      for (int k = 0;  k < n_val; ++k) {
         h1D[i]->Fill(val[i][k]);
      }

      c_correl->cd(1 + i * (n_par + 1));
      gPad->SetFillColor(kWhite);
      gPad->SetBorderMode(0);
      gPad->SetBorderSize(0);
      gPad->SetLeftMargin(0.25);
      gPad->SetRightMargin(0.01);
      gPad->SetTopMargin(0.01);
      gPad->SetBottomMargin(0.2);
      gPad->SetFrameBorderMode(0);
      gPad->SetFrameFillColor(kWhite);
      gPad->SetTickx(1);
      gPad->SetTicky(1);

      h1D[i]->SetStats(0);
      h1D[i]->SetTitle("");
      h1D[i]->GetXaxis()->SetNdivisions(504);
      h1D[i]->GetXaxis()->SetLabelOffset(0.02);
      h1D[i]->GetXaxis()->SetLabelSize(0.072);
      h1D[i]->GetXaxis()->SetTitleSize(0.09);
      //h1D[i]->GetXaxis()->SetTitle(params[i].c_str());
      h1D[i]->GetXaxis()->SetTickLength(0.08);
      h1D[i]->GetXaxis()->SetTitleOffset(1.);
      h1D[i]->GetXaxis()->SetTitleFont(132);
      h1D[i]->GetYaxis()->SetNdivisions(504);
      h1D[i]->GetYaxis()->SetLabelOffset(0.021);
      h1D[i]->GetYaxis()->SetLabelSize(0.072);
      h1D[i]->GetYaxis()->SetTitleSize(0.09);
      h1D[i]->GetYaxis()->SetTitle("PDF");
      h1D[i]->GetYaxis()->SetTickLength(0.08);
      h1D[i]->GetYaxis()->SetTitleOffset(1.28);
      h1D[i]->GetYaxis()->SetTitleFont(132);
      double norm = h1D[i]->Integral();

      h1D[i]->Scale(1. / norm);
      h1D[i]->Draw("");
      gSIM_CLUMPYAD->Draw();
      c_correl->Update();

      TPaveText *txt = new TPaveText(0.2, 0.82, 0.8, 0.92, "NDC");
      txt->AddText(params[ids[i] - 1].c_str());
      txt->Draw();
      txt->SetTextFont(132);
      txt->SetTextSize(0.08);
      txt->SetTextColor(kBlue + 1);
      txt->SetFillColor(kWhite);
      txt->SetBorderSize(0);
      txt->SetFillStyle(0);
      txt->SetTextAlign(22);

   }

   // Median
   if (is_mass) {
      double q_h1d = 0.5;
      double x_h1d[1] = {0.};
      h1D[0]->GetQuantiles(1, x_h1d, &q_h1d);
      cout << "M" << rkpc_for_mr << " (median) = " << pow(10., x_h1d[0]) << endl;

      const int n_q = 2;
      double x_cl[n_q] = {0.68, 0.95};
      for (int k = 0; k < n_q; ++k) {
         cout << "   " << x_cl[k] << "% CL: ";
         double xk[2] = {0., 0.};
         double qk[2] = {(1. - x_cl[k]) / 2., x_cl[k] + (1. - x_cl[k]) / 2.};
         //cout << x_cl[k] << " " << qk[0] << "  " << qk[1] << endl;
         h1D[0]->GetQuantiles(2, xk, qk);
         cout << "min=" << pow(10., xk[0]) << "  max=" << pow(10., xk[1]) << endl;
      }
   }

   // Quantities off-diagonal (n_par*(n_par-1)/2 of them)
   // Create a palette with the log likelihood values


   int ncolor = 50; // Number of palette colours
   vector<double> bin(ncolor + 1, 0.); // Values of (log) likelihood. Each bin is associated to a colour.
   double min_likelihood = find_min_value(stat_halo.StatChi2); // minimum (log) likelihood
   double max_likelihood = find_max_value(stat_halo.StatChi2); // maximum (log) likelihood
   double size_step = (max_likelihood - min_likelihood) / (double)ncolor; // size of step in likelihood
   for (int i = 0; i < ncolor + 1 ; i++) {
      bin[i] = min_likelihood + size_step * i; // Fill values of likelihoods for each colour
   }

   vector<int> color(ncolor, 0); // Values of colours associated to each bin of likelihood
   //Create palette

   gStyle->SetPalette(55);
   // Create canvas for the palette
   TCanvas *canv = new TCanvas("canv", "canv", 0, 0, 10, 10);
   // Create temporary histogram for creating the palette
   TH2F *h2 = new TH2F("h2", "h2", 1000, 0., 1., 1000, 0., 1.);
   h2->SetMinimum(min_likelihood);
   h2->SetMaximum(max_likelihood);
   h2->SetBinContent(1, 1);
   h2->Draw("colz");
   gSIM_CLUMPYAD->Draw();
   gPad->Update();
   // Get the palette from the histogram
   TPaletteAxis *palette = (TPaletteAxis *)h2->GetListOfFunctions()->FindObject("palette");
   // Copy palette (for re-draw later)
   TPaletteAxis *palette_new = new TPaletteAxis(0.09104031, 0.1844339, 0.1872661, 0.9864403, h2);
   // Delete canvas
   delete canv;

   for (int i = 0; i < ncolor; i++) { // Associate a colour to each bin of likelihood
      color[i] = palette->GetValueColor(bin[i]);
   }

   // Sort values of likelihood
   vector<pair<double, int> > pair_J_index;
   for (int k = 0; k < (int)stat_halo.StatChi2.size(); k++) {
      double z = stat_halo.StatChi2[k];
      //cout << "z = " << z << endl;
      pair <double, int> temp;
      temp = make_pair(z, k);
      pair_J_index.push_back(temp);
   }
   sort(pair_J_index.begin(), pair_J_index.end());

   //TH2D **h2D = NULL;
   TMultiGraph **mg = NULL;
   if (n_par > 1) {
      //h2D = new TH2D*[n_par * (n_par - 1) / 2];
      mg = new TMultiGraph*[n_par * (n_par - 1) / 2];

      int k = 0;
      for (int i = 0; i < n_par; ++i) {
         for (int j = 0; j < i; ++j) {
            // Allocate and fill histo
            char name_h2[50];
            sprintf(name_h2, "correl%d%d", i, j);
            //h2D[k] = new TH2D(name_h2, name_h2, nbins, valmin[j], valmax[j], nbins, valmin[i], valmax[i]);
            //for (int l = 0; l < n_val; ++l)
            //h2D[k]->Fill(val[j][l], val[i][l]);

            int ij_pos = i * n_par + j;
            c_correl->cd(ij_pos + 1);
            gPad->SetFillColor(kWhite);
            gPad->SetBorderMode(0);
            gPad->SetBorderSize(0);
            gPad->SetLeftMargin(0.25);
            gPad->SetRightMargin(0.01);
            gPad->SetTopMargin(0.01);
            gPad->SetBottomMargin(0.2);
            gPad->SetFrameFillColor(kWhite);
            gPad->SetFrameBorderMode(0);
            gPad->SetTickx(1);
            gPad->SetTicky(1);

            /*
            h2D[k]->SetStats(0);
            h2D[k]->SetTitle("");
            h2D[k]->GetXaxis()->SetNdivisions(504);
            h2D[k]->GetXaxis()->SetLabelOffset(0.02);
            h2D[k]->GetXaxis()->SetLabelSize(0.072);
            h2D[k]->GetXaxis()->SetTitleSize(0.09);
            h2D[k]->GetXaxis()->SetTickLength(0.08);
            h2D[k]->GetXaxis()->SetTitleOffset(1.);
            h2D[k]->GetXaxis()->SetTitle(params[ids[j] - 1].c_str());
            h2D[k]->GetXaxis()->SetTitleFont(132);
            h2D[k]->GetYaxis()->SetNdivisions(504);
            h2D[k]->GetYaxis()->SetLabelOffset(0.021);
            h2D[k]->GetYaxis()->SetLabelSize(0.072);
            h2D[k]->GetYaxis()->SetTitleSize(0.09);
            h2D[k]->GetYaxis()->SetTitle(params[ids[i] - 1].c_str());
            h2D[k]->GetYaxis()->SetTickLength(0.08);
            h2D[k]->GetYaxis()->SetTitleOffset(1.2);
            h2D[k]->GetYaxis()->SetTitleFont(132);
            gStyle->SetPalette(55);
            h2D[k]->SetContour(30);
            //h2D[k]->Draw("");

            h2D[k]->SetMarkerColor(kBlue+2);
            h2D[k]->SetMarkerStyle(20);
            h2D[k]->SetMarkerSize(0.4);
            */
            //h2D[k]->Draw("scat");
            //gSIM_CLUMPYAD->Draw();
            // Create array of TGraph. Each point of the stat file will be filled in a TGraph, with a colour corresponding to its likelihood
            /*
               TGraph **h2_final = new TGraph*[(int)stat_halo.StatChi2.size()];
               for (int zz=0; zz<(int)stat_halo.StatChi2.size(); ++zz) {
                  h2_final[zz] = new TGraph();
                  h2_final[zz]->SetMarkerStyle(20);
                  h2_final[zz]->SetMarkerSize(0.4);
                  h2_final[zz]->SetPoint(h2_final[zz]->GetN(), val[j][pair_J_index[zz].second], val[i][pair_J_index[zz].second]);
                  // Find the colour associated to the likelihood
                  int index_color = floor((stat_halo.StatChi2[pair_J_index[zz].second] - min_likelihood)/size_step);
                  cout << index_color << endl;
                  h2_final[zz]->SetMarkerColor(Color[index_color]);
               }
            */
            TGraph **h2_final = new TGraph*[ncolor];
            for (int n = 0; n < ncolor; n++) {
               h2_final[n] = new TGraph();
               h2_final[n]->SetMarkerStyle(20);
               h2_final[n]->SetMarkerSize(0.4);
               h2_final[n]->SetMarkerColor(color[n]);
            }
            for (int zz = 0; zz < (int)stat_halo.StatChi2.size(); ++zz) {
               int index_color = floor((stat_halo.StatChi2[pair_J_index[zz].second] - min_likelihood) / size_step);
               //cout << "index_color = " << index_color << endl;
               if (index_color == ncolor)
                  index_color -= 1;
               h2_final[index_color]->SetPoint(h2_final[index_color]->GetN(), val[j][pair_J_index[zz].second], val[i][pair_J_index[zz].second]);
               // Find the colour associated to the likelihood
            }

            // Create a Multigraph where all the TGraphs will be added.
            mg[k] = new TMultiGraph(name_h2, name_h2);
            //for (int zz=0; zz<(int)stat_halo.StatChi2.size(); ++zz) {
            for (int zz = 0; zz < ncolor; ++zz) {
               if (h2_final[zz]->GetN() > 0)
                  mg[k]->Add(h2_final[zz], "p"); // Add TGraphs
            }

            mg[k]->Draw("A"); // Draw multigraph
            gSIM_CLUMPYAD->Draw();
            mg[k]->SetTitle("");
            mg[k]->GetXaxis()->SetNdivisions(504);
            mg[k]->GetXaxis()->SetLabelOffset(0.02);
            mg[k]->GetXaxis()->SetLabelSize(0.072);
            mg[k]->GetXaxis()->SetTitleSize(0.09);
            mg[k]->GetXaxis()->SetTickLength(0.08);
            mg[k]->GetXaxis()->SetTitleOffset(0.95);
            mg[k]->GetXaxis()->SetTitle(params[ids[j] - 1].c_str());
            mg[k]->GetXaxis()->SetTitleFont(132);
            mg[k]->GetYaxis()->SetNdivisions(504);
            mg[k]->GetYaxis()->SetLabelOffset(0.021);
            mg[k]->GetYaxis()->SetLabelSize(0.072);
            mg[k]->GetYaxis()->SetTitleSize(0.09);
            mg[k]->GetYaxis()->SetTitle(params[ids[i] - 1].c_str());
            mg[k]->GetYaxis()->SetTickLength(0.08);
            mg[k]->GetYaxis()->SetTitleOffset(1.2);
            mg[k]->GetYaxis()->SetTitleFont(132);
            ++k;
            delete[] h2_final;
            h2_final = NULL;
         }
      }

      // Draw Palette
      c_correl->cd(2);
      TPaveText *pt = new TPaveText(0.3636801, 0.5316944, 0.961082, 0.6309117, "brNDC");
      pt->SetBorderSize(0);
      pt->SetFillColor(0);
      pt->SetFillStyle(0);
      pt->SetTextColor(kBlue + 1);
      pt->SetTextFont(132);
      pt->SetTextSize(0.1);
      pt->AddText("Log-Likelihood");
      pt->Draw();

      palette_new->Copy(*palette);
      palette_new->SetLabelSize(0.05);
      palette_new->Draw();
   }
   c_correl->Update();
   c_correl->Modified();

   if (gSIM_IS_DISPLAY)
      gSIM_ROOTAPP->Run(kTRUE);

   // Free memory
   delete[] h1D;
   h1D = NULL;
   // delete[] h2D;
   // h2D = NULL;
   delete[] mg;
   mg = NULL;
   delete c_correl;
   c_correl = NULL;
#else
   printf("\n====> ERROR: stat_draw_correlations() in janalysis.cc");
   printf("\n             You need ROOT to be installed to display the results.");
   printf("\n             => abort()\n\n");
   abort();
#endif
}

//______________________________________________________________________________
void stat_file2files(string const &stat_file, vector<string> &stat_files)
{
   //--- Checks if stat_file is a stat. analysis file, or a file containing a
   //    list of such files. Either way, a vector of names of files is returned.
   //
   //  stat_files     File containing a single or a list of files

   string stat_file_plain = stat_file;
   resolve_envvar(stat_file_plain);

   // Open file and check if exists
   ifstream f_mcmc(stat_file_plain.c_str());
   if (!f_mcmc) {
      printf("\n====> ERROR: stat_file2files() in janalysis.cc");
      printf("\n             Cannot open (and read) file %s", stat_file_plain.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }

   string line;
   stat_files.clear();
   while (getline(f_mcmc, line)) {
      line.erase(remove_if(line.begin(), line.end(), isNotASCII), line.end());
      size_t first = line.find_first_not_of(" ");
      size_t last = line.find_first_of(" ", first + 1);
      string word = line.substr(first, last);
      word.erase(remove(word.begin(), word.end(), '\t'), word.end());

      string name = upper_case(word);
      if (name == "NAME") { // Find "NAME" keyword in the stat file
         f_mcmc.close();
         stat_files.push_back(stat_file_plain);
         return;
      }
      if (word[0] == '#') continue;
      stat_files.push_back(line);
   };
   f_mcmc.close();
   return;
}

//______________________________________________________________________________
int stat_find_bestmodel(string const &stat_files, int switch_stat)
{
   //--- Finds and returns the index of the best fit of each statistical file
   //    and prints parameters for best-models (best chi2 or loglikelihood).
   //    N.B.: a stat. analysis file must be formatted as described in stat_load_list().
   //
   //  stat_files     File containing a single or a list of files
   //  switch_stat    Stat. mode [default = 0]
   //                    0->loglikelihood
   //                    1+3->chi2

   cout << ">>>>> Best-fit parameters (best chi2 or log likelihood) from a list of stat. analysis files" << endl;
   cout << "   [N.B.: a stat. analysis file must be formatted as described in stat_load_list()]" << endl;
   cout << endl;


   const int n_dm_params = 6;
   const int n_light_params = 6;
   const int n_aniso_params = 4;
   const int n_par_light_opt = 2;
   const int n_profile = 3;

   // Set parameters names
   const char name_dm_params [n_dm_params][100] = {"Rhos", "Rscale", "Rvir", "alpha", "beta", "gamma"};
   const char name_light_params [n_light_params][100] = {"Rvir", "L", "RLight", "alpha*", "beta*", "gamma*"};
   const char name_light_params_opt [n_par_light_opt][100] = {"RA_cnt(deg)", "Dec_cnt(deg)"};
   const char name_aniso_params [n_aniso_params][100] = {"Beta_Aniso_0", "Beta_Aniso_Inf", "RAniso", "AnisoShapeParam"};
   const char name_profile [n_profile][100] = {"DMprofile", "Lightprofile", "Anisoprofile"};

   // Check if one or multiples files
   vector<string> files;
   stat_file2files(stat_files, files);
   bool is_profile = 0;
   bool is_lightprofile = 0;
   bool is_lightprofile_supp = 0;
   bool is_anisoprofile = 0;

   // index of best fit
   int i_best = 0;

   for (int i = 0; i < (int)files.size(); ++i) { // loop on files
      struct gStructHalo stat_halo;
      stat_load_list(files[i], stat_halo, false);
      if (switch_stat == 1 || switch_stat == 3) { // chi2: find minimum
         double chi2min = 1.e99;
         for (int n = 0; n < int(stat_halo.StatChi2.size()); n++) {
            if (stat_halo.StatChi2[n] < chi2min) {
               chi2min = stat_halo.StatChi2[n];
               i_best = n;
            }
         }
      } else if (switch_stat == 0) {
         double Lmax = -1.e99; // log likelihood: find maximum
         for (int n = 0; n < int(stat_halo.StatChi2.size()); n++) {
            if (stat_halo.StatChi2[n] > Lmax) {
               Lmax = stat_halo.StatChi2[n];
               i_best = n;
            }
         }
      }

      // Determine what types of parameters are in the stat. file
      if (stat_halo.StatHaloProfile.size() != 0)
         is_profile = 1;
      if (stat_halo.StatLightProfile.size() != 0)
         is_lightprofile = 1;
      if (stat_halo.StatParam[15].size() != 0) // Ra_cnt and Dec_cnt are present
         is_lightprofile_supp = 1;
      if (stat_halo.StatAnisoProfile.size() != 0)
         is_anisoprofile = 1;

      // print parameters and best-fit values
      cout << "Name = " << stat_halo.Name.c_str() << endl;
      if (is_profile) { // print DM parameters
         cout << "     " << left << setw(20) << name_profile[0] << gNAMES_PROFILE[stat_halo.StatHaloProfile[i_best]] << endl;
         for (int j = 0; j < n_dm_params; j++)
            cout << "     " << left << setw(20) << name_dm_params[j] << stat_halo.StatParam[j][i_best] << endl;
      }
      if (is_lightprofile && !is_profile) { // in that case, print Rvir
         cout << "     " << left << setw(20) << name_profile[1] << gNAMES_LIGHTPROFILE[stat_halo.StatLightProfile[i_best]] << endl;
         cout << "     " << left << setw(20) << name_light_params[0] << stat_halo.StatParam[2][i_best] << endl;
         for (int j = 1; j < n_light_params; j++)
            cout << "     " << left << setw(20) << name_light_params[j] << stat_halo.StatParam[5 + j][i_best] << endl;
      }
      if (is_lightprofile && is_profile) { // do not print Rvir, already printed for DM parameters
         cout << "     " << left << setw(20) << name_profile[1] << gNAMES_LIGHTPROFILE[stat_halo.StatLightProfile[i_best]] << endl;
         for (int j = 1; j < n_light_params; j++)
            cout << "     " << left << setw(20) << name_light_params[j] << stat_halo.StatParam[5 + j][i_best] << endl;
      }
      if (is_lightprofile_supp) { // Print supplementary light parameters (Ra_cnt and Dec_cnt)
         for (int j = 0; j < 2; j++)
            cout << "     " << left << setw(20) << name_light_params_opt[j] << stat_halo.StatParam[15 + j][i_best] << endl;
      }
      if (is_anisoprofile) { // anisotropy parameters
         cout << "     " << left << setw(20) << name_profile[2] << gNAMES_ANISOTROPYPROFILE[stat_halo.StatAnisoProfile[i_best]] << endl;
         for (int j = 0; j < n_aniso_params; j++)
            cout << "     " << left << setw(20) << name_aniso_params[j] << stat_halo.StatParam[11 + j][i_best] << endl;
      }

      if (switch_stat == 1 || switch_stat == 3) { // print best chi2 and bestchi2/ndf
         cout << "     " << left << setw(20) << "chi2" << stat_halo.StatChi2[i_best] << endl;
         cout << "     " << left << setw(20) << "chi2/ndf" << stat_halo.StatChi2[i_best] / (double)(stat_halo.Ndata - stat_halo.Npar) << endl;
      } else // print best log likelihood
         cout << "     " << left << setw(20) << "Log likelihood" << stat_halo.StatChi2[i_best] << endl;
   }
   cout << endl;
   return i_best;
}

//______________________________________________________________________________
void stat_find_CLs(string const &stat_files, double const &cl, int switch_stat)
{
   //--- Finds and prints parameters for models having the min and max value
   //    (for a text user-selected parameter) among those meeting the CL
   //    requirement from the stat. analysis files.
   //    N.B.: a stat. analysis file must be formatted as described in stat_load_list().
   //
   //  stat_files     File containing a single or a list of files
   //  cl             Confidence level percentage for which we wish to search the min/max
   //  switch_stat    Stat. mode [default = 0]
   //                    0->uses the PDF CL method,
   //                    1->uses the chi2 CL method,
   //                    3->uses mean and dispersion (e.g., for bootstrap)

   // List of possible choices
   cout << ">>>>> CL lower and upper value (first and second line below) from a list of stat. analysis files" << endl;
   cout << "   [N.B.: a stat. analysis file must be formatted as described in stat_load_list()]" << endl;
   cout << endl;
   cout << " Select a parameter (from which the CL is calculated): " << endl;
   cout << "       rhos (kpc)                            [0]" << endl;
   cout << "       rs (kpc)                              [1] " << endl;
   cout << "       Rvir (kpc)                            [2] " << endl;
   cout << "       alpha                                 [3] " << endl;
   cout << "       beta (outer)                          [4] " << endl;
   cout << "       gamma (inner)                         [5] " << endl;
   cout << "       rho(r)                                [6] " << endl;
   cout << "       L  (L_sol)                            [7] " << endl;
   cout << "       rL (kpc)                              [8] " << endl;
   cout << "       alpha*                                [9] " << endl;
   cout << "       beta* (outer)                         [10] " << endl;
   cout << "       gamma* (inner)                        [11] " << endl;
   cout << "       nu(r) (Lsol/kpc^3)                    [12] " << endl;
   cout << "       I(R) (Lsol/kpc^2)                     [13] " << endl;
   cout << "       beta_0                                [14] " << endl;
   cout << "       beta_infty                            [15] " << endl;
   cout << "       ra (kpc)                              [16] " << endl;
   cout << "       eta                                   [17] " << endl;
   cout << "       beta(r)                               [18] " << endl;
   cout << "       M(r) (Msol)                           [19] " << endl;
   cout << "       J(alpha_int) (smooth)                 [20] " << endl;
   cout << "       sigma_p(R)(km/s) (projected)          [21] " << endl;
   cout << "       vr^2(r)(km^2/s^2) (unprojected)       [22] " << endl;

   cout << " .......your choice:  ";
   int choice = 0;
   cin >> choice; // Read choice

   double eps; // precision for numerical computation
   // Read supplementary information
   double r = 0.;
   if (choice < 0 || choice > 22) {
      printf("\n====> ERROR: stat_find_CLs() in janalysis.cc");
      printf("\n             Choice=%d is not a valid choice", choice);
      printf("\n             => abort()\n\n");
      abort();
   } else if (choice == 6 || choice == 12 || choice == 13 || choice == 18 || choice == 19 || choice == 21 || choice == 22) {
      cout << "   radius [kpc]: ";
      cin >> r;
   } else if (choice == 20) {
      cout << "   alpha_int [deg]: ";
      cin >> gSIM_ALPHAINT;
      gSIM_ALPHAINT *= (DEG_to_RAD);

      cout << "   Annihil[1] or decay[0]: ";
      cin >> gPP_DM_IS_ANNIHIL_OR_DECAY;

      cout << "   rho_sat [1.e19 Msol/kpc^3]: ";
      cin >> gDM_RHOSAT;
   }
   if (choice == 12 || choice == 13 || choice > 18) {
      cout << "   eps [0.01]:  ";
      cin >> eps;
   }
   cout << endl;

   // Check if one or multiples files
   vector<string> files;
   stat_file2files(stat_files, files);

   const int n_opt = 23;
   string xaxis[n_opt] = {"log10(rho_}/(Msol kpc3))", "log10(rs/kpc)", "log10(Rvir/kpc)", "alpha", "beta", "gamma", "log10(rho(r)/(Msol kpc3))", "log10(L/Lsol)",
                          "log10(rL/kpc)", "alpha*", "beta*", "gamma*", "log10(nu(r)/(Lsol/kpc3))", "log10(I(R)/Lsol/kpc2))", "beta0", "beta_inf", "log10(ra/kpc)", "eta", "beta(r)",
                          "log10(M(r)/Msol)", "log10(J(alpha_int)/(Msol2/kpc5))", "log10(sigmap(R)/(km/s)))", "log10(vr2(r)/(km2/s2))"
                         };

#if IS_ROOT
   int n_files = files.size();
   // PDF method
   if (switch_stat == 0) {
      string gr_name[n_opt] = {"cl_logrhos", "cl_logrs", "cl_logrvir", "cl_alpha", "cl_beta", "cl_gamma", "cl_logrhor", "cl_logL", "cl_logrL", "cl_alpha*", "cl_beta*", "cl_gamma*", "cl_lognur", "cl_logIr", "cl_beta0", "cl_betainf", "cl_logra", "cl_eta", "betar", "cl_logmr", "cl_logjalphaint", "cl_sigmap2", "cl_vr2"};
      string gr_xaxis[n_opt] = {"log_{10}(#rho_{s}/M_{#odot} kpc^{-3})", "log_{10}(r_{s}/kpc)", "log_{10}(R_{vir}/kpc)", "#alpha", "#beta", "#gamma", "log_{10}(#rho(r)/M_{#odot} kpc^{-3})", "log_{10}(L/L_{#odot})", "log_{10}(r_{L}/kpc)", "#alpha*", "#beta*", "#gamma*", "log_{10}(#nu(r)/L_{#odot} kpc^{-3})", "log_{10}(I(R)/L_{#odot} kpc^{-2})", "#beta_{0}",
                                "#beta_{#infty}", "log_{10}(r_{a}/kpc)", "#eta", "#beta(r)", "log_{10}(M(r)/M_{#odot})", "log_{10}(J(#alpha_{int})/M_{#odot}^{2} kpc^{-5})", "log_{10}(#sigma_{p}(R)/(km s^{-1}))",
                                "log_{10}(v_{r}^{2}(r)/(km^{2}s^{-2}))"
                               };

      if (gPP_DM_IS_ANNIHIL_OR_DECAY == 0) {
         xaxis[20] = "log10(D(alpha_int)/(Msol/kpc2))";
         gr_name[20] = "cl_logdalphaint";
         gr_xaxis[20] = "log_{10}(D(#alpha_{int})/M_{#odot} kpc^{-2})";
      }

      TH1D **h_y = new TH1D*[n_files];
      TH1D **h_y_temp = new TH1D*[n_files];
      vector<double> mean, median, most_likely, cl_lo, cl_up;

      for (int i = 0; i < n_files; ++i) { // loop on the file list
         struct gStructHalo stat_halo;
         stat_load_list(files[i], stat_halo, false); // load the parameters of each file
         int n_stat = (int)stat_halo.StatChi2.size();

         const int npar = 10; // General parameters (DM)
         const int npar_light = 8; // Light profile parameters
         const int npar_ani = 5; // Anisotropy parameters
         const int npar_jeans = 20; // Jeans parameters
         double par[npar];
         double par_light[npar_light];
         double par_ani[npar_ani];
         double par_jeans[npar_jeans];

         if (choice == 20) cout << " ... processing J for all file entries ..." << endl;
         if (choice == 21) cout << " ... processing sigma_p for all file entries ..." << endl;

         vector<double> val_all;
         for (int j = 0; j < n_stat; ++j) {
            double val = 0.;
            // Related to DM parameters
            if (choice >= 0 && choice <= 5) {
               val = stat_halo.StatParam[choice][j];
               if (val < 1.e-40) val = 1.e-40;
               val_all.push_back(log10(val));
            } else if (choice == 6) {
               // rho(r)
               stat_set_partot(j, par, stat_halo, true);
               rho(r, par, val);
               if (val < 1.e-40) val = 1.e-40;
               val_all.push_back(log10(val));
            } else if (choice == 19) {
               // M(r)
               stat_set_partot(j, par, stat_halo, true);
               par[6] = r;
               val = mass_singlehalo(par, eps);
               if (val < 1.e-40) val = 1.e-40;
               val_all.push_back(log10(val));
            } else if (choice == 20) {
               // J(alpha_int)
               stat_set_partot(j, par, stat_halo, true);
               val = jsmooth(par, par[8], par[9], eps);
               if (val < 1.e-40) val = 1.e-40;
               val_all.push_back(log10(val));
               // Related to light profile parameters
            } else if (choice >= 7 && choice <= 11) {
               // Light parameter values
               val = stat_halo.StatParam[choice - 1][j];
               if (val < 1.e-40) val = 1.e-40;
               val_all.push_back(log10(val));
            } else if (choice == 12) {
               // nu(r)
               stat_set_parlight(j, par_light, stat_halo);
               par_light[6] = gSIM_JEANS_RMAX; // Set the maximum radius of integration
               par_light[7] = eps;
               val = light_nu(r, par_light);
               if (val < 1.e-40) val = 1.e-40;
               val_all.push_back(log10(val));
            } else if (choice == 13) {
               // I(R)
               stat_set_parlight(j, par_light, stat_halo);
               par_light[6] = gSIM_JEANS_RMAX; // Set the maximum radius of integration
               par_light[7] = eps;
               val = light_i(r, par_light);
               if (val < 1.e-40) val = 1.e-40;
               val_all.push_back(log10(val));
            } else if (choice >= 14 && choice <= 17) {
               // Related to anisotropy parameters
               val = stat_halo.StatParam[choice - 3][j];
               if (fabs(val) < 1.e-40) val = 1.e-40;
               val_all.push_back(val);
            } else if (choice == 18) {
               // beta_ani(r)
               stat_set_paranis(j, par_ani, stat_halo);
               val = beta_anisotropy(r, par_ani);
               if (fabs(val) < 1.e-40) val = 1.e-40;
               val_all.push_back(val);
               // Related to Jeans analysis
            } else if (choice == 21) {
               // sigmap(r)
               stat_set_parjeans(j, par_jeans, stat_halo);
               double Rvir = par_jeans[6];
               par_jeans[6] = gSIM_JEANS_RMAX; // Set the maximum radius of integration
               par_jeans[13] = r; // set radius
               par_jeans[14] = eps; // set precision
               val = jeans_sigmap2(r, par_jeans);
               par_jeans[6] = Rvir; // Set back par_jeans[6] to its initial value
               val = sqrt(val);
               if (val < 1.e-40) val = 1.e-40;
               val_all.push_back(log10(val));
            } else if (choice == 22) {
               // nu*vr2(r)
               stat_set_parjeans(j, par_jeans, stat_halo);
               par_jeans[6] = gSIM_JEANS_RMAX; // Set the maximum radius of integration
               par_jeans[13] = r; // set radius
               par_jeans[14] = eps; // set precision
               val = jeans_nuvr2(r, par_jeans);
               par_jeans[13] = par_jeans[6]; // set maximum radius of integration (needed for some cases for computing nu)
               double nu = light_nu(r, &par_jeans[7]);
               if (nu < 1.e-20) {
                  cout << "The number density nu is 0 or negative at radius r = " << r << ". log10(vr2) set to -100" << endl;
                  val_all.push_back(-100.);
                  continue;
               }
               val *= NEWTON_CONSTANT_SI * M3perKG_to_KM2KPCperMSOL / nu;
               if (val < 1.e-40) val = 1.e-40;
               val_all.push_back(log10(val));
            }
         }

         //--- Calculate CLs

         double val_min_old = 1.e40;
         double val_max_old = -1.e40;

         for (int ii = 0; ii < (int)val_all.size(); ++ii) { // find min and max
            val_min_old = min(val_min_old, val_all[ii]);
            val_max_old = max(val_max_old, val_all[ii]);
         }

         if ((val_min_old == 0 && val_max_old == 0) || fabs((val_min_old - val_max_old) / val_max_old) < 1.e-5) {
            cout << "   " << xaxis[choice] << " for " << files[i] << " is a constant, nothing to do!" << endl;
            cout << endl;
            return;
         }

         double size_bin; // bin size of the histogram used to compute the CLs. It is automatically adapted.
         // initial values
         if (choice == 0 || choice == 1 || choice == 6 || choice == 7 || choice == 8 || choice == 12 || choice == 13 || choice == 16 || choice == 19 || choice == 20)
            size_bin = 0.01; // typical values
         else // quantities with less spread
            size_bin = 0.005;  //typical values

         int nbins = int((val_max_old - val_min_old) / size_bin);

         h_y_temp[i] = new TH1D("", "", nbins, val_min_old, val_max_old); // temporary histogram for finding the optimal bin size
         for (int k = 0; k < n_stat; ++k) {
            if (val_all[k] < val_max_old && val_all[k] > val_min_old)
               h_y_temp[i]->Fill(val_all[k]);
         }

         // Median
         double x_h1d_temp[1] = {0.};
         double q_h1d_temp = 0.5;
         h_y_temp[i]->GetQuantiles(1, x_h1d_temp, &q_h1d_temp);

         // 1 sigma quantiles
         double ql_temp[2] = {(1. - 0.68) / 2., 0.68 + (1. - 0.68) / 2.};
         double xl_temp[2] = {0., 0.};
         h_y_temp[i]->GetQuantiles(2, xl_temp, ql_temp);


         double val_max_new; // optimised values of min and max (5 sigma away from the median; CLs can be wrong if presence of a few points with very large/low values)
         double val_min_new;
         if (val_max_old > x_h1d_temp[0] + 5.*(xl_temp[1] - x_h1d_temp[0])) // 5 sigmas away from median
            val_max_new = x_h1d_temp[0] + 5.*(xl_temp[1] - x_h1d_temp[0]);
         else
            val_max_new = val_max_old;

         if (val_min_old < x_h1d_temp[0] - 5.*(x_h1d_temp[0] - xl_temp[0])) // 5 sigmas away from median
            val_min_new = x_h1d_temp[0] - 5.*(x_h1d_temp[0] - xl_temp[0]);
         else
            val_min_new = val_min_old;

         double dispersion = 0.5 * ((xl_temp[1] - x_h1d_temp[0]) + (x_h1d_temp[0] - xl_temp[0]));

         size_bin = dispersion / 25.; // optimised bin size, depends on the dispersion of the distribution
         nbins = int((val_max_new - val_min_new) / size_bin);

         h_y[i] = new TH1D(gr_name[choice].c_str(), gr_name[choice].c_str(), nbins, val_min_new, val_max_new); // real histo used for the CLs
         for (int k = 0; k < n_stat; ++k) {
            if (val_all[k] < val_max_new && val_all[k] > val_min_new)
               h_y[i]->Fill(val_all[k]);
         }

         // Mean
         mean.push_back(h_y[i]->GetMean());

         // Median
         double q_h1d = 0.5;
         double x_h1d[1] = {0.};
         h_y[i]->GetQuantiles(1, x_h1d, &q_h1d);
         median.push_back(x_h1d[0]);

         // Most likely
         double step_hy = (val_max_new - val_min_new) / (double)(val_all.size() + 1);
         most_likely.push_back(val_min_new + h_y[i]->GetMaximumBin() * step_hy);

         // PDF quantiles
         double ql[2] = {(1. - cl) / 2., cl + (1. - cl) / 2.};
         double xl[2] = {0., 0.};
         h_y[i]->GetQuantiles(2, xl, ql);
         cl_lo.push_back(xl[0]);
         cl_up.push_back(xl[1]);
      } // end of loop on files
      delete[] h_y_temp;
      h_y_temp = NULL;

      // Print result
      for (int i = 0; i < n_files; ++i) {
         cout << "   " << xaxis[choice] << " for " << files[i] << endl;
         printf("   mean=%le   median=%le   most-likely=%le\n", mean[i], median[i], most_likely[i]);
         printf("        CL_lo=%le   CL_up=%le\n", cl_lo[i], cl_up[i]);
      }

      //--- Displays
      if (gSIM_IS_DISPLAY) {
         TCanvas **c_cl_pdf = new TCanvas*[n_files];
         TPaveText *txt = NULL;
         TLine *line_lo = NULL;
         TLine *line_up = NULL;

         for (int i = 0; i < n_files; ++i) {
            // Draw canvas and histos.
            char name[50];
            sprintf(name, "c_cl_pdf_file%d", i);
            c_cl_pdf[i] = new TCanvas(name, name, i + 10, i + 5, 400, 400);
            c_cl_pdf[i]->SetHighLightColor(0);
            c_cl_pdf[i]->Range(0, 0, 1, 1);
            c_cl_pdf[i]->SetFillColor(kWhite);
            c_cl_pdf[i]->SetFrameFillColor(kWhite);
            c_cl_pdf[i]->SetBorderMode(0);
            c_cl_pdf[i]->SetBorderSize(0);
            c_cl_pdf[i]->SetLeftMargin(0.15);
            c_cl_pdf[i]->SetRightMargin(0.025);
            c_cl_pdf[i]->SetTopMargin(0.025);
            c_cl_pdf[i]->SetBottomMargin(0.15);
            c_cl_pdf[i]->SetFrameBorderMode(0);
            h_y[i]->SetStats(0);
            h_y[i]->SetTitle("");
            h_y[i]->GetXaxis()->SetNdivisions(504);
            h_y[i]->GetXaxis()->SetLabelOffset(0.01);
            h_y[i]->GetXaxis()->SetLabelFont(132);
            h_y[i]->GetXaxis()->SetLabelSize(0.06);
            h_y[i]->GetXaxis()->SetTitleSize(0.075);
            h_y[i]->GetXaxis()->SetTitle(gr_xaxis[choice].c_str());
            h_y[i]->GetXaxis()->SetTickLength(0.08);
            h_y[i]->GetXaxis()->SetTitleOffset(0.8);
            h_y[i]->GetXaxis()->SetTitleFont(132);
            h_y[i]->GetYaxis()->SetNdivisions(504);
            h_y[i]->GetYaxis()->SetLabelFont(132);
            h_y[i]->GetYaxis()->SetLabelOffset(0.01);
            h_y[i]->GetYaxis()->SetLabelSize(0.06);
            h_y[i]->GetYaxis()->SetTitleSize(0.075);
            h_y[i]->GetYaxis()->SetTitle("PDF");
            h_y[i]->GetYaxis()->SetTickLength(0.08);
            h_y[i]->GetYaxis()->SetTitleOffset(0.85);
            h_y[i]->GetYaxis()->SetTitleFont(132);
            double norm = h_y[i]->Integral();
            h_y[i]->Scale(1. / norm);
            h_y[i]->Draw("");
            gSIM_CLUMPYAD->Draw();
            line_lo = new TLine(cl_lo[i], h_y[i]->GetMinimum(), cl_lo[i], h_y[i]->GetMaximum());
            line_lo->SetLineStyle(1);
            line_lo->SetLineWidth(4);
            line_lo->SetLineColor(kGray + 1);
            line_lo->Draw();
            line_up = new TLine(cl_up[i], h_y[i]->GetMinimum(), cl_up[i], h_y[i]->GetMaximum());
            line_up->SetLineStyle(1);
            line_up->SetLineWidth(4);
            line_up->SetLineColor(kGray + 1);
            line_up->Draw();
            char txt_cl[50];
            sprintf(txt_cl, "%.1f%% CLs", cl * 100);
            txt = new TPaveText(0.7, 0.8, 0.95, 0.9, "NDC");
            txt->SetTextSize(0.06);
            txt->SetTextFont(132);
            txt->SetFillColor(0);
            txt->SetBorderSize(0);
            txt->SetTextColor(kGray + 1);
            txt->SetFillStyle(0);
            txt->SetTextAlign(12);
            txt->AddText(txt_cl);
            txt->Draw();

            c_cl_pdf[i]->Update();
            c_cl_pdf[i]->Modified();
         }

         gSIM_ROOTAPP->Run(kTRUE);
         delete txt;
         txt = NULL;
         delete line_lo;
         line_lo = NULL;
         delete line_up;
         line_up = NULL;
         delete[] h_y;
         h_y = NULL;
         delete[] c_cl_pdf;
         c_cl_pdf = NULL;
      }

      // chi2 method
   } else if (switch_stat == 1 || switch_stat == 3) {

      for (int i = 0; i < n_files; ++i) { // loop on list of files
         struct gStructHalo stat_halo;
         stat_load_list(files[i], stat_halo, false); // load stat file

         // Extract quantile using an histogram
         double chi2min = 1.e40, chi2max = -1.e40;
         int n_stat = stat_halo.StatChi2.size();
         for (int j = 0; j < n_stat; ++j) {
            if (stat_halo.StatChi2[j] < chi2min) chi2min = stat_halo.StatChi2[j]; // find chi2 min
            if (stat_halo.StatChi2[j] > chi2max) chi2max = stat_halo.StatChi2[j]; // find chi2 max
         }
         // Fill a TH1D histo for chi2 to retrieve the quantiles
         TH1D  *h_chi2 = new TH1D("chi2", "chi2", n_stat, chi2min, chi2max);
         for (int j = 0; j < n_stat; ++j)
            h_chi2->Fill(stat_halo.StatChi2[j]);
         double chi2_xcl = 0.;
         h_chi2->GetQuantiles(1, &chi2_xcl, &cl);
         delete h_chi2;
         h_chi2 = NULL;

         double valmin = 1.e40, valmax = -1.e40;

         if (choice == 20) cout << " ... processing J for all file entries ..." << endl;
         if (choice == 21) cout << " ... processing sigma_p for all file entries ..." << endl;
         for (int j = 0; j < n_stat; ++j) {
            // Find val_min and val_max

            if (stat_halo.StatChi2[j] > chi2_xcl) continue;

            const int npar = 10; // General parameters (DM)
            const int npar_light = 8; // Light profile parameters
            const int npar_ani = 5; // Anisotropy parameters
            const int npar_jeans = 20; // Jeans parameters
            double par[npar];
            double par_light[npar_light];
            double par_ani[npar_ani];
            double par_jeans[npar_jeans];
            double val = 0.;

            // Related to DM parameters
            if (choice >= 0 && choice <= 5) {
               // DM parameter values
               val = stat_halo.StatParam[choice][j];
               if (val < 1.e-40) val = 1.e-40;
               val = log10(val);
            } else if (choice == 6) {
               // rho(r)
               stat_set_partot(j, par, stat_halo, true);
               rho(r, par, val);
               if (val < 1.e-40) val = 1.e-40;
               val = log10(val);
            } else if (choice == 19) {
               // M(r)
               stat_set_partot(j, par, stat_halo, true);
               par[6] = r;
               val = mass_singlehalo(par, eps);
               if (val < 1.e-40) val = 1.e-40;
               val = log10(val);
            } else if (choice == 20) {
               // J(alpha_int)
               stat_set_partot(j, par, stat_halo, true);
               val = jsmooth(par, par[8], par[9], eps);
               if (val < 1.e-40) val = 1.e-40;
               val = log10(val);
               // Related to light profile parameters
            } else if (choice >= 7 && choice <= 11) {
               // Light parameter values
               val = stat_halo.StatParam[choice - 1][j];
               if (val < 1.e-40) val = 1.e-40;
               val = log10(val);
            } else if (choice == 12) {
               // nu(r)
               stat_set_parlight(j, par_light, stat_halo);
               par_light[6] = gSIM_JEANS_RMAX; // Set the maximum radius of integration
               par_light[7] = eps;
               val = light_nu(r, par_light);
               if (val < 1.e-40) val = 1.e-40;
               val = log10(val);
            } else if (choice == 13) {
               // I(R)
               stat_set_parlight(j, par_light, stat_halo);
               par_light[6] = gSIM_JEANS_RMAX; // Set the maximum radius of integration
               par_light[7] = eps;
               val = light_i(r, par_light);
               if (val < 1.e-40) val = 1.e-40;
               val = log10(val);
               // Related to anisotropy parameters
            } else if (choice >= 14 && choice <= 17) {
               val = stat_halo.StatParam[choice - 3][j];
               if (fabs(val) < 1.e-40) val = 1.e-40;
            } else if (choice == 18) {
               stat_set_paranis(j, par_ani, stat_halo);
               val = beta_anisotropy(r, par_ani);
               if (fabs(val) < 1.e-40) val = 1.e-40;
               // Related to Jeans analysis
            } else if (choice == 21) {
               // sigmap(r)
               stat_set_parjeans(j, par_jeans, stat_halo);
               par_jeans[6] = gSIM_JEANS_RMAX; // Set the maximum radius of integration
               par_jeans[13] = r; // set radius
               par_jeans[14] = eps; // set precision
               val = jeans_sigmap2(r, par_jeans);
               cout << "val = " << val << endl;
               val = sqrt(val);
               if (val < 1.e-40) val = 1.e-40;
               val = log10(val);
            } else if (choice == 22) {
               // nu*vr2(r)
               stat_set_parjeans(j, par_jeans, stat_halo);
               par_jeans[6] = gSIM_JEANS_RMAX; // Set the maximum radius of integration
               par_jeans[13] = r; // set radius
               par_jeans[14] = eps; // set precision
               val = jeans_nuvr2(r, par_jeans);
               par_jeans[13] = par_jeans[6]; // set maximum integration radius (needed for some cases for computing nu)
               double nu = light_nu(r, &par_jeans[7]);
               if (nu < 1.e-20) {
                  cout << "The number density nu is 0 or negative at radius r = " << r << ". log10(vr2) set to -100" << endl;
                  val = -100.;
                  continue;
               }
               val *= NEWTON_CONSTANT_SI * M3perKG_to_KM2KPCperMSOL / nu;
               if (val < 1.e-40) val = 1.e-40;
               val = log10(val);
            }
            if (val < valmin) // find minimum inside the quantiles
               valmin = val;
            if (val > valmax) // find maximum inside the quantiles
               valmax = val;
         }

         // Print result
         cout << "   " << xaxis[choice] << " for " << files[i] << endl;
         printf("        CL_lo=%le   CL_up=%le\n", valmin, valmax);
      }
   }
#else
   printf("\n====> ERROR: stat_find_CLs() in janalysis.cc");
   printf("\n             You need ROOT to be installed to run this function.");
   printf("\n             => abort()\n\n");
   abort();
#endif
   cout << endl;
}

//______________________________________________________________________________
void stat_load_list(string const &stat_file, struct gStructHalo &stat_halo, bool is_verbose)
{
   //--- Reads a statistical analysis file (halo position + list of possible
   //    combinations of DM, light and anisotropy profile parameters).
   //    N.B.: a statistical file must be formatted as follows (e.g., data/stat_example.dat):
   //       Name   long.(deg)  lat.(deg)  l(kpc)  Ndata  Npar
   //       Nameparam1 Nameparam2 Nameparam3 ...
   //    For each profile keyword (profile, lightprofile, anisoprofile) used as Nameparam,
   //    you need to use the set of following parameters (in any order):
   //      profile: rhos(Msol/kpc3)  rs(kpc)  Rvir(kpc) alpha  beta  gamma
   //      lightprofile: L(Lsol)  rL(kpc)  Rvir(kpc)  alpha*  beta*  gamma* (+ optionnal: RA_cnt(deg), Dec_cnt(deg))
   //      anisoprofile: beta0   betainf   raniso   eta
   //    You also need to use a "chi2" keyword
   //    Exemple:
   //      rhos(Msol/kpc3)  rs(kpc)  Rvir(kpc) alpha  beta  gamma profile lightprofile L(Lsol)  rL(kpc)  Rvir(kpc)  alpha*  beta*  gamma* chi2
   //
   // Input:
   //  stat_file      File to be read
   //  stat_halo      Structure to store the parameters
   //  is_verbose     Chatter or not...
   // Output:
   //  stat_halo      Structure to store the parameters

   string stat_file_plain = stat_file;
   resolve_envvar(stat_file_plain);


   if (is_verbose) {
      cout << ">>>>> Read and load " << stat_file_plain << endl;
      cout << "   N.B.: a stat. analysis file must be formatted as follows:" << endl;
      cout << "   Name   long.(deg)  lat.(deg)  l(kpc)  Ndata  Npar  Rmax(kpc)" << endl;
      cout << "   Nameparam1 Nameparam2 Nameparam3 ... " << endl;
      cout << "   For each profile keyword (profile, lightprofile, anisoprofile) used as Nameparam, you need to use the set of following parameters (in any order): " << endl;
      cout << "   profile: rhos(Msol/kpc3)  rs(kpc)  Rvir(kpc) alpha  beta  gamma " << endl;
      cout << "   lightprofile: L(Lsol)  rL(kpc)  Rvir(kpc)  alpha*  beta*  gamma* (+ optionnal: RA_cnt(deg), Dec_cnt(deg))" << endl;
      cout << "   anisoprofile: beta0   betainf   raniso(kpc)   eta " << endl;
      cout << "   You also need to use the \"chi2\" keyword " << endl;

      cout << "   Exemple: " << endl;
      cout << "   rhos(Msol/kpc3)  rs(kpc)  Rvir(kpc) alpha  beta  gamma profile lightprofile L(Lsol)  rL(kpc)  Rvir(kpc)  alpha*  beta*  gamma* chi2" << endl;
   }

   // Open file and check if exists
   ifstream f_mcmc(stat_file_plain.c_str());
   if (!f_mcmc) {
      printf("\n====> ERROR: stat_load_list() in janalysis.cc");
      printf("\n             Cannot open (and read) file %s", stat_file_plain.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }

   // list of possible parameters
   const int n_par_dm = 6;
   const char names_par_dm [n_par_dm][100] = {"rhos(Msol/kpc3)", "rs(kpc)", "Rvir(kpc)", "alpha", "beta", "gamma"};
   const int n_par_light = 6;
   const char names_par_light [n_par_light][100] = {"L(Lsol)", "rL(kpc)", "Rvir(kpc)", "alpha*", "beta*", "gamma*"};
   const int n_par_light_opt = 2;
   const char names_par_light_opt [n_par_light_opt][100] = {"RA_cnt(deg)", "Dec_cnt(deg)"};
   const int n_par_aniso = 4;
   const char names_par_aniso [n_par_aniso][100] = {"beta0", "betainf", "raniso(kpc)", "eta"};
   // list of profile keywords
   const int n_keyword_profiles = 3;
   const char keyword_profiles [n_keyword_profiles][100] = {"profile", "lightprofile", "anisoprofile"};

   // read stat_file
   ifstream f_read(stat_file_plain.c_str());
   string names_line;

   // booleans, describe what type of parameters are present in the stat_file
   bool is_profile = 0;
   bool is_lightprofile = 0;
   bool is_anisoprofile = 0;
   bool is_angles = 0;

   // indexes of different columns
   int jprofile = 0;
   int jlightprofile = 0;
   int janisoprofile = 0;
   //int jangles = 0;
   int jchi2 = -1;
   int iline = -1;
   int i = 0;

   // 0: column of rhos(Msol/kpc3), 1: column of rs(kpc) ... in the same order that names_par_dm
   int column_dm[n_par_dm];
   for (int j = 0; j < n_par_dm; j++)
      column_dm[j] = -10; // default value for testing if the array is fully filled

   // 0: column of L(Lsol), 1: column of rL(kpc) ... in the same order that names_par_light
   int column_light[n_par_light];
   for (int j = 0; j < n_par_light; j++)
      column_light[j] = -10; // default value for testing if the array is fully filled

   // 0: column of RA_cnt(deg), 1: column of Dec_cnt(deg) ... in the same order that names_par_light_opt
   int column_light_opt[n_par_light_opt];
   for (int j = 0; j < n_par_light_opt; j++)
      column_light_opt[j] = -10; // default value for testing if the array is fully filled

   // 0: column of beta0, 1: column of betainf ... in the same order that names_par_aniso
   int column_ani[n_par_aniso];
   for (int j = 0; j < n_par_aniso; j++)
      column_ani[j] = -10; // default value for testing if the array is fully filled

   // Read stat file and find keywords
   while (iline == -1) {
      getline(f_read, names_line);
      names_line.erase(remove_if(names_line.begin(), names_line.end(), isNotASCII), names_line.end());
      // if it is a comment (start with #) or a blanck line, skip it
      string line = removeblanks_from_startstop(names_line);
      if (line[0] == '#' || line.empty()) {
         i = i + 1;
         continue;
      }
      vector<string> read_params;
      replace_substring(line, "\t", " ");
      string2list(line, " ", read_params);

      // find column indexes of each possible profile keyword
      for (int j = 0; j < int(read_params.size()); j++) {
         if (read_params[j] == keyword_profiles[0]) { //DM profile
            is_profile = 1;
            jprofile = j;
            iline = i;
         } else if (read_params[j] == keyword_profiles[1]) { //lightprofile
            is_lightprofile = 1;
            jlightprofile = j;
            iline = i;
         } else if (read_params[j] == names_par_light_opt[0] || read_params[j] == names_par_light_opt[1]) { //lightprofile_opt
            is_angles = 1;
            //jangles = j;
            iline = i;
         } else if (read_params[j] == keyword_profiles[2]) { //anisotropy profile
            is_anisoprofile = 1;
            janisoprofile = j;
            iline = i;
         } else if (read_params[j] == "chi2") {
            jchi2 = j;
         }
      }

      if (is_profile) { // DM profile
         for (int j = 0; j < int(read_params.size()); j++) {
            for (int k = 0; k < n_par_dm; k++) {
               if (read_params[j] == names_par_dm[k])
                  column_dm[k] = j; // set the column indexes of DM profile parameters
            }
         }
         for (int n = 0; n < n_par_dm; n++) {
            if (column_dm[n] == -10) {
               printf("\n====> ERROR: stat_load_list() in janalysis.cc");
               printf("\n             Wrong number of DM profile parameters or wrong parameter names in file %s", stat_file_plain.c_str());
               printf("\n             The needed parameters are: ");
               for (int k = 0 ; k < n_par_dm; k++)
                  printf("%s ", names_par_dm[k]);
               printf("\n             => abort()\n\n");
               abort();
            }
         }
      }
      if (is_lightprofile) {
         for (int j = 0; j < int(read_params.size()); j++) {
            for (int k = 0; k < n_par_light; k++) {
               if (read_params[j] == names_par_light[k])
                  column_light[k] = j; // set the column indexes of light profile parameters
            }
         }
         for (int n = 0; n < n_par_light; n++) {
            if (column_light[n] == -10) {
               printf("\n====> ERROR: stat_load_list() in janalysis.cc");
               printf("\n             Wrong number of light profile parameters or wrong parameter names in file %s", stat_file_plain.c_str());
               printf("\n             The needed parameters are: ");
               for (int k = 0 ; k < n_par_light; k++)
                  printf("%s ", names_par_light[k]);
               printf("\n             => abort()\n\n");
               abort();
            }
         }
      }
      if (is_angles) {
         for (int j = 0; j < int(read_params.size()); j++) {
            for (int k = 0; k < n_par_light_opt; k++) {
               if (read_params[j] == names_par_light_opt[k])
                  column_light_opt[k] = j; // set the column indexes of light profile supplementary parameters (Ra_cnt and Dec_cnt)
            }
         }
         for (int n = 0; n < n_par_light_opt; n++) {
            if (column_light_opt[n] == -10) {
               printf("\n====> ERROR: stat_load_list() in janalysis.cc");
               printf("\n             Wrong number of angle parameters or wrong parameter names in file %s", stat_file_plain.c_str());
               printf("\n             The needed parameters are: ");
               for (int k = 0 ; k < n_par_light_opt; k++)
                  printf("%s ", names_par_light_opt[k]);
               printf("\n             => abort()\n\n");
               abort();
            }
         }
      }
      if (is_anisoprofile) {
         for (int j = 0; j < int(read_params.size()); j++) {
            for (int k = 0; k < n_par_aniso; k++) {
               if (read_params[j] == names_par_aniso[k])
                  column_ani[k] = j; // set the column indexes of anisotropy profile parameters
            }
         }
         for (int n = 0; n < n_par_aniso; n++) {
            if (column_ani[n] == -10) {
               printf("\n====> ERROR: stat_load_list() in janalysis.cc");
               printf("\n             Wrong number of anisotropy profile parameters or wrong parameter names in file %s", stat_file_plain.c_str());
               printf("\n             The needed parameters are: ");
               for (int k = 0 ; k < n_par_aniso; k++)
                  printf("%s ", names_par_aniso[k]);
               printf("\n             => abort()\n\n");
               abort();
            }
         }
      }
      i += 1;
   }

   if (iline == -1) {
      printf("\n====> ERROR: stat_load_list() in janalysis.cc");
      printf("\n             No profile keyword (profile, lightprofile, anisoprofile) found in file %s", stat_file_plain.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }
   if (jchi2 == -1) {
      printf("\n====> ERROR: stat_load_list() in janalysis.cc");
      printf("\n             No chi2 keyword found in file %s", stat_file_plain.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }

   // Read halo properties from header
   string trash;
   ifstream f_info(stat_file_plain.c_str());
   for (int p = 0; p < 7; ++p) f_info >> trash;
   f_info >> stat_halo.Name >> stat_halo.PsiDeg >> stat_halo.ThetaDeg
          >> stat_halo.l >> stat_halo.Ndata >> stat_halo.Npar >> gSIM_JEANS_RMAX; // set the halo properties from the stat file header

   for (int n = 0; n < int(sizeof(stat_halo.StatIsFreeParam) / sizeof(stat_halo.StatIsFreeParam[0])); n++)
      stat_halo.StatIsFreeParam[n] = 0; // determine what parameters are free/fixed

   i = 0;
   int k = 0;
   string line_input;
   ifstream f_load(stat_file_plain.c_str());
   while (getline(f_load, line_input).good()) { // Fill stat_halo with the different parameters
      if (i <= iline) {
         i = i + 1;
      } else {
         getline(f_read, line_input);
         line_input.erase(remove_if(line_input.begin(), line_input.end(), isNotASCII), line_input.end());

         // if it is a comment (start with #) or a blanck line, skip it
         string line = removeblanks_from_startstop(line_input);
         if (line[0] == '#' || line.empty()) continue;
         vector<double> params;
         vector<string> profiles;
         replace_substring(line, "\t", " ");
         string2list(line, " ", params);
         string2list(line, " ", profiles);
         stat_halo.StatChi2.push_back(params[jchi2]); // fill chi2 or loglikelihood values

         if (is_profile) { // DM profile parameters
            stat_halo.StatParam[0].push_back(params[column_dm[0]]);
            stat_halo.StatParam[1].push_back(params[column_dm[1]]);
            stat_halo.StatParam[2].push_back(params[column_dm[2]]);
            profiles[jprofile].erase(remove(profiles[jprofile].begin(), profiles[jprofile].end(), '\t'), profiles[jprofile].end());
            stat_halo.StatHaloProfile.push_back(string_to_enum("FLAG_PROFILE", profiles[jprofile]));
            stat_halo.StatParam[3].push_back(params[column_dm[3]]);
            stat_halo.StatParam[4].push_back(params[column_dm[4]]);
            stat_halo.StatParam[5].push_back(params[column_dm[5]]);

            if (k != 0) { // determine if a given parameter is fixed or not
               if (params[column_dm[0]] != stat_halo.StatParam[0][k - 1]) // if a parameter changes from one line to another, set it as free
                  stat_halo.StatIsFreeParam[0] = 1;
               if (params[column_dm[1]] != stat_halo.StatParam[1][k - 1])
                  stat_halo.StatIsFreeParam[1] = 1;
               if (params[column_dm[2]] != stat_halo.StatParam[2][k - 1])
                  stat_halo.StatIsFreeParam[2] = 1;
               if (params[column_dm[3]] != stat_halo.StatParam[3][k - 1])
                  stat_halo.StatIsFreeParam[3] = 1;
               if (params[column_dm[4]] != stat_halo.StatParam[4][k - 1])
                  stat_halo.StatIsFreeParam[4] = 1;
               if (params[column_dm[5]] != stat_halo.StatParam[5][k - 1])
                  stat_halo.StatIsFreeParam[5] = 1;
            }
         }
         if (is_lightprofile) { // light profile parameters
            stat_halo.StatParam[6].push_back(params[column_light[0]]);
            stat_halo.StatParam[7].push_back(params[column_light[1]]);
            if (!is_profile) // if no DM parameters, then fill the Rvir of the light profile parameters
               stat_halo.StatParam[2].push_back(params[column_light[2]]);
            profiles[jlightprofile].erase(remove(profiles[jlightprofile].begin(), profiles[jlightprofile].end(), '\t'), profiles[jlightprofile].end());
            stat_halo.StatLightProfile.push_back(string_to_enum("FLAG_LIGHTPROFILE", profiles[jlightprofile]));
            stat_halo.StatParam[8].push_back(params[column_light[3]]);
            stat_halo.StatParam[9].push_back(params[column_light[4]]);
            stat_halo.StatParam[10].push_back(params[column_light[5]]);

            if (k != 0) { // determine if a given parameter is fixed or not
               if (params[column_light[0]] != stat_halo.StatParam[6][k - 1]) // if a parameter changes from one line to another, set it as free
                  stat_halo.StatIsFreeParam[6] = 1;
               if (params[column_light[1]] != stat_halo.StatParam[7][k - 1])
                  stat_halo.StatIsFreeParam[7] = 1;
               if (!is_profile) {
                  if (params[column_light[2]] != stat_halo.StatParam[2][k - 1])
                     stat_halo.StatIsFreeParam[2] = 1;
               }
               if (params[column_light[3]] != stat_halo.StatParam[8][k - 1])
                  stat_halo.StatIsFreeParam[8] = 1;
               if (params[column_light[4]] != stat_halo.StatParam[9][k - 1])
                  stat_halo.StatIsFreeParam[9] = 1;
               if (params[column_light[5]] != stat_halo.StatParam[10][k - 1])
                  stat_halo.StatIsFreeParam[10] = 1;
            }
         }
         if (is_angles) { // light profile supplementary parameters
            stat_halo.StatParam[15].push_back(params[column_light_opt[0]]);
            stat_halo.StatParam[16].push_back(params[column_light_opt[1]]);

            if (k != 0) { // determine if a given parameter is fixed or not
               if (params[column_light[0]] != stat_halo.StatParam[15][k - 1]) // if a parameter changes from one line to another, set it as free
                  stat_halo.StatIsFreeParam[15] = 1;
               if (params[column_light[1]] != stat_halo.StatParam[16][k - 1])
                  stat_halo.StatIsFreeParam[16] = 1;
            }
         }
         if (is_anisoprofile) { // anisotropy parameters
            stat_halo.StatParam[11].push_back(params[column_ani[0]]);
            stat_halo.StatParam[12].push_back(params[column_ani[1]]);
            profiles[janisoprofile].erase(remove(profiles[janisoprofile].begin(), profiles[janisoprofile].end(), '\t'), profiles[janisoprofile].end());
            stat_halo.StatAnisoProfile.push_back(string_to_enum("FLAG_ANISOTROPYPROFILE", profiles[janisoprofile]));
            stat_halo.StatParam[13].push_back(params[column_ani[2]]);
            stat_halo.StatParam[14].push_back(params[column_ani[3]]);

            if (k != 0) { // determine if a given parameter is fixed or not
               if (params[column_ani[0]] != stat_halo.StatParam[11][k - 1]) // if a parameter changes from one line to another, set it as free
                  stat_halo.StatIsFreeParam[11] = 1;
               if (params[column_ani[1]] != stat_halo.StatParam[12][k - 1])
                  stat_halo.StatIsFreeParam[12] = 1;
               if (params[column_ani[2]] != stat_halo.StatParam[13][k - 1])
                  stat_halo.StatIsFreeParam[13] = 1;
               if (params[column_ani[3]] != stat_halo.StatParam[14][k - 1])
                  stat_halo.StatIsFreeParam[14] = 1;
            }
         }
         k = k + 1;
      }
   }

   int n_free_pars = 0;
   for (int l = 0; l < int(sizeof(stat_halo.StatIsFreeParam) / sizeof(stat_halo.StatIsFreeParam[0])); l++) {
      if (stat_halo.StatIsFreeParam[l])
         n_free_pars = n_free_pars + 1;
   }

   if (stat_halo.Npar != n_free_pars) {
      cout << "Npar in file " << stat_file_plain << " is different than the real number of free parameters. Its value (" << stat_halo.Npar <<
           ") is changed to the real one (" << n_free_pars << ")." << endl;
      stat_halo.Npar = n_free_pars;
   }
   f_read.close();

}

//______________________________________________________________________________
void stat_set_paranis(int i, double par_anis[5], struct gStructHalo &stat_halo)
{
   //--- Sets the content of par_anis from the i-th statistical halo from stat_halo.
   //
   // INPUTS:
   //  i              Index of the halo to use
   //  stat_halo      gStructHalo structure containing a vector of halo parameters (filled with stat_load_list())
   // OUTPUTS:
   //  par_anis[0]    Anisotropy at r=0
   //  par_anis{1]    Anisotropy at r=+infinity
   //  par_anis[2]    Anisotropy Scale radius [kpc]
   //  par_anis[3]    Shape parameter eta
   //  par_anis[4]    card_profile [gENUM_ANISOTROPYPROFILE] for Anisotropy Profile


   if (i < 0 || i >= (int)((stat_halo.StatParam[11]).size())) {
      printf("\n====> ERROR: stat_set_paranis() in janalysis.cc");
      printf("\n             Index i=%d is out of range", i);
      printf("\n             => abort()\n\n");
      abort();
   }

   if (stat_halo.StatAnisoProfile.size() == 0) {
      printf("\n====> ERROR: stat_set_paranis() in janalysis.cc");
      printf("\n             No anisotropy profile found in halo #%d", i);
      printf("\n             => abort()\n\n");
      abort();
   }

   // Fill parameters from stat_halo
   par_anis[0] = stat_halo.StatParam[11][i];
   par_anis[1] = stat_halo.StatParam[12][i];
   par_anis[2] = stat_halo.StatParam[13][i];
   par_anis[3] = stat_halo.StatParam[14][i];
   par_anis[4] = stat_halo.StatAnisoProfile[i];

}

//______________________________________________________________________________
void stat_set_parjeans(int i, double par_jeans[20], struct gStructHalo &stat_halo)
{
   //--- Sets the content of par_jeans from the i-th statistical halo from stat_halo.
   //    N.B.: eps and par_jeans[13] are set at a default value which will is changed
   //    during the call of a jeans analysis-related function.
   //
   // INPUTS:
   //  i              Index of the halo to use
   //  stat_halo      gStructHalo structure containing a vector of halo parameters (filled with stat_load_list())
   // OUTPUTS:
   //  par_jeans[0]   Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]   Dark matter profile Scale radius [kpc]
   //  par_jeans[2]   Dark matter Shape parameter #1
   //  par_jeans[3]   Dark matter Shape parameter #2
   //  par_jeans[4]   Dark matter Shape parameter #3
   //  par_jeans[5]   Dark matter card_profile [gENUM_PROFILE]
   //  par_jeans[6]   Max radius of integration
   //  par_jeans[7]   Normalisation of Light profile [Lsol]
   //  par_jeans[8]   Light profile scale radius [kpc]
   //  par_jeans[9]   Light profile Shape parameter #1
   //  par_jeans[10]  Light profile Shape parameter #2
   //  par_jeans[11]  Light profile Shape parameter #3
   //  par_jeans[12]  Light profile card_profile [gENUM_LIGHTPROFILE] for Light Profile
   //  par_jeans[13]  R: projected radius considered for calculation of a projected quantity (kpc)
   //  par_jeans[14]  eps: precision for integrations
   //  par_jeans[15]  Anisotropy at r=0
   //  par_jeans[16]  Anisotropy at r=+infinity
   //  par_jeans[17]  Anisotropy profile Scale radius [kpc]
   //  par_jeans[18]  Anisotropy profile Shape parameter
   //  par_jeans[19]  card_profile [gENUM_ANISOTROPYPROFILE] for Anisotropy Profile



   if (i < 0 || i >= (int)((stat_halo.StatParam[0]).size())) {
      printf("\n====> ERROR: stat_set_parjeans() in janalysis.cc");
      printf("\n             Index i=%d is out of range", i);
      printf("\n             => abort()\n\n");
      abort();
   }

   if (stat_halo.StatHaloProfile.size() == 0) {
      printf("\n====> ERROR: stat_set_parjeans() in janalysis.cc");
      printf("\n             No DM profile found in halo #%d", i);
      printf("\n             => abort()\n\n");
      abort();
   }

   if (stat_halo.StatLightProfile.size() == 0) {
      printf("\n====> ERROR: stat_set_parjeans() in janalysis.cc");
      printf("\n             No light profile found in halo #%d", i);
      printf("\n             => abort()\n\n");
      abort();
   }

   if (stat_halo.StatAnisoProfile.size() == 0) {
      printf("\n====> ERROR: stat_set_parjeans() in janalysis.cc");
      printf("\n             No anisotropy profile found in halo #%d", i);
      printf("\n             => abort()\n\n");
      abort();
   }

   double eps = 0.01; // These values are arbitraty. They have to be modified after the call of the function.
   double R = 1.;
   double Rmax = stat_halo.StatParam[2][i];

   // Fill parameters from stat_halo
   par_jeans[0]  = stat_halo.StatParam[0][i];
   par_jeans[1]  = stat_halo.StatParam[1][i];
   par_jeans[2]  = stat_halo.StatParam[3][i];
   par_jeans[3]  = stat_halo.StatParam[4][i];
   par_jeans[4]  = stat_halo.StatParam[5][i];
   par_jeans[5]  = stat_halo.StatHaloProfile[i];
   par_jeans[6]  = Rmax;
   par_jeans[7]  = stat_halo.StatParam[6][i];
   par_jeans[8]  = stat_halo.StatParam[7][i];
   par_jeans[9]  = stat_halo.StatParam[8][i];
   par_jeans[10]  = stat_halo.StatParam[9][i];
   par_jeans[11]  = stat_halo.StatParam[10][i];
   par_jeans[12]  = stat_halo.StatLightProfile[i];
   par_jeans[13]  = R;
   par_jeans[14]  = eps;
   par_jeans[15]  = stat_halo.StatParam[11][i];
   par_jeans[16]  = stat_halo.StatParam[12][i];
   par_jeans[17]  = stat_halo.StatParam[13][i];
   par_jeans[18]  = stat_halo.StatParam[14][i];
   par_jeans[19]  = stat_halo.StatAnisoProfile[i];
}

//______________________________________________________________________________
void stat_set_parlight(int i, double par_light[8], struct gStructHalo &stat_halo)
{
   //--- Sets the content of par_light from the i-th statistical halo from stat_halo/
   //
   // INPUTS:
   //  i              Index of the halo to use
   //  stat_halo      gStructHalo containing a vector of halo parameters (filled with stat_load_list())
   // OUTPUTS:
   //  par_light[0]   Light profile normalisation [Lsol]
   //  par_light{1]   Light profile scale radius [kpc]
   //  par_light[2]   Light profile shape parameter #1
   //  par_light[3]   Light profile shape parameter #2
   //  par_light[4]   Light profile shape parameter #3
   //  par_light[5]   Light profile card_profile [gENUM_LIGHTPROFILE]
   //  par_light[6]   Light profile maximum radius of integration [kpc]     [UNUSED if I(R) and nu(r) analytical]
   //  par_light[7]   Relative precision for the integration [UNUSED if I(R) and nu(r) analytical]


   if (i < 0 || i >= (int)((stat_halo.StatParam[6]).size())) {
      printf("\n====> ERROR: stat_set_parlight() in janalysis.cc");
      printf("\n             Index i=%d is out of range", i);
      printf("\n             => abort()\n\n");
      abort();
   }

   if (stat_halo.StatLightProfile.size() == 0) {
      printf("\n====> ERROR: stat_set_parlight() in janalysis.cc");
      printf("\n             No light profile found in halo #%d", i);
      printf("\n             => abort()\n\n");
      abort();
   }

   // Fill parameters from stat_halo
   par_light[0] = stat_halo.StatParam[6][i];
   par_light[1] = stat_halo.StatParam[7][i];
   par_light[2] = stat_halo.StatParam[8][i];
   par_light[3] = stat_halo.StatParam[9][i];
   par_light[4] = stat_halo.StatParam[10][i];
   par_light[5] = stat_halo.StatLightProfile[i];
   par_light[6] = stat_halo.StatParam[2][i]; // temporary value of maximum integration radius - will be modified after the call of stat_set_parlight
   par_light[7] = gSIM_EPS;
}

//______________________________________________________________________________
void stat_set_partot(int i, double par_tot[10], struct gStructHalo &stat_halo,
                     bool is_halo_set_to_0_0)
{
   //--- Sets the content of array par from the i-th statistical halo from stat_halo.
   //
   // INPUTS:
   //  i              Index of the halo to use
   //  stat_halo      gStructHalo structure containing a vector of halo parameters (filled with stat_load_list())
   //  is_halo_set_to_0_0  If true, sets the halo to (theta,psi)=(0,0)
   //                 [used for most calculations, avoids coord. issues if |b_halo|~PI/2]
   // OUTPUTS:
   //  par_tot[0]     rho_tot halo normalisation [Msol kpc^{-3}]
   //  par_tot[1]     rho_tot halo scale radius [kpc]
   //  par_tot[2]     rho_tot halo shape parameter #1
   //  par_tot[3]     rho_tot halo shape parameter #2
   //  par_tot[4]     rho_tot halo shape parameter #3
   //  par_tot[5]     rho_tot halo card_profile [gENUM_PROFILE]
   //  par_tot[6]     rho_tot halo radius [kpc]
   //  par_tot[7]     l_HC: distance observer to halo centre [kpc]
   //  par_tot[8]     psi_HC: longitude of halo centre [rad]
   //  par_tot[9]     theta_HC: latitude of halo centre [rad]


   if (i < 0 || i >= (int)((stat_halo.StatParam[0]).size())) {
      printf("\n====> ERROR: stat_set_partot() in janalysis.cc");
      printf("\n             Index i=%d is out of range", i);
      printf("\n             => abort()\n\n");
      abort();
   }

   double psi_halo = 0.;
   double theta_halo = 0.;
   // N.B.: if is_halo_set_to_0_0=true, the halo is set to (d_cl,0,0)
   //       to avoid coordinates problems in integrations if ever
   //       |b_halo|~PI/2" << endl;
   if (!is_halo_set_to_0_0) {
      psi_halo = stat_halo.PsiDeg * DEG_to_RAD;
      check_psi(psi_halo);
      theta_halo = stat_halo.ThetaDeg * DEG_to_RAD;
   }

   if (stat_halo.StatHaloProfile.size() == 0) {
      printf("\n====> ERROR: stat_set_partot() in janalysis.cc");
      printf("\n             No DM profile found in halo #%d", i);
      printf("\n             => abort()\n\n");
      abort();
   }

   // Fill parameters from stat_halo
   par_tot[0] = stat_halo.StatParam[0][i];
   par_tot[1] = stat_halo.StatParam[1][i];
   par_tot[2] = stat_halo.StatParam[3][i];
   par_tot[3] = stat_halo.StatParam[4][i];
   par_tot[4] = stat_halo.StatParam[5][i];
   par_tot[5] = stat_halo.StatHaloProfile[i];
   par_tot[6] = stat_halo.StatParam[2][i];
   par_tot[7] = stat_halo.l;
   par_tot[8] = psi_halo;
   par_tot[9] = theta_halo;

}

//______________________________________________________________________________
string units_or_canvasname_1D(int switch_y, int switch_name, bool is_norm)
{
   //--- Returns units (or canvas names) for ROOT 1D plots.
   //
   //  switch_y       Quantity selected for 1D display
   //                    0 => rho(r_kpc)
   //                    1 => J(alphaint_deg)
   //                    2 => J(theta_deg)
   //                    3 => M(r_kpc)
   //  switch_name    Name returned
   //                    0 => root canvas
   //                    1 => root x title
   //                    2 => root y title
   //  is_norm        Whether y returned is normalised or not

   // Special case of mass
   if (switch_y == 3) {
      if (switch_name == 0)
         return "Mr";
      else if (switch_name == 1)
         return "r [kpc]";
      else if (switch_name == 2)
         return "M_{tot}(r) [M_{#odot}]";
      else {
         printf("\n====> ERROR: units_or_canvasname_1D() in janalysis.cc");
         printf("\n             switch_name=%d is not a valid option.", switch_name);
         printf("\n             => abort()\n\n");
         abort();
      }
   }


   string cvs_name[2][3] = {
      {"rhor", "Dalphaint", "Dtheta"},
      {"rhor", "Jalphaint", "Jtheta"}
   };

   string x_name[3] = {"r [kpc]", "#alpha_{int} [deg]", "#theta [deg]"};

   string y_name[2][2][3] = {
      {  {"#rho(r) [GeV cm^{-3}]", "D(#alpha_{int}) [GeV cm^{-2}]", "D(#theta) [GeV cm^{-2}]"},
         {"#rho(r) [GeV cm^{-3}]", "J(#alpha_{int}) [GeV^{2} cm^{-5}]", "J(#theta) [GeV^{2} cm^{-5}]"}
      },
      {  {"#rho(r) [M_{#odot} kpc^{-3}]", "D(#alpha_{int}) [M_{#odot} kpc^{-2}]", "D(#theta) [M_{#odot} kpc^{-2}]"},
         {"#rho(r) [M_{#odot} kpc^{-3}]", "J(#alpha_{int}) [M_{#odot}^{2} kpc^{-5}]", "J(#theta) [M_{#odot}^{2} kpc^{-5}]"}
      }
   };

   string y_name_norm[2][3] = {
      {"Normalised #rho(r)", "D(#alpha_{int})/D(#alpha_{int}^{max})", "D(#theta)/D(#theta=0)"},
      {"Normalised #rho(r)", "J(#alpha_{int})/J(#alpha_{int}^{max})", "J(#theta)/J(#theta=0)"}
   };


   if (switch_y < 0 || switch_y > 3) {
      printf("\n====> ERROR: units_or_canvasname_1D() in janalysis.cc");
      printf("\n             swith_y_1D=%d is not a valid option.", switch_y);
      printf("\n             => abort()\n\n");
      abort();
   }

   // Returns name
   if (switch_name == 0)  // If canvas root name
      return cvs_name[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y];
   else if (switch_name == 1) // If x axis
      return x_name[switch_y];
   else if (switch_name == 2) { // y axis
      if (is_norm)
         return y_name_norm[gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y];
      else
         return y_name[gSIM_IS_ASTRO_OR_PP_UNITS][gPP_DM_IS_ANNIHIL_OR_DECAY][switch_y];
   } else {
      printf("\n====> ERROR: units_or_canvasname_1D() in janalysis.cc");
      printf("\n             switch_name=%d is not a valid option.", switch_name);
      printf("\n             => abort()\n\n");
      abort();
   }
}
