/*! \file clumps.cc \brief (see clumps.h) */

// CLUMPY includes
#include "../include/clumps.h"
#include "../include/cosmo.h"
#include "../include/geometry.h"
#include "../include/inlines.h"
#include "../include/integr.h"
#include "../include/integr_los.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/profiles.h"
#include "../include/stat.h"
#include "../include/janalysis.h"

// GSL includes
#include <gsl/gsl_sf_hyperg.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_sf_gamma.h>

// C++ std libraries
using namespace std;
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

// "clumps.cc"-specific precomputed vectors for
// Ludlow c(nu(M)) and Sanchez c(sigma(M)) relations:
vector<double> vec_m200;
vector< vector<double> > matrix_nu;
vector< vector<double> > matrix_sigma2;

//______________________________________________________________________________
double card_cdelta_to_Delta(int card_cdelta, double const &z)
{
   //--- Returns the Delta_c(z) which corresponds to a given card_cdelta implemented in the code
   //
   //  card_cdelta     cdelta-mdelta choice of parameterisation (from gENUM_CDELTAMDELTA)
   //  z               Redshift of the halo

   switch (card_cdelta) {
      // Cases using Deltavir according to Bryan and Norman (1998)
      case kB01_VIR:
      case kB01_VIR_RAD:
      case kENS01_VIR:
      case kDUFFY08F_VIR:
      case kGIOCOLI12_VIR:
      case kROCHA13_SIDM_VIR:
         return delta_x_to_delta_crit(-1, kBRYANNORMAN98, z);
      // Case using variables M200, c200 and Delta_c=200
      case kNETO07_200:
      case kDUFFY08F_200:
      case kETTORI10_200:
      case kPRADA12_200:
      case kPIERI11_VIALACTEA:
      case kPIERI11_AQUARIUS:
      case kSANCHEZ14_200:
      case kMOLINE17_200:
      case kLUDLOW16_200:
      case kCORREA15_PLANCK_200:
         return 200.;
      case kDUFFY08F_MEAN:
         return delta_x_to_delta_crit(200, kRHO_MEAN, z);
      default:
         printf("\n====> ERROR: card_cdelta_to_Delta() in clumps.cc");
         printf("\n             card_cdelta chosen does not correspond to any concentration model");
         printf("\n             => abort()\n\n");
         abort();
   }
   return 0.;
}

//______________________________________________________________________________
double card_mf_to_Delta(int card_mf)
{
   //--- Returns the Delta_c(z) which corresponds to a given mass function implemented in the code
   //
   //  card_mf         choice of mass function (from gENUM_MASSFUNCTION)


   switch (card_mf) {
      // Case using M200
      case kTINKER08:
      case kTINKER08_N:
      case kTINKER10:
      case kBOCQUET16_HYDRO:
      case kBOCQUET16_DMONLY:
      case kJENKINS01:
      case kSHETHTORMEN99:
      case kPRESSSCHECHTER74:
         return 200.;
      default:
         printf("\n====> ERROR: card_mf_to_Delta() in clumps.cc");
         printf("\n             card_mf chosen does not correspond to any mass function model");
         printf("\n             => abort()\n\n");
         abort();
   }
   return 0.;
}

//______________________________________________________________________________
double delta_x_to_delta_crit(double const &Delta_x, int const card_delta_ref, double const &z)
{
   //--- Returns Delta_crit for different definitions of the overdensity and overdensity reference,
   //    which might be redshift-dependent, Delta_x = Delta_x(z)
   //    Throughout the code, it is then used mdelta = 4pi/3 R_Delta^3 x Delta_crit x rho_crit(z),
   //    i.e., Delta given by this function is always defined w.r.t the critical density.
   //    Different authors use different parametrisations of Delta_x(z):
   //       - Eke et al. (2001): Delta_vir = 178*pow(omega_m, 0.45);
   //       - Bryan & Norman (1998): Delta_vir = (18 PI^2 + 82x - 39x^2) with x= omega_m-1;
   //       - Giocoli et al. (2010): Delta_vir = 9PI^2 * (1+omega_m^0.4403 - 0.7076*(1-omega_m)).
   //
   // INPUTS:
   //  Delta_x         Delta_x at redshift z. If set to a negative value,
   //                  Delta is set to the value contained in the model, if applicable.
   //  card_delta_ref  Delta reference model
   //  z               Redshift
   // OUTPUT:
   //  Delta_crit      Overdensity factor w.r.t. the critical density (redshift-independent).

   // shortcut for z=0:
   if (z < 1e-5 and Delta_x > 0) {
      if (card_delta_ref == kRHO_MEAN) return Delta_x * Omega_m(0);
      else return Delta_x;
   }

   switch (card_delta_ref) {
      case kRHO_CRIT: {
            if (Delta_x < 0) {
               printf("\n====> ERROR: delta_x_to_delta_crit() in clumps.cc");
               printf("\n             Negative Delta0 (-1 flag) only allowed for kBRYANNORMAN98 model.");
               printf("\n             => abort()\n\n");
               abort();
            }
            return Delta_x;
         }
      case kRHO_MEAN: {
            if (Delta_x < 0) {
               printf("\n====> ERROR: delta_x_to_delta_crit() in clumps.cc");
               printf("\n             Negative Delta0 (-1 flag) only allowed for kBRYANNORMAN98 models");
               printf("\n             => abort()\n\n");
               abort();
            }
            return Delta_x * Omega_m(z);
         }
      case kBRYANNORMAN98: { // Bryan & Norman (1998). Their formula is here generalized to allow an arbitrary Delta0.
            double x = Omega_m(z) - 1.;
            double res;
            if (fabs(gCOSMO_OMEGA0_K) < SMALL_NUMBER) {
               res = (18.* PI * PI + 82.*x - 39.*x * x);
               if (Delta_x > 0) {
                  double x0 = Omega_m(0) - 1.;
                  double norm = 18.* PI * PI + 82.*x0 - 39.*x0 * x0;
                  res *= (Delta_x / norm);
               }
            } else if (fabs(gCOSMO_OMEGA0_LAMBDA) < SMALL_NUMBER) {
               res = 18.*PI * PI + 60.*x - 32.*x * x;
               if (Delta_x > 0) {
                  double x0 = Omega_m(0) - 1.;
                  double norm = (18.* PI * PI + 60.* x0 - 32.* x0 * x0);
                  res *= (Delta_x / norm);
               }
            } else {
               printf("\n====> ERROR: delta_x_to_delta_crit() in clumps.cc");
               printf("\n             kBRYANNORMAN98 model only defined for gCOSMO_OMEGA0_LAMBDA + gCOSMO_OMEGA0_M = 1 or gCOSMO_OMEGA0_LAMBDA = 0.");
               printf("\n             => abort()\n\n");
               abort();
            }
            return res;
         }

      default:
         printf("\n====> ERROR: delta_x_to_delta_crit() in clumps.cc");
         printf("\n             card_delta_ref chosen does not correspond to any Delta reference model");
         printf("\n             => abort()\n\n");
         abort();
   }
   return 0.;
}

//______________________________________________________________________________
double delta_crit_to_delta_x(double const &Delta_c, int const card_delta_ref, double const &z)
{
   //--- Returns Delta_x(z) of different definitions of the overdensity and overdensity reference.
   //    Inverse function of delta_x_to_delta_crit
   //
   // INPUTS:
   //  Delta_c         Overdensity factor Delta_crit.
   //  card_delta_ref  Delta reference model
   //  z               Redshift
   // OUTPUT:
   //  Delta_x(z)    Overdensity factor at redshift z, w.r.t. x.

   if (Delta_c < 0) {
      printf("\n====> ERROR: delta_crit_to_delta_x() in clumps.cc");
      printf("\n             Negative Delta not allowed.");
      printf("\n             => abort()\n\n");
      abort();
   }


   // shortcut for z=0:
   if (z < 1e-5) {
      if (card_delta_ref == kRHO_MEAN) return Delta_c / Omega_m(0);
      else return Delta_c;
   }

   switch (card_delta_ref) {
      case kRHO_CRIT: {
            return Delta_c;
         }
      case kRHO_MEAN: {
            return Delta_c / Omega_m(z);
         }
      case kBRYANNORMAN98: { // Bryan & Norman (1998). Their formula is here generalized to allow an arbitrary Delta0.
            double x = Omega_m(z) - 1.;
            double x0 = Omega_m(0) - 1.;
            double res;
            if (fabs(gCOSMO_OMEGA0_K) < SMALL_NUMBER) {
               double norm = 18.* PI * PI + 82.*x0 - 39.*x0 * x0;
               res = norm / (18.* PI * PI + 82.*x - 39.*x * x) * Delta_c;
            } else if (fabs(gCOSMO_OMEGA0_LAMBDA) < SMALL_NUMBER) {
               double norm = (18.* PI * PI + 60.* x0 - 32.* x0 * x0);
               res = norm / (18.*PI * PI + 60.*x - 32.*x * x) * Delta_c;
            } else {
               printf("\n====> ERROR: delta_crit_to_delta_x() in clumps.cc");
               printf("\n             kBRYANNORMAN98 model only defined for gCOSMO_OMEGA0_LAMBDA + gCOSMO_OMEGA0_M = 1 or gCOSMO_OMEGA0_LAMBDA = 0.");
               printf("\n             => abort()\n\n");
               abort();
            }
            return res;
         }

      default:
         printf("\n====> ERROR: delta_crit_to_delta_x() in clumps.cc");
         printf("\n             card_delta_ref chosen not implemented or does not correspond to any Delta reference model");
         printf("\n             => abort()\n\n");
         abort();
   }
   return 0.;
}

//______________________________________________________________________________
double dpdlogc(double *log_c, double par_dpdc[3])
{
   //--- Logarithmic concentration distribution dP/dLogc (M,c) = c * dP/dc (M,c).
   //    N.B.: the use of this function in the code assumes that dpdlogc is
   //    properly normalised, i.e. it is a pdf (int_0^infty dp/dc dc = 1)
   //    for any input mass M.
   //
   //  log_c[0]      log10(c), c is the concentration
   //  par_dpdc[0]   dPdc mean concentration
   //  par_dpdc[1]   dPdc standard deviation
   //  par_dpdc[2]   dPdc card_profile [gENUM_CVIR_DIST]

   double c = pow(10., log_c[0]);
   return c * dpdc(&c, par_dpdc) / 0.4342944819;  // log10(exp(1)) = 0.434...
}

//______________________________________________________________________________
void dpdlogc(double &log_c, double par_dpdc[3], double &res)
{
   //--- Sets res = log. concentration distribution dP/dLogc (M,c) = c * dP/dc (M,c).
   //    N.B.: the use of this function in the code assumes that dpdlogc is
   //    properly normalised, i.e. it is a pdf (int_0^infty dp/dc dc = 1)
   //    for any input mass M.
   //
   // INPUTS:
   //  log_c[0]      log10(c), c is the concentration
   //  par_dpdc[0]   dPdc mean concentration
   //  par_dpdc[1]   dPdc standard deviation
   //  par_dpdc[2]   dPdc card_profile [gENUM_CVIR_DIST]
   // OUTPUT:
   //  res           c * dP/dc (M,c)

   res = dpdlogc(&log_c, par_dpdc);
}

//______________________________________________________________________________
double dpdc(double *c, double par_dpdc[3])
{
   //--- Returns the logarithmic concentration distribution dP/dc(M,c).
   //    N.B.: the use of this function in the code assumes that dpdc is
   //    properly normalised, i.e. it is a pdf (int_0^infty dp/dc dc = 1)
   //    for any input mass M.
   //
   //  c[0]          c is the concentration
   //  par_dpdc[0]   dPdc mean concentration
   //  par_dpdc[1]   dPdc standard deviation
   //  par_dpdc[2]   dPdc card_profile [gENUM_CVIR_DIST]

   switch ((int) par_dpdc[2]) {
      case kLOGNORM:
         return 1. / (sqrt(2.*PI) * par_dpdc[1] * c[0])
                * exp(-pow(log(c[0]) - log(par_dpdc[0]), 2)
                      / (2.*par_dpdc[1] * par_dpdc[1]));
      case kDIRAC:
         if (abs(c[0] - par_dpdc[0]) < 1.e-10)
            return 1.;
         else
            return 0.;
      default:
         printf("\n====> ERROR: dpdc() in clumps.cc");
         printf("\n             card_cvir = %d chosen does not correspond to any concentration model", int(par_dpdc[2]));
         printf("\n             => abort()\n\n");
         abort();
   }
   return 0.;
}

//______________________________________________________________________________
void dpdc(double &c, double par_dpdc[3], double &res)
{
   //--- Calculates and sets res = concentration distribution dP/dc (M,c).
   //    N.B.: the use of this function in the code assumes that dpdlogc is
   //    properly normalised, i.e. it is a pdf (int_0^infty dp/dc dc = 1)
   //    for any input mass M.
   //
   // INPUTS:
   //  c             c is the concentration
   //  par_dpdc[0]   dPdc mean concentration
   //  par_dpdc[1]   dPdc standard deviation
   //  par_dpdc[2]   dPdc card_profile [gENUM_CVIR_DIST]
   // OUTPUT:
   //  res           dP/dc (M,c)

   res = dpdc(&c, par_dpdc);
}

//______________________________________________________________________________
void dpdc_lum(double &c, double par_subs[25], double &res)
{
   //--- Sets res = L(R,m,c) * dP/dc (c,R), with L(R,m,c) calculated for nlevel of
   //    substructures (described by the concentration dPdc, mass dPdM, and
   //    spatial dPdV distributions), each level having the same properties
   //    and a mass fraction f (in substructures).
   //
   // INPUTS:
   //  c             cvir: concentration=Rvir/r_2 (and not cDelta!)
   //  par_subs[0]   rho_cl(r) shape parameter #1 (for all clumps)
   //  par_subs[1]   rho_cl(r) shape parameter #2 (for all clumps)
   //  par_subs[2]   rho_cl(r) shape parameter #3 (for all clumps)
   //  par_subs[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]   host halo outer radius where to stop integration [kpc]
   //  par_subs[5]   eps: relative precision sought L calculation
   //  par_subs[6]   z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]   mdelta: mass of rho_cl(r)
   //  par_subs[8]   dPdM normalisation
   //  par_subs[9]   dPdM slope alphaM
   //  par_subs[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_subs[11]  dPdc mean concentration <c>
   //  par_subs[12]  dPdc standard deviation             [UNUSED if gENUM_CVIR_DIST=kDIRAC]
   //  par_subs[13]  dPdc card_profile [gENUM_CVIR_DIST] [UNUSED if gENUM_CVIR_DIST=kDIRAC]
   //  par_subs[14]  f: mass fraction of substructures   [only for ANNIHILATION and nlevel>1]
   //  par_subs[15]  nlevel: sub-sub...halos (1=no sub)  [only for ANNIHILATION and nlevel>1]
   //  par_subs[16]  dPdV normalisation  [kpc^{-3}]      [only for ANNIHILATION and nlevel>1]
   //  par_subs[17]  dPdV scale radius [kpc]             [only for ANNIHILATION and nlevel>1]
   //  par_subs[18]  dPdV shape parameter #1             [only for ANNIHILATION and nlevel>1]
   //  par_subs[19]  dPdV shape parameter #2             [only for ANNIHILATION and nlevel>1]
   //  par_subs[20]  dPdV shape parameter #3             [only for ANNIHILATION and nlevel>1]
   //  par_subs[21]  dPdV card_profile [gENUM_PROFILE]   [only for ANNIHILATION and nlevel>1]
   //  par_subs[22]  host halo outer radius (unused)     [only for ANNIHILATION and nlevel>1]
   //  par_subs[23]  ratio rs dPdV to rs_cl              [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]  R_host (corresp. to current dPdV)   [only for ANNIHILATION and nlevel>1]
   // OUPUT:
   //  res           L(M,c) * dP/dc (c): annihilation [Msol^2/kpc^3], decay [Msol]


   // dPdc(c) for given dPdc parameters
   // N.B.: if distribution is kDIRAC, set dpdc_val=1
   // so that this function returns L(M,<c>), which
   double dpdc_val = 1.;
   if ((int)par_subs[13] != kDIRAC)
      dpdc_val = dpdc(&c, &par_subs[11]);
   else
      c = -1; // Force recalculation of c below

   // L is calculated for a host mdelta and cdelta (not for <c>)!
   // We also need par_host[25]:
   //   par_host[0]   rho_cl(r) normalisation [Msol/kpc^{-3}]
   //   par_host[1]   rho_cl(r) scale radius [kpc]
   //   par_host[i>1] = par_subs[i-2]
   // We allocate and fill par_host[0-7] by means of mdelta_to_par()
   // N.B.: this is almost the same code as shortcut in mean1cl_lumn_r_m()
   const int n_par = 26;
   double par_host[n_par];
   for (int i = 0; i < n_par - 2; ++i)
      par_host[i + 2] = par_subs[i];

   double xpos = par_subs[24] / par_subs[4];
   double Delta_c = delta_x_to_delta_crit(gCOSMO_DELTA0, gCOSMO_FLAG_DELTA_REF, par_subs[6]);

   mdelta_to_par(par_host, par_host[9] /*mass*/, Delta_c, (int)par_host[12] /*card cdelta-mdelta*/,
                 &par_host[2] /* shape params */, par_host[7] /*EPS */, par_host[8] /*redshift*/,
                 c, xpos);
   // update r_s of dPdV proportional to r_s of host:
   par_host[19] = par_host[25] * par_host[1];

   if (par_host[23] == kDPDV_PIERI11 and par_host[16] > SMALL_NUMBER) {
      // update r_s and shape parameter:
      gsl_function F;
      F.function = &solve_rbias;
      rbias_params_for_rootfinding params = {par_host[9], par_host[6], par_host[0], par_host[1],
                                             par_host[2], par_host[3], par_host[4], int(par_host[5]), par_host[16]
                                            };
      F.params = &params;
      double rmin = 1e-5;
      double rmax = 1e5;
      int return_status = 0;
      double rbias = rootsolver_gsl(gsl_root_fsolver_falsepos, F, rmin, rmax, gSIM_EPS, return_status); // faster than gsl_root_fsolver_brent and gsl_root_fsolver_bisection
      par_host[19] = par_host[1];
      par_host[20] = rbias;
      par_host[21] = 0;
      par_host[22] = 0;
   }

   // Calculate L(M,c)*dPdc
   // If decay, or one-level subs, lum with no_subs
   if (!gPP_DM_IS_ANNIHIL_OR_DECAY || (int)par_host[17] == 1)
      res = dpdc_val * lum_singlehalo_nosubs(par_host, par_host[7]/*eps*/);
   else
      res = dpdc_val * lum_singlehalo_subs(par_host);
}

//__________________________________________________________________________
void dpdc_lum2(double &c, double par_subs[25], double &res)
{
   //--- Sets res = L^2(R,m,c) * dP/dc (c,R), with L(R,m,c) calculated for nlevel
   //    of substructures (described by the concentration dPdc, mass dPdM,
   //    and spatial dPdV distributions), each level having the same
   //    properties and a mass fraction f (in substructures).
   //
   //  c             cvir: concentration=Rvir/r_2 (and not cDelta!)
   //  par_subs[0]   rho_cl(r) shape parameter #1 (for all clumps)
   //  par_subs[1]   rho_cl(r) shape parameter #2 (for all clumps)
   //  par_subs[2]   rho_cl(r) shape parameter #3 (for all clumps)
   //  par_subs[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]   host halo outer radius where to stop integration [kpc]
   //  par_subs[5]   eps: relative precision sought L calculation
   //  par_subs[6]   z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]   mdelta: mass of rho_cl(r)
   //  par_subs[8]   dPdM normalisation
   //  par_subs[9]   dPdM slope alphaM
   //  par_subs[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_subs[11]  dPdc mean concentration <c>
   //  par_subs[12]  dPdc standard deviation             [UNUSED if gENUM_CVIR_DIST=kDIRAC]
   //  par_subs[13]  dPdc card_profile [gENUM_CVIR_DIST] [UNUSED if gENUM_CVIR_DIST=kDIRAC]
   //  par_subs[14]  f: mass fraction of substructures   [only for ANNIHILATION and nlevel>1]
   //  par_subs[15]  nlevel: sub-sub...halos (1=no sub)  [only for ANNIHILATION and nlevel>1]
   //  par_subs[16]  dPdV normalisation  [kpc^{-3}]      [only for ANNIHILATION and nlevel>1]
   //  par_subs[17]  dPdV scale radius [kpc]             [only for ANNIHILATION and nlevel>1]
   //  par_subs[18]  dPdV shape parameter #1             [only for ANNIHILATION and nlevel>1]
   //  par_subs[19]  dPdV shape parameter #2             [only for ANNIHILATION and nlevel>1]
   //  par_subs[20]  dPdV shape parameter #3             [only for ANNIHILATION and nlevel>1]
   //  par_subs[21]  dPdV card_profile [gENUM_PROFILE]   [only for ANNIHILATION and nlevel>1]
   //  par_subs[22]  host halo outer radius (unused)     [only for ANNIHILATION and nlevel>1]
   //  par_subs[23]  ratio rs dPdV to rs_cl              [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]  R_host (corresp. to current dPdV)   [only for ANNIHILATION and nlevel>1]

   // OUPUT:
   //  res           L^2(M,c) * dP/dc (c): annihilation [Msol^4/kpc^6], decay [Msol^2]


   // dPdc(c) for given dPdc parameters
   // N.B.: if distribution is kDIRAC, set dpdc_val=1
   // so that this function returns L(M,<c>), which
   double dpdc_val = 1.;
   if ((int)par_subs[13] != kDIRAC)
      dpdc_val = dpdc(&c, &par_subs[11]);
   else
      c = -1; // Force recalculation of c

   // L is calculated for a host mdelta and cdelta (not for <c>)!
   // We also need par_host[25]:
   //   par_host[0]   rho_cl(r) normalisation [Msol/kpc^{-3}]
   //   par_host[1]   rho_cl(r) scale radius [kpc]
   //   par_host[i>1] = par_subs[i-2]
   // We allocate and fill par_host[0-7] by means of mdelta_to_par()
   // N.B.: this is almost the same code as shortcut in mean1cl_lumn_r_m()
   const int n_par = 26;
   double par_host[n_par];
   for (int i = 0; i < n_par - 2; ++i)
      par_host[i + 2] = par_subs[i];

   double xpos = par_subs[24] / par_subs[4];
   double Delta_c = delta_x_to_delta_crit(gCOSMO_DELTA0, gCOSMO_FLAG_DELTA_REF, par_subs[6]);

   mdelta_to_par(par_host, par_host[9] /*mass*/, Delta_c, (int)par_host[12] /*card cdelta-mdelta*/,
                 &par_host[2] /* shape params */, par_host[7] /*EPS */, par_host[8] /*redshift*/,
                 c, xpos);
   // update r_s of dPdV proportional to r_s of host:
   par_host[19] = par_host[25] * par_host[1];

   if (par_host[23] == kDPDV_PIERI11 and par_host[16] > SMALL_NUMBER) {
      // update r_s and shape parameter:
      gsl_function F;
      F.function = &solve_rbias;
      rbias_params_for_rootfinding params = {par_host[9], par_host[6], par_host[0], par_host[1],
                                             par_host[2], par_host[3], par_host[4], int(par_host[5]), par_host[16]
                                            };
      F.params = &params;
      double rmin = 1e-5;
      double rmax = 1e5;
      int return_status = 0;
      double rbias = rootsolver_gsl(gsl_root_fsolver_falsepos, F, rmin, rmax, gSIM_EPS, return_status); // faster than gsl_root_fsolver_brent and gsl_root_fsolver_bisection
      par_host[19] = par_host[1];
      par_host[20] = rbias;
      par_host[21] = 0;
      par_host[22] = 0;
   }

   // Calculate L(M,c)*dPdc
   double lum = 0.;
   // If decay, or one-level subs, lum with no_subs
   if (!gPP_DM_IS_ANNIHIL_OR_DECAY || (int)par_host[17] == 1)
      lum = lum_singlehalo_nosubs(par_host, par_host[7]/*eps*/);
   else
      lum = lum_singlehalo_subs(par_host);

   res = dpdc_val * lum * lum;
}

//______________________________________________________________________________
double dpdlogm(double *log_m, double par_dpdm[2])
{
   //--- Returns logarithmic mass distribution dP/dLogM (M) = M * dP/dM (M).
   //
   //  log_m[0]      log10(M/Msol), M is the clump mass
   //  par_dpdm[0]   dPdM normalisation [1/Msol]
   //  par_dpdm[1]   dPdM slope alphaM

   double m = pow(10., log_m[0]);
   return m * dpdm(&m, par_dpdm) / 0.4342944819;  // log10(exp(1)) = 0.434...
}

//______________________________________________________________________________
void dpdlogm(double &log_m, double par_dpdm[2], double &res)
{
   //--- Sets res = logarithmic mass distribution dP/dLogM (M) = M * dP/dM (M).
   //
   // INPUTS:
   //  log_m[0]      log10(M/Msol), M is the clump mass
   //  par_dpdm[0]   dPdM normalisation [1/Msol]
   //  par_dpdm[1]   dPdM slope alphaM
   // OUTPUT:
   //  res           M * dP/dM (M)  [1]

   res = dpdlogm(&log_m, par_dpdm);
}

//______________________________________________________________________________
double dpdm(double *m, double par_dpdm[2])
{
   //--- Returns mass PDF dP/dM(M) = B * M^{-alphaM}   [1/Msol].
   //
   //  m[0]          Mass of the clump [Msol]
   //  par_dpdm[0]   dPdM normalisation [1/Msol]
   //  par_dpdm[1]   dPdM slope alphaM

   return par_dpdm[0] * pow(m[0], -fabs(par_dpdm[1]));
}

//______________________________________________________________________________
void dpdm(double &m, double par_dpdm[2], double &res)
{
   //--- Sets res = mass distribution dP/dM (M) = B * M^{-alphaM}.
   //
   // INPUTS:
   //  m[0]          Mass of the clump [Msol]
   //  par_dpdm[0]   dPdM normalisation [1/Msol]
   //  par_dpdm[1]   dPdM slope alphaM
   // OUTPUT:
   //  res           dP/dM (M)  [1/Msol]

   res = dpdm(&m, par_dpdm);
}

//______________________________________________________________________________
void dpdm_lum_r_m(double &m, double par_subs[25], double &res)
{
   //--- Sets res = dP/dM (M) * <L(R,M)>_c = dP/dM (M) * \int_{cmin}^{cmax} dpdc_lum.
   //    N.B.: L quantities are calculated for nlevel of substructures (described
   //    by the concentration dPdc, mass dPdM, and spatial dPdV distributions),
   //    each level having the same properties and a mass fraction f (in substructures).
   //
   //  m             mdelta: virial mass of halo [Msol]
   //  par_subs[0]   rho_cl(r) shape parameter #1 (for all clumps)
   //  par_subs[1]   rho_cl(r) shape parameter #2 (for all clumps)
   //  par_subs[2]   rho_cl(r) shape parameter #3 (for all clumps)
   //  par_subs[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]   host halo outer radius where to stop integration [kpc]
   //  par_subs[5]   eps: relative precision sought L calculation
   //  par_subs[6]   z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]   mdelta: mass of rho_cl(r)
   //  par_subs[8]   dPdM normalisation
   //  par_subs[9]   dPdM slope alphaM
   //  par_subs[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_subs[11]  dPdc mean concentration <c>
   //  par_subs[12]  dPdc standard deviation             [UNUSED if gENUM_CVIR_DIST=kDIRAC]
   //  par_subs[13]  dPdc card_profile [gENUM_CVIR_DIST] [UNUSED if gENUM_CVIR_DIST=kDIRAC]
   //  par_subs[14]  f: mass fraction of substructures   [only for ANNIHILATION and nlevel>1]
   //  par_subs[15]  nlevel: sub-sub...halos (1=no sub)  [only for ANNIHILATION and nlevel>1]
   //  par_subs[16]  dPdV normalisation  [kpc^{-3}]      [only for ANNIHILATION and nlevel>1]
   //  par_subs[17]  dPdV scale radius [kpc]             [only for ANNIHILATION and nlevel>1]
   //  par_subs[18]  dPdV shape parameter #1             [only for ANNIHILATION and nlevel>1]
   //  par_subs[19]  dPdV shape parameter #2             [only for ANNIHILATION and nlevel>1]
   //  par_subs[20]  dPdV shape parameter #3             [only for ANNIHILATION and nlevel>1]
   //  par_subs[21]  dPdV card_profile [gENUM_PROFILE]   [only for ANNIHILATION and nlevel>1]
   //  par_subs[22]  host halo outer radius (unused)     [only for ANNIHILATION and nlevel>1]
   //  par_subs[23]  ratio rs dPdV to rs_cl              [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]  R_host (corresp. to current dPdV)   [only for ANNIHILATION and nlevel>1]
   // OUTPUT:
   //  res           <L(M)>_c * dP/dM (M): annihilation [Msol/kpc^3], decay [-]

   // Update mdelta
   par_subs[7] = m;

   // Calculate <L>_c*dPdM, with <L>_c=\int_{cmin}^{cmax} dpdc_lum.
   res = mean1cl_lumn_r_m(par_subs, 1) * dpdm(&m, &par_subs[8]);
}

//__________________________________________________________________________
void dpdm_lum2_r_m(double &m, double par_subs[25], double &res)
{
   //--- Sets res = dP/dM (M) * <L^2(R,M)>_c = dP/dM (M) * \int_{cmin}^{cmax} dpdc_lum2.
   //    N.B.: L quantities are calculated for nlevel of substructures (described
   //    by the concentration dPdc, mass dPdM, and spatial dPdV distributions),
   //    each level having the same properties and a mass fraction f (in substructures).
   //
   //  m             mdelta: virial mass of halo [Msol]
   //  par_subs[0]   rho_cl(r) shape parameter #1 (for all clumps)
   //  par_subs[1]   rho_cl(r) shape parameter #2 (for all clumps)
   //  par_subs[2]   rho_cl(r) shape parameter #3 (for all clumps)
   //  par_subs[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]   host halo outer radius where to stop integration [kpc]
   //  par_subs[5]   eps: relative precision sought L calculation
   //  par_subs[6]   z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]   mdelta: mass of rho_cl(r)
   //  par_subs[8]   dPdM normalisation
   //  par_subs[9]   dPdM slope alphaM
   //  par_subs[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_subs[11]  dPdc mean concentration <c>
   //  par_subs[12]  dPdc standard deviation             [UNUSED if gENUM_CVIR_DIST=kDIRAC]
   //  par_subs[13]  dPdc card_profile [gENUM_CVIR_DIST] [UNUSED if gENUM_CVIR_DIST=kDIRAC]
   //  par_subs[14]  f: mass fraction of substructures   [only for ANNIHILATION and nlevel>1]
   //  par_subs[15]  nlevel: sub-sub...halos (1=no sub)  [only for ANNIHILATION and nlevel>1]
   //  par_subs[16]  dPdV normalisation  [kpc^{-3}]      [only for ANNIHILATION and nlevel>1]
   //  par_subs[17]  dPdV scale radius [kpc]             [only for ANNIHILATION and nlevel>1]
   //  par_subs[18]  dPdV shape parameter #1             [only for ANNIHILATION and nlevel>1]
   //  par_subs[19]  dPdV shape parameter #2             [only for ANNIHILATION and nlevel>1]
   //  par_subs[20]  dPdV shape parameter #3             [only for ANNIHILATION and nlevel>1]
   //  par_subs[21]  dPdV card_profile [gENUM_PROFILE]   [only for ANNIHILATION and nlevel>1]
   //  par_subs[22]  host halo outer radius (unused)     [only for ANNIHILATION and nlevel>1]
   //  par_subs[23]  ratio rs dPdV to rs_cl              [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]  R_host (corresp. to current dPdV)   [only for ANNIHILATION and nlevel>1]
   // OUTPUT:
   //  res           <L^2(M)>_c * dP/dM (M): annihilation [Msol^3/kpc^6], decay [Msol]

   // Update mdelta
   par_subs[7] = m;

   // Calculate <L^2>_c*dPdM, with <L^2>_c=\int_{cmin}^{cmax} dpdc_lum2.
   res = mean1cl_lumn_r_m(par_subs, 2) * dpdm(&m, &par_subs[8]);
}

//___________________________________________________________________________
void dpdm_m(double &m, double par_dpdm[2], double &res)
{
   //--- Sets res = M dP/dM (M).
   //
   // INPUTS:
   //  m[0]          Mass of the clump [Msol]
   //  par_dpdm[0]   dPdM normalisation [1/Msol]
   //  par_dpdm[1]   dPdM slope alphaM
   // OUTPUT:
   //  res           M dP/dM (M)  [1]

   res = m * dpdm(&m, par_dpdm);
}

//______________________________________________________________________________
void dpdm_setnormprob(double par_dpdm[2], double &m1, double &m2, double const &eps)
{
   //--- Sets par_dpdm[0], i.e. norm [Msol^{-1}] such as dpdm is
   //    a probability (int_m1^m2 dP/dM dM = 1).
   //
   // INPUTS:
   //  par_dpdm[0]   UNUSED
   //  par_dpdm[1]   dPdM slope alphaM
   //  m1            Minimal mass where dpdm applies [Msol]
   //  m2            Maximal mass where dpdm applies [Msol]
   //  eps           Relative precision sought for integration
   // OUTPUT:
   //  par_dpdm[0]   dPdM normalisation [1/Msol]

   par_dpdm[0] = 1.;

   if (m2 > m1) {
      double res = 0.;
      simpson_log_adapt(dpdm, m1, m2, par_dpdm, res, eps);
      par_dpdm[0] /= res;
   } else
      par_dpdm[0] = 0.;
}

//______________________________________________________________________________
void dpdv_lum_r(double &r, double par[28], double &res)
{
   //--- Sets res = dP/dV(r) * <L(r)>_{m,c} of a clump in the range [m1,m2],
   //    with <L(r,m)>_c =int_c1^c2 (L^2.dP/dc) dc,
   //    the mean squared luminosity of a clump of mass m within the range of
   //    concentration [c1, c2].
   //    N.B.: L quantities are calculated for nlevel of substructures (described
   //    by the concentration dPdc, mass dPdM, and spatial dPdV distributions),
   //    each level having the same properties and a mass fraction f (in substructures).
   //
   //  -- par[0-24] is par_subs:
   //  par[0]   rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par[1]   rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par[2]   rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par[4]   host halo outer radius where to stop integration [kpc]
   //  par[5]   eps: relative precision sought L calculation
   //  par[6]   z: redshift of the halo hosting the sub-clumps
   //  par[7]   mdelta: mass of rho_cl(r)
   //  par[8]   dPdM normalisation [1/Msol]
   //  par[9]   dPdM slope alphaM
   //  par[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par[11]  dPdc mean concentration <c>
   //  par[12]  dPdc standard deviation              [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par[13]  dPdc card_profile [gENUM_CDELTA_DIST]  [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par[14]  f: mass fraction of substructures    [only for ANNIHILATION and nlevel>1]
   //  par[15]  nlevel: sub-sub...halos (1=no sub)   [only for ANNIHILATION and nlevel>1]
   //  par[16]  dPdV normalisation  [kpc^{-3}]       [only for ANNIHILATION and nlevel>1]
   //  par[17]  dPdV scale radius [kpc]              [only for ANNIHILATION and nlevel>1]
   //  par[18]  dPdV shape parameter #1              [only for ANNIHILATION and nlevel>1]
   //  par[19]  dPdV shape parameter #2              [only for ANNIHILATION and nlevel>1]
   //  par[20]  dPdV shape parameter #3              [only for ANNIHILATION and nlevel>1]
   //  par[21]  dPdV card_profile [gENUM_PROFILE]    [only for ANNIHILATION and nlevel>1]
   //  par[22]  host halo outer radius (unused)      [only for ANNIHILATION and nlevel>1]
   //  par[23]  ratio rs dPdV to rs_cl               [only for ANNIHILATION and nlevel>1]
   //  par[24]  R_host (corresp. to current dPdV)    [only for ANNIHILATION and nlevel>1]
   //  -- par[25-27] is for mass integration:
   //  par[25]  m1 lower integration limit [Msol]
   //  par[26]  m2 lower integration limit [Msol]
   //  par[27]  bool to decide if dpdm shall be normalized between m1 and m2

   double dpdv = 0.;
   rho(r, &par[16], dpdv);
   par[24] = r;

   int n_parsubs = 25;
   double par_subs[n_parsubs];
   for (int i = 0; i < n_parsubs; ++i)
      par_subs[i] = par[i];

   res = dpdv * mean1cl_lumn_r(par_subs, par[25], par[26], 1, bool(par[27]));
}

//______________________________________________________________________________
void dpdv_lum2_r(double &r, double par[28], double &res)
{
   //--- Sets res = dP/dV(r) * <L^2(r)>_{m,c} of a clump in the range [m1,m2],
   //    with <L(r,m)>_c =int_c1^c2 (L^2.dP/dc) dc,
   //    the mean squared luminosity of a clump of mass m within the range of
   //    concentration [c1, c2].
   //    N.B.: L quantities are calculated for nlevel of substructures (described
   //    by the concentration dPdc, mass dPdM, and spatial dPdV distributions),
   //    each level having the same properties and a mass fraction f (in substructures).
   //
   //  -- par[0-24] is par_subs:
   //  par[0]   rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par[1]   rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par[2]   rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par[4]   host halo outer radius where to stop integration [kpc]
   //  par[5]   eps: relative precision sought L calculation
   //  par[6]   z: redshift of the halo hosting the sub-clumps
   //  par[7]   mdelta: mass of rho_cl(r)
   //  par[8]   dPdM normalisation [1/Msol]
   //  par[9]   dPdM slope alphaM
   //  par[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par[11]  dPdc mean concentration <c>
   //  par[12]  dPdc standard deviation              [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par[13]  dPdc card_profile [gENUM_CDELTA_DIST]  [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par[14]  f: mass fraction of substructures    [only for ANNIHILATION and nlevel>1]
   //  par[15]  nlevel: sub-sub...halos (1=no sub)   [only for ANNIHILATION and nlevel>1]
   //  par[16]  dPdV normalisation  [kpc^{-3}]       [only for ANNIHILATION and nlevel>1]
   //  par[17]  dPdV scale radius [kpc]              [only for ANNIHILATION and nlevel>1]
   //  par[18]  dPdV shape parameter #1              [only for ANNIHILATION and nlevel>1]
   //  par[19]  dPdV shape parameter #2              [only for ANNIHILATION and nlevel>1]
   //  par[20]  dPdV shape parameter #3              [only for ANNIHILATION and nlevel>1]
   //  par[21]  dPdV card_profile [gENUM_PROFILE]    [only for ANNIHILATION and nlevel>1]
   //  par[22]  host halo outer radius (unused)      [only for ANNIHILATION and nlevel>1]
   //  par[23]  ratio rs dPdV to rs_cl               [only for ANNIHILATION and nlevel>1]
   //  par[24]  R_host (corresp. to current dPdV)    [only for ANNIHILATION and nlevel>1]
   //  -- par[25-27] is for mass integration:
   //  par[25]  m1 lower integration limit [Msol]
   //  par[26]  m2 lower integration limit [Msol]
   //  par[27]  bool to decide if dpdm shall be normalized between m1 and m2

   double dpdv = 0.;
   rho(r, &par[16], dpdv);
   par[24] = r;

   int n_parsubs = 25;
   double par_subs[n_parsubs];
   for (int i = 0; i < n_parsubs; ++i)
      par_subs[i] = par[i];

   res = dpdv * mean1cl_lumn_r(par_subs, par[25], par[26], 2, bool(par[27]));
}

//______________________________________________________________________________
void r2dpdv_lum_r(double &r, double par[28], double &res)
{
   //--- Sets res = 4pi * r^2 * dP/dV(r) * <L(r)>_{m,c} of a clump in the range [m1,m2],
   //    with <L(r,m)>_c =int_c1^c2 (L^2.dP/dc) dc,
   //    the mean luminosity of a clump of mass m within the range of
   //    concentration [c1, c2].
   //    N.B.: L quantities are calculated for nlevel of substructures (described
   //    by the concentration dPdc, mass dPdM, and spatial dPdV distributions),
   //    each level having the same properties and a mass fraction f (in substructures).
   //
   //  -- par[0-24] is par_subs:
   //  par[0]   rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par[1]   rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par[2]   rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par[4]   host halo outer radius where to stop integration [kpc]
   //  par[5]   eps: relative precision sought L calculation
   //  par[6]   z: redshift of the halo hosting the sub-clumps
   //  par[7]   mdelta: mass of rho_cl(r)
   //  par[8]   dPdM normalisation [1/Msol]
   //  par[9]   dPdM slope alphaM
   //  par[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par[11]  dPdc mean concentration <c>
   //  par[12]  dPdc standard deviation              [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par[13]  dPdc card_profile [gENUM_CDELTA_DIST]  [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par[14]  f: mass fraction of substructures    [only for ANNIHILATION and nlevel>1]
   //  par[15]  nlevel: sub-sub...halos (1=no sub)   [only for ANNIHILATION and nlevel>1]
   //  par[16]  dPdV normalisation  [kpc^{-3}]       [only for ANNIHILATION and nlevel>1]
   //  par[17]  dPdV scale radius [kpc]              [only for ANNIHILATION and nlevel>1]
   //  par[18]  dPdV shape parameter #1              [only for ANNIHILATION and nlevel>1]
   //  par[19]  dPdV shape parameter #2              [only for ANNIHILATION and nlevel>1]
   //  par[20]  dPdV shape parameter #3              [only for ANNIHILATION and nlevel>1]
   //  par[21]  dPdV card_profile [gENUM_PROFILE]    [only for ANNIHILATION and nlevel>1]
   //  par[22]  host halo outer radius (unused)      [only for ANNIHILATION and nlevel>1]
   //  par[23]  ratio rs dPdV to rs_cl               [only for ANNIHILATION and nlevel>1]
   //  par[24]  R_host (corresp. to current dPdV)    [only for ANNIHILATION and nlevel>1]
   //  -- par[25-27] is for mass integration:
   //  par[25]  m1 lower integration limit [Msol]
   //  par[26]  m2 lower integration limit [Msol]
   //  par[27]  bool to decide if dpdm shall be normalized between m1 and m2

   double r2dpdv = 0.;
   r2rho(r, &par[16], r2dpdv);
   par[24] = r;

   int n_parsubs = 25;
   double par_subs[n_parsubs];
   for (int i = 0; i < n_parsubs; ++i)
      par_subs[i] = par[i];

   res = 4. * PI * r2dpdv * mean1cl_lumn_r(par_subs, par[25], par[26], 1, bool(par[27]));
}

//______________________________________________________________________________
void r2dpdv_lum2_r(double &r, double par[28], double &res)
{
   //--- Sets res = 4pi * r^2 * dP/dV(r) * <L^2(r)>_{m,c} of a clump in the range [m1,m2],
   //    with <L^2(r,m)>_c =int_c1^c2 (L^2.dP/dc) dc,
   //    the mean squared luminosity of a clump of mass m within the range of
   //    concentration [c1, c2].
   //    N.B.: L quantities are calculated for nlevel of substructures (described
   //    by the concentration dPdc, mass dPdM, and spatial dPdV distributions),
   //    each level having the same properties and a mass fraction f (in substructures).
   //
   //  -- par[0-24] is par_subs:
   //  par[0]   rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par[1]   rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par[2]   rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par[4]   host halo outer radius where to stop integration [kpc]
   //  par[5]   eps: relative precision sought L calculation
   //  par[6]   z: redshift of the halo hosting the sub-clumps
   //  par[7]   mdelta: mass of rho_cl(r)
   //  par[8]   dPdM normalisation [1/Msol]
   //  par[9]   dPdM slope alphaM
   //  par[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par[11]  dPdc mean concentration <c>
   //  par[12]  dPdc standard deviation              [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par[13]  dPdc card_profile [gENUM_CDELTA_DIST]  [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par[14]  f: mass fraction of substructures    [only for ANNIHILATION and nlevel>1]
   //  par[15]  nlevel: sub-sub...halos (1=no sub)   [only for ANNIHILATION and nlevel>1]
   //  par[16]  dPdV normalisation  [kpc^{-3}]       [only for ANNIHILATION and nlevel>1]
   //  par[17]  dPdV scale radius [kpc]              [only for ANNIHILATION and nlevel>1]
   //  par[18]  dPdV shape parameter #1              [only for ANNIHILATION and nlevel>1]
   //  par[19]  dPdV shape parameter #2              [only for ANNIHILATION and nlevel>1]
   //  par[20]  dPdV shape parameter #3              [only for ANNIHILATION and nlevel>1]
   //  par[21]  dPdV card_profile [gENUM_PROFILE]    [only for ANNIHILATION and nlevel>1]
   //  par[22]  host halo outer radius (unused)      [only for ANNIHILATION and nlevel>1]
   //  par[23]  ratio rs dPdV to rs_cl               [only for ANNIHILATION and nlevel>1]
   //  par[24]  R_host (corresp. to current dPdV)    [only for ANNIHILATION and nlevel>1]
   //  -- par[25-27] is for mass integration:
   //  par[25]  m1 lower integration limit [Msol]
   //  par[26]  m2 lower integration limit [Msol]
   //  par[27]  bool to decide if dpdm shall be normalized between m1 and m2

   double r2dpdv = 0.;
   r2rho(r, &par[16], r2dpdv);
   par[24] = r;

   int n_parsubs = 25;
   double par_subs[n_parsubs];
   for (int i = 0; i < n_parsubs; ++i)
      par_subs[i] = par[i];

   res = 4. * PI * r2dpdv * mean1cl_lumn_r(par_subs, par[25], par[26], 2, bool(par[27]));
}

//______________________________________________________________________________
void r2dpdv_c(double &r, double par_dpdv_c[14], double &res)
{
   //--- Sets res = 4pi * r^2 * dP/dV(r) * c(M,r)
   //
   //  r               distance from halo center [kpc]
   //  par_dpdv_c[0]    dpdv normalisation [kpc^{-3}]
   //  par_dpdv_c[1]    dpdv scale radius [kpc]
   //  par_dpdv_c[2]    dpdv shape parameter #1
   //  par_dpdv_c[3]    dpdv shape parameter #2
   //  par_dpdv_c[4]    dpdv shape parameter #3
   //  par_dpdv_c[5]    dpdv card_profile [gENUM_PROFILE]
   //  par_dpdv_c[6]    Radius of dpdv, rvir [kpc]
   //  par_dpdv_c[7]    Distance observer - host halo centre [kpc]
   //  par_dpdv_c[8]    UNUSED (Longitude (psi) of host halo [rad])
   //  par_dpdv_c[9]    UNUSED (Latitude (theta) of host halo [rad])
   //  par_dpdv_c[10]   m_delta
   //  par_dpdv_c[11]   card_cvir;
   //  par_dpdv_c[12]   redshift z
   //  par_dpdv_c[13]   Delta_c

   double r2dpdv = 0.;
   double xpos = r / par_dpdv_c[6];
   r2rho(r, par_dpdv_c, r2dpdv);
   res = 4. * PI * r2dpdv * mdelta_to_cdelta(par_dpdv_c[10], par_dpdv_c[13], par_dpdv_c[11], &par_dpdv_c[2], par_dpdv_c[12], xpos);
}

//______________________________________________________________________________
double dpdv_lcosalphabeta(double *x, double par_dpdvmod[10])
{
   //--- Returns dpdv [kpc^{-3}] at position (l,alpha,beta)_Earth.
   //    [Frame attached to the observer (Earth), see geometry.h]
   //    N.B.: works only if dpdv centre is on the line joining Earth-Galactic centre].
   //    N.B.: par_dpdvmod are standard par_dpdv parameters, except for the last two
   //    (the modif contains l.o.s. direction instead of halo longitude and latitude).
   //    N.B.: We work with cos(alpha) to assure numerical stability towards alpha->0
   //          and alpha->pi for a limited number of nodes in the TF3 integral. Matters
   //          particularly for full-sky maps.
   //
   //  x[0]           l: distance to the observer [kpc]
   //  x[1]           cos(alpha): angle away from the l.o.s
   //  x[2]           beta: rotation angle [rad]
   //  par_dpdvmod[0] dPdV normalisation  [kpc^{-3}]
   //  par_dpdvmod[1] dPdV scale radius [kpc]
   //  par_dpdvmod[2] dPdV shape parameter #1
   //  par_dpdvmod[3] dPdV shape parameter #2
   //  par_dpdvmod[4] dPdV shape parameter #3
   //  par_dpdvmod[5] dPdV card_profile [gENUM_PROFILE]
   //  par_dpdvmod[6] dPdV radius [kpc]
   //  par_dpdvmod[7] l_C: distance observer to dPdV centre [kpc]
   //  par_dpdvmod[8] psi_los, l.o.s. longitude [rad]
   //  par_dpdvmod[9] theta_los, l.o.s. latitude [rad]

   // Calculate (x,y,z)_G coordinated (w.r.t. the clump centre) from (l,alpha,beta)_Earth
   double alpha = acos(x[1]);
   double psi_theta[2] = {par_dpdvmod[8], par_dpdvmod[9]}; //psi,theta
   double ref_kRsol = gMW_RSOL;
   gMW_RSOL = par_dpdvmod[7];

   // Watch out: a new x_G[3] is required, otherwise the operation below
   // would affects x[3] (and this trick already made us sweat once!)
   double x_G[3] = {x[0], alpha, x[2]};
   lalphabeta_to_xyzgal(x_G, psi_theta);
   gMW_RSOL = ref_kRsol;

   // dPdV is multiplied by the Jacobian of the transformation to adjust the volume element
   //    (l,theta, psi) --> (x,y,z)  => |l*l|
   return dpdv_xyz(x_G, par_dpdvmod) * fabs(x[0] * x[0]);
}

//______________________________________________________________________________
double dpdv_lthetapsi(double *x, double par_dpdv[8])
{
   //--- Returns dpdv [kpc^{-3}] at position (l,theta,psi)_Earth.
   //    [Frame attached to the observer (Earth), see geometry.h]
   //    N.B.: works only if dpdv centre is on the line joining Earth-Galactic centre.
   //
   //  x[0]          l: distance to the observer [kpc]
   //  x[1]          theta_los: l.o.s. longitude [rad]
   //  x[2]          psi_los: l.o.s. latitude [rad]
   //  par_dpdv[0]   dPdV normalisation  [kpc^{-3}]
   //  par_dpdv[1]   dPdV scale radius [kpc]
   //  par_dpdv[2]   dPdV shape parameter #1
   //  par_dpdv[3]   dPdV shape parameter #2
   //  par_dpdv[4]   dPdV shape parameter #3
   //  par_dpdv[5]   dPdV card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV radius [kpc]
   //  par_dpdv[7]   l_C: distance observer to dPdV centre [kpc]

   // Calculate (x,y,z)_G coordinates (w.r.t. the clump centre) from (l,theta,psi)_Earth
   double x_G = x[0] * MY_COS(x[2]) * MY_COS(x[1]) - par_dpdv[7];
   double y_G = x[0] * MY_COS(x[1]) * MY_SIN(x[2]);
   double z_G = x[0] * MY_SIN(x[1]);

   // dPdV is multiplied by the Jacobian of the transformation to adjust the volume element
   // (l,theta, psi) --> (x,y,z) => |l*l*MY_COS(theta)|
   double r = sqrt(x_G * x_G + y_G * y_G +  z_G * z_G);
   double res = 0.;
   rho(r, par_dpdv, res);
   return res * fabs(x[0] * x[0] * MY_COS(x[1]));
}

//______________________________________________________________________________
double dpdv_rthetapsi(double *x, double par_dpdv[8])
{
   //--- Returns dpdv [kpc^{-3}] at position (r,theta,psi).
   //    [spherical coordinates attached to the distribution centre]
   //
   //  x[0]          r: distance to the observer [kpc]
   //  x[1]          theta: standard spheric. coord. [rad]
   //  x[2]          psi: standard spheric. coord. [rad]
   //  par_dpdv[0]   dPdV normalisation  [kpc^{-3}]
   //  par_dpdv[1]   dPdV scale radius [kpc]
   //  par_dpdv[2]   dPdV shape parameter #1
   //  par_dpdv[3]   dPdV shape parameter #2
   //  par_dpdv[4]   dPdV shape parameter #3
   //  par_dpdv[5]   dPdV card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV radius [kpc]
   //  par_dpdv[7]   l_C: distance observer to dPdV centre [kpc]

   // dPdV is multiplied by the Jacobian of the transformation to adjust the volume element
   //    (l,theta, psi)_standard --> (x,y,z)  => |r*r*MY_SIN(theta)|
   double res = 0.;
   rho(x[0], par_dpdv, res);
   return res * x[0] * x[0] * MY_SIN(x[1]);
}

//______________________________________________________________________________
void dpdv_setnormprob(double par_dpdv[7], double const &eps)
{
   //--- Sets normalisation (such as dpdv is a probability), i.e. par_dpdv[0].
   //
   // INPUTS:
   //  par_dpdv[0]   UNUSED
   //  par_dpdv[1]   dPdV scale radius [kpc]
   //  par_dpdv[2]   dPdV shape parameter #1
   //  par_dpdv[3]   dPdV shape parameter #2
   //  par_dpdv[4]   dPdV shape parameter #3
   //  par_dpdv[5]   dPdV card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV radius [kpc]
   //  eps           Relative precision sought for the integration
   // OUTPUT:
   //  par_dpdv[0]   dPdV normalisation  [kpc^{-3}]

   par_dpdv[0] = 1.;
   par_dpdv[0] = 1. / mass_singlehalo(par_dpdv, eps);
}

//______________________________________________________________________________
double dpdv_xyz(double *x, double par_dpdv[8])
{
   //--- Returns dpdv [kpc^{-3}] at position (x,y,z).
   //    [frame attached to the distribution centre]
   //
   //  x[0]          x: cartesian coordinate [kpc]
   //  x[1]          y: cartesian coordinate [kpc]
   //  x[2]          z: cartesian coordinate [kpc]
   //  par_dpdv[0]   dPdV normalisation  [kpc^{-3}]
   //  par_dpdv[1]   dPdV scale radius [kpc]
   //  par_dpdv[2]   dPdV shape parameter #1
   //  par_dpdv[3]   dPdV shape parameter #2
   //  par_dpdv[4]   dPdV shape parameter #3
   //  par_dpdv[5]   dPdV card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV radius [kpc]
   //  par_dpdv[7]   l_C: distance observer to dPdV centre [kpc]

   double r = sqrt(x[0] * x[0] + x[1] * x[1] + x[2] * x[2]);
   if (r > par_dpdv[6]) return 0.;
   else {
      double res = 0.;
      rho(r, par_dpdv, res);
      return res;
   }
}

//______________________________________________________________________________
void find_fracjpointlike_dist(double par_tot[7], double const &frac, double &dist, int iter)
{
   //--- Finds recursively the distance dist [kpc] to the clump, beyond which
   //    the point-like calculation of J holds, i.e. Jd = frac*Jpointlike (with
   //    precision eps=0.1*(1-frac) where Jpointlike = lum / d^2. This is evaluated
   //    for a fixed opening angle alphaint = gSIM_ALPHAINT.
   //
   // INPUTS:
   //  par_tot[0]    Dark matter density normalisation [Msol/kpc^3]
   //  par_tot[1]    Scale radius [kpc]
   //  par_tot[2]    Shape parameter #1
   //  par_tot[3]    Shape parameter #2
   //  par_tot[4]    Shape parameter #3
   //  par_tot[5]    card_profile [gENUM_PROFILE]
   //  par_tot[6]    Radius of the DM halo [kpc]
   //  frac          Fraction sought
   //  iter          Number of recursive calls [must be 0 for first call]
   // OUTPUT:
   //  dist          Distance [kpc] for which J = frac * Jpointlike


   // Calculation stops when precision eps is reached
   double eps = 1.e-2 * (1. - frac);

   // Set all direction towards the GC direction
   double psi_los = 0., theta_los = 0.;

   // Set the starting value for dist
   double d_containment = par_tot[6] / tan(gSIM_ALPHAINT);
   double dmin = 1.e-3;
   double dmax = 1.e4 * d_containment;
   if (iter == 0) dist = dmin; // to ensure we start in a region where it is not point-like!

   double jpointlike = lum_singlehalo_nosubs(par_tot, eps);
   double j_frac = jpointlike * frac;

   // Calculate Jpointlike (even if not optimal to repeat this step in the dichotomy)
   // Requires par_tmp[10] for integration
   //  par_tmp[0...6] = par_tot[0...6]
   //  par_tmp[7]  = l
   //  par_tmp[8]  = psi = psi_los = 0
   //  par_tmp[9] = theta = theta_los = 0
   const int n_par = 10;
   double par_tmp[n_par];
   for (int i = 0; i < 7; ++i) par_tmp[i] = par_tot[i];
   par_tmp[7] = dist;
   par_tmp[8] = 0.;
   par_tmp[9] = 0.;
   double j_d = jsmooth(par_tmp, psi_los, theta_los, eps);

   // The integration for J is not perfectly accurate, it may be impossible
   // to get x% of J (the value may oscillate above and below that). We thus
   // stop after 20 iteration where the angle doesn't change anymore.
   if (fabs(j_d - j_frac) / (j_frac) < eps || iter > 50)
      return;

   // Each iter, the interval is half the initial one
   ++iter;

   // log-step
   double dr = pow(10., log10(dmax / dmin) / pow(2., iter));
   if (j_d < j_frac)
      dist *= dr;
   else
      dist /= dr;
   find_fracjpointlike_dist(par_tot, frac, dist, iter);
}

//______________________________________________________________________________
void find_fracjtot_alphaint(double par_tot[10], double const &frac, double &alphaint_rad, int iter)
{
   //--- Finds recursively alphaint_rad [kpc] for which J(alphaint_rad) = frac*jmax
   //    (with precision eps=0.1*(1-frac)), where jmax (only smooth) is evaluated
   //    for alphaint encompassing the halo.
   //
   // INPUTS:
   //  par_tot[0]    Dark matter density normalisation [Msol/kpc^3]
   //  par_tot[1]    Scale radius [kpc]
   //  par_tot[2]    Shape parameter #1
   //  par_tot[3]    Shape parameter #2
   //  par_tot[4]    Shape parameter #3
   //  par_tot[5]    card_profile [gENUM_PROFILE]
   //  par_tot[6]    Radius of the DM halo [kpc]
   //  par_tot[7]    Distance observer - halo centre [kpc]
   //  par_tot[8]    Longitude (psi) of halo [rad]
   //  par_tot[9]    Latitude (theta) of halo [rad]
   //  frac          Fraction sought
   //  iter          Number of recursive calls [must be 0 for first call]
   // OUTPUT:
   //  alphaint_rad  Angle [rad] (for which J = frac * jmax)


   // Calculation stops when precision eps is reached
   double eps = 5.e-2 * (1. - frac);

   // Set all direction towards the halo direction
   double psi_los = par_tot[8], theta_los = par_tot[9];

   if (par_tot[7] < par_tot[6]) {
      printf("\n====> ERROR: find_fracjtot_alphaint() in clumps.cc");
      printf("\n             d_halo<Rvir (par_tot[7] < par_tot[6]) is not allowed");
      printf("\n             => abort()\n\n");
      abort();
   }

   double alpha_max = fabs(asin(par_tot[6] / par_tot[7]));
   double alpha_min = 1.e-8 * alpha_max;
   if (frac > 0.5) alpha_min = 1.e-4 * alpha_max;

   // Set the starting value for alphaint_rad
   if (iter == 0) alphaint_rad = alpha_max;

   // Stores gSIM_ALPHAINT
   double alpha_ref = gSIM_ALPHAINT;
   gSIM_ALPHAINT = alphaint_rad;

   // a) even if it is not optimised, calculate jmax each time
   gSIM_ALPHAINT = alpha_max;
   double j_max = jsmooth(par_tot, psi_los, theta_los, eps);

   // b) calculate j for current tested angle
   gSIM_ALPHAINT = alphaint_rad;
   double j_alpha = jsmooth(par_tot, psi_los, theta_los, eps);
   double j_frac = j_max * frac;

   gSIM_ALPHAINT = alpha_ref;

   // The integration for J is not perfectly accurate, it may be impossible
   // to get x% of J (the value may oscillate above and below that). We thus
   // stop after 20 iteration where the angle doesn't change anymore.
   if (fabs(j_alpha - j_frac) / j_frac < eps || iter > 20) return;

   // Each iter, the interval is half the initial one
   ++iter;

   // log-step
   double dalpha = pow(10., log10(alpha_max / alpha_min) / pow(2., iter));
   if (j_alpha < j_frac)
      alphaint_rad *= dalpha;
   else
      alphaint_rad /= dalpha;

   find_fracjtot_alphaint(par_tot, frac, alphaint_rad, iter);
}

//______________________________________________________________________________
void find_fracjtot_alphaint(double par_tot[10], double const &mtot, double const &f_dm,
                            double par_dpdv[10], double par_subs[25], double const &ntot_subs,
                            double const &jtot, double const &frac, double &alphaint_rad, int iter)
{
   //--- Finds recursively alphaint_rad [kpc] for which J(alphaint_rad) = frac*jtot
   //    (with precision eps=0.1*(1-frac)), where jmax (smooth+subs+cross-prod)
   //    is evaluated for alphaint encompassing the halo.
   //
   // INPUTS:
   //  par_tot[0]    rho_tot host normalisation [Msol kpc^{-3}]
   //  par_tot[1]    rho_tot host scale radius [kpc]
   //  par_tot[2]    rho_tot host shape parameter #1
   //  par_tot[3]    rho_tot host shape parameter #2
   //  par_tot[4]    rho_tot host shape parameter #3
   //  par_tot[5]    rho_tot host card_profile [gENUM_PROFILE]
   //  par_tot[6]    rho_tot host radius [kpc]
   //  par_tot[7]    l_HC: distance observer to host centre [kpc]
   //  par_tot[8]    psi_HC: longitude of host centre [rad]
   //  par_tot[9]    theta_HC: latitude of host centre [rad]
   //  mtot          Total mass of host halo
   //  f_dm          Fraction of the halo mass in subs
   //  par_dpdv[0]   dPdV_Host normalisation  [kpc^{-3}]
   //  par_dpdv[1]   dPdV_Host scale radius [kpc]
   //  par_dpdv[2]   dPdV_Host shape parameter #1
   //  par_dpdv[3]   dPdV_Host shape parameter #2
   //  par_dpdv[4]   dPdV_Host shape parameter #3
   //  par_dpdv[5]   dPdV_Host card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV_Host radius [kpc]
   //  par_dpdv[7]   l_HC: distance observer to Host Centre [kpc]
   //  par_dpdv[8]   psi_HC: longitude of dpdv [rad]
   //  par_dpdv[9]   theta_HC: latitude of dpdv [rad]
   //  par_subs[0]   rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_subs[1]   rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_subs[2]   rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_subs[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]   host halo outer radius where to stop integration [kpc]
   //  par_subs[5]   eps: relative precision sought L calculation
   //  par_subs[6]   z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]   mdelta: mass of rho_cl(r)
   //  par_subs[8]   dPdM normalisation
   //  par_subs[9]   dPdM slope alphaM
   //  par_subs[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_subs[11]  dPdc mean concentration <c>
   //  par_subs[12]  dPdc standard deviation             [UNUSED if gENUM_CVIR_DIST=kDIRAC]
   //  par_subs[13]  dPdc card_profile [gENUM_CVIR_DIST] [UNUSED if gENUM_CVIR_DIST=kDIRAC]
   //  par_subs[14]  f: mass fraction of substructures   [only for ANNIHILATION and nlevel>1]
   //  par_subs[15]  nlevel: sub-sub...halos (1=no sub)  [only for ANNIHILATION and nlevel>1]
   //  par_subs[16]  dPdV normalisation  [kpc^{-3}]      [only for ANNIHILATION and nlevel>1]
   //  par_subs[17]  dPdV scale radius [kpc]             [only for ANNIHILATION and nlevel>1]
   //  par_subs[18]  dPdV shape parameter #1             [only for ANNIHILATION and nlevel>1]
   //  par_subs[19]  dPdV shape parameter #2             [only for ANNIHILATION and nlevel>1]
   //  par_subs[20]  dPdV shape parameter #3             [only for ANNIHILATION and nlevel>1]
   //  par_subs[21]  dPdV card_profile [gENUM_PROFILE]   [only for ANNIHILATION and nlevel>1]
   //  par_subs[22]  host halo outer radius (unused)     [only for ANNIHILATION and nlevel>1]
   //  par_subs[23]  ratio rs dPdV to rs_cl              [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]  R_host (corresp. to current dPdV)   [only for ANNIHILATION and nlevel>1]
   //  ntot_subs     Total number of subs in host halo
   //  jtot          Astrophysical factor for the whole halo
   //  frac          Fraction sought
   //  iter          Number of recursive calls [must be 0 for first call]
   // OUTPUT:
   //  alphaint_rad  Angle [rad] (for which J = frac * jmax)


   // The calculation stops when precision eps is reached
   double eps = 5.e-2 * (1. - frac);

   // Set all direction towards the halo direction
   double psi_los = par_tot[8], theta_los = par_tot[9];

   if (par_tot[7] < par_tot[6]) {
      printf("\n====> ERROR: find_fracjtot_alphaint() in clumps.cc");
      printf("\n             d_halo<Rvir (par_tot[7] < par_tot[6]) is not allowed");
      printf("\n             => abort()\n\n");
      abort();
   }


   // The half-angular size of the clump on the sky is asin(R_cl/l_cl)
   // N.B.: if the clump is too close, the angle is ill-defined. In that case
   // we set it to PI/2
   double alpha_max = min(PI / 2., fabs(asin(par_tot[6] / par_tot[7])));
   double alpha_min = 1.e-8 * alpha_max;
   if (frac > 0.5) alpha_min = 1.e-4 * alpha_max;

   // Set the starting value for alphaint_rad
   if (iter == 0) alphaint_rad = alpha_max;

   // Stores gSIM_ALPHAINT
   double alpha_ref = gSIM_ALPHAINT;

   // calculate j for current tested angle
   if (par_subs[6] > ZMIN_EXTRAGAL) gSIM_ALPHAINT = atan(1. / (1 + par_subs[6]) * MY_TAN(alphaint_rad));
   else gSIM_ALPHAINT = alphaint_rad;

   // Add contrib. for smooth
   double lhalomin = max(0., par_tot[7] - par_tot[6]);
   double lhalomax = par_tot[7] + par_tot[6];
   double j_alpha = jsmooth_mix(mtot, par_tot, psi_los, theta_los,
                                lhalomin, lhalomax, eps, f_dm, par_dpdv);
   // Add contrib. from subclumps
   double msubs_max = mtot * gDM_SUBS_MMAXFRAC;
   j_alpha += jsub_continuum(ntot_subs, par_dpdv, psi_los, theta_los,
                             lhalomin, lhalomax, par_subs, gDM_SUBS_MMIN, msubs_max);
   // Add cross-product
   if (gPP_DM_IS_ANNIHIL_OR_DECAY)
      j_alpha += jcrossprod_continuum(mtot, par_tot, psi_los, theta_los,
                                      lhalomin, lhalomax, eps, f_dm, par_dpdv);


   double j_frac = frac * jtot;
   gSIM_ALPHAINT = alpha_ref;

   // The integration for J is not perfectly accurate, it may be impossible
   // to get x% of J (the value may oscillate above and below that). We thus
   // stop after 20 iteration where the angle doesn't change anymore.
   if (fabs(j_alpha - j_frac) / j_frac < eps || iter > 20) return;

   // Each iter, the interval is half the initial one
   ++iter;

   // log-step
   double dalpha = pow(10., log10(alpha_max / alpha_min) / pow(2., iter));
   if (j_alpha < j_frac)
      alphaint_rad *= dalpha;
   else
      alphaint_rad /= dalpha;

   find_fracjtot_alphaint(par_tot,  mtot, f_dm, par_dpdv, par_subs, ntot_subs, jtot, frac, alphaint_rad, iter);
}

//______________________________________________________________________________
void find_fraclumref_radius(double par_tot[6], double const &r_ref, double const &lum_ref,
                            double const &frac, double &radius, int iter)
{
   //--- Finds recursively radius [kpc] for which lum(radius) = frac*lum_ref
   //    (with precision eps=1.e-2*frac), with lum_ref evaluated at r_ref.
   //
   // INPUTS:
   //  par_tot[0]    Dark matter density normalisation [Msol/kpc^3]
   //  par_tot[1]    Scale radius [kpc]
   //  par_tot[2]    Shape parameter #1
   //  par_tot[3]    Shape parameter #2
   //  par_tot[4]    Shape parameter #3
   //  par_tot[5]    card_profile [gENUM_PROFILE]
   //  r_ref         Radius for which lum_ref was calculated [kpc]
   //  lum_ref       Reference intrinsic luminosity (for rmax=rvir) [Msol^2/kpc^5]
   //  frac          Percentage sought
   //  iter          Number of recursive calls [must be 0 for first call]
   // OUTPUT:
   //  radius        Radius [kpc] (for which lum = frac * lum_ref)

   // Calculation stops when precision eps is reached
   double eps = 1.e-2 * frac;

   // Set the starting value for radius
   if (iter == 0) radius = r_ref;

   // Calculate lum for r_halo = radius (< r_ref)
   // Requires par_tmp[7] for integration
   //  par_tmp[0...5] = par_tot[0...5]
   //  par_tmp[6] = radius
   const int n_par = 8;
   double par_tmp[n_par];
   for (int i = 0; i < 6; ++i)
      par_tmp[i] = par_tot[i];
   par_tmp[6] = radius;
   par_tmp[7] = 0.; //UNUSED

   double lum_r = lum_singlehalo_nosubs(par_tmp, eps);
   double lum_frac = lum_ref * frac ;

   if (fabs(lum_r - lum_frac) / (lum_frac) < eps)
      return;

   // Each iter, the interval is half the initial one
   ++iter;

   // lin-step
   //double dr = par[3]/pow(2.,iter);
   //if (lum_r<lum_frac) radius += dr;
   //else radius -= dr;

   // log-step
   double dr = pow(10., 5.*log(10) / pow(2., iter));
   if (lum_r < lum_frac)
      radius *= dr;
   else
      radius /= dr;
   find_fraclumref_radius(par_tot, r_ref, lum_ref, frac, radius, iter);
}

//______________________________________________________________________________
double find_halo_physicalsize(double &r, double par_size[18])
{
   //--- Returns the physical size [kpc] of a DM satellite at distance d [kpc]
   //    from its host halo centre. Several estimates of the physical size are
   //    provided, depending on the method selected by means of par_size[0].
   //       - Tidal radius method: radius that solves (for r=r_tidal) the equation
   //            r^3-d^3*rho_satellite(r)/((2-4PI*rho_host(d)*d^3/M_host(d))*M_host(d)) = 0
   //          N.B.: dln(Mhost)/dln(r)[d] = 4*pi*rho_host(d)*d^3/M_host(d).
   //       - Equal-density: radius of halo at which
   //            rho_satellite(r)=rho_host(d-r)
   //
   // INPUTS:
   //  r             Trial radius for calculation [kpc]
   //  par_size[0]   rho_satellite density normalisation [Msol/kpc^3]
   //  par_size[1]   rho_satellite scale radius [kpc]
   //  par_size[2]   rho_satellite shape parameter #1
   //  par_size[3]   rho_satellite shape parameter #2
   //  par_size[4]   rho_satellite shape parameter #3
   //  par_size[5]   rho_satellite card_profile [gENUM_PROFILE]
   //  par_size[6]   UNUSED
   //  par_size[7]   rho_host DM density normalisation [Msol/kpc^3]
   //  par_size[8]   rho_host scale radius [kpc]
   //  par_size[9]   rho_host shape parameter #1
   //  par_size[10]  rho_host shape parameter #2
   //  par_size[11]  rho_host shape parameter #3
   //  par_size[12]  rho_host card_profile [gENUM_PROFILE]
   //  par_size[13]  d: distance from host center to satellite center [kpc]
   //  par_size[14]  eps: relative precision sought for mass calculation
   //  par_size[15]  n_call: must be initialised to 0!
   //  par_size[16]  Select method used to estimate physical size
   //                   0: tidal radius condition
   //                   1: equal-density condition
   // par_size[17]   Select hard or soft fail of function (returns -999 for soft fail)
   // OUTPUT:
   //  par_size[6]   Set to r (integration radius for rho_sat) [kpc]
   //  par_size[15]  n_call: #iterations (rootsolver_brent) used to find the physical size


   // Update values
   par_size[6] = r;
   double d = par_size[13];


   // If first call of function (i.e. par_size[15]=0),
   // call rootsolver_brent to find the physical size
   par_size[15] += 1.;
   if ((int)par_size[15] == 1) {
      double rmin = par_size[1] * 1.e-3;
      double rmax = par_size[1] * 1.e3;
      return rootsolver_brent(find_halo_physicalsize, par_size, rmin, rmax, par_size[14], par_size[17]);
   } else {
      // Tidal radius calculation
      if ((int)par_size[16] == 0) {
         // Mass of satellite halo at r=r_tidal
         double m_sat = mass_singlehalo(par_size, par_size[14]);

         // Mass and density of host halo at r=d
         double m_host = mass_singlehalo(&par_size[7], par_size[14]);
         double rho_host = 1.;
         double d_cube = d * d * d;
         rho(d, &par_size[7], rho_host);

         // Equation whose results gives 0 at r=r_tidal (Binney & Tremaine 1987, also eq. 12 in 0809.0898)
         return r * r * r - d_cube * m_sat / ((2. - 4.*PI * rho_host * d_cube / m_host) * m_host);
      } else if ((int)par_size[16] == 1) {
         double d_equal = fabs(d - r);

         // Calculate density rho_sat(r) - rho_host(|d-r|)
         double rho_sat = 0.;
         double rho_host = 0.;
         rho(d_equal, &par_size[7], rho_host);
         rho(r, &par_size[0], rho_sat);

         return rho_host - rho_sat;
      } else {
         printf("\n====> ERROR: find_halo_physicalsize() in clumps.cc");
         printf("\n             par_size[16]=%d is not a valid option", (int)par_size[16]);
         printf("\n             => abort()\n\n");
         abort();
      }
   }
   return 0.;
}

//______________________________________________________________________________
double frac_nsubs_in_foi(double par_dpdv[10], double const &psi_los, double const &theta_los,
                         double const &l1, double const &l2, double const &eps)
{
   //--- Returns the fraction of subs found in the field of integration
   //    (gSIM_ALPHAINT, [l1, l2]) w.r.t. total number of subs in host halo.
   ///   int_DOmega int_los dp/dv l^2 dl dOmega
   //
   //  par_dpdv[0]   dPdV normalisation  [kpc^{-3}]
   //  par_dpdv[1]   dPdV scale radius [kpc]
   //  par_dpdv[2]   dPdV shape parameter #1
   //  par_dpdv[3]   dPdV shape parameter #2
   //  par_dpdv[4]   dPdV shape parameter #3
   //  par_dpdv[5]   dPdV card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV radius [kpc]
   //  par_dpdv[7]   l_C: distance observer to dPdV centre [kpc]
   //  par_dpdv[8]   psi_C: longitude of dpdv [rad]
   //  par_dpdv[9]   theta_C: latitude of dpdv [rad]
   //  psi_los       psi_los: longitude for the l.o.s [rad]
   //  theta_los     theta_los: latitude for the l.o.s [rad]
   //  l1            Lower value for the l.o.s. integration boundary [kpc]
   //  l2            Upper value for the l.o.s. integration boundary [kpc]
   //  eps           Relative precision sought for integration

   // Number fraction = int dP/dV dV = int_dOmega int_l1^l2 l^2 dP/dV dOmega dl
   // To integrate f=l^2 dP/dV in los_integral()
   return mean1cl_ln(par_dpdv, psi_los, theta_los, l1, l2, eps, 0.);
}

//__________________________________________________________________________
double frac_nsubs_in_m1m2(double par_dpdm[2], double &m1, double &m2,
                          double const &eps)
{
   //--- Returns the fraction of subs in [m1,m2] w.r.t. the total number
   //    of subs in the host halo.
   //
   //  par_dpdm[0]   dPdM normalisation (for [Mminsub,Mmaxsub]) [1/Msol]
   //  par_dpdm[1]   dPdM slope alphaM
   //  m1            Lower value of the mass range of subs considered [Msol]
   //  m2            Upper value of the mass range of subs considered [Msol]
   //  eps           Relative precision sought for integration

   double res = 0.;
   simpson_log_adapt(dpdm, m1, m2, par_dpdm, res, eps);
   return res;
}

//______________________________________________________________________________
double jcrossprod_continuum(double const &mtot, double par_tot[10],
                            double const &psi_los, double const &theta_los,
                            double const &eps,
                            double const &f_dm, double par_dpdv[10])
{
   //--- Returns Jcrossprod=int_dOmega int_l {rho_sm <rho_dPdV>} dOmega dl
   //    along a F.O.I (l.o.s. + gSIM_ALPHAINT):
   //       - [Msol^2/kpc^5] for annihilation;
   //       - not applicable for decay.
   //
   //  mtot          Total mass of the host halo [Msol]
   //  par_tot[0]    rho_tot host normalisation [Msol kpc^{-3}]
   //  par_tot[1]    rho_tot host scale radius [kpc]
   //  par_tot[2]    rho_tot host shape parameter #1
   //  par_tot[3]    rho_tot host shape parameter #2
   //  par_tot[4]    rho_tot host shape parameter #3
   //  par_tot[5]    rho_tot host card_profile [gENUM_PROFILE]
   //  par_tot[6]    rho_tot host radius [kpc]
   //  par_tot[7]    l_HC: distance observer to host centre [kpc]
   //  par_tot[8]    psi_HC: longitude of host centre [rad]
   //  par_tot[9]    theta_HC: latitude of host centre [rad]
   //  psi_los       psi_los: longitude for the l.o.s [rad]
   //  theta_los     theta_los: latitude for the l.o.s [rad]
   //  eps           Relative precision sought for integration
   //  f_dm          Fraction of DM in subclumps (in the host halo)
   //  par_dpdv[0]   dPdV_Host normalisation  [kpc^{-3}]
   //  par_dpdv[1]   dPdV_Host scale radius [kpc]
   //  par_dpdv[2]   dPdV_Host shape parameter #1
   //  par_dpdv[3]   dPdV_Host shape parameter #2
   //  par_dpdv[4]   dPdV_Host shape parameter #3
   //  par_dpdv[5]   dPdV_Host card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV_Host radius [kpc]
   //  par_dpdv[7]   l_HC: distance observer to Host Centre [kpc]
   //  par_dpdv[8]   psi_HC: longitude of dpdv [rad]
   //  par_dpdv[9]   theta_HC: latitude of dpdv [rad]

   // Set integration distances (to encompass the host halo)
   double l1 = max(0., par_tot[7] - par_tot[6]);
   double l2 = par_tot[7] + par_tot[6];

   return jcrossprod_continuum(mtot, par_tot, psi_los, theta_los,
                               l1, l2, eps, f_dm, par_dpdv);
}

//______________________________________________________________________________
double jcrossprod_continuum(double const &mtot, double par_tot[10],
                            double const &psi_los, double const &theta_los,
                            double const &l1, double const &l2, double const &eps,
                            double const &f_dm, double par_dpdv[10])
{
   //--- Returns Jcrossprod=int_dOmega int_l {rho_sm <rho_dPdV>} dOmega dl
   //    along a F.O.I (l.o.s. + gSIM_ALPHAINT) and in the range [l1,l2]:
   //       - [Msol^2/kpc^5] for annihilation;
   //       - not applicable for decay.
   //
   //  mtot          Total mass of the host halo [Msol]
   //  par_tot[0]    rho_tot host normalisation [Msol kpc^{-3}]
   //  par_tot[1]    rho_tot host scale radius [kpc]
   //  par_tot[2]    rho_tot host shape parameter #1
   //  par_tot[3]    rho_tot host shape parameter #2
   //  par_tot[4]    rho_tot host shape parameter #3
   //  par_tot[5]    rho_tot host card_profile [gENUM_PROFILE]
   //  par_tot[6]    rho_tot host radius [kpc]
   //  par_tot[7]    l_HC: distance observer to host centre [kpc]
   //  par_tot[8]    psi_HC: longitude of host centre [rad]
   //  par_tot[9]    theta_HC: latitude of host centre [rad]
   //  psi_los       psi_los: longitude for the l.o.s [rad]
   //  theta_los     theta_los: latitude for the l.o.s [rad]
   //  l1            Lower value for the l.o.s. integration boundary [kpc]
   //  l2            Upper value for the l.o.s. integration boundary [kpc]
   //  eps           Relative precision sought for integration
   //  f_dm          Fraction of DM in subclumps (in the host halo)
   //  par_dpdv[0]   dPdV_Host normalisation  [kpc^{-3}]
   //  par_dpdv[1]   dPdV_Host scale radius [kpc]
   //  par_dpdv[2]   dPdV_Host shape parameter #1
   //  par_dpdv[3]   dPdV_Host shape parameter #2
   //  par_dpdv[4]   dPdV_Host shape parameter #3
   //  par_dpdv[5]   dPdV_Host card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV_Host radius [kpc]
   //  par_dpdv[7]   l_HC: distance observer to Host Centre [kpc]
   //  par_dpdv[8]   psi_HC: longitude of dpdv [rad]
   //  par_dpdv[9]   theta_HC: latitude of dpdv [rad]

   const int n_par_mix = 21;
   double par_mix[n_par_mix];

   for (int i = 0; i < 10; i++) par_mix[i] = par_tot[i];
   par_mix[10] = 2.; // switch_rho = 2 --> rho1*rho2
   for (int i = 11; i < n_par_mix; i++) par_mix[i] = par_dpdv[i - 11];
   par_mix[11] *= f_dm * mtot;
   int switch_f = 0; // to integrate rho1*rho2/l^2 along the l.o.s

   // Transform dPdV --> <rho_cl> the mean clump density
   double tmp = par_dpdv[0];
   par_dpdv[0] *= f_dm * mtot;

   double cross_prod = 2 * los_integral_mix(par_mix, switch_f, psi_los, theta_los, l1, l2, eps)
                       - 2 * jsmooth(par_dpdv, psi_los, theta_los, l1, l2, eps);

   par_dpdv[0] = tmp;
   return cross_prod;
}

//______________________________________________________________________________
double jsmooth(double par_tot[10], double const &psi_los, double const &theta_los,
               double const &eps)
{
   //--- Returns Jsmooth from a density profile rho_tot(r):
   //       - [Msol^2/kpc^5] for annihilation;
   //       - [Msol/kpc^2] for decay.
   //    N.B.: Quantities are given in proper coordinates.
   //
   //  par_tot[0]    rho_tot host normalisation [Msol kpc^{-3}]
   //  par_tot[1]    rho_tot host scale radius [kpc]
   //  par_tot[2]    rho_tot host shape parameter #1
   //  par_tot[3]    rho_tot host shape parameter #2
   //  par_tot[4]    rho_tot host shape parameter #3
   //  par_tot[5]    rho_tot host card_profile [gENUM_PROFILE]
   //  par_tot[6]    rho_tot host radius [kpc]
   //  par_tot[7]    l_HC: distance observer to host centre [kpc]
   //  par_tot[8]    psi_HC: longitude of host centre [rad]
   //  par_tot[9]    theta_HC: latitude of host centre [rad]
   //  psi_los       psi_los: longitude for the l.o.s [rad]
   //  theta_los     theta_los: latitude for the l.o.s [rad]
   //  eps           Relative precision sought for integration

   // Set integration distances (to encompass the host halo)
   double l1 = max(0., par_tot[7] - par_tot[6]);
   double l2 = par_tot[7] + par_tot[6];

   return jsmooth(par_tot, psi_los, theta_los, l1, l2, eps);
}

//______________________________________________________________________________
double jsmooth(double par_tot[10], double const &psi_los, double const &theta_los,
               double const &l1, double const &l2, double const &eps)
{
   //--- Returns Jsmooth between [l1,l2] from a density profile rho_tot(r):
   //       - [Msol^2/kpc^5] for annihilation;
   //       - [Msol/kpc^2] for decay.
   //    N.B.: Quantities are given in proper coordinates.
   //
   //  par_tot[0]    rho_tot host normalisation [Msol kpc^{-3}]
   //  par_tot[1]    rho_tot host scale radius [kpc]
   //  par_tot[2]    rho_tot host shape parameter #1
   //  par_tot[3]    rho_tot host shape parameter #2
   //  par_tot[4]    rho_tot host shape parameter #3
   //  par_tot[5]    rho_tot host card_profile [gENUM_PROFILE]
   //  par_tot[6]    rho_tot host radius [kpc]
   //  par_tot[7]    l_HC: distance observer to host centre [kpc]
   //  par_tot[8]    psi_HC: longitude of host centre [rad]
   //  par_tot[9]    theta_HC: latitude of host centre [rad]
   //  psi_los       psi_los: longitude for the l.o.s [rad]
   //  theta_los     theta_los: latitude for the l.o.s [rad]
   //  l1            Lower value for the l.o.s. integration boundary [kpc]
   //  l2            Upper value for the l.o.s. integration boundary [kpc]
   //  eps           Relative precision sought for integration

   int switch_f = 1;
   return los_integral(par_tot, switch_f, psi_los, theta_los, l1, l2, eps);
}

//______________________________________________________________________________
double jsmooth_mix(double const &mtot, double par_tot[10], double const &psi_los,
                   double const &theta_los, double const &eps, double const &f_dm,
                   double par_dpdv[10])
{
   //--- Returns Jsmooth for rho(r) which is a combo of rho1(r) and rho2(r):
   //       - [Msol^2/kpc^5] for annihilation;
   //       - [Msol/kpc^2] for decay.
   //    N.B.: Quantities are given in proper coordinates.
   //
   //  mtot          Total mass of the host halo [Msol]
   //  par_tot[0]    rho_tot host normalisation [Msol kpc^{-3}]
   //  par_tot[1]    rho_tot host scale radius [kpc]
   //  par_tot[2]    rho_tot host shape parameter #1
   //  par_tot[3]    rho_tot host shape parameter #2
   //  par_tot[4]    rho_tot host shape parameter #3
   //  par_tot[5]    rho_tot host card_profile [gENUM_PROFILE]
   //  par_tot[6]    rho_tot host radius [kpc]
   //  par_tot[7]    l_HC: distance observer to host centre [kpc]
   //  par_tot[8]    psi_HC: longitude of host centre [rad]
   //  par_tot[9]    theta_HC: latitude of host centre [rad]
   //  psi_los       psi_los: longitude for the l.o.s [rad]
   //  theta_los     theta_los: latitude for the l.o.s [rad]
   //  eps           Relative precision sought for integration
   //  f_dm          Fraction of DM in subclumps (in the host halo)
   //  par_dpdv[0]   dPdV_Host normalisation  [kpc^{-3}]
   //  par_dpdv[1]   dPdV_Host scale radius [kpc]
   //  par_dpdv[2]   dPdV_Host shape parameter #1
   //  par_dpdv[3]   dPdV_Host shape parameter #2
   //  par_dpdv[4]   dPdV_Host shape parameter #3
   //  par_dpdv[5]   dPdV_Host card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV_Host radius [kpc]
   //  par_dpdv[7]   l_HC: distance observer to Host Centre [kpc]
   //  par_dpdv[8]   psi_HC: longitude of dpdv [rad]
   //  par_dpdv[9]   theta_HC: latitude of dpdv [rad]

   // Set integration distances (to encompass the host halo)
   double l1 = max(0., par_tot[7] - par_tot[6]);
   double l2 = par_tot[7] + par_tot[6];

   return jsmooth_mix(mtot, par_tot, psi_los, theta_los, l1, l2, eps, f_dm, par_dpdv);

}

//______________________________________________________________________________
double jsmooth_mix(double const &mtot, double par_tot[10], double const &psi_los,
                   double const &theta_los, double const &l1, double const &l2,
                   double const &eps, double const &f_dm, double par_dpdv[10])
{
   //--- Returns Jsmooth in [l1,l2] for rho(r) which is a combo of rho1(r) and rho2(r):
   //       - [Msol^2/kpc^5] for annihilation;
   //       - [Msol/kpc^2] for decay.
   //    N.B.: Quantities are given in proper coordinates.
   //
   //  mtot          Total mass of the host halo [Msol]
   //  par_tot[0]    rho_tot host normalisation [Msol kpc^{-3}]
   //  par_tot[1]    rho_tot host scale radius [kpc]
   //  par_tot[2]    rho_tot host shape parameter #1
   //  par_tot[3]    rho_tot host shape parameter #2
   //  par_tot[4]    rho_tot host shape parameter #3
   //  par_tot[5]    rho_tot host card_profile [gENUM_PROFILE]
   //  par_tot[6]    rho_tot host radius [kpc]
   //  par_tot[7]    l_HC: distance observer to host centre [kpc]
   //  par_tot[8]    psi_HC: longitude of host centre [rad]
   //  par_tot[9]    theta_HC: latitude of host centre [rad]
   //  psi_los       psi_los: longitude for the l.o.s [rad]
   //  theta_los     theta_los: latitude for the l.o.s [rad]
   //  l1            Lower value for the l.o.s. integration boundary [kpc]
   //  l2            Upper value for the l.o.s. integration boundary [kpc]
   //  eps           Relative precision sought for integration
   //  f_dm          Fraction of DM in subclumps (in the host halo)
   //  par_dpdv[0]   dPdV_Host normalisation  [kpc^{-3}]
   //  par_dpdv[1]   dPdV_Host scale radius [kpc]
   //  par_dpdv[2]   dPdV_Host shape parameter #1
   //  par_dpdv[3]   dPdV_Host shape parameter #2
   //  par_dpdv[4]   dPdV_Host shape parameter #3
   //  par_dpdv[5]   dPdV_Host card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV_Host radius [kpc]
   //  par_dpdv[7]   l_HC: distance observer to Host Centre [kpc]
   //  par_dpdv[8]   psi_HC: longitude of dpdv [rad]
   //  par_dpdv[9]   theta_HC: latitude of dpdv [rad]

   if (l1 >= l2) return 0.;

   if (f_dm < 1.e-5) return jsmooth(par_tot, psi_los, theta_los, l1, l2, eps);

   // Need to create par_mix[21], with switch_rho=1
   const int n_par_mix = 21;
   double par_mix [n_par_mix ];
   //  par_mix [0-6]   rho_1(r): norm + rs [kpc] + shape#(1,2,3) + card_profile [gENUM_PROFILE] + Rmax [kpc]
   //  par_mix [7-9]   rho_1(r): distance + longitude (psi) + latitude (theta) to halo centre [kpc,rad,rad]
   //  par_mix [10]    switch_rho - selects which combination of rho_1 and rho_2 to use
   //                        0 -> rho(r) = rho1(r)
   //                        1 -> rho(r) = rho1(r)-rho2(r)
   //                        2 -> rho(r) = rho1(r)*rho2(r)
   //  par_mix [11-17] rho_2(r): norm + rs [kpc] + shape#(1,2,3) + card_profile [gENUM_PROFILE] + Rmax [kpc]
   //  par_mix [18-20] rho_2(r): distance + longitude (psi) + latitude (theta) to halo centre [kpc,rad,rad]
   for (int i = 0; i < 10; i++) par_mix [i] = par_tot[i];
   par_mix [10] = 1.; // switch_rho=1 --> uses rho = rho1-rho2
   for (int i = 11; i < n_par_mix ; i++) par_mix [i] = par_dpdv[i - 11];
   par_mix [11] *= f_dm * mtot; // normalise dpdv to make it a density (Msol/kpc3)
   int switch_f = 1;

   return los_integral_mix(par_mix, switch_f, psi_los, theta_los, l1, l2, eps);
}

//______________________________________________________________________________
double jsub_continuum(double const &ntot_subs, double par_dpdv[10], double const &psi_los,
                      double const &theta_los, double const &l1, double const &l2,
                      double par_subs[25], double  &m1, double  &m2)
{
   //--- Computes the continuum J-factor from all the subclumps in the F.O.I
   //    and in the mass range [m1, m2]. We have:
   //       Jsub_continuum(F.O.I; [m1,m2]) = ntot_subs * int_m1^m2 L(M) dP/dM dM
   //       int_DOmega int_los dpdv(r) * L(r) dl dOmega.
   //       - [Msol^2/kpc^5] for annihilation;
   //       - [Msol/kpc^2] for decay.
   //    N.B.: Quantities are given in proper coordinates.
   //
   //  ntot_subs     Total number of clumps in the host halo
   //  par_dpdv[0]   dPdV_Host normalisation  [kpc^{-3}]
   //  par_dpdv[1]   dPdV_Host scale radius [kpc]
   //  par_dpdv[2]   dPdV_Host shape parameter #1
   //  par_dpdv[3]   dPdV_Host shape parameter #2
   //  par_dpdv[4]   dPdV_Host shape parameter #3
   //  par_dpdv[5]   dPdV_Host card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV_Host radius [kpc]
   //  par_dpdv[7]   l_HC: distance observer to Host Centre [kpc]
   //  par_dpdv[8]   psi_HC: longitude of dpdv [rad]
   //  par_dpdv[9]   theta_HC: latitude of dpdv [rad]
   //  psi_los       psi_los: longitude for the l.o.s [rad]
   //  theta_los     theta_los: latitude for the l.o.s [rad]
   //  l1            Lower value for the l.o.s. integration boundary [kpc]
   //  l2            Upper value for the l.o.s. integration boundary [kpc]
   //  par_subs[0]   rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_subs[1]   rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_subs[2]   rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_subs[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]   host halo outer radius where to stop integration [kpc]
   //  par_subs[5]   eps: relative precision sought L calculation
   //  par_subs[6]   z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]   mdelta: mass of rho_cl(r)
   //  par_subs[8]   dPdM normalisation
   //  par_subs[9]   dPdM slope alphaM
   //  par_subs[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_subs[11]  dPdc mean concentration <c>
   //  par_subs[12]  dPdc standard deviation             [UNUSED if gENUM_CVIR_DIST=kDIRAC]
   //  par_subs[13]  dPdc card_profile [gENUM_CVIR_DIST] [UNUSED if gENUM_CVIR_DIST=kDIRAC]
   //  par_subs[14]  f: mass fraction of substructures   [only for ANNIHILATION and nlevel>1]
   //  par_subs[15]  nlevel: sub-sub...halos (1=no sub)  [only for ANNIHILATION and nlevel>1]
   //  par_subs[16]  dPdV normalisation  [kpc^{-3}]      [only for ANNIHILATION and nlevel>1]
   //  par_subs[17]  dPdV scale radius [kpc]             [only for ANNIHILATION and nlevel>1]
   //  par_subs[18]  dPdV shape parameter #1             [only for ANNIHILATION and nlevel>1]
   //  par_subs[19]  dPdV shape parameter #2             [only for ANNIHILATION and nlevel>1]
   //  par_subs[20]  dPdV shape parameter #3             [only for ANNIHILATION and nlevel>1]
   //  par_subs[21]  dPdV card_profile [gENUM_PROFILE]   [only for ANNIHILATION and nlevel>1]
   //  par_subs[22]  host halo outer radius (unused)     [only for ANNIHILATION and nlevel>1]
   //  par_subs[23]  ratio rs dPdV to rs_cl              [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]  R_host (corresp. to current dPdV)   [only for ANNIHILATION and nlevel>1]
   //  m1            Lower value of the mass range of subs considered [Msol]
   //  m2            Upper value of the mass range of subs considered [Msol]

   // N.B.: false flag: dP/dM not normalized!
   return ntot_subs * mean1cl_jn(par_dpdv, psi_los, theta_los,
                                 l1, l2, par_subs,
                                 m1, m2, 1, false);
}

//______________________________________________________________________________
void stref17_load_cmin200_vs_rgal(string const &filename, vector <double> &rgal_kpc, vector <double> &cmin_200)
{
   //--- Loads r_gal and cmin_200 for semi-analytical modelling à la Stref & Lavalle, PRD 95 (2017)
   // INPUTS:
   //  filename          File to read
   // OUTPUTS:
   //  rgal_kpc          Galactic position [kpc]
   //  cmin_200          Minimal concentration (c200) allowed at rgal for not being destructed

   // Open file and check if exists
   ifstream f_c200(filename.c_str());
   if (!f_c200) {
      printf("\n====> ERROR: stref17_load_cmin200_vs_rgal() in clumps.cc");
      printf("\n             Cannot open (and read) file %s", filename.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }

   rgal_kpc .clear();
   cmin_200.clear();

   // Read file (if reach this point, file exists)
   string line = "";
   while (getline(f_c200, line)) {
      // if it is a comment (start with #) or a blank line, skip it
      string line_trimmed = removeblanks_from_startstop(line);
      if (line_trimmed[0] == '#' || line_trimmed.empty()) continue;
      double r_tmp = 0., c_tmp = 0.;
      sscanf(line_trimmed.c_str(), "%le %le", &r_tmp, &c_tmp);
      rgal_kpc.push_back(r_tmp);
      cmin_200.push_back(c_tmp);
   };
   return;
}

//______________________________________________________________________________
void   stref17_load_lcrit(string const &filename, vector <double> &stref17_mmin, vector <double> &stref17_mmax, vector <double> &stref17_lcrit)
{
   //--- Loads l_crit for various mass decades for semi-analytical modelling à la Stref & Lavalle, PRD 95 (2017)
   // INPUTS:
   //  filename          File to read
   // OUTPUTS:
   //  rgal_kpc          Galactic position [kpc]
   //  cmin_200          Minimal concentration (c200) allowed at rgal for not being destructed

   // Open file and check if exists
   ifstream f_lcrit(filename.c_str());
   if (!f_lcrit) {
      printf("\n====> ERROR: stref17_load_lcrit() in clumps.cc");
      printf("\n             Cannot open (and read) file %s", filename.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }

   stref17_mmin .clear();
   stref17_mmax.clear();
   stref17_lcrit.clear();

   // Read file (if reach this point, file exists)
   string line = "";
   while (getline(f_lcrit, line)) {
      // if it is a comment (start with #) or a blank line, skip it
      string line_trimmed = removeblanks_from_startstop(line);
      if (line_trimmed[0] == '#' || line_trimmed.empty()) continue;
      double m1_tmp = 0., m2_tmp = 0., tmp = 0.;
      sscanf(line_trimmed.c_str(), "%le : %le  %le", &m1_tmp, &m2_tmp, &tmp);
      stref17_mmin.push_back(m1_tmp);
      stref17_mmax.push_back(m2_tmp);
      stref17_lcrit.push_back(tmp);
   };
   return;
}

//______________________________________________________________________________
void stref17_load_rt2rs_vs_rgal_c200(string const &filename, vector <double> &rgal_kpc, vector <double> &c200, vector< vector<double> > &rt2rs)
{
   //--- Loads r_gal and c200 and corresponding rt/rs for semi-analytical modelling à la Stref & Lavalle, PRD 95 (2017)
   // INPUTS:
   //  filename          File to read
   // OUTPUTS:
   //  rgal_kpc          Galactic position [kpc]
   //  c200              Concentration (c200) [-]
   //  rt2rs             epsilon=rt/rs (tidal radius/scale radius) for a given rgal_kpc and c200

   // Open file and check if exists
   ifstream f_rt2rs(filename.c_str());
   if (!f_rt2rs) {
      printf("\n====> ERROR: stref17_load_rt2rs_vs_rgal_c200() in clumps.cc");
      printf("\n             Cannot open (and read) file %s", filename.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }

   rgal_kpc .clear();
   c200.clear();
   rt2rs.clear();

   // Read file (if reach this point, file exists)
   string line = "";
   bool is_rgal_read = false;
   while (getline(f_rt2rs, line)) {
      // if it is a comment (start with #) or a blank line, skip it
      string line_trimmed = removeblanks_from_startstop(line);
      if (line_trimmed[0] == '#' || line_trimmed.empty()) continue;
      double r_tmp = 0., c_tmp = 0.;

      vector<double> tmp;
      string2list(line_trimmed, " ", tmp);
      if (!is_rgal_read) {
         // First line contains c200 values (except first dummy entry)
         tmp.erase(tmp.begin(), tmp.begin() + 1);
         c200 = tmp;
         is_rgal_read = true;
      } else {
         // First entry is rgal, and rest is rt2rs
         rgal_kpc.push_back(tmp[0]);

         // Check that correct number of entries per line
         tmp.erase(tmp.begin(), tmp.begin() + 1);
         if (tmp.size() != c200.size()) {
            printf("\n====> ERROR: stref17_load_rt2rs_vs_rgal_c200() in clumps.cc");
            printf("\n             Different number of entries bewteen first line and line\n %s", line_trimmed.c_str());
            printf("\n             => abort()\n\n");
            abort();
         }

         // add to rt2rs
         rt2rs.push_back(tmp);
      }
   };
   return;
}

//______________________________________________________________________________
double lum_singlehalo_nosubs(double par_tot[7], double const &eps, bool is_force_int, bool is_verbose)
{
   //--- Returns L the "intrinsic luminosity" of a DM halo for a halo (no substructures):
   //    It returns:
   //       L_prop = (1 + z)^3 L_com = 4*Pi*\int_0^Rvir r^2 rho^2 dr  [Msol^2/kpc3] if annihilation
   //       L_prop = L_com = 4*Pi*\int_0^Rvir r^2 rho dr  [Msol] if decay (i.e. mass)
   //    N.B.: Quantities are given in proper coordinates.
   //
   // par_tot[0]     rho(r) normalisation [Msol/kpc^{-3}]
   // par_tot[1]     rho(r) scale radius [kpc]
   // par_tot[2]     rho(r) shape parameter #1
   // par_tot[3]     rho(r) shape parameter #2
   // par_tot[4]     rho(r) shape parameter #3
   // par_tot[5]     rho(r) card_profile [gENUM_PROFILE]
   // par_tot[6]     rho(r) radius where to stop integration [kpc]
   // eps            Relative precision sought for integration
   // is_force_int   Force numerical integration even if analytical form is coded
   // is_verbose     Chatter on or off

   // We create a dummy par_mix[21] to call lum_singlehalo_nosubs_mix(). In particular,
   // par_mix[10]=0 ensures that the remaining parameters do not matter. We thus set:
   //   par_mix[0...6] = par_tot[0...6]
   //   par_mix[7...10] = 0
   //   par_mix[11...20] is UNUSED
   const int n_par_mix = 21;
   double par_mix[n_par_mix];
   for (int i = 0; i < 7; ++i)
      par_mix[i] = par_tot[i];
   for (int i = 7; i < 11; ++i)
      par_mix[i] = 0.;
   for (int i = 11; i < n_par_mix; ++i)
      par_mix[i] = 0.;

   return lum_singlehalo_nosubs_mix(par_mix, eps, is_force_int, is_verbose);
}

//______________________________________________________________________________
double lum_singlehalo_nosubs_mix(double par_mix[21], double const &eps, bool is_force_int, bool is_verbose)
{
   //--- Returns the "intrinsic luminosity" of a DM halo for a halo combo rho_mix,
   //    which is expressed as a function of rho_1 and rho_2 (passed in par_mix[21]).
   //    It returns:
   //       L_prop = (1 + z)^3 L_com = 4*Pi*\int_0^Rvir r^2 rho_mix^2 dr  [Msol^2/kpc3] if annihilation
   //       L_prop = L_com = 4*Pi*\int_0^Rvir r^2 rho_mix dr  [Msol] if decay (i.e. mass)
   //    N.B.: Quantities are given in proper coordinates.
   //
   //  par_mix[0]    rho_1(r) normalisation [Msol/kpc^{-3}]
   //  par_mix[1]    rho_1(r) scale radius [kpc]
   //  par_mix[2]    rho_1(r) shape parameter #1
   //  par_mix[3]    rho_1(r) shape parameter #2
   //  par_mix[4]    rho_1(r) shape parameter #3
   //  par_mix[5]    rho_1(r) card_profile [gENUM_PROFILE]
   //  par_mix[6]    rho_1(r) host radius [kpc]
   //  par_mix[7]    UNUSED
   //  par_mix[8]    UNUSED
   //  par_mix[9]    UNUSED
   //  par_mix[10]   switch_rho: selects which combination of rho_1 and rho_2 to use
   //                   0 -> rho_mix(r) = rho_1(r)
   //                   1 -> rho_mix(r) = rho_1(r)-rho_2(r)
   //                   2 -> rho_mix(r) = rho_1(r)*rho_2(r)
   //  par_mix[11]   rho_2(r) normalisation [Msol/kpc^{-3}]
   //  par_mix[12]   rho_2(r) scale radius [kpc]
   //  par_mix[13]   rho_2(r) shape parameter #1
   //  par_mix[14]   rho_2(r) shape parameter #2
   //  par_mix[15]   rho_2(r) shape parameter #3
   //  par_mix[16]   rho_2(r) card_profile [gENUM_PROFILE]
   //  par_mix[17]   rho_2(r) host radius [kpc]
   //  par_mix[18]   UNUSED
   //  par_mix[19]   UNUSED
   //  par_mix[20]   UNUSED
   //  eps           Relative precision sought for integration
   //  is_force_int  Force numerical integr. (even if analyt. coded)
   //  is_verbose    Chatter on or off


   if (gPP_DM_IS_ANNIHIL_OR_DECAY) {  // Annihilating DM
      // Analytic solution for kNFW97
      if (!is_force_int && int(par_mix[10]) == 0
            && int(par_mix[5]) == kZHAO && fabs(par_mix[2] - 1.) < 1.e-5
            && fabs(par_mix[3] - 3.) < 1.e-5
            && fabs(par_mix[4] - 1.) < 1.e-5) {
         // NFW97
         return 4.*PI / 3.*par_mix[1] * par_mix[1] * par_mix[1] * par_mix[0] * par_mix[0]
                * (1. - pow(1. + par_mix[6] / par_mix[1], -3.)) ;
      }

      // Else, numerical integration
      double I1 = 0., I2 = 0., I12 = 0.;
      double thresh_crit = 1.e-3;
      double rmin = 0.;
      double rmax = par_mix[6];
      double rsat = get_rsat(par_mix);
      double r_crit = thresh_crit * rmax;
      if (rsat > rmax) rsat = rmax;
      if (r_crit > rmax) r_crit = rmax;

      // Split integral in two or three parts
      if (rsat != 0. && rsat < r_crit) {
         // rho(r) is constant for r<rsat
         rho_mix(rmin, par_mix, I1);
         I1 = (rsat * rsat * rsat) * I1 * I1 / 3.;
         double Itmp = 0.;
         double thresh_sat = 1.e-7;
         double rsat_new = thresh_sat * par_mix[6];
         if (rsat_new > rmax) rsat_new = rmax;
         if (rsat < rsat_new) {
            if (rsat < 1.e-40)
               simpson_lin_adapt(r2rho2_mix, rsat, rsat_new, par_mix, Itmp, eps);
            else
               simpson_log_adapt(r2rho2_mix, rsat, rsat_new, par_mix, Itmp, eps);
            rsat = rsat_new;
         }
         simpson_log_adapt(r2rho2_mix, rsat, r_crit, par_mix, I12, eps);
         I12 += Itmp;
         simpson_log_adapt(r2rho2_mix, r_crit, rmax, par_mix, I2, eps);
      } else {
         simpson_lin_adapt(r2rho2_mix, rmin, r_crit, par_mix, I1, eps, is_verbose);
         simpson_log_adapt(r2rho2_mix, r_crit, rmax, par_mix, I2, eps, is_verbose);
      }
      return 4. * PI * (I1 + I12 + I2);

   } else { // Decaying DM
      double I1 = 0., I2 = 0., I3 = 0.;
      double thresh_crit = 1.e-3;
      double r_min = 0.;
      double rsat = get_rsat(par_mix);
      double rmax = par_mix[6];
      double r_crit = thresh_crit * par_mix[6];
      double eps2 = eps * 10.;
      // Numerical integration
      if (rsat != 0. && rsat < r_crit) {
         double Itmp = 0.;
         // rho(r) is constant for r<rsat
         rho_mix(r_min, par_mix, I1);

         I1 = (rsat * rsat * rsat) / 3. * I1;
         double thresh_sat = 1.e-7;
         if (rsat < thresh_sat * par_mix[6]) {
            double rsat_new = thresh_sat * par_mix[6];
            if (rsat < 1.e-50) rsat = 1.e-50;
            simpson_lin_adapt(r2rho_mix, rsat, rsat_new, par_mix, Itmp, eps, is_verbose);
            rsat = rsat_new;
         }
         simpson_log_adapt(r2rho_mix, rsat, r_crit, par_mix, I2, eps2, is_verbose);
         I2 += Itmp;
         simpson_log_adapt(r2rho_mix, r_crit, rmax, par_mix, I3, eps, is_verbose);
      } else {
         simpson_lin_adapt(r2rho_mix, r_min, r_crit, par_mix, I2, eps2, is_verbose);
         simpson_log_adapt(r2rho_mix, r_crit, rmax, par_mix, I3, eps, is_verbose);
      }
      return 4. * PI * (I1 + I2 + I3);
   }
   return 0.;
}

//______________________________________________________________________________
double lum_singlehalo_subs(double par_host[26], bool is_force_int, bool is_verbose)
{

   //--- Returns L(M,c) the "intrinsic luminosity" of a DM single halo rho_cl(r)
   //    for annihilation (use lum_singlehalo_nosubs() for decay) in [Msol^2/kpc3].
   //   It returns  L_prop = (1 + z)^3 L_com
   //    This accounts for contributions of multi-level of substructures (recursively).
   //    Each substructure level (n levels in total) is described by its mass clump
   //    distribution dPdM, spatial dPdV and concentration dP/dc distributions,
   //    DM fraction, etc.:
   //       L_n(M,c) = L_sm(M) + L_crossprod(M) +
   //          Ntot(M) \int_{Mmin}^{Mmax(M)} L_{n-1}(M') * dP/dM' dM'
   //    with (for annihilation)
   //       L_sm(M) = 4*Pi*\int_0^Rvir r^2 rho_sm^2 dr
   //       Lcrossprod(M) = 2* 4*Pi* \int_0^Rvir r^2 rho_sm * <rho_subs> dr  [Msol^2/kpc3]
   //    and
   //       L_1(M,c) = 4*Pi*\int_0^Rvir r^2 rho_tot^2 dr
   //    N.B.: Quantities are given in proper coordinates.
   //
   // INPUTS:
   //  par_host[0]   rho_cl(r) normalisation [Msol/kpc^{-3}]
   //  par_host[1]   rho_cl(r) scale radius [kpc]
   //  par_host[2]   rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_host[3]   rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_host[4]   rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_host[5]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_host[6]   rho_cl(r) radius where to stop integration
   //  par_host[7]   eps: relative precision sought L calculation
   //  par_host[8]   z: redshift of the halo hosting the sub-clumps
   //  par_host[9]   mdelta: mass of rho_cl(r)
   //  par_host[10]  dPdM normalisation
   //  par_host[11]  dPdM slope alphaM
   //  par_host[12]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_host[13]  dPdc mean concentration <c>
   //  par_host[14]  dPdc standard deviation             [UNUSED if gENUM_CVIR_DIST=kDIRAC]
   //  par_host[15]  dPdc card_profile [gENUM_CVIR_DIST] [UNUSED if gENUM_CVIR_DIST=kDIRAC]
   //  par_host[16]  f: mass fraction of substructures   [only for ANNIHILATION and nlevel>1]
   //  par_host[17]  nlevel: sub-sub...halos (1=no sub)  [only for ANNIHILATION and nlevel>1]
   //  par_host[18]  dPdV normalisation  [kpc^{-3}]      [only for ANNIHILATION and nlevel>1]
   //  par_host[19]  dPdV scale radius [kpc]             [only for ANNIHILATION and nlevel>1]
   //  par_host[20]  dPdV shape parameter #1             [only for ANNIHILATION and nlevel>1]
   //  par_host[21]  dPdV shape parameter #2             [only for ANNIHILATION and nlevel>1]
   //  par_host[22]  dPdV shape parameter #3             [only for ANNIHILATION and nlevel>1]
   //  par_host[23]  dPdV card_profile [gENUM_PROFILE]   [only for ANNIHILATION and nlevel>1]
   //  par_host[24]  dPdV radius [kpc]                   [only for ANNIHILATION and nlevel>1]
   //  par_host[25]  ratio rs dPdV to rs_cl              [only for ANNIHILATION and nlevel>1]

   // nlevel: number of level of substructures to consider
   //   * 1 -> no substructures
   //   * 2 -> one level (sub-substructures)
   //   * 3 -> two levels (sub-ssub-structures)
   //   * etc/
   int nlevel = (int)par_host[17];
   double mdelta = par_host[9];
   double eps = par_host[7];


   // If DECAY or nlevel = 1 => direct calculation
   if (!gPP_DM_IS_ANNIHIL_OR_DECAY || nlevel == 1)
      return lum_singlehalo_nosubs(par_host, eps, is_force_int, is_verbose);


   // Annihilation if nlevel>1
   // ------------------------
   // The luminosity L_n with n levels of subsub... for annihilation:
   // It is linked to L_{n-1} at the previous level (function stops when n=1)
   //    L_{n}(M,c) = L_{sm}(M) + L_{cross-prod}(M)
   //           + Ntot(M)\int_{Mmin}^{Mmax(M)} L_{n-1} dP/dM' (M') dM'.

   // Calculate dpdv parameters (in current host halo)
   const int n_pardpdv = 7;
   double pardpdv[n_pardpdv];
   pardpdv[0] = 1.;
   for (int k = 0; k < 5; k++)
      pardpdv[k + 1] = par_host[k + 19];
   pardpdv[6] = par_host[6]; // same Rvir as host
   dpdv_setnormprob(pardpdv, eps); //normalisation of dpdv w.r.t. volume V(m)
   // update dpdV normalisation in par_host:
   par_host[18] = pardpdv[0];

   double res = 0.;
   double f = par_host[16]; // mass fraction of substructures

   // 1. Calculate L_{sm}(M): requires lum_singlehalo_nosubs_mix(), with rho_sm=rho_tot-<rho_sub>.
   // The parameters are the following (we set psi and theta towards the
   //    par_mix[0-6]   rho_tot(r): norm + rs + shape #(1,2,3)+ card_profile + Rvir
   //    par_mix[7-9]   UNUSED
   //    par_mix[10]      switch_rho: selects which combination of rho_1 and rho_2 to use
   //                     0 -> rho_mix(r) = rho_1(r)
   //                     1 -> rho_mix(r) = rho_1(r)-rho_2(r)
   //                     2 -> rho_mix(r) = rho_1(r)*rho_2(r)
   //    par_mix[11-17] <rho_sub(r)>: norm + rs + shape #(1,2,3)+ card_profile + Rvir
   //    par_mix[18-20] UNUSED
   // Fill par_mix[21]
   const int n_parmix = 21;
   double par_mix[n_parmix];
   for (int i = 0; i < 7; ++i) // [0-6] => rho_tot
      par_mix[i] = par_host[i];
   for (int i = 7; i < 10; ++i) // [7-9] => UNUSED
      par_mix[i] = 0.;
   for (int i = 18; i < n_parmix; ++i) // [18-20] => UNUSED
      par_mix[i] = 0.;
   par_mix[10] = 1.;  // [10] => 1 to get (rho_tot - <rho_sub>)
   for (int i = 11; i < 18; ++i) // [11-17] dpdv parameters
      par_mix[i] = pardpdv[i - 11];
   // N.B.: <rho_sub>_norm = f*m*dpdv
   par_mix[11] *= f * mdelta;
   res += lum_singlehalo_nosubs_mix(par_mix, eps, is_force_int, is_verbose);

   // 2. Calculate L_{cross-product}(M) for rho_sm*<rho_sub> that we write
   // int{(rho_tot-<rho_sub>)*<rho_sub>} = int{rho_tot<rho_sub>)}*int{<rho_sub>^2}
   //  First term: int{rho_tot*<rho_sub>):
   //     - par_mix[10] set to 2 to get product of densities in lum_singlehalo_nosubs_mix
   //     - gPP_DM_IS_ANNIHIL_OR_DECAY is set to DECAY (not to take the square of rho_tot*<rho_sub>
   par_mix[10] = 2;
   bool ref_is_annihil = gPP_DM_IS_ANNIHIL_OR_DECAY;
   gPP_DM_IS_ANNIHIL_OR_DECAY = false;
   res += 2.*lum_singlehalo_nosubs_mix(par_mix, eps, is_force_int, is_verbose);
   //  Second term: int{<rho_sub>^2)
   gPP_DM_IS_ANNIHIL_OR_DECAY = ref_is_annihil;
   res -= 2.*lum_singlehalo_nosubs(&par_mix[11], eps, is_force_int, is_verbose);

   // 3. Calculate  Ntot(M)\int_{Mmin}^{Mmax(M)} L_{n-1} dP/dM' (M) dM'
   // N.B.: Mmax(M) = largest sub-halo mass allowed in host (mass mdelta)
   double mmax = gDM_SUBS_MMAXFRAC * mdelta; // maximum mass of subclumps in mdelta
   double msub_tot = f * mdelta;
   if (mmax > gDM_SUBS_MMIN && msub_tot > gDM_SUBS_MMIN) {
      dpdm_setnormprob(&par_host[10], gDM_SUBS_MMIN, mmax, eps);
      double nsubtot = nsubtot_from_msubtot(msub_tot, gDM_SUBS_MMIN, mmax, &par_host[10], eps);

      // Create par_subsub[25] with:
      //    * par_subsub[0-23] = par_host[2-24]
      // and update
      //    * mass of subsubs (parsubsub[7])
      //    * level of subsubs  (parsubsub[15])
      const int n_parsubsub = 25;
      double par_subsub[n_parsubsub];
      par_host[17] -= 1;
      for (int i = 0; i < n_parsubsub - 1; ++i)
         par_subsub[i] = par_host[i + 2];
      par_subsub[7] = 0.; // UNUSED
      if (gNAMES_SIMUMODES[gSIM_FLAG_MODE][0] == 'e')
         par_subsub[10] = gHALO_SUBS_FLAG_CDELTAMDELTA[kEXTRAGAL];

      // N.B.: dpdm already normalized above (so do not need to do it again)
      double result = nsubtot * mean1cl_lumn_fullhalo(par_subsub, gDM_SUBS_MMIN, mmax, 1, false);
      res += result;
   }
   return res;
}

//______________________________________________________________________________
double mass_singlehalo(double par_tot[7], double const &eps, bool is_force_int, bool is_verbose)
{
   //--- Returns total mass M= 4*Pi*\int_0^Rvir r^2 rho dr  [Msol].
   //
   //  par_tot[0]    DM density normalisation [Msol/kpc^3]
   //  par_tot[1]    Scale radius [kpc]
   //  par_tot[2]    Shape parameter #1
   //  par_tot[3]    Shape parameter #2
   //  par_tot[4]    Shape parameter #3
   //  par_tot[5]    card_profile [gENUM_PROFILE]
   //  par_tot[6]    Effective radius where to stop integration [kpc]
   //  eps           Relative precision sought for integration
   //  is_force_int  Force numerical integr. (even if analyt. coded)
   //  is_verbose    Chatter on or off


   // Analytic solution for kNFW97, kEINASTO_N and kZHAO
   if (!is_force_int) {
      if (int(par_tot[5]) == kEINASTO_N) {
         // par_tot[2] = n
         double dn = 3.*par_tot[2] - 0.333333333 + 0.0079 / par_tot[2];
         double g_3n = 1.;
         double xbar = dn * pow(par_tot[6] / par_tot[1], 1. / par_tot[2]);
         //    Gamma(n) = (n - 1)!
         for (int i = 2; i < int(par_tot[2] * 3); ++i)
            g_3n *= double(i);
         // Eq. (20) - (23) Merrit et al. (2006)
         return 4. *  PI * par_tot[2] * par_tot[1] * par_tot[1] * par_tot[1] * par_tot[0]
                * exp(dn) * pow(dn, -(3. * par_tot[2]))
                * gsl_sf_gamma_inc_P(3 * par_tot[2], xbar) * g_3n;

      } else if (int(par_tot[5]) == kZHAO
                 && fabs(par_tot[2] - 1.) < 1.e-5
                 && fabs(par_tot[3] - 3.) < 1.e-5
                 && fabs(par_tot[4] - 1.) < 1.e-5) {
         // NFW97
         double tmp = par_tot[6] / par_tot[1];
         return 4. * PI * pow(par_tot[1], 3.)
                * par_tot[0] * (log1p(tmp) - tmp / (1 + tmp));

      } else if (int(par_tot[5]) == kZHAO) {
         // kZHAO with special function Gauss Hypergeom
         double a = (3. - par_tot[4]) / par_tot[2];
         double b = (par_tot[3] - par_tot[4]) / par_tot[2];
         double c = (3. - par_tot[4] + par_tot[2]) / par_tot[2];

         double x = -pow(par_tot[6] / par_tot[1], par_tot[2]);

         // x must be lower than 1, + issues for gsl for values
         // too close to 1; a b and c not negative
         if (x > 0.995 || a <= 0. || b <= 0. || c <= 0.) {
            //cout << "Use of Numerical integration for mass calculation" << endl;
            return mass_singlehalo(par_tot, eps, true, is_verbose);
         }

         gsl_sf_result r;
         int test = 0;

         // Different cases
         if (x >= 0.) {
            test = gsl_sf_hyperg_2F1_e(a, b, c, x, &r);
            if (test != 0) {
               //cout << "Use of Numerical integration for mass calculation" << endl;
               return mass_singlehalo(par_tot, eps, true, is_verbose);
            } else
               return (4.*PI * par_tot[0] * pow(par_tot[1], 3.)) / (3 - par_tot[4])
                      * pow(par_tot[6] / par_tot[1], 3 - par_tot[4])
                      * gsl_sf_hyperg_2F1(a, b, c, x);
         } else {
            double xtilde = 1. - 1. / (1. - x);
            // N.B.: xtilde = 1 is excluded because hyperg_2F1 is defined on ]-infty,1[
            if (xtilde > 0.995 || c - a <= 0. || b <= 0. || c <= 0.) {
               //cout << "Use of Numerical integration for mass calculation" << endl;
               return mass_singlehalo(par_tot, eps, true, is_verbose);
            }

            test = gsl_sf_hyperg_2F1_e(c - a, b, c, xtilde, &r);
            if (test != 0) {
               //cout << "Use of Numerical integration for mass calculation" << endl;
               return mass_singlehalo(par_tot, eps, true, is_verbose);
            } else
               return (4.*PI * par_tot[0] * pow(par_tot[1], 3.)) / (3 - par_tot[4])
                      * pow(par_tot[6] / par_tot[1], 3 - par_tot[4])
                      * gsl_sf_hyperg_2F1(c - a, b, c, xtilde) / pow(1. - x, b);
         }
      }
   }


   // Otherwise, numerical integration!

   // First integration is from 0 to r_min (with r_min set to rsat if exists)
   // Note that the max or plateau of r^n*rho to integrate happens for ~ rs
   double res = 0.;
   double zero = 0.;
   double rsat = get_rsat(par_tot);
   double r_min = 1.e-6 * par_tot[1];
   double r_max = par_tot[6];
   if (rsat > 1.e-50) {
      // rho(r) is constant for r<rsat
      rho(zero, par_tot, res);
      res *= (rsat * rsat * rsat) / 3.;
      r_min = rsat;
   } else {
      // Integrate from 0. to r_min linearly
      zero = 0.;
      simpson_lin_adapt(r2rho, zero, r_min, par_tot, res, eps, is_verbose);
   }

   double integ_low = 0.;
   double integ_up = 0.;
   double integ_rs = 0.;
   r3rho(r_min, par_tot, integ_low);
   r3rho(r_max, par_tot, integ_up);
   r3rho(par_tot[1], par_tot, integ_rs);

   double r1 = 0.;
   double r2 = 0.;
   double r3 = 0.;
   if (integ_rs < integ_low || integ_rs < integ_up) {
      r1 = par_tot[1] / 5.;
      r2 = par_tot[1];
      r3 = 5.*par_tot[1];
   } else {
      // look for the radius of the maximum of r3rho (integration in log step)
      double x_valmax = 0.;
      double valmax = goldenmin_logstep(r_min, par_tot[1], r_max, r3rho, par_tot, 0.01, false, x_valmax);
      r2 = x_valmax;

      double fraction = 0.5;
      double y_ref = valmax * fraction;

      // find the radii where the integrand is half the maximum
      if (y_ref > integ_low)
         find_x_logstep(y_ref, r1, r_min, x_valmax, r3rho, par_tot, 0.01);
      else //y_res will not be in [r_min, x_valmax]
         r1 = par_tot[1] / 5.;

      if (y_ref > integ_up)
         find_x_logstep(y_ref, r3, x_valmax, r_max, r3rho, par_tot, 0.01);
      else //y_res will not be in [x_valmax, r_max]
         r3 = 5.*par_tot[1];
   }

   // Compute the rest of the integration on the ranges
   //    [r_min,r1] + [r1,r2] + [r2,r3]
   double tmp_res = 0.;
   vector<double> rr;
   rr.push_back(r_min);
   if (r1 > r_min && r1 < r_max)
      rr.push_back(r1);
   if (r2 > r_min && r2 < r_max)
      rr.push_back(r2);
   if (r3 > r_min && r3 < r_max)
      rr.push_back(r3);
   rr.push_back(r_max);

   // If starting point is 0, use lin integration
   if (rr[0] < 1.e-40)
      simpson_lin_adapt(r2rho, rr[0], rr[1], par_tot, tmp_res, eps, is_verbose);
   else
      simpson_log_adapt(r2rho, rr[0], rr[1], par_tot, tmp_res, eps, is_verbose);
   res += tmp_res;

   for (int i = 1; i < (int)rr.size() - 1; ++i) {
      simpson_log_adapt(r2rho, rr[i], rr[i + 1], par_tot, tmp_res, eps, is_verbose);
      res += tmp_res;
   }
   return 4. * PI * res;
}

//______________________________________________________________________________
double mass_triaxialhalo(double par_tot[7], double axes[3], double const &eps, bool is_verbose)
{
   //--- Returns total mass M [Msol] up to the radius par_tot[6]. Because the profile
   //    is triaxial, a triple integration is required, and we rely on los_integral()
   //    to do it (switch_f=2 integrates just the desired quantity, see integr_los.h).
   //
   //  par_tot[0]    DM density normalisation [Msol/kpc^3]
   //  par_tot[1]    Scale radius [kpc]
   //  par_tot[2]    Shape parameter #1
   //  par_tot[3]    Shape parameter #2
   //  par_tot[4]    Shape parameter #3
   //  par_tot[5]    card_profile [gENUM_PROFILE]
   //  par_tot[6]    Effective radius where to stop integration [kpc]
   //  axes[3]       Major(a)/second(b)/minor(c) axes (major/minor along x/z axis)
   //  eps           Relative precision sought for integration
   //  is_verbose    Chatter on or off


   // los_integral() requires 10 parameters
   const int n_par = 10;
   double par_int[n_par];
   // Copy first 7 parameters
   for (int i = 0; i < 7; ++i)
      par_int[i] = par_tot[i];
   // Fill new ones
   for (int i = 7; i < 10; ++i)
      par_int[i] = 0.;
   // Remaining parameters
   int switch_f = 2;
   double lmin = 0.;
   double lmax = par_tot[6];
   double psi_los = 0;
   double theta_los = 0.;

   //int enum_halo = 0; //string_to_enum("FLAG_TYPEHALOES", list_halos[i].Type);

   // Set halo triaxial parameters: depending on the condition,
   // either gHALO_TRIAXIAL_XXX or gMW_TRIAXIAL_XXX is used,
   // so we set both (and store reference values for the calculation,
   // reset after calculation)

   // keep reference value
   bool gal_ref_is = gMW_TRIAXIAL_IS;
   bool halo_ref_is = gHALO_TRIAXIAL_IS;
   vector<double> gal_ref_axes, gal_ref_eulerangles, halo_ref_axes, halo_ref_eulerangles;
   for (int i = 0; i < 3; ++i) {
      gal_ref_axes.push_back(gMW_TRIAXIAL_AXES[i]);
      gal_ref_eulerangles.push_back(gMW_TRIAXIAL_ROTANGLES[i]);
      halo_ref_axes.push_back(gHALO_TRIAXIAL_AXES[i]);
      halo_ref_eulerangles.push_back(gHALO_TRIAXIAL_ROTANGLES[i]);
   }
   double alpha_int_ref = gSIM_ALPHAINT;


   // Set triaxial parameters
   gHALO_TRIAXIAL_IS = true;
   gMW_TRIAXIAL_IS = true;
   for (int i = 0; i < 3; ++i) {
      gHALO_TRIAXIAL_AXES[i] = axes[i];
      gHALO_TRIAXIAL_ROTANGLES[i] = 0.;
      gMW_TRIAXIAL_AXES[i] = axes[i];
      gMW_TRIAXIAL_ROTANGLES[i] = 0.;
   }

   // Set alpha_int integration angle to full space!
   gSIM_ALPHAINT = 2.* PI;

   double mass = los_integral(par_int, switch_f, psi_los, theta_los, lmin, lmax, eps, is_verbose);
   // Reset halo triaxiality to default parameters
   gHALO_TRIAXIAL_IS = false;
   for (int i = 0; i < 3; ++i) {
      gHALO_TRIAXIAL_AXES[i] = 1.;
      gHALO_TRIAXIAL_ROTANGLES[i] = 0.;
   }

   // Reset to reference value
   gMW_TRIAXIAL_IS = gal_ref_is;
   gHALO_TRIAXIAL_IS = halo_ref_is;
   for (int i = 0; i < 3; ++i) {
      gMW_TRIAXIAL_AXES[i] = gal_ref_axes[i];
      gMW_TRIAXIAL_ROTANGLES[i] = gal_ref_eulerangles[i];
      gHALO_TRIAXIAL_AXES[i] = halo_ref_axes[i];
      gHALO_TRIAXIAL_ROTANGLES[i] = halo_ref_eulerangles[i];
   }
   gSIM_ALPHAINT = alpha_int_ref;

   return mass;
}

//______________________________________________________________________________
/*double mdelta_to_cdelta(double const &mdelta, double const &Delta_c, int card_cdelta, double par_prof[4], double const &z)
{
   //--- Returns the concentration parameter c_delta from mdelta.
   //    For parametrizations with a radial dependence from the host halo center,
   //    the mean c(M) = 4 * Pi * int_0_rvir r^2 dPdV * c(M,r) dr is given.
   //    Note that Delta_c is
   //       - Delta_c=200 (kNETO07_200, kDUFFY08F_200, kETTORI10_200, kPRADA12_200,
   //         kPIERI11_VIALACTEA, kPIERI11_AQUARIUS, kSANCHEZ14_200, kMOLINE17_200)
   //       - Delta_c=Delta_vir(z) (kB01_VIR, kB01_VIR_RAD, kENS01_VIR, kDUFFY08F_VIR, and kGIOCOLI12_VIR)
   //       - Delta_c=200*omega_m (kDUFFY08F_MEAN)
   //
   //  mdelta        Mass of the halo, depending on Delta_c [Msol]
   //  Delta_c       Critical overdensity factor
   //  card_cdelta   c_delta-mdelta choice of parameterisation (from gENUM_CDELTAMDELTA)
   //  par_prof[0]   DM profile shape parameter #1 (only needed when input Delta_c does not correspond to chosen card_cdelta profile)
   //  par_prof[1]   DM profile shape parameter #2 (only needed when input Delta_c does not correspond to chosen card_cdelta profile)
   //  par_prof[2]   DM profile shape parameter #3 (only needed when input Delta_c does not correspond to chosen card_cdelta profile)
   //  par_prof[3]   card_profile [gENUM_PROFILE]  (only needed when input Delta_c does not correspond to chosen card_cdelta profile)
   //  z             Redshift of the halo

   if (card_cdelta == kPIERI11_AQUARIUS || card_cdelta == kPIERI11_VIALACTEA || card_cdelta == kB01_VIR_RAD || card_cdelta == kMOLINE17_200) {

      double rvir = -1;
      const int npar1 = 14;
      double par_dpdv_c[npar1];
      string mode = gNAMES_SIMUMODES[gSIM_FLAG_MODE];
      static bool is_warning = false;

      if (gSIM_FLAG_MODE == ke2 or mode[0] == 'g') {
         rvir = gMW_RMAX;
         gal_set_pardpdv(par_dpdv_c);
         if (!is_warning) {
            print_warning("clumps.cc", "mdelta_to_cdelta()", "Distance-dependent cdelta-mdelta profile " + string(gNAMES_CDELTAMDELTA[card_cdelta])
                          + " is averaged over Milky Way halo dp/dV parameters.");
            is_warning = true;
         }
      } else if (mode[0] == 'h') {
         if (!is_warning) {
            print_error("clumps.cc", "mdelta_to_cdelta()", "Distance-dependent cdelta-mdelta profile " + string(gNAMES_CDELTAMDELTA[card_cdelta])
                        + " is not (yet) implemented for halo objects.");
            //is_warning = true;
         }
      }

      // calculate mean <c(M)>
      double rmin = 1e-10 * rvir;
      double rmax = rvir;
      par_dpdv_c[10] = mdelta;
      par_dpdv_c[11] = card_cdelta;
      par_dpdv_c[12] = z;
      par_dpdv_c[13] = Delta_c;

      double cM = 0.;
      simpson_log_adapt(r2dpdv_c, rmin, rmax, par_dpdv_c, cM, gSIM_EPS);
      //cM = mdelta_to_cdelta(mdelta, card_cdelta, z, 50., 260.); // uncomment this if you want to compute c(M) compute at a specific radius R.
      return cM;
   } else {
      // skip integration, as c(M) != c(M,r)
      double dummyposition = -999;
      return mdelta_to_cdelta(mdelta, Delta_c, card_cdelta, par_prof, z, dummyposition);
   }
}*/

//______________________________________________________________________________
double mdelta_to_cdelta(double const &mdelta, double const &Delta_c, int card_cdelta, double par_prof[4], double const &z, double const &xpos)
{
   //--- Returns the concentration parameter c_delta from mdelta. Note that delta is
   //       - Delta_c=200 (kNETO07_200, kDUFFY08F_200, kETTORI10_200, kPRADA12_200,
   //         kPIERI11_VIALACTEA, kPIERI11_AQUARIUS, kSANCHEZ14_200, kMOLINE17_200)
   //       - Delta_c=Delta_vir(z) (kB01_VIR, kB01_VIR_RAD, kENS01_VIR, kDUFFY08F_VIR, and kGIOCOLI12_VIR)
   //       - Delta_c=200*omega_m (kDUFFY08F_MEAN)
   //
   //  mdelta        Mass of the halo (Mdelta depending on card_cdelta) [Msol]
   //  Delta_c       Critical overdensity factor
   //  card_cdelta   c_delta-mdelta choice of parameterisation (from gENUM_CDELTAMDELTA)
   //  par_prof[0]   DM profile shape parameter #1 (only needed when input Delta_c does not correspond to chosen card_cdelta profile)
   //  par_prof[1]   DM profile shape parameter #2 (only needed when input Delta_c does not correspond to chosen card_cdelta profile)
   //  par_prof[2]   DM profile shape parameter #3 (only needed when input Delta_c does not correspond to chosen card_cdelta profile)
   //  par_prof[3]   card_profile [gENUM_PROFILE]  (only needed when input Delta_c does not correspond to chosen card_cdelta profile)
   //  z             Redshift of the halo
   //  xpos          distance r/rhost from the host halo center in case of a distance-dependent c(M) choice

   char char_tmp[512];

   if (card_cdelta == kPIERI11_AQUARIUS || card_cdelta == kPIERI11_VIALACTEA || card_cdelta == kB01_VIR_RAD || card_cdelta == kMOLINE17_200) {
      if (fabs(xpos + 1.) < SMALL_NUMBER) {
         static bool is_warning_average = true;
         if (is_warning_average) {
            is_warning_average = false;
            print_error("clumps.cc", "mdelta_to_cdelta()", "Distance-dependent c(M) chosen with xpos = -1.");
         }
      }
   }

   double Delta_c_ref = card_cdelta_to_Delta(card_cdelta, z);

   if (fabs(Delta_c  - Delta_c_ref) / Delta_c_ref > SMALL_NUMBER) {
      //cout << " Delta_c = " << Delta_c << ", Delta_c_ref = " << Delta_c_ref << endl;
      // have to compute cdelta corresponding to the input Delta_c
      if (!gSIM_MDELTA_TO_PAR_METHOD) {
         double Rdelta = mdelta_to_Rdelta(mdelta, Delta_c, z);
         double rscale = mdelta_to_rscale(mdelta, Delta_c, card_cdelta, par_prof, z, xpos);
         return Rdelta / get_r_2_from_rscale(rscale, par_prof);
      } else
         return solve_cdelta_celine(mdelta, Delta_c, card_cdelta, par_prof, z, xpos);
   } //else cout << "proceed directly" << endl;

   // else return values from formulae:
   switch (card_cdelta) {
      case kB01_VIR: { // Fit function: Lavalle et al. (2008) on Bullock et al. (2001) data
            // c_vir=f(M_vir)
            double log_M = log(mdelta);
            return exp(4.339267 - 3.8366e-2 * log_M - 3.91e-4 * pow(log_M, 2.)
                       - 2.e-6 * pow(log_M, 3.) - 5.5e-7 * pow(log_M, 4.)) / (1. + z);
         }
      case kB01_VIR_RAD: { // Fit function: Lavalle et al. (2008) on Bullock et al. (2001) data
            // c_vir=f(M_vir), scaled to radially-dependent concentration, acc. to Kuhlen et al (2008)
            double c_field = mdelta_to_cdelta(mdelta, Delta_c, kB01_VIR, par_prof, z);
            double alpha_r = 0.286;
            return c_field * pow(xpos, -alpha_r);
         }
      case kENS01_VIR: { // Fit function: Lavalle et al. (2008) on Eke et al. (2001) data
            // c_vir=f(M_vir)
            double log_M = log(mdelta);
            return exp(3.14 - 0.018 * log_M - 4.1e-4 * pow(log_M, 2.)) / (1. + z);
         }
      case kNETO07_200: { // Neto et al 2007 (Eq. 5)
            // c_200 = f(M_200)
            return 4.67 * pow(mdelta * gCOSMO_HUBBLE / 1.e14, -0.11) / (1. + z);
         }
      // Duffy et al. (2008) provides fit for 3 different halo definitions (F sample 0-2)
      // and fitting a NFW or Einasto profile, respectively
      case kDUFFY08F_VIR: { // Duffy et al 0804.2486
            // c_vir=f(M_vir)
            double A, B, C;
            if (gHALO_SUBS_FLAG_PROFILE[kEXTRAGAL] == kEINASTO) {
               A = 8.82;
               B = -0.106;
               C = -0.87;
            } else {
               A = 7.85;
               B = -0.081;
               C = -0.71;
            }
            return A * pow(mdelta * gCOSMO_HUBBLE / 2.e12, B) * pow(1. + z, C);
         }
      case kDUFFY08F_200: { // Duffy et al 0804.2486
            // c_200 = f(M_200)
            double A, B, C;
            if (gHALO_SUBS_FLAG_PROFILE[kEXTRAGAL] == kEINASTO) {
               A = 6.40;
               B = -0.108;
               C = -0.62;
            } else {
               A = 5.71;
               B = -0.084;
               C = -0.47;
            }
            return A * pow(mdelta * gCOSMO_HUBBLE / 2.e12, B) * pow(1. + z, C);
         }
      case kDUFFY08F_MEAN: { // Duffy et al 0804.2486
            // c_mean = f(M_mean)
            double A, B, C;
            if (gHALO_SUBS_FLAG_PROFILE[kEXTRAGAL] == kEINASTO) {
               A = 11.39;
               B = -0.107;
               C = -1.16;
            } else {
               A = 10.14;
               B = -0.081;
               C = -1.01;
            }
            return A * pow(mdelta * gCOSMO_HUBBLE / 2.e12, B) * pow(1. + z, C);
         }
      case kETTORI10_200: { // Ettori et al A&A524A68(2010)
            // c_200 = f(M_200)
            return pow(10, 0.62) * pow(mdelta / 1.e15, -0.1) / (1 + z);
         }
      case kPRADA12_200: { // Prada et al 1104.5130
            // Eqs. (12), (13)
            if (z > 6) {
               printf("\n====> ERROR: mdelta_to_cdelta() in clumps.cc");
               printf("\n             z>6 for kPRADA12_200 is out of range");
               printf("\n             => abort()\n\n");
               abort();
            }

            int growth_method = kHEATH77;
            // print warning if used outside extragalactic mode:
            static bool is_egal = false;
            if (!is_egal) {
               if (gNAMES_SIMUMODES[gSIM_FLAG_MODE][0] != 'e') {
                  sprintf(char_tmp, "cdelta-mdelta profile %s is only suited for extragalactic halos. "
                          "Its computation depends on gSIM_EXTRAGAL_FLAG_WINDOWFUNC = %s, "
                          "gSIM_EXTRAGAL_FLAG_GROWTHFACTOR = %s and gDM_KMAX = %g, which are hardcoded for this run."
                          , gNAMES_CDELTAMDELTA[card_cdelta],  gNAMES_WINDOWFUNC[gSIM_EXTRAGAL_FLAG_WINDOWFUNC], gNAMES_GROWTHFACTOR[gSIM_EXTRAGAL_FLAG_GROWTHFACTOR], gDM_KMAX);
                  print_warning("clumps.cc", "mdelta_to_cdelta()", string(char_tmp));
                  is_egal = true;
               }
            }
            // only valid for WMAP7 cosmology, so check it (only once):
            bool is_wmap7;

            if (fabs((gCOSMO_HUBBLE - 0.7) / gCOSMO_HUBBLE) < 0.05 and
                  fabs((gCOSMO_OMEGA0_M - 0.27) / gCOSMO_OMEGA0_M) < 0.05 and
                  fabs((gCOSMO_OMEGA0_LAMBDA - 0.73) / gCOSMO_OMEGA0_LAMBDA) < 0.05 and
                  fabs((gCOSMO_OMEGA0_B - 0.0469) / gCOSMO_OMEGA0_B) < 0.05 and
                  fabs((gCOSMO_SIGMA8   - 0.82) / gCOSMO_SIGMA8) < 0.05 and
                  fabs((gCOSMO_N_S      - 0.95) / gCOSMO_N_S) < 0.05) {
               is_wmap7 = true;
            } else {
               static bool is_wmap7_warning = false;
               if (!is_wmap7_warning) {
                  print_warning("clumps.cc", "mdelta_to_cdelta()", "cdelta-mdelta profile " + string(gNAMES_CDELTAMDELTA[card_cdelta]) + " is only valid for WMAP7 cosmology."
                                " (Your cosmology differs from Prada et al. (2012) by more than 5%%)");
                  is_wmap7_warning = true; // only to suppress print out after first occurence.
               }
               is_wmap7 = false;
            }

            double a = 1. / (1. + z);
            double x = pow(gCOSMO_OMEGA0_LAMBDA / gCOSMO_OMEGA0_M, 1. / 3.) * a;
            double sigma;
            if (!is_wmap7) {
               // we (pre-)compute sigma from a provided linear power spectrum
               if (vec_m200.size() == 0 || matrix_sigma2.size() == 0) {
                  // precompute sigma(M,z) for interpolation later. We will do it on a fixed
                  // mass grid of M200c = [HALO_MMIN, 4 * HALO_MMAX]:
                  double mmin = HALO_MMIN; // Msol
                  double mmax = 4 * HALO_MMAX; // Msol
                  double mhmin = mmin * gCOSMO_M_to_MH;
                  double mhmax = mmax * gCOSMO_M_to_MH;
                  vec_m200 =  make_1D_grid(mmin, mmax, gSIM_EXTRAGAL_NM_PRECOMP, gSIM_EXTRAGAL_M_PRECOMP_ISLOG);
                  vector<double> vec_mh200 =  make_1D_grid(mhmin, mhmax, gSIM_EXTRAGAL_NM_PRECOMP, gSIM_EXTRAGAL_M_PRECOMP_ISLOG);

                  // we will compute P(k) at redshift zero and then use the growth factor for higher redshift:
                  vector<double> linear_lnk_vec_0, linear_lnp_vec_0;
                  get_pk(0, linear_lnk_vec_0, linear_lnp_vec_0);
                  double sigma8_input = sigma8(linear_lnk_vec_0, linear_lnp_vec_0, gSIM_EXTRAGAL_FLAG_WINDOWFUNC);
                  for (int j = 0; j < int(linear_lnp_vec_0.size()); ++j) {
                     linear_lnp_vec_0[j] *= (gCOSMO_SIGMA8 / sigma8_input);
                  }
                  if (gCOSMO_Z_GRID.size() == 0) {
                     // we are probably in the -e2 mode:
                     gCOSMO_Z_GRID.push_back(z);
                     gCOSMO_Z_GRID.push_back(z + SMALL_NUMBER);
                  }
                  for (int i = 0; i < (int)gCOSMO_Z_GRID.size(); i++) {
                     matrix_sigma2.push_back(sigma2_andderiv(gCOSMO_Z_GRID[i], vec_mh200, linear_lnk_vec_0, linear_lnp_vec_0, gSIM_EXTRAGAL_FLAG_WINDOWFUNC, growth_method)[0]);
                  }
               }
               // now get sigma(M):
               sigma = sqrt(interp2D(z, mdelta, gCOSMO_Z_GRID, vec_m200, matrix_sigma2, kLINLOG));

            } else {
               // we use the analytic formula for sigma given by Prada et al., 2012
               double y = 1.e12 / (gCOSMO_HUBBLE * mdelta);
               sigma = growth_factor(z, growth_method) * (16.9 * pow(y, 0.41) / (1. + 1.102 * pow(y, 0.2) + 6.22 * pow(y, 0.333)));
            }

            // Eq. (22):
            double sig0 = 1.047, sig1 = 1.646, beta = 7.386, x1 = 0.526;
            // Eq. (20):
            double sigmin_x    = sig0 + (sig1 - sig0) * (1. / PI * atan(beta * (x     - x1)) + 0.5);
            double sigmin_1393 = sig0 + (sig1 - sig0) * (1. / PI * atan(beta * (1.393 - x1)) + 0.5);
            // Eq. (18):
            double B1x = sigmin_x / sigmin_1393;

            // Eq. (15):
            double sigprime = B1x * sigma;

            // Eq. (17):
            double A = 2.881, b = 1.257, c = 1.022, d = 0.06;
            // Eq. (16):
            double Csigprime = A * (pow(sigprime / b, c) + 1.) * exp(d / (sigprime * sigprime));

            // Eq. (21):
            double c0 = 3.681, c1 = 5.033, alpha = 6.948, x0 = 0.424;
            // Eq. (19):
            double cmin_x    = c0 + (c1 - c0) * (1. / PI * atan(alpha * (x     - x0)) + 0.5);
            double cmin_1393 = c0 + (c1 - c0) * (1. / PI * atan(alpha * (1.393 - x0)) + 0.5);
            // Eq. (18):
            double B0x = cmin_x / cmin_1393;

            // determine the redshift-dependent maximum mass until c(M,z) is valid:
            int n_points = 6;
            vector<double> z_base, logMh_base;
            double logMh_lim[6][2] = {
               {0, 15},
               {0.5, 14.8},
               {1, 14.6},
               {2, 14},
               {4, 13},
               {6, 11.6},
            };
            for (int i = 0; i < n_points; ++i) {
               z_base.push_back(logMh_lim[i][0]);
               logMh_base.push_back(logMh_lim[i][1]);
            }
            //double Mlim = pow(10, interp1D(z, z_base, logMh_base, kLINLIN, true)) / gCOSMO_HUBBLE;
            //if (mdelta < Mlim)
            return B0x * Csigprime;
            //else return max(1e-2, B0x * Csigprime * exp(0.5 * (1 - mdelta / Mlim)));
         }
      case kGIOCOLI12_VIR: { // Giocoli et al. 1111.6977
            const int n_z = 6;
            const int n_pars = 7;
            double z_fit[n_z] = {0., 0.25, 0.5, 1., 2., 4.001};
            double pars[n_z][n_pars] = {
               { 9.20785e+00, 1.87887e+00, -2.31723e-02, -2.29525e-03, 2.23096e+00, -5.55248e-02, -2.95938e-03},
               { 9.31712e+00, 1.70574e+00, -1.50209e-02, -2.64492e-03, 2.11544e+00, -6.41604e-02, -2.09662e-03},
               { 9.31481e+00, 1.59139e+00, -1.66454e-02, -2.28732e-03, 2.03006e+00, -7.32695e-02, -1.25920e-03},
               { 9.40100e+00, 1.39885e+00, -1.54266e-02, -1.93542e-03, 1.87667e+00, -8.34109e-02, -1.13856e-04},
               { 9.68604e+00, 1.14163e+00, -1.02095e-02, -1.71120e-03, 1.61898e+00, -8.58056e-02, 9.74857e-04},
               { 9.63169e+00, 8.85101e-01, -3.94524e-03, -1.35504e-03, 1.23256e+00, -6.24208e-02, 9.54376e-04}
            };

            int i_interp = 0;
            if (z > 1.e-5 && z <= z_fit[1])
               i_interp = 1;
            else if (z > z_fit[1] && z <= z_fit[2])
               i_interp = 2;
            else if (z > z_fit[2] && z <= z_fit[3])
               i_interp = 3;
            else if (z > z_fit[3] && z <= z_fit[4])
               i_interp = 4;
            else if (z > z_fit[4] && z <= z_fit[5])
               i_interp = 5;
            else if (z > 1.e-5) {
               printf("\n====> ERROR: mdelta_to_cdelta() in clumps.cc");
               printf("\n             z>4 for kGIOCOLI12_VIR is out of range");
               printf("\n             => abort()\n\n");
               abort();
            }

            double log10_m = log10(mdelta);
            double log10_cvir_up = 0.;
            if (log10_m < pars[i_interp][0])
               log10_cvir_up = pars[i_interp][1] + pars[i_interp][2] * log10_m + pars[i_interp][3] * log10_m * log10_m;
            else
               log10_cvir_up = pars[i_interp][4] + pars[i_interp][5] * log10_m + pars[i_interp][6] * log10_m * log10_m;

            if (i_interp == 0)
               return pow(10., log10_cvir_up);
            else {
               double log10_cvir_low = 0.;
               if (log10_m < pars[i_interp - 1][0])
                  log10_cvir_low = pars[i_interp - 1][1] + pars[i_interp - 1][2] * log10_m + pars[i_interp - 1][3] * log10_m * log10_m;
               else
                  log10_cvir_low = pars[i_interp - 1][4] + pars[i_interp - 1][5] * log10_m + pars[i_interp - 1][6] * log10_m * log10_m;
               return pow(10., linlin_interp(z, z_fit[i_interp - 1], z_fit[i_interp], log10_cvir_low, log10_cvir_up));
            }
         }
      case kPIERI11_VIALACTEA: { // Pieri et al. 0908.0195, with Via Lactea parameters
            double alpha_r = 0.286;
            double c1 = 119.75;
            double c2 = -85.16;
            double alpha_1 = 0.012;
            double alpha_2 = 0.0026;
            double c_200 = pow(xpos, -alpha_r) * (c1 * pow(mdelta, -alpha_1) + c2 * pow(mdelta, -alpha_2));
            return c_200 / (1. + z);
         }
      case kPIERI11_AQUARIUS: { // Pieri et al. 0908.0195, with Aquarius parameters
            double alpha_r = 0.237;
            double c1 = 232.15;
            double c2 = -181.74;
            double alpha_1 = 0.0146;
            double alpha_2 = 0.008;
            double c_200 = pow(xpos, -alpha_r) * (c1 * pow(mdelta, -alpha_1) + c2 * pow(mdelta, -alpha_2));
            return c_200 / (1. + z);
         }
      case kSANCHEZ14_200: { // Sanchez-Conde and Prada 1312.1729
            // c_200 = f(M_200)
            double c_200 = 0;
            double c_arr[6] = {37.5153, -1.5093, 1.636e-2, 3.66e-4, -2.89237e-5, 5.32e-7};
            int i = 0;
            while (i < 6) {
               c_200 += c_arr[i] * pow(log(mdelta * gCOSMO_HUBBLE), i);
               i++;
            }
            return c_200 / (1. + z);
         }
      case kMOLINE17_200: { // Moline et al. 1603.04057
            // c_200 = f(M_200)
            double c_200 = 0;
            double c_arr[3] = { -0.195, 0.089, 0.089};
            for (int i = 1; i <= 3; ++i)
               c_200 += pow(c_arr[i - 1] * log10(1.e-8 * mdelta * 0.71), i);
            c_200 = 19.9 * (1. + c_200) * (1. - 0.54 * log10(xpos));
            return c_200 / (1. + z);
         }
      case kLUDLOW16_200: { // Ludlow et al. 1601.02624
            // c_200 = f(nu(M_200))
            int growth_method = kHEATH77;
            // print warning if used outside extragalactic mode:
            static bool is_egal = false;
            if (!is_egal) {
               if (gNAMES_SIMUMODES[gSIM_FLAG_MODE][0] != 'e') {
                  sprintf(char_tmp, "cdelta-mdelta profile %s is only suited for extragalactic halos. "
                          "Its computation depends on gSIM_EXTRAGAL_FLAG_WINDOWFUNC = %s, "
                          "gSIM_EXTRAGAL_FLAG_GROWTHFACTOR = %s and gDM_KMAX = %g, which are hardcoded for this run."
                          , gNAMES_CDELTAMDELTA[card_cdelta],  gNAMES_WINDOWFUNC[gSIM_EXTRAGAL_FLAG_WINDOWFUNC], gNAMES_GROWTHFACTOR[gSIM_EXTRAGAL_FLAG_GROWTHFACTOR], gDM_KMAX);
                  print_warning("clumps.cc", "mdelta_to_cdelta()", string(char_tmp));
                  is_egal = true;
               }
            }
            // only valid for Planck cosmology, so check it (only once):
            static bool is_planck = false;
            if (!is_planck) {
               if (fabs((gCOSMO_HUBBLE - 0.678) / gCOSMO_HUBBLE) < 0.05 and
                     fabs((gCOSMO_OMEGA0_M - 0.308) / gCOSMO_OMEGA0_M) < 0.05 and
                     fabs((gCOSMO_OMEGA0_LAMBDA - 0.692) / gCOSMO_OMEGA0_LAMBDA) < 0.05 and
                     fabs((gCOSMO_OMEGA0_B - 0.048) / gCOSMO_OMEGA0_B) < 0.05 and
                     fabs((gCOSMO_SIGMA8   - 0.82) / gCOSMO_SIGMA8) < 0.05 and
                     fabs((gCOSMO_N_S      - 0.96) / gCOSMO_N_S) < 0.05) {
                  is_planck = true;
               } else {
                  print_warning("clumps.cc", "mdelta_to_cdelta()", "cdelta-mdelta profile " + string(gNAMES_CDELTAMDELTA[card_cdelta]) + " is only valid for WMAP7 cosmology."
                                " (Your cosmology differs from Planck (2015) by more than 5%%)");
                  is_planck = true; // only to suppress print out after first occurence.
               }
            }

            // use analytic formula for exact cosmology as in Ludlow et al. (2016):
            bool is_ludlowcosmo = false; // hardcoded switched off

//            if (fabs((gCOSMO_HUBBLE - 0.678) / gCOSMO_HUBBLE) < SMALL_NUMBER and
//                  fabs((gCOSMO_OMEGA0_M - 0.308) / gCOSMO_OMEGA0_M) < SMALL_NUMBER and
//                  fabs(gCOSMO_OMEGA0_K) < SMALL_NUMBER and
//                  fabs((gCOSMO_OMEGA0_B - 0.048) / gCOSMO_OMEGA0_B) < SMALL_NUMBER and
//                  fabs((gCOSMO_SIGMA8   - 0.82) / gCOSMO_SIGMA8) < SMALL_NUMBER and
//                  fabs((gCOSMO_N_S      - 0.96) / gCOSMO_N_S) < SMALL_NUMBER) {
//               is_ludlowcosmo = true;
//            } else {
//               is_ludlowcosmo = false;
//            }

            double sqrtnu;
            if (!is_ludlowcosmo) {
               if (vec_m200.size() == 0 || matrix_nu.size() == 0) {
                  // precompute nu(M,z) for interpolation later. We will do it on a fixed
                  // mass grid of M200c = [HALO_MMIN, 4 * HALO_MMAX]:
                  double mmin = HALO_MMIN; // Msol
                  double mmax = 4 * HALO_MMAX; // Msol
                  double mhmin = mmin * gCOSMO_M_to_MH;
                  double mhmax = mmax * gCOSMO_M_to_MH;
                  vec_m200 =  make_1D_grid(mmin, mmax, gSIM_EXTRAGAL_NM_PRECOMP, gSIM_EXTRAGAL_M_PRECOMP_ISLOG);
                  vector<double> vec_mh200 =  make_1D_grid(mhmin, mhmax, gSIM_EXTRAGAL_NM_PRECOMP, gSIM_EXTRAGAL_M_PRECOMP_ISLOG);

                  // we will compute P(k) at redshift zero and then use the growth factor for higher redshift:
                  vector<double> linear_lnk_vec_0, linear_lnp_vec_0;
                  get_pk(0, linear_lnk_vec_0, linear_lnp_vec_0);
                  double sigma8_input = sigma8(linear_lnk_vec_0, linear_lnp_vec_0, gSIM_EXTRAGAL_FLAG_WINDOWFUNC);
                  for (int j = 0; j < int(linear_lnp_vec_0.size()); ++j) {
                     linear_lnp_vec_0[j] *= (gCOSMO_SIGMA8 / sigma8_input);
                  }
                  if (gCOSMO_Z_GRID.size() == 0) {
                     // we are probably in the -e2 mode:
                     gCOSMO_Z_GRID.push_back(z);
                     gCOSMO_Z_GRID.push_back(z + SMALL_NUMBER);
                  }
                  for (int i = 0; i < (int)gCOSMO_Z_GRID.size(); i++) {
                     matrix_nu.push_back(nu_andderiv(gCOSMO_Z_GRID[i], vec_mh200, linear_lnk_vec_0, linear_lnp_vec_0, gSIM_EXTRAGAL_FLAG_WINDOWFUNC, growth_method)[0]);
                  }
               }
               // get nu(M):  NOTE: We define nu=(delta_collapse/sigma)^2
               sqrtnu = sqrt(interp2D(z, mdelta, gCOSMO_Z_GRID, vec_m200, matrix_nu, kLINLOG));
            } else {
               double y = 1.e10 / (gCOSMO_HUBBLE * mdelta);
               double sigma = growth_factor(z, growth_method) * (22.26 * pow(y, 0.292) / (1. + 1.53 * pow(y, 0.275) + 3.36 * pow(y, 0.198)));
               sqrtnu = DELTA_COLLAPSE / sigma;
            }

            //cout << mdelta << " "<<  nu << endl;

            // get coefficients:
            double sqrtnu0, c0, beta, gamma1, gamma2;
            sqrtnu0 = (4.135 - 0.564 * (1 + z) - 0.210 * pow(1 + z, 2) + 0.0557 * pow(1 + z, 3) - 0.00348 * pow(1 + z, 4)) / growth_factor(z, growth_method);

            // only valid for positive nu0 and z < 9:
            static bool is_print = false;
            bool is_extrapol = false;
            double zref = z;
            if (sqrtnu0 < SMALL_NUMBER and z <= 7.5) {
               printf("\n====> ERROR: mdelta_to_cdelta() in clumps.cc");
               printf("\n               Sqrt(nu_0) <= 0 obtained for kLUDLOW16_200 at z = %g.", z);
               printf("\n             => abort()\n\n");
               abort();
            } else if (sqrtnu0 < SMALL_NUMBER or z > 7.5) {

               if (!is_print) {
                  string str_tmp = "";
                  if (sqrtnu0 < SMALL_NUMBER) {
                     sprintf(char_tmp, "Sqrt(nu_0) <= 0 obtained for kLUDLOW16_200 at z = %g. ", z);
                     str_tmp += string(char_tmp);
                  }
                  if (z > 9) {
                     sprintf(char_tmp, "kLUDLOW16_200 concentration parametrization only valid up to z = 9. ");
                     str_tmp += string(char_tmp);
                  }
                  str_tmp += "We will extrapolate from z = 7.5 as c = c(z = 7.5)/ (1 + z - 7.5)";
                  print_warning("clumps.cc", "mdelta_to_cdelta()", str_tmp);
               }
               is_print = true;
               zref = 7.5;
               sqrtnu0 = (4.135 - 0.564 * (1 + zref) - 0.210 * pow(1 + zref, 2) + 0.0557 * pow(1 + zref, 3) - 0.00348 * pow(1 + zref, 4)) / growth_factor(zref, growth_method);
               is_extrapol = true;
            }

            c0 = 3.395 * pow(1 + zref, -0.215);
            beta = 0.307 * pow(1 + zref, 0.540);
            gamma1 = 0.628 * pow(1 + zref, -0.047);
            gamma2 = 0.317 * pow(1 + zref, -0.893);
            double res = c0 * pow(sqrtnu / sqrtnu0, -gamma1) * pow(1 + pow(sqrtnu / sqrtnu0, 1. / beta), -beta * (gamma2 - gamma1));
            if (!is_extrapol) return res;
            else return res / (z - 6.5);
         }
      case kCORREA15_PLANCK_200: { // Ludlow et al. 1601.02624
            // only valid for Planck cosmology, so check it (only once):
            static bool is_planck = false;
            if (!is_planck) {
               if (fabs((gCOSMO_HUBBLE - 0.678) / gCOSMO_HUBBLE) < 0.05 and
                     fabs((gCOSMO_OMEGA0_M - 0.308) / gCOSMO_OMEGA0_M) < 0.05 and
                     fabs((gCOSMO_OMEGA0_B - 0.048) / gCOSMO_OMEGA0_B) < 0.05 and
                     fabs((gCOSMO_SIGMA8   - 0.82) / gCOSMO_SIGMA8) < 0.05 and
                     fabs((gCOSMO_N_S      - 0.96) / gCOSMO_N_S) < 0.05) {
                  is_planck = true;
               } else {
                  print_warning("clumps.cc", "mdelta_to_cdelta()", "cdelta-mdelta profile " + string(gNAMES_CDELTAMDELTA[card_cdelta]) + " is only valid for WMAP7 cosmology."
                                " (Your cosmology differs from Planck (2015) by more than 5%%)");
                  is_planck = true; // only to suppress print out after first occurence.
               }
            }
            if (z <= 4) {
               double alpha = 1.7543 - 0.2766 * (1 + z) + 0.02039 * pow(1 + z, 2.);
               double beta =  0.2753 + 0.00351 * (1 + z) - 0.3038 * pow(1 + z, 0.0269);
               double gamma = -0.01537 + 0.02102 * pow(1 + z, -0.1475);
               return pow(10, alpha + beta * log10(mdelta) * (1 + gamma * pow(log10(mdelta), 2.)));
            } else {
               double alpha = 1.3081 - 0.1078 * (1 + z) + 0.00398 * pow(1 + z, 2.);
               double beta =  0.0223 - 0.0944 * pow(1 + z, -0.3907);
               return pow(10, alpha + beta * log10(mdelta));
            }
         }
      case kROCHA13_SIDM_VIR: {
            if (int(par_prof[3]) != kBURKERT) {
               printf("\n====> ERROR: mdelta_to_cdelta() in clumps.cc");
               printf("\n             kROCHA13_SIDM_VIR corresponds");
               printf("\n             (and only corresponds to a) Burkert density profile.");
               printf("\n             => use kBURKERT density profile.");
               printf("\n             => abort()\n\n");
               abort();
            }

            double rhob = 2.9e7 * pow(mdelta / 1e10, -0.19);
            double rvir = mdelta_to_Rdelta(mdelta, Delta_c, z);
            double fac = mdelta / PI / rhob / pow(rvir, 3);
            // N.B.: as also elsewhere in the code, instead of dividing rhob by (1+z^3)
            // (as in Huetten et al., 2017) and having a (1+z) factor in rvir
            // we discard the canceling (1+z) factors from the beginning.

            gsl_function F;
            c_params_for_rootfinding params = {fac};
            F.function = &solve_c_SIDM_Burkert;
            F.params = &params;
            double c_lo = 2e-6;
            double c_hi = 1e3;
            if (z > 9 and mdelta > 1e15) return 2e-6;
            int return_status = 0;
            double cb = rootsolver_gsl(gsl_root_fsolver_falsepos, F, c_lo, c_hi, gSIM_EPS, return_status); // faster than gsl_root_fsolver_brent and gsl_root_fsolver_bisection
            return cb / get_ratio_r_2_rscale(par_prof);
         }
      default:
         printf("\n====> ERROR: mdelta_to_cdelta() in clumps.cc");
         printf("\n             card_cdelta chosen does not correspond to any concentration model");
         printf("\n             => abort()\n\n");
         abort();
   }
   return 0.;
}

//______________________________________________________________________________
void mdelta_to_innerslope(double const &mdelta, double const Delta_c, int const card_cdelta,
                          double par_prof[4], double const &z, double const &xpos)
{
   //--- Return the inner slope of the dark matter profile from the virial mass [Msol].
   //    The mass dependence of the inner slope is predicted for the smallest mass halos.
   // INPUTS:
   //  mdelta         Mass of the halo [Msol]
   //  Delta_c        Critical overdensity factor defining mdelta(z).
   //  card_cdelta    Choice of cdelta-mdelta parametrization (from gENUM_CDELTAMDELTA)
   //  par_prof[0]    Clump shape parameter #1
   //  par_prof[1]    Clump shape parameter #2
   //  par_prof[2]    Clump shape parameter #3
   //  par_prof[3]    card_profile [gENUM_PROFILE]
   //  z              Redshift of the halo
   //  xpos           distance r/rhost from the host halo center in case of a distance-dependent c(M) choice
   // OUTPUT:
   //  par_prof[0-2]

   int card_profile = (int)par_prof[3];
   switch (card_profile) {
      case kISHIYAMA14: {
            // Ishiyama uses a NFW profile, with inner slope gamma=par_prof[3]
            // The formula is given in Eq.(2):

            // get mvir according to Ishiyama (using Bryan and Norman's formula
            // for a flat universe)
            if (fabs(gCOSMO_OMEGA0_K) > SMALL_NUMBER) {
               printf("\n====> ERROR: mdelta_to_innerslope() in clumps.cc");
               printf("\n             kISHIYAMA14 model only defined for gCOSMO_OMEGA0_LAMBDA + gCOSMO_OMEGA0_M = 1.");
               printf("\n             => abort()\n\n");
               abort();
            }

            double delta_vir = delta_x_to_delta_crit(-1, kBRYANNORMAN98, z);
            double mvir = mdelta1_to_mdelta2(mdelta, Delta_c, card_cdelta, par_prof, z, delta_vir, xpos);

            double gamma = -0.123 * log10(1.e6 * mvir) + 1.461;
            if (gamma > 1.)
               par_prof[2] = gamma;
            else
               par_prof[2] = 1.;
            break;
         }
      default :
         return;
   }
   return;
}

//______________________________________________________________________________
void mdelta_to_par(double par_tot[7], double const &mdelta, double const &Delta_c, int const card_cdelta, double par_prof[4],
                   double const &eps, double const &z, double const &cdelta, double const &xpos)
{
   //--- Fills par_tot[7] containing structural parameters given the halo mass mdelta,
   //    the <cdelta>-mdelta relation - or a given concetration c> 0, and the DM profile shape parameters.
   //    Note that the returned profile corresponds to the total DM profile of the halo.
   //    N.B.: If cdelta>0, the profile parameters are set from c, otherwise, they
   //    are set from <cdelta> calculated from mdelta. In any case, Rdelta is calculated
   //    from mdelta.
   //    N.B.: Quantities are given in proper coordinates.
   //
   // INPUTS:
   //  mdelta         Mass of the halo [Msol]
   //  Delta_c        Critical overdensity factor defining mdelta(z).
   //  par_prof[0]    Clump shape parameter #1
   //  par_prof[1]    Clump shape parameter #2
   //  par_prof[2]    Clump shape parameter #3
   //  par_prof[3]    card_profile [gENUM_PROFILE]
   //  eps            Relative precision sought for integration
   //  z              Redshift of the halo
   //  cdelta         Concentration of the halo [used if > 0]
   //  xpos           distance r/rhost from the host halo center in case of a distance-dependent c(M) choice
   // OUTPUTS:
   //  par_tot[0]    Clump density normalisation [Msol/kpc^3]
   //  par_tot[1]    Clump scale radius, based on mdelta_to_rscale() [kpc]
   //  par_tot[2]    Shape parameter #1 (=par_prof[0])
   //  par_tot[3]    Shape parameter #2 (=par_prof[1])
   //  par_tot[4]    Shape parameter #3 (=par_prof[2])
   //  par_tot[5]    card_profile (=par_prof[3])
   //  par_tot[6]    Rdelta: virial radius [kpc]

   // Copy structural parameters
   par_tot[0] = 1.;
   for (int i = 2; i < 6; ++i)
      par_tot[i] = par_prof[i - 2];

   // Update inner profile (for mass-dependent profiles)
   mdelta_to_innerslope(mdelta, Delta_c, card_cdelta, par_prof, z, xpos);

   // calculate Rdelta
   par_tot[6] = mdelta_to_Rdelta(mdelta, Delta_c, z);

   // calculate rscale:
   if (cdelta > 1.e-5) { // Parameters from mdelta and cdelta
      par_tot[1] = get_rscale_from_r_2(par_tot[6] / cdelta, par_prof);
   } else {
      // Parameters from mdelta and <cdelta>
      if (!gSIM_MDELTA_TO_PAR_METHOD) {
         par_tot[1] = mdelta_to_rscale(mdelta, Delta_c, card_cdelta, par_prof, z, xpos);
      } else {
         double cDelta = solve_cdelta_celine(mdelta, Delta_c, card_cdelta, par_prof, z, xpos);
         par_tot[1] = get_rscale_from_r_2(par_tot[6] / cDelta, par_prof); // rs
      }
   }

//   cout << endl << mdelta << endl << cdelta <<  endl;
//
//   cout << par_tot[0] << endl;
//   cout << par_tot[1] << endl;
//   cout << par_tot[2] << endl;
//   cout << par_tot[3] << endl;
//   cout << par_tot[4] << endl;
//   cout << par_tot[5] << endl;
//   cout << par_tot[6] << endl;

   // Get rho_scale:
   set_par0_given_mref(par_tot, par_tot[6], mdelta, eps);
   //cout << par_tot[0] << endl;

   return;
}

//______________________________________________________________________________
double mdelta_to_Rdelta(double const &mdelta, double const &Delta_c, double const &z)
{
   //--- Returns Rdelta [kpc] from mdelta.
   //
   // INPUTS:
   //  mdelta         Mdelta of the halo, depending on Delta_c(z) [Msol]
   //  Delta_c        Critical overdensity factor defining mdelta(z).
   //  z              Redshift of the halo
   // OUTPUT:
   //  Rdelta         [kpc], proper coordinates

   return  pow(mdelta / (4.* PI / 3.* Delta_c * rho_crit(z)), 1. / 3.);
}

//______________________________________________________________________________
double mdelta_to_rscale(double const &mdelta, double const &Delta_c,
                        int const card_cdelta, double par_prof[4], double const &z,
                        double const &xpos)
{
   //--- Returns the scale radius r_s [kpc] from mdelta,
   //    given a defined DM halo profile and a cdelta-mdelta relation.
   //    N.B.: Quantities are given in proper coordinates.
   //
   // INPUTS:
   //  mdelta         Mass of the halo [Msol]
   //  Delta_c        Critical overdensity factor defining mdelta(z).
   //  card_cdelta    cdelta-mdelta choice of parameterisation (from gENUM_CDELTAMDELTA)
   //  par_prof[0]    DM profile shape parameter #1
   //  par_prof[1]    DM profile shape parameter #2
   //  par_prof[2]    DM profile shape parameter #3
   //  par_prof[3]    card_profile [gENUM_PROFILE]
   //  z              Redshift of the halo
   //  xpos           distance r/rhost from the host halo center in case of a distance-dependent c(M) choice
   // OUTPUT:
   //  r_s            scale radius r_s [kpc]

   // calculate mdelta_ref which corresponds to the used cdelta parametrization
   double mdelta_ref = mdelta1_to_mdelta2(mdelta, Delta_c, card_cdelta, par_prof, z, -1, xpos);

   double a = get_ratio_r_2_rscale(par_prof);
   double Delta_c_ref = card_cdelta_to_Delta(card_cdelta, z);
   double cdelta_ref = mdelta_to_cdelta(mdelta_ref, Delta_c_ref, card_cdelta, par_prof, z, xpos);
   return mdelta_to_Rdelta(mdelta_ref, Delta_c_ref, z) / (a * cdelta_ref);
}

//______________________________________________________________________________
double mdelta1_to_mdelta2(double const &mdelta1, double const &Delta1,
                          int const  card_cdelta, double par_prof[4], double const &z,
                          double Delta2, double const &xpos)
{
   //--- Transforms the mass mdelta1 [Msol] for given Delta1 into the mass corresponding to a Delta2,
   //    given a defined DM halo profile and a cdelta-mdelta relation.
   //
   // INPUTS:
   //  mdelta1        Mass of the halo corresponding to Delta1, [Msol]
   //  Delta1         Critical overdensity factor corresponding to mdelta1
   //  card_cdelta    cdelta-mdelta choice of parameterisation (from gENUM_CDELTAMDELTA)
   //  par_prof[0]    DM profile shape parameter #1
   //  par_prof[1]    DM profile shape parameter #2
   //  par_prof[2]    DM profile shape parameter #3
   //  par_prof[3]    card_profile [gENUM_PROFILE]
   //  z              Redshift of the halo
   //  Delta2         Critical overdensity factor corresponding to mdelta2 (if not given, Delta2 = Delta of cdelta-mdelta parametrization)
   //  xpos           distance r/rhost from the host halo center in case of a distance-dependent c(M) choice
   // OUTPUT:
   //  mdelta2        Mass of the halo corresponding to Delta2, [Msol]

   if (Delta2 < 0)
      Delta2 = card_cdelta_to_Delta(card_cdelta, z);

   // Do not do anything when Delta2 = Delta1:
   if (fabs(Delta2  - Delta1) / Delta1 < SMALL_NUMBER) {
      //cout << "proceed directly" << endl;
      return mdelta1; // we are done!
   }

   // define boundaries for root search:
   double m_lo;
   double m_hi;
   bool find_mref_given_m1;
   double Delta_c_ref = card_cdelta_to_Delta(card_cdelta, z);
   double mdelta_ref;

   gsl_function F;
   F.function = &solve_Mdelta;

   // First, find the mass M_Delta_ref corresponding to Mdelta_ref for the chosen cdelta-mdelta relation:
   if (fabs(Delta_c_ref  - Delta1) / Delta_c_ref < SMALL_NUMBER) {
      mdelta_ref = mdelta1; // this was easy...
   } else { // not so easy...
      find_mref_given_m1 = true;

      // we deal with monotonous functions, so we can cleverly guess the boundary values to start with:
      if (Delta1 < Delta_c_ref) {
         m_lo = mdelta1 * 1e-6;
         if (m_lo < HALO_MMIN and (card_cdelta == kLUDLOW16_200 or card_cdelta == kPRADA12_200)) m_lo = HALO_MMIN;
         m_hi = mdelta1;
      } else {
         m_lo = mdelta1;
         if (card_cdelta == kMOLINE17_200) m_lo *= 1e-1;
         m_hi = mdelta1 * 1e6;
         if (m_hi > 4 * HALO_MMAX and (card_cdelta == kLUDLOW16_200  or card_cdelta == kPRADA12_200)) m_hi = 4 * HALO_MMAX;
      }

      Mdelta_params_for_rootfinding params1 = {mdelta1, Delta1, card_cdelta,
                                               par_prof[0], par_prof[1], par_prof[2],
                                               int(par_prof[3]), z, xpos,
                                               find_mref_given_m1
                                              };
      F.params = &params1;
      int return_status = 0;
      mdelta_ref = rootsolver_gsl(gsl_root_fsolver_brent, F, m_lo, m_hi, gSIM_EPS, return_status);  // faster than gsl_root_fsolver_bisection  gsl_root_fsolver_falsepos
   }

   if (fabs(Delta2  - Delta_c_ref) / Delta_c_ref < SMALL_NUMBER) {
      return mdelta_ref; // we are done!
   }

   // else, transform now mdelta_ref to mdelta2:
   find_mref_given_m1 = false;
   Mdelta_params_for_rootfinding params2 = {mdelta_ref, Delta2, card_cdelta,
                                            par_prof[0], par_prof[1], par_prof[2],
                                            int(par_prof[3]), z, xpos,
                                            find_mref_given_m1
                                           };
   F.params = &params2;
   if (Delta2 > Delta_c_ref) {
      m_lo = mdelta_ref * 1e-6;
      if (m_lo < HALO_MMIN and (card_cdelta == kLUDLOW16_200 or card_cdelta == kPRADA12_200)) m_lo = HALO_MMIN;
      m_hi = mdelta_ref;
   } else {
      m_lo = mdelta_ref;
      m_hi = mdelta_ref * 1e6;
   }
   int return_status = 0;
   return rootsolver_gsl(gsl_root_fsolver_brent, F, m_lo, m_hi, gSIM_EPS, return_status);
}

//__________________________________________________________________________
double nsubtot_from_msubtot(double const &mtot_subs, double &mmin_subs,
                            double &mmax_subs, double par_dpdm[2],
                            double const &eps)
{
   //--- Returns the total number of subs in [mmin_subs, mmax_subs] to get a
   //    total mass of subs mtot_subs in the host halo.
   //    The total number N of sub-halos is
   //        d2N/dPdV = Ntot . dP/dV . dP/dM
   //        => dN/dM = Ntot . dP/dM
   //    Using
   //        Mtot_subhalos = int_mmin^mmax (M . dN/dM) dM
   //                      = Ntot_subhalos . int_mmin^mmax (M. dP/dM) dM
   //    leads to
   //        Ntot_subhalos = Mtot_subhalos / <M>
   //    with
   //        <M> = int_mmin^mmax (M. dP/dM) dM.
   //
   //  mtot_subs     Total mass of all sub-halos in  [mmin_subs,mmax_subs] [Msol]
   //  mmin_subs     Lower mass of sub-halos populating this halo [Msol]
   //  mmax_subs     Upper mass of sub-halos populating this halo [Msol]
   //  par_dpdm[0]   dPdM normalisation [1/Msol]
   //  par_dpdm[1]   dPdM slope alphaM
   //  eps           Relative precision sought for integration

   // Check that the normalisation is correct
   double norm = par_dpdm[0];
   par_dpdm[0] = 1;
   dpdm_setnormprob(par_dpdm, mmin_subs, mmax_subs, eps);
   if (fabs(norm - par_dpdm[0]) / par_dpdm[0] > max(1.e-2, eps)) {
      printf("\n====> ERROR: nsubtot_from_msubtot() in clumps.cc");
      printf("\n             Norm of par_dpdm is not correctly set:");
      printf("\n               mtot_subs=%.2le in [mmin_subs,mmax_subs]=[%.2le,%.2le]", mtot_subs, mmin_subs, mmax_subs);
      printf("\n               par_dpdm[0]=%.2le   par_dpdm[1]=%.2le", par_dpdm[0], par_dpdm[1]);
      printf("\n             => abort()\n\n");
      abort();
   }

   double m_mean = 0.;
   simpson_log_adapt(dpdm_m, mmin_subs, mmax_subs, par_dpdm, m_mean, eps);

   return mtot_subs / m_mean;
}

//______________________________________________________________________________
double nsubtot_from_nsubm1m2(double par_dpdm[2], double &m1, double &m2,
                             double const &nsub_in_m1m2, double const &eps)
{
   //--- Returns the total number of clumps in the whole mass range given the
   //    number in [m1,m2].
   //
   //  par_dpdm[0]   dPdM normalisation [1/Msol]
   //  par_dpdm[1]   dPdM slope alphaM
   //  m1            Lower mass of the range for which we know the number of clumps [Msol]
   //  m2            Upper mass of the range for which we know the number of clumps [Msol]
   //  nsub_in_m1m2  Number of clumps in the mass range [m1,m2]
   //  eps           Relative precision sought for the integration

   // We have
   //   dN/dM = N dP/dM
   // so that
   //   N_[M1-M2] = int_m1^m2 (dN/dM) dM = N int_m1^m2 (dP/dM) dM
   // which gives
   //   N = N_[M1-M2] / int_m1^m2 (dP/dM) dM
   return nsub_in_m1m2 / frac_nsubs_in_m1m2(par_dpdm, m1, m2, eps);
}

//______________________________________________________________________________
void print_pardpdv(double par_dpdv[10])
{
   //--- Print dpdv parameters.
   //
   //  par_dpdv[0]   dPdV_Host normalisation  [kpc^{-3}]
   //  par_dpdv[1]   dPdV_Host scale radius [kpc]
   //  par_dpdv[2]   dPdV_Host shape parameter #1
   //  par_dpdv[3]   dPdV_Host shape parameter #2
   //  par_dpdv[4]   dPdV_Host shape parameter #3
   //  par_dpdv[5]   dPdV_Host card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV_Host radius [kpc]
   //  par_dpdv[7]   l_HC: distance observer to Host Centre [kpc]
   //  par_dpdv[8]   psi_HC: longitude of dpdv [rad]
   //  par_dpdv[9]   theta_HC: latitude of dpdv [rad]

   printf("       ================== par_dpdv[10] ==================\n");
   printf("         [0] norm       = %le [kpc^{-3}]\n", par_dpdv[0]);
   printf("         [1] rs         = %le [kpc]\n", par_dpdv[1]);
   printf("         [2] shape#1    = %le [-]\n", par_dpdv[2]);
   printf("         [3] shape#2    = %le [-]\n", par_dpdv[3]);
   printf("         [4] shape#3    = %le [-]\n", par_dpdv[4]);
   printf("         [5] profile    = %d (%s)\n", (int)par_dpdv[5], gNAMES_PROFILE[(int)par_dpdv[5]]);
   printf("         [6] Rmax       = %le [kpc]\n", par_dpdv[6]);
   printf("         [7] d          = %le [kpc]\n", par_dpdv[7]);
   printf("         [8] psi(long)  = %le [rad]\n", par_dpdv[8]);
   printf("         [9] theta(lat) = %le [rad]\n", par_dpdv[9]);
   printf("\n");
}

//______________________________________________________________________________
void print_parhost(double par_host[26])
{
   //--- Print dpdv parameters.
   //
   //  par_host[0]   rho_cl(r) normalisation [Msol/kpc^{-3}]
   //  par_host[1]   rho_cl(r) scale radius [kpc]
   //  par_host[2]   rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_host[3]   rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_host[4]   rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_host[5]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_host[6]   rho_cl(r) radius where to stop integration
   //  par_host[7]   eps: relative precision sought L calculation
   //  par_host[8]   z: redshift of the halo hosting the sub-clumps
   //  par_host[9]   mdelta: mass of rho_cl(r)
   //  par_host[10]  dPdM normalisation
   //  par_host[11]  dPdM slope alphaM
   //  par_host[12]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_host[13]  dPdc mean concentration <c>
   //  par_host[14]  dPdc standard deviation             [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_host[15]  dPdc card_profile [gENUM_CDELTA_DIST] [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_host[16]  f: mass fraction of substructures   [only for ANNIHILATION and nlevel>1]
   //  par_host[17]  nlevel: sub-sub...halos (1=no sub)  [only for ANNIHILATION and nlevel>1]
   //  par_host[18]  dPdV normalisation  [kpc^{-3}]      [only for ANNIHILATION and nlevel>1]
   //  par_host[19]  dPdV scale radius [kpc]             [only for ANNIHILATION and nlevel>1]
   //  par_host[20]  dPdV shape parameter #1             [only for ANNIHILATION and nlevel>1]
   //  par_host[21]  dPdV shape parameter #2             [only for ANNIHILATION and nlevel>1]
   //  par_host[22]  dPdV shape parameter #3             [only for ANNIHILATION and nlevel>1]
   //  par_host[23]  dPdV card_profile [gENUM_PROFILE]   [only for ANNIHILATION and nlevel>1]
   //  par_host[24]  dPdV radius [kpc]                   [only for ANNIHILATION and nlevel>1]
   //  par_host[25]  ratio rs dPdV to rs_cl              [only for ANNIHILATION and nlevel>1]

   printf("       ================== par_host[25] ==================\n");
   printf("         [0] rho_cl norm      = %le [kpc^{-3}]\n", par_host[0]);
   printf("         [1] rho_cl rs        = %le [kpc]\n", par_host[1]);
   printf("         [2] rho_cl shape#1   = %le [-]\n", par_host[2]);
   printf("         [3] rho_cl shape#2   = %le [-]\n", par_host[3]);
   printf("         [4] rho_cl shape#3   = %le [-]\n", par_host[4]);
   printf("         [5] rho_cl profile   = %d (%s)\n", (int)par_host[5], gNAMES_PROFILE[(int)par_host[5]]);
   printf("         [6] rho_cl Rmax      = %le [kpc]\n", par_host[6]);
   printf("         [7] eps              = %le\n", par_host[7]);
   printf("         [8] z                = %le\n", par_host[8]);
   printf("         [9] Mdelta           = %le [Msol]\n", par_host[9]);
   printf("         [10]dPdM norm        = %le [Msol^{-1}]\n", par_host[10]);
   printf("         [11]dPdM slope       = %le [-]\n", par_host[11]);
   printf("         [12]dPdc cdelta-mdelta   = %d %s\n", (int)par_host[12], gNAMES_CDELTAMDELTA[(int)par_host[12]]);
   printf("         [13]dPdc <cvir>      = %le [-]\n", par_host[13]);
   printf("         [14]dPdc stddev      = %le\n", par_host[14]);
   printf("         [15]dPdC distrib     = %d %s\n", (int)par_host[15], gNAMES_CVIR_DIST[(int)par_host[15]]);
   printf("         [16]f_dm             = %le [-]\n", par_host[16]);
   printf("         [17]nlevel(subs)     = %d [-]\n", (int)par_host[17]);
   printf("         [18]dPdV norm        = %le [kpc^{-3}]\n", par_host[18]);
   printf("         [19]dPdV rs          = %le [kpc]\n", par_host[19]);
   printf("         [20]dPdV shape#1     = %le\n", par_host[20]);
   printf("         [21]dPdV shape#2     = %le\n", par_host[21]);
   printf("         [22]dPdV shape#3     = %le\n", par_host[22]);
   printf("         [23]dPdV profile     = %d (%s)\n", (int)par_host[23], gNAMES_PROFILE[(int)par_host[23]]);
   printf("         [24]dPdV Rmax        = %le [kpc]\n", par_host[24]);
   printf("         [25]rs_cl to rs_dPdV = %le \n", par_host[25]);
   printf("\n");
}

//______________________________________________________________________________
void print_parmix(double par_mix[21])
{
   //--- Print smooth (mix) parameters.
   //
   //  par_mix[0]    rho_1(r) normalisation [Msol/kpc^{-3}]
   //  par_mix[1]    rho_1(r) scale radius [kpc]
   //  par_mix[2]    rho_1(r) shape parameter #1
   //  par_mix[3]    rho_1(r) shape parameter #2
   //  par_mix[4]    rho_1(r) shape parameter #3
   //  par_mix[5]    rho_1(r) card_profile [gENUM_PROFILE]
   //  par_mix[6]    rho_1(r) host radius [kpc]
   //  par_mix[7]    Distance to halo centre [kpc]
   //  par_mix[8]    Longitude of halo centre [rad]
   //  par_mix[9]    Latitude of halo centre [rad]
   //  par_mix[10]   switch_rho: selects which combination of rho_1 and rho_2 to use
   //                   0 -> rho_mix(r) = rho_1(r)
   //                   1 -> rho_mix(r) = rho_1(r)-rho_2(r)
   //                   2 -> rho_mix(r) = rho_1(r)*rho_2(r)
   //  par_mix[11]   rho_2(r) normalisation [Msol/kpc^{-3}]
   //  par_mix[12]   rho_2(r) scale radius [kpc]
   //  par_mix[13]   rho_2(r) shape parameter #1
   //  par_mix[14]   rho_2(r) shape parameter #2
   //  par_mix[15]   rho_2(r) shape parameter #3
   //  par_mix[16]   rho_2(r) card_profile [gENUM_PROFILE]
   //  par_mix[17]   rho_2(r) host radius [kpc]
   //  par_mix[18]   Distance to halo centre [kpc]
   //  par_mix[19]   Longitude of halo centre [rad]
   //  par_mix[20]   Latitude of halo centre [rad]


   printf("       ================== par_mix[21] ==================\n");
   printf("         [0] rho_1 norm       = %le [kpc^{-3}]\n", par_mix[0]);
   printf("         [1] rho_1 rs         = %le [kpc]\n", par_mix[1]);
   printf("         [2] rho_1 shape#1    = %le [-]\n", par_mix[2]);
   printf("         [3] rho_1 shape#2    = %le [-]\n", par_mix[3]);
   printf("         [4] rho_1 shape#3    = %le [-]\n", par_mix[4]);
   printf("         [5] rho_1 profile    = %d (%s)\n", (int)par_mix[5], gNAMES_PROFILE[(int)par_mix[5]]);
   printf("         [6] rho_1 Rmax       = %le [kpc]\n", par_mix[6]);
   printf("         [7] rho_1 d          = %le [kpc]\n", par_mix[7]);
   printf("         [8] rho_1 psi(long)  = %le [rad]\n", par_mix[8]);
   printf("         [9] rho_1 theta(lat) = %le [rad]\n", par_mix[9]);
   int switch_rho = par_mix[10];
   string switch_text[3] = {"(rho = rho_1)", "(rho = rho_1-rho2)", "(rho = rho_1*rho2)"};
   printf("         [10]switch_rho       = %d %s\n", (int)par_mix[10], switch_text[switch_rho].c_str());
   printf("         [11]rho_2 norm       = %le [kpc^{-3}]\n", par_mix[11]);
   printf("         [12]rho_2 rs         = %le [kpc]\n", par_mix[12]);
   printf("         [13]rho_2 shape#1    = %le [-]\n", par_mix[13]);
   printf("         [14]rho_2 shape#2    = %le [-]\n", par_mix[14]);
   printf("         [15]rho_2 shape#3    = %le [-]\n", par_mix[15]);
   printf("         [16]rho_2 profile    = %d (%s)\n", (int)par_mix[16], gNAMES_PROFILE[(int)par_mix[16]]);
   printf("         [17]rho_2 Rmax       = %le [kpc]\n", par_mix[17]);
   printf("         [18]rho_2 d          = %le [kpc]\n", par_mix[18]);
   printf("         [19]rho_2 psi(long)  = %le [rad]\n", par_mix[19]);
   printf("         [20]rho_2 theta(lat) = %le [rad]\n", par_mix[20]);
   printf("\n");
}

//______________________________________________________________________________
void print_parsubs(double par_subs[25])
{
   //--- Print par_subs parameters.
   //
   //  par_subs[0]   rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_subs[1]   rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_subs[2]   rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_subs[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]   host halo outer radius where to stop integration [kpc]
   //  par_subs[5]   eps: relative precision sought L calculation
   //  par_subs[6]   z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]   mdelta: mass of rho_cl(r)
   //  par_subs[8]   dPdM normalisation
   //  par_subs[9]   dPdM slope alphaM
   //  par_subs[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_subs[11]  dPdc mean concentration <c>
   //  par_subs[12]  dPdc standard deviation             [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[13]  dPdc card_profile [gENUM_CDELTA_DIST] [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[14]  f: mass fraction of substructures   [only for ANNIHILATION and nlevel>1]
   //  par_subs[15]  nlevel: sub-sub...halos (1=no sub)  [only for ANNIHILATION and nlevel>1]
   //  par_subs[16]  dPdV normalisation  [kpc^{-3}]      [only for ANNIHILATION and nlevel>1]
   //  par_subs[17]  dPdV scale radius [kpc]             [only for ANNIHILATION and nlevel>1]
   //  par_subs[18]  dPdV shape parameter #1             [only for ANNIHILATION and nlevel>1]
   //  par_subs[19]  dPdV shape parameter #2             [only for ANNIHILATION and nlevel>1]
   //  par_subs[20]  dPdV shape parameter #3             [only for ANNIHILATION and nlevel>1]
   //  par_subs[21]  dPdV card_profile [gENUM_PROFILE]   [only for ANNIHILATION and nlevel>1]
   //  par_subs[22]  host halo outer radius (unused)     [only for ANNIHILATION and nlevel>1]
   //  par_subs[23]  ratio rs dPdV to rs_cl              [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]  R_host (corresp. to current dPdV)   [only for ANNIHILATION and nlevel>1]

   printf("       ================== par_subs[23] ==================\n");
   printf("         [0] rho_cl shape#1   = %le [-]\n", par_subs[0]);
   printf("         [1] rho_cl shape#2   = %le [-]\n", par_subs[1]);
   printf("         [2] rho_cl shape#3   = %le [-]\n", par_subs[2]);
   printf("         [3] rho_cl profile   = %d (%s)\n", (int)par_subs[3], gNAMES_PROFILE[(int)par_subs[3]]);
   printf("         [4] host Rmax        = %le [kpc]\n", par_subs[4]);
   printf("         [5] eps              = %le\n", par_subs[5]);
   printf("         [6] z                = %le\n", par_subs[6]);
   printf("         [7] Mdelta           = %le [Msol]\n", par_subs[7]);
   printf("         [8] dPdM norm        = %le [Msol^{-1}]\n", par_subs[8]);
   printf("         [9] dPdM slope       = %le [-]\n", par_subs[9]);
   printf("         [10]dPdc cdelta-mdelta   = %d %s\n", (int)par_subs[10], gNAMES_CDELTAMDELTA[(int)par_subs[10]]);
   printf("         [11]dPdc <cvir>      = %le [-]\n", par_subs[11]);
   printf("         [12]dPdc stddev      = %le [kpc]\n", par_subs[12]);
   printf("         [13]dPdC distrib     = %d %s\n", (int)par_subs[13], gNAMES_CVIR_DIST[(int)par_subs[13]]);
   printf("         [14]f_dm             = %le [-]\n", par_subs[14]);
   printf("         [15]nlevel(subs)     = %d [-]\n", (int)par_subs[15]);
   printf("         [16]dPdV norm        = %le [kpc^{-3}]\n", par_subs[16]);
   printf("         [17]dPdV rs          = %le [kpc]\n", par_subs[17]);
   printf("         [18]dPdV shape#1     = %le\n", par_subs[18]);
   printf("         [19]dPdV shape#2     = %le\n", par_subs[19]);
   printf("         [20]dPdV shape#3     = %le\n", par_subs[20]);
   printf("         [21]dPdV profile     = %d (%s)\n", (int)par_subs[21], gNAMES_PROFILE[(int)par_subs[21]]);
   printf("         [22]dPdV Rmax        = %le [kpc]\n", par_subs[22]);
   printf("         [23]rs_cl to rs_dPdV = %le \n", par_subs[23]);
   printf("         [24]current R_host = %le \n", par_subs[24]);
   printf("\n");
}

//______________________________________________________________________________
void print_partot(double par_tot[10])
{
   //--- Print rho_tot parameters.
   //
   //  par_tot[0]    rho_tot host normalisation [Msol kpc^{-3}]
   //  par_tot[1]    rho_tot host scale radius [kpc]
   //  par_tot[2]    rho_tot host shape parameter #1
   //  par_tot[3]    rho_tot host shape parameter #2
   //  par_tot[4]    rho_tot host shape parameter #3
   //  par_tot[5]    rho_tot host card_profile [gENUM_PROFILE]
   //  par_tot[6]    rho_tot host radius [kpc]
   //  par_tot[7]    l_HC: distance observer to host centre [kpc]
   //  par_tot[8]    psi_HC: longitude of host centre [rad]
   //  par_tot[9]    theta_HC: latitude of host centre [rad]


   printf("       ================== par_tot[10] ==================\n");
   printf("         [0] norm       = %le [kpc^{-3}]\n", par_tot[0]);
   printf("         [1] rs         = %le [kpc]\n", par_tot[1]);
   printf("         [2] shape#1    = %le [-]\n", par_tot[2]);
   printf("         [3] shape#2    = %le [-]\n", par_tot[3]);
   printf("         [4] shape#3    = %le [-]\n", par_tot[4]);
   printf("         [5] profile    = %d (%s)\n", (int)par_tot[5], gNAMES_PROFILE[(int)par_tot[5]]);
   printf("         [6] Rmax       = %le [kpc]\n", par_tot[6]);
   printf("         [7] d          = %le [kpc]\n", par_tot[7]);
   printf("         [8] psi(long)  = %le [rad]\n", par_tot[8]);
   printf("         [9] theta(lat) = %le [rad]\n", par_tot[9]);
   printf("\n");
}

//______________________________________________________________________________
double Rdelta_to_mdelta(double const &Rdelta, double const &Delta_c, double const &z)
{
   //--- Returns mdelta [Msol] from Rdelta.
   //
   // INPUTS:
   //  Rdelta         Rdelta of the halo (proper coordinates), depending on Delta(z) [kpc]
   //  Delta_c        Critical overdensity factor defining mdelta(z).
   //  z              Redshift of the halo
   // OUTPUT:
   //  mdelta         [Msol]

   return  4.* PI / 3.* Delta_c * rho_crit(z) * pow(Rdelta, 3.);
}

//______________________________________________________________________________
double shapeparams_to_Delta(double par_tot[6], double const &Rdelta, double const &z)
{
   //--- Returns Delta_c [kpc] from given shape parameters and Rdelta.
   //    N.B.: Quantities are given in proper coordinates.
   //
   // INPUTS:
   //  par_tot[0]    Clump density normalisation [Msol/kpc^3]
   //  par_tot[1]    Clump scale radius, based on mdelta_to_rscale() [kpc]
   //  par_tot[2]    Shape parameter #1
   //  par_tot[3]    Shape parameter #2
   //  par_tot[4]    Shape parameter #3
   //  par_tot[5]    card_profile
   //  Rdelta        Virial radius of the halo
   //  z             Redshift of the halo
   // OUTPUT:
   //  Delta_c       w.r.t. the critical density

   double par_tot_R[7];
   for (int j = 0; j < 6; ++j)
      par_tot_R[j] = par_tot[j];
   par_tot_R[6] = Rdelta;
   double mtot = mass_singlehalo(par_tot_R, gSIM_EPS);
   return 3. * mtot / (4 * PI * rho_crit(z) * pow(Rdelta, 3));
}

//______________________________________________________________________________
double shapeparams_to_Rdelta(double par_tot[6], double const &Delta_c, double const &z)
{
   //--- Returns Rdelta [kpc] from given shape parameters and Delta_c.
   //    N.B.: Quantities are given in proper coordinates.
   //
   // INPUTS:
   //  par_tot[0]    Clump density normalisation [Msol/kpc^3]
   //  par_tot[1]    Clump scale radius, based on mdelta_to_rscale() [kpc]
   //  par_tot[2]    Shape parameter #1
   //  par_tot[3]    Shape parameter #2
   //  par_tot[4]    Shape parameter #3
   //  par_tot[5]    card_profile
   //  Delta_c       Overdensity factor
   //  z             Redshift of the halo
   // OUTPUT:
   //  Rdelta         [kpc]

   // Assume Rdelta > 0.01 * r_scale:
   double Rdelta_lo = 1e-2 * par_tot[1]; // r_scale
   double Rdelta_hi = 1e9 * par_tot[1]; // kpc
   if (par_tot[5] == kNODES) {
      Rdelta_hi = gHALO_NODES_X_GRID[int(par_tot[2])].back() * (1 - SMALL_NUMBER) * par_tot[1];
   }

   gsl_function F;
   Rdelta_params_for_rootfinding params = {par_tot[0], par_tot[1], par_tot[2], par_tot[3], par_tot[4], int(par_tot[5]), Delta_c, z};

   F.function = &solve_Rdelta;
   F.params = &params;
   int return_status = 0;
   return rootsolver_gsl(gsl_root_fsolver_falsepos, F, Rdelta_lo, Rdelta_hi, gSIM_EPS, return_status); // faster than gsl_root_fsolver_brent and gsl_root_fsolver_bisection
}

double solve_c_SIDM_Burkert(double c, void *p)
{
   //--- GSL Function to solve the implicit equation
   //
   //    [ log(c^2 + 1) + 2 log (c + 1) - 2 arctan(c) ] / c^3 - fac = 0
   //
   //    for c.

   // Parameters p (struct Rdelta_params_for_rootfinding):
   //  par_tot[0]    Clump density normalisation [Msol/kpc^3]
   //  par_tot[1]    Clump scale radius, based on mdelta_to_rscale() [kpc]
   //  par_tot[2]    Shape parameter #1
   //  par_tot[3]    Shape parameter #2
   //  par_tot[4]    Shape parameter #3
   //  par_tot[5]    card_profile
   //  Delta         Overdensity factor
   //  z             Redshift of the halo

   c_params_for_rootfinding *params = (struct c_params_for_rootfinding *)p;

   double fac = params->fac;

   return (log1p(pow(c, 2)) + 2 * log1p(c) - 2 * atan(c)) * pow(c, -3.) - fac;
}

double solve_Rdelta(double Rdelta, void *p)
{
   //--- GSL Function to solve the implicit equation
   //
   //    4*pi * Int_0^Rdelta r^2 rho(r) dr = 4*pi/3 * Rdelta^3 * Delta_c(z) * rho_crit(z)
   //
   //    for Rdelta.
   //    N.B.: Quantities are given in proper coordinates.
   //
   // Parameters p (struct Rdelta_params_for_rootfinding):
   //  par_tot[0]    Clump density normalisation [Msol/kpc^3]
   //  par_tot[1]    Clump scale radius, based on mdelta_to_rscale() [kpc]
   //  par_tot[2]    Shape parameter #1
   //  par_tot[3]    Shape parameter #2
   //  par_tot[4]    Shape parameter #3
   //  par_tot[5]    card_profile
   //  Delta         Overdensity factor
   //  z             Redshift of the halo

   Rdelta_params_for_rootfinding *params = (struct Rdelta_params_for_rootfinding *)p;

   double par_tot[7];
   par_tot[0] = (params->rho_s);
   par_tot[1] = (params->r_s);
   par_tot[2] = (params->par1);
   par_tot[3] = (params->par2);
   par_tot[4] = (params->par3);
   par_tot[5] = (params->card_profile);
   par_tot[6]  = Rdelta;
   double Delta_c = (params->Delta_c);
   double z = params->z;

   return mass_singlehalo(par_tot, gSIM_EPS) - 4 * PI * pow(Rdelta, 3.) * Delta_c * rho_crit(z) / 3.;
}

double solve_Mdelta(double Mdelta, void *p)
{
   //--- GSL Function to solve the implicit equation
   //
   //    Mdelta_ref * Int_0^[cdelta_ref(Mdelta_ref) * (Mdelta1/Mdelta_ref * Delta_c_ref/Delta1)^(1/3) ] x^2 profile(r_2/r_s * x) dx
   //       = Mdelta1 * Int_0^[cdelta_ref(Mdelta_ref)] x^2 profile(r_2/r_s * x) dx
   //
   //    for either Mdelta1 or Mdelta_ref, depending on the bool find_mref_given_m1.

   // Parameters p (struct Rdelta_params_for_rootfinding):
   //  mdelta              Mass mdelta or mdelta_ref, depending on bool find_m2_given_m1
   //  Delta1              Overdensity factor of Delta1
   //  card_cdelta         cdelta_Ref - mdelta_ref relation
   //  par1                Shape parameter #1
   //  par2                Shape parameter #2
   //  par3                Shape parameter #3
   //  card_profile        DM halo profile
   //  z                   Redshift of the halo
   //  find_mref_given_m1  specifies whether the function is solved for mdelta_ref or mdelta1.

   Mdelta_params_for_rootfinding *params = (struct Mdelta_params_for_rootfinding *)p;

   bool find_mref_given_m1 = (params->find_mref_given_m1);

   double Mdelta1;
   double Mdelta_ref;
   if (find_mref_given_m1) {
      Mdelta1 = (params->mdelta);
      Mdelta_ref = Mdelta;
   } else {
      Mdelta1 = Mdelta;
      Mdelta_ref = (params->mdelta);
   }

   double Delta1 = (params->Delta1);
   double z = (params->z);
   int    card_cdelta = (params->card_cdelta);  //  cdelta_Ref - mdelta_ref relation
   double Delta_c_ref = card_cdelta_to_Delta(card_cdelta, z);

   double par_prof1[7];
   par_prof1[0] = 1.; // rho_s (cancels out in the fraction of integrals and is set to dummy value 1.)
   par_prof1[1] = 1.;  // r_s (cancels out in the calculation and is set to dummy value 1)
   par_prof1[2] = (params->par1);
   par_prof1[3] = (params->par2);
   par_prof1[4] = (params->par3);
   par_prof1[5] = (params->card_profile);

   double par_prof2[7];
   for (int i = 0; i < 6; ++i) par_prof2[i] = par_prof1[i];

   double cdelta_ref;
   double xpos = (params->xpos);
   cdelta_ref = mdelta_to_cdelta(Mdelta_ref, Delta_c_ref, card_cdelta, &par_prof1[2], z, xpos);

   par_prof1[6]  = get_ratio_r_2_rscale(&par_prof1[2]) * cdelta_ref; //Rdelta_ref
   par_prof2[6]  = get_ratio_r_2_rscale(&par_prof1[2]) * cdelta_ref
                   * pow(Mdelta1 / Mdelta_ref * Delta_c_ref / Delta1, 1. / 3.); //Rdelta1

   return mass_singlehalo(par_prof1, gSIM_EPS) / mass_singlehalo(par_prof2, gSIM_EPS) - Mdelta_ref / Mdelta1;
}

double solve_cdelta_celine(double const &Mdelta, double const &Delta_c,
                           int const  card_cdelta, double par_prof[4], double const &z,
                           double const &xpos)
{

   double cDelta_init = 10.;
   double Mdelta_test = 0.;
   double cDelta_new  = 0.;
   int iter = 0;
   int iter_max = 100;

   double par[7];
   par[2] = par_prof[0];
   par[3] = par_prof[1];
   par[4] = par_prof[2];
   par[5] = par_prof[3];
   double Rdelta = mdelta_to_Rdelta(Mdelta, Delta_c, z);

   // iterative algorithm:
   while (fabs(Mdelta_test - Mdelta) / Mdelta > gSIM_EPS) {
      iter ++;

      par[1] = get_rscale_from_r_2(Rdelta / cDelta_init, par_prof); //rs
      set_par0_given_mref(par, Rdelta, Mdelta, gSIM_EPS);

      double Delta_c_ref = card_cdelta_to_Delta(card_cdelta, z);
      double Rdelta_ref = shapeparams_to_Rdelta(par, Delta_c_ref, z);
      par[6] = Rdelta_ref;

      double Mdelta_ref = mass_singlehalo(par, gSIM_EPS);
      double cDelta_ref = mdelta_to_cdelta(Mdelta_ref, Delta_c_ref, card_cdelta, par_prof,  z, xpos);

      double r_2 = Rdelta_ref / cDelta_ref;
      par[1] = get_rscale_from_r_2(r_2, par_prof); // rs

      set_par0_given_mref(par, Rdelta_ref, Mdelta_ref, gSIM_EPS);
      par[6] = Rdelta;
      Mdelta_test = mass_singlehalo(par, gSIM_EPS);
      cDelta_new = shapeparams_to_Rdelta(par, Delta_c, z) / r_2;
      cDelta_init = cDelta_new;

      //printf("iter=%d test=%le MDelta_ref=%le RDelta_ref=%le cDelta_new=%f, cDelta_ref=%f\n",
      //     iter, fabs(Mdelta - Mdelta_test) / Mdelta, Mdelta_ref, Rdelta_ref, cDelta_new, cDelta_ref);

      if (iter > iter_max) {
         printf("\n====> ERROR: solve_cdelta_celine() in clumps.cc");
         printf("\n             Too many steps (not converged)!");
         printf("\n             => abort()\n\n");
         abort();
      }
   }
   //printf("  solve_cdelta_celine() converged after %d iterations\n", iter);
   return cDelta_new;
}

double solve_rbias(double rbias, void *p)
{
   //--- GSL Function to solve the implicit equation A10 from Pieri et al. (2011) for r_b

// so far only for NFW!!

   rbias_params_for_rootfinding *params = (struct rbias_params_for_rootfinding *)p;

   double mdelta = (params->mdelta);             //!< Mass of host halo [Msol]
   double rdelta = (params->rdelta);             //!< Outer halo bound [kpc] of host
   double rho_s = (params->rho_s);               //!< scale density [Msol/kpc^{3}] of host
   double r_s = (params->r_s);                   //!< scale radius  [kpc] of host
   double par1 = (params->par1);                 //!< Shape parameter #1 of host
   double par2 = (params->par2);                 //!< Shape parameter #2 of host
   double par3 = (params->par3);                 //!< Shape parameter #3 of host
   int    card_profile = (params->card_profile); //!< DM halo profile of host
   double fsub = (params->fsub);                 //!< substructure mass fraction in host

   if (card_profile != kZHAO or par1 != 1 or par2 != 3 or par3 != 1) {
      cout << par1 << " " << par2 << " " << par3 << endl;
      print_error("clumps.cc", "solve_rbias()", "kDPDV_PIERI11 dP/dV profile only implemented for NFW host halos.");
   }

   return 4 * PI * rbias * pow(r_s, 3.) * rho_s * (rdelta * (r_s - rbias) + rbias * (rdelta + r_s) * log((rdelta + r_s) / (rbias + rdelta) * rbias / r_s)) /
          ((1 - fsub) * mdelta * pow(rbias - r_s, 2.) * (rdelta + r_s)) - 1;
}

