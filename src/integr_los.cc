/*! \file integr_los.cc \brief (see integr_los.h) */

// CLUMPY includes
#include "../include/clumps.h"
#include "../include/geometry.h"
#include "../include/inlines.h"
#include "../include/integr.h"
#include "../include/integr_los.h"
#include "../include/params.h"
#include "../include/profiles.h"
#include "../include/misc.h"

// C++ std libraries
//using namespace std;
//#include <stdlib.h>

//______________________________________________________________________________
double los_integral(double par[10], int switch_f, double const &psi_los, double const &theta_los,
                    double const &lmin, double const &lmax, double const &eps, bool is_verbose)
{
   //--- Integrates and returns, for a given l.o.s. direction (psi_los, theta_los),
   //    the function selected by switch_f between lmin and lmax over the aperture
   //    gSIM_ALPHAINT, until the relative precision eps is reached.
   //
   //  par[0]     Normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[1]     Scale radius [kpc]
   //  par[2]     Shape parameter #1
   //  par[3]     Shape parameter #2
   //  par[4]     Shape parameter #3
   //  par[5]     card_profile [gENUM_PROFILE]
   //  par[6]     Radius of rho [kpc]
   //  par[7]     l_C: distance observer - profile centre [kpc]
   //  par[8]     psi_C: to profile centre [rad]
   //  par[9]     theta_C: to profile centre [rad]
   //  switch_f   Selects the function f to integrate:
   //                0 -> rho:           for jsub_continuum (also proxy for dP/dV)
   //                1 -> rho^2[or rho]: annihilation[decay] for a single clump or DM halo
   //                2 -> l^2*rho:       for clump number/mass fraction estimates (proxy for dP/dV)
   //                3 -> l^3*rho:       to calculate <l>
   //                4 -> l^4*rho:       to calculate var(l)
   //                5 -> l^-2*rho:      to calculate var(J)
   //                6 -> l^2*rho^2:     to check with Lum direct calculation (integrand corresponds to d3Lum)
   //  psi_los    Longitude for the l.o.s [rad]
   //  theta_los  Latitude for the l.o.s.  [rad]
   //  lmin       Lower l.o.s. integration boundary [kpc]
   //  lmax       Upper l.o.s. integration boundary [kpc]
   //  eps        Relative precision sought for integrations

   // We create a dummy par_mix[21] to call los_integral_mix(), with
   //  par_mix[0...9] = par[0...9]
   //  par_mix[10...21] is UNUSED
   const int n_par = 21;
   double par_mix[n_par];
   for (int i = 0; i < 10; ++i)
      par_mix[i] = par[i];
   for (int i = 10; i < n_par; ++i)
      par_mix[i] = 0.;

   return los_integral_mix(par_mix, switch_f, psi_los, theta_los, lmin, lmax, eps, is_verbose);
}


//______________________________________________________________________________
double los_integral_mix(double par[21], int switch_f, double const &psi_los,
                        double const &theta_los, double const &lmin, double const &lmax,
                        double const &eps, bool is_verbose)
{
   //--- Integrates and returns, for a given l.o.s. direction (psi_los, theta_los),
   //    the function selected by switch_rho and switch_f between lmin and lmax
   //    over the aperture gSIM_ALPHAINT, until the relative precision eps is reached.
   //
   //  par[0]     rho_1(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[1]     rho_1(r) scale radius [kpc]
   //  par[2]     rho_1(r) shape parameter #1
   //  par[3]     rho_1(r) shape parameter #2
   //  par[4]     rho_1(r) shape parameter #3
   //  par[5]     rho_1(r) card_profile [gENUM_PROFILE]
   //  par[6]     rho_1(r) radius [kpc]
   //  par[7]     rho_1(r) distance to observer [kpc]
   //  par[8]     rho_1(r) psi_center [rad]
   //  par[9]     rho_1(r) theta_center [rad]
   //  par[10]    switch_rho: selects which combination of rho_1 and rho_2 to use
   //                0 -> rho(r) = rho1(r)
   //                1 -> rho(r) = rho1(r)-rho2(r)
   //                2 -> rho(r) = rho1(r)*rho2(r)
   //  par[11]    rho_2(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[12]    rho_2(r) scale radius [kpc]
   //  par[13]    rho_2(r) shape parameter #1
   //  par[14]    rho_2(r) shape parameter #2
   //  par[15]    rho_2(r) shape parameter #3
   //  par[16]    rho_2(r) card_profile [gENUM_PROFILE]
   //  par[17]    rho_2(r) radius [kpc]
   //  par[18]    rho_2(r) distance to observer [kpc]
   //  par[19]    rho_2(r) psi_center [rad]
   //  par[20]    rho_2(r) theta_center [rad]
   //  switch_f   Selects the function f to integrate:
   //                0 -> rho:           for jsub_continuum (also proxy for dP/dV)
   //                1 -> rho^2[or rho]: annihilation[decay] for a single clump or DM halo
   //                2 -> l^2*rho:       for clump number/mass fraction estimates (proxy for dP/dV)
   //                3 -> l^3*rho:       to calculate <l>
   //                4 -> l^4*rho:       to calculate var(l)
   //                5 -> l^-2*rho:      to calculate var(J)
   //                6 -> l^2*rho^2:     to check with Lum direct calculation (integrand corresponds to d3Lum)
   //  psi_los    Longitude for the l.o.s [rad]
   //  theta_los  Latitude for the l.o.s.  [rad]
   //  lmin       Lower l.o.s. integration boundary [kpc]
   //  lmax       Upper l.o.s. integration boundary [kpc]
   //  eps        Relative precision sought for integrations


   if (switch_f < 0 || switch_f > 6) {
      printf("\n====> ERROR: los_integral_mix() in integr_los.cc");
      printf("\n             switch_f=%d is not a valid option", switch_f);
      printf("\n             => abort()\n\n");
      abort();
   }

   // phi_los = angle between l.o.s. and Galactic centre
   double phi_los = psitheta_to_phi(psi_los, theta_los);

   // phi_los_densitypeak = angle between l.o.s and the DM density peak
   double phi_los_densitypeak = phi_los;
   // [initialised to phi_los (l.o.s. direction), only updated if we deal with a clump]

   // Look whether rho1 is the Galactic halo
   bool is_rho1_galhalo = false;
   if (fabs(par[7] - gMW_RSOL) / gMW_RSOL < 1.e-5 && fabs(par[8]) < 1.e-8 && fabs(par[9]) < 1.e-8)
      is_rho1_galhalo = true;

   // Default integration range for beta
   double beta_min = 0.;
   double beta_max = 2.*PI;


   // Calculate alpha_cl the angular size of the clump on the sky
   // N.B.: asin() returns an angle in [-pi/2,pi/2]
   // N.B.: if the clump is too close, the angle is ill-defined. In that case we
   // set it to pi (safe to do so as it is only used for comparison later on)

   double alpha_aperture_ref = gSIM_ALPHAINT;
   double check_alpha = par[6] / par[7];
   double alpha_cl = PI;
   if (check_alpha <= 1.) alpha_cl = fabs(asin(check_alpha));

   // If gSIM_ALPHAINT > alpha_cl the angular size of the clump on the sky
   // integration region is centered on the clump, we decrease it to optimise
   // the calculation (reset it to the "initial" value at the end of the calculation)
   // N.B.: before leaving this routine, we have to ensure that gSIM_ALPHAINT is reset
   // to its initial value!
   bool is_opt_integr_alpha = false;
   if (fabs((par[8] - psi_los)) <= 1.e-10 * fabs(psi_los)
         && fabs((par[9] - theta_los)) <= 1.e-10 * fabs(theta_los)
         &&  gSIM_ALPHAINT >= alpha_cl) {
      is_opt_integr_alpha = true;
      gSIM_ALPHAINT = alpha_cl * (1 - SMALL_NUMBER);
   }


   //----------------------------------------------------------------------------------------
   // A. Specific case of a clump integration (i.e. the DM clump is not the Galactic DM halo)
   //----------------------------------------------------------------------------------------
   if ((switch_f == 1 || switch_f == 6) && !is_rho1_galhalo) {
      //
      // 1. Preliminary calculations
      // +++++++++++++++++++++++++++
      // Calculate phi (angle between los and cl direction) using scalar product:
      //      ||x_los||.||x_cl||.cos(phi)=sum_{i=1,3} x_los[i].x_cl[i]
      //
      // a) x_los: coordinates of a point at distance par[7]=l_cl in the direction
      // of the l.o.s (do not change this distance, this is used later on!)
      // The function lalphabeta_to_xyzgal (geometry.h) returns cartesian coordinates
      // in the framework attached to the GC.
      double psi_theta_los[2] = {psi_los, theta_los}; // Set psi, theta
      double x_los[3] = {par[7], 0., 0.};             // Set l, alpha, beta
      lalphabeta_to_xyzgal(x_los, psi_theta_los);         // Convert (l,alpha,beta) to (x,y,z)_gal
      //
      // b) x_cl: coordinates of the clump
      double psi_theta_cl[2] = {par[8], par[9]}; // Set psi_cl, theta_cl
      double x_cl[3] = {par[7], 0., 0.};         // Set l_cl, alpha_cl=0, beta_cl=0
      lalphabeta_to_xyzgal(x_cl, psi_theta_cl);      // Convert (l,alpha,beta) to (x,y,z)_gal
      //
      // c) Coordinates in the Cartesian frame attached to Earth
      x_los[0] += gMW_RSOL;
      x_cl[0] += gMW_RSOL;

      phi_los_densitypeak = 0.;
      double cos_phi = (x_los[0] * x_cl[0] + x_los[1] * x_cl[1] + x_los[2] * x_cl[2])
                       / (sqrt(x_los[0] * x_los[0] + x_los[1] * x_los[1] + x_los[2] * x_los[2])
                          * sqrt(x_cl[0] * x_cl[0] + x_cl[1] * x_cl[1] + x_cl[2] * x_cl[2]));
      if (cos_phi < 1.) phi_los_densitypeak = acos(cos_phi);
      // N.B.: On some compilers, "1"=1.0000000000001, which makes acos("1") crash.
      // So better safe than sorry! (acos returns an angle in [0,pi])

      // 2. The clump is not in the F.O.I of the instrument => returns 0.
      // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      if ((phi_los_densitypeak >= gSIM_ALPHAINT)  && fabs(phi_los_densitypeak - gSIM_ALPHAINT) >= alpha_cl) {
         if (is_verbose) {
            printf(">>>> Clump position (%le,%le,%le) does not fall in the F.O.V of the "
                   "instrument in the direction (%le,%le)\n",
                   par[7], par[8], par[9], psi_los, theta_los);
            printf("   => flux = 0.\n");
         }
         // If gSIM_ALPHAINT was changed at the beginning of the routine,
         // we have to reset it to its initial value.
         if (is_opt_integr_alpha)
            gSIM_ALPHAINT = alpha_aperture_ref;
         return 0.;
      }


      // 3. The l.o.s. falls out of the clump volume: optimisation
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // To optimise the integration, the interval for the rotation angle beta can be reduced.
      // We calculate the maximum half angle under which the boundary of the clump appears
      // (this angle is maximal when the distance on the l.o.s. = distance to the clump,
      //  i.e. when d_cl_los = |x_cl - x_los| is minimal)
      //   => beta_view = asin(Rcl/d_cl_los_min)
      //
      // Yet, this can even be further refined, as the integration is generally
      // very small compared to the size of the clump. We thus calculate at which
      // angle beta_intersect (with respect to the line joining the clump centre and the l.o.s.
      // direction) the clumps intersect the l.o.s.. It is given by the Al-Kashi theorem
      // for any triangle
      //   beta_intersect = acos((Rcl^2+Rfoi^2-d_cl_los_min^2)/(2*Rcl*Rfoi))
      // where Rfoi = sin(gSIM_ALPHAINT)*l_cl
      //
      // We then have: I_{0}^{2pi} = 2 * I_{beta_ref}^{beta_ref+beta_integr}
      // where
      //   beta_ref = angle between beta=0 and the position of the clump
      // and
      //   beta_integr = min(beta_intersect,beta_view)
      //
      if (phi_los_densitypeak > alpha_cl && phi_los_densitypeak + alpha_cl > gSIM_ALPHAINT) {
         // N.B.: second condition ensures that the clump is not fully contained within the FoV.

         double d_cl_los_min = par[7] * MY_SIN(phi_los_densitypeak);

         if (is_verbose) {
            char char_tmp[256];
            sprintf(char_tmp, "We have phi_los_densitypeak > alpha_cl (%le > %le), "
                    "hence the l.o.s. lies out of the clump region: "
                    "Rcl = %le kpc, and min(los,clump) = %le kpc.",
                    phi_los_densitypeak, alpha_cl, par[6], d_cl_los_min);
            print_warning("integr_los.cc", "los_integral_mix()", string(char_tmp));
         }

         // Extra check (should not be necessary in real life),
         // but we prefer to avoid any possible numerical issues.
         double beta_view = PI;
         if (par[6] < d_cl_los_min)
            beta_view = fabs(asin(par[6] / d_cl_los_min));

         // Find beta_intersect and beta_ref
         // In a triangle a^2 = b^2 + c^2 - 2 bc cos(A)
         double Rfoi = MY_SIN(gSIM_ALPHAINT) * par[7];
         double beta_intersect = acos((Rfoi * Rfoi + d_cl_los_min * d_cl_los_min - par[6] * par[6])
                                      / (2.*d_cl_los_min * Rfoi));


         if (is_verbose && !((switch_f == 1 || switch_f == 6) && is_rho1_galhalo))
            printf("beta_intersect=%le, Rfoi+Rcl=%le, d=%le\n",
                   beta_intersect, Rfoi + par[6], d_cl_los_min);

         double beta_integr = min(beta_view, beta_intersect);

         // Find beta_ref
         x_cl[0] -= gMW_RSOL;
         if (is_verbose) {
            cout << "    - Clump (x,y,z) in kpc: "
                 << x_cl[0] << "  " << x_cl[1] <<  "  " << x_cl[2] << endl;
         }
         xyzgal_to_lalphabeta(x_cl, psi_theta_los);  // from geometry.h
         if (is_verbose) {
            cout << "    - Clump (l,alpha,beta) in deg: "
                 << x_cl[0] << "  "
                 << x_cl[1] * RAD_to_DEG <<  "  "
                 << x_cl[2] * RAD_to_DEG;
            cout << "  [in the los direction (psi,theta): "
                 << psi_theta_los[0] << "  "
                 << psi_theta_los[1] << "]" << endl;
         }
         double beta_ref = x_cl[2];

         if (is_verbose && !((switch_f == 1 || switch_f == 6) && is_rho1_galhalo)) {
            cout << "  beta_ref(rad)=" << beta_ref
                 << "  beta_view(rad)=" << beta_view
                 << "  beta_intersect(rad)=" << beta_intersect << endl;
         }

         // Set beta_min and beta_max. Note that the integral is symmetric:
         //    I_{beta_ref}^{beta_ref+beta_integr} = I_{beta_ref-beta_integr}^{beta_ref}
         // We just check whether beta_ref+beta_integr>2pi to decide which one to perform.
         if (beta_ref + beta_integr > 2.*PI) {
            beta_min = beta_ref - beta_intersect;
            beta_max = beta_ref;
         } else {
            beta_min = beta_ref;
            beta_max = beta_ref + beta_intersect;
         }
         if (is_verbose && !((switch_f == 1 || switch_f == 6) && is_rho1_galhalo))
            printf("\n >>>>>>>>>>> beta_min=%le beta_max=%le\n", beta_min, beta_max);
      }
   }


   //------------------------------------------------------------------------
   // B. Point-like contribution (whenever the peak density falls in the FOI)
   //------------------------------------------------------------------------
   // Density peaks are time-consuming to integrate on the line of sight.
   // Special case arises if the density peak is within the F.O.I (otherwise,
   // standard integration)
   //  => we integrate within a radius r_trick as a point-like contribution
   double point_like = 0.;
   double r_trick = 0.;
   if (phi_los_densitypeak < gSIM_ALPHAINT && (switch_f == 1 || switch_f == 6)) {

      //-- Find r_trick
      // Minimal distance between densitypeak and F.O.V. boundary
      //   r_bond = l_cl * sin (phi_los_densitypeak - gSIM_ALPHAINT)
      double r_bond = par[7] * fabs(MY_SIN(fabs(phi_los_densitypeak - gSIM_ALPHAINT)));
      // Maximal radius allowed for which the clump can be considered point-like
      //   r_pointlike = f*l_cl, where f is chosen to be 1.e-3
      double r_pointlike = 1.e-3 * par[7];
      if (par[6] < par[7]) r_trick = min(r_bond, r_pointlike);
      // Finally, we may want to check that this distance is smaller than
      // the integration boundary for the clump, which is R_cl (=par[6])
      r_trick = min(r_trick, par[6]);

      //-- Calculate point-like contribution
      // Reminder:
      //  par[6]    R_cl: effective radius where to stop integration [kpc]
      const int n_tmp = 21;
      double par_tmp[n_tmp];
      for (int i = 0; i < n_tmp; ++i)
         par_tmp[i] = par[i];
      par_tmp[6] = r_trick;

      point_like = 0.;
      if (r_trick > 0.) {
         point_like = lum_singlehalo_nosubs_mix(par_tmp, eps);
         if (switch_f == 1)  point_like /= (par[7] * par[7]);
      }
      //
      // print info if demanded
      if (is_verbose && !((switch_f == 1 || switch_f == 6) && is_rho1_galhalo)) {
         if (par[6] < par[7])
            cout << "r_bond=" << r_bond << "  r_pointlike=" << r_pointlike << "  r_trick=" << r_trick
                 << "  J(point_like)=" << point_like << endl;
         else
            cout << "r_pointlike=" << r_pointlike << "  r_trick=" << r_trick
                 << "  J(point_like)=" << point_like << endl;
      }
      //
      // In the unlikely event when r_trick~R_cl(=par[6]), this is over!
      // (means that the whole clump is within the F.O.I)
      if (fabs(r_trick - par[6]) / par[6] < 1.e-5) {
         // If gSIM_ALPHAINT was changed at the beginning of the routine,
         // we have to reset it to its initial value
         if (is_opt_integr_alpha)
            gSIM_ALPHAINT = alpha_aperture_ref;
         if (is_verbose) printf("clump is point-like !! DONE.\n");
         return point_like;
      }
   }



   //------------------------------------------------------------
   // C. Final results: adding all contributions and optimisations
   //------------------------------------------------------------
   // For the remaining deeper depth of integration, 31 parameters are required
   // It is faster to allocate this space once, and update them when required
   //
   // From all parameters and quantities calculated above, we create and fill
   // a new par_trick[36] which will be used for the integrations below:
   //
   //  par_trick[0]     rho_1(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par_trick[1]     rho_1(r) scale radius [kpc]
   //  par_trick[2]     rho_1(r) shape parameter #1
   //  par_trick[3]     rho_1(r) shape parameter #2
   //  par_trick[4]     rho_1(r) shape parameter #3
   //  par_trick[5]     rho_1(r) card_profile [gENUM_PROFILE]
   //  par_trick[6]     rho_1(r) radius [kpc]
   //  par_trick[7]     rho_1(r) distance to observer [kpc]
   //  par_trick[8]     rho_1(r) psi_center [rad]
   //  par_trick[9]     rho_1(r) theta_center [rad]
   //  par_trick[10]    switch_rho: selects which combination of rho_1 and rho_2 to use
   //                      0 -> rho(r) = rho1(r)
   //                      1 -> rho(r) = rho1(r)-rho2(r)
   //                      2 -> rho(r) = rho1(r)*rho2(r)
   //  par_trick[11]    rho_2(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par_trick[12]    rho_2(r) scale radius [kpc]
   //  par_trick[13]    rho_2(r) shape parameter #1
   //  par_trick[14]    rho_2(r) shape parameter #2
   //  par_trick[15]    rho_2(r) shape parameter #3
   //  par_trick[16]    rho_2(r) card_profile [gENUM_PROFILE]
   //  par_trick[17]    rho_2(r) radius [kpc]
   //  par_trick[18]    rho_2(r) distance to observer [kpc]
   //  par_trick[19]    rho_2(r) psi_center [rad]
   //  par_trick[20]    rho_2(r) theta_center [rad]
   //  par_trick[21]    switch_f: selects the function f to integrate
   //                      0 -> rho:           for jsub_continuum (also proxy for dP/dV)
   //                      1 -> rho^2[or rho]: annihilation[decay] for a single clump or DM halo
   //                      2 -> l^2*rho:       for clump number/mass fraction estimates (proxy for dP/dV)
   //                      3 -> l^3*rho:       to calculate <l>
   //                      4 -> l^4*rho:       to calculate var(l)
   //                      5 -> l^-2*rho:      to calculate var(J)
   //                      6 -> l^2*rho^2:     to check with Lum direct calculation (integrand corresponds to d3Lum)
   //  par_trick[22]     psi_los: longitude of the line of sight [rad]
   //  par_trick[23]     theta_los: latitude of the line of sight [rad]
   //  par_trick[24]     lmin: lower l.o.s. integration boundary [kpc]
   //  par_trick[25]     lmax: upper l.o.s. integration boundary [kpc]
   //  par_trick[26]     eps: relative precision sought for integrations
   //  par_trick[27]     phi_los: direction between GC and l.o.s. (psi,theta) [rad]
   //  par_trick[28]     beta: integr. angle in the plane perp. to the l.o.s. [rad]
   //  par_trick[29]     alpha: integr. angle measured from the ref. l.o.s. [rad]
   //  par_trick[30]     phi_los_peak: angle between phi_los and density peak [rad]
   //  par_trick[31]     r_trick: radius of the point-like contribution [kpc]
   //  par_trick[32]     l_offset: for the integr. along  l': l'=l+l_offset, dl'=dl [kpc]
   //  par_trick[33]     is_increasing_l (true/false if integr. corresponds to increas./decreas. l)
   //  par_trick[34]     Is this halo (rho1) the Galactic halo [true or false]
   //  par_trick[35]     Do rho1 and rho2 have the same centre [true or false]

   // Create and fill par[36]
   const int n_par = 36;
   double par_trick[n_par];
   for (int i = 0; i < 20; ++i) par_trick[i] = par[i];
   par_trick[21] = switch_f;
   par_trick[22] = psi_los;
   par_trick[23] = theta_los;
   par_trick[24] = lmin;
   par_trick[25] = lmax;
   par_trick[26] = eps;
   par_trick[27] = phi_los;
   par_trick[28] = 0.; // unused at this stage
   par_trick[29] = 0.; // unused at this stage
   par_trick[30] = phi_los_densitypeak;
   par_trick[31] = r_trick;
   par_trick[32] = 0.;  // not needed here
   par_trick[33] = 0.;  // not needed here
   par_trick[34] = is_rho1_galhalo;
   par_trick[35] = true;
   if ((int)par[10] != 0 && (fabs(par[18] - par[7]) > 1.e-3 || fabs(par[19] - par[8]) > 1.e-3 || fabs(par[20] - par[9]) > 1.e-3))
      par_trick[35] = false;


   // Integration along beta=[0,2pi] (or a smaller range if special case for clump)
   // linear-step integration (Simpson adaptive)
   double res_int = 0.;
   simpson_lin_adapt(fn_beta, beta_min, beta_max, par_trick, res_int, par_trick[26], false);


   // If beta_max-beta_min<2pi, means that we are in the special
   // case for the clump. By construction (see above), the total
   // integral is twice that calculated.
   if (beta_max - beta_min < (2 * PI - 1.e-8))
      res_int *= 2.;

   // If gSIM_ALPHAINT was changed at the beginning of the routine,
   // we have to reset it to its initial value before leaving the routine.
   if (is_opt_integr_alpha)
      gSIM_ALPHAINT = alpha_aperture_ref;



   return res_int + point_like;
}

//______________________________________________________________________________
void fn_beta(double &beta, double par[36], double &res)
{
   //--- fn_beta = \int_{0}^{gSIM_ALPHAINT} fn_beta_alpha.
   //
   // INPUTS:
   //  beta       Integration angle measured from the ref. l.o.s. [rad]
   //  par[0]     rho_1(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[1]     rho_1(r) scale radius [kpc]
   //  par[2]     rho_1(r) shape parameter #1
   //  par[3]     rho_1(r) shape parameter #2
   //  par[4]     rho_1(r) shape parameter #3
   //  par[5]     rho_1(r) card_profile [gENUM_PROFILE]
   //  par[6]     rho_1(r) radius [kpc]
   //  par[7]     rho_1(r) distance to observer [kpc]
   //  par[8]     rho_1(r) psi_center [rad]
   //  par[9]     rho_1(r) theta_center [rad]
   //  par[10]    switch_rho: selects which combination of rho_1 and rho_2 to use
   //                0 -> rho(r) = rho1(r)
   //                1 -> rho(r) = rho1(r)-rho2(r)
   //                2 -> rho(r) = rho1(r)*rho2(r)
   //  par[11]    rho_2(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[12]    rho_2(r) scale radius [kpc]
   //  par[13]    rho_2(r) shape parameter #1
   //  par[14]    rho_2(r) shape parameter #2
   //  par[15]    rho_2(r) shape parameter #3
   //  par[16]    rho_2(r) card_profile [gENUM_PROFILE]
   //  par[17]    rho_2(r) radius [kpc]
   //  par[18]    rho_2(r) distance to observer [kpc]
   //  par[19]    rho_2(r) psi_center [rad]
   //  par[20]    rho_2(r) theta_center [rad]
   //  par[21]    switch_f: selects the function f to integrate
   //                0 -> rho:           for jsub_continuum (also proxy for dP/dV)
   //                1 -> rho^2[or rho]: annihilation[decay] for a single clump or DM halo
   //                2 -> l^2*rho:       for clump number/mass fraction estimates (proxy for dP/dV)
   //                3 -> l^3*rho:       to calculate <l>
   //                4 -> l^4*rho:       to calculate var(l)
   //                5 -> l^-2*rho:      to calculate var(J)
   //                6 -> l^2*rho^2:     to check with Lum direct calculation (integrand corresponds to d3Lum)
   //  par[22]    psi_los: longitude of the line of sight [rad]
   //  par[23]    theta_los: latitude of the line of sight [rad]
   //  par[24]    lmin: lower l.o.s. integration boundary [kpc]
   //  par[25]    lmax: upper l.o.s. integration boundary [kpc]
   //  par[26]    eps: relative precision sought for integrations
   //  par[27]    phi_los: direction between GC and l.o.s. (psi,theta) [rad]
   //  par[28]    beta: integr. angle in the plane perp. to the l.o.s. [rad]
   //  par[29]    alpha: integr. angle measured from the ref. l.o.s. [rad]
   //  par[30]    phi_los_peak: angle between phi_los and density peak [rad]
   //  par[31]    r_trick: radius of the point-like contribution [kpc]
   //  par[32]    l_offset: for the integr. along  l': l'=l+l_offset, dl'=dl [kpc]
   //  par[33]    is_increasing_l (true/false if integr. corresponds to increas./decreas. l)
   //  par[34]    Is this halo (rho1) the Galactic halo [true or false]
   //  par[35]    Do rho1 and rho2 have the same centre [true or false]
   // OUTPUT:
   //  res        Result of fn_beta = \int_{0}^{gSIM_ALPHAINT} fn_beta_alpha

   bool is_rho1_galhalo = par[34];

   //--- Integration over alpha=[0,gSIM_ALPHAINT]
   double alpha_min = 0.;
   double alpha_max = gSIM_ALPHAINT;

   // a) Updates alpha_min
   // If the l.o.s. falls out of the clump volume but the F.O.I. intercepts it
   //  => we reduce the range of alpha to optimise the integration
   if (((int)par[21] == 1 || (int)par[21] == 6) && !is_rho1_galhalo) {
      double alpha_cl = fabs(asin(par[6] / par[7]));
      if (par[30] - alpha_cl > 0)
         alpha_min = par[30] - alpha_cl;
   }

   // b) Updates alpha_max
   // If the field of integration (i.e. alpha_max = gSIM_ALPHAINT) crosses
   // the centre of the clump and the profile is cuspy, the simpson routine
   // get stuck. Such a situation is rare but not impossible
   //  => we slightly reduce alpha_max to avoid the problematic region
   if (fabs(par[30] - gSIM_ALPHAINT) / gSIM_ALPHAINT < 1.e-3)
      alpha_max = 0.999 * gSIM_ALPHAINT;

   // c) Performs the integration
   par[28] = beta; // set current value of beta
   double res_int = 0;
   simpson_lin_adapt(fn_beta_alpha, alpha_min, alpha_max, par, res_int, par[26], false);
   res = res_int;
}

//______________________________________________________________________________
void fn_beta_alpha(double &alpha, double par[36], double &res)
{
   //--- fn_beta_alpha = sin(alpha) \int_{lmin}^{lmax} f(l,beta,alpha; psi,theta) dl.
   //
   // INPUTS:
   //  alpha      Integration angle measured from the ref. l.o.s. [rad]
   //  par[0]     rho_1(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[1]     rho_1(r) scale radius [kpc]
   //  par[2]     rho_1(r) shape parameter #1
   //  par[3]     rho_1(r) shape parameter #2
   //  par[4]     rho_1(r) shape parameter #3
   //  par[5]     rho_1(r) card_profile [gENUM_PROFILE]
   //  par[6]     rho_1(r) virial radius [kpc]
   //  par[7]     rho_1(r) distance to observer [kpc]
   //  par[8]     rho_1(r) psi_center [rad]
   //  par[9]     rho_1(r) theta_center [rad]
   //  par[10]    switch_rho: selects which combination of rho_1 and rho_2 to use
   //                0 -> rho(r) = rho1(r)
   //                1 -> rho(r) = rho1(r)-rho2(r)
   //                2 -> rho(r) = rho1(r)*rho2(r)
   //  par[11]    rho_2(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[12]    rho_2(r) scale radius [kpc]
   //  par[13]    rho_2(r) shape parameter #1
   //  par[14]    rho_2(r) shape parameter #2
   //  par[15]    rho_2(r) shape parameter #3
   //  par[16]    rho_2(r) card_profile [gENUM_PROFILE]
   //  par[17]    rho_2(r) virial radius [kpc]
   //  par[18]    rho_2(r) distance to observer [kpc]
   //  par[19]    rho_2(r) psi_center [rad]
   //  par[20]    rho_2(r) theta_center [rad]
   //  par[21]    switch_f: selects the function f to integrate
   //                0 -> rho:           for jsub_continuum (also proxy for dP/dV)
   //                1 -> rho^2[or rho]: annihilation[decay] for a single clump or DM halo
   //                2 -> l^2*rho:       for clump number/mass fraction estimates (proxy for dP/dV)
   //                3 -> l^3*rho:       to calculate <l>
   //                4 -> l^4*rho:       to calculate var(l)
   //                5 -> l^-2*rho:      to calculate var(J)
   //                6 -> l^2*rho^2:     to check with Lum direct calculation (integrand corresponds to d3Lum)
   //  par[22]    psi_los: longitude of the line of sight [rad]
   //  par[23]    theta_los: latitude of the line of sight [rad]
   //  par[24]    lmin: lower l.o.s. integration boundary [kpc]
   //  par[25]    lmax: upper l.o.s. integration boundary [kpc]
   //  par[26]    eps: relative precision sought for integrations
   //  par[27]    phi_los: direction between GC and l.o.s. (psi,theta) [rad]
   //  par[28]    beta: integr. angle in the plane perp. to the l.o.s. [rad]
   //  par[29]    alpha: integr. angle measured from the ref. l.o.s. [rad]
   //  par[30]    phi_los_peak: angle between phi_los and density peak [rad]
   //  par[31]    r_trick: radius of the point-like contribution [kpc]
   //  par[32]    l_offset: for the integr. along  l': l'=l+l_offset, dl'=dl [kpc]
   //  par[33]    is_increasing_l (true/false if integr. corresponds to increas./decreas. l)
   //  par[34]    Is this halo (rho1) the Galactic halo [true or false]
   //  par[35]    Do rho1 and rho2 have the same centre [true or false]
   // OUTPUT:
   //  res        Result of sin(alpha) \int_{lmin}^{lmax} f(l,beta,alpha; psi,theta) dl

   bool is_rho1_galhalo = par[34];

   // Update current value of alpha
   par[29] = alpha;
   double lmin = par[24];
   double lmax = par[25];

   //-- How to integrate on l?
   // I.   We first calculate the angle "phi_loi_densitypeak" between the integration direction
   //      and the centre of the density peak (a.k.a. the centre of the clump, or the centre of
   //      the Galaxy if the global DM halo is considered).
   // II.  If |phi_integr|>pi/2 and if not a clump => rho2 monotonically decreases with l.
   //      A single log-step integration suffices.
   // III. If |phi_integr|<pi/2, or if we integrate a clump, rho2 is no longer monotonic
   //      The integral is broken down in two parts, both of which are now monotonic
   // N.B.: for case III, we remind that a point-like integration on a radius r_trick(=par[35])
   // was performed to speed up the calculation. Hence, we need to skip this region!

   //-------------------------------------------------------------------------------------
   // I. Calculate phi_loi_densitypeak = angle(line of integration "loi", centre of clump)
   //-------------------------------------------------------------------------------------
   // phi_integr_densitypeak is the angle between the observed direction
   // X(psi, theta, alpha, beta) and the centre of the clump to integrate
   double phi_loi_densitypeak = 0.;

   // x_loi: coordinates of a point in the direction of the l.o.i (line of integration)
   // The function lalphabeta_to_xyzgal (geometry.h) returns cartesian coordinates
   // in the framework attached to the GC.
   //  => We add Rsol to x[0] to have the coordinates in the frame attached to Earth
   double psi_theta[2] = {par[22], par[23]};   // Set psi, theta
   double x_loi[3] = {1., par[29], par[28]}; // Set l, alpha, beta ("loi" is for line of integration)
   lalphabeta_to_xyzgal(x_loi, psi_theta);      // Convert (l,alpha,beta) to (x,y,z)_gal
   x_loi[0] += gMW_RSOL;

   if (is_rho1_galhalo) {
      //-- This is the Galactic DM halo: angle between loi direction and GC is
      //     cos(phi_loi_densitypeak) = x_loi[0]/||x_loi||
      phi_loi_densitypeak = acos(x_loi[0]
                                 / sqrt(x_loi[0] * x_loi[0] + x_loi[1] * x_loi[1] + x_loi[2] * x_loi[2]));

   } else {
      //-- This is a clump: centre is (l_cl, psi_cl, theta_cl)
      // The coordinates of the clump in the frame attache to Earth are:
      double psi_theta_cl[2] = {par[8], par[9]};   // Set psi_cl, theta_cl
      double x_cl[3] = {1., 0., 0.};               // Set l_cl, alpha_cl=0, beta_cl=0
      lalphabeta_to_xyzgal(x_cl, psi_theta_cl);       // Convert (l,alpha,beta) to (x,y,z)_gal
      x_cl[0] += gMW_RSOL;

      // Calculate phi_loi_densitypeak (using scalar product):
      //    ||x_loi||.||x_cl||.cos(phi_loi_densitypeak)=sum_{i=1,3} x_loi[i].x_cl[i]
      phi_loi_densitypeak = 0.;
      double cos_phi = (x_loi[0] * x_cl[0] + x_loi[1] * x_cl[1] + x_loi[2] * x_cl[2])
                       / (sqrt(x_loi[0] * x_loi[0] + x_loi[1] * x_loi[1] + x_loi[2] * x_loi[2])
                          * sqrt(x_cl[0] * x_cl[0] + x_cl[1] * x_cl[1] + x_cl[2] * x_cl[2]));
      if (cos_phi < 1.) phi_loi_densitypeak = acos(cos_phi);
      // N.B.: On some compilers, "1"=1.0000000000001, which makes acos("1") crash.
      // So better safe than sorry! (acos returns an angle in [0,pi])
   }

   //---------------------------------------------------------------------------------------------------
   // II. phi_loi_densitypeak>pi/2 and within halo => rho2 decreases with l, simple log-step integration
   //---------------------------------------------------------------------------------------------------
   // The maximum of rho^2 is ~ at Earth location => we use a standard log-step integration
   if (phi_loi_densitypeak >= PI / 2.  && par[7] < par[6]) {

      double res_int = 0.;
      res = 0.;
      if (lmin < 1.e-40) {
         double lmin_lin = 1.e-20 * par[6];
         if (lmax > lmin_lin) {
            simpson_lin_adapt(integrand_l, lmin, lmin_lin, par, res_int, par[26]);
            if (!gSIM_IS_SZ) res +=  res_int * MY_SIN(alpha);
            else res +=  res_int;
            lmin = lmin_lin;
         }

         double lmin_log1 = 1.e-3 * par[6];
         if (lmax > lmin_log1) {
            simpson_log_adapt(integrand_l, lmin_lin, lmin_log1, par, res_int, par[26]);
            lmin = lmin_log1;
            if (!gSIM_IS_SZ) res +=  res_int * MY_SIN(alpha);
            else res +=  res_int;

         }
      }
      simpson_log_adapt(integrand_l, lmin, lmax, par, res_int, par[26]);
      if (!gSIM_IS_SZ) res +=  res_int * MY_SIN(alpha);
      else res +=  res_int;


      return;
   }

   //---------------------------------------------------------------------------------------
   // III. For phi_loi_densitypeak<pi/2 or if outside halo => rho2 is not monotonic with l... stuff happens
   //---------------------------------------------------------------------------------------
   // Integration broken down in two parts: I[lmin,l_rhomax] and I[l_rhomax,lmax]
   // Special case if lmin>l_rhomax or lmax<l_rhomax
   //
   // 1. We first calculate the distance l_rhomax (in the direction
   //    of the line of integration phi_loi_densitypeak) for which
   //    we cross the max of the DM density. The profiles are mostly
   //    spherical, so that
   //        l_rhomax = l_cl * |MY_COS(phi_loi_densitypeak)|, where l_cl = par[7]
   double l_rhomax = fabs(par[7] * MY_COS(phi_loi_densitypeak));
   double r_rhomax = fabs(par[7] * MY_SIN(phi_loi_densitypeak));

   // 2. If the clump volume does not encompass the observer, updates integral boundaries
   // (integration has to start at the boundary of the clump, the size of which is par[6]).
   if (par[7] > par[6]) {
      // For some directions, the l.o.i. falls outside the clump radius...
      // i) if r_rhomax>Rvir => return 0.
      double alpha_cl = fabs(asin(par[6] / par[7]));
      if (phi_loi_densitypeak > alpha_cl) {
         res = 0.;
         return;
      }
      // ii) Otherwise the boundaries of the clump are:
      double tmp = sqrt(par[6] * par[6] - r_rhomax * r_rhomax);
      lmin = l_rhomax - tmp;
      lmax = l_rhomax + tmp;
      //cout << "lmin = " << lmin <<", lmax = " << lmax <<  endl;
   }

   // Most integrations are performed on lrel defined related to l_rhomax (lrel = fabs(l-l_rhomax))
   // N.B.: par[33]=is_increasing_l, to decide whether integration corresponds
   // to increasing or decreasing l (to speed up integration)
   // N.B.: we also define some useful distances with respect to l = l_rhomax
   // The integration is cast in 5 different zones. Indeed, as there is a break
   // in the profile slope (at rs), it is better to break the integration in two parts
   // (proven to work well empirically), where each integration is broken up in two
   // (around the density peak). And because log-step integration are used for them,
   // a lin-step in the inner part is required to ensure nothing is left. All in all,
   // we need to take into account:
   //   - a small zone around l_rhomax+/-d_lin for the linear part;
   //   - a log-step integration from  l_rhomax+/-d_lin to l_rhomax+/-d_log
   //   - a log-step integration from  l_rhomax+/-d_log to l_rhomax+/-d_cl_boundary
   // In the process, we have to ensure that the user-input [lmin-lmax] region is correctly
   // taken into account, and that the l_rhomax+/-d_trick region is skipped if necessary.
   par[32] = l_rhomax;

   double lmin_lrel = fabs(lmin - l_rhomax);
   double lmax_lrel = fabs(lmax - l_rhomax);
   double d_lin = 1.e-5 * par[6];
   double d_log = 1.e-1 * par[6];
   double d_trick = 0.;
   double res_int = 0., I = 0.;

   if (par[31] > r_rhomax)
      d_trick = sqrt(par[31] * par[31] - r_rhomax * r_rhomax);

   // 1. Linear integration (only if l_trick==0)
   // => Actually including this parts slows down the code while
   // not contributing negligibly to J
   if (d_trick < 1.e-20) {
      //double lmin_lin = l_rhomax - d_lin;
      //double lmax_lin = l_rhomax + d_lin;
      //if (lmin > lmin_lin) lmin_lin = lmin;
      //if (lmax < lmax_lin) lmax_lin = lmax;
      //simpson_lin_adapt(integrand_l, lmin_lin, lmax_lin, par, I, par[26]);
      //res_int +=  I;
   } else {
      d_lin = d_trick;
      if (d_lin > d_log)
         d_log = d_lin;
   }


   //--- Integral split in increasing and decreasing part (around l_rhomax)
   // A. Integration for increasing l (requires lmax>l_rhomax)
   par[33] = 1;
   if (lmax > l_rhomax) {
      // 2. Integration from l_rhomax+d_lin to l_rhomax+d_log
      // 3. Integration from l_rhomax+d_log to l_rhomax+d_max
      double lrelmin_inner = d_lin;
      double lrelmax_inner = d_log;
      double lrelmin_outer = d_log;
      double lrelmax_outer = lmax_lrel;

      if (lmin > l_rhomax && lmin_lrel > d_log) {
         lrelmin_outer = lmin_lrel;
         simpson_log_adapt(integrand_lrel, lrelmin_outer, lrelmax_outer, par, I, par[26]);
         res_int +=  I;
      } else if (lmax_lrel < d_log) {
         lrelmax_inner = lmax_lrel;
         if (lmin > l_rhomax && lmin_lrel > d_lin) lrelmin_inner = lmin_lrel;
         simpson_log_adapt(integrand_lrel, lrelmin_inner, lrelmax_inner, par, I, par[26]);
         res_int +=  I;
      } else {
         if (lmin > l_rhomax && lmin_lrel > d_lin) lrelmin_inner = lmin_lrel;
         simpson_log_adapt(integrand_lrel, lrelmin_inner, lrelmax_inner, par, I, par[26]);
         res_int +=  I;
         simpson_log_adapt(integrand_lrel, lrelmin_outer, lrelmax_outer, par, I, par[26]);
         res_int +=  I;
      }
   }

   int switch_f = int(par[21]);
   // do short cut to speed up calculation only in case observer is sitting outside the halo (r_obs > r_vir),
   // and in case the integrand does not depend on the line of sight.
   if ((switch_f == 0 || switch_f == 1) && par[7] > par[6]) res_int *= 2;
   else {
      // B. Integration for decreasing l (requires lmin<l_rhomax)
      par[33] = 0;
      if (lmin < l_rhomax) {
         // 3. Integration from l_rhomax-d_max to l_rhomax-d_log
         // 2. Integration from l_rhomax-d_log to l_rhomax-d_lin
         // N.B.: formally it is almost exactly the same as before,
         // except that par[33] now equals 0 and some condition on lmax instead of lmin
         double lrelmin_inner = d_lin;
         double lrelmax_inner = d_log;
         double lrelmin_outer = d_log;
         double lrelmax_outer = lmin_lrel;

         if (lmax < l_rhomax && lmax_lrel > d_log) {
            lrelmin_outer = lmax_lrel;
            simpson_log_adapt(integrand_lrel, lrelmin_outer, lrelmax_outer, par, I, par[26]);
            res_int +=  I;
         } else if (lmin_lrel < d_log) {
            lrelmax_inner = lmin_lrel;
            if (lmax < l_rhomax && lmax_lrel > d_lin) lrelmin_inner = lmax_lrel;
            simpson_log_adapt(integrand_lrel, lrelmin_inner, lrelmax_inner, par, I, par[26]);
            res_int +=  I;
         } else {
            if (lmax < l_rhomax && lmax_lrel > d_lin) lrelmin_inner = lmax_lrel;
            simpson_log_adapt(integrand_lrel, lrelmin_inner, lrelmax_inner, par, I, par[26]);
            res_int +=  I;
            simpson_log_adapt(integrand_lrel, lrelmin_outer, lrelmax_outer, par, I, par[26]);
            res_int +=  I;
         }
      }
   }
   res = res_int;
   if (!gSIM_IS_SZ) res = res_int * MY_SIN(alpha);
   return;

}

//______________________________________________________________________________
void integrand_l(double &l, double par[36], double &res)
{
   //--- Value of the integrand (related to the DM density) at position (l,alpha,beta).
   //
   // INPUTS:
   //  l          Integration distance [kpc]
   //  par[0]     rho_1(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[1]     rho_1(r) scale radius [kpc]
   //  par[2]     rho_1(r) shape parameter #1
   //  par[3]     rho_1(r) shape parameter #2
   //  par[4]     rho_1(r) shape parameter #3
   //  par[5]     rho_1(r) card_profile [gENUM_PROFILE]
   //  par[6]     rho_1(r) radius [kpc]
   //  par[7]     rho_1(r) distance to observer [kpc]
   //  par[8]     rho_1(r) psi_center [rad]
   //  par[9]     rho_1(r) theta_center [rad]
   //  par[10]    switch_rho: selects which combination of rho_1 and rho_2 to use
   //                0 -> rho(r) = rho1(r)
   //                1 -> rho(r) = rho1(r)-rho2(r)
   //                2 -> rho(r) = rho1(r)*rho2(r)
   //  par[11]    rho_2(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[12]    rho_2(r) scale radius [kpc]
   //  par[13]    rho_2(r) shape parameter #1
   //  par[14]    rho_2(r) shape parameter #2
   //  par[15]    rho_2(r) shape parameter #3
   //  par[16]    rho_2(r) card_profile [gENUM_PROFILE]
   //  par[17]    rho_2(r) radius [kpc]
   //  par[18]    rho_2(r) distance to observer [kpc]
   //  par[19]    rho_2(r) psi_center [rad]
   //  par[20]    rho_2(r) theta_center [rad]
   //  par[21]    switch_f: selects the function f to integrate
   //                0 -> rho:           for jsub_continuum (also proxy for dP/dV)
   //                1 -> rho^2[or rho]: annihilation[decay] for a single clump or DM halo
   //                2 -> l^2*rho:       for clump number/mass fraction estimates (proxy for dP/dV)
   //                3 -> l^3*rho:       to calculate <l>
   //                4 -> l^4*rho:       to calculate var(l)
   //                5 -> l^-2*rho:      to calculate var(J)
   //                6 -> l^2*rho^2:     to check with Lum direct calculation (integrand corresponds to d3Lum)
   //  par[22]    psi_los: longitude of the line of sight [rad]
   //  par[23]    theta_los: latitude of the line of sight [rad]
   //  par[24]    lmin: lower l.o.s. integration boundary [kpc]
   //  par[25]    lmax: upper l.o.s. integration boundary [kpc]
   //  par[26]    eps: relative precision sought for integrations
   //  par[27]    phi_los: direction between GC and l.o.s. (psi,theta) [rad]
   //  par[28]    beta: integr. angle in the plane perp. to the l.o.s. [rad]
   //  par[29]    alpha: integr. angle measured from the ref. l.o.s. [rad]
   //  par[30]    phi_los_peak: angle between phi_los and density peak [rad]
   //  par[31]    r_trick: radius of the point-like contribution [kpc]
   //  par[32]    l_offset: for the integr. along  l': l'=l+l_offset, dl'=dl [kpc]
   //  par[33]    is_increasing_l (true/false if integr. corresponds to increas./decreas. l)
   //  par[34]    Is this halo (rho1) the Galactic halo [true or false]
   //  par[35]    Do rho1 and rho2 have the same centre [true or false]
   // OUTPUT:
   //  res        Value of the integrand

   int switch_f = (int)par[21];
   bool is_rho1_galhalo = par[34];
   bool is_same_centre = par[35];

   // Calculate cartesian coordinates x[3] of the current point
   // [using lalphabeta_to_xyzgal, see geometry.h]
   double psi_theta[2] = {par[22], par[23]}; // Set psi_los, theta_los
   double x[3] = {l, par[29], par[28]};      // Set l, alpha, beta
   lalphabeta_to_xyzgal(x, psi_theta);          // Convert (l,alpha,beta) to (x,y,z)_gal

   // Distance from the halo centre
   double r = 0.;  // r corresponding to l,psi,theta position
   double r2 = 0.; // r2 corresponds to l,psi,theta position if the second halo rho_2's
   // centre is not the same as rho_1's centre (e.g., for cross-product tests)

   // If the halo is the Galactic halo (for rho1 and rho2)
   if (is_rho1_galhalo && is_same_centre) {
      // Different calculation whether this is triaxial or spherical halo
      if (!gMW_TRIAXIAL_IS) {
         r = sqrt(x[0] * x[0] + x[1] * x[1] + x[2] * x[2]);
      } else {
         r = get_riso_triaxial(x, gMW_TRIAXIAL_AXES, gMW_TRIAXIAL_ROTANGLES);
      }
   } else {
      // If spherical halo
      if (!gHALO_TRIAXIAL_IS) {
         // Calculate cartesian coordinates x_cl[3] of the current halo
         // Centre of the clump is (l_cl, psi_cl, theta_cl)
         double psi_theta_cl[2] = {par[8], par[9]}; // Set psi_cl, theta_cl
         double x_cl[3] = {par[7], 0., 0.};         // Set l_cl=par[7], alpha_cl=0, beta_cl=0
         lalphabeta_to_xyzgal(x_cl, psi_theta_cl);     // Convert (l,alpha,beta) to (x,y,z)_gal
         r = sqrt((x_cl[0] - x[0]) * (x_cl[0] - x[0])
                  + (x_cl[1] - x[1]) * (x_cl[1] - x[1])
                  + (x_cl[2] - x[2]) * (x_cl[2] - x[2]));

         if (!is_same_centre) {
            // Calculate cartesian coordinates x_cl[3] of the current halo
            // Centre of the clump is (l_cl, psi_cl, theta_cl)
            double psi_theta_cl2[2] = {par[19], par[20]}; // Set psi_cl, theta_cl
            double x_cl2[3] = {par[18], 0., 0.};         // Set l_cl=par[18], alpha_cl=0, beta_cl=0
            lalphabeta_to_xyzgal(x_cl2, psi_theta_cl2);     // Convert (l,alpha,beta) to (x,y,z)_gal
            r2 = sqrt((x_cl2[0] - x[0]) * (x_cl2[0] - x[0])
                      + (x_cl2[1] - x[1]) * (x_cl2[1] - x[1])
                      + (x_cl2[2] - x[2]) * (x_cl2[2] - x[2]));
         }
      } else {
         double x_los[3] = {l, par[29], par[28]};        // Set l, alpha, beta
         lalphabeta_to_xyzlos(x_los, &par[22], par[7]); // Convert (l,alpha,beta) to (x,y,z)_los, using the distance par[7] to the halo
         r = get_riso_triaxial(x_los, gHALO_TRIAXIAL_AXES, gHALO_TRIAXIAL_ROTANGLES);
      }
   }

   // Calculate rho(r) depending on the chosen profiles (and switch_rho)
   if (is_same_centre) {
      if (r >= par[6])
         res = 0.;
      else
         rho_mix(r, par, res);
      //cout << r << " " << res << endl;

   } else {
      if (r2 >= par[17])
         res = 0.;
      else {
         double res1 = 0.;
         rho(r, par, res1);
         double res2 = 0.;
         rho(r2, &par[11], res2);
         int switch_rho = (int)par[10];
         switch (switch_rho) {
            case 1:
               res = res2 - res1;
               break;
            case 2:
               res = res2 * res1;
               break;
            default :
               printf("\n====> ERROR: integrand_l() in integr_los.cc");
               printf("\n             switch_rho=%d does not correspond to any combo for rho_1 and rho_2", switch_rho);
               printf("\n             => abort()\n\n");
               abort();
         }
      }
   }

   // Calculate res=f(rho(r)) depending on the chosen integrand
   switch (switch_f) {
      case 0:
         return;
      case 1:
         if (gPP_DM_IS_ANNIHIL_OR_DECAY) res *= res;
         return;
      case 2:
         res *= (l * l);
         return;
      case 3:
         res *= (l * l * l);
         return;
      case 4:
         res *= (l * l * l * l);
         return;
      case 5:
         res /= (l * l);
         return;
      case 6:
         res *= (res * l * l);
         return;
      default :
         printf("\n====> ERROR: integrand_l() in integr_los.cc");
         printf("\n             switch_f=%d is not a valid option", switch_f);
         printf("\n             => abort()\n\n");
         abort();
   }
   return;
}

//______________________________________________________________________________
void integrand_lrel(double &lrel, double par[36], double &res)
{
   //--- Value of the integrand (related to the DM density) at position (lrel,alpha,beta).
   // N.B.: lrel is the distance relative to l_rhomax -- l.o.s "l" for which rho(l) is max --).
   //      => lrel = |l-l_rhomax|
   //
   // INPUTS:
   //  lrel       Relative integration distance (offset) [kpc]
   //  par[0]     rho_1(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[1]     rho_1(r) scale radius [kpc]
   //  par[2]     rho_1(r) shape parameter #1
   //  par[3]     rho_1(r) shape parameter #2
   //  par[4]     rho_1(r) shape parameter #3
   //  par[5]     rho_1(r) card_profile [gENUM_PROFILE]
   //  par[6]     rho_1(r) radius [kpc]
   //  par[7]     rho_1(r) distance to observer [kpc]
   //  par[8]     rho_1(r) psi_center [rad]
   //  par[9]     rho_1(r) theta_center [rad]
   //  par[10]    switch_rho: selects which combination of rho_1 and rho_2 to use
   //                0 -> rho(r) = rho1(r)
   //                1 -> rho(r) = rho1(r)-rho2(r)
   //                2 -> rho(r) = rho1(r)*rho2(r)
   //  par[11]    rho_2(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[12]    rho_2(r) scale radius [kpc]
   //  par[13]    rho_2(r) shape parameter #1
   //  par[14]    rho_2(r) shape parameter #2
   //  par[15]    rho_2(r) shape parameter #3
   //  par[16]    rho_2(r) card_profile [gENUM_PROFILE]
   //  par[17]    rho_2(r) radius [kpc]
   //  par[18]    rho_2(r) distance to observer [kpc]
   //  par[19]    rho_2(r) psi_center [rad]
   //  par[20]    rho_2(r) theta_center [rad]
   //  par[21]    switch_f: selects the function f to integrate
   //                0 -> rho:           for jsub_continuum (also proxy for dP/dV)
   //                1 -> rho^2[or rho]: annihilation[decay] for a single clump or DM halo
   //                2 -> l^2*rho:       for clump number/mass fraction estimates (proxy for dP/dV)
   //                3 -> l^3*rho:       to calculate <l>
   //                4 -> l^4*rho:       to calculate var(l)
   //                5 -> l^-2*rho:      to calculate var(J)
   //                6 -> l^2*rho^2:     to check with Lum direct calculation (integrand corresponds to d3Lum)
   //  par[22]    psi_los: longitude of the line of sight [rad]
   //  par[23]    theta_los: latitude of the line of sight [rad]
   //  par[24]    lmin: lower l.o.s. integration boundary [kpc]
   //  par[25]    lmax: upper l.o.s. integration boundary [kpc]
   //  par[26]    eps: relative precision sought for integrations
   //  par[27]    phi_los: direction between GC and l.o.s. (psi,theta) [rad]
   //  par[28]    beta: integr. angle in the plane perp. to the l.o.s. [rad]
   //  par[29]    alpha: integr. angle measured from the ref. l.o.s. [rad]
   //  par[30]    phi_los_peak: angle between phi_los and density peak [rad]
   //  par[31]    r_trick: radius of the point-like contribution [kpc]
   //  par[32]    l_offset: for the integr. along  l': l'=l+l_offset, dl'=dl [kpc]
   //  par[33]    is_increasing_l (true/false if integr. corresponds to increas./decreas. l)
   //  par[34]    Is this halo (rho1) the Galactic halo [true or false]
   //  par[35]    Do rho1 and rho2 have the same centre [true or false]
   // OUTPUT:
   //  res        Value of the integrand

   // Set l from lrel (= +/-lrel + l_offset)
   double l = 0.;
   if ((bool)par[33]) l = lrel +  par[32];
   else l = -lrel +  par[32];


   integrand_l(l, par, res);

}

