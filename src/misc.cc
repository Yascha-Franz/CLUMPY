/*! \file misc.cc \brief (see misc.h) */

// CLUMPY includes
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/inlines.h"

// ROOT includes
#if IS_ROOT
#include <TStyle.h>
#endif

// C++ std libraries
using namespace std;
#include <iostream>
#include <cstring>
#include <fstream>
#include <stdlib.h>
#include <iomanip>
#include <sys/stat.h>
#include <string_utils.h>
#include <dirent.h>

// GSL includes
#include <gsl/gsl_integration.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_deriv.h>
#include <gsl/gsl_chebyshev.h>
#include <gsl/gsl_math.h>

//______________________________________________________________________________
bool execute_test(const string &mode, const vector<string> &filenames_test, const string &exe_dir,
                  const string &test_dir, const string &compare_dir, const string &special_command)
{
   //--- Executes test of clumpy function.
   //
   // INPUTS:
   //  mode             Clumpy simulation mode to test (e.g., g0, g1, ...)
   //  filenames_test   Output file(s) to test.
   //  exe_dir          Absolute path of clumpy executable.
   //  test_dir         Directory into which test results are written.
   //  compare_dir      Directory where to find output files against which test
   //                   results (filenames_test) are compared.
   //  special_command  Add a special command of your choice when executing the test.
   //
   // OUTPUT:
   //    true: test passed, false: test not passed.

   string command;
   string nulloutput = " > nul";
   string output_passed = COLOR_BGREEN "OK" COLOR_RESET;
   string output_not_passed = COLOR_BRED "not passed" COLOR_RESET;
   bool is_passed = true;

   cout << " Testing module -" << mode << " ..." << endl;

   for (int i = 0; i < int(filenames_test.size()); ++i) {
      command = "cd " + test_dir + "; touch " + filenames_test[i] + "; cd " + gPATH_TO_USER_EXE;
      sys_safe_execution(command);
   }

   string loadgreat = "";

   char *env = getenv("GREAT");
   if (env != NULL) {
      loadgreat = "export LD_LIBRARY_PATH=$GREAT/lib:$LD_LIBRARY_PATH; export DYLD_LIBRARY_PATH=$GREAT/lib:$DYLD_LIBRARY_PATH; ";
   }

   command = loadgreat + "cd " + test_dir + "; "
             + exe_dir + "/clumpy -" + mode + " -D " + nulloutput  + "; "
             + exe_dir + "/clumpy -" + mode + " -p -i " + test_dir
             + "/clumpy_params_" + mode + ".txt --gSIM_OUTPUT_DIR="
             + test_dir + " --gSIM_IS_WRITE_ROOTFILES=0";

   if (mode == "r1") {
      command = loadgreat + "cd " + test_dir + "; "
                + exe_dir + "/clumpy_jeansChi2 -" + mode + " " + gPATH_TO_CLUMPY_HOME + "/data/data_sigmap.txt "
                + test_dir + "/chi2_binned.dat " + gPATH_TO_CLUMPY_HOME + "/data/params_jeans.txt 0.05";
   }

   if (special_command != "none") {
      command += " ";
      command += special_command;
      command += nulloutput;
   } else
      command += nulloutput;

   command += "; cd " + gPATH_TO_USER_EXE;

   sys_safe_execution(command);

   string diffstring = "dummy";
   for (int i = 0; i < int(filenames_test.size()); ++i) {
      command = "diff " + test_dir + "/" + filenames_test[i] + " " + compare_dir + "/" + filenames_test[i];
      diffstring = sys_command(command);

      if (diffstring == "") {
         command = "rm " + test_dir + "/" + filenames_test[i];
         sys_safe_execution(command);
      } else {
         is_passed = false;
      }
   }

   if (is_passed) {
      cout <<  output_passed << endl;
   } else {
      cout <<  output_not_passed << endl;
   }
   return is_passed;
}

//______________________________________________________________________________
double find_max_value(vector<double> &array, int i_start, int i_end)
{
   //--- Returns largest value in array.
   //
   //  array         Vector of double in which to search for max
   //  i_start       Specify index where to start to search for maximum (optional)
   //  i_end         Specify index where to stop to search for maximum (optional)

   // start with max = first element
   double max = array[i_start];
   if (i_end == -1) i_end = int(array.size()) - 1;
   for (int i = i_start + 1; i <= i_end; ++i) {
      if (array[i] > max)
         max = array[i];
   }
   return max;
}

//______________________________________________________________________________
double find_min_value(vector<double> &array, int i_start, int i_end)
{
   //--- Returns smallest value in array.
   //
   //  array         Vector of double in which to search for min
   //  i_start       Specify index where to start to search for minimum (optional)
   //  i_end         Specify index where to stop to search for minimum (optional)

   // start with min = first element
   double min = array[i_start];
   if (i_end == -1) i_end = int(array.size()) - 1;
   for (int i = i_start + 1; i <= i_end; ++i) {
      if (array[i] < min)
         min = array[i];
   }
   return min;
}

//______________________________________________________________________________
void find_x_logstep(double const &y_ref, double &res, double x1, double x2,
                    void fn(double &, double *, double &), double *par,
                    double const &tol, double y1, double y2, int n_iter)
{
   //--- Finds and returns recursively x_ref for which fn(x_ref)=y_ref in the region.
   //    N.B.: applicable only for a monotone function.
   //
   // INPUTS:
   //  y_ref         Reference value (sought)
   //  x1            Lower value of interval
   //  x2            Upper value of interval
   //  fn            Function to use
   //  par           Parameters of fn
   //  tol           Relative precision to search for y_ref
   //  y1            f(x1)
   //  y2            f(x2)
   //  n_iter        Number of iteration in recursion
   // OUTPUT:
   //  res           Value x for which |f(x)-y_ref|/y_ref<tol

   bool is_verbose = false;

   // If first iteration, calculate both y(x1) and y(x2), and check that not zero!
   if (n_iter == 0) {
      fn(x1, par, y1);
      fn(x2, par, y2);
      if (is_verbose)
         cout << "       START find_x_logstep (x1,x2,y1,y2,y_ref): "
              << x1 << " " << x2 << " " << y1 << " " << y2 << " " << y_ref << endl;
      if (x1 > x2 || fabs(x1 - x2) / x2 < 0.05) {
         res = sqrt(x1 * x2);
         return;
      }
   } else if (n_iter > 20) {
      printf("\n====> ERROR: find_x_logstep() in misc.cc");
      printf("\n             Too many doubling steps (max=20) without convergence.");
      printf("\n             => abort()\n\n");
      abort();
   }

   // If precision reached, result is x1 (or x2)
   if (fabs((y1 - y_ref) / y_ref) < tol) {
      res = x1;
      return;
   } else if (fabs((y2 - y_ref) / y_ref) < tol) {
      res = x2;
      return;
   } else if (x1 > x2 || fabs(x1 - x2) / x2 < 0.05) {
      res = sqrt(x1 * x2);
      return;
   }


   // Calculate value at new point
   double x_test = sqrt(x1 * x2);
   double y_test = 0.;
   fn(x_test, par, y_test);

   if (is_verbose)
      cout << "         IN find_x_logstep (x1,x2,x_test,y1,y2,y_test): "
           << x1 << " " << x2 << " " << x_test << " " << y1 << " " << y2 << " " << y_test << endl;

   if ((y_test > y2 && y_test > y1) || (y_test < y2 && y_test < y1)) {
      if (is_verbose) {
         print_warning("misc.cc", "find_x_logstep()", "Function is not monotonous... => Returns right boundary.");
      }
      res = y2;
      return;
   }

   // Check that range is correct
   if ((y_test > y1 && (y_ref < y1 || y_ref > y2)) || (y_test < y1 && (y_ref > y1 || y_ref < y2))) {
      printf("\n====> ERROR: find_x_logstep() in misc.cc");
      printf("\n             y_ref=%.2le not inside [y1,y2]=[%.2le,%.2le].", y_ref, y1, y2);
      printf("\n             => abort()\n\n");
      abort();
   }

   // If growing function (+ special case if value reaches zero!)
   if (y2 > y1) {
      if (y_test > y_ref) {
         x2 = x_test;
         y2 = y_test;
      } else {
         x1 = x_test;
         y1 = y_test;
      }
   } else {
      if (y_test < y_ref) {
         x2 = x_test;
         y2 = y_test;
      } else {
         x1 = x_test;
         y1 = y_test;
      }
   }

   ++n_iter;
   find_x_logstep(y_ref, res, x1, x2, fn, par, tol, y1, y2, n_iter);
}

//______________________________________________________________________________
void get_clumpy_dirs(string const &argv0)
{
   //--- Sets the global variables gPATH_TO_CLUMPY_EXE (the absolute path of the executed binary),
   //    and gPATH_TO_CLUMPY_HOME (the absolute path to the CLUMPY directory, where all the
   //    auxiliary data file are stored. gPATH_TO_CLUMPY_HOME is searched over the environmental
   //    variable CLUMPY, and if not found, assumed one level above the executable.
   //    An error is drawn if it is not found there either.
   //  argv0               argv[0] string

   gPATH_TO_USER_EXE = string(getenv("PWD")) + "/";

   // get the absolute path of executable:
   string executable = sys_command(string("which ") + argv0);
   // get the path:
   string path_raw = executable.substr(0, executable.find_last_of("\\/"));
   string path_raw2 = removeslashes_from_end(path_raw);
   string get_clumpydir_command = "cd " + path_raw2 + "; echo $PWD; cd " + gPATH_TO_USER_EXE;
   string path_clumpy_noendslash = sys_command(get_clumpydir_command);
   gPATH_TO_CLUMPY_EXE = path_clumpy_noendslash + "/";

   // try to find gPATH_TO_CLUMPY_HOME over CLUMPY environmental variable:
   char *env = getenv("CLUMPY");
   if (env == NULL) {
      // try to find the directory one level above executable:
      gPATH_TO_CLUMPY_HOME = path_clumpy_noendslash.substr(0, path_clumpy_noendslash.find_last_of("\\/") + 1);
   } else {
      string env_str = string(env);
      if (env_str[env_str.size() - 1] != '/') gPATH_TO_CLUMPY_HOME = env_str + "/";
      else gPATH_TO_CLUMPY_HOME = env_str;
   }

   // check whether CLUMPY home directory is meaningful:
   string clumpydatadir = gPATH_TO_CLUMPY_HOME + "data/";
   DIR *dir = opendir(clumpydatadir.c_str());
   if (dir) closedir(dir); // Directory exists, all fine.
   else if (ENOENT == errno) {
      printf("\n====> ERROR: get_clumpy_dirs() in misc.cc");
      printf("\n             Could not find CLUMPY's data directory searched for in %s", clumpydatadir.c_str());
      printf("\n             Have you set the environmental variable 'CLUMPY' pointing to CLUMPY's home directory?");
      printf("\n             => abort()\n\n");
      abort();
   } else {
      printf("\n====> ERROR: get_clumpy_dirs() in misc.cc");
      printf("\n             Could not open CLUMPY's data directory %s", clumpydatadir.c_str());
      printf("\n             Do you have the right privileges to access it?");
      printf("\n             => abort()\n\n");
      abort();
   }

}

//______________________________________________________________________________
string get_path(string const &str, bool is_ensure_trailing_dash)
{
   //--- Extracts and returns the absolute path from an environment variable '$env',
   //    or from a string formatted as '$env/relative_path'. If no $ (no path), returns
   //    str as it is.
   //  str               Name from which to extract path
   //  is_ensure_trailing_dash  Useful to ensure '/' at the end of path

   if (str.find_first_of("$") == string::npos)
      return str;

   // Finds how 'str' is formatted (if contains '/')
   size_t last = str.find_first_of("/");
   string path = "";
   if (last == string::npos)
      last = str.size();
   else {
      path = str.substr(last + 1, str.size());
      last -= 1;
   }
   string env_path = str.substr(str.find_first_of("$") + 1, last);
   if ((int)env_path.size() != 0) {
      char *env = getenv(env_path.c_str());
      if (env == NULL) {
         print_warning("misc.cc", "get_path()", "The environment variable " + env_path + " is not defined!");

      } else {
         if (path != "")
            path = (string) env + "/" + path;
         else
            path = (string) env;
      }
   }
   // always make sure that there is a trailing dash:
   if (path[path.size() - 1] != '/' && is_ensure_trailing_dash) path += "/";
   return path;
}

//______________________________________________________________________________
double goldenmin_logstep(double const &ax, double const &bx, double const &cx,
                         void fn(double &, double *, double &), double *par,
                         double const &tol, bool is_min_or_max, double &xmin_or_xmax)
{
   //--- Returns the minimum/maximum x value for which the function fn is min/max,
   //    given a bracketing triplet of abscissas ax, bx, cx (such that bx is between
   //    ax and cx, and f(bx) is less (or more for maximum) than both f(ax) and f(cx)),
   //    this routine performs a golden section search for the minimum (maximum),
   //    isolating it to a fractional precision of about tol. The abscissa of the
   //    minimum is returned as xmin, and the minimum function value is returned
   //    as golden, the returned function value.
   //    N.B.: adapted from Numerical Recipes for log step.
   //
   // INPUTS:
   //  ax            1st point (ax<bx<cx) at which to evaluate fn
   //  bx            2nd point (ax<bx<cx) at which to evaluate fn
   //  cx            3rd point (ax<bx<cx) at which to evaluate fn
   //  fn            Function to use
   //  par           Parameters of fn
   //  tol           Tolerance (relative precision) for which the search stops
   //  is_min_or_max Whether to search for the minimum or the maximum

   double Rgold = 0.61803399;
   double Cgold = 1.0 - Rgold;

   double f1, f2, x0, x1, x2, x3;
   x0 = log10(ax);
   x2 = log10(bx);
   x3 = log10(cx);
   if (fabs(x3 - x2) > fabs(x2 - x0)) {
      //Make x0 to x1 the smaller segment,
      x1 = x2;
      x2 = x2 + Cgold * (x3 - x2);
      //and fill in the new point to be tried.
   } else
      x1 = x2 - Cgold * (x2 - x0);

   // Initial function evaluations. Note that
   // we never need to evaluate the function
   // at the original endpoints.
   double y1 = pow(10., x1);
   double y2 = pow(10., x2);
   fn(y1, par, f1);
   fn(y2, par, f2);
   // If search for maximum, must take opposite sign!
   if (!is_min_or_max) {
      f1 = -f1;
      f2 = -f2;
   }

   while (fabs(x3 - x0) > tol * (fabs(x1) + fabs(x2))) {
      if (f2 < f1) {
         //One possible outcome, and a new function evaluation.
         SHFT3(x0, x1, x2, Rgold * x1 + Cgold * x3);
         double c;
         y2 = pow(10., x2);
         fn(y2, par, c);
         if (!is_min_or_max)
            c = -c;
         SHFT2(f1, f2, c);

      } else {
         //The other outcome, and its new function evaluation.
         SHFT3(x3, x2, x1, Rgold * x2 + Cgold * x0);
         double c;
         y1 = pow(10., x1);
         fn(y1, par, c);
         if (!is_min_or_max)
            c = -c;
         SHFT2(f2, f1, c);
      }
   }

   //Back to see if we are done.
   if (f1 < f2) {
      xmin_or_xmax = pow(10., x1);
      double goldenm = 0.;
      fn(xmin_or_xmax, par, goldenm);
      return goldenm;
   } else {
      xmin_or_xmax = pow(10., x2);
      double goldenm = 0.;
      fn(xmin_or_xmax, par, goldenm);
      return goldenm;
   }
}

//______________________________________________________________________________
double interp1D(double const &x_eval, const vector<double> &x_base,
                const vector<double> &y_base, const int card_interp, bool no_error)
{
   //--- Wrapper function around various interpolation methods for a 1D function y(x).
   // INPUTS:
   //  x_eval        Position at which to interpolate
   //  x_base        x values of base grid
   //  y_base        y values of base grid
   //  card_interp   enumerator for interpolation type.
   //  no_error      if true, returns HPX_BLIND_VALUE = -1.6375e+30 instead of
   //                an error when x_eval falls outside of x_base (default: false).
   // OUTPUT:
   //  y_eval        y(x_eval)

   // check dimensions:
   if (y_base.size() != x_base.size()) {
      printf("\n====> ERROR: interp1D() in misc.cc");
      printf("\n             dimensions of base grids do not match.");
      printf("\n             size(x) = %d, size(y) = %d.",
             int(x_base.size()),  int(y_base.size()));
      printf("\n             => abort()\n\n");
      abort();
   }

   int sign_lo = sign(x_base[0]);
   int sign_up = sign(x_base.back());
   if (x_eval < x_base[0] * (1 - sign_lo * SMALL_NUMBER) or x_eval > x_base[x_base.size() - 1] * (1 + sign_up * SMALL_NUMBER)) {
      if (no_error) return HPX_BLIND_VALUE;
      else {
         printf("\n====> ERROR: interp1D() in misc.cc");
         printf("\n             value x = %.16le to interpolate falls outside interpolation range", x_eval);
         printf("\n             (x in [%.16le, %.16le] required)", x_base[0],  x_base[x_base.size() - 1]);
         printf("\n             => abort()\n\n");
         abort();
      }
   }

   int i_x_lo = binary_search(x_base, x_eval);
   if (i_x_lo == int(x_base.size() - 1)) i_x_lo--;
   int i_x_up = i_x_lo + 1;

   switch (card_interp) {
      case kLINLIN:
         return linlin_interp(x_eval, x_base[i_x_lo], x_base[i_x_up],
                              y_base[i_x_lo], y_base[i_x_up]);
      case kLINLOG:
         return linlog_interp(x_eval, x_base[i_x_lo], x_base[i_x_up],
                              y_base[i_x_lo], y_base[i_x_up]);
      case kLOGLIN:
         return loglin_interp(x_eval, x_base[i_x_lo], x_base[i_x_up],
                              y_base[i_x_lo], y_base[i_x_up]);
      case kLOGLOG:
         return loglog_interp(x_eval, x_base[i_x_lo], x_base[i_x_up],
                              y_base[i_x_lo], y_base[i_x_up]);
      case kSPLINE:
         return spline_interp(x_eval, x_base, y_base);

      default :
         printf("\n====> ERROR: interp1D() in misc.cc");
         printf("\n             card_interp = %d not a valid interpolation type", card_interp);
         printf("\n             => abort()\n\n");
         abort();
   }
   return 0;
}

//______________________________________________________________________________
double interp2D(double const &x_eval, double const &y_eval, const vector<double> &x_base,
                const vector<double> &y_base, const vector< vector<double> > &z_base,
                const int card_interp, bool no_error)
{
   //--- Wrapper function around various interpolation methods for a 2D function z(x,y).
   // INPUTS:
   //  x_eval        x position at which to interpolate
   //  y_eval        y position at which to interpolate
   //  x_base        values of base grid in x direction
   //  y_base        values of base grid in y direction
   //  z_base        values of z(x_base, y_base) on base grid
   //  card_interp   enumerator for interpolation type.
   //  no_error      if true, returns HPX_BLIND_VALUE = -1.6375e+30 instead of
   //                an error when x_eval or y_eval fall outside of x_base or y_base
   //                (default: false).
   // OUTPUT:
   //  z_eval        z(x_eval,y_eval)

   // check dimensions:
   if (z_base.size() != x_base.size() or z_base[0].size() != y_base.size()) {
      printf("\n====> ERROR: interp2D() in misc.cc");
      printf("\n             dimensions of base grids do not match.");
      printf("\n             size(x) = %d, size(y) = %d, size(z) = (%d,%d) from checking first row.",
             int(x_base.size()),  int(y_base.size()), int(z_base.size()), int(z_base[0].size()));
      printf("\n             => abort()\n\n");
      abort();
   }

   int sign_lo = sign(x_base[0]);
   int sign_up = sign(x_base.back());
   if (x_eval < x_base[0] * (1 - sign_lo * SMALL_NUMBER) or x_eval > x_base[x_base.size() - 1] * (1 + sign_up * SMALL_NUMBER)) {
      if (no_error) return HPX_BLIND_VALUE;
      else {
         printf("\n====> ERROR: interp2D() in misc.cc");
         printf("\n             value x = %.16le to interpolate falls outside interpolation range", x_eval);
         printf("\n             (x in [%.16le, %.16le] required)", x_base[0],  x_base[x_base.size() - 1]);
         printf("\n             => abort()\n\n");
         abort();
      }
   }

   sign_lo = sign(y_base[0]);
   sign_up = sign(y_base.back());
   if (y_eval < y_base[0] * (1 - sign_lo * SMALL_NUMBER) or y_eval > y_base[y_base.size() - 1] * (1 + sign_up * SMALL_NUMBER)) {
      if (no_error) return HPX_BLIND_VALUE;
      else {
         printf("\n====> ERROR: interp2D() in misc.cc");
         printf("\n             value y = %.16le to interpolate falls outside interpolation range", y_eval);
         printf("\n             (y in [%.16le, %.16le] required)", y_base[0],  y_base[y_base.size() - 1]);
         printf("\n             => abort()\n\n");
         abort();
      }
   }

   int i_x_lo = binary_search(x_base, x_eval);
   if (i_x_lo == int(x_base.size() - 1)) i_x_lo--;
   int i_x_up = i_x_lo + 1;

   int i_y_lo = binary_search(y_base, y_eval);
   if (i_y_lo == int(y_base.size() - 1)) i_y_lo--;
   int i_y_up = i_y_lo + 1;

   fix_arr<double, 2> x_base_points;
   fix_arr<double, 2> y_base_points;
   fix_arr<double, 4> z_base_points;

   x_base_points[0] = x_base[i_x_lo];
   x_base_points[1] = x_base[i_x_up];
   y_base_points[0] = y_base[i_y_lo];
   y_base_points[1] = y_base[i_y_up];
   z_base_points[0] = z_base[i_x_lo][i_y_lo];
   z_base_points[1] = z_base[i_x_lo][i_y_up];
   z_base_points[2] = z_base[i_x_up][i_y_lo];
   z_base_points[3] = z_base[i_x_up][i_y_up];

   switch (card_interp) {
      case kLINLIN:
         return linlin_interp2D(x_eval, y_eval,
                                x_base_points, y_base_points, z_base_points);
      case kLINLOG:
         return linlog_interp2D(x_eval, y_eval,
                                x_base_points, y_base_points, z_base_points);
      case kLOGLIN:
         return loglin_interp2D(x_eval, y_eval,
                                x_base_points, y_base_points, z_base_points);
      case kLOGLOG:
         return loglog_interp2D(x_eval, y_eval,
                                x_base_points, y_base_points, z_base_points);
      default :
         printf("\n====> ERROR: interp2D() in misc.cc");
         printf("\n             card_interp = %d not a valid interpolation type", card_interp);
         printf("\n             => abort()\n\n");
         abort();
   }
   return 0;
}

//______________________________________________________________________________
bool isNotASCII(const char s)
{
   //--- Returns true if a character is not ASCII, false otherwise.
   //    Includes the tab symbol also as ASCII character
   if (s == '\t') return false;
   else return isprint(s) == 0;
}

//______________________________________________________________________________
bool isNumeric(const char *s)
{
   //--- Returns true if an input char is numeric, false otherwise.
   //    Taken from https://rosettacode.org

   if (s == NULL || *s == '\0' || isspace(*s))
      return 0;
   char *p;
   strtod(s, &p);
   int a =  *p == '\0';
   if (a == 1) return true;
   else if (a == 0) return false;
   else {
      char tmp[256];
      sprintf(tmp, "%d", a);
      print_error("misc.cc", "isNumeric()", "Should return zero or one. Returns" + string(tmp));
   }
   return false;
}

//______________________________________________________________________________
double linlin_interp(double const &x_eval, double const &x1, double const &x2,
                     double const &y1, double const &y2)
{
   //--- Returns lin-lin simple interpolation:
   //       y_interp = y1 + (x-x1)*(y2-y1)/(x2-x1).
   //    N.B.: linear extrapolation for x_eval outside range. An error is drawn
   //          when using interp1D, but not when directly calling this routine.
   //
   //  x_eval        Position at which to interpolate
   //  x1            Lower position for which y is known
   //  x2            Upper position for which y is known
   //  y1            Value of y(x) at x1
   //  y2            Value of y(x) at x2

   double tmp = (x_eval - x1) / (x2 - x1);
   if (fabs(tmp) < SMALL_NUMBER) return y1;
   else if (fabs(1 - tmp) < SMALL_NUMBER)  return y2;
   else return y1 + tmp * (y2 - y1);
}

//______________________________________________________________________________
double linlog_interp(double const &x_eval, double const &x1, double const &x2,
                     double const &y1, double const &y2)
{
   //--- Returns lin-log simple interpolation:
   //       y_interp = y1 * (y2/y1)^[(x-x1)/(x2-x1)].
   //    N.B.: linear extrapolation for x_eval outside range. An error is drawn
   //          when using interp1D, but not when directly calling this routine.
   //
   //  x_eval        Position at which to interpolate
   //  x1            Lower position for which y is known
   //  x2            Upper position for which y is known
   //  y1            Value of y(x) at x1
   //  y2            Value of y(x) at x2

   return y1 * pow(y2 / y1, (x_eval - x1) / (x2 - x1));
}

//______________________________________________________________________________
double loglin_interp(double const &x_eval, double const &x1, double const &x2,
                     double const &y1, double const &y2)
{
   //--- Returns log-lin simple interpolation (x=log, y=lin):
   //       y_interp = y1 + (y2-y1)*log(x/x1)/log(x2/x1).
   //    N.B.: linear extrapolation for x_eval outside range. An error is drawn
   //          when using interp1D, but not when directly calling this routine.
   //
   //  x_eval        Position at which to interpolate
   //  x1            Lower position for which y is known
   //  x2            Upper position for which y is known
   //  y1            Value of y(x) at x1
   //  y2            Value of y(x) at x2

   return  y1 + log(x_eval / x1) * (y2 - y1) / log(x2 / x1);
}

//______________________________________________________________________________
double loglog_interp(double const &x_eval, double const &x1, double const &x2,
                     double const &y1, double const &y2)
{
   //--- Returns log-log simple interpolation:
   //       y_interp = y1 * (x/x1)^(log(y2/y1)/log(x2/x1)).
   //
   //  x_eval        Position at which to interpolate
   //  x1            Lower position for which y is known
   //  x2            Upper position for which y is known
   //  y1            Value of y(x) at x1
   //  y2            Value of y(x) at x2

   return ((y1 <= 0. || y2 <= 0.) ? 0. :
           y1 * pow(x_eval / x1, log(y2 / y1) / log(x2 / x1)));
}

//______________________________________________________________________________
double spline_interp(double const &x_eval, const vector<double> &x_base, const vector<double> &y_base)
{
   // returns the value y_eval(x_eval) on a grid (x_base, y_base) via gsl spline interpolation.

   gsl_interp_accel *acc = gsl_interp_accel_alloc();
   gsl_spline *spline = gsl_spline_alloc(gsl_interp_cspline, x_base.size());

   const double *x_arr = &x_base[0];
   const double *y_arr = &y_base[0];
   gsl_spline_init(spline, x_arr, y_arr, x_base.size());
   double result = gsl_spline_eval(spline, x_eval, acc);

   gsl_spline_free(spline);
   gsl_interp_accel_free(acc);
   return result;
}


//______________________________________________________________________________
double linlin_interp2D(double const &x_eval, double const &y_eval,
                       fix_arr<double, 2> &x_base, fix_arr<double, 2> &y_base, fix_arr<double, 4> &z_base)
{
   //--- Returns simple 2D bilinear interpolation for f(x,y) on a 2D grid

   // Input:
   //  x_eval        x position at which to interpolate
   //  y_eval        y position at which to interpolate
   //  x_base        array holding x1, x2.
   //  y_base        array holding y1, y2.
   //  z_base        array holding z(x1,y1), z(x1,y2), z(x2,y1), z(x2,y2)
   // Output:
   //  f(x,y)           interpolated value at (x,y)

   //      f_int = Sum wgt_i * f_i, with f_i the four base points f11, f12, f21, f22 and
   //      wgt[0] = (x2 - x)  * (y2 - y) / (  (x2 -x1) * (y2 -y1) )
   //      wgt[1] = (x2 - x)  * (y - y1) / (  (x2 -x1) * (y2 -y1) )
   //      wgt[2] = (x - x1)  * (y2 - y) / (  (x2 -x1) * (y2 -y1) )
   //      wgt[3] = (x - x1)  * (y - y1) / (  (x2 -x1) * (y2 -y1) )
   //        -> Sum wgt_i = 1 ;

   fix_arr<double, 4> fac;
   fix_arr<double, 4> wgt;

   double norm = (x_base[1] - x_base[0]) * (y_base[1] - y_base[0]);

   fac[0] = x_base[1] - x_eval;
   fac[1] = x_eval    - x_base[0];
   fac[2] = y_base[1] - y_eval;
   fac[3] = y_eval    - y_base[0];

   wgt[0] = fac[0] * fac[2];
   wgt[1] = fac[0] * fac[3];
   wgt[2] = fac[1] * fac[2];
   wgt[3] = fac[1] * fac[3];

   return (z_base[0] * wgt[0] + z_base[1] * wgt[1]
           + z_base[2] * wgt[2] + z_base[3] * wgt[3]) / norm;
}

//______________________________________________________________________________
double linlog_interp2D(double const &x_eval, double const &y_eval,
                       fix_arr<double, 2> &x_base, fix_arr<double, 2> &y_base, fix_arr<double, 4> &z_base)
{
   //--- Returns simple 2D bilinear-log interpolation for f(x,y) on a 2D grid

   // Input:
   //  x_eval        x position at which to interpolate
   //  y_eval        y position at which to interpolate
   //  x_base        array holding x1, x2.
   //  y_base        array holding y1, y2.
   //  z_base        array holding z(x1,y1), z(x1,y2), z(x2,y1), z(x2,y2)
   // Output:
   //  f(x,y)           interpolated value at (x,y)

   //      f_int = Sum wgt_i * f_i, with f_i the four base points f11, f12, f21, f22 and
   //      wgt[0] = (x2 - x)  * (y2 - y) / (  (x2 -x1) * (y2 -y1) )
   //      wgt[1] = (x2 - x)  * (y - y1) / (  (x2 -x1) * (y2 -y1) )
   //      wgt[2] = (x - x1)  * (y2 - y) / (  (x2 -x1) * (y2 -y1) )
   //      wgt[3] = (x - x1)  * (y - y1) / (  (x2 -x1) * (y2 -y1) )
   //        -> Sum wgt_i = 1 ;
   //    Now, log(f_int) = Sum wgt_i * log(f_i), and one obtains the expression:

   fix_arr<double, 4> fac;
   fix_arr<double, 4> wgt;

   double norm = (x_base[1] - x_base[0]) * (y_base[1] - y_base[0]);

   fac[0] = x_base[1] - x_eval;
   fac[1] = x_eval    - x_base[0];
   fac[2] = y_base[1] - y_eval;
   fac[3] = y_eval    - y_base[0];

   wgt[0] = fac[0] * fac[2] / norm;
   wgt[1] = fac[0] * fac[3] / norm;
   wgt[2] = fac[1] * fac[2] / norm;
   wgt[3] = fac[1] * fac[3] / norm;

   return pow(z_base[0], wgt[0]) * pow(z_base[1], wgt[1])
          * pow(z_base[2], wgt[2]) * pow(z_base[3], wgt[3]);
}

//______________________________________________________________________________
double loglin_interp2D(double const &x_eval, double const &y_eval,
                       fix_arr<double, 2> &x_base, fix_arr<double, 2> &y_base, fix_arr<double, 4> &z_base)
{
   //--- Returns simple 2D bilog-lin interpolation for f(x,y) on a 2D grid

   // Input:
   //  x_eval        x position at which to interpolate
   //  y_eval        y position at which to interpolate
   //  x_base        array holding x1, x2.
   //  y_base        array holding y1, y2.
   //  z_base        array holding z(x1,y1), z(x1,y2), z(x2,y1), z(x2,y2)
   // Output:
   //  f(x,y)           interpolated value at (x,y)

   fix_arr<double, 4> fac;
   fix_arr<double, 4> wgt;

   double norm = log(x_base[1] / x_base[0]) * log(y_base[1] / y_base[0]);

   fac[0] = log(x_base[1] / x_eval);
   fac[1] = log(x_eval    / x_base[0]);
   fac[2] = log(y_base[1] / y_eval);
   fac[3] = log(y_eval    / y_base[0]);

   wgt[0] = fac[0] * fac[2];
   wgt[1] = fac[0] * fac[3];
   wgt[2] = fac[1] * fac[2];
   wgt[3] = fac[1] * fac[3];

   return (z_base[0] * wgt[0] + z_base[1] * wgt[1]
           + z_base[2] * wgt[2] + z_base[3] * wgt[3]) / norm;
}

//______________________________________________________________________________
double loglog_interp2D(double const &x_eval, double const &y_eval,
                       fix_arr<double, 2> &x_base, fix_arr<double, 2> &y_base, fix_arr<double, 4> &z_base)
{
   //--- Returns simple 2D bilog-log interpolation for f(x,y) on a 2D grid

   // Input:
   //  x_eval        x position at which to interpolate
   //  y_eval        y position at which to interpolate
   //  x_base        array holding x1, x2.
   //  y_base        array holding y1, y2.
   //  z_base        array holding z(x1,y1), z(x1,y2), z(x2,y1), z(x2,y2)
   // Output:
   //  f(x,y)           interpolated value at (x,y)

   fix_arr<double, 4> fac;
   fix_arr<double, 4> wgt;

   double norm = log(x_base[1] / x_base[0]) * log(y_base[1] / y_base[0]);

   fac[0] = log(x_base[1] / x_eval);
   fac[1] = log(x_eval    / x_base[0]);
   fac[2] = log(y_base[1] / y_eval);
   fac[3] = log(y_eval    / y_base[0]);

   wgt[0] = fac[0] * fac[2] / norm;
   wgt[1] = fac[0] * fac[3] / norm;
   wgt[2] = fac[1] * fac[2] / norm;
   wgt[3] = fac[1] * fac[3] / norm;

   return pow(z_base[0], wgt[0]) * pow(z_base[1], wgt[1])
          * pow(z_base[2], wgt[2]) * pow(z_base[3], wgt[3]);
}

//______________________________________________________________________________
vector <double> make_1D_grid(double &x_min, const double &x_max, const int n_x, const bool is_x_log)
{

   vector <double> x;
   int i_start = 1;
   if (!is_x_log) {
      x.push_back(x_min);
      double step = (x_max - x_min) / (double)(n_x - 1);
      for (int k = i_start; k < n_x - 1; ++k) x.push_back(x[k - 1] + step);
   } else {
      if (x_min == 0.) x_min = 1.e-5;
      x.push_back(x_min);
      double step = pow(10., log10(x_max / x_min) / (double)(n_x - 1));
      for (int k = i_start; k < n_x - 1; ++k) x.push_back(x[k - 1]*step);
   }
   x.push_back(x_max);

   return x;
}

//______________________________________________________________________________
bool makedir(const char *dir)
{
   //--- Creates dir, a relative or absolute directory or directory tree. Returns
   //    true if a directory had to be created, returns false if all directories
   //    in the tree have already existed.
   //
   //  dir           Relative or absolute (starting with '/') dir or dir tree.

   struct stat sb;
   bool is_dir = true;
   int is_dir_run;
   char tmp[256];
   char *p = NULL;
   size_t len;

   // check if output dir already exists:
   if (stat(dir, &sb) == 0 && S_ISDIR(sb.st_mode)) return is_dir;

   snprintf(tmp, sizeof(tmp), "%s", dir);
   len = strlen(tmp);
   if (tmp[len - 1] == '/')
      tmp[len - 1] = 0;
   for (p = tmp + 1; *p; p++)
      if (*p == '/') {
         *p = 0;
         is_dir_run = mkdir(tmp, 0755);
         *p = '/';
         if (is_dir_run == 0) is_dir = false ;
      }
   is_dir_run = mkdir(tmp, 0755);
   if (is_dir_run == 0) is_dir = false ;

   // check if dir has been successfully created:
   if (stat(dir, &sb) == 0 && S_ISDIR(sb.st_mode)) {
      cout << "\n====> Created output directory " << dir <<  endl;
   } else {
      printf("\n====> ERROR: makedir() in misc.cc");
      printf("\n             Output directory %s could not be created.", dir);
      printf("\n             Check your permissions or available disk space.");
      printf("\n             => abort()\n\n");
      abort();
   }
   return is_dir;
}

//______________________________________________________________________________
int name_to_indexinlist(string const &name, vector<string> const &names)
{
   //--- Returns the index of name in the list of names (returns -1 if not in the list).
   //
   //  name          String to find in list
   //  names         List of names

   string name_uc = upper_case(name);
   for (int i = 0; i < (int)names.size(); ++i) {
      if (name_uc == upper_case(names[i]))
         return i;
   }
   return -1;
}


#if IS_ROOT
//______________________________________________________________________________
TText *orphanCLUMPYadonplots()
{
   string text = "CLUMPY " + gCLUMPY_VERSION + " (http://lpsc.in2p3.fr/clumpy)";
   TText *clumpy_txt = new TText(0.99, 0.965, text.c_str());
   clumpy_txt->SetNDC(kTRUE);
   clumpy_txt->SetTextFont(92);
   clumpy_txt->SetTextAlign(31);
   clumpy_txt->SetTextSize(0.035);
   return clumpy_txt;
}
#endif

//______________________________________________________________________________
void print_error(string library, string function, string message, bool is_abort)
{
   //--- Prints an error message in red color on screen and aborts the program.
   //    N.B. The strings are by purpose not called by reference to allow to write the
   //         the messages directly into the function in the code.
   //
   //  library  Name of the file in which the error message is called.
   //  function Name of the function in which the error message is called.
   //  message  Error message.

   string message_wrapped = string_wrap(message, 74, 6);
   printf(COLOR_BRED "\n====> ERROR: %s in %s\n", function.c_str(), library.c_str());
   printf("%s", message_wrapped.c_str());

   if (is_abort) {
      printf("\n      => abort()\n\n" COLOR_RESET);
      abort();
   } else {
      printf("" COLOR_RESET);
   }
}

//______________________________________________________________________________
void print_warning(string library, string function, string message)
{
   //--- Prints a warning message in yellow color on screen.
   //    N.B. The strings are by purpose not called by reference to allow to write the
   //         the messages directly into the function in the code.
   //
   //  library  Name of the file in which the warning message is called.
   //  function Name of the function in which the warning message is called.
   //  message  warning message.

   string message_wrapped = string_wrap(message, 74, 6);
   printf(COLOR_BYELLOW "\n====> WARNING: %s in %s\n", function.c_str(), library.c_str());
   printf("%s\n\n" COLOR_RESET, message_wrapped.c_str());
   if (!gSIM_IS_TEST) usleep(1e6);
}

//______________________________________________________________________________
vector< vector<double> >read_ascii(string const &filename, int const ncolumns,
                                   bool const is_monotonous, int const column_start)
{
   //--- Reads a ASCII file into a vector of vectors. It requires that
   //    all columns have the same length.
   //
   //  filename      Filename (including path) of file
   //  ncolumns      Number of columns to be read (optional, or set to -1 to read all columns).
   //  is_monotonous Stops reading if a number in the first column is smaller or equal than the previous number in the column (optional).
   //  column_start  Index of column where to start >= 0 (optional).

   string filename_clean = removeblanks_from_startstop(filename);
   resolve_envvar(filename_clean);

   ifstream ifile(filename_clean.c_str());

   if (!ifile) {
      ifile.close();
      printf("\n====> ERROR: read_ascii() in misc.cc");
      printf("\n             File %s does not exist", filename_clean.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }
   string line = "";
   string strnum = "";

   vector< vector<double> > res;

   // parse line by line

   int i_row = 0;
   while (getline(ifile, line)) {
      // remove non-ascii characters
      line.erase(remove_if(line.begin(), line.end(), isNotASCII), line.end());
      // remove leading or trailing spaces
      line = removeblanks_from_startstop(line);
      vector<double> row;

      if (line[0] == '#' || line.empty()) continue;
      int i_col = 0;
      bool is_break = false;

      for (string::const_iterator j = line.begin(); j != line.end(); ++j) {
         // If i is not a delim, then append it to strnum
         const string delim = " \t";
         if (delim.find(*j) == string::npos) {
            strnum += *j;
            if (j + 1 != line.end()) // If it's the last char, do not continue
               continue;
         }
         // if strnum is still empty, it means the previous char is also a
         // delim (several delims appear together). Ignore this char.
         if (strnum.empty())
            continue;

         // If we reach here, we got a number. Convert it to double.
         double number = 0.;
         istringstream(strnum) >> number;

         if (i_col == 0 and is_monotonous) {
            // only push back if values are increasing
            if (i_row == 0 || number > res[i_row - 1][0]) {
               row.push_back(number);
            } else {
               is_break = true;
               break;
            }
         } else {
            if (ncolumns == -1 or i_col < ncolumns) {
               if (i_col >= column_start) row.push_back(number);
            }
         }

         strnum.clear();
         ++i_col;
      }
      if (is_break) break;

      res.push_back(row);
      ++i_row;
   }
   ifile.close();

   return res;
}

//______________________________________________________________________________
string removeblanks_from_startstop(string const &str)
{
   //--- Removes blancks (or tabs) at the beginning and end of a string.
   //    N.B.: as long as the Healpix C++ library is linked to Clumpy, with
   //    #include <string_utils.h>, one can also use trim(const std::string &orig)).
   //
   //  str           String to trim

   if (str.size() == 0) return str;

   // Trim tabs
   string test_tab(str.size(), '\t');
   if (str == test_tab) return "";
   std::string::size_type begin = str.find_first_not_of("\t");
   std::string::size_type end   = str.find_last_not_of("\t");
   string strbis = str.substr(begin, end - begin + 1);

   // Trim leading and trailing spaces
   string test_spaces(str.size(), ' ');
   if (strbis == test_spaces) return "";
   begin = strbis.find_first_not_of(" ");
   end   = strbis.find_last_not_of(" ");
   string strtres = strbis.substr(begin, end - begin + 1);

   // Trim leading and trailing newlines
   string test_newlines(str.size(), '\n');
   if (strtres == test_newlines) return "";
   begin = strtres.find_first_not_of("\n");
   end   = strtres.find_last_not_of("\n");
   return strtres.substr(begin, end - begin + 1);
}

//______________________________________________________________________________
string removeslashes_from_end(string const &str)
{
   //--- Removes slashes at the end of a string.
   //    N.B.: as long as the Healpix C++ library is linked to Clumpy, with
   //    #include <string_utils.h>, one can also use trim(const std::string &orig)).
   //
   //  str           String to trim

   // Trim tabs
   if (str.size() == 0) return str;

   std::string::size_type end   = str.find_last_not_of("\\/");
   string strbis = str.substr(0, end + 1);

   // Trim leading and trailing spaces
   end   = strbis.find_last_not_of("\\/");
   return strbis.substr(0, end + 1);
}

//______________________________________________________________________________
string removechars(string const &str, string const &delimiters)
{
   //--- Drops chars in a string.
   //
   //  str           String to analyse
   //  delimiters    Characters to drop in str

   string tmp = str;
   size_t found = tmp.find_first_of(delimiters);
   while (found != string::npos) {
      tmp.erase(found, 1);
      found = tmp.find_first_of(delimiters, found);
   };
   return tmp;
}


//______________________________________________________________________________
void replace_substring(string &s, const string &search, const string &replace)
{
   //--- Replaces substring search in string s by string replace.
   //    From Stackoverflow, http://stackoverflow.com/questions/4643512/replace-substring-with-another-substring-c

   for (size_t pos = 0; ; pos += replace.length()) {
      // Locate the substring to replace
      pos = s.find(search, pos);
      if (pos == string::npos) break;
      // Replace by erasing and inserting
      s.erase(pos, search.length());
      s.insert(pos, replace);
   }
}


//______________________________________________________________________________
void resolve_envvar(string &str)
{
   //--- Finds an environmental variable in a string
   //    and replaces it with the variable value
   //
   //  str           String to analyse

   string tmp = str;

   string envvar;
   while (tmp.find_first_of("$") != string::npos) {
      string envvar_tmp = tmp.substr(tmp.find_first_of("$"), tmp.size());
      string envvar;
      string envvar_dollar;
      if (envvar_tmp[1] == '{') {
         envvar = envvar_tmp.substr(2, envvar_tmp.find_first_of("}") - 2);
         envvar_dollar = "${" + envvar + "}";
      } else {
         envvar = envvar_tmp.substr(1, envvar_tmp.find_first_of("/") - 1);
         envvar_dollar = "$" + envvar;
      }
      if (getenv(envvar.c_str()) == NULL) {
         printf("\n====> ERROR: resolve_envvar() in misc.cc");
         printf("\n             Environmental variable '%s' not set.", envvar.c_str());
         printf("\n             Set it by typing 'export %s=you_have_to_decide_this'", envvar.c_str());
         printf("\n             => abort()\n\n");
         abort();
      }
      string envvar_resolved = string(getenv(envvar.c_str()));
      replace_substring(tmp, envvar_dollar, envvar_resolved);
   }
   str = tmp;
}

//______________________________________________________________________________
void root_trimname4object(string &s)
{
   //--- Discards  special caracter "{}*~#$ -:?!,;/+=[]()." and replaces
   //    '(' and ')' by '_' in s to be ROOT compliant, i.e., so that the
   //    trimmed string can be used as a root object name.
   //
   // INPUT/OUTPUT
   //  s             String to trim

   // Trim string
   string trim = "{}*~#$ -:?!,;/+=[].";
   s = removechars(s, trim);

   // Replace with '_'
   for (int i = 0; i < (int)s.size(); ++i) {
      if (s[i] == '(' || s[i] == ')')
         s[i] = '_';
   }
}

#if IS_ROOT
//______________________________________________________________________________
int rootcolor(int i)
{
   //--- Converts index into better ROOT color index.
   //
   //  i             Index to convert

   if (i > 13) i = (i % 14);
   switch (i) {
      case 0 :
         return kBlack;
      case 1 :
         return kRed;
      case 2 :
         return kBlue;
      case 3 :
         return kGreen + 1;
      case 4 :
         return kOrange + 1;
      case 5 :
         return kViolet;
      case 6 :
         return kMagenta - 7;
      case 7 :
         return kGray + 1;
      case 8 :
         return kRed + 2;
      case 9 :
         return kCyan + 1;
      case 10 :
         return kSpring;
      case 11 :
         return kPink - 4;
      case 12 :
         return kAzure + 4;
      case 13 :
         return kYellow + 1;
      default :
         return i;
   }
}

//______________________________________________________________________________
int rootmarker(int i)
{
   //--- Converts index into better ROOT marker index.
   //
   //  i             Index to convert

   if (i > 15) i = (i % 16) + 1;
   switch (i) {
      case 0 :
         return 2;
      case 1 :
         return 5;
      case 2 :
         return 24;
      case 3 :
         return 25;
      case 4 :
         return 26;
      case 5 :
         return 32;
      case 6 :
         return 30;
      case 7 :
         return 28;
      case 8 :
         return 27;
      case 9  :
         return 20;
      case 10 :
         return 21;
      case 11 :
         return 22;
      case 12 :
         return 23;
      case 13 :
         return 29;
      case 14 :
         return 33;
      case 15 :
         return 34;
      default :
         return 1;
   }
}

//______________________________________________________________________________
void rootstyle_set2CLUMPY()
{
   //--- Sets style for plots in CLUMPY (ROOT style). Note that when called,
   //    this function leads to a memory leak (but it is unimportant, as it
   //    is only at the display stage).

   TStyle *style = new  TStyle("style", "style");

   style->SetFillColor(0);
   style->SetFillStyle(0);

   // Canvas style
   style->SetCanvasBorderMode(0);
   style->SetCanvasBorderSize(0);
   style->SetCanvasColor(0);
   style->SetCanvasDefH(500);
   style->SetCanvasDefW(650);
   style->SetCanvasDefX(10);
   style->SetCanvasDefY(10);

   // Frame style (no color framed around plots)
   style->SetFrameBorderMode(0);
   style->SetFrameBorderSize(0);
   style->SetFrameFillColor(0);
   style->SetFrameFillStyle(0);
   style->SetFrameLineColor(0);
   style->SetFrameLineStyle(0);
   style->SetFrameLineWidth(1);

   // Pad style
   style->SetPadBorderMode(0);
   style->SetPadBorderSize(0);
   style->SetPadColor(0);
   style->SetPadGridX(0);
   style->SetPadGridY(0);
   style->SetPadTickX(1);
   style->SetPadTickY(1);
   style->SetPadTopMargin(0.05);
   style->SetPadRightMargin(0.02);
   style->SetPadLeftMargin(0.12);
   style->SetPadBottomMargin(0.12);

   //use the primary color palette
   style->SetPalette(1, 0);
   //set stat off
   style->SetOptLogx(1);
   style->SetOptLogy(1);
   style->SetOptLogz(1);
   style->SetOptStat(0);
   style->SetOptFit(0);

   //set histogram default
   style->SetHistTopMargin(0.5);
   style->SetHistFillColor(0);
   style->SetHistFillStyle(0);
   style->SetHistLineColor(kBlack);
   style->SetHistLineStyle(1);
   style->SetHistLineWidth(2);
   //set function default
   style->SetFuncColor(kRed);
   style->SetFuncStyle(1);
   style->SetFuncWidth(4);

   //set label, legend, and title default
   style->SetLabelColor(kBlack, "XYZ");
   style->SetLabelFont(132, "XYZ");
   style->SetLabelOffset(0.004, "X");
   style->SetLabelOffset(0.004, "Y");
   style->SetLabelSize(0.045, "XYZ");
   style->SetLegendBorderSize(0);
   style->SetLegendFillColor(0);
   style->SetLegendFont(132);
   style->SetTitleAlign(22);
   style->SetTitleBorderSize(0);
   style->SetTitleColor(kBlack, "XYZ");
   style->SetTitleFillColor(0);
   style->SetTitleFont(132, "XYZ");
   style->SetTitleSize(0.05, "XYZ");
   style->SetTitleFontSize(0.06);
   style->SetTitleOffset(1.1, "X");
   style->SetTitleOffset(1.1, "Y");
   style->SetTitleOffset(1, "Z");
   style->SetTitleStyle(0);
   style->SetTitleTextColor(kBlack);


   //set the number of divisions to show
   style->SetNdivisions(512, "xy");

   style->cd();
   gStyle = style;
   style = NULL;
}
#endif

//______________________________________________________________________________
double rootsolver_brent(double fn(double &, double *), double par[], double x1, double x2, double tol, bool is_softfail)
{
   //--- Using Brent's method, find the root F(x) = 0 of a function F(x) known to
   //    lie between x1 and x2. The root, returned as rootsolver_brent, will be
   //    refined until its accuracy is tol.
   //    N.B.: adapted from Numerical Recipes' zbrent for log step.
   //
   //  fn            Function to use
   //  par[]         Parameters of fn
   //  x1            Lower value of interval
   //  x2            Upper value of interval
   //  tol           Tolerance (relative precision) for which the search stops
   //  is_softfail   Optional: if enabled, continue without aborts and set result to -999

   const int ITMAX = MAX_ITER; // Maximum allowed number of iterations.
   const double eps = 3.0e-8; //Machine floating - point precision.

   int iter;
   double a = x1, b = x2, c = x2, d = 0., e = 0., min1, min2;
   double fa = fn(a, par);
   double fb = fn(b, par);
   double fc, p, q = 0., r, s;
   if ((fa > 0.0 && fb > 0.0) || (fa < 0.0 && fb < 0.0)) {
      printf("\n====> ERROR: rootsolver_brent() in misc.cc");
      printf("\n             Root (zero) looked for must be bracketed.");
      if (is_softfail) {
         printf("\n             Soft fail enabled => continue and set output result to -999.\n\n");
         return -999;
      } else {
         printf("\n             => abort()\n\n");
         abort();
      }
   }
   fc = fb;
   for (iter = 1; iter <= ITMAX; iter++) {
      if ((fb > 0.0 && fc > 0.0) || (fb < 0.0 && fc < 0.0)) {
         // Rename a, b, c and adjust bounding interval d
         c = a;
         fc = fa;
         e = d = b - a;
      }
      if (fabs(fc) < fabs(fb)) {
         a = b;
         b = c;
         c = a;
         fa = fb;
         fb = fc;
         fc = fa;
      }
      double tol1 = 2.0 * eps * fabs(b) + 0.5 * tol;
      // Convergence check.
      double xm = 0.5 * (c - b);
      if (fabs(xm) <= tol1 || fb == 0.0)
         return b;
      if (fabs(e) >= tol1 && fabs(fa) > fabs(fb)) {
         // Attempt inverse quadratic interpolation
         s = fb / fa;
         if (a == c) {
            p = 2.0 * xm * s;
            q = 1.0 - s;
         } else {
            q = fa / fc;
            r = fb / fc;
            p = s * (2.0 * xm * q * (q - r) - (b - a) * (r - 1.0));
            q = (q - 1.0) * (r - 1.0) * (s - 1.0);
         }
         // Check whether in bounds
         if (p > 0.0) q = -q;
         p = fabs(p);
         min1 = 3.0 * xm * q - fabs(tol1 * q);
         min2 = fabs(e * q);
         if (2.0 * p < (min1 < min2 ? min1 : min2)) {
            // Accept interpolation
            e = d;
            d = p / q;
         } else {
            // Interpolation failed, use bisection
            d = xm;
            e = d;
         }
      } else {
         // Bounds decreasing too slowly, use bisection
         d = xm;
         e = d;
      }
      // Move last best guess to a
      a = b;
      fa = fb;
      if (fabs(d) > tol1)
         b += d;
      else
         b += SIGN(tol1, xm);
      // Evaluate new trial root
      fb = fn(b, par);
   }

   printf("\n====> ERROR: rootsolver_brent() in misc.cc");
   printf("\n             Maximum number of iterations (%d) exceeded.", ITMAX);
   if (is_softfail) {
      printf("\n             Soft fail enabled => continue and set output result to -999.\n\n");
      return -999;
   } else {
      printf("\n             => abort()\n\n");
      abort();
   }

   // Never get here.
   return 0.0;
}

//______________________________________________________________________________
double rootsolver_gsl(const gsl_root_fsolver_type *T, gsl_function Fx, double &x_lo, double &x_hi, double &precision, int &return_status)
{
   //--- Wrapper function to the find the root F(x) = 0 between the boundaries x_lo and x_hi
   //    using a GSL solver.
   // INPUTS:
   //  gsl_root_fsolver_type  Solver type. Possible choices:
   //                          - gsl_root_fsolver_bisection
   //                          - gsl_root_fsolver_brent
   //                          - gsl_root_fsolver_falsepos
   //  Fx                     GSL function F(x,params).
   //  x_lo                   Root is searched for x > x_lo.
   //  x_hi                   Root is searched for x < x_hi
   //                         Whether endpoints are included depends on the search method.
   //  precision              Result is returned when abs(x_hi-x_low)/x_lo <  precision.
   //  return_status          reurn status code if initially equal to one
   // OUTPUT:
   //  x0                     Value F(x0) = 0.
   //  return_status          set to status code if initially equal to one
   //
   //  See also www.gnu.org/software/gsl/manual/html_node/One-dimensional-Root_002dFinding.html

   if (return_status == 1)
      gsl_set_error_handler_off();

   gsl_root_fsolver *s;
   double res;
   s = gsl_root_fsolver_alloc(T);
   int status;
   int iter = 0, max_iter = MAX_ITER;
   status = gsl_root_fsolver_set(s, &Fx, x_lo, x_hi);
   if (return_status == 1 and status > 0) {
      return_status = status;
      return 0;
   }

   do {
      iter++;
      status = gsl_root_fsolver_iterate(s);
      if (return_status == 1 and status > 0) {
         return_status = status;
         return 0;
      }
      res = gsl_root_fsolver_root(s);

      x_lo = gsl_root_fsolver_x_lower(s);
      x_hi = gsl_root_fsolver_x_upper(s);
      status = gsl_root_test_interval(x_lo, x_hi, 0, precision);
      if (return_status == 1 and status > 0) {
         return_status = status;
         return 0;
      }

      if (status == GSL_SUCCESS and iter <= max_iter) {
         //printf("Converged after %d iterations\n", iter);
         break;
      } else if (iter == max_iter) {
         printf("\n====> ERROR: rootsolver_gsl() in misc.cc");
         printf("\n             Too many steps (not converged)!");
         printf("\n             => abort()\n\n");
         abort();
      }
   } while (status == GSL_CONTINUE && iter < max_iter);

   gsl_root_fsolver_free(s);
   return_status = 0;
   return res;
}

//______________________________________________________________________________
double round(const double &x, const int digits)
{
   //--- Overloads std function round() to be able to round a float
   //    to "digits" digits after the decimal point.

   double fac = pow(10, digits);
   return round(fac * x) / fac;
}

//______________________________________________________________________________
double sigmoid_window(double const &x, double const &x0, double const &sigma)
{
   //--- Function describing a log-sigmoid curve around x0
   //    f(x0) = 0.5, f(x-> -inf) -> 0, f(x-> +inf) -> 1.
   //    For sigma -> 0, this function approaches a Heaviside step-function.

   // INPUTS:
   //  x         value to evaluate
   //  x0        value where f(x=x0) = 0.5
   //  sigma     steepness of the transition between zero and one.
   // OUTPUT:
   //  f(x)          function value at x
   if (sigma < 0) {
      printf("\n====> ERROR: sigmoid_window() in misc.cc");
      printf("\n             sigma > 0 required");
      printf("\n             => abort()\n\n");
      abort();
   }

   if (x * x0 <= 0) {
      printf("\n====> ERROR: sigmoid_window() in misc.cc");
      printf("\n             sign(x) = sign(x0) and x,x0 != 0 required");
      printf("\n             => abort()\n\n");
      abort();
   }

   return 1. / (1 + pow((x / x0), -1 / sigma));
}

//______________________________________________________________________________
void string2list(string const &str, string const &delimiters, vector<string> &list)
{
   //--- Seeks and extracts the number of Elements in a list separated by any
   //    separator. This extraction checks possible mistakes in the input, e.g.
   //    if separator=',', it is able to extract correctly from str=",,ddf,dd,,ee,"
   //    the list={"ddf","dd","ee"}.
   //    Discards all elements in the string after a # symbol!
   //
   // INPUTS:
   //  str           String to analyse
   //  delimiters    Separator searched for
   // OUTPUT:
   //  list          Vector of string found in str

   // cut off string after #
   string str_cropped = str.substr(0, str.find_first_of("#"));

   // Skip delimiters at beginning.
   string::size_type lastPos = str_cropped.find_first_not_of(delimiters, 0);
   // Find first "non-delimiter".
   string::size_type pos     = str_cropped.find_first_of(delimiters, lastPos);
   while (string::npos != pos || string::npos != lastPos) {
      // Found a token, add it to the vector.
      string sub = str_cropped.substr(lastPos, pos - lastPos);
      if (sub == "" || sub.find_first_not_of(" ") == string::npos)
         list.push_back("");
      else {
         string entry = removeblanks_from_startstop(sub);
         list.push_back(entry);
      }
      // Skip delimiters.
      lastPos = str_cropped.find_first_not_of(delimiters, pos);
      // Find next "non-delimiter"
      pos = str_cropped.find_first_of(delimiters, lastPos);
   }
}

//______________________________________________________________________________
void string2list(string const &str, string const &delimiters, vector<float> &list)
{
   //--- Extracts a list of float from a string.
   //
   // INPUTS:
   //  str           String to analyse
   //  delimiters    Separator searched for
   // OUTPUT:
   //  list          Vector of float found in str

   vector<string> tokens;
   string2list(str, delimiters, tokens);

   // Convert in Float_t
   for (int i = 0; i < (int)tokens.size(); ++i)
      list.push_back(atof(tokens[i].c_str()));
}

//______________________________________________________________________________
void string2list(string const &str, string const &delimiters, vector<double> &list)
{
   //--- Extracts a list of double from a string.
   //
   // INPUTS:
   //  str           String to analyse
   //  delimiters    Separator searched for
   // OUTPUT:
   //  list          Vector of double found in str

   vector<string> tokens;
   string2list(str, delimiters, tokens);

   // Convert in Double_t
   for (int i = 0; i < (int)tokens.size(); ++i)
      list.push_back(atof(tokens[i].c_str()));
}

//______________________________________________________________________________
void string2list(string const &str, string const &delimiters, vector<int> &list)
{
   //--- Extracts a list of int from a string.
   //
   // INPUTS:
   //  str           String to analyse
   //  delimiters    Separator searched for
   // OUTPUT:
   //  list          Vector of int found in str

   vector<string> tokens;
   string2list(str, delimiters, tokens);

   // Convert in int
   for (int i = 0; i < (int)tokens.size(); ++i)
      list.push_back(atoi(tokens[i].c_str()));
}

//______________________________________________________________________________
string string_fixlength(string const &str, size_t string_out_length)
{
   //--- Fixes the string to the length string_out_length. If the input string
   //    has been shorter, spaces will be added. If the input has been longer,
   //    the end of the input string will be truncated.
   //
   //  str                String to fix
   //  string_out_length  Total fixed length of output string

   string space = " ";
   string buffer(string_out_length, ' ');
   string cleanstring = removeblanks_from_startstop(str);

   for (size_t i = 0; i < string_out_length; ++i)
      buffer[i] = space[0] ;
   for (size_t i = 0; i < min(cleanstring.length(), string_out_length); ++i)
      buffer[i] = cleanstring[i] ;

   return buffer;
}

//______________________________________________________________________________
string string_wrap(string const &str, size_t line_length, size_t leading_spaces)
{
   //--- Wraps a string according to a given line length.
   //    Additionally, the number of leading spaces can be specified
   //    (leading spaces will be added to the specified line length).
   //    Taken from www.rosettacode.org/wiki/Word_wrap
   //
   // INPUTS:
   //  str            String to wrap
   //  line_length    Line length around which to wrap
   /// leading_spaces Number of additional leading spaces
   // OUTPUT:
   //  String with wrapped text and leading spaces.

   istringstream words(str);
   ostringstream wrapped;
   string word;
   string leading_space;

   for (size_t i = 0; i < leading_spaces; ++i) {
      leading_space = leading_space += " ";
   }

   if (words >> word) {
      wrapped << word;
      size_t space_left = line_length - word.length();
      while (words >> word) {
         if (space_left < word.length() + 1) {
            wrapped << '\n' << leading_space << word;
            space_left = line_length - word.length();
         } else {
            wrapped << ' ' << word;
            space_left -= word.length() + 1;
         }
      }
   }
   return leading_space + wrapped.str();
}

//______________________________________________________________________________
string sys_command(string const &command)
{
   //--- executes the command "command" on the command line
   //    and dumps the output of the command into an output string.
   //
   // INPUTS:
   //  command    string containing the command(s) to execute.
   // OUTPUT:
   //  outstring  command line output of the command(s) in a string.

   string outstring = "";
   const int max_buffer = 256;

   FILE *stream = popen(command.c_str(), "r");
   if (stream) {
      char buffer[max_buffer];
      while (!feof(stream))
         if (fgets(buffer, max_buffer, stream) != NULL) outstring.append(buffer);
      pclose(stream);
   }
   return removeblanks_from_startstop(outstring);
}

//______________________________________________________________________________
void sys_safe_execution(string const &command)
{
   //--- executes the command "command" via system()
   //    and returns a safe error message in case the command
   //    execution failed.
   //
   // INPUTS:
   //  command    string containing the command(s) to execute.

   int sys_return = system(command.c_str());

   if (sys_return == -1) {
      printf("\n====> ERROR: sys_safe_execution() in misc.cc");
      printf("\n             System command execution\n");
      printf("\n             %s\n", command.c_str());
      printf("\n             failed.");
      printf("\n             => abort()\n\n");
      abort();
   }
}

//______________________________________________________________________________
string upper_case(string const &s)
{
   //--- Returns an upper-cased version of the string s.
   //
   //  s             Input string

   string s2 = s;
   for (int i = s.length() - 1; i >= 0; --i) {
      if ('a' <= s[i] && s[i] <= 'z')
         s2[i] = s[i] - 'a' + 'A';
   }
   return s2;
}
