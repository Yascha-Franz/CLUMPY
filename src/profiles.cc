/*! \file profiles.cc \brief (see profiles.h) */

// CLUMPY includes
#include "../include/clumps.h"
#include "../include/geometry.h"
#include "../include/inlines.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/profiles.h"
// C++ std libraries
using namespace std;
#include <iostream>
#include <math.h>
#include <numeric>

// GSL includes
#include <gsl/gsl_deriv.h>
#include <gsl/gsl_errno.h>

//______________________________________________________________________________
void dlog_rho_dlog_r(double &r, double par[6], double &res)
{
   //--- Sets res = d(log rho(r) ) / d(log r) of density profile [-].
   //
   // INPUTS:
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        Normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[1]        Scale radius [kpc]
   //  par[2]        Shape parameter #1
   //  par[3]        Shape parameter #2
   //  par[4]        Shape parameter #3
   //  par[5]        card_profile [gENUM_PROFILE]
   // OUTPUT:
   //  res           Value of rho(r)

   int profile = (int)par[5];
   // Calculate rho(r)
   switch (profile) {
      case kISHIYAMA14:
      case kZHAO:
         res = dlog_rho_dlog_r_ZHAO(r, par);
         break;
      case kEINASTO:
         res = dlog_rho_dlog_r_EINASTO(r, par);
         break;
      case kEINASTO_N:
         res = dlog_rho_dlog_r_EINASTO_N(r, par);
         break;
      case kBURKERT:
         res = dlog_rho_dlog_r_BURKERT(r, par);
         break;
      case kDPDV_GAO04:
         res = dlog_rho_dlog_r_DPDV_GAO04(r, par);
         break;
      case kDPDV_SIGMOID_EINASTO:
         res = dlog_rho_dlog_r_DPDV_SIGMOID_EINASTO(r, par);
         break;
      case kDPDV_SPRINGEL08_ANTIBIASED:
         res = dlog_rho_dlog_r_DPDV_SPRINGEL08_ANTIBIASED(r, par);
         break;
      case kDPDV_SPRINGEL08_FIT:
         res = dlog_rho_dlog_r_DPDV_SPRINGEL08_FIT(r, par);
         break;
      case kDPDV_PIERI11:
         res = 0;
         break;
      case kNODES:
         res = dlog_rho_dlog_r_NODES(r, par);
         break;

      default :
         printf("\n====> ERROR: dlog_rho_dlog_r() in profiles.cc");
         printf("\n             card_profile=%d does not correspond to any yet implemented DM profile", (int)par[5]);
         printf("\n             => abort()\n\n");
         abort();
   }
   return;
}

//______________________________________________________________________________
double dlog_rho_dlog_r_BURKERT(double &r, double par[2])
{
   //--- Returns d(log rho(r) ) / d(log r) for a Burkert profile (Burkert 1995) :
   //       => d(log rho(r) ) / d(log r)  = -1 / (1 +  1/(r/r_0) ) - 2 /(1+ 1/(r/r_0)^2) ).
   //
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_0: density normalisation [Msol/kpc^3]
   //  par[1]        r_0: scale radius (replaces rs for this profile) [kpc]

   return -1. / ((1. + par[1] / r)  - 2. / (1. + ((par[1] * par[1]) / (r * r))));
}

//______________________________________________________________________________
double dlog_rho_dlog_r_EINASTO(double &r, double par[3])
{
   //--- Returns d(log rho(r) ) / d(log r) for an Einasto profile (Navarro et al. 2004 or Springel
   //    et al. 2008) [Msol/kpc^3]:
   //       => d(log rho(r) ) / d(log r) =  - 2 * (r/r_{-2})^{alpha}.
   //
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_{-2}: density normalisation [Msol/kpc^3]
   //  par[1]        r_{-2}: radius at which the slope is -2 (replaces rs for this profile) [kpc]
   //  par[2]        alpha: shape parameter of the profile

   return -2. * pow(r / par[1], par[2]);
}

//______________________________________________________________________________
double dlog_rho_dlog_r_EINASTO_N(double &r, double par[3])
{
   //--- Returns d(log rho(r) ) / d(log r) for an Einasto r^{1/n} profile (Merritt et al. 2006) [Msol/kpc^3]:
   //       => d(log rho(r) ) / d(log r) =  - d_n/n * (r/r_{-2})^{1/n}.
   //
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_e: density normalisation [Msol/kpc^3]
   //  par[1]        r_e: radius containing half the mass of the halo (replaces rs for this profile) [kpc]
   //  par[2]        n (double): index of the profile (shape parameter)

   // Analytical formula not valid below n<0.5
   if (par[2] < 0.5) {
      printf("\n====> ERROR: dlog_rho_dlog_r_EINASTO_N() in profiles.cc");
      printf("\n             n < 0.5 not authorised in CLUMPY (here n=%.2lf).", par[2]);
      printf("\n             => abort()\n\n");
      abort();
   }

   double dn = 3.*par[2] - 0.3333333333 + 0.0079 / par[2];
   return - dn / par[2] * pow(r / par[1], 1. / par[2]) ;
}

//______________________________________________________________________________
double dlog_rho_dlog_r_DPDV_GAO04(double &r, double par[5])
{
   //--- Returns d(log rho(r) ) / d(log r) for a Gao-like fit formula (parameters alpha
   //    and beta) to model the distribution of subclumps in a given host halo of
   //    contentration c and scale radius r_200. See, e.g., Gao (2004) and Madau
   //    et al. (2008). Note that these authors give
   //       N(<x) = (1+ac)x^beta / (1+ac x^alpha),
   //
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_200: density normalisation = 200 times the mean background density [Msol/kpc^3]
   //  par[1]        r_200: scale radius for the host halo [kpc]
   //  par[2]        a*c: product of parameter a with concentration parameter c of host.
   //  par[3]        alpha
   //  par[4]        beta

   double xtoalpha =  pow(r / par[1], par[3]);

   if (par[2] < 0. || par[3] < 0. || par[4] < 0.
         || par[4] < par[3] || par[4] > 3 + par[3]) {
      printf("\n====> ERROR: rho_DPDV_GAO04() in profiles.cc");
      printf("\n             kDPDV_GAO04 profile is not defined with these parameters.");
      printf("\n             The following conditions are required:");
      printf("\n             * SHAPE_PARAMS > 0");
      printf("\n             * SHAPE_PARAMS[2] > SHAPE_PARAMS[1]");
      printf("\n             * SHAPE_PARAMS[2] < SHAPE_PARAMS[1] + 3");
      printf("\n             => abort()\n\n");
      abort();
   } else if (par[4] - par[3] < SMALL_NUMBER) {
      // Solution for special case beta = alpha
      return -3 + par[3] * (-1 + 2 / (1 + par[2] * xtoalpha));
   }  else if (fabs(par[4] - par[3] - 1) < SMALL_NUMBER) {
      // Solution for special case beta-alpha=1
      return -2 + (2 * par[3]) / (1 + par[2] * xtoalpha) - (par[3] * (1 + par[3])) / (
                1 + par[3] + par[2] * xtoalpha);
   } else {
      return -3 + par[4] + par[3] * (-1 + 2 / (1 + par[2] * xtoalpha) - par[4] /
                                     (par[4] + par[2] * (par[4] - par[3]) *  xtoalpha));
   }
   return 0.;
}

//______________________________________________________________________________
double dlog_rho_dlog_r_DPDV_SIGMOID_EINASTO(double &r, double par[5])
{
   //--- Returns  d(log rho(r) ) / d(log r) for a Sigmoid*Einasto profile  [Msol/kpc^3]
   //       => d(log rho(r) ) / d(log r) =  r / [r_c * (1+exp((r-r_0)/r_c))]  + d(log(Einasto)/d(log r).
   //
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        A: density normalisation [Msol/kpc^3]
   //  par[1]        r_e: Einasto scale radius [kpc]
   //  par[2]        alpha: shape parameter of the profile
   //  par[3]        r_0: value of the sigmoid mid-point [kpc]
   //  par[4]        r_c: steepness of the sigmoid [kpc]

   return r / (par[4] * (1. + exp((r - par[3]) / par[4]))) + dlog_rho_dlog_r_EINASTO(r, par);
}

//______________________________________________________________________________
double dlog_rho_dlog_r_DPDV_SPRINGEL08_ANTIBIASED(double &r, double par[3])
{
   //--- Returns d(log rho(r) ) / d(log r) for an anti-biased Einasto profile  [Msol/kpc^3]
   //       => d(log rho(r) ) / d(log r) =  1 - 2 * (r/r_{-2})^{alpha}.
   //
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_{-2}: density normalisation [Msol/kpc^3]
   //  par[1]        r_{-2}: radius at which the slope of rho_EINASTO (this is not a typo!) is -2 [kpc]
   //  par[2]        alpha: shape parameter of the profile

   return 1. + dlog_rho_dlog_r_EINASTO(r, par);
}

//______________________________________________________________________________
double dlog_rho_dlog_r_DPDV_SPRINGEL08_FIT(double &r, double par[5])
{
   //--- Returns d(log rho(r) ) / d(log r) for the Aquarius fitting (Springel et al., 2008):
   //       => rho_{sub}(r) propto exp{ gamma +  beta  log{r/r_{50}} + 0.5  alpha  log{r/r_{50}}^2} rho_{tot}(r).
   //
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_{-2}: density normalisation [Msol/kpc^3]
   //  par[1]        r_{50}: radius at which the the density of the total halo is 50 times the critical density [kpc]
   //  par[2]        alpha: shape parameter of the profile
   //  par[3]        beta: shape parameter of the profile
   //  par[4]        gamma: shape parameter of the profile (no influence on d(log rho(r) ) / d(log r))

   return par[3] + par[2] * log(r / par[1]);
}

//______________________________________________________________________________
double dlog_rho_dlog_r_NODES(double &r, double par[3])
{
   //--- Returns d(log rho(r) ) / d(log r) for a numerically given profile

   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_s: density normalisation [Msol/kpc^3]
   //  par[1]        r_s: scale radius [kpc]
   //  par[2]        which node halo to choose

   double x = r / par[1];
   int i_halo = int(par[2]);

   if (par[2] != i_halo) {
      char tmp[256];
      sprintf(tmp, "Shape parameter (#1) for kNODES profile, indicating which halo "
              "from gLIST_HALOES_NODES is used, must be an integer >= 1 (is %g).", par[2]);
      print_error("profiles.cc", "dlog_rho_dlog_r_NODES()", string(tmp));
   } else if (i_halo >= gHALO_NODES_X_GRID.size()) {
      char tmp[256];
      sprintf(tmp, "Shape parameter (#1) for kNODES profile, "
              "indicating which halo from gLIST_HALOES_NODES is used, is larger than number "
              "of halos provided in list gLIST_HALOES_NODES (is %d and must be <= %d).", i_halo, int(gHALO_NODES_X_GRID.size() - 1));
      print_error("profiles.cc", "dlog_rho_dlog_r_NODES()", string(tmp));
   }

   if (x <= gHALO_NODES_X_GRID[i_halo][0] or x >= gHALO_NODES_X_GRID[i_halo][gHALO_NODES_X_GRID[i_halo].size() - 1])
      return 0.;
   else {
      gsl_function F;
      F.function = &rho_NODES_log;
      structHaloProfile params;
      params.rhos = 1;
      params.rscale =  1;
      params.shapeParam1 = i_halo;
      F.params = &params;
      double result, abserr;
      gsl_deriv_central(&F, x, gSIM_EPS, &result, &abserr);
      return x * result;
   }
}

//______________________________________________________________________________
double dlog_rho_dlog_r_ZHAO(double &r, double par[5])
{
   //--- Returns d(log rho(r) ) / d(log r) for a ZHAO (alpha,beta,gamma) [Msol/kpc^3]:
   //       => d(log rho(r) ) / d(log r) =  - beta / [1+(r/r_s)^{-alpha}] - gamma / [1+(r/r_s)^{alpha}]

   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_s: density normalisation [Msol/kpc^3]
   //  par[1]        r_s: scale radius [kpc]
   //  par[2]        alpha: transition slope
   //  par[3]        beta: outer slope
   //  par[4]        gamma: inner slope

   return -par[3] / (1. + pow(r / par[1], - par[2])) - par[4] / (1. + pow(r / par[1], par[2])) ;
}

//______________________________________________________________________________
double get_ratio_r_2_rscale(double par_prof[4])
{
   //--- Returns F = r_2 / rscale, a conversion factor to calculate r_2 to rs.
   //    N.B.: r_2 corresponds to d/dr(r^2 rho(r)) |(r=r_2) = 0, with
   //       - BURKERT           : rscale=r0 => Root of -x^3+x+2 => F = 1.52137970680456756960
   //       - EINASTO           : rscale=r_2 => F = 1
   //       - DPDV_SPRINGEL08_ANTIBIASED: rscale=r_2EINASTO => F = (3/2)^(1/alpha)
   //       - EINASTO_N         : rscale=re  =>  F = pow(2n/dn,n) [Eq. 8 Graham et al., AJ 132, 2701 (2006)]
   //       - ZHAO,ISHIYAMA     : rscale=rs  =>  F = pow((beta-2)/(2-gamma),-1/alpha)
   //       - GAO               : Tedious formula... (see below or documentation)
   //
   //  par_prof[0]   Shape parameter #1
   //  par_prof[1]   Shape parameter #2
   //  par_prof[2]   Shape parameter #3
   //  par_prof[3]   card_profile [gENUM_PROFILE]

   int card_profile = (int)par_prof[3];
   switch (card_profile) {
      case kEINASTO:
         return 1.;
      case kISHIYAMA14:
      case kZHAO:
         if (par_prof[1] > 2.)
            return pow((par_prof[1] - 2.) / (2. - par_prof[2]), -1. / par_prof[0]);
         else {
            printf("\n====> ERROR: get_ratio_r_2_rscale() in profiles.cc");
            printf("\n             r_{-2} is not defined for kZHAO if beta<2 (here beta=%.2le)", par_prof[1]);
            printf("\n             => abort()\n\n");
            abort();
         }
      case kDPDV_SIGMOID_EINASTO:
         // No analytical solution, but as long as rc<rs/4 and r0<rs/4, behaves as Einasto
         if (par_prof[2] < par_prof[1] / 4. && par_prof[3] < par_prof[1] / 4.)
            return 1.;
         else {
            printf("\n====> ERROR: get_ratio_r_2_rscale() in profiles.cc");
            printf("\n             r_{-2} has no analytical form for kDPDV_SIGMOID_EINASTO with these parameters");
            printf("\n             => abort()\n\n");
            abort();
         }
      case kDPDV_SPRINGEL08_ANTIBIASED:
         return pow(1.5, 1. / par_prof[0]);
      case kEINASTO_N:
         return pow(2. * par_prof[0] / (3.*par_prof[0] - 0.3333333333 + 0.0079 / par_prof[0]), par_prof[0]);
      case kDPDV_GAO04:
         // Params: par_prof[0]=ac, par_prof[1]=alpha, par_prof[2]=beta
         // Crosses -2 if inner slope>-2 and outer slope<-2
         //    - Inner slope is beta-3 => beta>1
         //    - Outer slope is beta-3-alpha (beta > alpha)
         //                         -3-beta  (beta = alpha)   => beta-alpha=<1
         //    - Singularity for alpha > beta => beta-alpha>=0
         if (par_prof[1] < SMALL_NUMBER ||  par_prof[2] <= 1.
               || par_prof[2] - par_prof[1] > 1.
               || par_prof[2] < par_prof[1]) {
            printf("\n====> ERROR: get_ratio_r_2_rscale() in profiles.cc");
            printf("\n             r_{-2} is not defined for kDPDV_GAO04 with these parameters");
            printf("\n             => abort()\n\n");
            abort();
         } else if (par_prof[2] - par_prof[1] < SMALL_NUMBER) {
            // Solution for special case beta = alpha
            return pow((par_prof[1] - 1) / (par_prof[0] * (1 + par_prof[1])), 1. / par_prof[1]);
         }  else if (par_prof[2] - par_prof[1] < 1. - SMALL_NUMBER) {
            // Solution is that of Ay^2+By+C=0, with
            //    r=(y/(ac))^{1/alpha}
            // and
            //   y = -B+sqrt(D)]/(2A)
            //   A = (beta-alpha)(alpha+beta-1) - 2*alpha*beta +2*alpha^2
            //   B= (beta-alpha)(alpha+beta-1) - 2*alpha*beta + C
            //   C = beta*(beta-1)
            //   D = B^2-4AC
            double tmp = (par_prof[2] - par_prof[1]) * (par_prof[1] + par_prof[2] - 1) - 2.*par_prof[1] * par_prof[2];
            double C = par_prof[2] * (par_prof[2] - 1);
            double B = tmp + C;
            double A = tmp + 2.*par_prof[1] * par_prof[1];
            double D = B * B - 4.*A * C;
            return pow((-B - sqrt(D)) / (2.*A * par_prof[0]), 1. / par_prof[1]);
         }  else if (par_prof[2] - par_prof[1] > 1. - SMALL_NUMBER && par_prof[1] > 1.) {
            // Solution for special case beta-alpha=1
            double numerator = (par_prof[1] + 1) ;
            double denominator = par_prof[0] * (par_prof[1] - 1);
            return pow(numerator / denominator, 1. / par_prof[1]);
         } else {
            printf("\n====> ERROR: get_ratio_r_2_rscale() in profiles.cc");
            printf("\n             r_{-2} is not defined for kDPDV_GAO04 with beta = alpha + 1 and alpha <= 1.");
            printf("\n             => abort()\n\n");
            abort();
         }
         break;
      case kBURKERT:
         return 1.52137970680456756960;
      case kDPDV_SPRINGEL08_FIT:
         cout << ">>> get_ratio_r_2_rscale: kDPDV_SPRINGEL08_FIT cannot be chosen as density profile => abort() " << endl;
         abort();
      case kNODES:
         if (par_prof[0] != int(par_prof[0])) {
            char tmp[256];
            sprintf(tmp, "Shape parameter (#1) for kNODES profile, indicating which halo "
                    "from gLIST_HALOES_NODES is used, must be an integer >= 1 (is %g).", par_prof[0]);
            print_error("profiles.cc", "get_ratio_r_2_rscale()", string(tmp));
         } else if (int(par_prof[0]) >= gHALO_NODES_RATIO_R_2_RSCALE.size()) {
            char tmp[256];
            sprintf(tmp, "Shape parameter (#1) for kNODES profile, "
                    "indicating which halo from gLIST_HALOES_NODES is used, is larger than number "
                    "of halos provided in list gLIST_HALOES_NODES (is %d and must be <= %d).",
                    int(par_prof[0]), int(gHALO_NODES_RATIO_R_2_RSCALE.size() - 1));
            print_error("profiles.cc", "get_ratio_r_2_rscale()", string(tmp));
         }
         return gHALO_NODES_RATIO_R_2_RSCALE[int(par_prof[0])];
      default :
         printf("\n====> ERROR: get_ratio_r_2_rscale() in profiles.cc");
         printf("\n             card_profile=%d does not correspond to any DM profile", card_profile);
         printf("\n             => abort()\n\n");
         abort();
   }
   return 0.;
}

//______________________________________________________________________________
double get_rscale_from_r_2(double const &r_2, double par_prof[4])
{
   //--- Returns scale radius rs [kpc] from r_2 [kpc] for a given profile.
   //
   //  r_2           Radius [kpc] for which the DM profile  slope = -2
   //  par_prof[0]   Shape parameter #1
   //  par_prof[1]   Shape parameter #2
   //  par_prof[2]   Shape parameter #3
   //  par_prof[3]   card_profile [gENUM_PROFILE]

   return r_2 / get_ratio_r_2_rscale(par_prof);
}

//______________________________________________________________________________
double get_r_2_from_rscale(double const &rs, double par_prof[4])
{
   //--- Returns r_2 [kpc] from the scale radius rs [kpc] for a given profile.
   //
   //  r_s           Scale radius [kpc] (rs, re or r{-2}, depending on card_profile)
   //  par_prof[0]   Shape parameter #1
   //  par_prof[1]   Shape parameter #2
   //  par_prof[2]   Shape parameter #3
   //  par_prof[3]   card_profile [gENUM_PROFILE]


   return rs * get_ratio_r_2_rscale(par_prof);
}

//______________________________________________________________________________
double get_rsat(double par[6])
{
   //--- Returns r_sat [kpc] for which rho(r<r_sat)=rho_sat.
   //
   //  par[0]        Dark matter density normalisation [Msol/kpc^3]
   //  par[1]        Scale radius [kpc]
   //  par[2]        Shape parameter #1
   //  par[3]        Shape parameter #2
   //  par[4]        Shape parameter #3
   //  par[5]        card_profile [gENUM_PROFILE]

   int profile = (int)par[5];
   switch (profile) {
      case kISHIYAMA14:
      case kZHAO:
         if (par[4] > 0.)
            return par[1] * pow(par[0] / gDM_RHOSAT, 1. / par[4]);
         else
            return 0.;
      case kBURKERT:
         return 0.;
      case kEINASTO:
         return par[1] * pow(1 + par[2] / 2 * log(par[0] / gDM_RHOSAT), 1. / par[2]);
      case kDPDV_SPRINGEL08_ANTIBIASED:
         return 0.;
      case kEINASTO_N:
         return 0.;
      case kDPDV_GAO04:
         if (par[4] < 3.) {
            double norm_factor = par[4] - par[3] * par[2] / (1. + par[2]);
            return par[1] * pow(gDM_RHOSAT / par[0] * norm_factor / (par[4] * (1 + par[2])), 1. / (par[4] - 3.));
         } else
            return 0.;
      case kDPDV_SIGMOID_EINASTO:
      case kDPDV_SPRINGEL08_FIT:
         return 0.;
      case kDPDV_PIERI11:
         return 0.;
      case kNODES:
         if (par[2] != int(par[2])) {
            char tmp[256];
            sprintf(tmp, "Shape parameter (#1) for kNODES profile, indicating which halo "
                    "from gLIST_HALOES_NODES is used, must be an integer >= 1 (is %g).", par[2]);
            print_error("profiles.cc", "get_rsat()", string(tmp));
         } else if (int(par[2]) >= gHALO_NODES_X_GRID.size()) {
            char tmp[256];
            sprintf(tmp, "Shape parameter (#1) for kNODES profile, "
                    "indicating which halo from gLIST_HALOES_NODES is used, is larger than number "
                    "of halos provided in list gLIST_HALOES_NODES (is %g and must be <= %d).", par[2], int(gHALO_NODES_X_GRID.size() - 1));
            print_error("profiles.cc", "get_rsat()", string(tmp));
         }
         if (par[0] * gHALO_NODES_Y_GRID[int(par[2])][0] <= gDM_RHOSAT)
            return 0.;
         else {
            print_warning("profiles.cc", "get_rsat()", "kNODES profile exceeding gDM_RHOSAT. "
                          "Cropping is very slow, consider cropping input profile.");
            gsl_function F;
            F.function = &rho_NODES_find_rsat;
            structHaloProfile params;
            params.rhos = par[0];
            params.rscale =  par[1];
            params.shapeParam1 = par[2];
            F.params = &params;
            double rmin = 0;
            double rmax = par[1];
            int return_status = 01;
            return rootsolver_gsl(gsl_root_fsolver_brent, F, rmin, rmax, gSIM_EPS, return_status);
         }
      default :
         printf("\n====> ERROR: get_rsat() in profiles.cc");
         printf("\n             card_profile=%d does not correspond to any DM profile", (int)par[5]);
         printf("\n             => abort()\n\n");
         abort();
   }
   return 0.;
}

//______________________________________________________________________________
double get_rvir(double par[6], double const &rho_vir)
{
   //--- Returns Rvir, such that rho(Rvir)=rho_vir.
   //
   //  par[0]        Dark matter density normalisation [Msol/kpc^3]
   //  par[1]        Scale radius [kpc]
   //  par[2]        Shape parameter #1
   //  par[3]        Shape parameter #2
   //  par[4]        Shape parameter #3
   //  par[5]        card_profile [gENUM_PROFILE]
   //  rho_vir       Alleged DM density at the virial radius [Msol/kpc^3]

   double rmin = par[1] / 10.;
   double rmax = 1.e3;
   int n = 1000;
   double r = rmin;
   double dr = pow(rmax / rmin, 1. / (double)n);
   for (int i = 0; i < n; ++i) {
      r *= dr;
      double res = 0.;
      rho(r, par, res);
      if (res < rho_vir)
         return r;
   }
   return 0.;
}

//______________________________________________________________________________
string legend_for_profile(double par_prof[4], bool is_with_formula)
{
   //--- Returns a name as "profile name formula parameters".
   //
   //  par_prof[0]   Shape parameter #1
   //  par_prof[1]   Shape parameter #2
   //  par_prof[2]   Shape parameter #3
   //  par_prof[3]   card_profile [gENUM_PROFILE]

   int card_profile = (int)par_prof[3];
   char tmp[500];
   string name = gNAMES_PROFILE[card_profile];
   string formula = "";

   switch (card_profile) {
      case kBURKERT:
         if (is_with_formula)
            formula = "#rho(r) #propto #frac{1}{(1+r/r_0)(1+((r/r_0)^2))}";
         sprintf(tmp, "%s %s", name.c_str(), formula.c_str());
         return tmp;
      case kEINASTO:
         if (is_with_formula)
            formula = "#rho(r) #propto #exp(-(2/#alpha) [(r/r_{-2})^{#alpha} -1]) with ";
         sprintf(tmp, "%s %s (#alpha = %g)", name.c_str(), formula.c_str(), par_prof[0]);
         return tmp;
      case kEINASTO_N:
         if (is_with_formula)
            formula = "#rho(r) #propto #exp(-d_{n}[(r/r_{e})^{1/n}- 1]) with";
         sprintf(tmp, "%s %s (n = %d)", name.c_str(), formula.c_str(), (int)par_prof[0]);
         return tmp;
      case kISHIYAMA14:
      case kZHAO:
         if (is_with_formula)
            formula = "#rho(r) #propto#frac{(r/r_{s})^{-#gamma}}{[1+(r/r_{s})^{#alpha}]^{(#beta-#gamma)/#alpha}} with";
         sprintf(tmp, "%s %s (%g,%g,%g)", name.c_str(), formula.c_str(), par_prof[0], par_prof[1], par_prof[2]);
         return tmp;
      case kDPDV_GAO04:
         if (is_with_formula)
            formula = "dP/dV(x = r/r_{200}) #propto #frac{[ (1+ac) x^{#beta -3} (#beta + ac (#beta - #alpha) x^{#alpha})]}{(1+ ac x^{#alpha})^{2}}";
         sprintf(tmp, "%s %s (%g,%g,%g)", name.c_str(), formula.c_str(), par_prof[0], par_prof[1], par_prof[2]);
         return tmp;
      case kDPDV_SIGMOID_EINASTO:
         if (is_with_formula)
            formula = "dP/dV(r) #propto #frac{1}{1+#exp(-(r-r_{0})/r_{c})} #exp(-(2/#alpha) [(r/r_{-2})^{#alpha} -1]) with ";
         sprintf(tmp, "%s %s (#alpha = %g, r_{0}=%g, r_{c}=%g)", name.c_str(), formula.c_str(), par_prof[0], par_prof[1], par_prof[2]);
         return tmp;
      case kDPDV_SPRINGEL08_ANTIBIASED:
         if (is_with_formula)
            formula = "dP/dV(r) #propto r #exp(-(2/#alpha) [(r/r_{-2})^{#alpha} -1]) with ";
         sprintf(tmp, "%s %s (#alpha = %g)", name.c_str(), formula.c_str(), par_prof[0]);
         return tmp;
      case kDPDV_SPRINGEL08_FIT:
         if (is_with_formula)
            formula = "dP/dV_{sub}(r) #propto #exp{#gamma + #beta #log{r/r_{50}} + 0.5 #alpha #log{r/r_{50}}^2} rho_{tot}(r) with";
         sprintf(tmp, "%s %s (%g,%g,%g)", name.c_str(), formula.c_str(), par_prof[0], par_prof[1], par_prof[2]);
         return tmp;
      case kDPDV_PIERI11:
         sprintf(tmp, "Pieri et al. (2011) NFW implementation");
         return tmp;
      case kNODES:
         sprintf(tmp, "%s #%d (numeric)", name.c_str(), int(par_prof[0]));
         return tmp;
      default :
         printf("\n====> ERROR: legend_for_profile() in profiles.cc");
         printf("\n             card_profile=%d does not correspond to any DM profile", card_profile);
         printf("\n             => abort()\n\n");
         abort();
   }
   return "";
}

//______________________________________________________________________________
double rho_BURKERT(double &r, double par[2])
{
   //--- Returns rho(r) for Burkert profile (Burkert 1995) [Msol/kpc^3]:
   //       => rho(r)= rho_0 / ( (1+r/r_0) (1+(r/r_0)^2) ).
   //
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_0: density normalisation [Msol/kpc^3]
   //  par[1]        r_0: scale radius (replaces rs for this profile) [kpc]

   return par[0] / ((1. + r / par[1]) * (1. + (r * r / (par[1] * par[1]))));
}

//______________________________________________________________________________
double rho_EINASTO(double &r, double par[3])
{
   //--- Returns rho(r) for Einasto profile (Navarro et al. 2004 or Springel
   //    et al. 2008) [Msol/kpc^3]:
   //       => rho(r)= rho_{-2} * exp{-2/alpha [(r/r_{-2})^{alpha} - 1] }.
   //
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_{-2}: density normalisation [Msol/kpc^3]
   //  par[1]        r_{-2}: radius at which the slope is -2 (replaces rs for this profile) [kpc]
   //  par[2]        alpha: shape parameter of the profile

   // prevent singularity for alpha_E = 1:
   if (fabs(par[2] - 1) < SMALL_NUMBER) par[2] += SMALL_NUMBER;

   double arg_exp = -2. / par[2] * (pow(r / par[1], par[2]) - 1.);
   if (arg_exp > 300) //overflow
      return gDM_RHOSAT;

   return par[0] * exp(arg_exp);
}

//______________________________________________________________________________
double rho_EINASTO_N(double &r, double par[3])
{
   //--- Returns rho(r) for Einasto r^{1/n} profile (Merritt et al. 2006) [Msol/kpc^3]:
   //       => rho(r)= rho_e * exp{-dn [(r/r_e)^{1/n} - 1] }.
   //
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_e: density normalisation [Msol/kpc^3]
   //  par[1]        r_e: radius containing half the mass of the halo (replaces rs for this profile) [kpc]
   //  par[2]        n (double): index of the profile (shape parameter)

   // Analytical formula not valid below n<0.5
   if (par[2] < 0.5) {
      printf("\n====> ERROR: rho_EINASTO_N() in profiles.cc");
      printf("\n             n < 0.5 not authorised in CLUMPY (here n=%.2lf).", par[2]);
      printf("\n             => abort()\n\n");
      abort();
   }
   double dn = 3.*par[2] - 0.3333333333 + 0.0079 / par[2];

   return par[0] * exp(-dn * (pow((r / par[1]), (1. / par[2])) - 1.));
}

//______________________________________________________________________________
double rho_DPDV_GAO04(double &r, double par[5])
{
   //--- Returns rho(r) [Msol/kpc^3] for a Gao-like fit formula (parameters alpha
   //    and beta) to model the distribution of subclumps in a given host halo of
   //    contentration c and scale radius r_200. See, e.g., Gao (2004) and Madau
   //    et al. (2008). Note that these authors give
   //       N(<x) = (1+ac)x^beta / (1+ac x^alpha),
   //    with
   //       x=r/r_200.
   //    Using N(<x) = int r^2 dN/dr dr, with dN/dr the profile we are looking for,
   //    we get
   //       rho(x)= rho_s [ (1+ac) x^{beta -3} * (beta + ac (beta - alpha) x^alpha)]
   //               / [(1+ ac x^alpha)^2].
   //
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_200: density normalisation = 200 times the mean background density [Msol/kpc^3]
   //  par[1]        r_200: scale radius for the host halo [kpc]
   //  par[2]        a*c: product of parameter a with concentration parameter c of host.
   //  par[3]        alpha
   //  par[4]        beta

   double x = r / par[1];

   // Norm factor to ensure rho(r_{200})=rho_{200}
   // Norm factor always > 0 for condition par[4] > par[3]
   double norm_factor = par[4] - par[3] * par[2] / (1. + par[2]);
   double res;

   if (par[2] < 0. || par[3] < 0. || par[4] < 0.
         || par[4] < par[3] || par[4] > 3 + par[3]) {
      printf("\n====> ERROR: rho_DPDV_GAO04() in profiles.cc");
      printf("\n             kDPDV_GAO04 profile is not defined with these parameters.");
      printf("\n             The following conditions are required:");
      printf("\n             * SHAPE_PARAMS > 0");
      printf("\n             * SHAPE_PARAMS[2] > SHAPE_PARAMS[1]");
      printf("\n             * SHAPE_PARAMS[2] < SHAPE_PARAMS[1] + 3");
      printf("\n             => abort()\n\n");
      abort();
   } else if (par[4] - par[3] < SMALL_NUMBER) {
      // Solution for special case beta = alpha
      res = par[0] / norm_factor * (1 + par[2]) * pow(x, par[4] - 3)
            / pow(1 + par[2] * pow(x, par[3]), 2);
   } else {
      res = par[0] / norm_factor * ((1 + par[2]) * pow(x, par[4] - 3)
                                    * (par[4] + par[2] * (par[4] - par[3]) * pow(x, par[3])))
            / pow(1 + par[2] * pow(x, par[3]), 2);
   }
   return res;
}

//______________________________________________________________________________
double rho_DPDV_SIGMOID_EINASTO(double &r, double par[5])
{
   //--- Returns sigmoid(r) * rho_EINASTO(r) fitted on phat-ELVIS simulation (Kelley et al., 2019)
   //        rho(r) = A * {1 /[1+exp(-(r-r_0)/r_c)]} * exp{-2/alpha*[(r/r_e)^alpha-1]}
   //    N.B.: fit proposed in Hütten et al. (2019) using phat-ELVIS catalogue (courtesy of Kelley & Bullock)
   //
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        A: density normalisation [Msol/kpc^3]
   //  par[1]        r_e: Einasto scale radius [kpc]
   //  par[2]        alpha: shape parameter of the profile
   //  par[3]        r_0: value of the sigmoid mid-point [kpc]
   //  par[4]        r_c: steepness of the sigmoid [kpc]

   double sigmoid = 1. / (1. + exp(- (r - par[3]) / par[4]));
   return sigmoid * rho_EINASTO(r, par);
}

//______________________________________________________________________________
double rho_DPDV_SPRINGEL08_ANTIBIASED(double &r, double par[3])
{
   //--- Returns r * rho_EINASTO(r) for Einasto profile  [Msol/kpc^3]
   //    for modeling an anti-biased distribution of subclumps for an
   //    Einasto-shaped total halo (e.g., Springel et al., 2008):
   //       => rho(r)= (r/rho_{-2}) * exp{-2/alpha [(r/r_{-2})^{alpha} - 1] }.
   //
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_{-2}: density normalisation [Msol/kpc^3]
   //  par[1]        r_{-2}: radius at which the slope of rho_EINASTO (this is not a typo!) is -2 [kpc]
   //  par[2]        alpha: shape parameter of the profile

   double arg_exp = -2. / par[2] * (pow(r / par[1], par[2]) - 1.);
   if (arg_exp > 300) //overflow
      return gDM_RHOSAT;

   return (r / par[1]) * par[0] * exp(arg_exp);
}

//______________________________________________________________________________
double rho_DPDV_SPRINGEL08_FIT(double &r, double par[5])
{
   //--- Returns f_sub(r) * rho_tot(r) for Aquarius fitting (Springel et al., 2008):
   //       => rho_{sub}(r) propto exp{ gamma +  beta  log{r/r_{50}} + 0.5  alpha  log{r/r_{50}}^2} rho_{tot}(r).
   //
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_{-2}: density normalisation [Msol/kpc^3]
   //  par[1]        r_{50}: radius at which the the density of the total halo is 50 times the critical density [kpc]
   //  par[2]        alpha: shape parameter of the profile
   //  par[3]        beta: shape parameter of the profile
   //  par[4]        gamma: shape parameter of the profile

   if (par[2] > 0.) {
      printf("\n====> ERROR: rho_DPDV_SPRINGEL08_FIT() in profiles.cc");
      printf("\n             alpha = SHAPE_PARAMS[0] < 0 required.");
      printf("\n             => abort()\n\n");
      abort();
   }

   // Calculate rho_tot(r)
   // it is checked in params.cc that this profile is only used together with the Galactic module.
   double *par_tot = new double[6];
   par_tot[0] = 1.;
   par_tot[1] = gMW_TOT_RSCALE;
   par_tot[2] = gMW_TOT_SHAPE_PARAMS[0];
   par_tot[3] = gMW_TOT_SHAPE_PARAMS[1];
   par_tot[4] = gMW_TOT_SHAPE_PARAMS[2];
   par_tot[5] = gMW_TOT_FLAG_PROFILE;

   double rho_tot;
   rho(r, par_tot, rho_tot);

   double x = r / par[1];
   return par[0] * exp(par[4] +  par[3] * log(x) + 0.5 * par[2] * pow(log(x), 2)) * rho_tot;
}

//______________________________________________________________________________
double rho_DPDV_PIERI11(double &r, double par[3])
{
   //--- Returns NFW substructure profile according to (Pieri et al., 2011):
   //
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_{-2}: density normalisation [Msol/kpc^3]
   //  par[1]        r_s
   //  par[2]        r_b bias radius
   return par[0] * par[1] / par[2] / (1. + pow(r / par[1], 2.)) / (1 + r / par[2]);

}

//______________________________________________________________________________
double rho_NODES(double &r, double par[3])
{
   //--- Returns rho(r) for a profile given on the grid.
   //       => rho(r)= rho_0 * f(r/r0)
   //
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_s: density normalisation [Msol/kpc^3]
   //  par[1]        r_0: scale radius [kpc]
   //  par[2]        which node halo to choose

   double x = r / par[1];
   int i_halo = int(par[2]);

   if (par[2] != i_halo) {
      char tmp[256];
      sprintf(tmp, "Shape parameter (#1) for kNODES profile, indicating which halo "
              "from gLIST_HALOES_NODES is used, must be an integer >= 1 (is %g).", par[2]);
      print_error("profiles.cc", "rho_NODES()", string(tmp));
   } else if (i_halo >= gHALO_NODES_X_GRID.size()) {
      char tmp[256];
      sprintf(tmp, "Shape parameter (#1) for kNODES profile, "
              "indicating which halo from gLIST_HALOES_NODES is used, is larger than number "
              "of halos provided in list gLIST_HALOES_NODES (is %d and must be <= %d).", i_halo, int(gHALO_NODES_X_GRID.size() - 1));
      print_error("profiles.cc", "rho_NODES()", string(tmp));
   }

   double f_x = interp1D(x, gHALO_NODES_X_GRID[i_halo], gHALO_NODES_Y_GRID[i_halo], gHALO_NODES_INTERPOLTYPE[i_halo], true);
   if (f_x == HPX_BLIND_VALUE) {
      // value falls outside interpolation range:
      if (x >= 1) {
         char tmp[256];
         sprintf(tmp, "Requested value at r/r_s = %g larger than max value r/r_s = %g "
                 "provided in grid (gLIST_HALOES_NODES).", x, gHALO_NODES_X_GRID[i_halo].back());
         print_error("profiles.cc", "rho_NODES()", string(tmp));
      } else f_x = gHALO_NODES_Y_GRID[i_halo][0]; // constant core at small radii
   }
   return par[0] * f_x;
}

//______________________________________________________________________________
double rho_NODES_log(double r, void *p)
{
   //--- GSL Function for log(rho_nodes)

   structHaloProfile *params = (struct structHaloProfile *)p;

   double par[3] = {params->rhos, params->rscale, params->shapeParam1};

   return log(rho_NODES(r, par));
}

//______________________________________________________________________________
double rho_NODES_find_rsat(double r, void *p)
{
   //--- GSL Function for to find d(log_rho_nodes)/d log r = - 2

   structHaloProfile *params = (struct structHaloProfile *)p;

   double par[3] = {params->rhos, params->rscale, params->shapeParam1};

   return rho_NODES(r, par) - gDM_RHOSAT;
}

//______________________________________________________________________________
double rho_NODES_find_r_2(double r, void *p)
{
   //--- GSL Function for to find radius of rho_nodes = r_sat

   structHaloProfile *params = (struct structHaloProfile *)p;

   double par[3] = {params->rhos, params->rscale, params->shapeParam1};

   return dlog_rho_dlog_r_NODES(r, par) + 2;
}

//______________________________________________________________________________
double rho_ZHAO(double &r, double par[5])
{
   //--- Returns rho(r) for a ZHAO (alpha,beta,gamma) [Msol/kpc^3]:
   //       => rho(r)= rho_s/[(r/r_s)^{gamma} [1+(r/r_s)^{alpha}]^{(beta-gamma)/alpha}].
   //
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_s: density normalisation [Msol/kpc^3]
   //  par[1]        r_s: scale radius [kpc]
   //  par[2]        alpha: transition slope
   //  par[3]        beta: outer slope
   //  par[4]        gamma: inner slope


   // Required to avoid divergences at r=0
   if (par[4] > 1.e-5) {
      if (r > par[1] * pow(par[0] / gDM_RHOSAT, 1. / par[4])) {
         return par[0] * pow(par[1] / r, par[4])
                / pow(1. + pow(r / par[1], par[2]), (par[3] - par[4]) / par[2]);
      } else
         return gDM_RHOSAT;
   } else
      return par[0] / pow(1. + pow(r / par[1], par[2]), par[3] / par[2]);
}

//______________________________________________________________________________
void rho(double &r, double par[6], double &res)
{
   //--- Sets res = Density profile [Msol/kpc^3].
   //
   // INPUTS:
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        Normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[1]        Scale radius [kpc]
   //  par[2]        Shape parameter #1
   //  par[3]        Shape parameter #2
   //  par[4]        Shape parameter #3
   //  par[5]        card_profile [gENUM_PROFILE]
   // OUTPUT:
   //  res           Value of rho(r)

   int profile = (int)par[5];
   // Calculate rho(r)
   switch (profile) {
      case kISHIYAMA14:
      case kZHAO:
         res = rho_ZHAO(r, par);
         break;
      case kEINASTO:
         res = rho_EINASTO(r, par);
         break;
      case kEINASTO_N:
         res = rho_EINASTO_N(r, par);
         break;
      case kBURKERT:
         res = rho_BURKERT(r, par);
         break;
      case kDPDV_GAO04:
         res = rho_DPDV_GAO04(r, par);
         break;
      case kDPDV_SIGMOID_EINASTO:
         res = rho_DPDV_SIGMOID_EINASTO(r, par);
         break;
      case kDPDV_SPRINGEL08_ANTIBIASED:
         res = rho_DPDV_SPRINGEL08_ANTIBIASED(r, par);
         break;
      case kDPDV_SPRINGEL08_FIT:
         res = rho_DPDV_SPRINGEL08_FIT(r, par);
         break;
      case kDPDV_PIERI11:
         res = rho_DPDV_PIERI11(r, par);
         break;
      case kNODES:
         res = rho_NODES(r, par);
         break;
      default :
         printf("\n====> ERROR: rho() in profiles.cc");
         printf("\n             card_profile=%d does not correspond to any DM profile", (int)par[5]);
         printf("\n             => abort()\n\n");
         abort();
   }

   if (res >= gDM_RHOSAT)
      res = gDM_RHOSAT;
   return;
}

//______________________________________________________________________________
void rho_mix(double &r, double par[21], double &res)
{
   //--- Sets res = Density profile [Msol/kpc^3].
   //
   // INPUTS:
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_1(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[1]        rho_1(r) scale radius [kpc]
   //  par[2]        rho_1(r) shape parameter #1
   //  par[3]        rho_1(r) shape parameter #2
   //  par[4]        rho_1(r) shape parameter #3
   //  par[5]        rho_1(r) card_profile [gENUM_PROFILE]
   //  par[6]        UNUSED
   //  par[7]        UNUSED
   //  par[8]        UNUSED
   //  par[9]        UNUSED
   //  par[10]       switch_rho - selects which combination of rho_1 and rho_2 to use
   //                        0 -> rho(r) = rho1(r)
   //                        1 -> rho(r) = rho1(r)-rho2(r)
   //                        2 -> rho(r) = rho1(r)*rho2(r)
   //  par[11]       rho_2(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[12]       rho_2(r) scale radius [kpc]
   //  par[13]       rho_2(r) shape parameter #1
   //  par[14]       rho_2(r) shape parameter #2
   //  par[15]       rho_2(r) shape parameter #3
   //  par[16]       rho_2(r) card_profile [gENUM_PROFILE]
   //  par[17]       UNUSED
   //  par[18]       UNUSED
   //  par[19]       UNUSED
   //  par[20]       UNUSED
   // OUTPUT:
   //  res           Value of rho(r)

   int switch_rho = (int)par[10];

   // Calculate rho1(r)
   double res_first = 0.;
   rho(r, par, res_first);
   res = res_first;

   // Check combo and calculate rho2 if necessary
   if (switch_rho == 0) {
      return;
   } else {
      double res_second = 0.;
      rho(r, &par[11], res_second);

      if (switch_rho == 1) {
         // check for inner profile:
         double innerslope1;
         double innerslope2;
         double r_check = 1e-40 * par[1];
         dlog_rho_dlog_r(r_check, par, innerslope1);
         dlog_rho_dlog_r(r_check, &par[11], innerslope2);
         if (innerslope2 - innerslope1 < - SMALL_NUMBER) {
            printf("\n====> ERROR: rho_mix() in profiles.cc");
            printf("\n             dP/dV profile steeper than total halo profile for r->0:");
            printf("\n             d(log rho_<subs>)/d(log r) = %.4g, d(log rho_tot)/d(log r) = %.4g", round(innerslope2, 3), round(innerslope1, 3));
            printf("\n             Assure that dP/dV logarithmic slope is always");
            printf("\n             equal/smaller than total halo slope for r->0.\n");
            printf("\n             => abort()\n\n");
            printf("\n             The following halo profile parameters may help to find the issue:\n\n");
            print_parmix(par);
            abort();
         }
         res  -= res_second;
         if (res < 0.) {
            // we do not throw an error if the conflict arises at radii much larger
            // than the scale radius, where the fluxes are negligible, and so is the
            // error arising from obtaining a negative density. Otherwise, crash if
            // negative densities are produced.
            string message = "\n             Negative smooth density obtained:"
                             "\n             Your choice of mass distributed in subhalos is in conflict"
                             "\n             with maintaining your defined total DM halo profile."
                             "\n             The following quantities influence this issue: \n"
                             "\n             - Combination of total & dP/dV profile."
                             "\n             - Subclustered mass m_subs = f_subs x M_tot."
                             "\n             - R_Delta, the physical size of the halo\n"
                             "\n       Advice: Use the following parameters of the halo"
                             "\n       which caused the error for plotting the halo densities in "
                             "\n       the -h1 mode to visualize the issue:\n";

            if (r < 10 * par[1]) {
               print_error("profiles.cc", "rho_mix()", message, false);
               printf("\n");
               print_parmix(par);
               abort();
            } else {
               // print info only once:
               static bool is_negdens_warning = true;
               if (is_negdens_warning == true) {
                  print_warning("profiles.cc", "rho_mix()", message);
                  print_parmix(par);
                  is_negdens_warning = false;
               }
            }
         }
         return;
      } else if (switch_rho == 2) {
         res  *= res_second;
         return;
      } else {
         printf("\n====> ERROR: rho_mix() in profiles.cc");
         printf("\n             switch_rho=%d does not correspond to any combo for rho_1 and rho_2", (int)par[1]);
         printf("\n             => abort()\n\n");
         abort();
      }
   }
   return;
}

//______________________________________________________________________________
void rho2(double &r, double par[6], double &res)
{
   //--- Sets res = Density square of the profile [Msol^2/kpc^6].
   //
   // INPUTS:
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        Normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[1]        Scale radius [kpc]
   //  par[2]        Shape parameter #1
   //  par[3]        Shape parameter #2
   //  par[4]        Shape parameter #3
   //  par[5]        card_profile [gENUM_PROFILE]
   // OUTPUT:
   //  res           Value of rho^2(r)

   rho(r, par, res);
   res *= res;
   return;
}

//______________________________________________________________________________
void rho2_mix(double &r, double par[21], double &res)
{
   //--- Sets res = Density square of the profile [Msol^2/kpc^6].
   //
   // INPUTS:
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_1(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[1]        rho_1(r) scale radius [kpc]
   //  par[2]        rho_1(r) shape parameter #1
   //  par[3]        rho_1(r) shape parameter #2
   //  par[4]        rho_1(r) shape parameter #3
   //  par[5]        rho_1(r) card_profile [gENUM_PROFILE]
   //  par[6]        UNUSED
   //  par[7]        UNUSED
   //  par[8]        UNUSED
   //  par[9]        UNUSED
   //  par[10]       switch_rho - selects which combination of rho_1 and rho_2 to use
   //                        0 -> rho(r) = rho1(r)
   //                        1 -> rho(r) = rho1(r)-rho2(r)
   //                        2 -> rho(r) = rho1(r)*rho2(r)
   //  par[11]       rho_2(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[12]       rho_2(r) scale radius [kpc]
   //  par[13]       rho_2(r) shape parameter #1
   //  par[14]       rho_2(r) shape parameter #2
   //  par[15]       rho_2(r) shape parameter #3
   //  par[16]       rho_2(r) card_profile [gENUM_PROFILE]
   //  par[17]       UNUSED
   //  par[18]       UNUSED
   //  par[19]       UNUSED
   //  par[20]       UNUSED
   // OUTPUT:
   //  res           Value of rho^2(r)

   rho_mix(r, par, res);
   res *= res;
   return;
}

//______________________________________________________________________________
void r2rho(double &r, double par[6], double &res)
{
   //--- Sets res = r^2 * Density of the profile [Msol/kpc].
   //
   // INPUTS:
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        Normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[1]        Scale radius [kpc]
   //  par[2]        Shape parameter #1
   //  par[3]        Shape parameter #2
   //  par[4]        Shape parameter #3
   //  par[5]        card_profile [gENUM_PROFILE]
   // OUTPUT:
   //  res           Value of r^2*rho(r)

   rho(r, par, res);
   res *= (r * r);
   return;
}

//______________________________________________________________________________
void r3rho(double &r, double par[6], double &res)
{
   //--- Sets res = r^3 * Density of the profile [Msol/kpc].
   //
   // INPUTS:
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        Normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[1]        Scale radius [kpc]
   //  par[2]        Shape parameter #1
   //  par[3]        Shape parameter #2
   //  par[4]        Shape parameter #3
   //  par[5]        card_profile [gENUM_PROFILE]
   // OUTPUT:
   //  res           Value of r^3*rho(r)

   rho(r, par, res);
   res *= (r * r * r);
   return;
}

//______________________________________________________________________________
void r2rho_mix(double &r, double par[21], double &res)
{
   //--- Sets res = r^2 * Density of the profile [Msol/kpc].
   //
   // INPUTS:
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_1(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[1]        rho_1(r) scale radius [kpc]
   //  par[2]        rho_1(r) shape parameter #1
   //  par[3]        rho_1(r) shape parameter #2
   //  par[4]        rho_1(r) shape parameter #3
   //  par[5]        rho_1(r) card_profile [gENUM_PROFILE]
   //  par[6]        UNUSED
   //  par[7]        UNUSED
   //  par[8]        UNUSED
   //  par[9]        UNUSED
   //  par[10]       switch_rho - selects which combination of rho_1 and rho_2 to use
   //                        0 -> rho(r) = rho1(r)
   //                        1 -> rho(r) = rho1(r)-rho2(r)
   //                        2 -> rho(r) = rho1(r)*rho2(r)
   //  par[11]       rho_2(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[12]       rho_2(r) scale radius [kpc]
   //  par[13]       rho_2(r) shape parameter #1
   //  par[14]       rho_2(r) shape parameter #2
   //  par[15]       rho_2(r) shape parameter #3
   //  par[16]       rho_2(r) card_profile [gENUM_PROFILE]
   //  par[17]       UNUSED
   //  par[18]       UNUSED
   //  par[19]       UNUSED
   //  par[20]       UNUSED
   // OUTPUT:
   //  res           Value of r^2*rho(r)

   rho_mix(r, par, res);
   res *= (r * r);
   return;
}

//______________________________________________________________________________
void r2rho2(double &r, double par[6], double &res)
{
   //--- Sets res = r^2 * Density square of the profile [Msol^2/kpc^4].
   //
   // INPUTS:
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        Normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[1]        Scale radius [kpc]
   //  par[2]        Shape parameter #1
   //  par[3]        Shape parameter #2
   //  par[4]        Shape parameter #3
   //  par[5]        card_profile [gENUM_PROFILE]
   // OUTPUT:
   //  res           Value of r^2*rho^2(r)


   rho(r, par, res);
   res *= (res * r * r);
   return;
}

//______________________________________________________________________________
void r2rho2_mix(double &r, double par[21], double &res)
{
   //--- Sets res = r^2 * Density square of the profile [Msol^2/kpc^4].
   //
   // INPUTS:
   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        rho_1(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[1]        rho_1(r) scale radius [kpc]
   //  par[2]        rho_1(r) shape parameter #1
   //  par[3]        rho_1(r) shape parameter #2
   //  par[4]        rho_1(r) shape parameter #3
   //  par[5]        rho_1(r) card_profile [gENUM_PROFILE]
   //  par[6]        UNUSED
   //  par[7]        UNUSED
   //  par[8]        UNUSED
   //  par[9]        UNUSED
   //  par[10]       switch_rho - selects which combination of rho_1 and rho_2 to use
   //                        0 -> rho(r) = rho1(r)
   //                        1 -> rho(r) = rho1(r)-rho2(r)
   //                        2 -> rho(r) = rho1(r)*rho2(r)
   //  par[11]       rho_2(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[12]       rho_2(r) scale radius [kpc]
   //  par[13]       rho_2(r) shape parameter #1
   //  par[14]       rho_2(r) shape parameter #2
   //  par[15]       rho_2(r) shape parameter #3
   //  par[16]       rho_2(r) card_profile [gENUM_PROFILE]
   //  par[17]       UNUSED
   //  par[18]       UNUSED
   //  par[19]       UNUSED
   //  par[20]       UNUSED
   // OUTPUT:
   //  res           Value of r^2*rho^2(r)

   rho_mix(r, par, res);
   res *= (res * r * r);
   return;
}

//______________________________________________________________________________
double get_riso_triaxial(double x[3], double semi_axes[3], double euler_deg[3])
{
   //--- Returns R = sqrt( (X/a)^2 + (Y/b)^2 + (Z/c)^2) to be used for triaxial
   //    halos, where (a,b,c) are the semi-axes of the triaxial DM halo. The
   //    radius R corresponds to iso-density contours of the triaxial halo
   //    (which can then be described by a standard spherical profile).
   //
   //  x[3]          X,Y,Z in the xyz_halo or xyz_gal (gal. halo) coordinates (see geometry.h) [kpc]
   //  semi_axes[3]  Major(a)/second(b)/minor(c) axes (major/minor along x/z axis)
   //  euler_deg[3]  Euler rotation angle (alpha=[-180,180],beta=[-90,90],gamma=[-1800,180]) [deg]

   double r = 0.;

   // Calculate (X,Y,Z) from (x,y,z) after Euler rotations
   euler_rotation(x, euler_deg);

   // Calculate effective radius
   for (int i = 0; i < 3; ++i)
      r += x[i] * x[i] / (semi_axes[i] * semi_axes[i]);
   r = sqrt(r);

   return r;
}

//______________________________________________________________________________
void set_par0_given_mref(double par[6], double const &r_ref, double const &m_ref, double const &eps)
{
   //--- Returns DM density normalisation [Msol/kpc^3] matching the constraint M(r<r_ref)=m_ref.
   //
   // INPUTS:
   //  par[0]        UNUSED
   //  par[1]        Scale radius [kpc]
   //  par[2]        Shape parameter #1
   //  par[3]        Shape parameter #2
   //  par[4]        Shape parameter #3
   //  par[5]        card_profile [gENUM_PROFILE]
   //  r_ref         Reference radius [kpc]
   //  m_ref         Mass [Msol] within the radius r_ref
   //  eps           Relative precision sought for integration
   // OUTPUT:
   //  par[0]        Dark matter density normalisation [Msol/kpc^3]

   const int npar = 7;
   double par_tmp[npar] = {1., par[1], par[2], par[3], par[4], par[5], r_ref};
   par[0] = m_ref / mass_singlehalo(par_tmp, eps);
}

//______________________________________________________________________________
void set_par0_given_rhoref(double par[6], double &r_ref, double const &rho_ref)
{
   //--- Sets normalisation par[0] such that rho(r_ref)=rho_ref.
   //
   // INPUTS:
   //  par[0]        UNUSED
   //  par[1]        Scale radius [kpc]
   //  par[2]        Shape parameter #1
   //  par[3]        Shape parameter #2
   //  par[4]        Shape parameter #3
   //  par[5]        card_profile [gENUM_PROFILE]
   //  r_ref         Reference radius [kpc]
   //  rho_ref       Density at r=r_ref [Msol/kpc^3]
   // OUTPUT:
   //  par[0]        Dark matter density normalisation [Msol/kpc^3]

   // do nothing for kNODES and rho_ref = -1
   if (par[5] == kNODES and rho_ref < 0) {
      par[0] = gHALO_NODES_INPUT_RHOSCALE[int(par[2])];
      par[1] = gHALO_NODES_INPUT_RSCALE[int(par[2])];
      return;
   }

   // Calculate normalisation
   double res = 0.;
   par[0] = 1.;
   rho(r_ref, par, res);
   par[0] = rho_ref / res;
   return;
}

//______________________________________________________________________________
void set_rhonodes(const vector<double> &r_vec, const vector<double> &rho_vec,
                  const int i_halo, const double r0, int card_interpol_type,
                  bool is_verbose)
{
   //--- Fills the global variables gHALO_NODES_X_GRID, gHALO_NODES_Y_GRID,
   //     gHALO_NODES_INTERPOLTYPE for rho_NODES() profile
   //
   // INPUTS:
   //  r_vec         Vector with radial coordinates [kpc]
   //  rho_vec       Vector with values at r coordinates (arb. units)
   //  r0            scale radius [kpc]. This is used to allow
   //                for (re-)scaling of the input data (optional).
   //  card_interpol_type interpolation type out of gENUM_INTERPOLTYPE (optional)
   // OUTPUT:
   //  gHALO_NODES_X_GRID in units of x = r/r0
   //  gHALO_NODES_Y_GRID such that y(x) = 1
   //  gHALO_NODES_INTERPOLTYPE

   double n_nodes = r_vec.size();

   if (i_halo < gHALO_NODES_X_GRID.size()) {
      vector<double>().swap(gHALO_NODES_X_GRID[i_halo]);
      gHALO_NODES_X_GRID[i_halo].reserve(n_nodes);
      vector<double>().swap(gHALO_NODES_Y_GRID[i_halo]);
      gHALO_NODES_Y_GRID[i_halo].reserve(n_nodes);
   } else if (i_halo == gHALO_NODES_X_GRID.size()) {
      vector<double> x_grid;
      x_grid.reserve(n_nodes);
      gHALO_NODES_X_GRID.push_back(x_grid);
      vector<double> y_grid;
      y_grid.reserve(n_nodes);
      gHALO_NODES_Y_GRID.push_back(y_grid);
      gHALO_NODES_INTERPOLTYPE.push_back(-1);
      gHALO_NODES_RATIO_R_2_RSCALE.push_back(-1);
      gHALO_NODES_INPUT_RSCALE.push_back(-1);
      gHALO_NODES_INPUT_RHOSCALE.push_back(-1);
   } else {
      print_error("profiles.cc", "set_rhonodes()", "i_halo node index larger than "
                  "lengths of global node array + 1");
   }

   if (n_nodes != rho_vec.size())
      print_error("profiles.cc", "set_rhonodes()", "Lengths of x and y vectors must be equal");

   // find best interpolation type
   string interpol_str = "";
   if (card_interpol_type == -1) {
      vector<double> x_diff(n_nodes - 1);
      vector<double> y_diff(n_nodes - 1);
      vector<double> log_x_diff(n_nodes - 1);
      vector<double> log_y_diff(n_nodes - 1);
      for (int i = 0; i < int(n_nodes - 1); i++) {
         x_diff[i] = r_vec[i + 1] - r_vec[i];
         y_diff[i] = rho_vec[i + 1] - rho_vec[i];
         log_x_diff[i] = log(r_vec[i + 1] / r_vec[i]);
         log_y_diff[i] = log(rho_vec[i + 1] / rho_vec[i]);
      }
      double x_diff_mean = accumulate(x_diff.begin(), x_diff.end(), 0.0) / x_diff.size();
      double y_diff_mean = accumulate(y_diff.begin(), y_diff.end(), 0.0) / y_diff.size();
      double log_x_diff_mean = accumulate(log_x_diff.begin(), log_x_diff.end(), 0.0) / log_x_diff.size();
      double log_y_diff_mean = accumulate(log_y_diff.begin(), log_y_diff.end(), 0.0) / log_y_diff.size();
      double x_diff_var = 0;
      double y_diff_var = 0;
      double log_x_diff_var = 0;
      double log_y_diff_var = 0;
      for (int i = 0; i < int(n_nodes - 1); i++) {
         x_diff_var += pow(x_diff[i] - x_diff_mean, 2);
         y_diff_var += pow(y_diff[i] - y_diff_mean, 2);
         log_x_diff_var += pow(log_x_diff[i] - log_x_diff_mean, 2);
         log_y_diff_var += pow(log_y_diff[i] - log_y_diff_mean, 2);
      }
      x_diff_var /= ((n_nodes - 2) * pow(x_diff_mean, 2));
      y_diff_var /= ((n_nodes - 2) * pow(y_diff_mean, 2));
      log_x_diff_var /= ((n_nodes - 2) * pow(log_x_diff_mean, 2));
      log_y_diff_var /= ((n_nodes - 2) * pow(log_y_diff_mean, 2));
      string interpol_name;
      if (x_diff_var < log_x_diff_var) {
         if (y_diff_var < log_y_diff_var) {
            card_interpol_type = kLINLIN;
            interpol_name = "lin-lin";
         } else {
            card_interpol_type = kLINLOG;
            interpol_name = "lin-log";
         }
      } else {
         if (y_diff_var < log_y_diff_var) {
            card_interpol_type = kLOGLIN;
            interpol_name = "log-lin";
         } else {
            card_interpol_type = kLOGLOG;
            interpol_name = "log-log";
         }
      }
      interpol_str = "Choose " + interpol_name + " interpolation";
   }

   gHALO_NODES_INTERPOLTYPE[i_halo] = card_interpol_type;

   for (int i = 0; i < int(r_vec.size()); i++) {
      gHALO_NODES_X_GRID[i_halo].push_back(r_vec[i]);
      gHALO_NODES_Y_GRID[i_halo].push_back(rho_vec[i]);
   }

   // find r_2:
   gsl_function F;
   F.function = &rho_NODES_find_r_2;
   structHaloProfile params;
   params.rhos = 1;
   params.rscale =  1;
   params.shapeParam1 = i_halo;
   F.params = &params;
   double rmin = (1 + gSIM_EPS) * gHALO_NODES_X_GRID[i_halo][0];
   double rmax = (1 - gSIM_EPS) * gHALO_NODES_X_GRID[i_halo][gHALO_NODES_X_GRID[i_halo].size() - 1];
   int return_status = 1;

   double r_2 = rootsolver_gsl(gsl_root_fsolver_brent, F, rmin, rmax, gSIM_EPS, return_status);
   if (return_status != 0) {
      char tmp[256];
      sprintf(tmp, "r-2 could not be found by GSL. Radial nodes must be provided "
              "for numeric halo #%d around r_2, where d(log rho(r) ) / d(log r) = -2", i_halo);
      print_error("profiles.cc", "set_rhonodes()", string(tmp));
   }

   // normalize and set gHALO_NODES_RATIO_R_2_RSCALE:
   double r_s;
   if (r0 == -1) r_s = r_2;
   else r_s = r0;
   // find y value at r_2:
   double rho_s = interp1D(r_s, gHALO_NODES_X_GRID[i_halo], gHALO_NODES_Y_GRID[i_halo], card_interpol_type, true);

   if (rho_s == HPX_BLIND_VALUE) {
      char tmp[256];
      sprintf(tmp, "Did you set a user-defined via optional gLIST_HALOES_NODES_RSCALE?"
              " Radial nodes must be provided for halo #%d around user-defined r_s", i_halo);
      print_error("profiles.cc", "set_rhonodes()", string(tmp));
   }

   // store absolute unnormalized values in case user wants to use later:
   gHALO_NODES_INPUT_RSCALE[i_halo] = r_s;
   gHALO_NODES_INPUT_RHOSCALE[i_halo] = rho_s;

   // set ratio r_s/r-2:
   gHALO_NODES_RATIO_R_2_RSCALE[i_halo] = r_2 / r_s;
   // normalize
   for (int i = 0; i < int(r_vec.size()); i++) {
      gHALO_NODES_X_GRID[i_halo][i] /= r_s;
      gHALO_NODES_Y_GRID[i_halo][i] /= rho_s;
   }
   if (is_verbose) {
      if (r0 != -1)
         printf("      Set rho_nodes halo #%d: %s, r-2/r_s = %g\n", i_halo, interpol_str.c_str(), gHALO_NODES_RATIO_R_2_RSCALE[i_halo]);
      else
         printf("      Set rho_nodes halo #%d: %s, r-2/r_s == 1 fixed\n", i_halo, interpol_str.c_str());
   }
}

//______________________________________________________________________________
void set_rhonodes_fromfile()
{
   vector< vector<double> > node_data = read_ascii(gLIST_HALOES_NODES, 2, false);
   int n_nodes_tot = node_data.size();
   if (n_nodes_tot == 0)
      print_error("profiles.cc", "set_rhonodes_fromfile()", "Empty list found.");
   int n_objects = 1;
   vector<int> n_nodes_ihalo;
   int i_nodes = 0;
   for (int i = 1; i < int(n_nodes_tot); i++) {
      if (node_data[i][0] - node_data[i - 1][0] <= 0) {
         n_objects++;
         n_nodes_ihalo.push_back(i_nodes + 1);
         i_nodes = 0;
         continue;
      }
      i_nodes++;
   }
   n_nodes_ihalo.push_back(i_nodes + 1);

   printf(">>>>> Load file %s, found %d objects\n", gLIST_HALOES_NODES.c_str(), n_objects);

   // evaluate gLIST_HALOES_NODES_RSCALE input:
   vector<double> haloes_nodes_rscale(n_objects, -1);

   if (gLIST_HALOES_NODES_RSCALE != "-1") {
      vector<double> haloes_nodes_rscale_tmp;
      string2list(gLIST_HALOES_NODES_RSCALE, ",", haloes_nodes_rscale_tmp);
      if (haloes_nodes_rscale_tmp.size() < n_objects) {
         print_warning("profiles.cc", "set_rhonodes_fromfile()", "gLIST_HALOES_NODES_RSCALE "
                       "contains less entries as halo objects provided in "
                       "gLIST_HALOES_NODES. Will set r-2/r_scale = 1 for missing objects.");
         for (int i = 0; i < int(haloes_nodes_rscale_tmp.size()); i++) {
            haloes_nodes_rscale[i] = haloes_nodes_rscale_tmp[i];
         }
      } else if (haloes_nodes_rscale_tmp.size() >= n_objects) {
         for (int i = 0; i < int(n_objects); i++) {
            haloes_nodes_rscale[i] = haloes_nodes_rscale_tmp[i];
         }
         if (haloes_nodes_rscale_tmp.size() > n_objects) {
            print_warning("profiles.cc", "set_rhonodes_fromfile()", "gLIST_HALOES_NODES_RSCALE "
                          "contains more entries as halo objects provided in "
                          "gLIST_HALOES_NODES. Will ignore unneeded entries.");
         }
      }

   }

   // Separate the objects and fill gHALO_NODES_X_GRID/gHALO_NODES_Y_GRID:
   int j_start = 0;
   for (int i = 0; i < int(n_objects); i++) {
      if (n_nodes_ihalo[i] < 4) {
         char tmp[256];
         sprintf(tmp, "Only %d nodes found for halo #%d. Require at least 4 nodes per object", n_nodes_ihalo[i], i + 1);
         print_error("profiles.cc", "set_rhonodes_fromfile()", string(tmp));
      }
      vector<double> x_val;
      x_val.reserve(n_nodes_ihalo[i]);
      vector<double> y_val;
      y_val.reserve(n_nodes_ihalo[i]);
      for (int j = 0; j < int(n_nodes_ihalo[i]); j++) {
         if (node_data[j_start + j][1] > SMALL_NUMBER) {
            x_val.push_back(node_data[j_start + j][0]);
            y_val.push_back(node_data[j_start + j][1]);
         }
      }
      j_start += n_nodes_ihalo[i];

      // Fill gHALO_NODES_X_GRID/gHALO_NODES_Y_GRID, gHALO_NODES_INTERPOLTYPE, gHALO_NODES_RATIO_R_2_RSCALE:
      set_rhonodes(x_val, y_val, i + 1, haloes_nodes_rscale[i]);
      vector<double>().swap(x_val);
      vector<double>().swap(y_val);
   }
   printf("\n");
}
