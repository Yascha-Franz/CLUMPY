/*! \file params.cc{  \brief (see params.h) */

// CLUMPY includes
#include "../include/clumps.h"
#include "../include/inlines.h"
#include "../include/janalysis.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/spectra.h"
#include "../include/geometry.h"
#include "../include/profiles.h"

// C++ std libraries
using namespace std;
#include <fstream>
#include <iostream>

//------------------------------------------------------------------------------
// Global variables for names of keywords
const char    gNAMES_ABSORPTIONPROFILE[gN_ABSORPTIONPROFILE][50] = {"NOEBL", "FRANCESCHINI08", "FRANCESCHINI17", "FINKE10", "DOMINGUEZ11_REF", "DOMINGUEZ11_LO", "DOMINGUEZ11_UP", "GILMORE12_FIDUCIAL", "GILMORE12_FIXED", "INOUE13_REF", "INOUE13_LO", "INOUE13_UP"};
const char    gNAMES_ANISOTROPYPROFILE[gN_ANISOTROPYPROFILE][50] = {"CONSTANT", "BAES", "OSIPKOV"};
const char    gNAMES_LIGHTPROFILE[gN_LIGHTPROFILE][50] = {"EXP2D", "EXP3D", "KING2D", "PLUMMER2D", "SERSIC2D", "ZHAO3D"};
const char    gNAMES_CDELTAMDELTA[gN_CDELTAMDELTA][50]         = {"B01_VIR", "B01_VIR_RAD", "ENS01_VIR", "NETO07_200", "DUFFY08F_VIR", "DUFFY08F_200", "DUFFY08F_MEAN", "ETTORI10_200", "PRADA12_200", "GIOCOLI12_VIR", "PIERI11_VIALACTEA", "PIERI11_AQUARIUS", "SANCHEZ14_200", "MOLINE17_200", "LUDLOW16_200", "CORREA15_PLANCK_200", "ROCHA13_SIDM_VIR"};
const char    gNAMES_CVIR_DIST[gN_CVIR_DIST][50]       = {"DIRAC", "LOGNORM"};
const char    gNAMES_FINALSTATE[gN_FINALSTATE][50]     = {"GAMMA", "NEUTRINO", "ANTIPROTON", "POSITRON", "ELECTRON"};
const char    gNAMES_MASSFUNCTION[gN_MASSFUNCTION][50] = {"TINKER08", "TINKER08_N", "TINKER10", "BOCQUET16_HYDRO", "BOCQUET16_DMONLY", "JENKINS01",  "SHETHTORMEN99", "PRESSSCHECHTER74", "RODRIGUEZPUEBLA16_PLANCK"};
const char    gNAMES_NUFLAVOUR[gN_NUFLAVOUR][50]       = {"NUE", "NUMU", "NUTAU"};
const char    gNAMES_PP_SPECTRUMMODEL[gN_PP_SPECTRUMMODEL][50] = {"BERGSTROM98", "TASITSIOMI02", "BRINGMANN08", "CIRELLI11_EW", "CIRELLI11_NOEW"};
const char    gNAMES_PROFILE[gN_PROFILE][50]           = {"EINASTO", "EINASTO_N", "ZHAO", "BURKERT", "DPDV_GAO04", "DPDV_SIGMOID_EINASTO", "DPDV_SPRINGEL08_ANTIBIASED", "DPDV_SPRINGEL08_FIT", "DPDV_PIERI11", "ISHIYAMA14", "NODES"};
const char    gNAMES_TYPEHALOES[gN_TYPEHALOES][50]     = {"DSPH", "GALAXY", "CLUSTER", "EXTRAGAL"};
const char    gNAMES_PP_BR[gN_PP_BR][50]               = {
   "e_{L}^{+}e_{L}^{-}",       "e_{R}^{+}e_{R}^{-}",       "e^{+}e^{-}",
   "#mu_{L}^{+}#mu_{L}^{-}",   "#mu_{R}^{+}#mu_{R}^{-}",   "#mu^{+}#mu^{-}",
   "#tau_{L}^{+}#tau_{L}^{-}", "#tau_{R}^{+}#tau_{R}^{-}", "#tau^{+}#tau^{-}",
   "q#bar{q}",        "c#bar{c}",        "b#bar{b}",       "t#bar{t}",
   "W_{L}^{+}W_{L}^{-}",       "WT^{+}WT^{-}",             "W^{+}W^{-}",
   "Z_{L}Z_{L}",               "Z_{T}Z_{T}",               "ZZ",
   "gluglu",                   "#gamma#gamma",             "hh",
   "#nu_{e}#nu_{e}",           "#nu_{#mu}#nu_{#mu}",       "#nu_{#tau}#nu_{#tau}",
   "VV4_{e}",                  "VV4_{#mu}",                "VV4_{#tau}"
};
const char    gNAMES_SIMUMODES[gN_SIMUMODES][50]      = {
   "g0", "g1", "g2", "g3", "g4", "g5", "g6", "g7", "g8",
   "h0", "h1", "h2", "h3", "h4", "h5", "h6", "h7", "h8", "h9", "h10",
   "e0", "e1", "e2", "e3", "e4", "e5", "e6",
   "s0", "s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10", "s11", "s12",
   "o1", "o2",
   "z",
   "f"
};
const char    gNAMES_WINDOWFUNC[gN_WINDOWFUNC][50]    = {"TOP_HAT", "GAUSS", "SHARP_K"};
const char    gNAMES_DELTA_REF[gN_DELTA_REF][50]      = {"RHO_CRIT", "RHO_MEAN", "BRYANNORMAN98"};
const char    gNAMES_GROWTHFACTOR[gN_GROWTHFACTOR][50] = {"PKZ_FROMFILE", "CARROLL92", "HEATH77"};

//------------------------------------------------------------------------------
// Global variables for the selection of parametrisations and key parameters
string        gCLUMPY_VERSION = "v3.1";

// all parameters are set to their default values and updated by the user input.
// See Huetten et al. (2016) for their motivation.

// Cosmology related
double        gCOSMO_HUBBLE =                                                   0.678;  // Planck 2015 TT+lowP+lensing
double        gCOSMO_OMEGA0_M =                                                 0.308;  // Planck 2015 TT+lowP+lensing
double        gCOSMO_OMEGA0_B =                                                 0.0486; // Planck 2015
double        gCOSMO_OMEGA0_K =                                                 0.;     //  Planck 2015, fixed.
double        gCOSMO_SIGMA8 =                                                   0.815;  // Planck 2015 TT+lowP+lensing
double        gCOSMO_N_S =                                                      0.968;  // Planck 2015 TT+lowP+lensing
double        gCOSMO_TAU_REIO =                                                 0.066;  // Planck 2015 TT+lowP+lensing
double        gCOSMO_WDE =                                                      -1;     // Planck 2015 TT+lowP+lensing
double        gCOSMO_T0 =                                                       2.7255; // Planck 2015 [K].
double        gCOSMO_DELTA0 =                                                   200;
int           gCOSMO_FLAG_DELTA_REF =                                           kRHO_CRIT;

// - no input parameters
double        gCOSMO_RHO0_C =                                                   RHO_CRITperh2_MSOLperKPC3 * pow(gCOSMO_HUBBLE, 2.); // will be updated at run time in load_params()
double        gCOSMO_OMEGA0_R = (1. + 3.046 * 7. / 8. * pow(4. / 11., 4. / 3.)) * RADIATION_CONSTANT * pow(gCOSMO_T0, 4) / gCOSMO_RHO0_C; // will be updated at run time in load_params()
double        gCOSMO_OMEGA0_LAMBDA =                                            1 - gCOSMO_OMEGA0_M - gCOSMO_OMEGA0_K;  // Planck 2015 TT+lowP+lensing, will be updated at run time in load_params()
double        gCOSMO_OMEGA0_CDM =                                               gCOSMO_OMEGA0_M - gCOSMO_OMEGA0_B; // will be updated at run time in load_params()
double        gCOSMO_M_to_MH =                                                  gCOSMO_HUBBLE / gCOSMO_OMEGA0_M; // will be updated at run time in load_params()
vector<double>gCOSMO_Z_GRID;                                                    // will be initialized at run time in init_extragal()
vector<double>gCOSMO_MH_GRID;                                                   // will be initialized at run time in init_extragal()
vector<double>gCOSMO_D_TRANS_GRID;                                              // will be initialized at run time in init_extragal()
vector<double>gCOSMO_D_LUM_GRID;                                                // will be initialized at run time in init_extragal()
vector<double>gCOSMO_D_ANG_GRID;                                                // will be initialized at run time in init_extragal()
vector<vector<double> > gCOSMO_DNDVHDLNMH_Z;                                    // will be initialized at run time in init_extragal()

// Dark matter related
double        gDM_LOGCDELTA_STDDEV =                                            0.;
double        gDM_SUBS_MMIN =                                                   1.e-6;
double        gDM_SUBS_MMAXFRAC =                                               1.e-2;
int           gDM_SUBS_NUMBEROFLEVELS =                                         1;
double        gDM_RHOSAT =                                                      1.e19;
double        gDM_RHOHALOES_TO_RHOMEAN =                                        1.;
bool          gDM_IS_IDM =                                                      false;
double        gDM_KMAX =                                                        -1;
double        gDM_KMAX_SIGMA_CUTOFF =                                            0.5;
// - no input parameters
int           gDM_FLAG_CDELTA_DIST =                                            kDIRAC; // will be updated at run time in load_params()


// Galaxy (Milky-way) related
double        gMW_RHOSOL =                                                      0.4 * GEVperCM3_to_MSOLperKPC3;
double        gMW_RSOL =                                                        8.;
double        gMW_RMAX =                                                        260.;
int           gMW_SUBS_FLAG_PROFILE =                                           kHOST;
double        gMW_SUBS_SHAPE_PARAMS[gN_SHAPE_PARAMS] =                          {kHOST, kHOST, kHOST};
int           gMW_SUBS_FLAG_CDELTAMDELTA =                                      kMOLINE17_200;
int           gMW_SUBS_DPDV_FLAG_PROFILE =                                      kHOST;
double        gMW_SUBS_DPDV_RSCALE_TO_RS_HOST =                                 13.14399;
double        gMW_SUBS_DPDV_SHAPE_PARAMS[gN_SHAPE_PARAMS] =                     {0.68, kHOST, kHOST};
double        gMW_SUBS_DPDM_SLOPE =                                             1.9;
double        gMW_SUBS_M1 =                                                     1.e8;
double        gMW_SUBS_M2 =                                                     1.e10;
int           gMW_SUBS_N_INM1M2 =                                               150;
bool          gMW_SUBS_TABULATED_IS =                                           false;
string        gMW_SUBS_TABULATED_CMIN_OF_R =                                    "$CLUMPY/data/subhalos_Stref17/cmin_core_eps1.dat";
string        gMW_SUBS_TABULATED_LCRIT =                                        "$CLUMPY/data/subhalos_Stref17/l_crit_core_eps1.dat";
string        gMW_SUBS_TABULATED_RTIDAL_TO_RS =                                 "$CLUMPY/data/subhalos_Stref17/rt_over_rs_core.dat";
int           gMW_TOT_FLAG_PROFILE =                                            kEINASTO;
double        gMW_TOT_RSCALE =                                                  15.14;
double        gMW_TOT_SHAPE_PARAMS[gN_SHAPE_PARAMS] =                           {0.17, -1, -1};
double        gMW_TRIAXIAL_AXES[gN_XYZ] =                                       {1.47, 1.22, 0.98};
bool          gMW_TRIAXIAL_IS =                                                 false;
double        gMW_TRIAXIAL_ROTANGLES[gN_XYZ] =                                  {0., 0., 0.};


// Any other halo related
int           gHALO_SUBS_FLAG_CDELTAMDELTA[gN_TYPEHALOES] =                     {kSANCHEZ14_200, kSANCHEZ14_200, kSANCHEZ14_200, kSANCHEZ14_200};
int           gHALO_SUBS_FLAG_PROFILE[gN_TYPEHALOES] =                          {kHOST, kHOST, kHOST, kEINASTO};
double        gHALO_SUBS_SHAPE_PARAMS[gN_TYPEHALOES][gN_SHAPE_PARAMS] =         {{kHOST, kHOST, kHOST}, {kHOST, kHOST, kHOST}, {kHOST, kHOST, kHOST}, {0.17, -1, -1}};
double        gHALO_SUBS_DPDM_SLOPE[gN_TYPEHALOES] =                            {1.9, 1.9, 1.9, 1.9};
int           gHALO_SUBS_DPDV_FLAG_PROFILE[gN_TYPEHALOES] =                     {kEINASTO, kEINASTO, kEINASTO, kEINASTO};
double        gHALO_SUBS_DPDV_RSCALE_TO_RS_HOST[gN_TYPEHALOES] =                {10., 10., 10., 10};
double        gHALO_SUBS_DPDV_SHAPE_PARAMS[gN_TYPEHALOES][gN_SHAPE_PARAMS]  =   {{0.68, kHOST, kHOST}, {0.68, kHOST, kHOST}, {0.68, kHOST, kHOST}, {0.68, kHOST, kHOST}};
double        gHALO_SUBS_MASSFRACTION[gN_TYPEHALOES] =                          {0.2, 0.2, 0.2, 0.2};
double        gHALO_TRIAXIAL_AXES[gN_XYZ] =                                     {1., 1., 1.};
bool          gHALO_TRIAXIAL_IS =                                               false;
double        gHALO_TRIAXIAL_ROTANGLES[gN_XYZ] =                                {0., 0., 0.};
vector<vector<double>>gHALO_NODES_X_GRID(1);                                    // will be initialized at run time in set_rho_nodes(). N.B.: first entry currently never used, use it for special profile
vector<vector<double>>gHALO_NODES_Y_GRID(1);                                    // will be initialized at run time in set_rho_nodes(). N.B.: first entry currently never used, use it for special profile
vector<int>   gHALO_NODES_INTERPOLTYPE(1, kLINLOG);
vector<double>gHALO_NODES_RATIO_R_2_RSCALE(1);                                  // will be initialized at run time in set_rho_nodes(). N.B.: first entry currently never used, use it for special profile
vector<double>gHALO_NODES_INPUT_RSCALE(1);                                      // will be initialized at run time in set_rho_nodes(). N.B.: first entry currently never used, use it for special profile
vector<double>gHALO_NODES_INPUT_RHOSCALE(1);                                    // will be initialized at run time in set_rho_nodes(). N.B.: first entry currently never used, use it for special profile

// special parameters only existing for extragalactic objects
string        gEXTRAGAL_SUBS_DPDM_SLOPE_LIST =                                  "1.9,2.0";
int           gEXTRAGAL_FLAG_CDELTAMDELTA =                                     kSANCHEZ14_200;
string        gEXTRAGAL_FLAG_CDELTAMDELTA_LIST =                                "kSANCHEZ14_200,kB01_VIR";
int           gEXTRAGAL_FLAG_MASSFUNCTION =                                     kTINKER08;
int           gEXTRAGAL_FLAG_ABSORPTIONPROFILE =                                kFRANCESCHINI08;
double        gEXTRAGAL_IDM_MHALFMODE =                                         4.3e9 / gCOSMO_HUBBLE;
double        gEXTRAGAL_IDM_ALPHA =                                             -1.;
double        gEXTRAGAL_IDM_BETA =                                              0.33;
double        gEXTRAGAL_IDM_GAMMA =                                             1.;
double        gEXTRAGAL_IDM_DELTA =                                             0.6;
double        gEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE =                             -1;
double        gEXTRAGAL_HMF_SMALLSCALE_DPDM_MLIM =                              1e10;

// Particle physics model related
double        gPP_BR[gN_PP_BR] =                                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1.0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
int           gPP_DM_ANNIHIL_DELTA =                                            2;
double        gPP_DM_ANNIHIL_SIGMAV_CM3PERS =                                   3.e-26;
double        gPP_DM_DECAY_LIFETIME_S =                                         1.e27;
bool          gPP_DM_IS_ANNIHIL_OR_DECAY =                                      1.;
double        gPP_DM_MASS_GEV =                                                 100.;
int           gPP_FLAG_SPECTRUMMODEL =                                          kCIRELLI11_EW;
double        gPP_NUMIXING_THETA12_DEG =                                        34.;
double        gPP_NUMIXING_THETA13_DEG =                                        9.;
double        gPP_NUMIXING_THETA23_DEG =                                        41.;
// - no input parameter
double        gPP_NUOSCILLATIONMATRIX[gN_NUFLAVOUR][gN_NUFLAVOUR];              // will be initialized at run time after in load_params()

// Sunyaev–Zel'dovich clusters
double        gSZ_REF_C500 =                                                    1.177;
double        gSZ_REF_PRESSURE_NORM =                                           8.403;
double        gSZ_REF_SHAPE_PARAMS[gN_SHAPE_PARAMS] =                           {1.0510, 5.4905, 0.3081};
double        gSZ_REF_ALPHA_MYX =                                               0.561;
int           gSZ_FLAG_MASSFUNCTION =                                           kTINKER08;

// Statistical analysis related
string        gSTAT_FILES =                                                     "$CLUMPY/data/stat_example.dat";
double        gSTAT_CL =                                                        0.68;
string        gSTAT_CL_LIST =                                                   "0.68,0.95";
string        gSTAT_ID_LIST =                                                   "1,2,4";
int           gSTAT_MODE =                                                      0;
bool          gSTAT_IS_LOGL_OR_CHI2 =                                           true;
double        gSTAT_RKPC_FOR_MR =                                               0.;
string        gSTAT_DATAFILES =                                                 "-1";
int           gSTAT_N_REALIZATIONS =                                            0;

// List of halos used in CLUMPY run
string        gLIST_HALOES =                                                    "$CLUMPY/data/list_generic.txt";
string        gLIST_HALOES_JEANS =                                              "$CLUMPY/data/list_generic_jeans.txt";
string        gLIST_HALONAME =                                                  "rs01_gamma05";
string        gLIST_HALOES_NODES =                                              "$CLUMPY/data/list_generic_nodes.txt";
string        gLIST_HALOES_NODES_RSCALE =                                       "-1";
// Dummy input parameters: these are defined in the halo list, but are needed here to be saved in FITS output.
double        gLIST_HALO_DISTANCE =                                             0.;
double        gLIST_HALO_RSCALE =                                               0.;
double        gLIST_HALO_RHOSCALE =                                             0.;
double        gLIST_HALO_RDELTA =                                               0.;

// CLUMPY run related
double        gSIM_EPS =                                                        1.e-2;
double        gSIM_EPS_DRAWN =                                                  max(0.1, gSIM_EPS);
double        gSIM_USER_RSE =                                                   2;
int           gSIM_SEED =                                                       666;
string        gSIM_OUTPUT_DIR =                                                 "output/";

bool          gSIM_IS_ASTRO_OR_PP_UNITS =                                       false;
double        gSIM_ALPHAINT =                                                   0.1 * DEG_to_RAD;

bool          gSIM_IS_WRITE_FLUXMAPS =                                          true;
bool          gSIM_FLUX_IS_INTEG_OR_DIFF =                                      true;
double        gSIM_FLUX_AT_E_GEV =                                              10.;
double        gSIM_FLUX_EMIN_GEV =                                              1.;
double        gSIM_FLUX_EMAX_GEV =                                              99.;
int           gSIM_FLUX_FLAG_NUFLAVOUR =                                        kNUMU;
int           gSIM_FLUX_FLAG_FINALSTATE =                                       kGAMMA;
double        gSIM_JFACTOR =                                                    1e20;
double        gSIM_JFRACTION =                                                  0.8;

double        gSIM_GAUSSBEAM_GAMMA_FWHM =                                       -1;
double        gSIM_GAUSSBEAM_NEUTRINO_FWHM =                                    -1;
double        gSIM_GAUSSBEAM_SZ_FWHM =                                          -1;

double        gSIM_REDSHIFT =                                                   0.;
double        gSIM_EXTRAGAL_ZMIN =                                              0.;
double        gSIM_EXTRAGAL_ZMAX =                                              4.;
int           gSIM_EXTRAGAL_NZ =                                                41;
bool          gSIM_EXTRAGAL_IS_ZLOG =                                           false;
double        gSIM_EXTRAGAL_DELTAZ_PRECOMP =                                    0.1;
double        gSIM_EXTRAGAL_MMIN =                                              1.e10;
double        gSIM_EXTRAGAL_MMAX =                                              1.e16;
int           gSIM_EXTRAGAL_NM =                                                100;
int           gSIM_EXTRAGAL_NM_PRECOMP =                                        1000;
bool          gSIM_EXTRAGAL_IS_MLOG =                                           true;
int           gSIM_EXTRAGAL_FLAG_WINDOWFUNC =                                   kTOP_HAT;
double        gSIM_EXTRAGAL_MF_SIGMA_CUTOFF =                                   0.2;
double        gSIM_EXTRAGAL_KMAX_PRECOMP =                                      1e12; // in units of h/Mpc
double        gSIM_EXTRAGAL_EBL_UNCERTAINTY =                                   0.; //
int           gSIM_EXTRAGAL_FLAG_GROWTHFACTOR =                                 kHEATH77;

// no input variables:
double        gSIM_EXTRAGAL_KMIN_PRECOMP =                                      1.e-5; // in units of h/Mpc
bool          gSIM_EXTRAGAL_M_PRECOMP_ISLOG =                                   true;
bool          gSIM_EXTRAGAL_DELTAZ_PRECOMP_ISLOG =                              false;
bool          gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE =                         false;

bool          gSIM_IS_WRITE_GALPOWERSPECTRUM =                                  false;
bool          gSIM_IS_WRITE_ROOTFILES =                                         true;
bool          gSIM_IS_SZ =                                                      false;

int           gSIM_HEALPIX_NSIDE =                                              1024;
string        gSIM_HEALPIX_RING_WEIGHTS_DIR =                                   "$CLUMPY/data/healpix";
int           gSIM_HEALPIX_ITER =                                               -1;
double        gSIM_HEALPIX_NLMAX_FAC =                                          -1;
PDT           gSIM_HEALPIX_FITS_DATATYPE =                                      PLANCK_FLOAT32;
Healpix_Ordering_Scheme gSIM_HEALPIX_SCHEME =                                   NEST;

// parameters for 1D runs
int           gSIM_NX =                                                         100;
bool          gSIM_IS_XLOG =                                                    true;
double        gSIM_XPOWER =                                                     2.;

double        gSIM_R_MIN =                                                      1.e-2;
double        gSIM_R_MAX =                                                      1.e2;

double        gSIM_ALPHAINT_MIN =                                               1.e-2 * DEG_to_RAD;
double        gSIM_ALPHAINT_MAX =                                               5. * DEG_to_RAD;
double        gSIM_THETA_MIN =                                                  0. * DEG_to_RAD;
double        gSIM_THETA_MAX =                                                  180. * DEG_to_RAD;
bool          gSIM_SORT_CONTRAST_THRESH =                                       true;
double        gSIM_PHI_CUT =                                                    20 * DEG_to_RAD;

// parameters for 2D runs
double        gSIM_PSI_OBS =                                                    180. * DEG_to_RAD;
string        gSIM_THETA_OBS_DEG =                                              "0";
string        gSIM_THETA_ORTH_SIZE_DEG =                                        "4";
double        gSIM_THETA_SIZE =                                                 4. * DEG_to_RAD;

bool          gSIM_IS_DISPLAY =                                                 true;
bool          gSIM_IS_PRINT   =                                                 true;

// special input parameter (always via command line):
int           gSIM_FLAG_MODE;

// - no input parameters
int           gSIM_SIGDIGITS;                                                   // will be initialized at run time after loading gSIM_EPS in load_params()
double        gSIM_HEALPIX_DELTAOMEGA;                                          // will be initialized at run time in hp_set_resolution()
double        gSIM_RESOLUTION;                                                  // will be initialized at run time in hp_set_resolution()
bool          gSIM_IS_CIRCULAR_SYMMETRY =                                       true; // will be changed only in case of triaxial halo
double        gSIM_JEANS_RMAX;
int           gSIM_PARAMLENGTH_MAX =                                            15; // maximum string length of a input parameter value

bool          gSIM_MDELTA_TO_PAR_METHOD =                                       false; // hardcoded switch: false => Moritz' method, true => Celine's method
#if IS_ROOT
TApplication *gSIM_ROOTAPP =                                                    NULL;
TText        *gSIM_CLUMPYAD =                                                   NULL;
#endif
vector<string>gSIM_INPUTPARAM_VALUESTRING(gN_INPUTPARAMS, "-999");

vector<string> gSIM_STANDARD_INPUTPARAMS;                                       // will initialized at run time in main()

string        gPATH_TO_CLASS;                                                   // will initialized at run time from environmental variable
string        gPATH_TO_CLUMPY_EXE;                                              // will initialized at run time in main()
string        gPATH_TO_CLUMPY_HOME;                                             // will initialized at run time in main()
string        gPATH_TO_USER_EXE;                                                // will initialized at run time in main()
bool          gSIM_IS_TEST =                                                    true;

//------------------------------------------------------------------------------
// Global variables for input paramaters
// Global variables for FITS keywords corresponding to the input parameters (limited to eight letters)
// Global variables for the input parameters' units
// Global variables for commenting the meaning of the input parameters (limited to 47 letters to be FITS compatible)
// Global variables for the input parameters' data types (only for display purpose to the user)
// Order must match the gENUM_INPUTPARAMS definitions in params.h


const char  gSIM_INPUTPARAMS[gN_INPUTPARAMS][5][50]      = {
   {"gCOSMO_DELTA0",                              "DELTA0",   "[-]",            "Overdensity factor at z=0;-1 for kBRYANNORMAN98", "float",         },
   {"gCOSMO_FLAG_DELTA_REF",                      "FLAGDELT", "[-]",            "Reference density of the overdensity factor",     "string",        },
   {"gCOSMO_HUBBLE",                              "HUBBLE",   "[-]",            "Reduced dimensionless Hubble parameter",          "float",         },
   {"gCOSMO_N_S",                                 "N_S",      "[-]",            "Scalar spectral index of primord. perturbations", "float",         },
   {"gCOSMO_OMEGA0_M",                            "OMEGA0_M", "[-]",            "Dark and Baryonic Mass Omega0_mass",              "float",         },
   {"gCOSMO_OMEGA0_B",                            "OMEGA0_B", "[-]",            "Baryonic Mass Omega0_baryon",                     "float",         },
   {"gCOSMO_OMEGA0_K",                            "OMEGA0_K", "[-]",            "Curvature Omega0_k",                              "float",         },
   {"gCOSMO_SIGMA8",                              "SIGMA_8",  "[-]",            "Fluctuation amplitude at 8 Mpc h^-1",             "float",         },
   {"gCOSMO_T0",                                  "GAMMA_T0", "[K]",            "Present-day CMB photon temperature",              "float",         },
   {"gCOSMO_TAU_REIO",                            "TAU_REIO", "[-]",            "Reionization optical depth",                      "float",         },
   {"gCOSMO_WDE",                                 "WDE",      "[-]",            "Dark energy equation of state exponent",          "float",         },

   // DM names
   {"gDM_IS_IDM",                                 "IS_IDM",   "[-]",            "Interacting DM HMF cut-off from  Moline (2016)",  "boolean (0/1)", },
   {"gDM_KMAX",                                   "KMAX", "[h/Mpc]",            "k_max/h of power spectrum (-1=value from file)",  "float",         },
   {"gDM_KMAX_SIGMA_CUTOFF",                      "KMAXSIGM", "[-]",            "Smoothness of the P(k) cut-off",                  "float",         },
   {"gDM_LOGCDELTA_STDDEV",                       "CDELTSTD", "[-]",            "log-std. deviation of subhalo scattering",        "float",         },
   {"gDM_RHOHALOES_TO_RHOMEAN",                   "RHOHFRAC", "[-]",            "Max. fraction rho_m bound into DM halos",         "float",         },
   {"gDM_RHOSAT",                                 "SAT_DENS", "[Msol/kpc^3]",   "Dark Matter saturation density",                  "float",         },
   {"gDM_SUBS_MMIN",                              "MMINSUBS", "[Msol]",         "Smallest subclump mass",                          "float",         },
   {"gDM_SUBS_MMAXFRAC",                          "MMAXSUBS", "[-]",            "Biggest subclump mass, fraction of host mass",    "float",         },
   {"gDM_SUBS_NUMBEROFLEVELS",                    "MULT_SUB", "[-]",            "Number of multilevel substructures",              "integer",       },

   // GAL (Milky-Way) names
   {"gMW_TOT_FLAG_PROFILE",                       "MWPRFTOT", "[-]",            "Profile of total Galactic DM halo",               "string",        },
   {"gMW_TOT_SHAPE_PARAMS_0",                     "MWSP0TOT", "[-]",            "Shape parameter 1 of total Gal. DM halo profile", "float",         },
   {"gMW_TOT_SHAPE_PARAMS_1",                     "MWSP1TOT", "[-]",            "Shape parameter 2 of total Gal. DM halo profile", "float",         },
   {"gMW_TOT_SHAPE_PARAMS_2",                     "MWSP2TOT", "[-]",            "Shape parameter 3 of total Gal. DM halo profile", "float",         },
   {"gMW_TOT_RSCALE",                             "MWRSCTOT", "[kpc]",          "Scale radius of total Dark Matter halo profile",  "float",         },
   {"gMW_RMAX",                                   "MW_RMAX",  "[kpc]",          "Virial radius of Dark Matter halo",               "float",         },
   {"gMW_RSOL",                                   "MW_RSOL",  "[kpc]",          "Sun's distance from the Gal. center",             "float",         },
   {"gMW_RHOSOL",                                 "MWRHOSOL", "[GeV/cm3]",      "Local Dark Matter density",                       "float",         },

   {"gMW_TRIAXIAL_IS",                            "MW_ISTRI", "[-]",            "Triaxial halo (True/False)",                      "boolean (0/1)", },
   {"gMW_TRIAXIAL_AXES_0",                        "MWTRIAX0", "[-]",            "1st axis of triaxial halo",                       "float",         },
   {"gMW_TRIAXIAL_AXES_1",                        "MWTRIAX1", "[-]",            "2nd axis of triaxial halo",                       "float",         },
   {"gMW_TRIAXIAL_AXES_2",                        "MWTRIAX2", "[-]",            "3rd axis of triaxial halo",                       "float",         },
   {"gMW_TRIAXIAL_ROTANGLES_0",                   "MWTRIAN0", "[deg]",          "1st rotation angle of triaxial halo",             "float",         },
   {"gMW_TRIAXIAL_ROTANGLES_1",                   "MWTRIAN1", "[deg]",          "2nd rotation angle of triaxial halo",             "float",         },
   {"gMW_TRIAXIAL_ROTANGLES_2",                   "MWTRIAN2", "[deg]",          "3rd rotation angle of triaxial halo",             "float",         },

   {"gMW_SUBS_FLAG_PROFILE",                      "MW_PRFCL", "[-]",            "Halo profile of subclumps",                       "string",        },
   {"gMW_SUBS_SHAPE_PARAMS_0",                    "MW_SP0CL", "[-]",            "Shape parameter 1 of subclumps halo profile",     "float or kHOST",},
   {"gMW_SUBS_SHAPE_PARAMS_1",                    "MW_SP1CL", "[-]",            "Shape parameter 2 of subclumps halo profile",     "float or kHOST",},
   {"gMW_SUBS_SHAPE_PARAMS_2",                    "MW_SP2CL", "[-]",            "Shape parameter 3 of subclumps halo profile",     "float or kHOST",},
   {"gMW_SUBS_FLAG_CDELTAMDELTA",                 "MW_C_M",   "[-]",            "Mass-concentration model of subclumps",           "string",        },
   {"gMW_SUBS_DPDV_FLAG_PROFILE",                 "MW_PRFDP", "[-]",            "dP/dV profile of subhalo distribution in host",   "string",        },
   {"gMW_SUBS_DPDV_SHAPE_PARAMS_0",               "MW_SP1DP", "[-]",            "Shape parameter 1 of dP/dV distrib. profile",     "float or kHOST",},
   {"gMW_SUBS_DPDV_SHAPE_PARAMS_1",               "MW_SP2DP", "[-]",            "Shape parameter 2 of dP/dV distrib. profile",     "float or kHOST",},
   {"gMW_SUBS_DPDV_SHAPE_PARAMS_2",               "MW_RDPDV", "[-]",            "Shape parameter 3 of dP/dV distrib. profile",     "float or kHOST",},
   {"gMW_SUBS_DPDV_RSCALE_TO_RS_HOST",            "MW_SP0DP", "[-]",            "Scale radius of dP/dV profile to r_s of host",    "float",         },
   {"gMW_SUBS_DPDM_SLOPE",                        "MW_DPDM",  "[-]",            "Slope of power-law subhalo mass spectrum dP/dM",  "float",         },
   {"gMW_SUBS_M1",                                "MWSUBSM1", "[Msol]",         "Mass m1",                                         "float",         },
   {"gMW_SUBS_M2",                                "MWSUBSM2", "[Msol]",         "Mass m2",                                         "float",         },
   {"gMW_SUBS_N_INM1M2",                          "SUBSM1M2", "[-]",            "# of subclumps with masses between m1 and m2",    "integer",       },

   {"gMW_SUBS_TABULATED_IS",                      "MWT_IS",   "[-]",            "If true, use tabulated (evolved) sub properties", "boolean (0/1)", },
   {"gMW_SUBS_TABULATED_CMIN_OF_R",               "MWT_CMIN", "[-]",            "File for minimal c(r_gal) after tidal effects",   "string",        },
   {"gMW_SUBS_TABULATED_LCRIT",                   "MWTLCRIT", "[-]",            "File for l_crit (for evolved sub properties)",    "string",        },
   {"gMW_SUBS_TABULATED_RTIDAL_TO_RS",            "MWT_RTID", "[-]",            "File for r_tidal/r_s (for evolved subs)",         "string",        },

   // Extragalactic structures names:
   {"gEXTRAGAL_FLAG_PROFILE",                     "EGPRFTOT", "[-]",            "Dark Matter halo density profile",                "string",        },
   {"gEXTRAGAL_SHAPE_PARAMS_0",                   "EGSP0TOT", "[-]",            "Shape parameter 1 of cosmic DM halo profile",     "float",         },
   {"gEXTRAGAL_SHAPE_PARAMS_1",                   "EGSP1TOT", "[-]",            "Shape parameter 2 of cosmic DM halo profile",     "float",         },
   {"gEXTRAGAL_SHAPE_PARAMS_2",                   "EGSP2TOT", "[-]",            "Shape parameter 3 of cosmic DM halo profile",     "float",         },
   {"gEXTRAGAL_FLAG_CDELTAMDELTA",                "EG_CMTOT", "[-]",            "Mass-concentration model of cosmic DM halos",     "string",        },
   {"gEXTRAGAL_FLAG_CDELTAMDELTA_LIST",           "EG_CMLST", "[-]",            "List of mass-concentration models",               "comma-sep list",},

   {"gEXTRAGAL_FLAG_MASSFUNCTION",                "MASSFUNC", "[-]",            "Mass function of extragalactic halos",            "string",        },
   {"gEXTRAGAL_IDM_MHALFMODE",                    "EG_IDMMH", "[Msol]",         "IDM half-mode mass of cut-off (Moline, 2016)",    "float",         },
   {"gEXTRAGAL_IDM_ALPHA",                        "EG_IDM_A", "[-]",            "IDM cut-off parameter alpha (Moline, 2016)",      "float",         },
   {"gEXTRAGAL_IDM_BETA",                         "EG_IDM_B", "[-]",            "IDM cut-off parameter beta (Moline, 2016)",       "float",         },
   {"gEXTRAGAL_IDM_GAMMA",                        "EG_IDM_C", "[-]",            "IDM cut-off parameter gamma (Moline, 2016)",      "float",         },
   {"gEXTRAGAL_IDM_DELTA",                        "EG_IDM_D", "[-]",            "IDM cut-off parameter dalta (Moline, 2016)",      "float",         },
   {"gEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE",        "EG_DPDM2", "[-]",            "Force HMF slope steeper than value (>0, -1=off)", "float",         },
   {"gEXTRAGAL_HMF_SMALLSCALE_DPDM_MLIM",         "EG_MLIM",  "[Msol]",         "Set mass below which HMF slope is enforced",      "float",         },

   {"gEXTRAGAL_TRIAXIAL_IS",                      "EG_ISTRI", "[-]",            "Triaxial halo (True/False)",                      "boolean (0/1)", },
   {"gEXTRAGAL_TRIAXIAL_AXES_0",                  "EGTRIAX0", "[-]",            "1st axis of triaxial halo",                       "float",         },
   {"gEXTRAGAL_TRIAXIAL_AXES_1",                  "EGTRIAX1", "[-]",            "2nd axis of triaxial halo",                       "float",         },
   {"gEXTRAGAL_TRIAXIAL_AXES_2",                  "EGTRIAX2", "[-]",            "3rd axis of triaxial halo",                       "float",         },
   {"gEXTRAGAL_TRIAXIAL_ROTANGLES_0",             "EGTRIAN0", "[deg]",          "1st rotation angle of triaxial halo",             "float",         },
   {"gEXTRAGAL_TRIAXIAL_ROTANGLES_1",             "EGTRIAN1", "[deg]",          "2nd rotation angle of triaxial halo",             "float",         },
   {"gEXTRAGAL_TRIAXIAL_ROTANGLES_2",             "EGTRIAN2", "[deg]",          "3rd rotation angle of triaxial halo",             "float",         },

   {"gEXTRAGAL_SUBS_FLAG_CDELTAMDELTA",           "EG_CMSUB", "[-]",            "Mass-concentration model of subclumps",           "string",        },
   {"gEXTRAGAL_SUBS_DPDV_FLAG_PROFILE",           "EG_PRFDP", "[-]",            "dP/dV profile of subhalo distribution in host",   "string",        },
   {"gEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_0",         "EG_SP0DP", "[-]",            "Shape parameter 1 of dP/dV distrib. profile",     "float or kHOST",},
   {"gEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_1",         "EG_SP1DP", "[-]",            "Shape parameter 2 of dP/dV distrib. profile",     "float or kHOST",},
   {"gEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_2",         "EG_SP2DP", "[-]",            "Shape parameter 3 of dP/dV distrib. profile",     "float or kHOST",},
   {"gEXTRAGAL_SUBS_DPDV_RSCALE_TO_RS_HOST",      "EG_RDPDV", "[-]",            "Scale radius of dP/dV profile to r_s of host",    "float",         },
   {"gEXTRAGAL_SUBS_DPDM_SLOPE",                  "EG_DPDM",  "[-]",            "Slope of power-law subhalo mass spectrum dP/dM",  "float",         },
   {"gEXTRAGAL_SUBS_DPDM_SLOPE_LIST",             "EG_DPDML", "[-]",            "List of slopes of subhalo mass spectrum dP/dM",   "comma-sep list",},
   {"gEXTRAGAL_SUBS_MASSFRACTION",                "EG_F_DM",  "[-]",            "Fraction of host halo mass bound in subhalos",    "float",         },

   {"gEXTRAGAL_FLAG_ABSORPTIONPROFILE",           "PROF_ABS", "[-]",            "EBL absorption profile",                          "string",        },

   // LIST names
   {"gLIST_HALOES",                               "LIST_ADD", "[-]",            "List of external halos definitions",              "string or -1",  },
   {"gLIST_HALOES_JEANS",                         "LISTJEAN", "[-]",            "List of halos added for Jeans analysis",          "string or -1",  },
   {"gLIST_HALONAME",                             "LIST_OBJ", "[-]",            "Object selected out of a list of haloes",         "string",        },
   {"gLIST_HALOES_NODES",                         "LISTNODE", "[-]",            "Node list of numerical halo definition(s)",       "string",        },
   {"gLIST_HALOES_NODES_RSCALE",                  "LNODE_RS", "[-]",            "Scale radius/unit def. of halo(s) nodes in list", "string",        },
   // Dummy input parameters: these are defined in the halo list, but are needed here to be saved in FITS output.
   {"gLIST_HALO_DISTANCE",                        "LISTDIST", "[kpc]",          "Comoving line-of-sight dist. of halo",            "float",         },
   {"gLIST_HALO_RSCALE",                          "LIST_RS",  "[kpc]",          "Scale radius of halo (comoving)",                 "float",         },
   {"gLIST_HALO_RHOSCALE",                        "LISTRHOS", "[Msol/kpc^3]",   "Density at r_scale of halo (comoving)",           "float",         },
   {"gLIST_HALO_RDELTA",                          "LISTRVIR", "[kpc]",          "Virial radius of halo (comoving)",                "float",         },

   // DSPH names
   {"gDSPH_SUBS_FLAG_PROFILE",                    "SPHPRFCL", "[-]",            "Halo profile of subclumps",                       "string",        },
   {"gDSPH_SUBS_SHAPE_PARAMS_0",                  "SPHSP0CL", "[-]",            "Shape parameter 1 of subclumps halo profile",     "float or kHOST",},
   {"gDSPH_SUBS_SHAPE_PARAMS_1",                  "SPHSP1CL", "[-]",            "Shape parameter 2 of subclumps halo profile",     "float or kHOST",},
   {"gDSPH_SUBS_SHAPE_PARAMS_2",                  "SPHSP2CL", "[-]",            "Shape parameter 3 of subclumps halo profile",     "float or kHOST",},
   {"gDSPH_SUBS_FLAG_CDELTAMDELTA",               "SPH_C_M",  "[-]",            "Mass-concentration model of subclumps",           "string",        },
   {"gDSPH_SUBS_DPDV_FLAG_PROFILE",               "SPHPRFDP", "[-]",            "dP/dV profile of subhalo distribution in host",   "string",        },
   {"gDSPH_SUBS_DPDV_SHAPE_PARAMS_0",             "SPHSP0DP", "[-]",            "Shape parameter 1 of dP/dV distrib. profile",     "float or kHOST",},
   {"gDSPH_SUBS_DPDV_SHAPE_PARAMS_1",             "SPHSP1DP", "[-]",            "Shape parameter 2 of dP/dV distrib. profile",     "float or kHOST",},
   {"gDSPH_SUBS_DPDV_SHAPE_PARAMS_2",             "SPHSP2DP", "[-]",            "Shape parameter 3 of dP/dV distrib. profile",     "float or kHOST",},
   {"gDSPH_SUBS_DPDV_RSCALE_TO_RS_HOST",          "SPHRDPDV", "[-]",            "Scale radius of dP/dV profile to r_s of host",    "float",         },
   {"gDSPH_SUBS_DPDM_SLOPE",                      "SPH_DPDM", "[-]",            "Slope of power-law subhalo mass spectrum dP/dM",  "float",         },
   {"gDSPH_SUBS_MASSFRACTION",                    "SPH_F_DM", "[-]",            "Fraction of host halo mass bound in subhalos",    "float",         },

   // GALAXY (external halo) names:
   {"gGALAXY_SUBS_FLAG_PROFILE",                  "GALPRFCL", "[-]",            "Halo profile of subclumps",                       "string",        },
   {"gGALAXY_SUBS_SHAPE_PARAMS_0",                "GALSP0CL", "[-]",            "Shape parameter 1 of subclumps halo profile",     "float or kHOST",},
   {"gGALAXY_SUBS_SHAPE_PARAMS_1",                "GALSP1CL", "[-]",            "Shape parameter 2 of subclumps halo profile",     "float or kHOST",},
   {"gGALAXY_SUBS_SHAPE_PARAMS_2",                "GALSP2CL", "[-]",            "Shape parameter 3 of subclumps halo profile",     "float or kHOST",},
   {"gGALAXY_SUBS_FLAG_CDELTAMDELTA",             "GAL_C_M",  "[-]",            "Mass-concentration model of subclumps",           "string",        },
   {"gGALAXY_SUBS_DPDV_FLAG_PROFILE",             "GALPRFDP", "[-]",            "dP/dV profile of subhalo distribution in host",   "string",        },
   {"gGALAXY_SUBS_DPDV_SHAPE_PARAMS_0",           "GALSP0DP", "[-]",            "Shape parameter 1 of dP/dV distrib. profile",     "float or kHOST",},
   {"gGALAXY_SUBS_DPDV_SHAPE_PARAMS_1",           "GALSP1DP", "[-]",            "Shape parameter 2 of dP/dV distrib. profile",     "float or kHOST",},
   {"gGALAXY_SUBS_DPDV_SHAPE_PARAMS_2",           "GALSP2DP", "[-]",            "Shape parameter 3 of dP/dV distrib. profile",     "float or kHOST",},
   {"gGALAXY_SUBS_DPDV_RSCALE_TO_RS_HOST",        "GALRDPDV", "[-]",            "Scale radius of dP/dV profile to r_s of host",    "float",         },
   {"gGALAXY_SUBS_DPDM_SLOPE",                    "GAL_DPDM", "[-]",            "Slope of power-law subhalo mass spectrum dP/dM",  "float",         },
   {"gGALAXY_SUBS_MASSFRACTION",                  "GAL_F_DM", "[-]",            "Fraction of host halo mass bound in subhalos",    "float",         },

   // CLUSTERS names
   {"gCLUSTER_SUBS_FLAG_PROFILE",                 "CLUPRFCL", "[-]",            "Halo profile of subclumps",                       "string",        },
   {"gCLUSTER_SUBS_SHAPE_PARAMS_0",               "CLUSP0CL", "[-]",            "Shape parameter 1 of subclumps halo profile",     "float or kHOST",},
   {"gCLUSTER_SUBS_SHAPE_PARAMS_1",               "CLUSP1CL", "[-]",            "Shape parameter 2 of subclumps halo profile",     "float or kHOST",},
   {"gCLUSTER_SUBS_SHAPE_PARAMS_2",               "CLUSP2CL", "[-]",            "Shape parameter 3 of subclumps halo profile",     "float or kHOST",},
   {"gCLUSTER_SUBS_FLAG_CDELTAMDELTA",            "CLU_C_M",  "[-]",            "Mass-concentration model of subclumps",           "string",        },
   {"gCLUSTER_SUBS_DPDV_FLAG_PROFILE",            "CLUPRFDP", "[-]",            "dP/dV profile of subhalo distribution in host",   "string",        },
   {"gCLUSTER_SUBS_DPDV_SHAPE_PARAMS_0",          "CLUSP0DP", "[-]",            "Shape parameter 1 of dP/dV distrib. profile",     "float or kHOST",},
   {"gCLUSTER_SUBS_DPDV_SHAPE_PARAMS_1",          "CLUSP1DP", "[-]",            "Shape parameter 2 of dP/dV distrib. profile",     "float or kHOST",},
   {"gCLUSTER_SUBS_DPDV_SHAPE_PARAMS_2",          "CLUSP2DP", "[-]",            "Shape parameter 3 of dP/dV distrib. profile",     "float or kHOST",},
   {"gCLUSTER_SUBS_DPDV_RSCALE_TO_RS_HOST",       "CLURDPDV", "[-]",            "Scale radius of dP/dV profile to r_s of host",    "float",         },
   {"gCLUSTER_SUBS_DPDM_SLOPE",                   "CLU_DPDM", "[-]",            "Slope of power-law subhalo mass spectrum dP/dM",  "float",         },
   {"gCLUSTER_SUBS_MASSFRACTION",                 "CLU_F_DM", "[-]",            "Fraction of host halo mass bound in subhalos",    "float",         },

   // PP names
   {"gPP_BR",                                     "BR_", "[-]",                 "Branching channel ratio",                         "comma-sep list",},
   {"gPP_FLAG_SPECTRUMMODEL",                     "SPEC_MOD", "[-]",            "Spectrum model",                                  "string",        },
   {"gPP_DM_ANNIHIL_DELTA",                       "PP_DELTA", "[-]",            "Majorana (2) or Dirac (4) particle",              "integer",       },
   {"gPP_DM_ANNIHIL_SIGMAV_CM3PERS",              "SIGMA_V",  "[cm^3/s]",       "Annihilation cross section",                      "float",         },
   {"gPP_DM_DECAY_LIFETIME_S",                    "DECAY_LT", "[s]",            "Decay lifetime",                                  "float",         },
   {"gPP_DM_IS_ANNIHIL_OR_DECAY",                 "DMMODEL",  "[-]",            "Dark Matter Model (0: Decay, 1: Annihilation)",   "boolean (0/1)", },
   {"gPP_DM_MASS_GEV",                            "DM_MASS",  "[GeV]",          "[GeV] dark matter particle mass",                 "float",         },
   {"gPP_NUMIXING_THETA12_DEG",                   "NU_MIX12", "[deg]",          "Neutrino mixing angle 1",                         "float",         },
   {"gPP_NUMIXING_THETA13_DEG",                   "NU_MIX13", "[deg]",          "Neutrino mixing angle 2",                         "float",         },
   {"gPP_NUMIXING_THETA23_DEG",                   "NU_MIX23", "[deg]",          "Neutrino mixing angle 3",                         "float",         },

   // Sunyaev–Zel'dovich clusters
   {"gSZ_REF_C500",                               "SZ_C500",  "[-]",            "Lorem ipsum dolor sit amet",                      "float",         },
   {"gSZ_REF_PRESSURE_NORM",                      "SZ_PRESS", "[-]",            "Lorem ipsum dolor sit amet",                      "float",         },
   {"gSZ_REF_SHAPE_PARAMS_0",                     "SP0_SZ",   "[-]",            "Lorem ipsum dolor sit amet",                      "float",         },
   {"gSZ_REF_SHAPE_PARAMS_1",                     "SP1_SZ",   "[-]",            "Lorem ipsum dolor sit amet",                      "float",         },
   {"gSZ_REF_SHAPE_PARAMS_2",                     "SP2_SZ",   "[-]",            "Lorem ipsum dolor sit amet",                      "float",         },
   {"gSZ_REF_ALPHA_MYX",                          "SZ_ALPHA", "[-]",            "Lorem ipsum dolor sit amet",                      "float",         },
   {"gSZ_FLAG_MASSFUNCTION",                      "SZ_MASSF", "[-]",            "Lorem ipsum dolor sit amet",                      "string"         },

   // Statistical analysis related
   {"gSTAT_CL",                                   "STAT_CL",  "[-]",            "CL percentage for which to search the min/max",   "float",         },
   {"gSTAT_CL_LIST",                              "STAT_CLS", "[-]",            "List of x% CL to be drawn",                       "string",        },
   {"gSTAT_DATAFILES",                            "STADATAF", "[-]",            "Data on which to superimpose the median + CLs",   "string",        },
   {"gSTAT_FILES",                                "STAFILES", "[-]",            "Stat. analysis file / file containing filenames", "string",        },
   {"gSTAT_ID_LIST",                              "STAT_IDS", "[-]",            "IDs of parameters to plot",                       "string",        },
   {"gSTAT_IS_LOGL_OR_CHI2",                      "STATLOGL", "[-]",            "Analysis file contains log-likelihood or chi2",   "boolean (0/1)", },
   {"gSTAT_MODE",                                 "STATMODE", "[-]",            "Mode: 0:PDF, 1:chi2, 2:both, 3:mean&dispersion",  "integer",       },
   {"gSTAT_N_REALIZATIONS",                       "STATNREP",    "[-]",         "# of samples on which mean value is based",       "integer",       },
   {"gSTAT_RKPC_FOR_MR",                          "STATRKPC", "[-]",            "If 1: plot M(rkpc_for_mr) inst. of 1st ID par.",  "float",         },

   // 'special' parameter: flag of performed simulation
   {"gSIM_FLAG_MODE",                            "SIMUMODE", "[-]",             "Type of performed simulation",                    "string",        },

   // SIMU names
   {"gSIM_ALPHAINT_DEG",                          "ALPHAINT", "[deg]",          "Spatial J-factor integration angle",              "float or -1",   },
   {"gSIM_ALPHAINT_MIN_DEG",                      "ALPH_MIN", "[deg]",          "Minimum alpha_int value for 1D profile",          "float",         },
   {"gSIM_ALPHAINT_MAX_DEG",                      "ALPH_MAX", "[deg]",          "Maximum alpha_int value for 1D profile",          "float",         },

   {"gSIM_IS_WRITE_FLUXMAPS",                     "FLUXBOOL", "[-]",            "Calculate gamma-ray and/or neutrino fluxes",      "boolean (0/1)", },
   {"gSIM_FLUX_IS_INTEG_OR_DIFF",                 "DIFFBOOL", "[-]",            "Flux either differential (0) or integrated (1)",  "boolean (0/1)", },
   {"gSIM_FLUX_AT_E_GEV",                         "E_PIVOT",  "[GeV]",          "Energy of differential flux",                     "float",         },
   {"gSIM_FLUX_EMIN_GEV",                         "E_MIN",    "[GeV]",          "Lower limit for integrated flux & shown spectrum", "float",         },
   {"gSIM_FLUX_EMAX_GEV",                         "E_MAX",    "[GeV]",          "Upper limit for integrated flux & shown spectrum", "float",         },
   {"gSIM_FLUX_FLAG_NUFLAVOUR",                   "NU_FLAV",  "[-]",            "Considered neutrino flavour",                     "string",        },
   {"gSIM_FLUX_FLAG_FINALSTATE",                  "FINSTATE", "[-]",            "Considered final state particle",                 "string",        },
   {"gSIM_GAUSSBEAM_GAMMA_FWHM_DEG",              "FWHM_GMA", "[deg]",          "FWHM of Gaussian beam of gamma-ray telescope",    "float or -1",   },
   {"gSIM_GAUSSBEAM_NEUTRINO_FWHM_DEG",           "FWHM_NEU", "[deg]",          "FWHM of Gaussian beam of neutrino telescope",     "float or -1",   },

   {"gSIM_HEALPIX_FITS_DATATYPE",                 "HPX_TYPE", "[-]",            "HEALPix datatype (FLOAT32 or FLOAT64)",           "string",        },
   {"gSIM_HEALPIX_ITER",                          "HPX_ITER", "[-]",            "# of iterations for alm computation",             "int or -1",     },
   {"gSIM_HEALPIX_NLMAX_FAC",                     "HPXNLMAX", "[-]",            "Factor (x NSIDE) = maximum ell of alm",          "float or -1",   },
   {"gSIM_HEALPIX_NSIDE",                         "NSIDE",    "[-]",            "Resolution parameter for HEALPix",                "integer or -1", },
   {"gSIM_HEALPIX_RING_WEIGHTS_DIR",              "HPXWEIGH", "[-]",            "Directory of HEALPix ring weights",               "string or -1",  },
   {"gSIM_HEALPIX_SCHEME",                        "HPX_SCHM", "[-]",            "HEALPix scheme (RING or NESTED)",                 "string",        },

   {"gSIM_IS_XLOG",                               "ISXLOG1D", "[-]",            "Evaluate and plot points in x-axis log-scale",    "boolean (0/1)", },
   {"gSIM_NX",                                    "N_POINTS", "[-]",            "# of points at which 1D calculation is done",     "integer",       },
   {"gSIM_XPOWER",                                "X_POWER",  "[-]",            "Multiply dependent variable with power of x val", "float",         },

   {"gSIM_IS_ASTRO_OR_PP_UNITS",                  "UNITBOOL", "[-]",            "Output units [Msol]&[kpc] (1) or [GeV]&[cm] (0)", "boolean (0/1)", },
   {"gSIM_JFACTOR",                               "JFACTOR",  "[variable]",     "Input J/D-Factor for plotting flux spectrum",     "double",        },
   {"gSIM_JFRACTION",                             "J_FRAC",   "[-]",            "Fraction of total spatial extended J-factor",     "double",        },
   {"gSIM_PHI_CUT_DEG",                           "PHI_CUT",  "[deg]",          "Neglect halos with distance < value from GC",     "float",         },
   {"gSIM_R_MIN",                                 "R_MIN",    "[kpc]",          "Minimum radial coordinate for 1D profile",        "float",         },
   {"gSIM_R_MAX",                                 "R_MAX",    "[kpc]",          "Maximum radial coordinate for 1D profile",        "float",         },
   {"gSIM_REDSHIFT",                              "REDSHIFT", "[-]",            "Redshift of object(s) in the calculation",        "float",         },
   {"gSIM_SORT_CONTRAST_THRESH",                  "CONTR_TH", "[-]",            "Only print/sort halos with J_halo/J_Gal > value", "boolean (0/1)", },
   {"gSIM_THETA_MIN_DEG",                         "THET_MIN", "[deg]",          "Minimum theta value for 1D profile",              "float",         },
   {"gSIM_THETA_MAX_DEG",                         "THET_MAX", "[deg]",          "Maximum theta value for 1D profile",              "float",         },

   {"gSIM_PSI_OBS_DEG",                           "PSI_0",    "[deg]",          "Galactic long. coordinate of FOV center",         "float",         },
   {"gSIM_THETA_OBS_DEG",                         "THETA_0",  "[deg]",          "Galactic lat. coordinate of FOV center",          "float or 's'",  },
   {"gSIM_THETA_ORTH_SIZE_DEG",                   "SIZE_X",   "[deg]",          "Grid diameter in theta_orth-dir.",                "float or 'd'",  },
   {"gSIM_THETA_SIZE_DEG",                        "SIZE_Y",   "[deg]",          "Grid diameter in theta-dir.",                     "float",         },

   {"gSIM_EXTRAGAL_EBL_UNCERTAINTY",              "EBL_ERR",  "[-]",            "Systematic uncertainty on EBL extinction tau",    "double",        },
   {"gSIM_EXTRAGAL_FLAG_GROWTHFACTOR",            "GROW_FAC", "[-]",            "Method to compute lin. dens. perturb growth fac.", "string",        },
   {"gSIM_EXTRAGAL_FLAG_WINDOWFUNC",              "WINFUNC",  "[-]",            "Spherical collapse model window function",        "string",        },
   {"gSIM_EXTRAGAL_KMAX_PRECOMP",                 "KMAX",     "[h/Mpc]",        "k_max/h of computed CDM power spectrum",          "double",        },
   {"gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE",    "HMF_PROF", "[-]",            "Calculate HMF from defined halo profile shape",   "boolean (0/1)", },
   {"gSIM_EXTRAGAL_MF_SIGMA_CUTOFF",              "SIGMACUT", "[-]",            "Smoothness of the mass function cut-off",         "double",        },
   {"gSIM_EXTRAGAL_MMIN",                         "MMIN_HMF", "[Msol]",         "Minimum mass for extragalactic mass function",    "float",         },
   {"gSIM_EXTRAGAL_MMAX",                         "MMAX_HMF", "[Msol]",         "Maximum mass for extragalactic mass function",    "float",         },
   {"gSIM_EXTRAGAL_NM",                           "NM",       "[-]",            "Number of mass points to be evaluated",           "integer",       },
   {"gSIM_EXTRAGAL_IS_MLOG",                      "ISMLOG",   "[-]",            "Plot/integrate mass values in log-space",         "boolean (0/1)", },
   {"gSIM_EXTRAGAL_NM_PRECOMP",                   "NM_GRID",  "[-]",            "Number of points for HMF grid",                   "integer",       },
   {"gSIM_EXTRAGAL_ZMIN",                         "Z_MIN",    "[-]",            "Minimum redshift (for plot/integration)",         "float",         },
   {"gSIM_EXTRAGAL_ZMAX",                         "Z_MAX",    "[-]",            "Maximum redshift (for plot/integration)",         "float",         },
   {"gSIM_EXTRAGAL_NZ",                           "NZ",       "[-]",            "Number of redshift points (only for plotting)",   "float",         },
   {"gSIM_EXTRAGAL_IS_ZLOG",                      "ISZLOG",   "[-]",            "Plot/integrate z-values in log-space",            "boolean (0/1)", },
   {"gSIM_EXTRAGAL_DELTAZ_PRECOMP",               "DZ_GRID",  "[-]",            "Delta z for precomputed P(k,z) at various z ",    "integer",       },

   {"gSIM_IS_SZ",                                 "SZ_BOOL",  "[-]",            "Are we dealing with SZ map?"                      "boolean (0/1)", },

   {"gSIM_EPS",                                   "EPS",      "[-]",            "Numeric precision of the simulation",             "float",         },
   {"gSIM_EPS_DRAWN",                             "EPSDRAWN", "[-]",            "Numeric precision of the drawn clumps",           "float",         },
   {"gSIM_IS_WRITE_GALPOWERSPECTRUM",             "SPECBOOL", "[-]",            "Write angular power spectrum of DM skymap",       "boolean (0/1)", },
   {"gSIM_IS_WRITE_ROOTFILES",                    "ROOTBOOL", "[-]",            "Write output additionally in ROOT format",        "boolean (0/1)", },
   {"gSIM_OUTPUT_DIR",                            "OUT_DIR",  "[-]",            "Output directory (absolute or relative)",         "string or -1",  },
   {"gSIM_SEED",                                  "RANDSEED", "[-]",            "Random generator seed",                           "integer",       },
   {"gSIM_USER_RSE",                              "USER_RSE", "[-]",            "Contrast (%) above which 2D subhalos are drawn",  "float",         },

};

//------------------------------------------------------------------------------
// Define the parameters which can be read from the command line. The order does
// not count.

// NOTE: Something is strange with index 63 - replaced by index gN_INPUTPARAMS
// check for different systems!
struct option gSIM_COMMANDLINE_OPTIONS[] = {
   {"infile",  required_argument, 0, gN_INPUTPARAMS + 1},
   {"readfile",   required_argument, 0, gN_INPUTPARAMS + 2},
   {"print",      no_argument, 0, gN_INPUTPARAMS + 3},
   {"display",    no_argument, 0, gN_INPUTPARAMS + 4},
   {"nothing",    no_argument, 0, gN_INPUTPARAMS + 5},
   {"Default",    no_argument, 0, gN_INPUTPARAMS + 6},
   {"all",        no_argument, 0, gN_INPUTPARAMS + 7},
   {gSIM_INPUTPARAMS[kCOSMO_HUBBLE                        ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCOSMO_HUBBLE                        },
   {gSIM_INPUTPARAMS[kCOSMO_OMEGA0_M                      ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCOSMO_OMEGA0_M                      },
   {gSIM_INPUTPARAMS[kCOSMO_OMEGA0_B                      ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCOSMO_OMEGA0_B                      },
   {gSIM_INPUTPARAMS[kCOSMO_OMEGA0_K                      ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCOSMO_OMEGA0_K                      },
   {gSIM_INPUTPARAMS[kCOSMO_SIGMA8                        ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCOSMO_SIGMA8                        },
   {gSIM_INPUTPARAMS[kCOSMO_N_S                           ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCOSMO_N_S                           },
   {gSIM_INPUTPARAMS[kCOSMO_TAU_REIO                      ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCOSMO_TAU_REIO                      },
   {gSIM_INPUTPARAMS[kCOSMO_WDE                           ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCOSMO_WDE                           },
   {gSIM_INPUTPARAMS[kCOSMO_T0                            ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCOSMO_T0                            },
   {gSIM_INPUTPARAMS[kCOSMO_DELTA0                        ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCOSMO_DELTA0                        },
   {gSIM_INPUTPARAMS[kCOSMO_FLAG_DELTA_REF                ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCOSMO_FLAG_DELTA_REF                },

   {gSIM_INPUTPARAMS[kDM_LOGCDELTA_STDDEV                 ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDM_LOGCDELTA_STDDEV                 },
   {gSIM_INPUTPARAMS[kDM_SUBS_MMIN                        ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDM_SUBS_MMIN                        },
   {gSIM_INPUTPARAMS[kDM_SUBS_MMAXFRAC                    ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDM_SUBS_MMAXFRAC                    },
   {gSIM_INPUTPARAMS[kDM_RHOSAT                           ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDM_RHOSAT                           },
   {gSIM_INPUTPARAMS[kDM_SUBS_NUMBEROFLEVELS              ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDM_SUBS_NUMBEROFLEVELS              },
   {gSIM_INPUTPARAMS[kDM_RHOHALOES_TO_RHOMEAN             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDM_RHOHALOES_TO_RHOMEAN             },
   {gSIM_INPUTPARAMS[kDM_IS_IDM                           ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDM_IS_IDM                           },
   {gSIM_INPUTPARAMS[kDM_KMAX                             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDM_KMAX                             },
   {gSIM_INPUTPARAMS[kDM_KMAX_SIGMA_CUTOFF                ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDM_KMAX_SIGMA_CUTOFF                },

   {gSIM_INPUTPARAMS[kMW_SUBS_FLAG_PROFILE                ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_SUBS_FLAG_PROFILE                },
   {gSIM_INPUTPARAMS[kMW_SUBS_SHAPE_PARAMS_0              ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_SUBS_SHAPE_PARAMS_0              },
   {gSIM_INPUTPARAMS[kMW_SUBS_SHAPE_PARAMS_1              ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_SUBS_SHAPE_PARAMS_1              },
   {gSIM_INPUTPARAMS[kMW_SUBS_SHAPE_PARAMS_2              ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_SUBS_SHAPE_PARAMS_2              },
   {gSIM_INPUTPARAMS[kMW_SUBS_FLAG_CDELTAMDELTA           ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_SUBS_FLAG_CDELTAMDELTA           },
   {gSIM_INPUTPARAMS[kMW_SUBS_DPDV_FLAG_PROFILE           ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_SUBS_DPDV_FLAG_PROFILE           },
   {gSIM_INPUTPARAMS[kMW_SUBS_DPDV_RSCALE_TO_RS_HOST      ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_SUBS_DPDV_RSCALE_TO_RS_HOST      },
   {gSIM_INPUTPARAMS[kMW_SUBS_DPDV_SHAPE_PARAMS_0         ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_SUBS_DPDV_SHAPE_PARAMS_0         },
   {gSIM_INPUTPARAMS[kMW_SUBS_DPDV_SHAPE_PARAMS_1         ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_SUBS_DPDV_SHAPE_PARAMS_1         },
   {gSIM_INPUTPARAMS[kMW_SUBS_DPDV_SHAPE_PARAMS_2         ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_SUBS_DPDV_SHAPE_PARAMS_2         },
   {gSIM_INPUTPARAMS[kMW_SUBS_DPDM_SLOPE                  ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_SUBS_DPDM_SLOPE                  },
   {gSIM_INPUTPARAMS[kMW_SUBS_M1                          ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_SUBS_M1                          },
   {gSIM_INPUTPARAMS[kMW_SUBS_M2                          ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_SUBS_M2                          },
   {gSIM_INPUTPARAMS[kMW_SUBS_N_INM1M2                    ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_SUBS_N_INM1M2                    },

   {gSIM_INPUTPARAMS[kMW_RHOSOL                           ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_RHOSOL                           },
   {gSIM_INPUTPARAMS[kMW_RSOL                             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_RSOL                             },
   {gSIM_INPUTPARAMS[kMW_RMAX                             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_RMAX                             },

   {gSIM_INPUTPARAMS[kMW_TOT_FLAG_PROFILE                 ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_TOT_FLAG_PROFILE                 },
   {gSIM_INPUTPARAMS[kMW_TOT_RSCALE                       ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_TOT_RSCALE                       },
   {gSIM_INPUTPARAMS[kMW_TOT_SHAPE_PARAMS_0               ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_TOT_SHAPE_PARAMS_0               },
   {gSIM_INPUTPARAMS[kMW_TOT_SHAPE_PARAMS_1               ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_TOT_SHAPE_PARAMS_1               },
   {gSIM_INPUTPARAMS[kMW_TOT_SHAPE_PARAMS_2               ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_TOT_SHAPE_PARAMS_2               },

   {gSIM_INPUTPARAMS[kMW_TRIAXIAL_IS                      ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_TRIAXIAL_IS                      },
   {gSIM_INPUTPARAMS[kMW_TRIAXIAL_AXES_0                  ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_TRIAXIAL_AXES_0                  },
   {gSIM_INPUTPARAMS[kMW_TRIAXIAL_AXES_1                  ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_TRIAXIAL_AXES_1                  },
   {gSIM_INPUTPARAMS[kMW_TRIAXIAL_AXES_2                  ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_TRIAXIAL_AXES_2                  },
   {gSIM_INPUTPARAMS[kMW_TRIAXIAL_ROTANGLES_0             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_TRIAXIAL_ROTANGLES_0             },
   {gSIM_INPUTPARAMS[kMW_TRIAXIAL_ROTANGLES_1             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_TRIAXIAL_ROTANGLES_1             },
   {gSIM_INPUTPARAMS[kMW_TRIAXIAL_ROTANGLES_2             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_TRIAXIAL_ROTANGLES_2             },

   {gSIM_INPUTPARAMS[kMW_SUBS_TABULATED_IS                ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_SUBS_TABULATED_IS                },
   {gSIM_INPUTPARAMS[kMW_SUBS_TABULATED_CMIN_OF_R         ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_SUBS_TABULATED_CMIN_OF_R         },
   {gSIM_INPUTPARAMS[kMW_SUBS_TABULATED_LCRIT             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_SUBS_TABULATED_LCRIT             },
   {gSIM_INPUTPARAMS[kMW_SUBS_TABULATED_RTIDAL_TO_RS      ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kMW_SUBS_TABULATED_RTIDAL_TO_RS      },

   {gSIM_INPUTPARAMS[kEXTRAGAL_FLAG_PROFILE               ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_FLAG_PROFILE               },
   {gSIM_INPUTPARAMS[kEXTRAGAL_SHAPE_PARAMS_0             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_SHAPE_PARAMS_0             },
   {gSIM_INPUTPARAMS[kEXTRAGAL_SHAPE_PARAMS_1             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_SHAPE_PARAMS_1             },
   {gSIM_INPUTPARAMS[kEXTRAGAL_SHAPE_PARAMS_2             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_SHAPE_PARAMS_2             },
   {gSIM_INPUTPARAMS[kEXTRAGAL_FLAG_CDELTAMDELTA          ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_FLAG_CDELTAMDELTA          },
   {gSIM_INPUTPARAMS[kEXTRAGAL_FLAG_CDELTAMDELTA_LIST     ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_FLAG_CDELTAMDELTA_LIST     },
   {gSIM_INPUTPARAMS[kEXTRAGAL_FLAG_MASSFUNCTION          ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_FLAG_MASSFUNCTION          },
   {gSIM_INPUTPARAMS[kEXTRAGAL_FLAG_ABSORPTIONPROFILE     ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_FLAG_ABSORPTIONPROFILE     },
   {gSIM_INPUTPARAMS[kEXTRAGAL_SUBS_DPDM_SLOPE            ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_SUBS_DPDM_SLOPE            },
   {gSIM_INPUTPARAMS[kEXTRAGAL_SUBS_DPDM_SLOPE_LIST       ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_SUBS_DPDM_SLOPE_LIST       },
   {gSIM_INPUTPARAMS[kEXTRAGAL_SUBS_DPDV_FLAG_PROFILE     ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_SUBS_DPDV_FLAG_PROFILE     },
   {gSIM_INPUTPARAMS[kEXTRAGAL_SUBS_DPDV_RSCALE_TO_RS_HOST][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_SUBS_DPDV_RSCALE_TO_RS_HOST},
   {gSIM_INPUTPARAMS[kEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_0   ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_0   },
   {gSIM_INPUTPARAMS[kEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_1   ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_1   },
   {gSIM_INPUTPARAMS[kEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_2   ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_2   },
   {gSIM_INPUTPARAMS[kEXTRAGAL_SUBS_MASSFRACTION          ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_SUBS_MASSFRACTION          },
   {gSIM_INPUTPARAMS[kEXTRAGAL_IDM_MHALFMODE              ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_IDM_MHALFMODE              },
   {gSIM_INPUTPARAMS[kEXTRAGAL_IDM_ALPHA                  ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_IDM_ALPHA                  },
   {gSIM_INPUTPARAMS[kEXTRAGAL_IDM_BETA                   ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_IDM_BETA                   },
   {gSIM_INPUTPARAMS[kEXTRAGAL_IDM_GAMMA                  ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_IDM_GAMMA                  },
   {gSIM_INPUTPARAMS[kEXTRAGAL_IDM_DELTA                  ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_IDM_DELTA                  },
   {gSIM_INPUTPARAMS[kEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE  ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE  },
   {gSIM_INPUTPARAMS[kEXTRAGAL_HMF_SMALLSCALE_DPDM_MLIM   ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_HMF_SMALLSCALE_DPDM_MLIM   },

   {gSIM_INPUTPARAMS[kEXTRAGAL_TRIAXIAL_IS                ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, gN_INPUTPARAMS                       },
   {gSIM_INPUTPARAMS[kEXTRAGAL_TRIAXIAL_AXES_0            ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_TRIAXIAL_AXES_0            },
   {gSIM_INPUTPARAMS[kEXTRAGAL_TRIAXIAL_AXES_1            ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_TRIAXIAL_AXES_1            },
   {gSIM_INPUTPARAMS[kEXTRAGAL_TRIAXIAL_AXES_2            ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_TRIAXIAL_AXES_2            },
   {gSIM_INPUTPARAMS[kEXTRAGAL_TRIAXIAL_ROTANGLES_0       ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_TRIAXIAL_ROTANGLES_0       },
   {gSIM_INPUTPARAMS[kEXTRAGAL_TRIAXIAL_ROTANGLES_1       ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_TRIAXIAL_ROTANGLES_1       },
   {gSIM_INPUTPARAMS[kEXTRAGAL_TRIAXIAL_ROTANGLES_2       ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kEXTRAGAL_TRIAXIAL_ROTANGLES_2       },

   {gSIM_INPUTPARAMS[kLIST_HALOES                         ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kLIST_HALOES                         },
   {gSIM_INPUTPARAMS[kLIST_HALOES_JEANS                   ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kLIST_HALOES_JEANS                   },
   {gSIM_INPUTPARAMS[kLIST_HALONAME                       ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kLIST_HALONAME                       },
   {gSIM_INPUTPARAMS[kLIST_HALOES_NODES                   ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kLIST_HALOES_NODES                   },
   {gSIM_INPUTPARAMS[kLIST_HALOES_NODES_RSCALE            ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kLIST_HALOES_NODES_RSCALE            },
   {gSIM_INPUTPARAMS[kLIST_HALO_DISTANCE                  ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kLIST_HALO_DISTANCE                  },
   {gSIM_INPUTPARAMS[kLIST_HALO_RSCALE                    ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kLIST_HALO_RSCALE                    },
   {gSIM_INPUTPARAMS[kLIST_HALO_RHOSCALE                  ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kLIST_HALO_RHOSCALE                  },
   {gSIM_INPUTPARAMS[kLIST_HALO_RDELTA                    ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kLIST_HALO_RDELTA                    },

   {gSIM_INPUTPARAMS[kDSPH_SUBS_FLAG_CDELTAMDELTA         ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDSPH_SUBS_FLAG_CDELTAMDELTA         },
   {gSIM_INPUTPARAMS[kDSPH_SUBS_FLAG_PROFILE              ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDSPH_SUBS_FLAG_PROFILE              },
   {gSIM_INPUTPARAMS[kDSPH_SUBS_SHAPE_PARAMS_0            ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDSPH_SUBS_SHAPE_PARAMS_0            },
   {gSIM_INPUTPARAMS[kDSPH_SUBS_SHAPE_PARAMS_1            ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDSPH_SUBS_SHAPE_PARAMS_1            },
   {gSIM_INPUTPARAMS[kDSPH_SUBS_SHAPE_PARAMS_2            ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDSPH_SUBS_SHAPE_PARAMS_2            },
   {gSIM_INPUTPARAMS[kDSPH_SUBS_DPDM_SLOPE                ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDSPH_SUBS_DPDM_SLOPE                },
   {gSIM_INPUTPARAMS[kDSPH_SUBS_DPDV_FLAG_PROFILE         ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDSPH_SUBS_DPDV_FLAG_PROFILE         },
   {gSIM_INPUTPARAMS[kDSPH_SUBS_DPDV_RSCALE_TO_RS_HOST    ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDSPH_SUBS_DPDV_RSCALE_TO_RS_HOST    },
   {gSIM_INPUTPARAMS[kDSPH_SUBS_DPDV_SHAPE_PARAMS_0       ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDSPH_SUBS_DPDV_SHAPE_PARAMS_0       },
   {gSIM_INPUTPARAMS[kDSPH_SUBS_DPDV_SHAPE_PARAMS_1       ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDSPH_SUBS_DPDV_SHAPE_PARAMS_1       },
   {gSIM_INPUTPARAMS[kDSPH_SUBS_DPDV_SHAPE_PARAMS_2       ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDSPH_SUBS_DPDV_SHAPE_PARAMS_2       },
   {gSIM_INPUTPARAMS[kDSPH_SUBS_MASSFRACTION              ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kDSPH_SUBS_MASSFRACTION              },

   {gSIM_INPUTPARAMS[kGALAXY_SUBS_FLAG_CDELTAMDELTA       ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kGALAXY_SUBS_FLAG_CDELTAMDELTA       },
   {gSIM_INPUTPARAMS[kGALAXY_SUBS_FLAG_PROFILE            ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kGALAXY_SUBS_FLAG_PROFILE            },
   {gSIM_INPUTPARAMS[kGALAXY_SUBS_SHAPE_PARAMS_0          ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kGALAXY_SUBS_SHAPE_PARAMS_0          },
   {gSIM_INPUTPARAMS[kGALAXY_SUBS_SHAPE_PARAMS_1          ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kGALAXY_SUBS_SHAPE_PARAMS_1          },
   {gSIM_INPUTPARAMS[kGALAXY_SUBS_SHAPE_PARAMS_2          ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kGALAXY_SUBS_SHAPE_PARAMS_2          },
   {gSIM_INPUTPARAMS[kGALAXY_SUBS_DPDM_SLOPE              ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kGALAXY_SUBS_DPDM_SLOPE              },
   {gSIM_INPUTPARAMS[kGALAXY_SUBS_DPDV_FLAG_PROFILE       ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kGALAXY_SUBS_DPDV_FLAG_PROFILE       },
   {gSIM_INPUTPARAMS[kGALAXY_SUBS_DPDV_RSCALE_TO_RS_HOST  ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kGALAXY_SUBS_DPDV_RSCALE_TO_RS_HOST  },
   {gSIM_INPUTPARAMS[kGALAXY_SUBS_DPDV_SHAPE_PARAMS_0     ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kGALAXY_SUBS_DPDV_SHAPE_PARAMS_0     },
   {gSIM_INPUTPARAMS[kGALAXY_SUBS_DPDV_SHAPE_PARAMS_1     ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kGALAXY_SUBS_DPDV_SHAPE_PARAMS_1     },
   {gSIM_INPUTPARAMS[kGALAXY_SUBS_DPDV_SHAPE_PARAMS_2     ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kGALAXY_SUBS_DPDV_SHAPE_PARAMS_2     },
   {gSIM_INPUTPARAMS[kGALAXY_SUBS_MASSFRACTION            ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kGALAXY_SUBS_MASSFRACTION            },

   {gSIM_INPUTPARAMS[kCLUSTER_SUBS_FLAG_CDELTAMDELTA      ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCLUSTER_SUBS_FLAG_CDELTAMDELTA      },
   {gSIM_INPUTPARAMS[kCLUSTER_SUBS_FLAG_PROFILE           ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCLUSTER_SUBS_FLAG_PROFILE           },
   {gSIM_INPUTPARAMS[kCLUSTER_SUBS_SHAPE_PARAMS_0         ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCLUSTER_SUBS_SHAPE_PARAMS_0         },
   {gSIM_INPUTPARAMS[kCLUSTER_SUBS_SHAPE_PARAMS_1         ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCLUSTER_SUBS_SHAPE_PARAMS_1         },
   {gSIM_INPUTPARAMS[kCLUSTER_SUBS_SHAPE_PARAMS_2         ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCLUSTER_SUBS_SHAPE_PARAMS_2         },
   {gSIM_INPUTPARAMS[kCLUSTER_SUBS_DPDM_SLOPE             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCLUSTER_SUBS_DPDM_SLOPE             },
   {gSIM_INPUTPARAMS[kCLUSTER_SUBS_DPDV_FLAG_PROFILE      ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCLUSTER_SUBS_DPDV_FLAG_PROFILE      },
   {gSIM_INPUTPARAMS[kCLUSTER_SUBS_DPDV_RSCALE_TO_RS_HOST ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCLUSTER_SUBS_DPDV_RSCALE_TO_RS_HOST },
   {gSIM_INPUTPARAMS[kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_0    ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_0    },
   {gSIM_INPUTPARAMS[kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_1    ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_1    },
   {gSIM_INPUTPARAMS[kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_2    ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_2    },
   {gSIM_INPUTPARAMS[kCLUSTER_SUBS_MASSFRACTION           ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kCLUSTER_SUBS_MASSFRACTION           },

   {gSIM_INPUTPARAMS[kPP_BR                               ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kPP_BR                               },

   {gSIM_INPUTPARAMS[kPP_DM_ANNIHIL_DELTA                 ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kPP_DM_ANNIHIL_DELTA                 },
   {gSIM_INPUTPARAMS[kPP_DM_ANNIHIL_SIGMAV_CM3PERS        ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kPP_DM_ANNIHIL_SIGMAV_CM3PERS        },
   {gSIM_INPUTPARAMS[kPP_DM_DECAY_LIFETIME_S              ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kPP_DM_DECAY_LIFETIME_S              },
   {gSIM_INPUTPARAMS[kPP_DM_IS_ANNIHIL_OR_DECAY           ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kPP_DM_IS_ANNIHIL_OR_DECAY           },
   {gSIM_INPUTPARAMS[kPP_DM_MASS_GEV                      ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kPP_DM_MASS_GEV                      },
   {gSIM_INPUTPARAMS[kPP_FLAG_SPECTRUMMODEL               ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kPP_FLAG_SPECTRUMMODEL               },
   {gSIM_INPUTPARAMS[kPP_NUMIXING_THETA12_DEG             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kPP_NUMIXING_THETA12_DEG             },
   {gSIM_INPUTPARAMS[kPP_NUMIXING_THETA13_DEG             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kPP_NUMIXING_THETA13_DEG             },
   {gSIM_INPUTPARAMS[kPP_NUMIXING_THETA23_DEG             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kPP_NUMIXING_THETA23_DEG             },

   {gSIM_INPUTPARAMS[kSZ_REF_C500                         ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSZ_REF_C500                         },
   {gSIM_INPUTPARAMS[kSZ_REF_PRESSURE_NORM                ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSZ_REF_PRESSURE_NORM                },
   {gSIM_INPUTPARAMS[kSZ_REF_SHAPE_PARAMS_0               ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSZ_REF_SHAPE_PARAMS_0               },
   {gSIM_INPUTPARAMS[kSZ_REF_SHAPE_PARAMS_1               ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSZ_REF_SHAPE_PARAMS_1               },
   {gSIM_INPUTPARAMS[kSZ_REF_SHAPE_PARAMS_2               ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSZ_REF_SHAPE_PARAMS_2               },
   {gSIM_INPUTPARAMS[kSZ_REF_ALPHA_MYX                    ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSZ_REF_ALPHA_MYX                    },
   {gSIM_INPUTPARAMS[kSZ_FLAG_MASSFUNCTION                ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSZ_FLAG_MASSFUNCTION                },

   {gSIM_INPUTPARAMS[kSTAT_FILES                          ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSTAT_FILES                          },
   {gSIM_INPUTPARAMS[kSTAT_CL                             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSTAT_CL                             },
   {gSIM_INPUTPARAMS[kSTAT_CL_LIST                        ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSTAT_CL_LIST                        },
   {gSIM_INPUTPARAMS[kSTAT_ID_LIST                        ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSTAT_ID_LIST                        },
   {gSIM_INPUTPARAMS[kSTAT_MODE                           ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSTAT_MODE                           },
   {gSIM_INPUTPARAMS[kSTAT_IS_LOGL_OR_CHI2                ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSTAT_IS_LOGL_OR_CHI2                },
   {gSIM_INPUTPARAMS[kSTAT_RKPC_FOR_MR                    ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSTAT_RKPC_FOR_MR                    },
   {gSIM_INPUTPARAMS[kSTAT_DATAFILES                      ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSTAT_DATAFILES                      },
   {gSIM_INPUTPARAMS[kSTAT_N_REALIZATIONS                 ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSTAT_N_REALIZATIONS                 },

   {gSIM_INPUTPARAMS[kSIM_R_MIN                           ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_R_MIN                           },
   {gSIM_INPUTPARAMS[kSIM_R_MAX                           ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_R_MAX                           },
   {gSIM_INPUTPARAMS[kSIM_NX                              ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_NX                              },
   {gSIM_INPUTPARAMS[kSIM_IS_XLOG                         ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_IS_XLOG                         },
   {gSIM_INPUTPARAMS[kSIM_XPOWER                          ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_XPOWER                          },
   {gSIM_INPUTPARAMS[kSIM_ALPHAINT                        ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_ALPHAINT                        },
   {gSIM_INPUTPARAMS[kSIM_ALPHAINT_MIN                    ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_ALPHAINT_MIN                    },
   {gSIM_INPUTPARAMS[kSIM_ALPHAINT_MAX                    ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_ALPHAINT_MAX                    },
   {gSIM_INPUTPARAMS[kSIM_THETA_MIN                       ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_THETA_MIN                       },
   {gSIM_INPUTPARAMS[kSIM_THETA_MAX                       ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_THETA_MAX                       },
   {gSIM_INPUTPARAMS[kSIM_SORT_CONTRAST_THRESH            ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_SORT_CONTRAST_THRESH            },
   {gSIM_INPUTPARAMS[kSIM_PHI_CUT                         ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_PHI_CUT                         },

   {gSIM_INPUTPARAMS[kSIM_PSI_OBS                         ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_PSI_OBS                         },
   {gSIM_INPUTPARAMS[kSIM_THETA_OBS                       ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_THETA_OBS                       },
   {gSIM_INPUTPARAMS[kSIM_THETA_ORTH_SIZE                 ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_THETA_ORTH_SIZE                 },
   {gSIM_INPUTPARAMS[kSIM_THETA_SIZE                      ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_THETA_SIZE                      },

   {gSIM_INPUTPARAMS[kSIM_IS_ASTRO_OR_PP_UNITS            ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_IS_ASTRO_OR_PP_UNITS            },

   {gSIM_INPUTPARAMS[kSIM_HEALPIX_NSIDE                   ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_HEALPIX_NSIDE                   },
   {gSIM_INPUTPARAMS[kSIM_HEALPIX_RING_WEIGHTS_DIR        ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_HEALPIX_RING_WEIGHTS_DIR        },
   {gSIM_INPUTPARAMS[kSIM_HEALPIX_ITER                    ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_HEALPIX_ITER                    },
   {gSIM_INPUTPARAMS[kSIM_HEALPIX_NLMAX_FAC               ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_HEALPIX_NLMAX_FAC               },
   {gSIM_INPUTPARAMS[kSIM_HEALPIX_FITS_DATATYPE           ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_HEALPIX_FITS_DATATYPE           },
   {gSIM_INPUTPARAMS[kSIM_HEALPIX_SCHEME                  ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_HEALPIX_SCHEME                  },

   {gSIM_INPUTPARAMS[kSIM_IS_WRITE_FLUXMAPS               ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_IS_WRITE_FLUXMAPS               },
   {gSIM_INPUTPARAMS[kSIM_FLUX_IS_INTEG_OR_DIFF           ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_FLUX_IS_INTEG_OR_DIFF },
   {gSIM_INPUTPARAMS[kSIM_FLUX_AT_E_GEV                   ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_FLUX_AT_E_GEV                   },
   {gSIM_INPUTPARAMS[kSIM_FLUX_EMIN_GEV                   ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_FLUX_EMIN_GEV                   },
   {gSIM_INPUTPARAMS[kSIM_FLUX_EMAX_GEV                   ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_FLUX_EMAX_GEV                   },
   {gSIM_INPUTPARAMS[kSIM_FLUX_FLAG_NUFLAVOUR             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_FLUX_FLAG_NUFLAVOUR             },
   {gSIM_INPUTPARAMS[kSIM_FLUX_FLAG_FINALSTATE            ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_FLUX_FLAG_FINALSTATE            },
   {gSIM_INPUTPARAMS[kSIM_JFACTOR                         ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_JFACTOR                         },
   {gSIM_INPUTPARAMS[kSIM_JFRACTION                       ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_JFRACTION                       },

   {gSIM_INPUTPARAMS[kSIM_GAUSSBEAM_GAMMA_FWHM            ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_GAUSSBEAM_GAMMA_FWHM            },
   {gSIM_INPUTPARAMS[kSIM_GAUSSBEAM_NEUTRINO_FWHM         ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_GAUSSBEAM_NEUTRINO_FWHM         },

   {gSIM_INPUTPARAMS[kSIM_REDSHIFT                        ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_REDSHIFT                        },
   {gSIM_INPUTPARAMS[kSIM_EXTRAGAL_ZMIN                   ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_EXTRAGAL_ZMIN                   },
   {gSIM_INPUTPARAMS[kSIM_EXTRAGAL_ZMAX                   ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_EXTRAGAL_ZMAX                   },
   {gSIM_INPUTPARAMS[kSIM_EXTRAGAL_NZ                     ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_EXTRAGAL_NZ                     },
   {gSIM_INPUTPARAMS[kSIM_EXTRAGAL_IS_ZLOG                ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_EXTRAGAL_IS_ZLOG                },
   {gSIM_INPUTPARAMS[kSIM_EXTRAGAL_DELTAZ_PRECOMP         ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_EXTRAGAL_DELTAZ_PRECOMP         },
   {gSIM_INPUTPARAMS[kSIM_EXTRAGAL_MMIN                   ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_EXTRAGAL_MMIN                   },
   {gSIM_INPUTPARAMS[kSIM_EXTRAGAL_MMAX                   ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_EXTRAGAL_MMAX                   },
   {gSIM_INPUTPARAMS[kSIM_EXTRAGAL_NM                     ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_EXTRAGAL_NM                     },
   {gSIM_INPUTPARAMS[kSIM_EXTRAGAL_IS_MLOG                ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_EXTRAGAL_IS_MLOG                },
   {gSIM_INPUTPARAMS[kSIM_EXTRAGAL_NM_PRECOMP             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_EXTRAGAL_NM_PRECOMP             },
   {gSIM_INPUTPARAMS[kSIM_EXTRAGAL_FLAG_WINDOWFUNC        ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_EXTRAGAL_FLAG_WINDOWFUNC        },
   {gSIM_INPUTPARAMS[kSIM_EXTRAGAL_MF_SIGMA_CUTOFF        ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_EXTRAGAL_MF_SIGMA_CUTOFF        },
   {gSIM_INPUTPARAMS[kSIM_EXTRAGAL_KMAX_PRECOMP           ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_EXTRAGAL_KMAX_PRECOMP           },
   {gSIM_INPUTPARAMS[kSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE][kSIM_INPUTPARAM_VARNAME], required_argument, 0, kSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE},
   {gSIM_INPUTPARAMS[kSIM_EXTRAGAL_EBL_UNCERTAINTY        ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_EXTRAGAL_EBL_UNCERTAINTY        },
   {gSIM_INPUTPARAMS[kSIM_EXTRAGAL_FLAG_GROWTHFACTOR      ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_EXTRAGAL_FLAG_GROWTHFACTOR      },

   {gSIM_INPUTPARAMS[kSIM_IS_SZ                           ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_IS_SZ                           },

   {gSIM_INPUTPARAMS[kSIM_EPS                             ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_EPS                             },
   {gSIM_INPUTPARAMS[kSIM_EPS_DRAWN                       ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_EPS_DRAWN                       },
   {gSIM_INPUTPARAMS[kSIM_USER_RSE                        ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_USER_RSE                        },
   {gSIM_INPUTPARAMS[kSIM_SEED                            ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_SEED                            },
   {gSIM_INPUTPARAMS[kSIM_OUTPUT_DIR                      ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_OUTPUT_DIR                      },

   {gSIM_INPUTPARAMS[kSIM_IS_WRITE_GALPOWERSPECTRUM       ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_IS_WRITE_GALPOWERSPECTRUM       },
   {gSIM_INPUTPARAMS[kSIM_IS_WRITE_ROOTFILES              ][kSIM_INPUTPARAM_VARNAME],  required_argument, 0, kSIM_IS_WRITE_ROOTFILES              },

   {0, 0, 0, 0}
};



vector< vector<int> > gPARAMS_REQUIRED;
vector< vector<int> > gPARAMS_HIDDEN;
//______________________________________________________________________________
void get_required_params()
{

   // Define which global parameters are demanded as input parameters from the user
   // for each of the different clumpy runs. Depending on the choice of various parameters,
   // more enumerations can be added for each run at the load_parameters() stage
   // (e.g., gSIM_IS_WRITE_FLUXMAPS = true -> require info about flux)

   // put into function as shitty workaround to work with <C++2011 compilers.

   gPARAMS_REQUIRED.clear();// clear vector CHECK!
   gPARAMS_HIDDEN.clear();  // clear vector CHECK!

   int vars_g0[] = {kSIM_EPS, kSIM_R_MIN, kSIM_R_MAX, kSIM_NX, kSIM_IS_XLOG, kSIM_OUTPUT_DIR,
                    kSIM_IS_WRITE_ROOTFILES, kMW_TOT_RSCALE,
                    kMW_TOT_FLAG_PROFILE, kMW_RHOSOL, kMW_RSOL, kMW_RMAX,
                    kCOSMO_DELTA0, kCOSMO_FLAG_DELTA_REF,
                    kMW_TRIAXIAL_IS,
                    kDM_RHOSAT
                   };
   int vars_g0to3_hidden[] = {kCOSMO_HUBBLE, kCOSMO_OMEGA0_M, kCOSMO_OMEGA0_K, kCOSMO_T0};

   int vars_g1[] = {kSIM_EPS, kSIM_R_MIN, kSIM_R_MAX, kSIM_NX, kSIM_IS_XLOG, kSIM_OUTPUT_DIR, kSIM_IS_WRITE_ROOTFILES,
                    kSIM_IS_ASTRO_OR_PP_UNITS,
                    kDM_RHOSAT,
                    kMW_SUBS_N_INM1M2,
                    kMW_TOT_FLAG_PROFILE, kMW_TOT_RSCALE,
                    kMW_RHOSOL, kMW_RSOL, kMW_RMAX, kCOSMO_DELTA0, kCOSMO_FLAG_DELTA_REF
                   };

   int vars_g2[] = {kSIM_EPS, kSIM_ALPHAINT_MIN, kSIM_ALPHAINT_MAX, kSIM_NX, kSIM_PSI_OBS, kSIM_THETA_OBS,
                    kSIM_IS_XLOG, kSIM_OUTPUT_DIR, kSIM_IS_WRITE_ROOTFILES,
                    kSIM_IS_ASTRO_OR_PP_UNITS, kSIM_IS_WRITE_FLUXMAPS,
                    kDM_RHOSAT,
                    kDM_SUBS_NUMBEROFLEVELS,
                    kMW_TOT_FLAG_PROFILE, kMW_TOT_RSCALE,
                    kMW_RHOSOL, kMW_RSOL, kMW_RMAX,
                    kMW_TRIAXIAL_IS, kPP_DM_IS_ANNIHIL_OR_DECAY,
                    kCOSMO_DELTA0, kCOSMO_FLAG_DELTA_REF
                   };

   int vars_g3[] = {kSIM_EPS, kSIM_THETA_MIN, kSIM_THETA_MAX, kSIM_NX, kSIM_ALPHAINT,
                    kSIM_IS_XLOG, kSIM_OUTPUT_DIR, kSIM_IS_WRITE_ROOTFILES,
                    kSIM_IS_ASTRO_OR_PP_UNITS, kSIM_IS_WRITE_FLUXMAPS,
                    kDM_RHOSAT,
                    kDM_SUBS_NUMBEROFLEVELS,
                    kMW_TOT_FLAG_PROFILE, kMW_TOT_RSCALE,
                    kMW_RHOSOL, kMW_RSOL, kMW_RMAX,
                    kPP_DM_IS_ANNIHIL_OR_DECAY,
                    kCOSMO_DELTA0, kCOSMO_FLAG_DELTA_REF
                   };

   int vars_g4[] = {kLIST_HALOES, kSIM_EPS, kSIM_SORT_CONTRAST_THRESH, kSIM_PHI_CUT,
                    kSIM_THETA_MIN, kSIM_THETA_MAX, kSIM_NX, kSIM_ALPHAINT,
                    kSIM_IS_XLOG, kSIM_OUTPUT_DIR, kSIM_IS_WRITE_ROOTFILES,
                    kSIM_IS_ASTRO_OR_PP_UNITS, kSIM_IS_WRITE_FLUXMAPS,
                    kDM_RHOSAT,
                    kDM_SUBS_NUMBEROFLEVELS,
                    kMW_TOT_FLAG_PROFILE, kMW_TOT_RSCALE,
                    kMW_RHOSOL, kMW_RSOL, kMW_RMAX,
                    kPP_DM_IS_ANNIHIL_OR_DECAY,
                    kCOSMO_DELTA0, kCOSMO_FLAG_DELTA_REF
                   };
   int vars_g4_hidden[] = {kCOSMO_HUBBLE, kCOSMO_OMEGA0_M, kCOSMO_OMEGA0_K, kCOSMO_T0,
                           kLIST_HALOES_NODES, kLIST_HALOES_NODES_RSCALE
                          };

   int vars_g5[] = {kSIM_EPS, kSIM_OUTPUT_DIR,
                    kSIM_PSI_OBS, kSIM_THETA_OBS, kSIM_THETA_ORTH_SIZE, kSIM_THETA_SIZE,
                    kSIM_IS_ASTRO_OR_PP_UNITS, kSIM_HEALPIX_NSIDE,
                    kSIM_IS_WRITE_FLUXMAPS, kSIM_GAUSSBEAM_GAMMA_FWHM,
                    kSIM_GAUSSBEAM_NEUTRINO_FWHM, kSIM_IS_WRITE_GALPOWERSPECTRUM, kSIM_IS_WRITE_ROOTFILES,
                    kDM_RHOSAT,
                    kDM_SUBS_NUMBEROFLEVELS,
                    kMW_TOT_FLAG_PROFILE, kMW_TOT_RSCALE,
                    kMW_RHOSOL, kMW_RSOL, kMW_RMAX,
                    kMW_TRIAXIAL_IS, kPP_DM_IS_ANNIHIL_OR_DECAY,
                    kCOSMO_DELTA0, kCOSMO_FLAG_DELTA_REF
                   };
   int vars_g5_hidden[] = {kCOSMO_HUBBLE, kCOSMO_OMEGA0_M, kCOSMO_OMEGA0_K, kCOSMO_T0,
                           kSIM_HEALPIX_ITER, kSIM_HEALPIX_NLMAX_FAC, kSIM_HEALPIX_FITS_DATATYPE,
                           kSIM_HEALPIX_SCHEME, kSIM_HEALPIX_RING_WEIGHTS_DIR, kSIM_ALPHAINT
                          };

   int vars_g6[] = {kLIST_HALOES, kSIM_EPS, kSIM_OUTPUT_DIR,
                    kSIM_PSI_OBS, kSIM_THETA_OBS, kSIM_THETA_ORTH_SIZE, kSIM_THETA_SIZE,
                    kSIM_IS_ASTRO_OR_PP_UNITS, kSIM_HEALPIX_NSIDE,
                    kSIM_IS_WRITE_FLUXMAPS, kSIM_GAUSSBEAM_GAMMA_FWHM,
                    kSIM_GAUSSBEAM_NEUTRINO_FWHM, kSIM_IS_WRITE_GALPOWERSPECTRUM, kSIM_IS_WRITE_ROOTFILES,
                    kDM_RHOSAT,
                    kDM_SUBS_NUMBEROFLEVELS,
                    kMW_RHOSOL, kMW_RSOL, kMW_RMAX,
                    kMW_TOT_FLAG_PROFILE, kMW_TOT_RSCALE,
                    kMW_TRIAXIAL_IS, kPP_DM_IS_ANNIHIL_OR_DECAY,
                    kCOSMO_DELTA0, kCOSMO_FLAG_DELTA_REF
                   };
   int vars_g6_hidden[] = {kCOSMO_HUBBLE, kCOSMO_OMEGA0_M, kCOSMO_OMEGA0_K, kCOSMO_T0,
                           kSIM_HEALPIX_ITER, kSIM_HEALPIX_NLMAX_FAC, kSIM_HEALPIX_FITS_DATATYPE,
                           kSIM_HEALPIX_SCHEME, kSIM_HEALPIX_RING_WEIGHTS_DIR, kSIM_ALPHAINT,
                           kLIST_HALOES_NODES, kLIST_HALOES_NODES_RSCALE
                          };

   int vars_g7[] = {kSIM_USER_RSE, kSIM_EPS, kSIM_SEED, kSIM_OUTPUT_DIR,
                    kSIM_PSI_OBS, kSIM_THETA_OBS, kSIM_THETA_ORTH_SIZE, kSIM_THETA_SIZE,
                    kSIM_IS_ASTRO_OR_PP_UNITS, kSIM_HEALPIX_NSIDE,
                    kSIM_IS_WRITE_FLUXMAPS, kSIM_GAUSSBEAM_GAMMA_FWHM,
                    kSIM_GAUSSBEAM_NEUTRINO_FWHM, kSIM_IS_WRITE_GALPOWERSPECTRUM, kSIM_IS_WRITE_ROOTFILES,
                    kDM_RHOSAT,
                    kDM_SUBS_NUMBEROFLEVELS,
                    kMW_SUBS_TABULATED_IS, kMW_SUBS_TABULATED_CMIN_OF_R,
                    kMW_SUBS_TABULATED_LCRIT, kMW_SUBS_TABULATED_RTIDAL_TO_RS,
                    kMW_TOT_FLAG_PROFILE, kMW_TOT_RSCALE,
                    kMW_RHOSOL, kMW_RSOL, kMW_RMAX,
                    kMW_TRIAXIAL_IS, kPP_DM_IS_ANNIHIL_OR_DECAY,
                    kCOSMO_DELTA0, kCOSMO_FLAG_DELTA_REF
                   };
   int vars_g7_hidden[] = {kCOSMO_HUBBLE, kCOSMO_OMEGA0_M, kCOSMO_OMEGA0_K, kCOSMO_T0,
                           kSIM_HEALPIX_ITER, kSIM_HEALPIX_NLMAX_FAC, kSIM_HEALPIX_FITS_DATATYPE,
                           kSIM_HEALPIX_SCHEME, kSIM_HEALPIX_RING_WEIGHTS_DIR, kSTAT_N_REALIZATIONS,
                           kSIM_ALPHAINT
                          };

   int vars_g8[] = {kLIST_HALOES, kSIM_EPS, kSIM_USER_RSE, kSIM_SEED, kSIM_OUTPUT_DIR,
                    kSIM_PSI_OBS, kSIM_THETA_OBS, kSIM_THETA_ORTH_SIZE, kSIM_THETA_SIZE,
                    kSIM_IS_ASTRO_OR_PP_UNITS, kSIM_HEALPIX_NSIDE,
                    kSIM_IS_WRITE_FLUXMAPS, kSIM_GAUSSBEAM_GAMMA_FWHM,
                    kSIM_GAUSSBEAM_NEUTRINO_FWHM, kSIM_IS_WRITE_GALPOWERSPECTRUM, kSIM_IS_WRITE_ROOTFILES,
                    kDM_RHOSAT,
                    kDM_SUBS_NUMBEROFLEVELS,
                    kMW_TOT_FLAG_PROFILE, kMW_TOT_RSCALE,
                    kMW_RHOSOL, kMW_RSOL, kMW_RMAX,
                    kMW_TRIAXIAL_IS, kPP_DM_IS_ANNIHIL_OR_DECAY,
                    kCOSMO_DELTA0, kCOSMO_FLAG_DELTA_REF
                   };
   int vars_g8_hidden[] = {kCOSMO_HUBBLE, kCOSMO_OMEGA0_M, kCOSMO_OMEGA0_K, kCOSMO_T0,
                           kSIM_HEALPIX_ITER, kSIM_HEALPIX_NLMAX_FAC, kSIM_HEALPIX_FITS_DATATYPE,
                           kSIM_HEALPIX_SCHEME, kSIM_HEALPIX_RING_WEIGHTS_DIR, kSTAT_N_REALIZATIONS,
                           kSIM_ALPHAINT, kLIST_HALOES_NODES, kLIST_HALOES_NODES_RSCALE
                          };

   int vars_h0[] = {kCOSMO_DELTA0, kCOSMO_FLAG_DELTA_REF, kSIM_EPS,
                    kSIM_R_MIN, kSIM_R_MAX, kSIM_NX, kSIM_IS_XLOG, kSIM_OUTPUT_DIR,
                    kSIM_IS_WRITE_ROOTFILES, kLIST_HALOES,
                    kDM_RHOSAT
                   };
   int vars_h0to3_hidden[] = {kCOSMO_HUBBLE, kCOSMO_OMEGA0_M, kCOSMO_OMEGA0_K, kCOSMO_T0,
                              kLIST_HALOES_NODES, kLIST_HALOES_NODES_RSCALE
                             };

   int vars_h1[] = {kCOSMO_DELTA0, kCOSMO_FLAG_DELTA_REF, kSIM_EPS,
                    kSIM_R_MIN, kSIM_R_MAX, kSIM_NX, kSIM_IS_XLOG, kSIM_OUTPUT_DIR,
                    kSIM_IS_WRITE_ROOTFILES, kLIST_HALOES, kSIM_IS_ASTRO_OR_PP_UNITS,
                    kDM_RHOSAT,
                    kDM_SUBS_NUMBEROFLEVELS
                   };

   int vars_h2[] = {kCOSMO_DELTA0, kCOSMO_FLAG_DELTA_REF, kSIM_EPS,
                    kSIM_ALPHAINT_MIN, kSIM_ALPHAINT_MAX, kSIM_NX,
                    kSIM_IS_XLOG, kSIM_OUTPUT_DIR, kSIM_IS_WRITE_ROOTFILES,
                    kSIM_IS_ASTRO_OR_PP_UNITS, kSIM_IS_WRITE_FLUXMAPS,
                    kDM_RHOSAT,
                    kDM_SUBS_NUMBEROFLEVELS,
                    kPP_DM_IS_ANNIHIL_OR_DECAY,
                    kLIST_HALOES
                   };

   int vars_h3[] = {kCOSMO_DELTA0, kCOSMO_FLAG_DELTA_REF, kSIM_EPS,
                    kSIM_ALPHAINT,
                    kSIM_THETA_MIN, kSIM_THETA_MAX, kSIM_NX,
                    kSIM_IS_XLOG, kSIM_OUTPUT_DIR, kSIM_IS_WRITE_ROOTFILES,
                    kSIM_IS_ASTRO_OR_PP_UNITS, kSIM_IS_WRITE_FLUXMAPS,
                    kDM_RHOSAT,
                    kDM_SUBS_NUMBEROFLEVELS,
                    kPP_DM_IS_ANNIHIL_OR_DECAY,
                    kLIST_HALOES,
                   };

   int vars_h4[] = {kCOSMO_DELTA0, kCOSMO_FLAG_DELTA_REF, kSIM_EPS,
                    kSIM_THETA_ORTH_SIZE,
                    kSIM_THETA_SIZE,
                    kSIM_HEALPIX_NSIDE,
                    kSIM_IS_WRITE_FLUXMAPS, kSIM_GAUSSBEAM_GAMMA_FWHM,
                    kSIM_GAUSSBEAM_NEUTRINO_FWHM,
                    kSIM_OUTPUT_DIR, kSIM_IS_WRITE_ROOTFILES,
                    kSIM_IS_ASTRO_OR_PP_UNITS, kSIM_IS_WRITE_FLUXMAPS,
                    kDM_RHOSAT,
                    kDM_SUBS_NUMBEROFLEVELS,
                    kPP_DM_IS_ANNIHIL_OR_DECAY,
                    kLIST_HALOES,
                    kLIST_HALONAME,
                    // will be loaded from external file:
                    kSIM_PSI_OBS,
                    kSIM_THETA_OBS,
                    kLIST_HALO_DISTANCE,
                    kSIM_REDSHIFT,
                    kLIST_HALO_RDELTA,
                    kLIST_HALO_RHOSCALE,
                    kLIST_HALO_RSCALE,
                    kEXTRAGAL_FLAG_PROFILE,
                    kEXTRAGAL_SHAPE_PARAMS_0,
                    kEXTRAGAL_SHAPE_PARAMS_1,
                    kEXTRAGAL_SHAPE_PARAMS_2,
                    kEXTRAGAL_TRIAXIAL_IS,
                    kEXTRAGAL_TRIAXIAL_AXES_0,
                    kEXTRAGAL_TRIAXIAL_AXES_1,
                    kEXTRAGAL_TRIAXIAL_AXES_2,
                    kEXTRAGAL_TRIAXIAL_ROTANGLES_0,
                    kEXTRAGAL_TRIAXIAL_ROTANGLES_1,
                    kEXTRAGAL_TRIAXIAL_ROTANGLES_2
                   };

   int vars_h4toh5_hidden[] = {kCOSMO_HUBBLE, kCOSMO_OMEGA0_M, kCOSMO_OMEGA0_K, kCOSMO_T0,
                               kSIM_HEALPIX_FITS_DATATYPE,
                               kSIM_HEALPIX_SCHEME,
                               kSIM_HEALPIX_ITER, kSIM_HEALPIX_NLMAX_FAC,
                               kSIM_HEALPIX_RING_WEIGHTS_DIR,
                               kSIM_ALPHAINT,
                               kLIST_HALOES_NODES, kLIST_HALOES_NODES_RSCALE
                              };

   int vars_h5[] = {kCOSMO_DELTA0, kCOSMO_FLAG_DELTA_REF, kSIM_EPS,
                    kSIM_THETA_ORTH_SIZE,
                    kSIM_THETA_SIZE,
                    kSIM_HEALPIX_NSIDE,
                    kSIM_IS_WRITE_FLUXMAPS, kSIM_GAUSSBEAM_GAMMA_FWHM,
                    kSIM_GAUSSBEAM_NEUTRINO_FWHM,
                    kSIM_OUTPUT_DIR, kSIM_IS_WRITE_ROOTFILES,
                    kSIM_IS_ASTRO_OR_PP_UNITS, kSIM_IS_WRITE_FLUXMAPS,
                    kDM_RHOSAT,
                    kDM_SUBS_NUMBEROFLEVELS,
                    kPP_DM_IS_ANNIHIL_OR_DECAY,
                    kLIST_HALOES,
                    kLIST_HALONAME,
                    kSIM_USER_RSE,
                    kSIM_SEED,
                    // will be loaded from external file:
                    kSIM_PSI_OBS,
                    kSIM_THETA_OBS,
                    kLIST_HALO_DISTANCE,
                    kSIM_REDSHIFT,
                    kLIST_HALO_RDELTA,
                    kLIST_HALO_RHOSCALE,
                    kLIST_HALO_RSCALE,
                    kEXTRAGAL_FLAG_PROFILE,
                    kEXTRAGAL_SHAPE_PARAMS_0,
                    kEXTRAGAL_SHAPE_PARAMS_1,
                    kEXTRAGAL_SHAPE_PARAMS_2,
                    kEXTRAGAL_TRIAXIAL_IS,
                    kEXTRAGAL_TRIAXIAL_AXES_0,
                    kEXTRAGAL_TRIAXIAL_AXES_1,
                    kEXTRAGAL_TRIAXIAL_AXES_2,
                    kEXTRAGAL_TRIAXIAL_ROTANGLES_0,
                    kEXTRAGAL_TRIAXIAL_ROTANGLES_1,
                    kEXTRAGAL_TRIAXIAL_ROTANGLES_2
                   };

   int vars_h6[] = {kLIST_HALOES, kSIM_JFRACTION, kPP_DM_IS_ANNIHIL_OR_DECAY,
                    kDM_RHOSAT,
                    kDM_SUBS_NUMBEROFLEVELS,
                    kSIM_EPS
                   };
   int vars_h6_hidden[] = {kCOSMO_HUBBLE, kCOSMO_OMEGA0_M, kCOSMO_OMEGA0_K, kCOSMO_T0,
                           kLIST_HALOES_NODES, kLIST_HALOES_NODES_RSCALE
                          };

   int vars_h7[] = {kLIST_HALOES, kSIM_EPS, kSIM_ALPHAINT, kSIM_JFRACTION, kPP_DM_IS_ANNIHIL_OR_DECAY,
                    kDM_RHOSAT,
                    kDM_SUBS_NUMBEROFLEVELS,
                   };
   int vars_h7_hidden[] = {kCOSMO_HUBBLE, kCOSMO_OMEGA0_M, kCOSMO_OMEGA0_K, kCOSMO_T0,
                           kLIST_HALOES_NODES, kLIST_HALOES_NODES_RSCALE
                          };

   int vars_h8toh10[] = {kSIM_EPS,
                         kSIM_R_MIN,
                         kSIM_R_MAX,
                         kSIM_NX,
                         kSIM_IS_XLOG,
                         kLIST_HALOES_JEANS,
                         kSIM_OUTPUT_DIR
                        };
   int vars_h8toh10_hidden[] = {kCOSMO_HUBBLE, kCOSMO_OMEGA0_M, kCOSMO_OMEGA0_K, kCOSMO_T0,
                                kLIST_HALOES_NODES, kLIST_HALOES_NODES_RSCALE
                               };


   int vars_e0[] = {kCOSMO_HUBBLE, kCOSMO_OMEGA0_M, kCOSMO_OMEGA0_B,
                    kCOSMO_T0,
                    kSIM_EXTRAGAL_ZMIN, kSIM_EXTRAGAL_ZMAX, kSIM_EXTRAGAL_NZ,
                    kSIM_EXTRAGAL_IS_ZLOG,
                    kSIM_OUTPUT_DIR,
                    kSIM_IS_WRITE_ROOTFILES,
                    kSIM_EPS
                   };

   int vars_e0_hidden[] = {kCOSMO_OMEGA0_K
                          };

   int vars_e1[] = {kCOSMO_HUBBLE, kCOSMO_OMEGA0_M, kCOSMO_OMEGA0_B,
                    kCOSMO_SIGMA8, kCOSMO_N_S, kCOSMO_TAU_REIO,
                    kEXTRAGAL_FLAG_MASSFUNCTION,
                    kDM_RHOSAT,
                    kEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE,
                    kSIM_EPS,
                    kSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE,
                    kSIM_EXTRAGAL_FLAG_WINDOWFUNC,
                    kSIM_EXTRAGAL_MMIN, kSIM_EXTRAGAL_MMAX, kSIM_EXTRAGAL_NM,
                    kSIM_EXTRAGAL_ZMIN, kSIM_EXTRAGAL_ZMAX, kSIM_EXTRAGAL_NZ,
                    kSIM_EXTRAGAL_IS_ZLOG,  kSIM_XPOWER,
                    kSIM_IS_WRITE_ROOTFILES,
                    kCOSMO_DELTA0,
                    kCOSMO_FLAG_DELTA_REF,
                    kDM_KMAX,
                    kDM_IS_IDM,
                    kSIM_OUTPUT_DIR
                   };

   int vars_e1_hidden[] = {kCOSMO_OMEGA0_K, kCOSMO_T0,
                           kDM_RHOHALOES_TO_RHOMEAN,
                           kDM_KMAX_SIGMA_CUTOFF,
                           kSIM_EXTRAGAL_MF_SIGMA_CUTOFF,
                           kSIM_EXTRAGAL_KMAX_PRECOMP,
                           kSIM_EXTRAGAL_NM_PRECOMP, kSIM_EXTRAGAL_DELTAZ_PRECOMP, kSIM_EXTRAGAL_IS_MLOG,
                           kSIM_EXTRAGAL_FLAG_GROWTHFACTOR
                          };

   int vars_e2[] = {kSIM_EPS, kSIM_OUTPUT_DIR, kSIM_IS_WRITE_ROOTFILES,
                    kCOSMO_DELTA0, kCOSMO_FLAG_DELTA_REF, kSIM_REDSHIFT,
                    kSIM_EXTRAGAL_MMIN, kSIM_EXTRAGAL_MMAX, kSIM_NX,
                    kDM_SUBS_NUMBEROFLEVELS, kDM_LOGCDELTA_STDDEV,
                    kDM_RHOSAT,
                    kPP_DM_IS_ANNIHIL_OR_DECAY,
                    kEXTRAGAL_FLAG_PROFILE,
                    // we assume the subhalos to be self/similar to the host,
                    // so no extra profile for the subhalos required.
                    kEXTRAGAL_FLAG_CDELTAMDELTA_LIST,
                   };
   int vars_e2_hidden[] = {kSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE,
                           kCOSMO_HUBBLE, kCOSMO_OMEGA0_M, kCOSMO_OMEGA0_B, kCOSMO_T0,
                           kCOSMO_SIGMA8, kCOSMO_N_S, kCOSMO_TAU_REIO
                          };

   int vars_e3[] = {kSIM_EPS, kSIM_OUTPUT_DIR, kSIM_IS_WRITE_ROOTFILES,
                    kCOSMO_DELTA0, kCOSMO_FLAG_DELTA_REF,
                    kSIM_EXTRAGAL_MMIN, kSIM_EXTRAGAL_MMAX, kSIM_EXTRAGAL_NM,
                    kSIM_EXTRAGAL_ZMIN, kSIM_EXTRAGAL_ZMAX, kSIM_EXTRAGAL_NZ,
                    kSIM_EXTRAGAL_IS_ZLOG,  kSIM_XPOWER,
                    kDM_KMAX,
                    kDM_IS_IDM,
                    kDM_SUBS_NUMBEROFLEVELS, kDM_LOGCDELTA_STDDEV,
                    kDM_RHOSAT,
                    kEXTRAGAL_FLAG_MASSFUNCTION,
                    kEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE,
                    kEXTRAGAL_FLAG_PROFILE,
                    kEXTRAGAL_FLAG_CDELTAMDELTA,
                    kSIM_FLUX_AT_E_GEV
                   };

   int vars_e3_hidden[] = {kCOSMO_HUBBLE, kCOSMO_OMEGA0_M, kCOSMO_OMEGA0_B, kCOSMO_T0,
                           kCOSMO_SIGMA8, kCOSMO_N_S, kCOSMO_TAU_REIO,
                           kCOSMO_OMEGA0_K,
                           kDM_RHOHALOES_TO_RHOMEAN,
                           kDM_KMAX_SIGMA_CUTOFF,
                           kSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE,
                           kSIM_EXTRAGAL_FLAG_WINDOWFUNC,
                           kSIM_EXTRAGAL_MF_SIGMA_CUTOFF,
                           kSIM_EXTRAGAL_KMAX_PRECOMP,
                           kSIM_EXTRAGAL_NM_PRECOMP, kSIM_EXTRAGAL_DELTAZ_PRECOMP, kSIM_EXTRAGAL_IS_MLOG,
                           kSIM_EXTRAGAL_FLAG_GROWTHFACTOR,
                           kEXTRAGAL_FLAG_ABSORPTIONPROFILE
                          };


   int vars_e4[] = {kSIM_EPS, kSIM_OUTPUT_DIR, kSIM_IS_WRITE_ROOTFILES,
                    kSIM_FLUX_EMIN_GEV, kSIM_FLUX_EMAX_GEV, kSIM_NX,
                    kSIM_EXTRAGAL_ZMIN, kSIM_EXTRAGAL_ZMAX, kSIM_EXTRAGAL_NZ, kSIM_EXTRAGAL_IS_ZLOG,
                    kEXTRAGAL_FLAG_ABSORPTIONPROFILE
                   };
   int vars_e4_hidden[] = {};

   int vars_e5[] = {kSIM_EPS, kSIM_OUTPUT_DIR, kSIM_IS_WRITE_ROOTFILES,
                    kSIM_EXTRAGAL_MMIN, kSIM_EXTRAGAL_MMAX, kSIM_EXTRAGAL_NM,
                    kSIM_EXTRAGAL_ZMIN, kSIM_EXTRAGAL_ZMAX, kSIM_EXTRAGAL_NZ,
                    kSIM_EXTRAGAL_IS_ZLOG,
                    kSIM_XPOWER,
                    kSIM_FLUX_AT_E_GEV,
                    kPP_DM_IS_ANNIHIL_OR_DECAY,
                    kPP_DM_MASS_GEV,
                    kPP_FLAG_SPECTRUMMODEL,
                    kSIM_FLUX_FLAG_FINALSTATE,
                   };
   int vars_e5_hidden[] = {kCOSMO_HUBBLE, kCOSMO_OMEGA0_M, kCOSMO_OMEGA0_B, kCOSMO_T0,
                           kCOSMO_SIGMA8, kCOSMO_N_S, kCOSMO_TAU_REIO, kSIM_EXTRAGAL_FLAG_WINDOWFUNC,
                           kDM_RHOHALOES_TO_RHOMEAN, kSIM_EXTRAGAL_MF_SIGMA_CUTOFF,
                           kDM_KMAX_SIGMA_CUTOFF,
                           kSIM_EXTRAGAL_KMAX_PRECOMP, kSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE,
                           kSIM_EXTRAGAL_NM_PRECOMP, kSIM_EXTRAGAL_DELTAZ_PRECOMP, kSIM_EXTRAGAL_IS_MLOG,
                           kSIM_EXTRAGAL_FLAG_GROWTHFACTOR
                          };

   int vars_e6[] = {kSIM_EPS, kSIM_OUTPUT_DIR, kSIM_IS_WRITE_ROOTFILES,
                    kSIM_EXTRAGAL_ZMIN, kSIM_EXTRAGAL_ZMAX,
                    kSIM_EXTRAGAL_EBL_UNCERTAINTY,
                    kSIM_XPOWER,
                    kSIM_FLUX_EMIN_GEV, kSIM_FLUX_EMAX_GEV, kSIM_NX,
                    kPP_DM_IS_ANNIHIL_OR_DECAY,
                    kPP_DM_MASS_GEV,
                    kPP_FLAG_SPECTRUMMODEL,
                    kSIM_FLUX_FLAG_FINALSTATE
                   };
   int vars_e6_hidden[] = {kCOSMO_HUBBLE, kCOSMO_OMEGA0_M, kCOSMO_OMEGA0_B, kCOSMO_T0,
                           kCOSMO_SIGMA8, kCOSMO_N_S, kCOSMO_TAU_REIO, kSIM_EXTRAGAL_FLAG_WINDOWFUNC,
                           kDM_RHOHALOES_TO_RHOMEAN, kSIM_EXTRAGAL_MF_SIGMA_CUTOFF,
                           kDM_KMAX_SIGMA_CUTOFF,
                           kSIM_EXTRAGAL_KMAX_PRECOMP, kSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE,
                           kSIM_EXTRAGAL_NM_PRECOMP, kSIM_EXTRAGAL_DELTAZ_PRECOMP, kSIM_EXTRAGAL_IS_MLOG,
                           kSIM_EXTRAGAL_FLAG_GROWTHFACTOR
                          };

   int vars_s0[] = {kSIM_EPS, kSIM_OUTPUT_DIR, kSTAT_FILES, kSTAT_MODE, kSIM_R_MIN, kSIM_R_MAX, kSIM_NX,
                    kSTAT_CL_LIST, kDM_RHOSAT
                   };
   int vars_s0_hidden[] = {kSIM_IS_XLOG};

   int vars_s1[] = {kSTAT_FILES, kSTAT_IS_LOGL_OR_CHI2};
   int vars_s1_hidden[] = {};

   int vars_s2[] = {kSTAT_FILES, kSTAT_ID_LIST, kDM_RHOSAT, kSTAT_RKPC_FOR_MR};
   int vars_s2_hidden[] = {};

   int vars_s3[] = {kSTAT_FILES, kSTAT_MODE};
   int vars_s3_hidden[] = {};

   int vars_s4[] = {kSTAT_FILES, kSTAT_MODE, kSTAT_CL};
   int vars_s4_hidden[] = {};

   int vars_s5[] = {kSIM_OUTPUT_DIR, kSTAT_FILES, kSTAT_MODE, kSIM_R_MIN, kSIM_R_MAX, kSIM_NX,
                    kSTAT_CL_LIST, kDM_RHOSAT, kSIM_IS_ASTRO_OR_PP_UNITS
                   };
   int vars_s5_hidden[] = {kSIM_IS_XLOG};

   int vars_s6[] = {kSIM_EPS, kSIM_OUTPUT_DIR, kSTAT_FILES, kSTAT_MODE, kSIM_ALPHAINT_MIN, kSIM_ALPHAINT_MAX, kSIM_NX,
                    kSTAT_CL_LIST, kDM_RHOSAT, kSIM_IS_ASTRO_OR_PP_UNITS, kPP_DM_IS_ANNIHIL_OR_DECAY
                   };
   int vars_s6_hidden[] = {kSIM_IS_XLOG};

   int vars_s7[] = {kSIM_EPS, kSIM_OUTPUT_DIR, kSTAT_FILES, kSTAT_MODE, kSIM_THETA_MIN, kSIM_THETA_MAX, kSIM_NX,
                    kSTAT_CL_LIST, kDM_RHOSAT, kSIM_IS_ASTRO_OR_PP_UNITS, kPP_DM_IS_ANNIHIL_OR_DECAY,
                    kSIM_ALPHAINT
                   };
   int vars_s7_hidden[] = {kSIM_IS_XLOG};

   int vars_s8[] = {kSIM_EPS, kSIM_OUTPUT_DIR, kSTAT_FILES, kSTAT_MODE, kSIM_R_MIN, kSIM_R_MAX, kSIM_NX,
                    kSTAT_CL_LIST, kDM_RHOSAT, kSTAT_DATAFILES
                   };
   int vars_s8_hidden[] = {kSIM_IS_XLOG};

   int vars_s9[] = {kSIM_EPS, kSIM_OUTPUT_DIR, kSTAT_FILES, kSTAT_MODE, kSIM_R_MIN, kSIM_R_MAX, kSIM_NX,
                    kSTAT_CL_LIST, kDM_RHOSAT
                   };
   int vars_s9_hidden[] = {kSIM_IS_XLOG};

   int vars_s10[] = {kSIM_OUTPUT_DIR, kSTAT_FILES, kSTAT_MODE, kSIM_R_MIN, kSIM_R_MAX, kSIM_NX,
                     kSTAT_CL_LIST,
                    };
   int vars_s10_hidden[] = {kSIM_IS_XLOG};

   int vars_s11[] = {kSIM_EPS, kSIM_OUTPUT_DIR, kSTAT_FILES, kSTAT_MODE, kSIM_R_MIN, kSIM_R_MAX, kSIM_NX,
                     kSTAT_CL_LIST
                    };
   int vars_s11_hidden[] = {kSIM_IS_XLOG};

   int vars_s12[] = {kSIM_EPS, kSIM_OUTPUT_DIR, kSTAT_FILES, kSTAT_MODE, kSIM_R_MIN, kSIM_R_MAX, kSIM_NX,
                     kSTAT_CL_LIST, kSTAT_DATAFILES
                    };
   int vars_s12_hidden[] = {kSIM_IS_XLOG};

   int vars_o1[] = {};
   int vars_o1_hidden[] = {};

   int vars_o2[] = {};
   int vars_o2_hidden[] = {};

   int vars_z[] = {kSIM_EPS, kSIM_IS_ASTRO_OR_PP_UNITS, kSIM_JFACTOR, kSIM_OUTPUT_DIR,
                   kSIM_REDSHIFT, kSIM_XPOWER, kSIM_FLUX_FLAG_FINALSTATE,
                   kPP_DM_IS_ANNIHIL_OR_DECAY, kPP_DM_MASS_GEV, kPP_FLAG_SPECTRUMMODEL,
                   kSIM_FLUX_EMIN_GEV, kSIM_FLUX_EMAX_GEV, kSIM_NX,
                   kSIM_IS_WRITE_ROOTFILES
                  };
   int vars_z_hidden[] = {};

   int vars_f[] = {kSIM_EPS, kSIM_FLUX_IS_INTEG_OR_DIFF,
                   kPP_NUMIXING_THETA12_DEG,
                   kPP_NUMIXING_THETA13_DEG,
                   kPP_NUMIXING_THETA23_DEG,
                   kSIM_FLUX_FLAG_NUFLAVOUR,
                   kPP_DM_MASS_GEV, kPP_FLAG_SPECTRUMMODEL,
                  };
   int vars_f_hidden[] = {kSIM_HEALPIX_FITS_DATATYPE};

   pushback_array2vector(gPARAMS_REQUIRED, &vars_g0[0], sizeof vars_g0);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_g1[0], sizeof vars_g1);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_g2[0], sizeof vars_g2);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_g3[0], sizeof vars_g3);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_g4[0], sizeof vars_g4);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_g5[0], sizeof vars_g5);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_g6[0], sizeof vars_g6);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_g7[0], sizeof vars_g7);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_g8[0], sizeof vars_g8);

   pushback_array2vector(gPARAMS_REQUIRED, &vars_h0[0], sizeof vars_h0);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_h1[0], sizeof vars_h1);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_h2[0], sizeof vars_h2);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_h3[0], sizeof vars_h3);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_h4[0], sizeof vars_h4);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_h5[0], sizeof vars_h5);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_h6[0], sizeof vars_h6);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_h7[0], sizeof vars_h7);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_h8toh10[0], sizeof vars_h8toh10);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_h8toh10[0], sizeof vars_h8toh10);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_h8toh10[0], sizeof vars_h8toh10);

   pushback_array2vector(gPARAMS_REQUIRED, &vars_e0[0], sizeof vars_e0);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_e1[0], sizeof vars_e1);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_e2[0], sizeof vars_e2);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_e3[0], sizeof vars_e3);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_e4[0], sizeof vars_e4);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_e5[0], sizeof vars_e5);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_e6[0], sizeof vars_e6);

   pushback_array2vector(gPARAMS_REQUIRED, &vars_s0[0], sizeof vars_s0);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_s1[0], sizeof vars_s1);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_s2[0], sizeof vars_s2);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_s3[0], sizeof vars_s3);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_s4[0], sizeof vars_s4);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_s5[0], sizeof vars_s5);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_s6[0], sizeof vars_s6);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_s7[0], sizeof vars_s7);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_s8[0], sizeof vars_s8);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_s9[0], sizeof vars_s9);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_s10[0], sizeof vars_s10);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_s11[0], sizeof vars_s11);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_s12[0], sizeof vars_s12);

   pushback_array2vector(gPARAMS_REQUIRED, &vars_o1[0], sizeof vars_o1);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_o2[0], sizeof vars_o2);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_z[0], sizeof vars_z);
   pushback_array2vector(gPARAMS_REQUIRED, &vars_f[0], sizeof vars_f);

   pushback_array2vector(gPARAMS_HIDDEN, &vars_g0to3_hidden[0], sizeof vars_g0to3_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_g0to3_hidden[0], sizeof vars_g0to3_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_g0to3_hidden[0], sizeof vars_g0to3_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_g0to3_hidden[0], sizeof vars_g0to3_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_g4_hidden[0], sizeof vars_g4_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_g5_hidden[0], sizeof vars_g5_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_g6_hidden[0], sizeof vars_g6_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_g7_hidden[0], sizeof vars_g7_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_g8_hidden[0], sizeof vars_g8_hidden);

   pushback_array2vector(gPARAMS_HIDDEN, &vars_h0to3_hidden[0], sizeof vars_h0to3_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_h0to3_hidden[0], sizeof vars_h0to3_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_h0to3_hidden[0], sizeof vars_h0to3_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_h0to3_hidden[0], sizeof vars_h0to3_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_h4toh5_hidden[0], sizeof vars_h4toh5_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_h4toh5_hidden[0], sizeof vars_h4toh5_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_h6_hidden[0], sizeof vars_h6_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_h7_hidden[0], sizeof vars_h7_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_h8toh10_hidden[0], sizeof vars_h8toh10_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_h8toh10_hidden[0], sizeof vars_h8toh10_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_h8toh10_hidden[0], sizeof vars_h8toh10_hidden);

   pushback_array2vector(gPARAMS_HIDDEN, &vars_e0_hidden[0], sizeof vars_e0_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_e1_hidden[0], sizeof vars_e1_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_e2_hidden[0], sizeof vars_e2_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_e3_hidden[0], sizeof vars_e3_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_e4_hidden[0], sizeof vars_e4_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_e5_hidden[0], sizeof vars_e5_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_e6_hidden[0], sizeof vars_e6_hidden);

   pushback_array2vector(gPARAMS_HIDDEN, &vars_s0_hidden[0], sizeof vars_s0_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_s1_hidden[0], sizeof vars_s1_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_s2_hidden[0], sizeof vars_s2_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_s3_hidden[0], sizeof vars_s3_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_s4_hidden[0], sizeof vars_s4_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_s5_hidden[0], sizeof vars_s5_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_s6_hidden[0], sizeof vars_s6_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_s7_hidden[0], sizeof vars_s7_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_s8_hidden[0], sizeof vars_s8_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_s9_hidden[0], sizeof vars_s9_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_s10_hidden[0], sizeof vars_s10_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_s11_hidden[0], sizeof vars_s11_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_s12_hidden[0], sizeof vars_s12_hidden);

   pushback_array2vector(gPARAMS_HIDDEN, &vars_o1_hidden[0], sizeof vars_o1_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_o2_hidden[0], sizeof vars_o2_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_z_hidden[0], sizeof vars_z_hidden);
   pushback_array2vector(gPARAMS_HIDDEN, &vars_f_hidden[0], sizeof vars_f_hidden);
   return;
}

//______________________________________________________________________________
void get_dependent_required_params(const int card_param, const vector<string> &inputvalues, const int enum_simumode, vector<bool> &is_needed)
{
   // --- Appends required parameters to gPARAMS_REQUIRED if their requirement depends
   // 1.) on the value of inputvalues[card_param]
   // 2.) of the combination of two values in the vector inputvalues[].

   // Inputs:
   //  card_param    Index in the inputvalues[] string where to check the value of inputvalues[]
   //                For finding a joint condition based on two entries in inputvalues[], this
   //                parameter must be set to 999.
   //  inputvalues   Vector of strings in which to check the values.
   //  enum_simumode Enumerator of the simulation mode.
   //  is_needed     Flag which is set to true if the corresponding entry is required. N.B: only to check irrelevant parameters!

   // append required parameters to gPARAMS_REQUIRED if their requirement depends
   // on the parameter flag card_param, and the value of *the same* parameter card_param:

   if ((card_param == kDM_SUBS_NUMBEROFLEVELS or card_param == kMW_SUBS_N_INM1M2) and atoi(inputvalues[card_param].c_str()) > 0) {

      gPARAMS_REQUIRED[enum_simumode].push_back(kDM_SUBS_MMIN);
      is_needed[kDM_SUBS_MMIN] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kDM_SUBS_MMAXFRAC);
      is_needed[kDM_SUBS_MMAXFRAC] = true;
      if (enum_simumode != kh1 and enum_simumode != kg1) {
         gPARAMS_REQUIRED[enum_simumode].push_back(kDM_LOGCDELTA_STDDEV);
         is_needed[kDM_LOGCDELTA_STDDEV] = true;
      }
      if (gNAMES_SIMUMODES[enum_simumode][0] == 'g') {
         if (enum_simumode != kg1) {
            gPARAMS_REQUIRED[enum_simumode].push_back(kMW_SUBS_FLAG_CDELTAMDELTA);
            is_needed[kMW_SUBS_FLAG_CDELTAMDELTA] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kMW_SUBS_FLAG_PROFILE);
            is_needed[kMW_SUBS_FLAG_PROFILE] = true;
         }
         gPARAMS_REQUIRED[enum_simumode].push_back(kMW_SUBS_DPDM_SLOPE);
         is_needed[kMW_SUBS_DPDM_SLOPE] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kMW_SUBS_M1);
         is_needed[kMW_SUBS_M1] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kMW_SUBS_M2);
         is_needed[kMW_SUBS_M2] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kMW_SUBS_N_INM1M2);
         is_needed[kMW_SUBS_N_INM1M2] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kMW_SUBS_DPDV_FLAG_PROFILE);
         is_needed[kMW_SUBS_DPDV_FLAG_PROFILE] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kMW_SUBS_DPDV_RSCALE_TO_RS_HOST);
         is_needed[kMW_SUBS_DPDV_RSCALE_TO_RS_HOST] = true;
      }
      if (enum_simumode == kh1) {
         gPARAMS_REQUIRED[enum_simumode].push_back(kDSPH_SUBS_DPDV_FLAG_PROFILE);
         is_needed[kDSPH_SUBS_DPDV_FLAG_PROFILE] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kDSPH_SUBS_DPDV_SHAPE_PARAMS_0);
         is_needed[kDSPH_SUBS_DPDV_SHAPE_PARAMS_0] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kDSPH_SUBS_DPDV_SHAPE_PARAMS_1);
         is_needed[kDSPH_SUBS_DPDV_SHAPE_PARAMS_1] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kDSPH_SUBS_DPDV_SHAPE_PARAMS_2);
         is_needed[kDSPH_SUBS_DPDV_SHAPE_PARAMS_2] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kDSPH_SUBS_DPDV_RSCALE_TO_RS_HOST);
         is_needed[kDSPH_SUBS_DPDV_RSCALE_TO_RS_HOST] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kDSPH_SUBS_MASSFRACTION);
         is_needed[kDSPH_SUBS_MASSFRACTION] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kGALAXY_SUBS_DPDV_FLAG_PROFILE);
         is_needed[kGALAXY_SUBS_DPDV_FLAG_PROFILE] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kGALAXY_SUBS_DPDV_SHAPE_PARAMS_0);
         is_needed[kGALAXY_SUBS_DPDV_SHAPE_PARAMS_0] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kGALAXY_SUBS_DPDV_SHAPE_PARAMS_1);
         is_needed[kGALAXY_SUBS_DPDV_SHAPE_PARAMS_1] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kGALAXY_SUBS_DPDV_SHAPE_PARAMS_2);
         is_needed[kGALAXY_SUBS_DPDV_SHAPE_PARAMS_2] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kGALAXY_SUBS_DPDV_RSCALE_TO_RS_HOST);
         is_needed[kGALAXY_SUBS_DPDV_RSCALE_TO_RS_HOST] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kGALAXY_SUBS_MASSFRACTION);
         is_needed[kGALAXY_SUBS_MASSFRACTION] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kCLUSTER_SUBS_DPDV_FLAG_PROFILE);
         is_needed[kCLUSTER_SUBS_DPDV_FLAG_PROFILE] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_0);
         is_needed[kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_0] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_1);
         is_needed[kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_1] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_2);
         is_needed[kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_2] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kCLUSTER_SUBS_DPDV_RSCALE_TO_RS_HOST);
         is_needed[kCLUSTER_SUBS_DPDV_RSCALE_TO_RS_HOST] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kCLUSTER_SUBS_MASSFRACTION);
         is_needed[kCLUSTER_SUBS_MASSFRACTION] = true;
      } else if (gNAMES_SIMUMODES[enum_simumode][0] == 'h' or enum_simumode == kg4 or enum_simumode == kg6 or enum_simumode == kg8) {
         gPARAMS_REQUIRED[enum_simumode].push_back(kDSPH_SUBS_FLAG_PROFILE);
         is_needed[kDSPH_SUBS_FLAG_PROFILE] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kDSPH_SUBS_SHAPE_PARAMS_0);
         is_needed[kDSPH_SUBS_SHAPE_PARAMS_0] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kDSPH_SUBS_SHAPE_PARAMS_1);
         is_needed[kDSPH_SUBS_SHAPE_PARAMS_1] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kDSPH_SUBS_SHAPE_PARAMS_2);
         is_needed[kDSPH_SUBS_SHAPE_PARAMS_2] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kDSPH_SUBS_FLAG_CDELTAMDELTA);
         is_needed[kDSPH_SUBS_FLAG_CDELTAMDELTA] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kDSPH_SUBS_DPDV_FLAG_PROFILE);
         is_needed[kDSPH_SUBS_DPDV_FLAG_PROFILE] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kDSPH_SUBS_DPDV_SHAPE_PARAMS_0);
         is_needed[kDSPH_SUBS_DPDV_SHAPE_PARAMS_0] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kDSPH_SUBS_DPDV_SHAPE_PARAMS_1);
         is_needed[kDSPH_SUBS_DPDV_SHAPE_PARAMS_1] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kDSPH_SUBS_DPDV_SHAPE_PARAMS_2);
         is_needed[kDSPH_SUBS_DPDV_SHAPE_PARAMS_2] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kDSPH_SUBS_DPDV_RSCALE_TO_RS_HOST);
         is_needed[kDSPH_SUBS_DPDV_RSCALE_TO_RS_HOST] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kDSPH_SUBS_DPDM_SLOPE);
         is_needed[kDSPH_SUBS_DPDM_SLOPE] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kDSPH_SUBS_MASSFRACTION);
         is_needed[kDSPH_SUBS_MASSFRACTION] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kGALAXY_SUBS_FLAG_PROFILE);
         is_needed[kGALAXY_SUBS_FLAG_PROFILE] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kGALAXY_SUBS_SHAPE_PARAMS_0);
         is_needed[kGALAXY_SUBS_SHAPE_PARAMS_0] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kGALAXY_SUBS_SHAPE_PARAMS_1);
         is_needed[kGALAXY_SUBS_SHAPE_PARAMS_1] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kGALAXY_SUBS_SHAPE_PARAMS_2);
         is_needed[kGALAXY_SUBS_SHAPE_PARAMS_2] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kGALAXY_SUBS_FLAG_CDELTAMDELTA);
         is_needed[kGALAXY_SUBS_FLAG_CDELTAMDELTA] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kGALAXY_SUBS_DPDV_FLAG_PROFILE);
         is_needed[kGALAXY_SUBS_DPDV_FLAG_PROFILE] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kGALAXY_SUBS_DPDV_SHAPE_PARAMS_0);
         is_needed[kGALAXY_SUBS_DPDV_SHAPE_PARAMS_0] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kGALAXY_SUBS_DPDV_SHAPE_PARAMS_1);
         is_needed[kGALAXY_SUBS_DPDV_SHAPE_PARAMS_1] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kGALAXY_SUBS_DPDV_SHAPE_PARAMS_2);
         is_needed[kGALAXY_SUBS_DPDV_SHAPE_PARAMS_2] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kGALAXY_SUBS_DPDV_RSCALE_TO_RS_HOST);
         is_needed[kGALAXY_SUBS_DPDV_RSCALE_TO_RS_HOST] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kGALAXY_SUBS_DPDM_SLOPE);
         is_needed[kGALAXY_SUBS_DPDM_SLOPE] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kGALAXY_SUBS_MASSFRACTION);
         is_needed[kGALAXY_SUBS_MASSFRACTION] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kCLUSTER_SUBS_FLAG_PROFILE);
         is_needed[kCLUSTER_SUBS_FLAG_PROFILE] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kCLUSTER_SUBS_SHAPE_PARAMS_0);
         is_needed[kCLUSTER_SUBS_SHAPE_PARAMS_0] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kCLUSTER_SUBS_SHAPE_PARAMS_1);
         is_needed[kCLUSTER_SUBS_SHAPE_PARAMS_1] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kCLUSTER_SUBS_SHAPE_PARAMS_2);
         is_needed[kCLUSTER_SUBS_SHAPE_PARAMS_2] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kCLUSTER_SUBS_FLAG_CDELTAMDELTA);
         is_needed[kCLUSTER_SUBS_FLAG_CDELTAMDELTA] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kCLUSTER_SUBS_DPDV_FLAG_PROFILE);
         is_needed[kCLUSTER_SUBS_DPDV_FLAG_PROFILE] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_0);
         is_needed[kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_0] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_1);
         is_needed[kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_1] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_2);
         is_needed[kCLUSTER_SUBS_DPDV_SHAPE_PARAMS_2] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kCLUSTER_SUBS_DPDV_RSCALE_TO_RS_HOST);
         is_needed[kCLUSTER_SUBS_DPDV_RSCALE_TO_RS_HOST] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kCLUSTER_SUBS_DPDM_SLOPE);
         is_needed[kCLUSTER_SUBS_DPDM_SLOPE] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kCLUSTER_SUBS_MASSFRACTION);
         is_needed[kCLUSTER_SUBS_MASSFRACTION] = true;
      } else if (gNAMES_SIMUMODES[enum_simumode][0] == 'e') {
         gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_SUBS_DPDV_FLAG_PROFILE);
         is_needed[kEXTRAGAL_SUBS_DPDV_FLAG_PROFILE] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_SUBS_DPDV_RSCALE_TO_RS_HOST);
         is_needed[kEXTRAGAL_SUBS_DPDV_RSCALE_TO_RS_HOST] = true;
         if (enum_simumode == ke2) {
            gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_SUBS_DPDM_SLOPE_LIST);
            is_needed[kEXTRAGAL_SUBS_DPDM_SLOPE_LIST] = true;
         } else {
            gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_SUBS_DPDM_SLOPE);
            is_needed[kEXTRAGAL_SUBS_DPDM_SLOPE] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_SUBS_FLAG_CDELTAMDELTA);
            is_needed[kEXTRAGAL_SUBS_FLAG_CDELTAMDELTA] = true;
         }
         gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_SUBS_MASSFRACTION);
         is_needed[kEXTRAGAL_SUBS_MASSFRACTION] = true;
      }
   } else if (card_param == kEXTRAGAL_SUBS_DPDV_FLAG_PROFILE) {
      if (inputvalues[card_param] != "kBURKERT" and inputvalues[card_param] != "kDPDV_PIERI11") {
         gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_0);
         is_needed[kEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_0] = true;
         if (inputvalues[card_param] == "kNODES") {
            gPARAMS_REQUIRED[enum_simumode].push_back(kLIST_HALOES_NODES);
            is_needed[kLIST_HALOES_NODES] = true;
         } else if (inputvalues[card_param].substr(0, 4) != "kEIN"
                    and inputvalues[card_param] != "kDPDV_SPRINGEL08_ANTIBIASED") {
            gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_1);
            is_needed[kEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_1] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_2);
            is_needed[kEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_2] = true;
         }
      }
   } else if (card_param == kEXTRAGAL_FLAG_PROFILE) {
      if (inputvalues[card_param] != "kBURKERT" and inputvalues[card_param] != "kDPDV_PIERI11") {
         gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_SHAPE_PARAMS_0);
         is_needed[kEXTRAGAL_SHAPE_PARAMS_0] = true;
         if (inputvalues[card_param] == "kNODES") {
            gPARAMS_REQUIRED[enum_simumode].push_back(kLIST_HALOES_NODES);
            is_needed[kLIST_HALOES_NODES] = true;
         } else if (inputvalues[card_param].substr(0, 4) != "kEIN"
                    and inputvalues[card_param] != "kDPDV_SPRINGEL08_ANTIBIASED") {
            gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_SHAPE_PARAMS_1);
            is_needed[kEXTRAGAL_SHAPE_PARAMS_1] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_SHAPE_PARAMS_2);
            is_needed[kEXTRAGAL_SHAPE_PARAMS_2] = true;
         }
      }
   } else if (card_param == kDSPH_SUBS_FLAG_PROFILE) {
      if (inputvalues[card_param] == "kNODES") {
         gPARAMS_REQUIRED[enum_simumode].push_back(kLIST_HALOES_NODES);
         is_needed[kLIST_HALOES_NODES] = true;
      }
   } else if (card_param == kGALAXY_SUBS_FLAG_PROFILE) {
      if (inputvalues[card_param] == "kNODES") {
         gPARAMS_REQUIRED[enum_simumode].push_back(kLIST_HALOES_NODES);
         is_needed[kLIST_HALOES_NODES] = true;
      }
   } else if (card_param == kCLUSTER_SUBS_FLAG_PROFILE) {
      if (inputvalues[card_param] == "kNODES") {
         gPARAMS_REQUIRED[enum_simumode].push_back(kLIST_HALOES_NODES);
         is_needed[kLIST_HALOES_NODES] = true;
      }
   } else if (card_param == kDSPH_SUBS_DPDV_FLAG_PROFILE) {
      if (inputvalues[card_param] == "kNODES") {
         gPARAMS_REQUIRED[enum_simumode].push_back(kLIST_HALOES_NODES);
         is_needed[kLIST_HALOES_NODES] = true;
      }
   } else if (card_param == kGALAXY_SUBS_DPDV_FLAG_PROFILE) {
      if (inputvalues[card_param] == "kNODES") {
         gPARAMS_REQUIRED[enum_simumode].push_back(kLIST_HALOES_NODES);
         is_needed[kLIST_HALOES_NODES] = true;
      }
   } else if (card_param == kCLUSTER_SUBS_DPDV_FLAG_PROFILE) {
      if (inputvalues[card_param] == "kNODES") {
         gPARAMS_REQUIRED[enum_simumode].push_back(kLIST_HALOES_NODES);
         is_needed[kLIST_HALOES_NODES] = true;
      }
   } else if (card_param == kDM_IS_IDM and atoi(inputvalues[card_param].c_str()) == 1
              and gNAMES_SIMUMODES[enum_simumode][0] == 'e' and enum_simumode != ke2) {
      gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_IDM_MHALFMODE);
      is_needed[kEXTRAGAL_IDM_MHALFMODE] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_IDM_ALPHA);
      is_needed[kEXTRAGAL_IDM_ALPHA] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_IDM_BETA);
      is_needed[kEXTRAGAL_IDM_BETA] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_IDM_GAMMA);
      is_needed[kEXTRAGAL_IDM_GAMMA] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_IDM_DELTA);
      is_needed[kEXTRAGAL_IDM_DELTA] = true;
   } else if (card_param == kEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE
              and atoi(inputvalues[card_param].c_str()) != -1) {
      gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_HMF_SMALLSCALE_DPDM_MLIM);
      is_needed[kEXTRAGAL_HMF_SMALLSCALE_DPDM_MLIM] = true;
   } else if (card_param == kSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE
              and atoi(inputvalues[card_param].c_str()) == 1) {
      gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_FLAG_PROFILE);
      is_needed[kEXTRAGAL_FLAG_PROFILE] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_FLAG_CDELTAMDELTA);
      is_needed[kEXTRAGAL_FLAG_CDELTAMDELTA] = true;
   } else if (card_param == kMW_SUBS_FLAG_PROFILE) {
      if (inputvalues[card_param] != "kBURKERT" and inputvalues[card_param] != "kDPDV_PIERI11") {
         gPARAMS_REQUIRED[enum_simumode].push_back(kMW_SUBS_SHAPE_PARAMS_0);
         is_needed[kMW_SUBS_SHAPE_PARAMS_0] = true;
         if (inputvalues[card_param] == "kNODES") {
            gPARAMS_REQUIRED[enum_simumode].push_back(kLIST_HALOES_NODES);
            is_needed[kLIST_HALOES_NODES] = true;
         } else if (inputvalues[card_param].substr(0, 4) != "kEIN"
                    and inputvalues[card_param] != "kDPDV_SPRINGEL08_ANTIBIASED") {
            gPARAMS_REQUIRED[enum_simumode].push_back(kMW_SUBS_SHAPE_PARAMS_1);
            is_needed[kMW_SUBS_SHAPE_PARAMS_1] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kMW_SUBS_SHAPE_PARAMS_2);
            is_needed[kMW_SUBS_SHAPE_PARAMS_2] = true;
         }
      }
   } else if (card_param == kMW_SUBS_DPDV_FLAG_PROFILE) {
      if (inputvalues[card_param] != "kBURKERT" and inputvalues[card_param] != "kDPDV_PIERI11") {
         gPARAMS_REQUIRED[enum_simumode].push_back(kMW_SUBS_DPDV_SHAPE_PARAMS_0);
         is_needed[kMW_SUBS_DPDV_SHAPE_PARAMS_0] = true;
         if (inputvalues[card_param] == "kNODES") {
            gPARAMS_REQUIRED[enum_simumode].push_back(kLIST_HALOES_NODES);
            is_needed[kLIST_HALOES_NODES] = true;
         } else if (inputvalues[card_param].substr(0, 4) != "kEIN"
                    and inputvalues[card_param] != "kDPDV_SPRINGEL08_ANTIBIASED") {
            gPARAMS_REQUIRED[enum_simumode].push_back(kMW_SUBS_DPDV_SHAPE_PARAMS_1);
            is_needed[kMW_SUBS_DPDV_SHAPE_PARAMS_1] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kMW_SUBS_DPDV_SHAPE_PARAMS_2);
            is_needed[kMW_SUBS_DPDV_SHAPE_PARAMS_2] = true;
         }
      }
   } else if (card_param == kMW_TOT_FLAG_PROFILE) {
      if (inputvalues[card_param] != "kBURKERT" and inputvalues[card_param] != "kDPDV_PIERI11") {
         gPARAMS_REQUIRED[enum_simumode].push_back(kMW_TOT_SHAPE_PARAMS_0);
         is_needed[kMW_TOT_SHAPE_PARAMS_0] = true;
         if (inputvalues[card_param] == "kNODES") {
            gPARAMS_REQUIRED[enum_simumode].push_back(kLIST_HALOES_NODES);
            is_needed[kLIST_HALOES_NODES] = true;
         } else if (inputvalues[card_param].substr(0, 4) != "kEIN"
                    and inputvalues[card_param] != "kDPDV_SPRINGEL08_ANTIBIASED") {
            gPARAMS_REQUIRED[enum_simumode].push_back(kMW_TOT_SHAPE_PARAMS_1);
            is_needed[kMW_TOT_SHAPE_PARAMS_1] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kMW_TOT_SHAPE_PARAMS_2);
            is_needed[kMW_TOT_SHAPE_PARAMS_2] = true;
         }
      }
   } else if (card_param == kMW_TRIAXIAL_IS
              and atoi(inputvalues[card_param].c_str()) == 1) {
      gPARAMS_REQUIRED[enum_simumode].push_back(kMW_TRIAXIAL_AXES_0);
      is_needed[kMW_TRIAXIAL_AXES_0] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kMW_TRIAXIAL_AXES_1);
      is_needed[kMW_TRIAXIAL_AXES_1] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kMW_TRIAXIAL_AXES_2);
      is_needed[kMW_TRIAXIAL_AXES_2] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kMW_TRIAXIAL_ROTANGLES_0);
      is_needed[kMW_TRIAXIAL_ROTANGLES_0] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kMW_TRIAXIAL_ROTANGLES_1);
      is_needed[kMW_TRIAXIAL_ROTANGLES_1] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kMW_TRIAXIAL_ROTANGLES_2);
      is_needed[kMW_TRIAXIAL_ROTANGLES_2] = true;
   } else if (card_param == kMW_SUBS_TABULATED_IS
              and atoi(inputvalues[card_param].c_str()) == 1) {
      gPARAMS_REQUIRED[enum_simumode].push_back(kMW_SUBS_TABULATED_CMIN_OF_R);
      is_needed[kMW_SUBS_TABULATED_CMIN_OF_R] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kMW_SUBS_TABULATED_LCRIT);
      is_needed[kMW_SUBS_TABULATED_LCRIT] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kMW_SUBS_TABULATED_RTIDAL_TO_RS);
      is_needed[kMW_SUBS_TABULATED_RTIDAL_TO_RS] = true;
   } else if (card_param == kSIM_IS_WRITE_FLUXMAPS
              and atoi(inputvalues[card_param].c_str()) == 1) {
      gPARAMS_REQUIRED[enum_simumode].push_back(kSIM_FLUX_IS_INTEG_OR_DIFF);
      is_needed[kSIM_FLUX_IS_INTEG_OR_DIFF] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kPP_DM_MASS_GEV);
      is_needed[kPP_DM_MASS_GEV] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kPP_FLAG_SPECTRUMMODEL);
      is_needed[kPP_FLAG_SPECTRUMMODEL] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kPP_DM_IS_ANNIHIL_OR_DECAY);
      is_needed[kPP_DM_IS_ANNIHIL_OR_DECAY] = true;
   } else if (card_param == kSIM_FLUX_IS_INTEG_OR_DIFF
              and atoi(inputvalues[card_param].c_str()) == 1) {
      gPARAMS_REQUIRED[enum_simumode].push_back(kSIM_FLUX_EMIN_GEV);
      is_needed[kSIM_FLUX_EMIN_GEV] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kSIM_FLUX_EMAX_GEV);
      is_needed[kSIM_FLUX_EMAX_GEV] = true;
   } else if (card_param == kSIM_FLUX_IS_INTEG_OR_DIFF
              and atoi(inputvalues[card_param].c_str()) == 0) {
      gPARAMS_REQUIRED[enum_simumode].push_back(kSIM_FLUX_AT_E_GEV);
      is_needed[kSIM_FLUX_AT_E_GEV] = true;
   } else if (card_param == kSIM_FLUX_FLAG_FINALSTATE
              and inputvalues[card_param] == "kNEUTRINO") {
      gPARAMS_REQUIRED[enum_simumode].push_back(kPP_NUMIXING_THETA12_DEG);
      is_needed[kPP_NUMIXING_THETA12_DEG] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kPP_NUMIXING_THETA13_DEG);
      is_needed[kPP_NUMIXING_THETA13_DEG] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kPP_NUMIXING_THETA23_DEG);
      is_needed[kPP_NUMIXING_THETA23_DEG] = true;
      gPARAMS_REQUIRED[enum_simumode].push_back(kSIM_FLUX_FLAG_NUFLAVOUR);
      is_needed[kSIM_FLUX_FLAG_NUFLAVOUR] = true;
   };

   // append required parameters to gPARAMS_REQUIRED if their requirement depends
   // on the parameter flag card_param, and the value of *another* parameter card_param2:

   // check whether annihilation/decay information is needed for flux calculation:
   // (card_param == 999: dummy flag to indicate second scan)
   if (card_param == 999) {
      if ((atoi(inputvalues[kSIM_IS_WRITE_FLUXMAPS].c_str()) == 1
            or enum_simumode == kz or enum_simumode == ke5
            or enum_simumode == ke6)
            and atoi(inputvalues[kPP_DM_IS_ANNIHIL_OR_DECAY].c_str()) == 1) {
         gPARAMS_REQUIRED[enum_simumode].push_back(kPP_DM_ANNIHIL_SIGMAV_CM3PERS);
         is_needed[kPP_DM_ANNIHIL_SIGMAV_CM3PERS] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kPP_DM_ANNIHIL_DELTA);
         is_needed[kPP_DM_ANNIHIL_DELTA] = true;
         if (enum_simumode == ke5 or enum_simumode == ke6) {
            gPARAMS_REQUIRED[enum_simumode].push_back(kSIM_EXTRAGAL_MMIN);
            is_needed[kSIM_EXTRAGAL_MMIN] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kSIM_EXTRAGAL_MMAX);
            is_needed[kSIM_EXTRAGAL_MMAX] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kDM_RHOSAT);
            is_needed[kDM_RHOSAT] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kDM_SUBS_NUMBEROFLEVELS);
            is_needed[kDM_SUBS_NUMBEROFLEVELS] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kDM_LOGCDELTA_STDDEV);
            is_needed[kDM_LOGCDELTA_STDDEV] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kDM_KMAX);
            is_needed[kDM_KMAX] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_FLAG_MASSFUNCTION);
            is_needed[kEXTRAGAL_FLAG_MASSFUNCTION] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE);
            is_needed[kEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_FLAG_PROFILE);
            is_needed[kEXTRAGAL_FLAG_PROFILE] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kDM_IS_IDM);
            is_needed[kDM_IS_IDM] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_FLAG_CDELTAMDELTA);
            is_needed[kEXTRAGAL_FLAG_CDELTAMDELTA] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kCOSMO_DELTA0);
            is_needed[kCOSMO_DELTA0] = true;
            gPARAMS_REQUIRED[enum_simumode].push_back(kCOSMO_FLAG_DELTA_REF);
            is_needed[kCOSMO_FLAG_DELTA_REF] = true;
         }
      } else if ((inputvalues[kSIM_IS_WRITE_FLUXMAPS] == "1" or enum_simumode == kz
                  or enum_simumode == ke5 or enum_simumode == ke6)
                 and atoi(inputvalues[kPP_DM_IS_ANNIHIL_OR_DECAY].c_str()) == 0) {
         gPARAMS_REQUIRED[enum_simumode].push_back(kPP_DM_DECAY_LIFETIME_S);
         is_needed[kPP_DM_DECAY_LIFETIME_S] = true;
      }

      // check if gamma-ray absorption has to be taken into account:
      if ((enum_simumode == kh2 or enum_simumode == kh3 or enum_simumode == kh4 or enum_simumode == kh5) and atoi(inputvalues[kSIM_IS_WRITE_FLUXMAPS].c_str()) == 1) {
         gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_FLAG_ABSORPTIONPROFILE);
         is_needed[kEXTRAGAL_FLAG_ABSORPTIONPROFILE] = true;
      }
      // check that neutrino flux info is loaded when writing 2d fluxmaps (calculate always gamma and neutrino fluxes,
      // hardcoded by default):
      if (atoi(inputvalues[kSIM_IS_WRITE_FLUXMAPS].c_str()) == 1
            and enum_simumode != kz) {
         gPARAMS_REQUIRED[enum_simumode].push_back(kPP_NUMIXING_THETA12_DEG);
         is_needed[kPP_NUMIXING_THETA12_DEG] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kPP_NUMIXING_THETA13_DEG);
         is_needed[kPP_NUMIXING_THETA13_DEG] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kPP_NUMIXING_THETA23_DEG);
         is_needed[kPP_NUMIXING_THETA23_DEG] = true;
         gPARAMS_REQUIRED[enum_simumode].push_back(kSIM_FLUX_FLAG_NUFLAVOUR);
         is_needed[kSIM_FLUX_FLAG_NUFLAVOUR] = true;
      }
      // check for branching ratios in case of selecting Cirelli spectra:
      if (inputvalues[kPP_FLAG_SPECTRUMMODEL].substr(0, 4) == "kCIR") {
         gPARAMS_REQUIRED[enum_simumode].push_back(kPP_BR);
         is_needed[kPP_BR] = true;
      }

      if (atoi(inputvalues[kSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE].c_str()) == 1) {
         if (inputvalues[kEXTRAGAL_FLAG_PROFILE] != "kBURKERT" and inputvalues[card_param] != "kDPDV_PIERI11") {
            gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_SHAPE_PARAMS_0);
            is_needed[kEXTRAGAL_SHAPE_PARAMS_0] = true;
            if (inputvalues[card_param] == "kNODES") {
               gPARAMS_REQUIRED[enum_simumode].push_back(kLIST_HALOES_NODES);
               is_needed[kLIST_HALOES_NODES] = true;
            } else if (inputvalues[kEXTRAGAL_FLAG_PROFILE].substr(0, 4) != "kEIN"
                       and inputvalues[kEXTRAGAL_FLAG_PROFILE] != "kDPDV_SPRINGEL08_ANTIBIASED") {
               gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_SHAPE_PARAMS_1);
               is_needed[kEXTRAGAL_SHAPE_PARAMS_1] = true;
               gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_SHAPE_PARAMS_2);
               is_needed[kEXTRAGAL_SHAPE_PARAMS_2] = true;
            }
         }
      }
      if (inputvalues[kSIM_FLUX_FLAG_FINALSTATE].substr(0, 6) == "kGAMMA"
            and (enum_simumode == ke5 or enum_simumode == ke6)) {
         gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_FLAG_ABSORPTIONPROFILE);
         is_needed[kEXTRAGAL_FLAG_ABSORPTIONPROFILE] = true;
      }
      if (inputvalues[kSIM_FLUX_FLAG_FINALSTATE].substr(0, 6) == "kGAMMA"
            and (enum_simumode == kz or enum_simumode == kf)
            and atof(inputvalues[kSIM_REDSHIFT].c_str()) > 0) {
         gPARAMS_REQUIRED[enum_simumode].push_back(kEXTRAGAL_FLAG_ABSORPTIONPROFILE);
         is_needed[kEXTRAGAL_FLAG_ABSORPTIONPROFILE] = true;
      }
   }
   return;
}

//______________________________________________________________________________
void load_parameters(const string &file_name, const vector<string> &commandline_values, const int enum_simumode, bool is_verbose)
{
   //--- Loads input parameters from the file file_name and/or the values read
   //    from the command line stored in commandline_values. It checks the consistency of the
   //    input with simulation mode enum_simumode. Input values are pushed into the corresponding
   //    global variables, and filled into the string gSIM_INPUTPARAM_VALUESTRING.
   //    Values from the file file_name overwrite default values set in param.cc,
   //    and values from the command line overwrite values from the file file_name.

   //    Only the global input variables needed for flag_simumode are overwritten, and only the
   //    needed values are filled in gSIM_INPUTPARAM_VALUESTRING (default "-999" otherwise)

   //
   //  file_name             Name of file of clumpy parameters. If everything shall be read from the command line, set to -1
   //  commandline_values    Values read from the command line in string.
   //  enum_simumode         Simulation mode for which the parameters shall be read
   //  is_verbose            Verbose or not verbose

   vector<string> name, value;
   vector<string> paramfile_values(gN_INPUTPARAMS, "-999");
   char char_tmp[256];

   if (file_name != "-1") {
      // Read parameters
      ifstream file(file_name.c_str());
      if (!file) {
         print_error("params.cc", "load_parameters()", "Could not open file '" + file_name + "'");
      }
      cout << ">>>>> Load " << file_name << endl;


      string line;
      vector<bool> is_filled_twice(gN_INPUTPARAMS, false);
      while (getline(file, line)) {

         line.erase(remove_if(line.begin(), line.end(), isNotASCII), line.end());

         if (line[0] == '#' || line.empty()) continue;
         vector<string> params;
         string2list(line, " \t", params);
         if (params.size() < 3) {
            print_error("params.cc", "load_parameters()", "Wrong format for current line read: " + line +
                        " Must consist of at least 3 whitespace/tabulator separated columns: "
                        "Variable  Unit  Value  (+ optional comments)");
         }
         params[0].erase(remove(params[0].begin(), params[0].end(), '\t'), params[0].end());
         params[0].erase(remove_if(params[0].begin(), params[0].end(), isNotASCII), params[0].end());
         name.push_back(params[0]);
         value.push_back(params[2]);
      }

      // check for unknown parameter names in name-vector:
      for (int j = 0; j < (int)name.size(); ++j) {
         bool is_found = false ; // bool to check if all entries in file correspond to CLUMPY parameters.
         for (int i = 0; i < gN_INPUTPARAMS; ++i) {
            if (name[j] == string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]))
               is_found = true;
         }
         if (!is_found) {
            print_error("params.cc", "load_parameters()", "Parameter name '" + name[j] + "' "
                        "(specified in file) not known. Maybe a spelling error?");
         }
      }

      // fill input values from vector "value" into ordered vector paramfile_values
      for (int i = 0; i < gN_INPUTPARAMS; ++i) {
         for (int j = 0; j < (int)name.size(); ++j) {
            if (gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME] == name[j]) {
               if (is_filled_twice[i]) {
                  print_error("params.cc", "load_parameters()", "Parameter " + name[j] + " is specified twice in parameter file. "
                              "Please remove redundant definition.");
               }
               paramfile_values[i] = value[j];
               is_filled_twice[i] = true;
            }
         }
      }
   }

   vector<bool> is_filled(gN_INPUTPARAMS, false); // to check missing parameters
   vector<bool> is_needed(gN_INPUTPARAMS, true);  // to check irrelevant parameters
   vector<bool> is_hidden_found(gPARAMS_HIDDEN[enum_simumode].size(), false);

   // now merge commandline_values and paramfile_values:
   vector<string> merged_values = paramfile_values;

   for (int i = 0; i < gN_INPUTPARAMS; ++i) {
      if (commandline_values[i] != "-999") {
         merged_values[i] = commandline_values[i];
         if (paramfile_values[i] != "-999") {
            print_warning("params.cc", "load_parameters()",
                          "Parameter " + string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]) +
                          " set in parameter file overwritten by command line.");
         }
      }
      // allow for boolean names:
      if (merged_values[i] == "False" or merged_values[i] == "false") merged_values[i] = "0";
      else if (merged_values[i] == "True" or merged_values[i] == "true") merged_values[i] = "1";
   }

   // now check consistency of input and fill result into global gSIM_INPUTPARAM_VALUESTRING
   // iterate three times:
   // 1.) get dependent parameters
   // 2.) set values of dependent variables missed in first round (as they weren't known to be required)
   // 3.) get and set parameters dependent of values from other parameters



   for (int k = 0; k < 3; ++k) {

      // check for variables that depend on other variables in third iteration (999: dummy flag to indicate second scan):
      if (k == 2) get_dependent_required_params(999, gSIM_INPUTPARAM_VALUESTRING, enum_simumode, is_needed);

      // fill input values from vector "value" into ordered vector "gSIM_INPUTPARAM_VALUESTRING":
      for (int i = 0; i < gN_INPUTPARAMS; ++i) {
         if (merged_values[i] != "-999") {
            is_filled[i] = true;

            if (k == 0) {
               if (find(gPARAMS_HIDDEN[enum_simumode].begin(), gPARAMS_HIDDEN[enum_simumode].end(), i) != gPARAMS_HIDDEN[enum_simumode].end()) {
                  if (i != kLIST_HALOES_NODES) // don't print gLIST_HALOES_NODES as hidden parameter
                     cout  << "  ... found hidden optional parameter " << gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME] << " set by user." << endl;
                  gPARAMS_REQUIRED[enum_simumode].push_back(i);
                  is_hidden_found[(int)distance(gPARAMS_HIDDEN[enum_simumode].begin(), find(gPARAMS_HIDDEN[enum_simumode].begin(), gPARAMS_HIDDEN[enum_simumode].end(), i))] = true;
               }
            }

            // set only parameters which are needed for this simulation:
            if (find(gPARAMS_REQUIRED[enum_simumode].begin(), gPARAMS_REQUIRED[enum_simumode].end(), i) == gPARAMS_REQUIRED[enum_simumode].end()) {
               is_needed[i] = false; // ..and do not fill anything into gSIM_INPUTPARAM_VALUESTRING
               // only exception: flux extension option -f:
               if (enum_simumode == kf) {
                  if (i == kPP_DM_ANNIHIL_DELTA or i == kPP_DM_ANNIHIL_SIGMAV_CM3PERS or i == kPP_DM_DECAY_LIFETIME_S or i == kEXTRAGAL_FLAG_ABSORPTIONPROFILE) {
                     is_needed[i] = true;
                     gSIM_INPUTPARAM_VALUESTRING[i] = merged_values[i];
                  }
               }
            } else {
               gSIM_INPUTPARAM_VALUESTRING[i] = merged_values[i];

               // check for variables which demand further specifications depending on value.
               // and add values to gPARAMS_REQUIRED:
               if (k == 0 or k == 2) get_dependent_required_params(i, gSIM_INPUTPARAM_VALUESTRING, enum_simumode, is_needed);
            }
         }
      }
   }

   // update standard input parameters:
   gSIM_STANDARD_INPUTPARAMS = inputparameters_globalparams2string(gSIM_FLAG_MODE);
// hardcoded workaround for f mode
   if (enum_simumode == kf) {
      gSIM_STANDARD_INPUTPARAMS[kPP_DM_ANNIHIL_DELTA]  = "2";
      gSIM_STANDARD_INPUTPARAMS[kPP_DM_ANNIHIL_SIGMAV_CM3PERS]  = "3e-26";
      gSIM_STANDARD_INPUTPARAMS[kPP_DM_DECAY_LIFETIME_S] = "1e27";
      gSIM_STANDARD_INPUTPARAMS[kEXTRAGAL_FLAG_ABSORPTIONPROFILE] = "kFRANCESCHINI08";
   }

   // copy the values from gSIM_INPUTPARAM_VALUESTRING into the global variables;
   inputparameters_string2globalparams(gSIM_INPUTPARAM_VALUESTRING);

   // make consistent redundant definition of substructures:
   if (gDM_SUBS_NUMBEROFLEVELS == 0) {
      if (gNAMES_SIMUMODES[gSIM_FLAG_MODE][0] == 'g' and gMW_SUBS_N_INM1M2 != 0) {
         print_warning("params.cc", "load_parameters()",
                       "gDM_SUBS_NUMBEROFLEVELS = 0 overrides gMW_SUBS_N_INM1M2 != 0");
         gMW_SUBS_N_INM1M2 = 0;
      }

      for (int k = 0; k < gN_TYPEHALOES; ++k) {
         int halotype;
         if (k == 0) halotype = kDSPH_SUBS_MASSFRACTION;
         else if (k == 1) halotype = kGALAXY_SUBS_MASSFRACTION;
         else if (k == 2) halotype = kCLUSTER_SUBS_MASSFRACTION;
         else halotype = kEXTRAGAL_SUBS_MASSFRACTION;
         if (k < gN_TYPEHALOES - 1 and gNAMES_SIMUMODES[gSIM_FLAG_MODE][0] == 'h' and gHALO_SUBS_MASSFRACTION[k] != 0) {
            print_warning("params.cc", "load_parameters()", "gDM_SUBS_NUMBEROFLEVELS =0  overrides "
                          + string(gSIM_INPUTPARAMS[halotype][kSIM_INPUTPARAM_VARNAME]) + " != 0");

         } else if (k == gN_TYPEHALOES - 1 and gNAMES_SIMUMODES[gSIM_FLAG_MODE][0] == 'e' and gHALO_SUBS_MASSFRACTION[k] != 0) {
            print_warning("params.cc", "load_parameters()", "gDM_SUBS_NUMBEROFLEVELS = 0 overrides "
                          + string(gSIM_INPUTPARAMS[halotype][kSIM_INPUTPARAM_VARNAME]) + " != 0\n");
         }
         gHALO_SUBS_MASSFRACTION[k] = 0.;
      }
   }

   // update gSIM_INPUTPARAM_VALUESTRING for subs and to replace kHOST entries by actual values
   // Also, propagate only relevant parameters (irrelevant but set parameters are replaced with -999)
   gSIM_INPUTPARAM_VALUESTRING = inputparameters_globalparams2string(gSIM_FLAG_MODE);

//   for (int i = 0; i < gN_INPUTPARAMS; ++i) {
//      cout << gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME] << "\t" << gSIM_INPUTPARAM_VALUESTRING[i]  << endl;
//   }

   // check gSIM_EPS (and hack for test functions):
   if ((gSIM_EPS > 1 and gSIM_EPS != 999 and gSIM_EPS != 998) or gSIM_EPS < 0) {
      print_error("params.cc", "load_parameters()", "gSIM_EPS > 1 or negative not allowed. "
                  "Please choose gSIM_EPS <= 1");
   }

   if (gSIM_EPS == 999) {
      gSIM_EPS = 1e-2;
      gSIM_EPS_DRAWN = 1e-2;
      gSIM_SIGDIGITS = 2;
   } else if (gSIM_EPS == 998) {
      gSIM_EPS = 7e-4;
      gSIM_EPS_DRAWN = 3e-3;
      gSIM_SIGDIGITS = 2;
   } else {
      gSIM_SIGDIGITS = int(ceil(-log10(gSIM_EPS)));
      gSIM_IS_TEST = false;
   }

   // *********** CHECK input parameter consistency and abort/warn for potential issues **********

   // announce further hidden parameters used:
   if (find(is_hidden_found.begin(), is_hidden_found.end(), false) != is_hidden_found.end()) {
      print_warning("params.cc", "load_parameters()",
                    "The following hidden parameters are used in this simulation:");

      cout << string_fixlength("# Variable name", 40) << string_fixlength("Unit", gSIM_PARAMLENGTH_MAX) << string_fixlength("Set to value", gSIM_PARAMLENGTH_MAX) << "What is this?" << endl << endl;

      for (int i = 0; i < (int)gPARAMS_HIDDEN[enum_simumode].size(); ++i) {
         if (is_hidden_found[i] == false) {
            int standardparam_length = gSIM_PARAMLENGTH_MAX;
            // never use hidden halo nodes input:
            if (gPARAMS_HIDDEN[enum_simumode][i] == kLIST_HALOES_NODES) {
               gSIM_INPUTPARAM_VALUESTRING[kLIST_HALOES_NODES] = "-999";
               gLIST_HALOES_NODES = "-999";
               continue;
               // only show hidden gLIST_HALOES_NODES_RSCALE if gLIST_HALOES_NODES is set:
            } else if (gPARAMS_HIDDEN[enum_simumode][i] == kLIST_HALOES_NODES_RSCALE) {
               if (gSIM_INPUTPARAM_VALUESTRING[kLIST_HALOES_NODES] == "-999")
                  continue;
            }

            if (gPARAMS_HIDDEN[enum_simumode][i] == kPP_BR) standardparam_length = string(gSIM_STANDARD_INPUTPARAMS[gPARAMS_HIDDEN[enum_simumode][i]]).length() + 1;
            cout << string_fixlength(gSIM_INPUTPARAMS[gPARAMS_HIDDEN[enum_simumode][i]][kSIM_INPUTPARAM_VARNAME], 40)
                 << string_fixlength(gSIM_INPUTPARAMS[gPARAMS_HIDDEN[enum_simumode][i]][kSIM_INPUTPARAM_UNIT], gSIM_PARAMLENGTH_MAX)
                 << string_fixlength(gSIM_STANDARD_INPUTPARAMS[gPARAMS_HIDDEN[enum_simumode][i]], standardparam_length)
                 << gSIM_INPUTPARAMS[gPARAMS_HIDDEN[enum_simumode][i]][kSIM_INPUTPARAM_DESCRIPTION] << endl;
         }
      }
      cout << endl << "      You can add them to your command line or parameter file" <<  endl;
      cout <<         "      according to the scheme above." <<  endl;
      if (file_name != "-1") cout         << "      (Just copy and paste the printout into " << file_name.c_str()  << ".)" << endl;
      cout << endl;
   }

   bool is_something_missing = false;
   bool is_something_irrelevant = false;

   // exception for properties loaded from separate text file in halo mode:
   for (int i = 0; i < (int)gPARAMS_REQUIRED[enum_simumode].size(); ++i) {
      bool exception = false;
      if (enum_simumode == kh4 or enum_simumode == kh5) {
         if (gPARAMS_REQUIRED[enum_simumode][i] == kSIM_THETA_OBS or
               gPARAMS_REQUIRED[enum_simumode][i] == kSIM_PSI_OBS or
               gPARAMS_REQUIRED[enum_simumode][i] == kSIM_THETA_ORTH_SIZE or
               gPARAMS_REQUIRED[enum_simumode][i] == kSIM_REDSHIFT or
               gPARAMS_REQUIRED[enum_simumode][i] == kLIST_HALO_DISTANCE or
               gPARAMS_REQUIRED[enum_simumode][i] == kLIST_HALO_RSCALE or
               gPARAMS_REQUIRED[enum_simumode][i] == kLIST_HALO_RHOSCALE or
               gPARAMS_REQUIRED[enum_simumode][i] == kLIST_HALO_RDELTA or
               gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_FLAG_PROFILE or
               gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_SHAPE_PARAMS_0 or
               gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_SHAPE_PARAMS_1 or
               gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_SHAPE_PARAMS_2 or
               gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_TRIAXIAL_IS or
               gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_TRIAXIAL_AXES_0 or
               gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_TRIAXIAL_AXES_1 or
               gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_TRIAXIAL_AXES_2 or
               gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_TRIAXIAL_ROTANGLES_0 or
               gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_TRIAXIAL_ROTANGLES_1 or
               gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_TRIAXIAL_ROTANGLES_2) {
            exception = true;
         }
      }
      if (!is_filled[gPARAMS_REQUIRED[enum_simumode][i]] and !exception) {
         is_something_missing = true;
         break;
      }
   }

   for (int i = 0; i < gN_INPUTPARAMS; ++i) {
      if (!is_needed[i]) {
         is_something_irrelevant = true;
         break;
      }
   }

   if (is_something_missing) {

      vector<bool> is_printed(gN_INPUTPARAMS, false); // to check already printed missing values

      cout << "\n====> Your choice of clumpy routine and options requires additional parameters:" << endl << endl;
      cout << string_fixlength("# Variable name", 40) << string_fixlength("Unit", gSIM_PARAMLENGTH_MAX) << string_fixlength("Standard Value", gSIM_PARAMLENGTH_MAX + 3)
           << string_fixlength("(Format)", gSIM_PARAMLENGTH_MAX + 3) << "(Comment)" << endl << endl;

      for (int i = 0; i < (int)gPARAMS_REQUIRED[enum_simumode].size(); ++i) {
         int standardparam_length = gSIM_PARAMLENGTH_MAX;

         bool exception = false;
         if (enum_simumode == kh4 or enum_simumode == kh5) {
            if (gPARAMS_REQUIRED[enum_simumode][i] == kSIM_THETA_OBS or
                  gPARAMS_REQUIRED[enum_simumode][i] == kSIM_PSI_OBS or
                  gPARAMS_REQUIRED[enum_simumode][i] == kSIM_THETA_ORTH_SIZE or
                  gPARAMS_REQUIRED[enum_simumode][i] == kSIM_REDSHIFT or
                  gPARAMS_REQUIRED[enum_simumode][i] == kLIST_HALO_DISTANCE or
                  gPARAMS_REQUIRED[enum_simumode][i] == kLIST_HALO_RSCALE or
                  gPARAMS_REQUIRED[enum_simumode][i] == kLIST_HALO_RHOSCALE or
                  gPARAMS_REQUIRED[enum_simumode][i] == kLIST_HALO_RDELTA or
                  gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_FLAG_PROFILE or
                  gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_SHAPE_PARAMS_0 or
                  gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_SHAPE_PARAMS_1 or
                  gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_SHAPE_PARAMS_2 or
                  gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_TRIAXIAL_IS or
                  gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_TRIAXIAL_AXES_0 or
                  gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_TRIAXIAL_AXES_1 or
                  gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_TRIAXIAL_AXES_2 or
                  gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_TRIAXIAL_ROTANGLES_0 or
                  gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_TRIAXIAL_ROTANGLES_1 or
                  gPARAMS_REQUIRED[enum_simumode][i] == kEXTRAGAL_TRIAXIAL_ROTANGLES_2) {
               exception = true;
            }
         }

         if (!is_filled[gPARAMS_REQUIRED[enum_simumode][i]] and !exception and !is_printed[gPARAMS_REQUIRED[enum_simumode][i]]) {
            if (gPARAMS_REQUIRED[enum_simumode][i] == kPP_BR
                  || gPARAMS_REQUIRED[enum_simumode][i] == kLIST_HALOES
                  || gPARAMS_REQUIRED[enum_simumode][i] == kLIST_HALOES_JEANS
                  || gPARAMS_REQUIRED[enum_simumode][i] == kLIST_HALOES_NODES
                  || gPARAMS_REQUIRED[enum_simumode][i] == kMW_SUBS_TABULATED_CMIN_OF_R
                  || gPARAMS_REQUIRED[enum_simumode][i] == kMW_SUBS_TABULATED_LCRIT
                  || gPARAMS_REQUIRED[enum_simumode][i] == kMW_SUBS_TABULATED_RTIDAL_TO_RS
               )
               standardparam_length = string(gSIM_STANDARD_INPUTPARAMS[gPARAMS_REQUIRED[enum_simumode][i]]).length() + 1;

            cout << string_fixlength(gSIM_INPUTPARAMS[gPARAMS_REQUIRED[enum_simumode][i]][kSIM_INPUTPARAM_VARNAME], 40)
                 << string_fixlength(gSIM_INPUTPARAMS[gPARAMS_REQUIRED[enum_simumode][i]][kSIM_INPUTPARAM_UNIT], gSIM_PARAMLENGTH_MAX)
                 << string_fixlength(gSIM_STANDARD_INPUTPARAMS[gPARAMS_REQUIRED[enum_simumode][i]], standardparam_length) << "   "
                 << string_fixlength("<" + string(gSIM_INPUTPARAMS[gPARAMS_REQUIRED[enum_simumode][i]][kSIM_INPUTPARAM_DATATYPE]) + ">", gSIM_PARAMLENGTH_MAX + 3)
                 << gSIM_INPUTPARAMS[gPARAMS_REQUIRED[enum_simumode][i]][kSIM_INPUTPARAM_DESCRIPTION] << endl;
            is_printed[gPARAMS_REQUIRED[enum_simumode][i]] = true;
         }
      }
      cout << endl;
      if (file_name != "-1") cout         << "      Please add the above parameters to the command line or your input parameter file " <<  endl;
      else  {
         cout   << "      Please add the above parameters to your command line" <<  endl;
         cout   << "      or paste them into a parameter text file according to " <<  endl;
         cout   << "      the scheme above (load the file with -i or -inputfile)" <<  endl;
      };
      cout       << "      and restart the program."  << endl <<  endl;
      abort();
   }

   // print obsolete parameters only in the case nothing is missing at this stage.
   if (is_something_irrelevant) {
      print_warning("params.cc", "load_parameters()", "The following parameters set by the user "
                    "are irrelevant for the performed simulation:");
      cout << string_fixlength("# Variable name", 40)  << "(Comment)" << endl << endl;

      for (int i = 0; i < gN_INPUTPARAMS; ++i) {
         if (!is_needed[i]) {
            cout << string_fixlength(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME], 40) << gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_DESCRIPTION] << endl;
         }
      }
      printf("\n\n");
   }

   // fill the node halos arrays:
   if (gSIM_INPUTPARAM_VALUESTRING[kLIST_HALOES_NODES] != "-999") {
      set_rhonodes_fromfile();

      // check all relevant parameters for consistency with loaded file
      if ((gMW_SUBS_FLAG_PROFILE == kNODES && gMW_SUBS_SHAPE_PARAMS[0] == 0) or
            (gMW_SUBS_DPDV_FLAG_PROFILE == kNODES && gMW_SUBS_DPDV_SHAPE_PARAMS[0] == 0) or
            (gMW_TOT_FLAG_PROFILE == kNODES && gMW_TOT_SHAPE_PARAMS[0] == 0))

      {
         print_error("params.cc", "load_parameters()", "Shape parameter = 0 forbidden for kNODES profile (indexing starts with one).");
      }
      for (int k = 0; k < gN_TYPEHALOES; ++k) {
         if ((gHALO_SUBS_FLAG_PROFILE[k] == kNODES && gHALO_SUBS_SHAPE_PARAMS[k][0] == 0) or
               (gHALO_SUBS_DPDV_FLAG_PROFILE[k] == kNODES && gHALO_SUBS_DPDV_SHAPE_PARAMS[k][0] == 0)) {
            print_error("params.cc", "load_parameters()", "Shape parameter = 0 forbidden for kNODES profile (indexing starts with one).");
         }
      }
   }


// *********** UPDATE or SET non-input global parameters **********

// Fill neutrino oscillation matrix (from mixing angles)
   if (is_filled[kSIM_FLUX_FLAG_NUFLAVOUR] == true) {
      nu_oscillationmatrix(gPP_NUOSCILLATIONMATRIX, is_verbose);
   }


   gCOSMO_RHO0_C = RHO_CRITperh2_MSOLperKPC3 * pow(gCOSMO_HUBBLE, 2.); // [Msol/kpc3]
   gCOSMO_OMEGA0_R = (1. + 3.046 * 7. / 8. * pow(4. / 11., 4. / 3.)) * RADIATION_CONSTANT * pow(gCOSMO_T0, 4) / gCOSMO_RHO0_C;
   gCOSMO_OMEGA0_CDM = gCOSMO_OMEGA0_M - gCOSMO_OMEGA0_B;
   gCOSMO_OMEGA0_LAMBDA = 1 - gCOSMO_OMEGA0_M - gCOSMO_OMEGA0_K - gCOSMO_OMEGA0_R;
   gCOSMO_M_to_MH = gCOSMO_HUBBLE / gCOSMO_OMEGA0_M;

   // print warning for non-flat cosmology:
   if (abs(gCOSMO_OMEGA0_K) > SMALL_NUMBER) {
      sprintf(char_tmp, "%g", gCOSMO_OMEGA0_LAMBDA);
      print_warning("params.cc", "load_parameters()", "You have chosen a non-flat cosmology with"
                    " OMEGA0_LAMBDA = 1 - OMEGA0_K - OMEGA0_M = " + string(char_tmp));
   }

   if (gDM_LOGCDELTA_STDDEV > 1.e-5)
      gDM_FLAG_CDELTA_DIST = kLOGNORM;
   else
      gDM_FLAG_CDELTA_DIST = kDIRAC;

   if (!gSIM_IS_PRINT) gSIM_IS_WRITE_ROOTFILES = false;

   // switch off printing/ writing ROOT files when no ROOT is installed and warn
#if !IS_ROOT
   if (gSIM_IS_WRITE_ROOTFILES) {
      print_warning("params.cc", "load_parameters()", "gSIM_IS_WRITE_ROOTFILES=1 has no effect"
                    " when no ROOT is installed.\n");
   }
   gSIM_IS_DISPLAY = false;
   gSIM_IS_WRITE_ROOTFILES = false;
#endif

   return;
}

//______________________________________________________________________________
void inputparameters_string2globalparams(const vector<string> &input_params)
{

   char char_tmp[256];

   // Assign values to global variables
   for (int i = 0; i < gN_INPUTPARAMS; ++i) {
      if (input_params[i] != "-999") { // do not set parameters
         // which haven't been specified during the parameter loading


         // check if input values required to be numeric are numeric:
         if (gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_DATATYPE][0] == 'f'
               or gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_DATATYPE][0] == 'i') {

            string type_tmp = string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_DATATYPE]);
            if (!isNumeric(input_params[i].c_str()) and type_tmp.substr(0, 8) != "float or") {
               print_error("params.cc", "inputparameters_string2globalparams()", "Parameter value " +
                           string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]) + " = " + input_params[i] +
                           " is not numeric.");
            }
         } else if (gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_DATATYPE][0] == 'b') {
            if (!isNumeric(input_params[i].c_str())
                  or (atoi(input_params[i].c_str()) != 0
                      and atoi(input_params[i].c_str()) != 1)) {
               print_error("params.cc", "inputparameters_string2globalparams()", "Parameter value " +
                           string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]) + " = " + input_params[i] +
                           " is not a boolean (either 0 or 1).");
            }
         }

         //--- Cosmology parameters
         if (i == kCOSMO_HUBBLE)                         gCOSMO_HUBBLE = atof(input_params[i].c_str());
         else if (i == kCOSMO_OMEGA0_M)                  gCOSMO_OMEGA0_M = atof(input_params[i].c_str());
         else if (i == kCOSMO_OMEGA0_B)                  gCOSMO_OMEGA0_B = atof(input_params[i].c_str());
         else if (i == kCOSMO_OMEGA0_K)                  gCOSMO_OMEGA0_K = atof(input_params[i].c_str());
         else if (i == kCOSMO_SIGMA8)                    gCOSMO_SIGMA8 = atof(input_params[i].c_str());
         else if (i == kCOSMO_N_S)                       gCOSMO_N_S = atof(input_params[i].c_str());
         else if (i == kCOSMO_TAU_REIO)                  gCOSMO_TAU_REIO = atof(input_params[i].c_str());
         else if (i == kCOSMO_WDE)                       gCOSMO_WDE = atof(input_params[i].c_str());
         else if (i == kCOSMO_T0)                        gCOSMO_T0 = atof(input_params[i].c_str());
         else if (i == kCOSMO_DELTA0)                    gCOSMO_DELTA0 = atof(input_params[i].c_str());
         else if (i == kCOSMO_FLAG_DELTA_REF)            gCOSMO_FLAG_DELTA_REF = string_to_enum(string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]), input_params[i]);

         //--- Dark matter parameters
         else if (i == kDM_LOGCDELTA_STDDEV)             gDM_LOGCDELTA_STDDEV = atof(input_params[i].c_str());
         else if (i == kDM_SUBS_MMIN)                    gDM_SUBS_MMIN = atof(input_params[i].c_str());
         else if (i == kDM_SUBS_MMAXFRAC)                gDM_SUBS_MMAXFRAC = atof(input_params[i].c_str());
         else if (i == kDM_RHOSAT)                       gDM_RHOSAT = atof(input_params[i].c_str());
         else if (i == kDM_SUBS_NUMBEROFLEVELS)          gDM_SUBS_NUMBEROFLEVELS = atof(input_params[i].c_str());
         else if (i == kDM_RHOHALOES_TO_RHOMEAN)         gDM_RHOHALOES_TO_RHOMEAN = atof(input_params[i].c_str());
         else if (i == kDM_IS_IDM)                       gDM_IS_IDM = atoi(input_params[i].c_str());
         else if (i == kDM_KMAX)                         gDM_KMAX = atof(input_params[i].c_str());
         else if (i == kDM_KMAX_SIGMA_CUTOFF)            gDM_KMAX_SIGMA_CUTOFF = atof(input_params[i].c_str());

         //--- Galactic DM halo parameters (smooth and clumps)
         else if (i == kMW_SUBS_FLAG_CDELTAMDELTA)       gMW_SUBS_FLAG_CDELTAMDELTA = string_to_enum(string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]), input_params[i]);
         else if (i == kMW_SUBS_FLAG_PROFILE)            gMW_SUBS_FLAG_PROFILE = string_to_enum(string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]), input_params[i]);
         else if (i == kMW_SUBS_SHAPE_PARAMS_0) {
            if (input_params[i] == "kHOST" or input_params[i] == "-1") gMW_SUBS_SHAPE_PARAMS[0] = kHOST;
            else                                         gMW_SUBS_SHAPE_PARAMS[0] = atof(input_params[i].c_str());
         } else if (i == kMW_SUBS_SHAPE_PARAMS_1) {
            if (input_params[i] == "kHOST" or input_params[i] == "-1") gMW_SUBS_SHAPE_PARAMS[1] = kHOST;
            else                                         gMW_SUBS_SHAPE_PARAMS[1] = atof(input_params[i].c_str());
         } else if (i == kMW_SUBS_SHAPE_PARAMS_2) {
            if (input_params[i] == "kHOST" or input_params[i] == "-1") gMW_SUBS_SHAPE_PARAMS[2] = kHOST;
            else                                         gMW_SUBS_SHAPE_PARAMS[2] = atof(input_params[i].c_str());
         } else if (i == kMW_SUBS_DPDM_SLOPE)            gMW_SUBS_DPDM_SLOPE = atof(input_params[i].c_str());
         else if (i == kMW_SUBS_DPDV_FLAG_PROFILE)       gMW_SUBS_DPDV_FLAG_PROFILE = string_to_enum(string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]), input_params[i]);
         else if (i == kMW_SUBS_DPDV_RSCALE_TO_RS_HOST)  gMW_SUBS_DPDV_RSCALE_TO_RS_HOST = atof(input_params[i].c_str());
         else if (i == kMW_SUBS_DPDV_SHAPE_PARAMS_0) {
            if (input_params[i] == "kHOST" or input_params[i] == "-1") gMW_SUBS_DPDV_SHAPE_PARAMS[0] = kHOST;
            else                                         gMW_SUBS_DPDV_SHAPE_PARAMS[0] = atof(input_params[i].c_str());
         } else if (i == kMW_SUBS_DPDV_SHAPE_PARAMS_1) {
            if (input_params[i] == "kHOST" or input_params[i] == "-1") gMW_SUBS_DPDV_SHAPE_PARAMS[1] = kHOST;
            else                                         gMW_SUBS_DPDV_SHAPE_PARAMS[1] = atof(input_params[i].c_str());
         } else if (i == kMW_SUBS_DPDV_SHAPE_PARAMS_2) {
            if (input_params[i] == "kHOST"  or input_params[i] == "-1") gMW_SUBS_DPDV_SHAPE_PARAMS[2] = kHOST;
            else                                         gMW_SUBS_DPDV_SHAPE_PARAMS[2] = atof(input_params[i].c_str());
         } else if (i == kMW_SUBS_M1)                    gMW_SUBS_M1 = atof(input_params[i].c_str());
         else if (i == kMW_SUBS_M2)                      gMW_SUBS_M2 = atof(input_params[i].c_str());
         else if (i == kMW_SUBS_N_INM1M2)                gMW_SUBS_N_INM1M2 = atoi(input_params[i].c_str());
         else if (i == kMW_RHOSOL)                       gMW_RHOSOL = atof(input_params[i].c_str()) * GEVperCM3_to_MSOLperKPC3;
         else if (i == kMW_RSOL)                         gMW_RSOL = atof(input_params[i].c_str());
         else if (i == kMW_RMAX)                         gMW_RMAX = atof(input_params[i].c_str());
         else if (i == kMW_SUBS_TABULATED_IS)            gMW_SUBS_TABULATED_IS = atoi(input_params[i].c_str());
         else if (i == kMW_SUBS_TABULATED_CMIN_OF_R)     gMW_SUBS_TABULATED_CMIN_OF_R = input_params[i];
         else if (i == kMW_SUBS_TABULATED_LCRIT)         gMW_SUBS_TABULATED_LCRIT = input_params[i];
         else if (i == kMW_SUBS_TABULATED_RTIDAL_TO_RS)  gMW_SUBS_TABULATED_RTIDAL_TO_RS = input_params[i];
         else if (i == kMW_TOT_FLAG_PROFILE)  {
            if (input_params[i] == "kHOST" or input_params[i] == "-1") {
               print_error("params.cc", "inputparameters_string2globalparams()",
                           "Value kHOST not allowed for gMW_TOT_FLAG_PROFILE (What shall be the host "
                           "of your Milky Way halo?)");
            } else gMW_TOT_FLAG_PROFILE = string_to_enum(string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]), input_params[i]);
         } else if (i == kMW_TOT_RSCALE)                   gMW_TOT_RSCALE = atof(input_params[i].c_str());
         else if (i == kMW_TOT_SHAPE_PARAMS_0)           gMW_TOT_SHAPE_PARAMS[0] = atof(input_params[i].c_str());
         else if (i == kMW_TOT_SHAPE_PARAMS_1)           gMW_TOT_SHAPE_PARAMS[1] = atof(input_params[i].c_str());
         else if (i == kMW_TOT_SHAPE_PARAMS_2)           gMW_TOT_SHAPE_PARAMS[2] = atof(input_params[i].c_str());
         else if (i == kMW_TRIAXIAL_AXES_0)              gMW_TRIAXIAL_AXES[0] = atof(input_params[i].c_str());
         else if (i == kMW_TRIAXIAL_AXES_1)              gMW_TRIAXIAL_AXES[1] = atof(input_params[i].c_str());
         else if (i == kMW_TRIAXIAL_AXES_2)              gMW_TRIAXIAL_AXES[2] = atof(input_params[i].c_str());
         else if (i == kMW_TRIAXIAL_IS)                  gMW_TRIAXIAL_IS = atoi(input_params[i].c_str());
         else if (i == kMW_TRIAXIAL_ROTANGLES_0)         gMW_TRIAXIAL_ROTANGLES[0] = atof(input_params[i].c_str());
         else if (i == kMW_TRIAXIAL_ROTANGLES_1)         gMW_TRIAXIAL_ROTANGLES[1] = atof(input_params[i].c_str());
         else if (i == kMW_TRIAXIAL_ROTANGLES_2)         gMW_TRIAXIAL_ROTANGLES[2] = atof(input_params[i].c_str());

         //--- extragalactic parameters:
         else if (i == kEXTRAGAL_SUBS_DPDM_SLOPE_LIST)   gEXTRAGAL_SUBS_DPDM_SLOPE_LIST = input_params[i].c_str();
         else if (i == kEXTRAGAL_FLAG_CDELTAMDELTA)      gEXTRAGAL_FLAG_CDELTAMDELTA = string_to_enum(string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]), input_params[i]);
         else if (i == kEXTRAGAL_FLAG_CDELTAMDELTA_LIST) gEXTRAGAL_FLAG_CDELTAMDELTA_LIST = input_params[i].c_str();
         else if (i == kEXTRAGAL_FLAG_MASSFUNCTION)      gEXTRAGAL_FLAG_MASSFUNCTION = string_to_enum(string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]), input_params[i]);
         else if (i == kEXTRAGAL_FLAG_ABSORPTIONPROFILE) {
            if (input_params[i] == "-1")                 gEXTRAGAL_FLAG_ABSORPTIONPROFILE = kNOEBL;
            else                                         gEXTRAGAL_FLAG_ABSORPTIONPROFILE = string_to_enum(string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]), input_params[i]);
         } else if (i == kEXTRAGAL_IDM_MHALFMODE)          gEXTRAGAL_IDM_MHALFMODE = atof(input_params[i].c_str());
         else if (i == kEXTRAGAL_IDM_ALPHA)              gEXTRAGAL_IDM_ALPHA = atof(input_params[i].c_str());
         else if (i == kEXTRAGAL_IDM_BETA)               gEXTRAGAL_IDM_BETA = atof(input_params[i].c_str());
         else if (i == kEXTRAGAL_IDM_GAMMA)              gEXTRAGAL_IDM_GAMMA = atof(input_params[i].c_str());
         else if (i == kEXTRAGAL_IDM_DELTA)              gEXTRAGAL_IDM_DELTA = atof(input_params[i].c_str());
         else if (i == kEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE) gEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE = atof(input_params[i].c_str());
         else if (i == kEXTRAGAL_HMF_SMALLSCALE_DPDM_MLIM)  gEXTRAGAL_HMF_SMALLSCALE_DPDM_MLIM = atof(input_params[i].c_str());
         else if (i == kEXTRAGAL_TRIAXIAL_AXES_0)        gHALO_TRIAXIAL_AXES[0] = atof(input_params[i].c_str());
         else if (i == kEXTRAGAL_TRIAXIAL_AXES_1)        gHALO_TRIAXIAL_AXES[1] = atof(input_params[i].c_str());
         else if (i == kEXTRAGAL_TRIAXIAL_AXES_2)        gHALO_TRIAXIAL_AXES[2] = atof(input_params[i].c_str());
         else if (i == kEXTRAGAL_TRIAXIAL_IS)            gHALO_TRIAXIAL_IS = atoi(input_params[i].c_str());
         else if (i == kEXTRAGAL_TRIAXIAL_ROTANGLES_0)   gHALO_TRIAXIAL_ROTANGLES[0] = atof(input_params[i].c_str());
         else if (i == kEXTRAGAL_TRIAXIAL_ROTANGLES_1)   gHALO_TRIAXIAL_ROTANGLES[1] = atof(input_params[i].c_str());
         else if (i == kEXTRAGAL_TRIAXIAL_ROTANGLES_2)   gHALO_TRIAXIAL_ROTANGLES[2] = atof(input_params[i].c_str());

         //--- Particle physics parameters
         else if (i == kPP_BR) {
            string br_list = input_params[i].c_str();
            vector<double> list;
            string2list(br_list, ",", list);
            if (gN_PP_BR != (int)list.size()) {
               sprintf(char_tmp, "Parameter gPP_BR is not a comma-separated list of %d values, only %d were found!", gN_PP_BR, (int)list.size());
               print_error("params.cc", "inputparameters_string2globalparams()", string(char_tmp));
            } else {
               for (int k = 0; k < gN_PP_BR; ++k)
                  gPP_BR[k] = list[k];
            }
         } else if (i == kPP_DM_ANNIHIL_DELTA)           gPP_DM_ANNIHIL_DELTA = atoi(input_params[i].c_str());
         else if (i == kPP_DM_ANNIHIL_SIGMAV_CM3PERS)    gPP_DM_ANNIHIL_SIGMAV_CM3PERS = atof(input_params[i].c_str());
         else if (i == kPP_DM_DECAY_LIFETIME_S)          gPP_DM_DECAY_LIFETIME_S = atof(input_params[i].c_str());
         else if (i == kPP_DM_IS_ANNIHIL_OR_DECAY)       gPP_DM_IS_ANNIHIL_OR_DECAY = atoi(input_params[i].c_str());
         else if (i == kPP_DM_MASS_GEV)                  gPP_DM_MASS_GEV = atof(input_params[i].c_str());
         else if (i == kPP_FLAG_SPECTRUMMODEL)           gPP_FLAG_SPECTRUMMODEL = string_to_enum(string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]), input_params[i]);
         else if (i == kPP_NUMIXING_THETA12_DEG)         gPP_NUMIXING_THETA12_DEG = atof(input_params[i].c_str());
         else if (i == kPP_NUMIXING_THETA13_DEG)         gPP_NUMIXING_THETA13_DEG = atof(input_params[i].c_str());
         else if (i == kPP_NUMIXING_THETA23_DEG)         gPP_NUMIXING_THETA23_DEG = atof(input_params[i].c_str());

         // Sunyaev–Zel'dovich clusters
         else if (i == kSZ_REF_C500)                     gSZ_REF_C500 = atof(input_params[i].c_str());
         else if (i == kSZ_REF_PRESSURE_NORM)            gSZ_REF_PRESSURE_NORM = atof(input_params[i].c_str());
         else if (i == kSZ_REF_SHAPE_PARAMS_0)           gSZ_REF_SHAPE_PARAMS[0] = atof(input_params[i].c_str());
         else if (i == kSZ_REF_SHAPE_PARAMS_1)           gSZ_REF_SHAPE_PARAMS[1] = atof(input_params[i].c_str());
         else if (i == kSZ_REF_SHAPE_PARAMS_2)           gSZ_REF_SHAPE_PARAMS[2] = atof(input_params[i].c_str());
         else if (i == kSZ_REF_ALPHA_MYX)                gSZ_REF_ALPHA_MYX = atof(input_params[i].c_str());
         else if (i == kSZ_FLAG_MASSFUNCTION)            gSZ_FLAG_MASSFUNCTION = string_to_enum(string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]), input_params[i]);

         // Statistical analysis related
         else if (i == kSTAT_FILES)                      gSTAT_FILES = input_params[i].c_str();
         else if (i == kSTAT_CL)                         gSTAT_CL = atof(input_params[i].c_str());
         else if (i == kSTAT_CL_LIST)                    gSTAT_CL_LIST = input_params[i].c_str();
         else if (i == kSTAT_ID_LIST)                    gSTAT_ID_LIST = input_params[i].c_str();
         else if (i == kSTAT_MODE)                       gSTAT_MODE = atoi(input_params[i].c_str());
         else if (i == kSTAT_IS_LOGL_OR_CHI2)            gSTAT_IS_LOGL_OR_CHI2 = atoi(input_params[i].c_str());
         else if (i == kSTAT_RKPC_FOR_MR)                gSTAT_RKPC_FOR_MR = atof(input_params[i].c_str());
         else if (i == kSTAT_DATAFILES)                  gSTAT_DATAFILES = input_params[i].c_str();

         //--- List of halos
         else if (i == kLIST_HALOES)                     gLIST_HALOES = input_params[i].c_str();
         else if (i == kLIST_HALOES_JEANS)               gLIST_HALOES_JEANS = input_params[i].c_str();
         else if (i == kLIST_HALONAME)                   gLIST_HALONAME = input_params[i].c_str();
         else if (i == kLIST_HALOES_NODES)               gLIST_HALOES_NODES = input_params[i].c_str();
         else if (i == kLIST_HALOES_NODES_RSCALE)        gLIST_HALOES_NODES_RSCALE = input_params[i].c_str();
         else if (i == kLIST_HALO_DISTANCE)              gLIST_HALO_DISTANCE = atof(input_params[i].c_str());
         else if (i == kLIST_HALO_RSCALE)                gLIST_HALO_RSCALE = atof(input_params[i].c_str());
         else if (i == kLIST_HALO_RHOSCALE)              gLIST_HALO_RHOSCALE = atof(input_params[i].c_str());
         else if (i == kLIST_HALO_RDELTA)                gLIST_HALO_RDELTA = atof(input_params[i].c_str());

         else if (i == kSIM_FLAG_MODE)                   gSIM_FLAG_MODE = string_to_enum(string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]), "k" + input_params[i]);

         //--- Simulation parameters
         else if (i == kSIM_EPS)                         gSIM_EPS = atof(input_params[i].c_str());
         else if (i == kSIM_EPS_DRAWN)                   gSIM_EPS_DRAWN = atof(input_params[i].c_str());
         else if (i == kSIM_USER_RSE)                    gSIM_USER_RSE = atof(input_params[i].c_str());
         else if (i == kSIM_SEED)                        gSIM_SEED = atoi(input_params[i].c_str());
         else if (i == kSIM_OUTPUT_DIR) {
            gSIM_OUTPUT_DIR = input_params[i].c_str();
            char last_char = gSIM_OUTPUT_DIR.at(gSIM_OUTPUT_DIR.length() - 1);
            string last_char_str;
            last_char_str.append(1, last_char) ;
            string slash = "/";
            if (atoi(gSIM_OUTPUT_DIR.c_str()) == -1)     gSIM_OUTPUT_DIR = "";
            else {
               if (last_char_str != slash)               gSIM_OUTPUT_DIR += "/" ;
               makedir(gSIM_OUTPUT_DIR.c_str()) ;
            }
         }

         else if (i == kSIM_R_MIN)                       gSIM_R_MIN = atof(input_params[i].c_str());
         else if (i == kSIM_R_MAX)                       gSIM_R_MAX = atof(input_params[i].c_str());
         else if (i == kSIM_NX)                          gSIM_NX = atoi(input_params[i].c_str());
         else if (i == kSIM_IS_XLOG)                     gSIM_IS_XLOG = atoi(input_params[i].c_str());
         else if (i == kSIM_ALPHAINT_MIN)                gSIM_ALPHAINT_MIN = atof(input_params[i].c_str()) * DEG_to_RAD;
         else if (i == kSIM_ALPHAINT_MAX)                gSIM_ALPHAINT_MAX = atof(input_params[i].c_str()) * DEG_to_RAD;
         else if (i == kSIM_THETA_MIN)                   gSIM_THETA_MIN = atof(input_params[i].c_str()) * DEG_to_RAD;
         else if (i == kSIM_THETA_MAX)                   gSIM_THETA_MAX = atof(input_params[i].c_str()) * DEG_to_RAD;
         else if (i == kSIM_SORT_CONTRAST_THRESH)        gSIM_SORT_CONTRAST_THRESH = atoi(input_params[i].c_str());
         else if (i == kSIM_PHI_CUT)                     gSIM_ALPHAINT_MIN = atof(input_params[i].c_str()) * DEG_to_RAD;

         else if (i == kSIM_PSI_OBS)   {
            gSIM_PSI_OBS = atof(input_params[i].c_str()) * DEG_to_RAD;
            check_psi(gSIM_PSI_OBS);
         } else if (i == kSIM_THETA_OBS)                 gSIM_THETA_OBS_DEG = input_params[i];
         else if (i == kSIM_THETA_ORTH_SIZE)             gSIM_THETA_ORTH_SIZE_DEG = input_params[i];
         else if (i == kSIM_THETA_SIZE)                  gSIM_THETA_SIZE = atof(input_params[i].c_str()) * DEG_to_RAD;

         else if (i == kSIM_IS_ASTRO_OR_PP_UNITS)        gSIM_IS_ASTRO_OR_PP_UNITS = atoi(input_params[i].c_str());
         else if (i == kSIM_ALPHAINT) {
            gSIM_ALPHAINT = atof(input_params[i].c_str()) * DEG_to_RAD;
            if (atof(input_params[i].c_str()) < 0)       gSIM_ALPHAINT = -1 ;
            else if (atof(input_params[i].c_str()) > 180.) {
               print_error("params.cc", "inputparameters_string2globalparams()",
                           "Value for gSIM_ALPHAINT_DEG must be <= 180 deg.");
            } else                                         gSIM_ALPHAINT = atof(input_params[i].c_str()) * DEG_to_RAD;
         } else if (i == kSIM_HEALPIX_NSIDE) {
            if (input_params[i] == "DYNAMIC" || atoi(input_params[i].c_str()) == -1)  gSIM_HEALPIX_NSIDE = -1;
            else                                         gSIM_HEALPIX_NSIDE = atoi(input_params[i].c_str());
         }

         else if (i == kSIM_HEALPIX_RING_WEIGHTS_DIR)    gSIM_HEALPIX_RING_WEIGHTS_DIR = input_params[i].c_str();
         else if (i == kSIM_HEALPIX_ITER)                gSIM_HEALPIX_ITER = atoi(input_params[i].c_str());
         else if (i == kSIM_HEALPIX_NLMAX_FAC)           gSIM_HEALPIX_NLMAX_FAC = atof(input_params[i].c_str());
         else if (i == kSIM_HEALPIX_FITS_DATATYPE)       gSIM_HEALPIX_FITS_DATATYPE = string2type(input_params[i]);
         else if (i == kSIM_HEALPIX_SCHEME) {
            if (input_params[i] == "NESTED")             gSIM_HEALPIX_SCHEME = NEST;
            else if (input_params[i] == "RING")          gSIM_HEALPIX_SCHEME = RING;
            else {
               print_error("params.cc", "inputparameters_string2globalparams()",
                           "gSIM_HEALPIX_SCHEME must be either RING or NESTED.");
            }
         } else if (i == kSIM_IS_WRITE_FLUXMAPS)         gSIM_IS_WRITE_FLUXMAPS = atoi(input_params[i].c_str());
         else if (i == kSIM_FLUX_IS_INTEG_OR_DIFF)       gSIM_FLUX_IS_INTEG_OR_DIFF = atoi(input_params[i].c_str());
         else if (i == kSIM_FLUX_AT_E_GEV)               gSIM_FLUX_AT_E_GEV = atof(input_params[i].c_str());
         else if (i == kSIM_XPOWER)                      gSIM_XPOWER = atof(input_params[i].c_str());
         else if (i == kSIM_FLUX_EMIN_GEV)               gSIM_FLUX_EMIN_GEV = atof(input_params[i].c_str());
         else if (i == kSIM_FLUX_EMAX_GEV)               gSIM_FLUX_EMAX_GEV = atof(input_params[i].c_str());
         else if (i == kSIM_FLUX_FLAG_NUFLAVOUR)         gSIM_FLUX_FLAG_NUFLAVOUR = string_to_enum(string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]), input_params[i]);
         else if (i == kSIM_FLUX_FLAG_FINALSTATE)        gSIM_FLUX_FLAG_FINALSTATE = string_to_enum(string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]), input_params[i]);
         else if (i == kSIM_JFACTOR)                     gSIM_JFACTOR = atof(input_params[i].c_str());
         else if (i == kSIM_JFRACTION)                   gSIM_JFRACTION = atof(input_params[i].c_str());

         else if (i == kSIM_GAUSSBEAM_GAMMA_FWHM) {
            gSIM_GAUSSBEAM_GAMMA_FWHM = atof(input_params[i].c_str()) * DEG_to_RAD;
            if (atof(input_params[i].c_str()) < 0)       gSIM_GAUSSBEAM_GAMMA_FWHM = -1 ;
            else                                         gSIM_GAUSSBEAM_GAMMA_FWHM = atof(input_params[i].c_str()) * DEG_to_RAD;
         } else if (i == kSIM_GAUSSBEAM_NEUTRINO_FWHM) {
            gSIM_GAUSSBEAM_NEUTRINO_FWHM = atof(input_params[i].c_str()) * DEG_to_RAD;
            if (atof(input_params[i].c_str()) < 0)       gSIM_GAUSSBEAM_NEUTRINO_FWHM = -1 ;
            else                                         gSIM_GAUSSBEAM_NEUTRINO_FWHM = atof(input_params[i].c_str()) * DEG_to_RAD;
         }

         else if (i == kSIM_REDSHIFT)                    gSIM_REDSHIFT = atof(input_params[i].c_str());
         else if (i == kSIM_EXTRAGAL_ZMIN)               gSIM_EXTRAGAL_ZMIN = atof(input_params[i].c_str());
         else if (i == kSIM_EXTRAGAL_ZMAX)               gSIM_EXTRAGAL_ZMAX = atof(input_params[i].c_str());
         else if (i == kSIM_EXTRAGAL_NZ)                 gSIM_EXTRAGAL_NZ = atoi(input_params[i].c_str());
         else if (i == kSIM_EXTRAGAL_IS_ZLOG)            gSIM_EXTRAGAL_IS_ZLOG = atoi(input_params[i].c_str());
         else if (i == kSIM_EXTRAGAL_DELTAZ_PRECOMP)     gSIM_EXTRAGAL_DELTAZ_PRECOMP = atof(input_params[i].c_str());
         else if (i == kSIM_EXTRAGAL_MMIN)               gSIM_EXTRAGAL_MMIN = atof(input_params[i].c_str());
         else if (i == kSIM_EXTRAGAL_MMAX)               gSIM_EXTRAGAL_MMAX = atof(input_params[i].c_str());
         else if (i == kSIM_EXTRAGAL_NM)                 gSIM_EXTRAGAL_NM = atoi(input_params[i].c_str());
         else if (i == kSIM_EXTRAGAL_IS_MLOG)            gSIM_EXTRAGAL_IS_MLOG = atoi(input_params[i].c_str());
         else if (i == kSIM_EXTRAGAL_NM_PRECOMP)         gSIM_EXTRAGAL_NM_PRECOMP  = atoi(input_params[i].c_str());
         else if (i == kSIM_EXTRAGAL_FLAG_WINDOWFUNC)    gSIM_EXTRAGAL_FLAG_WINDOWFUNC = string_to_enum(string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]), input_params[i]);
         else if (i == kSIM_EXTRAGAL_MF_SIGMA_CUTOFF)    gSIM_EXTRAGAL_MF_SIGMA_CUTOFF = atof(input_params[i].c_str());
         else if (i == kSIM_EXTRAGAL_KMAX_PRECOMP)       gSIM_EXTRAGAL_KMAX_PRECOMP = atof(input_params[i].c_str());
         else if (i == kSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE) gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE = atoi(input_params[i].c_str());
         else if (i == kSIM_EXTRAGAL_EBL_UNCERTAINTY)    gSIM_EXTRAGAL_EBL_UNCERTAINTY = atof(input_params[i].c_str());
         else if (i == kSIM_EXTRAGAL_FLAG_GROWTHFACTOR)  gSIM_EXTRAGAL_FLAG_GROWTHFACTOR = string_to_enum(string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]), input_params[i]);

         else if (i == kSIM_IS_SZ)                       gSIM_IS_SZ = atoi(input_params[i].c_str());

         else if (i == kSIM_IS_WRITE_GALPOWERSPECTRUM)   gSIM_IS_WRITE_GALPOWERSPECTRUM = atoi(input_params[i].c_str());
         else if (i == kSIM_IS_WRITE_ROOTFILES)          gSIM_IS_WRITE_ROOTFILES = atoi(input_params[i].c_str());

         else if (i == kSTAT_N_REALIZATIONS) {
            gSTAT_N_REALIZATIONS = atoi(input_params[i].c_str());
            if (gSTAT_N_REALIZATIONS != 0 && gSTAT_N_REALIZATIONS < 10) {
               sprintf(char_tmp, "gSTAT_N_REALIZATIONS(=%d) should be at least 10 samples.", gSTAT_N_REALIZATIONS);
               print_error("params.cc", "inputparameters_string2globalparams()", string(char_tmp));
            }
         }

         else {
            //--- DSPH, GALAXY, CLUSTER, and EXTRAGAL sub-clump parameters (universal for objects belonging to the same type)
            for (int k = 0; k < gN_TYPEHALOES; ++k) {
               string halo = gNAMES_TYPEHALOES[k];
               string name = string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]);
               if (name == string("g" + halo + "_SUBS_FLAG_CDELTAMDELTA"))
                  gHALO_SUBS_FLAG_CDELTAMDELTA[k]                                                                           = string_to_enum(string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]), input_params[i]);
               else if (name == string("g" + halo + "_SUBS_FLAG_PROFILE") || (k == 3 && name == string("g" + halo + "_FLAG_PROFILE"))) {    // no "SUBS" for extragalactic
                  if (k == 3 && (input_params[i] == "kHOST" or input_params[i] == "-1")) {
                     print_error("params.cc", "inputparameters_string2globalparams()",
                                 "Value kHOST not allowed for gEXTRAGAL_FLAG_PROFILE"
                                 " (There is no host above your field halos)");
                  }
                  // no "SUBS" for extragalactic
                  else gHALO_SUBS_FLAG_PROFILE[k]                                                                                = string_to_enum(string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]), input_params[i]);
               } else if (name == string("g" + halo + "_SUBS_SHAPE_PARAMS_0") || (k == 3 && name == string("g" + halo + "_SHAPE_PARAMS_0"))) {
                  if (input_params[i] == "kHOST" && k != 3)                           gHALO_SUBS_SHAPE_PARAMS[k][0]        = kHOST;
                  else                                                                 gHALO_SUBS_SHAPE_PARAMS[k][0]        = atof(input_params[i].c_str());
               } else if (name == string("g" + halo + "_SUBS_SHAPE_PARAMS_1") || (k == 3 && name == string("g" + halo + "_SHAPE_PARAMS_1"))) {
                  if (input_params[i] == "kHOST" && k != 3)                           gHALO_SUBS_SHAPE_PARAMS[k][1]        = kHOST;
                  else                                                                 gHALO_SUBS_SHAPE_PARAMS[k][1]        = atof(input_params[i].c_str());
               } else if (name == string("g" + halo + "_SUBS_SHAPE_PARAMS_2") || (k == 3 && name == string("g" + halo + "_SHAPE_PARAMS_2"))) {
                  if (input_params[i] == "kHOST" && k != 3)                           gHALO_SUBS_SHAPE_PARAMS[k][2]        = kHOST;
                  else                                                                 gHALO_SUBS_SHAPE_PARAMS[k][2]        = atof(input_params[i].c_str());
               } else if (name == string("g" + halo + "_SUBS_DPDM_SLOPE"))             gHALO_SUBS_DPDM_SLOPE[k]             = atof(input_params[i].c_str());
               else if (name == string("g" + halo + "_SUBS_DPDV_FLAG_PROFILE")) {
                  if (input_params[i] == "kHOST")                                      gHALO_SUBS_DPDV_FLAG_PROFILE[k]      = kHOST;
                  else                                                                 gHALO_SUBS_DPDV_FLAG_PROFILE[k]      = string_to_enum(string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]), input_params[i]);
                  if (gHALO_SUBS_DPDV_FLAG_PROFILE[k] == kDPDV_SPRINGEL08_FIT) {
                     print_warning("params.cc", "load_parameters()", "kDPDV_SPRINGEL08_FIT is not valid for any "
                                   "dPdV profile but the Galactic.");
                     //printf("\n             => abort()\n\n");
                     //abort();
                  }
               } else if (name == string("g" + halo + "_SUBS_DPDV_RSCALE_TO_RS_HOST")) gHALO_SUBS_DPDV_RSCALE_TO_RS_HOST[k] = atof(input_params[i].c_str());
               else if (name == string("g" + halo + "_SUBS_DPDV_SHAPE_PARAMS_0")) {
                  if (input_params[i] == "kHOST")                                      gHALO_SUBS_DPDV_SHAPE_PARAMS[k][0]   = kHOST;
                  else                                                                 gHALO_SUBS_DPDV_SHAPE_PARAMS[k][0]   = atof(input_params[i].c_str());
               } else if (name == string("g" + halo + "_SUBS_DPDV_SHAPE_PARAMS_1")) {
                  if (input_params[i] == "kHOST")                                      gHALO_SUBS_DPDV_SHAPE_PARAMS[k][1]   = kHOST;
                  else                                                                 gHALO_SUBS_DPDV_SHAPE_PARAMS[k][1]   = atof(input_params[i].c_str());
               } else if (name == string("g" + halo + "_SUBS_DPDV_SHAPE_PARAMS_2")) {
                  if (input_params[i] == "kHOST")                                      gHALO_SUBS_DPDV_SHAPE_PARAMS[k][2]   = kHOST;
                  else                                                                 gHALO_SUBS_DPDV_SHAPE_PARAMS[k][2]   = atof(input_params[i].c_str());
               } else if (name == string("g" + halo + "_SUBS_MASSFRACTION"))           gHALO_SUBS_MASSFRACTION[k]           = atof(input_params[i].c_str());
            }
         }
         // cout << i << " " << j << " " << name.size() << " " << value.size() << " " << gN_INPUTPARAMS;
         //printf("     %-33s %s\n", name[j].c_str(), input_params[i].c_str());
      }
   }

   // Assign correct values for kHOST flag for Galactic parameters:
   bool is_shapeparams_likehost_error = false;
   if (gMW_SUBS_DPDV_FLAG_PROFILE == kHOST)         gMW_SUBS_DPDV_FLAG_PROFILE    = gMW_TOT_FLAG_PROFILE;

   if (int(gMW_SUBS_DPDV_SHAPE_PARAMS[0]) == kHOST) {
      if (gMW_TOT_SHAPE_PARAMS[0] == -1
            && gMW_SUBS_DPDV_FLAG_PROFILE != kBURKERT
            && gMW_SUBS_DPDV_FLAG_PROFILE != kDPDV_PIERI11
            && gMW_SUBS_DPDV_FLAG_PROFILE != kNODES) is_shapeparams_likehost_error = true;
      gMW_SUBS_DPDV_SHAPE_PARAMS[0] = gMW_TOT_SHAPE_PARAMS[0];
   }

   if (int(gMW_SUBS_DPDV_SHAPE_PARAMS[1]) == kHOST) {
      if (gMW_TOT_SHAPE_PARAMS[1] == -1
            && gMW_SUBS_DPDV_FLAG_PROFILE != kBURKERT
            && gMW_SUBS_DPDV_FLAG_PROFILE != kDPDV_PIERI11
            && gMW_SUBS_DPDV_FLAG_PROFILE != kNODES
            && gMW_SUBS_DPDV_FLAG_PROFILE != kDPDV_SPRINGEL08_ANTIBIASED
            && gMW_SUBS_DPDV_FLAG_PROFILE != kEINASTO
            && gMW_SUBS_DPDV_FLAG_PROFILE != kEINASTO_N) is_shapeparams_likehost_error = true;
      gMW_SUBS_DPDV_SHAPE_PARAMS[1] = gMW_TOT_SHAPE_PARAMS[1];
   }

   if (int(gMW_SUBS_DPDV_SHAPE_PARAMS[2]) == kHOST) {
      if (gMW_TOT_SHAPE_PARAMS[2] == -1
            && gMW_SUBS_DPDV_FLAG_PROFILE != kBURKERT
            && gMW_SUBS_DPDV_FLAG_PROFILE != kDPDV_PIERI11
            && gMW_SUBS_DPDV_FLAG_PROFILE != kNODES
            && gMW_SUBS_DPDV_FLAG_PROFILE != kDPDV_SPRINGEL08_ANTIBIASED
            && gMW_SUBS_DPDV_FLAG_PROFILE != kEINASTO
            && gMW_SUBS_DPDV_FLAG_PROFILE != kEINASTO_N) is_shapeparams_likehost_error = true;
      gMW_SUBS_DPDV_SHAPE_PARAMS[2] = gMW_TOT_SHAPE_PARAMS[2];
   }

   if (gMW_SUBS_FLAG_PROFILE ==  kHOST)        gMW_SUBS_FLAG_PROFILE              = gMW_TOT_FLAG_PROFILE;

   if (int(gMW_SUBS_SHAPE_PARAMS[0]) == kHOST) {
      if (gMW_TOT_SHAPE_PARAMS[0] == -1
            && gMW_SUBS_FLAG_PROFILE != kBURKERT
            && gMW_SUBS_DPDV_FLAG_PROFILE != kDPDV_PIERI11
            && gMW_SUBS_FLAG_PROFILE != kNODES) is_shapeparams_likehost_error = true;
      gMW_SUBS_SHAPE_PARAMS[0] = gMW_TOT_SHAPE_PARAMS[0];
   }

   if (int(gMW_SUBS_SHAPE_PARAMS[1]) == kHOST) {
      if (gMW_TOT_SHAPE_PARAMS[1] == -1
            && gMW_SUBS_FLAG_PROFILE != kBURKERT
            && gMW_SUBS_FLAG_PROFILE != kDPDV_PIERI11
            && gMW_SUBS_FLAG_PROFILE != kNODES
            && gMW_SUBS_FLAG_PROFILE != kDPDV_SPRINGEL08_ANTIBIASED
            && gMW_SUBS_FLAG_PROFILE != kEINASTO
            && gMW_SUBS_FLAG_PROFILE != kEINASTO_N) is_shapeparams_likehost_error = true;
      gMW_SUBS_SHAPE_PARAMS[1] = gMW_TOT_SHAPE_PARAMS[1];
   }

   if (int(gMW_SUBS_SHAPE_PARAMS[2]) == kHOST) {
      if (gMW_TOT_SHAPE_PARAMS[2] == -1
            && gMW_SUBS_FLAG_PROFILE != kBURKERT
            && gMW_SUBS_FLAG_PROFILE != kDPDV_PIERI11
            && gMW_SUBS_FLAG_PROFILE != kNODES
            && gMW_SUBS_FLAG_PROFILE != kDPDV_SPRINGEL08_ANTIBIASED
            && gMW_SUBS_FLAG_PROFILE != kEINASTO
            && gMW_SUBS_FLAG_PROFILE != kEINASTO_N) is_shapeparams_likehost_error = true;
      gMW_SUBS_SHAPE_PARAMS[2] = gMW_TOT_SHAPE_PARAMS[2];
   }

   // Assign correct values for kHOST flag for extraalactic parameters (other halo parameters are set in janalysis, halo_load_list):
   if (gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] == kHOST)         gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL]    = gHALO_SUBS_FLAG_PROFILE[kEXTRAGAL];

   if (int(gHALO_SUBS_DPDV_SHAPE_PARAMS[kEXTRAGAL][0]) == kHOST) {
      if (gHALO_SUBS_SHAPE_PARAMS[kEXTRAGAL][0] == -1
            && gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] != kBURKERT
            && gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] != kDPDV_PIERI11
            && gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] != kNODES) is_shapeparams_likehost_error = true;
      gHALO_SUBS_DPDV_SHAPE_PARAMS[kEXTRAGAL][0] = gHALO_SUBS_SHAPE_PARAMS[kEXTRAGAL][0];
   }

   if (int(gHALO_SUBS_DPDV_SHAPE_PARAMS[kEXTRAGAL][1]) == kHOST) {
      if (gHALO_SUBS_SHAPE_PARAMS[kEXTRAGAL][1] == -1
            && gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] != kBURKERT
            && gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] != kDPDV_PIERI11
            && gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] != kNODES
            && gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] != kDPDV_SPRINGEL08_ANTIBIASED
            && gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] != kEINASTO
            && gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] != kEINASTO_N) is_shapeparams_likehost_error = true;
      gHALO_SUBS_DPDV_SHAPE_PARAMS[kEXTRAGAL][1] = gHALO_SUBS_SHAPE_PARAMS[kEXTRAGAL][1];
   }

   if (int(gHALO_SUBS_DPDV_SHAPE_PARAMS[kEXTRAGAL][2]) == kHOST) {
      if (gHALO_SUBS_SHAPE_PARAMS[kEXTRAGAL][2] == -1
            && gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] != kBURKERT
            && gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] != kDPDV_PIERI11
            && gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] != kNODES
            && gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] != kDPDV_SPRINGEL08_ANTIBIASED
            && gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] != kEINASTO
            && gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] != kEINASTO_N) is_shapeparams_likehost_error = true;
      gHALO_SUBS_DPDV_SHAPE_PARAMS[kEXTRAGAL][2] = gHALO_SUBS_SHAPE_PARAMS[kEXTRAGAL][2];
   }

   if (is_shapeparams_likehost_error and gDM_SUBS_NUMBEROFLEVELS > 0) {
      print_error("params.cc", "inputparameters_string2globalparams()",
                  "(i) You have set value 'kHOST' in substructure shape/dPdV parameter description and the corresponding "
                  "shape parameter does not exist for the host halo OR (ii) some shape parameters are missing for the "
                  "substructure description. Please specify explicit value(s) and/or don't use 'kHOST'.");
   }

   if (fabs(gMW_RMAX + 1) < SMALL_NUMBER) {
      double par[10];
      gal_set_partot(par);
      double z = 0.;
      double Delta_c = delta_x_to_delta_crit(gCOSMO_DELTA0, gCOSMO_FLAG_DELTA_REF, z);
      gMW_RMAX = shapeparams_to_Rdelta(par, Delta_c, z);
   }

   // Check for forbidden parameter values:
   // if (gMW_TOT_FLAG_PROFILE == kDPDV_GAO04 || gMW_TOT_FLAG_PROFILE == kDPDV_SPRINGEL08_FIT || gMW_TOT_FLAG_PROFILE == kDPDV_SPRINGEL08_ANTIBIASED) {
   if (gMW_TOT_FLAG_PROFILE == kDPDV_GAO04 || gMW_TOT_FLAG_PROFILE == kDPDV_SPRINGEL08_ANTIBIASED || gMW_TOT_FLAG_PROFILE == kDPDV_PIERI11 || gMW_TOT_FLAG_PROFILE == kISHIYAMA14) {
      sprintf(char_tmp, "The chosen density profile %d = k%s is not valid for parametrizing the total Galactic halo.",
              gMW_TOT_FLAG_PROFILE, gNAMES_PROFILE[gMW_TOT_FLAG_PROFILE]);
      print_error("params.cc", "inputparameters_string2globalparams()", string(char_tmp));
   }
   if (gMW_SUBS_DPDV_FLAG_PROFILE == kISHIYAMA14) {
      sprintf(char_tmp, "The chosen density profile %d = k%s is not valid for parametrizing the "
              "spatial distribution of Galactic subhalos.",
              gMW_SUBS_DPDV_FLAG_PROFILE, gNAMES_PROFILE[gMW_SUBS_DPDV_FLAG_PROFILE]);
      print_error("params.cc", "inputparameters_string2globalparams()", string(char_tmp));
   }
   for (int i = 0; i < gN_TYPEHALOES; ++i) {
      if (gHALO_SUBS_DPDV_FLAG_PROFILE[i] == kISHIYAMA14) {
         sprintf(char_tmp, "The chosen density profile %d = k%s is not valid for parametrizing the spatial distribution of subhalos in the halo object.",
                 gHALO_SUBS_DPDV_FLAG_PROFILE[i], gNAMES_PROFILE[gHALO_SUBS_DPDV_FLAG_PROFILE[i]]);
         print_error("params.cc", "inputparameters_string2globalparams()", string(char_tmp));
      }
   }

   if (gSIM_EXTRAGAL_MMAX > HALO_MMAX) {
      sprintf(char_tmp, "Code cannot handle halo masses larger than Mmax = %f", HALO_MMAX);
      print_error("params.cc", "inputparameters_string2globalparams()", string(char_tmp));

   }
   if (gSIM_EXTRAGAL_MMAX < HALO_MMIN or gDM_SUBS_MMIN < HALO_MMIN) {
      sprintf(char_tmp, "Code cannot handle halo masses smaller than Mmin = %f", HALO_MMIN);
      print_error("params.cc", "inputparameters_string2globalparams()", string(char_tmp));

   }
   return;
}

//______________________________________________________________________________
vector<string> inputparameters_globalparams2string(int enum_simumode)
{
//
   vector<string> inputparam_valuestring(gN_INPUTPARAMS, "-999");

   // dump always out all dependent quantities:
   vector< vector<int> > gPARAMS_REQUIRED_tmp = gPARAMS_REQUIRED;

   vector<bool> is_needed_dummy(gN_INPUTPARAMS);

   bool is_write_all = false;

   if (enum_simumode == -1) {
      is_write_all = true;
      // assign a dummy value to enum_simumode
      enum_simumode = 0;
   }
   // dump hidden parameters to gPARAMS_REQUIRED:
   gPARAMS_REQUIRED[enum_simumode].insert(gPARAMS_REQUIRED[enum_simumode].end(), gPARAMS_HIDDEN[enum_simumode].begin(), gPARAMS_HIDDEN[enum_simumode].end());

//   for (int i = 0; i < gPARAMS_REQUIRED[enum_simumode].size(); ++i) {
//    cout << gSIM_INPUTPARAMS[gPARAMS_REQUIRED[enum_simumode][i]][kSIM_INPUTPARAM_VARNAME] << endl;
//    }
//   abort();

   char char_tmp[256];
   char expo_format[] = "%.8g";
   char float_format[] = "%.16g";
   char integer_format[] = "%d";

   for (int h = 0; h < 3; ++h) {
      // iterate three times:
      // 1.) get dependent parameters
      // 2.) set values of dependent variables missed in first round (as they weren't known to be required)
      // 3.) get and set parameters dependent of values from other parameters
      if (h == 2) get_dependent_required_params(999, inputparam_valuestring, enum_simumode, is_needed_dummy);

      for (int i = 0; i < gN_INPUTPARAMS; ++i) {

         // add cases to write global parameter to string even if it is not contained in gPARAMS_REQUIRED
         bool is_exception = false;
         if (enum_simumode == kf) {
            if (i == kPP_DM_ANNIHIL_DELTA or i == kPP_DM_ANNIHIL_SIGMAV_CM3PERS or i == kPP_DM_DECAY_LIFETIME_S or i == kEXTRAGAL_FLAG_ABSORPTIONPROFILE) {
               is_exception = true;
            }
         }

         if ((find(gPARAMS_REQUIRED[enum_simumode].begin(), gPARAMS_REQUIRED[enum_simumode].end(), i) != gPARAMS_REQUIRED[enum_simumode].end()) || is_write_all || is_exception) {

            if (i == kCOSMO_HUBBLE)                         sprintf(char_tmp, "%.6g", gCOSMO_HUBBLE);
            else if (i == kCOSMO_OMEGA0_M)                  sprintf(char_tmp, "%.6g", gCOSMO_OMEGA0_M);
            else if (i == kCOSMO_OMEGA0_B)                  sprintf(char_tmp, "%.6g", gCOSMO_OMEGA0_B);
            else if (i == kCOSMO_OMEGA0_K)                  sprintf(char_tmp, "%.5g", gCOSMO_OMEGA0_K);
            else if (i == kCOSMO_SIGMA8)                    sprintf(char_tmp, "%.5g", gCOSMO_SIGMA8);
            else if (i == kCOSMO_N_S)                       sprintf(char_tmp, "%.5g", gCOSMO_N_S);
            else if (i == kCOSMO_TAU_REIO)                  sprintf(char_tmp, "%.5g", gCOSMO_TAU_REIO);
            else if (i == kCOSMO_WDE)                       sprintf(char_tmp, expo_format, gCOSMO_WDE);
            else if (i == kCOSMO_T0)                        sprintf(char_tmp, "%.5g", gCOSMO_T0);
            else if (i == kCOSMO_DELTA0)                    sprintf(char_tmp, float_format, gCOSMO_DELTA0);
            else if (i == kCOSMO_FLAG_DELTA_REF)            sprintf(char_tmp, "%s", ("k" + string(gNAMES_DELTA_REF[gCOSMO_FLAG_DELTA_REF])).c_str());

            //--- Dark matter parameters
            else if (i == kDM_LOGCDELTA_STDDEV)             sprintf(char_tmp, float_format, gDM_LOGCDELTA_STDDEV);
            else if (i == kDM_SUBS_MMIN)                    sprintf(char_tmp, expo_format, gDM_SUBS_MMIN);
            else if (i == kDM_SUBS_MMAXFRAC)                sprintf(char_tmp, float_format, gDM_SUBS_MMAXFRAC);
            else if (i == kDM_RHOSAT)                       sprintf(char_tmp, expo_format, gDM_RHOSAT);
            else if (i == kDM_SUBS_NUMBEROFLEVELS)          sprintf(char_tmp, integer_format, gDM_SUBS_NUMBEROFLEVELS);
            else if (i == kDM_RHOHALOES_TO_RHOMEAN)         sprintf(char_tmp, float_format, gDM_RHOHALOES_TO_RHOMEAN);
            else if (i == kDM_IS_IDM)                       sprintf(char_tmp, integer_format, gDM_IS_IDM);
            else if (i == kDM_KMAX)                         sprintf(char_tmp, expo_format, gDM_KMAX);
            else if (i == kDM_KMAX_SIGMA_CUTOFF)            sprintf(char_tmp, float_format, gDM_KMAX_SIGMA_CUTOFF);

            //--- Galactic DM halo parameters (smooth and    clumps)
            else if (i == kMW_SUBS_FLAG_CDELTAMDELTA)       sprintf(char_tmp, "%s", ("k" + string(gNAMES_CDELTAMDELTA[gMW_SUBS_FLAG_CDELTAMDELTA])).c_str());
            else if (i == kMW_SUBS_FLAG_PROFILE) {
               if (gMW_SUBS_FLAG_PROFILE == kHOST)          sprintf(char_tmp, "%s", "kHOST");
               else                                         sprintf(char_tmp, "%s", ("k" + string(gNAMES_PROFILE[gMW_SUBS_FLAG_PROFILE])).c_str());
            } else if (i == kMW_SUBS_SHAPE_PARAMS_0) {
               if (gMW_SUBS_SHAPE_PARAMS[0] == kHOST)       sprintf(char_tmp, "%s", "kHOST");
               else                                         sprintf(char_tmp, float_format, gMW_SUBS_SHAPE_PARAMS[0]);
            } else if (i == kMW_SUBS_SHAPE_PARAMS_1) {
               if (gMW_SUBS_SHAPE_PARAMS[1] == kHOST)       sprintf(char_tmp, "%s", "kHOST");
               else                                         sprintf(char_tmp, float_format, gMW_SUBS_SHAPE_PARAMS[1]);
            } else if (i == kMW_SUBS_SHAPE_PARAMS_2) {
               if (gMW_SUBS_SHAPE_PARAMS[2] == kHOST)       sprintf(char_tmp, "%s", "kHOST");
               else                                         sprintf(char_tmp, float_format, gMW_SUBS_SHAPE_PARAMS[2]);
            } else if (i == kMW_SUBS_DPDM_SLOPE)            sprintf(char_tmp, "%.6g", gMW_SUBS_DPDM_SLOPE);
            else if (i == kMW_SUBS_DPDV_FLAG_PROFILE) {
               if (gMW_SUBS_DPDV_FLAG_PROFILE == kHOST)     sprintf(char_tmp, "%s", "kHOST");
               else                                         sprintf(char_tmp, "%s", ("k" + string(gNAMES_PROFILE[gMW_SUBS_DPDV_FLAG_PROFILE])).c_str());
            } else if (i == kMW_SUBS_DPDV_RSCALE_TO_RS_HOST) sprintf(char_tmp, float_format, gMW_SUBS_DPDV_RSCALE_TO_RS_HOST);
            else if (i == kMW_SUBS_DPDV_SHAPE_PARAMS_0) {
               if (gMW_SUBS_DPDV_SHAPE_PARAMS[0] == kHOST)  sprintf(char_tmp, "%s", "kHOST");
               else                                         sprintf(char_tmp, float_format, gMW_SUBS_DPDV_SHAPE_PARAMS[0]);
            } else if (i == kMW_SUBS_DPDV_SHAPE_PARAMS_1) {
               if (gMW_SUBS_DPDV_SHAPE_PARAMS[1] == kHOST)  sprintf(char_tmp, "%s", "kHOST");
               else                                         sprintf(char_tmp, float_format, gMW_SUBS_DPDV_SHAPE_PARAMS[1]);
            } else if (i == kMW_SUBS_DPDV_SHAPE_PARAMS_2) {
               if (gMW_SUBS_DPDV_SHAPE_PARAMS[2] == kHOST)  sprintf(char_tmp, "%s", "kHOST");
               else                                         sprintf(char_tmp, float_format, gMW_SUBS_DPDV_SHAPE_PARAMS[2]);
            } else if (i == kMW_SUBS_M1)                    sprintf(char_tmp, expo_format, gMW_SUBS_M1);
            else if (i == kMW_SUBS_M2)                      sprintf(char_tmp, expo_format, gMW_SUBS_M2);
            else if (i == kMW_SUBS_N_INM1M2)                sprintf(char_tmp, integer_format, gMW_SUBS_N_INM1M2);
            else if (i == kMW_SUBS_TABULATED_IS)            sprintf(char_tmp, integer_format, gMW_SUBS_TABULATED_IS);
            else if (i == kMW_SUBS_TABULATED_CMIN_OF_R)     sprintf(char_tmp, "%s", gMW_SUBS_TABULATED_CMIN_OF_R.c_str());
            else if (i == kMW_SUBS_TABULATED_LCRIT)         sprintf(char_tmp, "%s", gMW_SUBS_TABULATED_LCRIT.c_str());
            else if (i == kMW_SUBS_TABULATED_RTIDAL_TO_RS)  sprintf(char_tmp, "%s", gMW_SUBS_TABULATED_RTIDAL_TO_RS.c_str());
            else if (i == kMW_RHOSOL)                       sprintf(char_tmp, expo_format, gMW_RHOSOL / GEVperCM3_to_MSOLperKPC3);
            else if (i == kMW_RSOL)                         sprintf(char_tmp, float_format, gMW_RSOL);
            else if (i == kMW_RMAX)                         sprintf(char_tmp, float_format, gMW_RMAX);
            else if (i == kMW_TOT_FLAG_PROFILE)             sprintf(char_tmp, "%s", ("k" + string(gNAMES_PROFILE[gMW_TOT_FLAG_PROFILE])).c_str());
            else if (i == kMW_TOT_RSCALE)                   sprintf(char_tmp, float_format, gMW_TOT_RSCALE);
            else if (i == kMW_TOT_SHAPE_PARAMS_0)           sprintf(char_tmp, float_format, gMW_TOT_SHAPE_PARAMS[0]);
            else if (i == kMW_TOT_SHAPE_PARAMS_1)           sprintf(char_tmp, float_format, gMW_TOT_SHAPE_PARAMS[1]);
            else if (i == kMW_TOT_SHAPE_PARAMS_2)           sprintf(char_tmp, float_format, gMW_TOT_SHAPE_PARAMS[2]);
            else if (i == kMW_TRIAXIAL_AXES_0)              sprintf(char_tmp, float_format, gMW_TRIAXIAL_AXES[0]);
            else if (i == kMW_TRIAXIAL_AXES_1)              sprintf(char_tmp, float_format, gMW_TRIAXIAL_AXES[1]);
            else if (i == kMW_TRIAXIAL_AXES_2)              sprintf(char_tmp, float_format, gMW_TRIAXIAL_AXES[2]);
            else if (i == kMW_TRIAXIAL_IS)                  sprintf(char_tmp, integer_format, gMW_TRIAXIAL_IS);
            else if (i == kMW_TRIAXIAL_ROTANGLES_0)         sprintf(char_tmp, float_format, gMW_TRIAXIAL_ROTANGLES[0]);
            else if (i == kMW_TRIAXIAL_ROTANGLES_1)         sprintf(char_tmp, float_format, gMW_TRIAXIAL_ROTANGLES[1]);
            else if (i == kMW_TRIAXIAL_ROTANGLES_2)         sprintf(char_tmp, float_format, gMW_TRIAXIAL_ROTANGLES[2]);

            //--- extragalactic parameters:
            else if (i == kEXTRAGAL_SUBS_DPDM_SLOPE_LIST)   sprintf(char_tmp, "%s", gEXTRAGAL_SUBS_DPDM_SLOPE_LIST.c_str());
            else if (i == kEXTRAGAL_FLAG_CDELTAMDELTA)      sprintf(char_tmp, "%s", ("k" + string(gNAMES_CDELTAMDELTA[gEXTRAGAL_FLAG_CDELTAMDELTA])).c_str());
            else if (i == kEXTRAGAL_FLAG_CDELTAMDELTA_LIST) sprintf(char_tmp, "%s", gEXTRAGAL_FLAG_CDELTAMDELTA_LIST.c_str());
            else if (i == kEXTRAGAL_FLAG_MASSFUNCTION)      sprintf(char_tmp, "%s", ("k" + string(gNAMES_MASSFUNCTION[gEXTRAGAL_FLAG_MASSFUNCTION])).c_str());
            else if (i == kEXTRAGAL_FLAG_ABSORPTIONPROFILE) {
               if (gEXTRAGAL_FLAG_ABSORPTIONPROFILE != -999) sprintf(char_tmp, "%s", ("k" + string(gNAMES_ABSORPTIONPROFILE[gEXTRAGAL_FLAG_ABSORPTIONPROFILE])).c_str());
               else sprintf(char_tmp, "-999");
            } else if (i == kEXTRAGAL_IDM_MHALFMODE)          sprintf(char_tmp, expo_format, gEXTRAGAL_IDM_MHALFMODE);
            else if (i == kEXTRAGAL_IDM_ALPHA)              sprintf(char_tmp, float_format, gEXTRAGAL_IDM_ALPHA);
            else if (i == kEXTRAGAL_IDM_BETA)               sprintf(char_tmp, float_format, gEXTRAGAL_IDM_BETA);
            else if (i == kEXTRAGAL_IDM_GAMMA)              sprintf(char_tmp, float_format, gEXTRAGAL_IDM_GAMMA);
            else if (i == kEXTRAGAL_IDM_DELTA)              sprintf(char_tmp, float_format, gEXTRAGAL_IDM_DELTA);
            else if (i == kEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE) sprintf(char_tmp, float_format, gEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE);
            else if (i == kEXTRAGAL_HMF_SMALLSCALE_DPDM_MLIM)  sprintf(char_tmp, expo_format, gEXTRAGAL_HMF_SMALLSCALE_DPDM_MLIM);
            else if (i == kEXTRAGAL_TRIAXIAL_AXES_0)        sprintf(char_tmp, float_format, gHALO_TRIAXIAL_AXES[0]);
            else if (i == kEXTRAGAL_TRIAXIAL_AXES_1)        sprintf(char_tmp, float_format, gHALO_TRIAXIAL_AXES[1]);
            else if (i == kEXTRAGAL_TRIAXIAL_AXES_2)        sprintf(char_tmp, float_format, gHALO_TRIAXIAL_AXES[2]);
            else if (i == kEXTRAGAL_TRIAXIAL_IS)            sprintf(char_tmp, integer_format, gHALO_TRIAXIAL_IS);
            else if (i == kEXTRAGAL_TRIAXIAL_ROTANGLES_0)   sprintf(char_tmp, float_format, gHALO_TRIAXIAL_ROTANGLES[0]);
            else if (i == kEXTRAGAL_TRIAXIAL_ROTANGLES_1)   sprintf(char_tmp, float_format, gHALO_TRIAXIAL_ROTANGLES[1]);
            else if (i == kEXTRAGAL_TRIAXIAL_ROTANGLES_2)   sprintf(char_tmp, float_format, gHALO_TRIAXIAL_ROTANGLES[2]);

            //--- Particle physics parameters
            else if (i == kPP_BR) {
               string br_list;
               for (int k = 0; k < gN_PP_BR; ++k) {
                  sprintf(char_tmp, float_format, gPP_BR[k]);
                  br_list += string(char_tmp) + ",";
               }
               sprintf(char_tmp, "%s", br_list.c_str());
            } else if (i == kPP_DM_ANNIHIL_DELTA)           sprintf(char_tmp, integer_format, gPP_DM_ANNIHIL_DELTA);
            else if (i == kPP_DM_ANNIHIL_SIGMAV_CM3PERS)    sprintf(char_tmp, expo_format, gPP_DM_ANNIHIL_SIGMAV_CM3PERS);
            else if (i == kPP_DM_DECAY_LIFETIME_S)          sprintf(char_tmp, expo_format, gPP_DM_DECAY_LIFETIME_S);
            else if (i == kPP_DM_IS_ANNIHIL_OR_DECAY)       sprintf(char_tmp, integer_format, gPP_DM_IS_ANNIHIL_OR_DECAY);
            else if (i == kPP_DM_MASS_GEV)                  sprintf(char_tmp, float_format, gPP_DM_MASS_GEV);
            else if (i == kPP_FLAG_SPECTRUMMODEL)           sprintf(char_tmp, "%s", ("k" + string(gNAMES_PP_SPECTRUMMODEL[gPP_FLAG_SPECTRUMMODEL])).c_str());
            else if (i == kPP_NUMIXING_THETA12_DEG)         sprintf(char_tmp, float_format, gPP_NUMIXING_THETA12_DEG);
            else if (i == kPP_NUMIXING_THETA13_DEG)         sprintf(char_tmp, float_format, gPP_NUMIXING_THETA13_DEG);
            else if (i == kPP_NUMIXING_THETA23_DEG)         sprintf(char_tmp, float_format, gPP_NUMIXING_THETA23_DEG);

            // Sunyaev–Zel'dovich clusters
            else if (i == kSZ_REF_C500)                     sprintf(char_tmp, float_format, gSZ_REF_C500);
            else if (i == kSZ_REF_PRESSURE_NORM)            sprintf(char_tmp, float_format, gSZ_REF_PRESSURE_NORM);
            else if (i == kSZ_REF_SHAPE_PARAMS_0)           sprintf(char_tmp, float_format, gSZ_REF_SHAPE_PARAMS[0]);
            else if (i == kSZ_REF_SHAPE_PARAMS_1)           sprintf(char_tmp, float_format, gSZ_REF_SHAPE_PARAMS[1]);
            else if (i == kSZ_REF_SHAPE_PARAMS_2)           sprintf(char_tmp, float_format, gSZ_REF_SHAPE_PARAMS[2]);
            else if (i == kSZ_REF_ALPHA_MYX)                sprintf(char_tmp, float_format, gSZ_REF_ALPHA_MYX);
            else if (i == kSZ_FLAG_MASSFUNCTION)            sprintf(char_tmp, "%s", ("k" + string(gNAMES_MASSFUNCTION[gSZ_FLAG_MASSFUNCTION])).c_str());

            // Statistical analysis related
            else if (i == kSTAT_FILES)                      sprintf(char_tmp, "%s", gSTAT_FILES.c_str());
            else if (i == kSTAT_CL)                         sprintf(char_tmp, float_format, gSTAT_CL);
            else if (i == kSTAT_CL_LIST)                    sprintf(char_tmp, "%s", gSTAT_CL_LIST.c_str());
            else if (i == kSTAT_ID_LIST)                    sprintf(char_tmp, "%s", gSTAT_ID_LIST.c_str());
            else if (i == kSTAT_MODE)                       sprintf(char_tmp, integer_format, gSTAT_MODE);
            else if (i == kSTAT_IS_LOGL_OR_CHI2)            sprintf(char_tmp, integer_format, gSTAT_IS_LOGL_OR_CHI2);
            else if (i == kSTAT_RKPC_FOR_MR)                sprintf(char_tmp, float_format, gSTAT_RKPC_FOR_MR);
            else if (i == kSTAT_DATAFILES)                  sprintf(char_tmp, "%s", gSTAT_DATAFILES.c_str());

            //--- List of halos
            else if (i == kLIST_HALOES)                     sprintf(char_tmp, "%s", gLIST_HALOES.c_str());
            else if (i == kLIST_HALOES_JEANS)               sprintf(char_tmp, "%s", gLIST_HALOES_JEANS.c_str());
            else if (i == kLIST_HALONAME)                   sprintf(char_tmp, "%s", gLIST_HALONAME.c_str());
            else if (i == kLIST_HALOES_NODES)               sprintf(char_tmp, "%s", gLIST_HALOES_NODES.c_str());
            else if (i == kLIST_HALOES_NODES_RSCALE)        sprintf(char_tmp, "%s", gLIST_HALOES_NODES_RSCALE.c_str());
            else if (i == kLIST_HALO_DISTANCE)              sprintf(char_tmp, float_format, gLIST_HALO_DISTANCE);
            else if (i == kLIST_HALO_RSCALE)                sprintf(char_tmp, float_format, gLIST_HALO_RSCALE);
            else if (i == kLIST_HALO_RHOSCALE)              sprintf(char_tmp, float_format, gLIST_HALO_RHOSCALE);
            else if (i == kLIST_HALO_RDELTA)                sprintf(char_tmp, float_format, gLIST_HALO_RDELTA);

            else if (i == kSIM_FLAG_MODE)                   sprintf(char_tmp, "%s", ("k" + string(gNAMES_SIMUMODES[gSIM_FLAG_MODE])).c_str());

            //--- Simulation parameters
            else if (i == kSIM_EPS)                         sprintf(char_tmp, expo_format, gSIM_EPS);
            else if (i == kSIM_EPS_DRAWN)                   sprintf(char_tmp, expo_format, gSIM_EPS_DRAWN);
            else if (i == kSIM_USER_RSE)                    sprintf(char_tmp, float_format, gSIM_USER_RSE);
            else if (i == kSIM_SEED)                        sprintf(char_tmp, integer_format, gSIM_SEED);
            else if (i == kSIM_OUTPUT_DIR)                  sprintf(char_tmp, "%s", gSIM_OUTPUT_DIR.c_str());

            else if (i == kSIM_R_MIN)                       sprintf(char_tmp, float_format, gSIM_R_MIN);
            else if (i == kSIM_R_MAX)                       sprintf(char_tmp, float_format, gSIM_R_MAX);
            else if (i == kSIM_NX)                          sprintf(char_tmp, integer_format, gSIM_NX);
            else if (i == kSIM_IS_XLOG)                     sprintf(char_tmp, integer_format, gSIM_IS_XLOG);
            else if (i == kSIM_ALPHAINT_MIN)                sprintf(char_tmp, float_format, gSIM_ALPHAINT_MIN * RAD_to_DEG);
            else if (i == kSIM_ALPHAINT_MAX)                sprintf(char_tmp, float_format, gSIM_ALPHAINT_MAX * RAD_to_DEG);
            else if (i == kSIM_THETA_MIN)                   sprintf(char_tmp, float_format, gSIM_THETA_MIN * RAD_to_DEG);
            else if (i == kSIM_THETA_MAX)                   sprintf(char_tmp, float_format, gSIM_THETA_MAX * RAD_to_DEG);
            else if (i == kSIM_SORT_CONTRAST_THRESH)        sprintf(char_tmp, integer_format, gSIM_SORT_CONTRAST_THRESH);
            else if (i == kSIM_PHI_CUT)                     sprintf(char_tmp, float_format, gSIM_PHI_CUT * RAD_to_DEG);

            else if (i == kSIM_PSI_OBS)                     sprintf(char_tmp, float_format, gSIM_PSI_OBS * RAD_to_DEG);
            else if (i == kSIM_THETA_OBS)                   sprintf(char_tmp, "%s", gSIM_THETA_OBS_DEG.c_str());
            else if (i == kSIM_THETA_ORTH_SIZE)             sprintf(char_tmp, "%s", gSIM_THETA_ORTH_SIZE_DEG.c_str());
            else if (i == kSIM_THETA_SIZE)                  sprintf(char_tmp, float_format, gSIM_THETA_SIZE * RAD_to_DEG);

            else if (i == kSIM_IS_ASTRO_OR_PP_UNITS)        sprintf(char_tmp, integer_format, gSIM_IS_ASTRO_OR_PP_UNITS);
            else if (i == kSIM_ALPHAINT)   {
               if (gSIM_ALPHAINT < 0)                       sprintf(char_tmp, "%s", "-1");
               else                                         sprintf(char_tmp, float_format, gSIM_ALPHAINT * RAD_to_DEG);
            } else if (i == kSIM_HEALPIX_NSIDE)             sprintf(char_tmp, integer_format, gSIM_HEALPIX_NSIDE);

            else if (i == kSIM_HEALPIX_RING_WEIGHTS_DIR)    sprintf(char_tmp, "%s", gSIM_HEALPIX_RING_WEIGHTS_DIR.c_str());
            else if (i == kSIM_HEALPIX_ITER)                sprintf(char_tmp, integer_format, gSIM_HEALPIX_ITER);
            else if (i == kSIM_HEALPIX_NLMAX_FAC)           sprintf(char_tmp, float_format, gSIM_HEALPIX_NLMAX_FAC);
            else if (i == kSIM_HEALPIX_FITS_DATATYPE)       sprintf(char_tmp, "%s", string(type2string(gSIM_HEALPIX_FITS_DATATYPE)).c_str());
            else if (i == kSIM_HEALPIX_SCHEME) {
               if (gSIM_HEALPIX_SCHEME == NEST)             sprintf(char_tmp, "%s", "NESTED");
               else if (gSIM_HEALPIX_SCHEME == RING)        sprintf(char_tmp, "%s", "RING");
               else {
                  print_error("params.cc", "inputparameters_globalparams2string()",
                              "gSIM_HEALPIX_SCHEME must be either RING or NEST.");
               }
            } else if (i == kSIM_IS_WRITE_FLUXMAPS)         sprintf(char_tmp, integer_format, gSIM_IS_WRITE_FLUXMAPS);
            else if (i == kSIM_FLUX_IS_INTEG_OR_DIFF)       sprintf(char_tmp, integer_format, gSIM_FLUX_IS_INTEG_OR_DIFF);
            else if (i == kSIM_FLUX_AT_E_GEV)               sprintf(char_tmp, float_format, gSIM_FLUX_AT_E_GEV);
            else if (i == kSIM_XPOWER)                      sprintf(char_tmp, float_format, gSIM_XPOWER);
            else if (i == kSIM_FLUX_EMIN_GEV)               sprintf(char_tmp, float_format, gSIM_FLUX_EMIN_GEV);
            else if (i == kSIM_FLUX_EMAX_GEV)               sprintf(char_tmp, float_format, gSIM_FLUX_EMAX_GEV);
            else if (i == kSIM_FLUX_FLAG_NUFLAVOUR)         sprintf(char_tmp, "%s", ("k" + string(gNAMES_NUFLAVOUR[gSIM_FLUX_FLAG_NUFLAVOUR])).c_str());
            else if (i == kSIM_FLUX_FLAG_FINALSTATE)        sprintf(char_tmp, "%s", ("k" + string(gNAMES_FINALSTATE[gSIM_FLUX_FLAG_FINALSTATE])).c_str());
            else if (i == kSIM_JFACTOR)                     sprintf(char_tmp, expo_format, gSIM_JFACTOR);
            else if (i == kSIM_JFRACTION)                   sprintf(char_tmp, float_format, gSIM_JFRACTION);

            else if (i == kSIM_GAUSSBEAM_GAMMA_FWHM) {
               if (gSIM_GAUSSBEAM_GAMMA_FWHM < 0)           sprintf(char_tmp, "%s",  "-1");
               else                                         sprintf(char_tmp, float_format, gSIM_GAUSSBEAM_GAMMA_FWHM * RAD_to_DEG);
            } else if (i == kSIM_GAUSSBEAM_NEUTRINO_FWHM) {
               if (gSIM_GAUSSBEAM_NEUTRINO_FWHM < 0)        sprintf(char_tmp, "%s",  "-1");
               else                                         sprintf(char_tmp, float_format, gSIM_GAUSSBEAM_NEUTRINO_FWHM * RAD_to_DEG);
            } else if (i == kSIM_IS_WRITE_GALPOWERSPECTRUM) sprintf(char_tmp, integer_format, gSIM_IS_WRITE_GALPOWERSPECTRUM);

            else if (i == kSIM_REDSHIFT)                    sprintf(char_tmp, float_format, gSIM_REDSHIFT);
            else if (i == kSIM_EXTRAGAL_ZMIN)               sprintf(char_tmp, float_format, gSIM_EXTRAGAL_ZMIN);
            else if (i == kSIM_EXTRAGAL_ZMAX)               sprintf(char_tmp, float_format, gSIM_EXTRAGAL_ZMAX);
            else if (i == kSIM_EXTRAGAL_NZ)                 sprintf(char_tmp, integer_format, gSIM_EXTRAGAL_NZ);
            else if (i == kSIM_EXTRAGAL_IS_ZLOG)            sprintf(char_tmp, integer_format, gSIM_EXTRAGAL_IS_ZLOG);
            else if (i == kSIM_EXTRAGAL_DELTAZ_PRECOMP)     sprintf(char_tmp, float_format, gSIM_EXTRAGAL_DELTAZ_PRECOMP);
            else if (i == kSIM_EXTRAGAL_MMIN)               sprintf(char_tmp, expo_format, gSIM_EXTRAGAL_MMIN);
            else if (i == kSIM_EXTRAGAL_MMAX)               sprintf(char_tmp, expo_format, gSIM_EXTRAGAL_MMAX);
            else if (i == kSIM_EXTRAGAL_NM)                 sprintf(char_tmp, integer_format, gSIM_EXTRAGAL_NM);
            else if (i == kSIM_EXTRAGAL_IS_MLOG)            sprintf(char_tmp, integer_format, gSIM_EXTRAGAL_IS_MLOG);
            else if (i == kSIM_EXTRAGAL_NM_PRECOMP)         sprintf(char_tmp, integer_format, gSIM_EXTRAGAL_NM_PRECOMP);
            else if (i == kSIM_EXTRAGAL_FLAG_WINDOWFUNC)    sprintf(char_tmp, "%s", ("k" + string(gNAMES_WINDOWFUNC[gSIM_EXTRAGAL_FLAG_WINDOWFUNC])).c_str());
            else if (i == kSIM_EXTRAGAL_MF_SIGMA_CUTOFF)    sprintf(char_tmp, float_format, gSIM_EXTRAGAL_MF_SIGMA_CUTOFF);
            else if (i == kSIM_EXTRAGAL_KMAX_PRECOMP)       sprintf(char_tmp, float_format, gSIM_EXTRAGAL_KMAX_PRECOMP);
            else if (i == kSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE) sprintf(char_tmp, integer_format, gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE);
            else if (i == kSIM_EXTRAGAL_EBL_UNCERTAINTY)    sprintf(char_tmp, float_format, gSIM_EXTRAGAL_EBL_UNCERTAINTY);
            else if (i == kSIM_EXTRAGAL_FLAG_GROWTHFACTOR)  sprintf(char_tmp, "%s", ("k" + string(gNAMES_GROWTHFACTOR[gSIM_EXTRAGAL_FLAG_GROWTHFACTOR])).c_str());

            else if (i == kSIM_IS_SZ)                       sprintf(char_tmp, integer_format, gSIM_IS_SZ);

            else if (i == kSIM_IS_WRITE_ROOTFILES)          sprintf(char_tmp, integer_format, gSIM_IS_WRITE_ROOTFILES);
            else if (i == kSTAT_N_REALIZATIONS)     sprintf(char_tmp, integer_format, gSTAT_N_REALIZATIONS);

            else {
               //--- CLUSTER, DSPH and GALAXY sub-clump parameters (universal for objects belonging to the same type)
               for (int k = 0; k < gN_TYPEHALOES; ++k) {
                  string halo = gNAMES_TYPEHALOES[k];
                  string name = string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]);
                  if (name == string("g" + halo + "_SUBS_FLAG_CDELTAMDELTA")) {
                     if (gHALO_SUBS_FLAG_CDELTAMDELTA[k] == kHOST)                    sprintf(char_tmp, "%s", "kHOST");
                     else                                                             sprintf(char_tmp, "%s", ("k" + string(gNAMES_CDELTAMDELTA[gHALO_SUBS_FLAG_CDELTAMDELTA[k]])).c_str());
                  } else if (name == string("g" + halo + "_SUBS_FLAG_PROFILE") || (k == 3 && name == string("g" + halo + "_FLAG_PROFILE"))) { // no "SUBS" for extragalactic
                     if (gHALO_SUBS_FLAG_PROFILE[k] == kHOST)                         sprintf(char_tmp, "%s", "kHOST");
                     else                                                             sprintf(char_tmp, "%s", ("k" + string(gNAMES_PROFILE[gHALO_SUBS_FLAG_PROFILE[k]])).c_str());
                  } else if (name == string("g" + halo + "_SUBS_SHAPE_PARAMS_0") || (k == 3 && name == string("g" + halo + "_SHAPE_PARAMS_0"))) { // no "SUBS" for extragalactic
                     if (gHALO_SUBS_SHAPE_PARAMS[k][0] == kHOST) {
                        if (k != 3)                                                   sprintf(char_tmp, "%s", "kHOST");
                        else                                                          sprintf(char_tmp, "%s", "-1");
                     } else                                                           sprintf(char_tmp, float_format, gHALO_SUBS_SHAPE_PARAMS[k][0]);
                  } else if (name == string("g" + halo + "_SUBS_SHAPE_PARAMS_1") || (k == 3 && name == string("g" + halo + "_SHAPE_PARAMS_1"))) { // no "SUBS" for extragalactic
                     if (gHALO_SUBS_SHAPE_PARAMS[k][1] == kHOST) {
                        if (k != 3)                                                   sprintf(char_tmp, "%s", "kHOST");
                        else                                                          sprintf(char_tmp, "%s", "-1");
                     } else                                                           sprintf(char_tmp, float_format, gHALO_SUBS_SHAPE_PARAMS[k][1]);
                  } else if (name == string("g" + halo + "_SUBS_SHAPE_PARAMS_2") || (k == 3 && name == string("g" + halo + "_SHAPE_PARAMS_2"))) { // no "SUBS" for extragalactic
                     if (gHALO_SUBS_SHAPE_PARAMS[k][2] == kHOST) {
                        if (k != 3)                                                   sprintf(char_tmp, "%s", "kHOST");
                        else                                                          sprintf(char_tmp, "%s", "-1");
                     } else                                                           sprintf(char_tmp, float_format, gHALO_SUBS_SHAPE_PARAMS[k][2]);
                  } else if (name == string("g" + halo + "_SUBS_DPDM_SLOPE"))         sprintf(char_tmp, float_format, gHALO_SUBS_DPDM_SLOPE[k]);
                  else if (name == string("g" + halo + "_SUBS_DPDV_FLAG_PROFILE")) {
                     if (gHALO_SUBS_DPDV_FLAG_PROFILE[k] == kHOST)                    sprintf(char_tmp, "%s", "kHOST");
                     else                                                             sprintf(char_tmp, "%s", ("k" + string(gNAMES_PROFILE[gHALO_SUBS_DPDV_FLAG_PROFILE[k]])).c_str());
                  } else if (name == string("g" + halo + "_SUBS_DPDV_RSCALE_TO_RS_HOST")) sprintf(char_tmp, float_format, gHALO_SUBS_DPDV_RSCALE_TO_RS_HOST[k]);
                  else if (name == string("g" + halo + "_SUBS_DPDV_SHAPE_PARAMS_0")) {
                     if (gHALO_SUBS_DPDV_SHAPE_PARAMS[k][0] == kHOST)                 sprintf(char_tmp, "%s", "kHOST");
                     else                                                             sprintf(char_tmp, float_format, gHALO_SUBS_DPDV_SHAPE_PARAMS[k][0]);
                  } else if (name == string("g" + halo + "_SUBS_DPDV_SHAPE_PARAMS_1")) {
                     if (gHALO_SUBS_DPDV_SHAPE_PARAMS[k][1] == kHOST)                 sprintf(char_tmp, "%s", "kHOST");
                     else                                                             sprintf(char_tmp, float_format, gHALO_SUBS_DPDV_SHAPE_PARAMS[k][1]);
                  } else if (name == string("g" + halo + "_SUBS_DPDV_SHAPE_PARAMS_2")) {
                     if (gHALO_SUBS_DPDV_SHAPE_PARAMS[k][2] == kHOST)                 sprintf(char_tmp, "%s", "kHOST");
                     else                                                             sprintf(char_tmp, float_format, gHALO_SUBS_DPDV_SHAPE_PARAMS[k][2]);
                  } else if (name == string("g" + halo + "_SUBS_MASSFRACTION"))       sprintf(char_tmp, float_format, gHALO_SUBS_MASSFRACTION[k]);
               }
            }
            // now set inputparam_valuestring[i]
            inputparam_valuestring[i] = string(char_tmp);

            if (h == 0 or h == 2) get_dependent_required_params(i, inputparam_valuestring, enum_simumode, is_needed_dummy);

         }
      }
   }

   // restore gPARAMS_REQUIRED
   gPARAMS_REQUIRED = gPARAMS_REQUIRED_tmp;

   return inputparam_valuestring;
}

//______________________________________________________________________________
void inputparameters_write_paramfile(const string &filename, int simumode, bool is_write_hidden, const string &inputfile)
{
   //--- writes a file with current global parameters for simulation mode
   //    simumode which can be used as input file.
   //    if is_write_hidden=true, all hidden parameters will also be written.

   FILE *fp;
   fp = fopen(filename.c_str(), "w");

   bool is_cosmo = false;
   bool is_dmglob = false;
   bool is_sub_universal = false;
   bool is_dsph = false;
   bool is_galaxy = false;
   bool is_cluster = false;
   bool is_extragal = false;
   bool is_mw = false;
   bool is_pp = false;
   bool is_sz = false;
   bool is_stat = false;
   bool is_list = false;
   bool is_simu = false;

   bool is_write_all = false;

   // System independent-name for CLUMPY
   fprintf(fp, "# CLUMPY, version %s\n", gCLUMPY_VERSION.c_str());
   string clumpy_path = get_path("$CLUMPY");
   string clumpy_compact_path = gPATH_TO_CLUMPY_EXE;
   if (clumpy_compact_path.find(clumpy_path) != string::npos and clumpy_path != "") {
      if (*clumpy_path.rbegin() == '/') clumpy_compact_path = "$CLUMPY" + clumpy_compact_path.substr(clumpy_path.size() - 1);
      else clumpy_compact_path = "$CLUMPY/" + clumpy_compact_path.substr(clumpy_path.size());
   }

   if (simumode != -1) {
      if (inputfile == "") {
         fprintf(fp, "# Standard parameter file for simulation mode %s\n", gNAMES_SIMUMODES[simumode]);
         if (simumode == kg4) gSIM_IS_XLOG = false;
         else if (simumode == kg2 or simumode == kg6) {
            gSIM_PSI_OBS = 0.;
            gSIM_THETA_OBS_DEG = "0";
            gSIM_THETA_ORTH_SIZE_DEG = "90";
            gSIM_THETA_SIZE = 45 * DEG_to_RAD;
         } else if (simumode == ke1) {
            gCOSMO_FLAG_DELTA_REF = kRHO_MEAN;
         } else if (simumode == ke2) {
            gSIM_NX = 10;
            gSIM_EXTRAGAL_MMIN = 1e-2;
            gDM_LOGCDELTA_STDDEV = 0.2;
         } else if (simumode == ke3) {
            gSIM_EXTRAGAL_IS_ZLOG = false;
            gSIM_EXTRAGAL_ZMIN = 0;
            gSIM_XPOWER = 0;
            gSIM_FLUX_AT_E_GEV = 0.1;
         } else if (simumode == ke4) {
            gDM_SUBS_NUMBEROFLEVELS = 0;
            gSIM_EXTRAGAL_IS_ZLOG = true;
            gSIM_EXTRAGAL_ZMIN = 1e-3;
            gSIM_FLUX_EMIN_GEV = 10.;
            gSIM_FLUX_EMAX_GEV  = 2e5;
            gSIM_EXTRAGAL_NZ = 8;
         } else if (simumode == ke5) {
            gDM_SUBS_NUMBEROFLEVELS = 0;
            gSIM_EXTRAGAL_IS_ZLOG = true;
            gSIM_EXTRAGAL_ZMIN = 1e-3;
            gSIM_EXTRAGAL_NZ = 21;
            gSIM_XPOWER = 1;
         } else if (simumode == ke6) {
            gDM_SUBS_NUMBEROFLEVELS = 0;
         } else if (simumode == kh1) {
            gSIM_R_MIN = 1e-4;
         } else if (simumode == kh3) {
            gSIM_THETA_MIN = 5e-2 * DEG_to_RAD;
            gSIM_THETA_MAX = 5e1 * DEG_to_RAD;
         } else if (simumode == kh4) {
            gSIM_THETA_SIZE = 2 * DEG_to_RAD;
         } else if (simumode == kh5) {
            gSIM_THETA_SIZE = 2 * DEG_to_RAD;
            gSIM_USER_RSE = 10;
         } else if (simumode == kh8 or simumode == kh9 or simumode == kh10) {
            gSIM_NX = 20;
            gSIM_R_MAX = 5;
         } else if (simumode == ks0 or simumode == ks5) {
            gSIM_R_MAX = 10.;
         } else if (simumode == ks6) {
            gSIM_ALPHAINT_MAX = 2. * DEG_to_RAD;
            gSIM_NX = 20.;
            gSIM_EPS = 5e-2;
         } else if (simumode == ks7) {
            gSIM_NX = 20.;
            gSIM_THETA_MIN = 1e-2 * DEG_to_RAD;
            gSIM_THETA_MAX = 2. * DEG_to_RAD;
         } else if (simumode == ks8) {
            gSIM_NX = 20.;
            gSIM_R_MIN = 0.005;
            gSIM_R_MAX = 0.1;
            gSTAT_DATAFILES = "$CLUMPY/data/data_sigmap.txt";
         } else if (simumode == ks9 or simumode == ks10
                    or simumode == ks11 or simumode == ks12) {
            gSIM_NX = 20.;
            gSIM_R_MAX = 2.;
            if (simumode == ks12) gSTAT_DATAFILES = "$CLUMPY/data/data_light.txt";
         }

      } else fprintf(fp, "# Parameter file for simulation mode %s, extracted from %s\n", gNAMES_SIMUMODES[simumode], inputfile.c_str());
      fprintf(fp, "# Execute with %sclumpy -%s -i %s \n\n", clumpy_compact_path.c_str(), gNAMES_SIMUMODES[simumode], filename.c_str());
   } else fprintf(fp, "# Global standard parameter file\n");

   fprintf(fp, "%s%s%s%s%s\n",
           string_fixlength("# Variable name", 40).c_str(), string_fixlength("Unit", gSIM_PARAMLENGTH_MAX).c_str(),
           string_fixlength("Value", gSIM_PARAMLENGTH_MAX + 3).c_str(), string_fixlength("(Format)", gSIM_PARAMLENGTH_MAX + 3).c_str(),
           "(Comment)");

   vector<string> input_params = inputparameters_globalparams2string(simumode);


   if (simumode == -1) {
      is_write_all = true;
      is_write_hidden = true;
      // assign dummy value to simumode:
      simumode = 0;
   }

   for (int i = 0; i < gN_INPUTPARAMS; ++i) {

      // add cases to write global parameter to string even if it is set in input_params
      if (simumode == kf) {
         if (i == kPP_DM_ANNIHIL_DELTA or i == kPP_DM_ANNIHIL_SIGMAV_CM3PERS or i == kPP_DM_DECAY_LIFETIME_S or i == kEXTRAGAL_FLAG_ABSORPTIONPROFILE)
            continue;
      }
      if (i == kSIM_FLAG_MODE) continue;

      if (input_params[i] != "-999") {

         if (find(gPARAMS_HIDDEN[simumode].begin(), gPARAMS_HIDDEN[simumode].end(), i) != gPARAMS_HIDDEN[simumode].end() and is_write_hidden == false) continue;

         int standardparam_length = gSIM_PARAMLENGTH_MAX;
         if (i == kPP_BR or i == kLIST_HALOES or i == kLIST_HALOES_JEANS or i == kEXTRAGAL_FLAG_CDELTAMDELTA_LIST
               or i == kEXTRAGAL_SUBS_DPDM_SLOPE_LIST or i == kSTAT_FILES or i == kSTAT_DATAFILES
               or i == kMW_SUBS_TABULATED_CMIN_OF_R or i == kMW_SUBS_TABULATED_LCRIT or i == kMW_SUBS_TABULATED_RTIDAL_TO_RS)
            standardparam_length = input_params[i].length() + 1;

         if (string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]).substr(0, 6) == "gCOSMO" and is_cosmo == false) {
            fprintf(fp, "\n#------------------------------------------------------------------------------\n");
            fprintf(fp,  "# Cosmological parameters\n");
            fprintf(fp,  "#------------------------------------------------------------------------------\n");
            is_cosmo = true;
         } else if (string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]).substr(0, 3) == "gDM" and is_dmglob == false) {
            fprintf(fp, "\n#------------------------------------------------------------------------------\n");
            fprintf(fp,  "# Dark Matter global parameters\n");
            fprintf(fp,  "#------------------------------------------------------------------------------\n");
            is_dmglob = true;
         } else if (string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]).substr(0, 3) == "gMW" and is_mw == false) {
            fprintf(fp, "\n#------------------------------------------------------------------------------\n");
            fprintf(fp,  "# Milky-Way DM total and clump parameters (see profiles.h and clumps.h)\n");
            fprintf(fp,  "#------------------------------------------------------------------------------\n");
            is_mw = true;
         } else if (string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]).substr(0, 5) == "gEXTR" and is_extragal == false) {
            fprintf(fp, "\n#------------------------------------------------------------------------------\n");
            fprintf(fp,  "# Extragalactic DM total and clump parameters (see cosmo.h and extragal.h)\n");
            fprintf(fp,  "#------------------------------------------------------------------------------\n");
            is_extragal = true;
         } else if (string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]).substr(0, 5) == "gDSPH" or
                    string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]).substr(0, 5) == "gGALA" or
                    string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]).substr(0, 5) == "gCLUS") {
            if (is_sub_universal == false) {
               fprintf(fp, "\n#------------------------------------------------------------------------------\n");
               fprintf(fp,  "# Universal sub-clustering properties for CLUMPY-defined external haloes,\n");
               fprintf(fp,  "# separated into DSPH, GALAXY (but not our Galaxy!), and CLUSTER-like objects.\n");
               fprintf(fp,  "# (N.B.: use gHALO_DPDV_RSCALE=kHOST and/or gHALO_DPDV_FLAG_PROFILE=kHOST\n");
               fprintf(fp,  "#  for the subclump distribution to follow that of the host smooth halo)\n");
               fprintf(fp,  "#------------------------------------------------------------------------------");
               is_sub_universal = true;
            }
            if (string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]).substr(0, 5) == "gDSPH" and is_dsph == false) {
               fprintf(fp,  "\n");
               is_dsph = true;
            } else if (string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]).substr(0, 5) == "gGALA" and is_galaxy == false) {
               fprintf(fp,  "\n");
               is_galaxy = true;
            } else if (string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]).substr(0, 5) == "gCLUS" and is_cluster == false) {
               fprintf(fp,  "\n");
               is_cluster = true;
            }
         } else if (string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]).substr(0, 3) == "gPP" and is_pp == false) {
            fprintf(fp, "\n#------------------------------------------------------------------------------\n");
            fprintf(fp,  "# Particle physics parameters\n");
            fprintf(fp,  "# N.B.: we recommend the use of kCIRELLI11 for the spectra. You then have to\n");
            fprintf(fp,  "#    select branching ratios in gPP_BR as a comma-separated\n");
            fprintf(fp,  "#    list (no space) of Br values for the 28 channels below:\n");
            fprintf(fp,  "#   0: eL+eL-       1: eR+eR-       2: e+e-\n");
            fprintf(fp,  "#   3: muL+muL-     4: muR+muR-     5: mu+mu-\n");
            fprintf(fp,  "#   6: tauL+tauL-   7: tauR+tauR-   8: tau+tau-\n");
            fprintf(fp,  "#   9: qqbar        10: ccbar       11: bbbar      12: ttbar\n");
            fprintf(fp,  "#  13: WL+WL-       14: WT+WT-      15: W+W-\n");
            fprintf(fp,  "#  16: ZLZL         17: ZTZT        18: ZZ\n");
            fprintf(fp,  "#  19: gluglu       20: gammagamma  21: hh\n");
            fprintf(fp,  "#  22: nuenue       23: numunumu    24: nutaunutau\n");
            fprintf(fp,  "#  25: VV4e         26: VV4mu       27: VV4tau\n");
            fprintf(fp,  "#------------------------------------------------------------------------------\n");
            is_pp = true;
         } else if (string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]).substr(0, 3) == "gSZ" and is_sz == false) {
            fprintf(fp, "\n#------------------------------------------------------------------------------\n");
            fprintf(fp,  "# Sunyaev-Zel'dovich module (not yet implemented in this version)\n");
            fprintf(fp,  "#------------------------------------------------------------------------------\n");
            is_sz = true;
         } else if (string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]).substr(0, 5) == "gSTAT" and is_stat == false) {
            fprintf(fp, "\n#------------------------------------------------------------------------------\n");
            fprintf(fp,  "# Statistical analysis of single halo\n");
            fprintf(fp,  "#------------------------------------------------------------------------------\n");
            is_stat = true;
         } else if (string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]).substr(0, 5) == "gLIST" and is_list == false) {
            fprintf(fp, "\n#------------------------------------------------------------------------------\n");
            fprintf(fp,  "# External lists with specified objects to add to simulations\n");
            fprintf(fp,  "#------------------------------------------------------------------------------\n");
            is_list = true;
         } else if (string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME]).substr(0, 4) == "gSIM" and is_simu == false) {
            fprintf(fp, "\n#------------------------------------------------------------------------------\n");
            fprintf(fp,  "# Simulation parameters\n");
            fprintf(fp,  "#  Some parameters can be set to default values respectively automatically\n");
            fprintf(fp,  "#  adapted by setting to -1 (see documentation).\n");
            fprintf(fp,  "#  gSIM_OUTPUT_DIR: when set to -1, set to folder from where clumpy is executed.\n");
            fprintf(fp,  "#  gSIM_SEED: if=0, seed is chosen from computer clock (for drawing clumps)\n");
            fprintf(fp,  "#------------------------------------------------------------------------------\n");
            is_simu = true;
         }

         // exception for halo coordinates in halo mode:
         bool exception = false;
         if (simumode == kh4 or simumode == kh5) {
            if (i == kSIM_THETA_OBS or
                  i == kSIM_PSI_OBS or
                  i == kSIM_THETA_ORTH_SIZE or
                  i == kSIM_REDSHIFT or
                  i == kLIST_HALO_DISTANCE or
                  i == kLIST_HALO_RSCALE or
                  i == kLIST_HALO_RHOSCALE or
                  i == kLIST_HALO_RDELTA or
                  i == kEXTRAGAL_FLAG_PROFILE or
                  i == kEXTRAGAL_SHAPE_PARAMS_0 or
                  i == kEXTRAGAL_SHAPE_PARAMS_1 or
                  i == kEXTRAGAL_SHAPE_PARAMS_2 or
                  i == kEXTRAGAL_TRIAXIAL_IS or
                  i == kEXTRAGAL_TRIAXIAL_AXES_0 or
                  i == kEXTRAGAL_TRIAXIAL_AXES_1 or
                  i == kEXTRAGAL_TRIAXIAL_AXES_2 or
                  i == kEXTRAGAL_TRIAXIAL_ROTANGLES_0 or
                  i == kEXTRAGAL_TRIAXIAL_ROTANGLES_1 or
                  i == kEXTRAGAL_TRIAXIAL_ROTANGLES_2) {
               exception = true;
            }

         }
         if (!exception) {
            fprintf(fp, "%s%s%s   %s%s\n",
                    string_fixlength(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME], 40).c_str(),
                    string_fixlength(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_UNIT], gSIM_PARAMLENGTH_MAX).c_str(),
                    string_fixlength(input_params[i], standardparam_length).c_str(),
                    string_fixlength("<" + string(gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_DATATYPE]) + ">", gSIM_PARAMLENGTH_MAX + 3).c_str(),
                    gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_DESCRIPTION]);
         }
      }

   }
   if (inputfile != "") {
      if (!is_write_all) {
         cout << "\n   Extracted parameters for simulation mode " << gNAMES_SIMUMODES[simumode] << endl;
         cout << "   written to file " << filename << endl;
      }

   } else {
      if (!is_write_all) cout << "\n   Default parameters for simulation mode " << gNAMES_SIMUMODES[simumode] << " have been written to file " << filename << endl;
      else cout << "\n   Default clumpy parameters have been written to file " << filename << endl;
      cout << "   Now execute:\n"  << endl;
      if (!is_write_all) {
         cout << "   " << clumpy_compact_path << "clumpy -" << gNAMES_SIMUMODES[gSIM_FLAG_MODE] << " -i "  << filename;
         if (gSIM_FLAG_MODE == kf) cout << " -r inputoutputfile.fits";
         cout << "\n" << endl;
      } else  cout << "   " << clumpy_compact_path << "clumpy -any_simumode -i "  << filename << "\n" << endl;

      cout << "   or alternatively, use the following default command line variables & values:\n" << endl;
      if (!is_write_all) {
         cout << "   " << clumpy_compact_path << "clumpy -" << gNAMES_SIMUMODES[gSIM_FLAG_MODE] << " ";
         if (gSIM_FLAG_MODE == kf) cout << "-r inputoutputfile.fits ";
      }      else  cout << "   " << clumpy_compact_path  << "clumpy -any_simumode  ";
      for (int i = 0; i < gN_INPUTPARAMS; ++i) {

         if (simumode == kf) {
            if (i == kPP_DM_ANNIHIL_DELTA or i == kPP_DM_ANNIHIL_SIGMAV_CM3PERS or i == kPP_DM_DECAY_LIFETIME_S or i == kEXTRAGAL_FLAG_ABSORPTIONPROFILE)
               continue;
         }

         if (i == kSIM_FLAG_MODE) continue;

         if (input_params[i] != "-999") {
            if (simumode == kh4 or simumode == kh5) {
               if (i == kSIM_THETA_OBS or
                     i == kSIM_PSI_OBS or
                     i == kSIM_THETA_ORTH_SIZE or
                     i == kSIM_REDSHIFT or
                     i == kLIST_HALO_DISTANCE or
                     i == kLIST_HALO_RSCALE or
                     i == kLIST_HALO_RHOSCALE or
                     i == kLIST_HALO_RDELTA or
                     i == kEXTRAGAL_FLAG_PROFILE or
                     i == kEXTRAGAL_SHAPE_PARAMS_0 or
                     i == kEXTRAGAL_SHAPE_PARAMS_1 or
                     i == kEXTRAGAL_SHAPE_PARAMS_2 or
                     i == kEXTRAGAL_TRIAXIAL_IS or
                     i == kEXTRAGAL_TRIAXIAL_AXES_0 or
                     i == kEXTRAGAL_TRIAXIAL_AXES_1 or
                     i == kEXTRAGAL_TRIAXIAL_AXES_2 or
                     i == kEXTRAGAL_TRIAXIAL_ROTANGLES_0 or
                     i == kEXTRAGAL_TRIAXIAL_ROTANGLES_1 or
                     i == kEXTRAGAL_TRIAXIAL_ROTANGLES_2) {
                  continue;
               }
            }

            if (find(gPARAMS_HIDDEN[simumode].begin(), gPARAMS_HIDDEN[simumode].end(), i) != gPARAMS_HIDDEN[simumode].end() and is_write_hidden == false) continue;
            cout << "--" << gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME] << "=" << input_params[i] << " ";
         }
      }
      cout << endl;
   }
   fclose(fp);
   return;
}


//______________________________________________________________________________
int string_to_enum(string flag_type, string card_param)
{
   //--- Returns index of parameter par in a list of available values for a
   //    given global enum parameter.
   //
   //  flag_type     Flag name of the enum list to choose from (e.g., FLAG_PROFILE)
   //  card_param    Parameter name sought in the above enum list (e.g., ZHAO)


   // Upper-case + discard first character 'k'
   string flag_raw = upper_case(card_param);
   string flag = flag_raw.substr(1);
   char char_tmp[256];

   // Loop on all parameter names until matches
   if (flag_type.find("FLAG_PROFILE") != string::npos) {
      if (flag == "HOST")
         return kHOST;
      for (int i = 0; i < gN_PROFILE; ++i) {
         if (flag == upper_case(gNAMES_PROFILE[i])) return i;
      }
   } else if (flag_type.find("FLAG_CDELTAMDELTA") != string::npos) {
      for (int i = 0; i < gN_CDELTAMDELTA; ++i) {
         if (flag == upper_case(gNAMES_CDELTAMDELTA[i])) return i;
      }
   } else if (flag_type.find("FLAG_CVIR_DIST") != string::npos) {
      for (int i = 0; i < gN_CVIR_DIST; ++i) {
         if (flag == upper_case(gNAMES_CVIR_DIST[i])) return i;
      }
   } else if (flag_type.find("FLAG_FINALSTATE") != string::npos) {
      for (int i = 0; i < gN_FINALSTATE; ++i) {
         if (flag == upper_case(gNAMES_FINALSTATE[i])) return i;
      }
   } else if (flag_type.find("FLAG_SPECTRUMMODEL") != string::npos) {
      for (int i = 0; i < gN_PP_SPECTRUMMODEL; ++i) {
         if (flag == upper_case(gNAMES_PP_SPECTRUMMODEL[i])) return i;
      }
   } else if (flag_type.find("FLAG_LIGHTPROFILE") != string::npos) {
      for (int i = 0; i < gN_LIGHTPROFILE; ++i) {
         if (flag == upper_case(gNAMES_LIGHTPROFILE[i])) return i;
      }
   } else if (flag_type.find("FLAG_ANISOTROPYPROFILE") != string::npos) {
      for (int i = 0; i < gN_ANISOTROPYPROFILE; ++i) {
         if (flag == upper_case(gNAMES_ANISOTROPYPROFILE[i])) return i;
      }
   } else if (flag_type.find("FLAG_NUFLAVOUR") != string::npos) {
      for (int i = 0; i < gN_NUFLAVOUR; ++i) {
         if (flag == upper_case(gNAMES_NUFLAVOUR[i])) return i;
      }
   } else if (flag_type.find("FLAG_TYPEHALOES") != string::npos) {
      for (int i = 0; i < gN_TYPEHALOES; ++i) {
         if (flag == upper_case(gNAMES_TYPEHALOES[i])) return i;
      }
   } else if (flag_type.find("FLAG_MODE") != string::npos) {
      for (int i = 0; i < gN_SIMUMODES; ++i) {
         if (flag == upper_case(gNAMES_SIMUMODES[i])) return i;
      }
   } else if (flag_type.find("FLAG_MASSFUNCTION") != string::npos) {
      for (int i = 0; i < gN_MASSFUNCTION; ++i) {
         if (flag == upper_case(gNAMES_MASSFUNCTION[i])) return i;
      }
   } else if (flag_type.find("FLAG_ABSORPTIONPROFILE") != string::npos) {
      for (int i = 0; i < gN_ABSORPTIONPROFILE; ++i) {
         if (flag == upper_case(gNAMES_ABSORPTIONPROFILE[i])) return i;
      }
   } else if (flag_type.find("FLAG_WINDOWFUNC") != string::npos) {
      for (int i = 0; i < gN_WINDOWFUNC; ++i) {
         if (flag == upper_case(gNAMES_WINDOWFUNC[i])) return i;
      }
   } else if (flag_type.find("FLAG_DELTA_REF") != string::npos) {
      for (int i = 0; i < gN_DELTA_REF; ++i) {
         if (flag == upper_case(gNAMES_DELTA_REF[i])) return i;
      }
   } else if (flag_type.find("FLAG_GROWTHFACTOR") != string::npos) {
      for (int i = 0; i < gN_GROWTHFACTOR; ++i) {
         if (flag == upper_case(gNAMES_GROWTHFACTOR[i])) return i;
      }
   }


   // If not found, print available options, and then abort()
   sprintf(char_tmp, "Option %s for flag %s does not exist. Possible options are: ",
           card_param.c_str(), flag_type.c_str());
   print_error(" params.cc", "string_to_enum()", string(char_tmp), false);
   cout << " ";

   if (flag_type.find("FLAG_PROFILE") != string::npos) {
      for (int i = 0; i < gN_PROFILE; ++i)
         cout << "k" << gNAMES_PROFILE[i] << " ";
   } else if (flag_type.find("FLAG_CDELTAMDELTA") != string::npos) {
      for (int i = 0; i < gN_CDELTAMDELTA; ++i)
         cout << "k" << gNAMES_CDELTAMDELTA[i] << " ";
   } else if (flag_type.find("FLAG_CVIR_DIST") != string::npos) {
      for (int i = 0; i < gN_CVIR_DIST; ++i)
         cout << "k" << gNAMES_CVIR_DIST[i] << " ";
   } else if (flag_type.find("FLAG_FINALSTATE") != string::npos) {
      for (int i = 0; i < gN_FINALSTATE; ++i)
         cout << "k" << gNAMES_FINALSTATE[i] << " ";
   } else if (flag_type.find("FLAG_SPECTRUMMODEL") != string::npos) {
      for (int i = 0; i < gN_PP_SPECTRUMMODEL; ++i)
         cout << "k" << gNAMES_PP_SPECTRUMMODEL[i] << " ";
   } else if (flag_type.find("FLAG_LIGHTPROFILE") != string::npos) {
      for (int i = 0; i < gN_LIGHTPROFILE; ++i)
         cout << "k" << gNAMES_LIGHTPROFILE[i] << " ";
   } else if (flag_type.find("FLAG_ANISOTROPYPROFILE") != string::npos) {
      for (int i = 0; i < gN_ANISOTROPYPROFILE; ++i)
         cout << "k" << gNAMES_ANISOTROPYPROFILE[i] << " ";
   } else if (flag_type.find("FLAG_NUFLAVOUR") != string::npos) {
      for (int i = 0; i < gN_NUFLAVOUR; ++i)
         cout << "k" << gNAMES_NUFLAVOUR[i] << " ";
   } else if (flag_type.find("FLAG_TYPEHALOES") != string::npos) {
      for (int i = 0; i < gN_TYPEHALOES; ++i)
         cout << "k" << gNAMES_TYPEHALOES[i] << " ";
   } else if (flag_type.find("FLAG_MODE") != string::npos) {
      for (int i = 0; i < gN_SIMUMODES; ++i)
         cout << "k" << gNAMES_SIMUMODES[i] << " ";
   } else if (flag_type.find("FLAG_MASSFUNCTION") != string::npos) {
      for (int i = 0; i < gN_MASSFUNCTION; ++i)
         cout << "k" << gNAMES_MASSFUNCTION[i] << " ";
   } else if (flag_type.find("FLAG_ABSORPTIONPROFILE") != string::npos) {
      for (int i = 0; i < gN_ABSORPTIONPROFILE; ++i)
         cout << "k" << gNAMES_ABSORPTIONPROFILE[i] << " ";
   } else if (flag_type.find("FLAG_WINDOWFUNC") != string::npos) {
      for (int i = 0; i < gN_WINDOWFUNC; ++i)
         cout << "k" << gNAMES_WINDOWFUNC[i] << " ";
   } else if (flag_type.find("FLAG_DELTA_REF") != string::npos) {
      for (int i = 0; i < gN_DELTA_REF; ++i)
         cout << "k" << gNAMES_DELTA_REF[i] << " ";
   } else if (flag_type.find("FLAG_GROWTHFACTOR") != string::npos) {
      for (int i = 0; i < gN_GROWTHFACTOR; ++i)
         cout << "k" << gNAMES_GROWTHFACTOR[i] << " ";
   }
   printf(COLOR_BRED "\n             => abort()\n\n" COLOR_RESET);
   abort();

   return 0;
}
