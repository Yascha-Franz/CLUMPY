/*! \file jeans_analysis.cc \brief (see jeans_analysis.h) */

// CLUMPY includes
#include "../include/clumps.h"
#include "../include/inlines.h"
#include "../include/integr.h"
#include "../include/params.h"
#include "../include/profiles.h"
#include "../include/janalysis.h"
#include "../include/jeans_analysis.h"
#include "../include/misc.h"

// GSL includes
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_sf_hyperg.h>

// C++ std libraries
using namespace std;
#include <iostream>
#include <fstream>

//______________________________________________________________________________
double beta_anisotropy(double &r, double par_ani[5])
{
   //--- Returns the anisotropy profile (no unit) at a radius r [kpc] for any
   //    anisotropy profile available in gENUM_ANISOTROPYPROFILE (see params.h).
   //    Note that physical values for the anisotropy are smaller than 1.
   //
   //  r                    Radius [kpc]
   //  par_ani[0]    Anisotropy at r=0
   //  par_ani[1]    Anisotropy at r=+infinity
   //  par_ani[2]    Anisotropy scale radius ra [kpc]
   //  par_ani[3]    Anisotropy shape parameter eta
   //  par_ani[4]    Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)

   double beta = 0.;

   int card_profile = int(par_ani[4]);
   switch (card_profile) {
      case kBAES: {
            beta = beta_anisotropy_BAES(r, par_ani);
            break;
         }
      case kCONSTANT: {
            beta = beta_anisotropy_CONSTANT(r, par_ani);
            break;
         }
      case kOSIPKOV: {
            beta = beta_anisotropy_OSIPKOV(r, &par_ani[2]);
            break;
         }
      default: {
            printf("\n====> ERROR: beta_anisotropy() in jeans_analysis.cc");
            printf("\n             Profile %d does not correspond to any anisotropy profile", card_profile);
            printf("\n             => abort()\n\n");
            abort();
         }
   }

   // Beta_anisotropy must be <= 1 to be physical
   if (beta > 1.) {
      printf("\n====> ERROR: beta_anisotropy() in jeans_analysis.cc");
      printf("\n             beta_ani(r=%.2le kpc) > 1 is not allowed (here profile=%s)",
             r, gNAMES_ANISOTROPYPROFILE[card_profile]);
      printf("\n             => abort()\n\n");
      abort();
   }
   return beta;
}

//______________________________________________________________________________
double beta_anisotropy_fr(double &r, double par_ani[5], double const &eps)
{
   //--- Returns f(r)=f_r1 \int_{r1}^\infty (2/t) \beta^{ani}(t) dt. For most
   //    profiles, an analytical solution exists. Note that the integration on
   //    the lower boundary r1 leads to a normalisation factor that disappears
   //    in the solution \nu(r)\bar{v_r^2}(r), in which f(r) appears both in
   //    the numerator and denominator (hence f(r) has no unit).
   //
   //  r             Radius [kpc]
   //  par_ani[0]    Anisotropy at r=0
   //  par_ani[1]    Anisotropy at r=+infinity
   //  par_ani[2]    Anisotropy scale radius ra [kpc]
   //  par_ani[3]    Anisotropy shape parameter eta
   //  par_ani[4]    Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  eps           Relative precision sought for integrations [if required]

   int card_profile = int(par_ani[4]);
   switch (card_profile) {
      case kCONSTANT: {
            return pow(r, 2.*par_ani[0]);
         }
      case kBAES: {
            return pow(r, 2.*par_ani[0])
                   * pow(1. + pow(r / par_ani[2], par_ani[3]), 2.*(par_ani[1] - par_ani[0]) /
                         par_ani[3]);
         }
      case kOSIPKOV: {
            // N.B.: we do not divide by ra^2 which cancels out for Jeans calculations
            return (r * r + par_ani[2] * par_ani[2]);
         }
      default: {
            // N.B.: so far, eps unused, because an analytical form of f(r)
            // for all profiles is implemented!
            printf("\n====> ERROR: beta_anisotropy_fr() in jeans_analysis.cc");
            printf("\n             Profile %d does not correspond to any anisotropy profile", card_profile);
            printf("\n             => abort()\n\n");
            abort();
            //printf("DUMMY (no printed): %le\n", eps);
         }
   }
   return 0.;
}

//______________________________________________________________________________
double beta_anisotropy_kernel(double &r, double par_ani[5], double const &R)
{
   //--- Returns (if exist) the kernel (no unit) to calculate in one single
   //    integration the solution of the projected Jeans equation, as discussed
   //    in Appendix of Mamon & Lokas, MNRAS 363, 705 (2005).
   //
   //  r             Radius from halo centre [kpc]
   //  par_ani[0]    Anisotropy at r=0
   //  par_ani[1]    Anisotropy at r=+infinity
   //  par_ani[2]    Anisotropy scale radius ra [kpc]
   //  par_ani[3]    Anisotropy shape parameter eta
   //  par_ani[4]    Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  R             Projected radius from halo centre [kpc]

   double u = r / R;

   if (u < 1.)
      return 0.;

   int card_profile = int(par_ani[4]);
   switch (card_profile) {
      case kCONSTANT: {

            double um2 = 1. / (u * u);
            double beta0 = par_ani[0];
            // N.B.: if beta_ani(r) too close to integer or half-integer (e.g.,
            // 0, 1, -0.5, etc.), the Gamma function (in the integrand) is numerically
            // unstable (see Mamon & Lokas 2005 addendum)
            //   => we slighly shift it
            double tol = 1.e-3;
            double residual = beta0 - 0.5;
            residual -= round(residual);
            if (abs(residual) < tol) {
               if (residual < 0)
                  beta0 -= tol;
               else
                  beta0 += tol;
            }
            double residual_2 = beta0 - round(beta0);
            if (abs(residual_2) < tol) {
               if (residual_2 < 0)
                  beta0 -= tol;
               else
                  beta0 += tol;
            }

            double a = beta0 + 0.5;
            double b = 0.5;

            // The regularized incomplete beta function I can be used directly, or it
            // can be calculated by means of the hypergeometric 2F1 and beta functions.
            // N.B.: validity range of gsl_sf_beta_inc (or gsl_sf_hyperg_2F1) is 0<=um2<=1
            // with gsl_sf_beta_inc(a,b,x=1) = gsl_sf_hyperg_2F1(a,b,x=1) = 1
            if (um2 < 0.) um2 = 0.;
            if (um2 > 1.) um2 = 1.;

            //double I = gsl_sf_beta_inc(a, b, um2);
            double I = (pow(um2, a) / a * gsl_sf_hyperg_2F1(a, 1. - b, a + 1., um2))
                       / gsl_sf_beta(a, b);
            // Kernel from Eq.(A16) of Mamon & Lokas MNRAS 363, 705 (2005) + MNRAS 370, 1582 (2006)
            return sqrt(1. - um2) / (1. - 2.*beta0) + 0.5 * sqrt(PI)
                   * gsl_sf_gamma(beta0 - 0.5) / gsl_sf_gamma(beta0)
                   * (1.5 - beta0) * pow(u, 2.*beta0 - 1.) * (1. - I);

         }
      case kOSIPKOV: {
            double ua = par_ani[2] / R;
            double ua2 = ua * ua;
            double u2 = u * u;

            // Kernel from Eq.(A16) of Mamon & Lokas MNRAS 363, 705 (2005)
            return (u2 + ua2) * (ua2 + 0.5) / (u * pow(ua2 + 1., 1.5))
                   * atan(sqrt((u2 - 1.) / (ua2 + 1.)))
                   - 0.5 * sqrt(1. - 1. / u2) / (ua2 + 1.) ;
         }
      default: {
            printf("\n====> ERROR: beta_anisotropy_kernel() in jeans_analysis.cc");
            printf("\n             Profile %d does not correspond to any anisotropy profile", card_profile);
            printf("\n             => abort()\n\n");
            abort();
         }
   }
   return 0.;
}

//______________________________________________________________________________
string beta_anisotropy_legend(double par_ani[5], bool is_with_formula)
{
   //--- Returns a name as "profile name (#1,#2,#3,#4)" for beta_anisotropy.
   //
   //  par_ani[0]    Anisotropy at r=0
   //  par_ani[1]    Anisotropy at r=+infinity
   //  par_ani[2]    Anisotropy scale radius ra [kpc]
   //  par_ani[3]    Anisotropy shape parameter eta
   //  par_ani[4]    Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  is_with_formula Whether to write full formula in legend or not

   int card_profile = (int)par_ani[4];
   char tmp[500];
   string name = gNAMES_ANISOTROPYPROFILE[card_profile];
   string formula = "";

   switch (card_profile) {
      case kCONSTANT:
         sprintf(tmp, "%s (#beta_{0}=%.2lf)", name.c_str(), par_ani[0]);
         return tmp;
      case kOSIPKOV:
         if (is_with_formula)
            formula = "#beta(r) = #frac{r^{2}}{r^{2}+r_{a}^{2}}";
         sprintf(tmp, "%s %s (r_{a}=%.2lf)", name.c_str(), formula.c_str(), par_ani[2]);
         return tmp;
      case kBAES:
         if (is_with_formula)
            formula = "#beta(r) = #frac{#beta_{0} + #beta_{#infty}(r/r_{a})^{#eta}}{1+(r/r_{a})^{#eta}}";
         sprintf(tmp, "%s %s (#beta_{0}^{#infty}=_{%+.1lf}^{%+.1lf}, r_{a}=%.2lf, #eta=%.1lf)",
                 name.c_str(), formula.c_str(), par_ani[0], par_ani[1],
                 par_ani[2], par_ani[3]);
         return tmp;
      default :
         printf("\n====> ERROR: beta_anisotropy_legend() in jeans_analysis.cc");
         printf("\n             card_profile=%d does not correspond to any anisotropy profile", card_profile);
         printf("\n             => abort()\n\n");
         abort();
   }
   return "";
}

//______________________________________________________________________________
double beta_anisotropy_CONSTANT(double &r, double par[1])
{
   //--- Returns anisotropy profile (no unit) at radius r [kpc] for constant profile.
   //
   //  r             Radius [kpc]
   //  par[0]        Anisotropy constant value

   r += 0.;
   return par[0];
}

//______________________________________________________________________________
double beta_anisotropy_BAES(double &r, double par[4])
{
   //--- Returns anisotropy profile (no unit) at radius r [kpc] for Baes & van Hese
   //    profile, as described in Baes & van Hese, A&A 471, 419 (2007):
   //           beta(r) = [ beta_0 +beta_infty (r/ra)^eta] / [ 1 + (r/ra)^eta].
   //
   //  r             Radius [kpc]
   //  par[0]        Anisotropy at r=0
   //  par[1]        Anisotropy at r=+infinity
   //  par[2]        Anisotropy scale radius [kpc]
   //  par[3]        Anisotropy shape parameter eta (transition smoothness)

   return (par[0] + par[1] * pow(r / par[2], par[3])) / (1. + pow(r / par[2], par[3]));
}

//______________________________________________________________________________
double beta_anisotropy_OSIPKOV(double &r, double par[1])
{
   //--- Returns anisotropy profile (no unit) at radius r [kpc] for Osipkov-Merrit,
   //    as described in Osipkov, PAZh 5, 77O (1979) and Merritt, AJ 90, 1027 (1985):
   //           beta(r) = r^2 / [r^2 + ra^2].
   //
   //  r             Radius [kpc]
   //  par[0]        Anisotropy scale radius [kpc]

   return pow(r, 2.) / (pow(r, 2.) + pow(par[0], 2.));
}

//______________________________________________________________________________
double light_i(double &R, double par_light[8])
{
   //--- Returns at projected radius R [kpc] the projected (2D) surface density I(R) [Lsol].
   //
   //  R             Projected radius [kpc]
   //  par_light[0]  Light profile 1st free par (usually normalisation)
   //  par_light[1]  Light profile 2nd free par (usually scale radius [kpc])
   //  par_light[2]  Light profile 3rd free par (if used)
   //  par_light[3]  Light profile 4th free par (if used)
   //  par_light[4]  Light profile 5th free par (if used)
   //  par_light[5]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_light[6]  If not analytical, maximum radius for integration [kpc]
   //  par_light[7]  If not analytical, relative precision for the integration

   double rmax_integr = par_light[6];

   // In light_profile():
   //   - switch_qty set to 1 to calculate I(R)
   //   - par_light[6] set to R
   par_light[6] = R;
   double res = light_profile(R, par_light, 1, rmax_integr);

   // Reset par_light[6] to its former value
   par_light[6] = rmax_integr;

   if (res < 1.e-60) // If i_light is zero (e.g. for King profile above rh)
      return 0.;

   return res;
}

//______________________________________________________________________________
void light_i_integrand(double &y, double par_light[8], double &res)
{
   //--- Evaluates the light profile integrand [kpc^{-3}] which enters the calculation
   //    of the surface brightness (at a projected radius R [kpc]). We have
   //       I(R) = 2 \int_0^\infty nu(y) dy  with y = sqrt(r^2-R^2)
   //    so that the integrand is
   //       i_integrand = 2 * nu(y)
   //    N.B.: i_integrand = light_profile(y, par_light, 4).
   //
   // INPUTS:
   //  y             Integration variable y = sqrt(r^2-R^2)
   //  par_light[0]  Light profile 1st free par (usually normalisation)
   //  par_light[1]  Light profile 2nd free par (usually scale radius [kpc])
   //  par_light[2]  Light profile 3rd free par (if used)
   //  par_light[3]  Light profile 4th free par (if used)
   //  par_light[4]  Light profile 5th free par (if used)
   //  par_light[5]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_light[6]  Radius R [kpc] considered for projection
   //  par_light[7]  UNUSED
   // OUTPUT:
   //  res                  2*nu(y) [integrand required to calculate I(R)]

   // In light_profile():
   //   - switch_qty set to 3 to calculate 2 * nu(y)
   res = light_profile(y, par_light, 3);
}

//______________________________________________________________________________
double light_i_norm(double par_light[8])
{
   //--- Returns normalisation [Lsol kpc^2] for surface brightness \int_0^rmax 2*pi*R*I(R)dR.
   //
   //  par_light[0]  Light profile 1st free par (usually normalisation)
   //  par_light[1]  Light profile 2nd free par (usually scale radius [kpc])
   //  par_light[2]  Light profile 3rd free par (if used)
   //  par_light[3]  Light profile 4th free par (if used)
   //  par_light[4]  Light profile 5th free par (if used)
   //  par_light[5]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_light[6]  Maximum radius for integration [kpc]
   //  par_light[7]  Relative precision sought for integration

   double rmin = 1.e-8;
   double res = 0.;
   simpson_log_adapt(light_i_norm_integrand, rmin, par_light[1], par_light, res, par_light[7], false);
   double res_tmp = 0.;
   simpson_log_adapt(light_i_norm_integrand, par_light[1], par_light[6], par_light, res_tmp, par_light[7], false);
   res += res_tmp;

   return res;
}

//______________________________________________________________________________
void light_i_norm_integrand(double &R, double par_light[8], double &res)
{
   //--- Integrand 2*pi*R*I(R) [Lsol kpc^2] to calculate normalisation for surface brightness.
   //
   // INPUTS:
   //  R             Projected radius [kpc]
   //  par_light[0]  Light profile 1st free par (usually normalisation)
   //  par_light[1]  Light profile 2nd free par (usually scale radius [kpc])
   //  par_light[2]  Light profile 3rd free par (if used)
   //  par_light[3]  Light profile 4th free par (if used)
   //  par_light[4]  Light profile 5th free par (if used)
   //  par_light[5]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_light[6]  Maximum radius for integration [kpc]
   //  par_light[7]  Relative precision for the integration
   // OUTPUT:
   //  res                  2*PI*R*I(R) [integrand to calculate I(R) normalisation]

   res = 2.* PI * R * light_i(R, par_light);
}

//______________________________________________________________________________
double light_nu(double &r, double par_light[8])
{
   //--- Returns at radius r [kpc] the deprojected (3D) density profile nu(r) [kpc^[-3]).
   //
   //  r             Radius [kpc]
   //  par_light[0]  Light profile 1st free par (usually normalisation)
   //  par_light[1]  Light profile 2nd free par (usually scale radius [kpc])
   //  par_light[2]  Light profile 3rd free par (if used)
   //  par_light[3]  Light profile 4th free par (if used)
   //  par_light[4]  Light profile 5th free par (if used)
   //  par_light[5]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_light[6]  If not analytical, maximum radius for integration [kpc]
   //  par_light[7]  If not analytical, relative precision for the integration

   double rmax_integr = par_light[6];

   // In light_profile():
   //   - switch_qty set to 2 to calculate nu(r)
   //   - par_light[6] set to r
   par_light[6] = r;
   double res = light_profile(r, par_light, 2, rmax_integr);

   // Reset par_light[6] to its former value
   par_light[6] = rmax_integr;

   return res;
}

//______________________________________________________________________________
void light_nu_integrand(double &Y, double par_light[8], double &res)
{
   //--- Evaluates the density profile integrand [Lsol/kpc2] which enters the
   //    calculation of density profile (at (de)projected radius r [kpc]). We have
   //       nu(r) =  -(1/pi) \int_0^\infty  dI(R)/dR * dY/R
   //    so that the integrand is
   //       nu_integrand = -(1/pi) * dI(R)/dR * dY/R
   //    N.B.: nu_integrand = light_profile(y, par_light, 4).
   //
   // INPUTS:
   //  par_light[0]  Light profile 1st free par (usually normalisation)
   //  par_light[1]  Light profile 2nd free par (usually scale radius [kpc])
   //  par_light[2]  Light profile 3rd free par (if used)
   //  par_light[3]  Light profile 4th free par (if used)
   //  par_light[4]  Light profile 5th free par (if used)
   //  par_light[5]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_light[6]  Radius r [kpc] considered for de-projection
   //  par_light[7]  UNUSED
   // OUTPUT:
   //  res           -(1/pi) * dI(R)/dR * dY/R [integrand required to calculate nu(r)]

   // In light_profile():
   //   - switch_qty set to 3 to calculate 2 * nu(y)
   res = light_profile(Y, par_light, 4);
}

//______________________________________________________________________________
double light_profile(double &rR_or_yY, double par_light[8], int switch_qty, double rmax_integr)
{
   //--- Calculates quantity (surface brightness, density profile, surface brightness
   //    integrand, etc.) for any light profile available in gENUM_LIGHTPROFILE
   //    (see params.h), depending on switch_qty (see below).
   //
   //  rR_or_yY      Distance (depends on switch_qty) if 3D: r or y=sqrt(r^2-R^2); if 2D: R or Y=sqrt(R^2-r^2)  [kpc]
   //  par_light[0]  Light profile 1st free par (usually normalisation)
   //  par_light[1]  Light profile 2nd free par (usually scale radius [kpc])
   //  par_light[2]  Light profile 3rd free par (if used)
   //  par_light[3]  Light profile 4th free par (if used)
   //  par_light[4]  Light profile 5th free par (if used)
   //  par_light[5]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_light[6]  Radius R (resp. r) [kpc] considered for projection (resp. deprojection)
   //  par_light[7]  Relative precision sought for integration (if switch_qty==3 or 4)
   //  switch_qty    Switch quantity to calculate:
   //                   1 -> I(R)=Sigma(R): surface brightness [Lsol]
   //                   2 -> nu(r)=rho(r): density profile [kpc^{-3}]
   //                   3 -> Integrand to calculate I(R) from nu(r): integrand = 2 * nu(y) with y = sqrt(r^2-R^2)]
   //                   4 -> Integrand to calculate nu(r) from I(R): integrand = -(1/pi) * dI(R)/dR * 1/R
   //  rmax_integr   Maximum radius for integration [kpc]


   // If switch_qty integer not in [0,4], abort!
   if (switch_qty < 1 || switch_qty > 4) {
      printf("\n====> ERROR: light_profile() in jeans_analysis.cc");
      printf("\n             switch_qty=%d is not allowed (should be 1, 2, 3, or 4)", switch_qty);
      printf("\n             => abort()\n\n");
      abort();
   }

   // Switch on profile selected to return desired quantity
   int card_profile = (int)par_light[5];
   switch (card_profile) {

      case kEXP2D:
         if (switch_qty == 1)
            // I(R)
            return light_profile_EXP2D(rR_or_yY, par_light, false);
         else if (switch_qty == 2)
            // nu(r)
            return light_profile_EXP2D(rR_or_yY, par_light, true);
         else if (switch_qty == 3) {
            // 2*nu(y)
            print_warning("jeans_analysis.cc", "light_profile()",
                          "I(R) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         } else if (switch_qty == 4) {
            // -(1/pi)*dI(R)/dR*1/R
            print_warning("jeans_analysis.cc", "light_profile()",
                          "nu(r) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         }

      case kEXP3D:
         if (switch_qty == 1)
            // I(R)
            return light_profile_EXP3D(rR_or_yY, par_light, true);
         else if (switch_qty == 2)
            // nu(r)
            return light_profile_EXP3D(rR_or_yY, par_light, false);
         else if (switch_qty == 3) {
            // 2*nu(y)
            print_warning("jeans_analysis.cc", "light_profile()",
                          "I(R) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         } else if (switch_qty == 4) {
            // -(1/pi)*dI(R)/dR*1/R
            print_warning("jeans_analysis.cc", "light_profile()",
                          "nu(r) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         }

      case kKING2D:
         if (switch_qty == 1)
            // I(R)
            return light_profile_KING2D(rR_or_yY, par_light, false);
         else if (switch_qty == 2)
            // nu(r)
            return light_profile_KING2D(rR_or_yY, par_light, true);
         else if (switch_qty == 3) {
            // 2*nu(y)
            print_warning("jeans_analysis.cc", "light_profile()",
                          "I(R) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         } else if (switch_qty == 4) {
            // -(1/pi)*dI(R)/dR*1/R
            print_warning("jeans_analysis.cc", "light_profile()",
                          "nu(r) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         }

      case kPLUMMER2D:
         if (switch_qty == 1)
            // I(R)
            return light_profile_PLUMMER2D(rR_or_yY, par_light, false);
         else if (switch_qty == 2)
            // nu(r)
            return light_profile_PLUMMER2D(rR_or_yY, par_light, true);
         else if (switch_qty == 3) {
            // 2*nu(y)
            print_warning("jeans_analysis.cc", "light_profile()",
                          "I(R) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         } else if (switch_qty == 4) {
            // -(1/pi)*dI(R)/dR*1/R
            print_warning("jeans_analysis.cc", "light_profile()",
                          "nu(r) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         }

      case kSERSIC2D:
         if (switch_qty == 1)
            // I(R)
            return light_profile_SERSIC2D(rR_or_yY, par_light, false);
         else if (switch_qty == 2)
            // nu(r)
            return light_profile_SERSIC2D(rR_or_yY, par_light, true, rmax_integr, par_light[7]);
         else if (switch_qty == 3) {
            // 2*nu(y)
            print_warning("jeans_analysis.cc", "light_profile()",
                          "I(R) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         } else if (switch_qty == 4) {
            // -(1/pi)*dI(R)/dR*1/R
            // Calculate nu(r) from I(R) with integration performed on dY
            //        [the change of variable is Y = sqrt(R^2-r^2)]
            //   nu(r) = -(1/pi) \int_r^\infty  dI(R)/dR * dR/\sqrt{R^2-r^2}
            //         = -(1/pi) \int_0^\infty  dI(R)/dR * dY/R
            //   => integrand = -(1/pi) * dI(R)/dR * 1/R

            // For Sersic (using bn = 2n - 1/3 + 0.009876/n)
            //  I(R)= I0 * exp{-bn [(R/rc)^{1/n} - 1] }
            // so that
            //  dI(R)/dR = - [ bn / (R*n) * (R/rc)^[1/n] ] * I(R)
            double n = par_light[2];
            double bn = 2.*n - 0.3333333333 + 0.009876 / n;
            double Y = rR_or_yY;
            double ref_par6 = par_light[6];
            double r = par_light[6];
            double R = sqrt(Y * Y + r * r);
            double dIdR = -bn / n * pow(R / par_light[1], 1. / n) / R;
            double res = - dIdR * light_profile_SERSIC2D(R, par_light, false) / (PI * R);
            par_light[6] = ref_par6;
            return res;
         }

      case kZHAO3D:
         if (switch_qty == 1)
            // I(R)
            return light_profile_ZHAO3D(rR_or_yY, par_light, true, rmax_integr, par_light[7]);
         else if (switch_qty == 2)
            // nu(r)
            return light_profile_ZHAO3D(rR_or_yY, par_light, false);
         else if (switch_qty == 3) {
            // 2*nu(y)
            // Calculate I(R) from nu(r) with integration performed on dy:
            //        [the change of variable is y = sqrt(r^2-R^2)]
            //   I(R) = 2 \int_R^\infty r*nu(r) * dr/\sqrt{r^2-R^2}
            //        = 2 \int_0^\infty nu(y) dy
            //   => integrand = 2 * nu(y)

            double R = par_light[6];
            double y = rR_or_yY;
            double r = sqrt(y * y + R * R);
            return 2. * light_profile_ZHAO3D(r, par_light, false);
         } else if (switch_qty == 4) {
            // -(1/pi)*dI(R)/dR*1/R
            print_warning("jeans_analysis.cc", "light_profile()",
                          "nu(r) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         }

      default:
         printf("\n====> ERROR: light_profile() in jeans_analysis.cc");
         printf("\n             card_profile=%d does not correspond to any light profile", card_profile);
         printf("\n             => abort()\n\n");
         abort();
   }
   return -1.;
}

//______________________________________________________________________________
string light_profile_legend(double par_light[6], bool is_with_formula)
{
   //--- Returns a name as "lightprofile name (#1,#2,#3,#4, #5)" for light profile.
   //
   //  par_light[0]  Light profile 1st free par (usually normalisation)
   //  par_light[1]  Light profile 2nd free par (usually scale radius [kpc])
   //  par_light[2]  Light profile 3rd free par (if used)
   //  par_light[3]  Light profile 4th free par (if used)
   //  par_light[4]  Light profile 5th free par (if used)
   //  par_light[5]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  is_with_formula Whether to write full formula in legend or not

   int card_profile = (int)par_light[5];
   char tmp[500];
   string name = gNAMES_LIGHTPROFILE[card_profile];
   string formula = "";

   // Switch on profile selected to return desired quantity
   switch (card_profile) {

      case kEXP2D:
         if (is_with_formula)
            formula = "I(R)#propto exp(-R/r_{c}) with";
         sprintf(tmp, "%s %s (r_{c}=%.2lf)", name.c_str(),
                 formula.c_str(), par_light[1]);
         return tmp;
      case kEXP3D:
         if (is_with_formula)
            formula = "#nu(r)#propto exp(-r/r_{c}) with";
         sprintf(tmp, "%s %s (r_{c}=%.2lf)", name.c_str(),
                 formula.c_str(), par_light[1]);
         return tmp;
      case kKING2D:
         if (is_with_formula)
            formula = "I(R)#propto [(1+#frac{R^{2}}{r_{c}^{2}})^{-1/2} - (1+#frac{r_{lim}^{2}}{r_{c}^{2}})^{-1/2}]^{2} with";
         sprintf(tmp, "%s %s (r_{c}=%.2lf, r_{lim}=%.2lf)", name.c_str(),
                 formula.c_str(), par_light[1], par_light[2]);
         return tmp;
      case kPLUMMER2D:
         if (is_with_formula)
            formula = "I(R)#propto #pi r_{c}^{2} (1+#frac{R^{2}}{r_{c}^{2}})^{-2} with";
         sprintf(tmp, "%s %s (r_{c}=%.2lf)", name.c_str(), formula.c_str(), par_light[1]);
         return tmp;
      case kSERSIC2D:
         if (is_with_formula)
            formula = "I(R)#propto exp(-b_{n} [(R/r_{c})^{1/n} - 1])";
         sprintf(tmp, "%s %s (r_{c}=%.2lf, n=%.2lf)", name.c_str(),
                 formula.c_str(), par_light[1], par_light[2]);
         return tmp;
      case kZHAO3D:
         if (is_with_formula)
            formula = "#nu(r)#propto #frac{(r/r_{c})^{-#gamma}}{[1+(r/r_{c})^{#alpha}]^{(#beta-#gamma)/#alpha}} with";
         sprintf(tmp, "%s %s (r_{c}=%.2lf,#alpha=%.1lf,#beta=%.1lf,#gamma=%.1lf)",
                 name.c_str(), formula.c_str(), par_light[1], par_light[2], par_light[3], par_light[4]);
         return tmp;
      default :
         printf("\n====> ERROR: light_profile_legend() in jeans_analysis.cc");
         printf("\n             card_profile=%d does not correspond to any light profile", card_profile);
         printf("\n             => abort()\n\n");
         abort();
   }
   return "";
}

//______________________________________________________________________________
double light_profile_EXP2D(double &R_or_r, double par[2], bool is_deproject)
{
   //--- Returns surface brightness I(R) or de-projected (3D) for an exponential profile [Lsol].
   //    See Sect. 4.2 of Evan, An, and Walker, MNRAS 393, 50 (2009):
   //          I(R)= I0 * exp{-R/rc}              [2D]
   //       => nu(r) = I0 / (PI*rc) * K0(r/rc)    [3D]
   //
   //  R_or_r        Projected distance R (or distance r) from profile centre [kpc]
   //  par[0]        Light surface brightness normalisation I0 [Lsol]
   //  par[1]        Light critical radius rc [kpc]
   //  is_deproject  Whether to return I(R) or nu(r).

   double arg = R_or_r / par[1];

   // De-projected (analytical) [3D]
   if (is_deproject) {
      if (arg > 500.)
         arg = 500.;
      gsl_sf_result x;
      int test = gsl_sf_bessel_K0_e(arg, &x);
      if (test == 15) // underflow is reached
         return 0.;
      else
         return par[0] / (PI * par[1]) * gsl_sf_bessel_K0(arg);
   }

   // Surface brightness [2D]
   return par[0] * exp(-arg);
}

//______________________________________________________________________________
double light_profile_EXP3D(double &r_or_R, double par[2], bool is_project)
{
   //--- Returns density profile nu(r) or projected (2D) for an exponential profile [kpc^{-3}].
   //    See Sect. 4.2 of Evan, An, and Walker, MNRAS 393, 50 (2009):
   //          rho(r)= rho0 * exp{-r/rc}          [3D]
   //       => I(R) = 2 * rho0 * R * K1(R/rc)     [2D]
   //
   //  r_or_R        Distance (r) or projected distance (R) from profile centre [kpc]
   //  par[0]        Light density profile normalisation rho0 [kpc^{-3}]
   //  par[1]        Light critical radius rc [kpc]
   //  is_project    Whether to return nu(r) or I(R)

   // Projected (analytical) [2D]
   if (is_project) {
      if (r_or_R / par[1] > 500.) // underflow is reached
         return 0.;
      else
         return 2. * par[0] * r_or_R * gsl_sf_bessel_K1(r_or_R / par[1]);
   }

   // Density profile [3D]
   if (r_or_R / par[1] > 500.) // underflow is reached
      return 0.;
   else
      return par[0] * exp(-r_or_R / par[1]);
}

//______________________________________________________________________________
double light_profile_KING2D(double &R_or_r, double par[3], bool is_deproject)
{
   //--- Returns surface brightness I(R) or de-projected (3D) for a King profile [Lsol].
   //    See Supplementary information of Strigari et al., Nature 454, 1096 (2008):
   //          I(R)= I0 * [ (1+R^2/rc^2)^{-1/2} - (1+rlim^2/rc^2)^{-1/2} ]^2                      [2D]
   //       => nu(r) = I0 * [acos(z)/z)-(1-z^2)^{1/2}] / [ pi*rc*z^2 * (1+rlim^2/rc^2)^{3/2} ]    [3D]
   //          where z^2 = (1+r^2/rc^2)/(1+rlim^2/rc^2)
   //
   //  R_or_r        Projected distance R (or distance r) from profile centre [kpc]
   //  par[0]        Light surface brightness normalisation I0 [Lsol]
   //  par[1]        Light critical radius rc [kpc]
   //  par[2]        Light second radius rlim [kpc]
   //  is_deproject  Whether to return I(R) or nu(r).

   // Profile is zero above rlim
   if (R_or_r > par[2])
      return 0;

   // Useful quantities
   double rc2 = par[1] * par[1];
   double rlim2 = par[2] * par[2];

   // De-projected (analytical)
   if (is_deproject) {
      double tmp = 1. + rlim2 / rc2;
      double z2 = (1. + R_or_r * R_or_r / rc2) / tmp;
      double z = sqrt(z2);
      return par[0] * (acos(z) / z - sqrt(1. - z2)) / (PI * par[1] * pow(tmp, 1.5) * z2);
   }

   // Surface brightness (2D)
   return par[0] * pow(pow(1. + R_or_r * R_or_r / rc2, -0.5) - pow(1. + rlim2 / rc2, -0.5), 2);
}

//______________________________________________________________________________
double light_profile_PLUMMER2D(double &R_or_r, double par[2], bool is_deproject)
{
   //--- Returns surface brightness I(R) or de-projected (3D) for a Plummer profile [Lsol].
   //    See Sect. 4.1 of Evan, An, and Walker, MNRAS 393, 50 (2009):
   //          I(R)= I0 / [ pi rc^2 * (1+R^2/rc^2)^2 ]               [2D]
   //       => nu(r) = 3*I0 / [ 4*pi*rc^3 * (1+r^2/rc^2)^{5/2} ]     [3D]
   //
   //  R_or_r        Projected distance R (or distance r) from the profile centre [kpc]
   //  par[0]        Light (surface brightness) normalisation I0 [Lsol]
   //  par[1]        Light critical radius rc [kpc]
   //  is_deproject  Whether to return I(R) or nu(r).

   double rc2 = par[1] * par[1];

   // De-projected (analytical)  [3D]
   if (is_deproject)
      return 3.*par[0] / ((4.*PI * rc2 * par[1]) * pow(1. + (R_or_r * R_or_r) / rc2, 2.5));

   // Surface brightness [2D]
   return par[0] / (PI * rc2 * pow(1. + (R_or_r * R_or_r) / rc2, 2.));
}

//______________________________________________________________________________
double light_profile_SERSIC2D(double &R_or_r, double par[3], bool is_deproject,
                              double const &rmax_integr, double const &eps)
{
   //--- Returns surface brightness I(R) or de-projected (3D) for a Sersic profile [Lsol].
   //    See Graham and Driver, PASA 22, 118 (2005) or Merrit et al., AJ 132, 2685 (2006):
   //          I(R)= I0 * exp{-bn [(r/rc)^{1/n} - 1] }                       [2D]
   //               with bn = 2n - 1/3 + 0.009876/n
   //       => nu(r) = -(1/pi) \int_r^\infty  dI(R)/dR * dR/\sqrt{R^2-r^2}   [3D]
   //
   //  R_or_r        Projected distance R (or distance r) from the profile centre [kpc]
   //  par[0]        Light surface brightness normalisation I0 [Lsol]
   //  par[1]        Light critical radius rc [kpc]
   //  par[2]        Light shape parameter n (double) [-]
   //  is_deproject  Whether to return I(R) or nu(r).
   //  rmax_integr   Maximum radius for integration (if is_deproject==true) [kpc]
   //  eps           Relative precision sought for integration (if is_deproject==true)

   // Not valid below n<0.5
   if (par[2] < 0.5) {
      printf("\n====> ERROR: light_profile_SERSIC2D in jeans_analysis.cc");
      printf("\n             n<0.5 is not authorised in CLUMPY (here n=%.2le)", par[2]);
      printf("\n             => abort()\n\n");
      abort();
   }

   // De-projected (numerical integration)
   if (is_deproject) {
      // No analytical formula for SERSIC2D de-projection
      //       -> use of inverse Abel transform!
      //   nu(r) = -(1/pi) \int_r^\infty  dI(R)/dR * dR/\sqrt{R^2-r^2}
      //         = -(1/pi) \int_0^\infty  dI(R)/dR * dY/R
      // where we used the change of variable Y = sqrt(R^2-r^2).

      // Fill par_light parameters to enable calling of function light_nu_integrand()
      const int npar = 8;
      double par_light[npar];
      //  par_light[0...4] Light profile free pars (usually normalisation)
      //  par_light[5]     Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
      //  par_light[6]     Radius r [kpc] considered for de-projection
      //  par_light[7]     UNUSED
      for (int i = 0; i < 3; i++)
         par_light[i] = par[i];
      par_light[3] = 0.;
      par_light[4] = 0.;
      par_light[5] = kSERSIC2D;
      par_light[6] = R_or_r;
      par_light[7] = 0.;

      // Cast integration in three pieces:
      double zero = 0.;
      double y_min = 1.e-4 * par[1];
      double inter = 0;
      double y_max = rmax_integr;
      double y_inter = par[1];
      double res = 0.;
      //  1. Integrate on 'flat region'
      //    a) from 0 to y_min (linear step)
      simpson_lin_adapt(light_nu_integrand, zero, y_min, par_light, inter, eps);
      res += inter;
      //    b) from y_min to y_max (log-step)
      simpson_log_adapt(light_nu_integrand, y_min, y_inter, par_light, inter, eps);
      res += inter;
      //  2. Integrate after critical radius (decreasing part) -> log step
      simpson_log_adapt(light_nu_integrand, y_inter, y_max, par_light, inter, eps);
      res += inter;

      return res;
   }

   // Surface brightness (2D)
   double bn = 2.*par[2] - 0.3333333333 + 0.009876 / par[2];
   return  par[0] * exp(-bn * (pow(R_or_r / par[1], 1. / par[2]) - 1.));
}

//______________________________________________________________________________
double light_profile_ZHAO3D(double &r_or_R, double par[5], bool is_project,
                            double const &rmax_integr, double const &eps)
{
   //--- Returns density profile nu(r) or projected (2D) for an exponential profile [kpc^{-3}].
   //    See Hernquist, ApJ 356, 359 (1990) or Zhao, MNRAS 278, 488 (1996):
   //          nu(r)= rho0 / [ (r/rc)^\gamma (1+(r/rc)^\alpha)^{(\beta-\gamma)/\alpha}]   [3D]
   //       => I(R) = 2 \int_R^\infty r*rho(r) * dr/\sqrt{r^2-R^2}                        [2D]
   //
   //  r_or_R        Distance (r) or projected distance (R) from the profile centre [kpc]
   //  par[0]        Light density profile normalisation rho0 [kpc^{-3}]
   //  par[1]        Light critical radius rc [kpc]
   //  par[2]        Light shape parameter #1
   //  par[3]        Light shape parameter #2
   //  par[4]        Light shape parameter #3
   //  is_project    Whether to return nu(r) or I(R)
   //  rmax_integr   Maximum radius for integration (if is_deproject==true) [kpc]
   //  eps           Relative precision sought for integration (if is_deproject==true)

   // Projected (numerical integration) [2D]
   if (is_project) {
      // No analytical formula for ZHAO3D projection.
      //       -> use of Abel transform!
      //   I(R) = 2 * \int_R^\infty  nu(r) * r dr/\sqrt{r^2-R^2}
      //        = 2 * \int_0^\infty  nu[r(y)] * dy
      // where we used the change of variable y = sqrt(r^2-R^2).

      // Fill par_light parameters to enable calling of function light_i_integrand(y)
      const int npar = 8;
      double par_light[npar];
      //  par_light[0...4] Light profile free pars (usually normalisation)
      //  par_light[5]     Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
      //  par_light[6]     Radius R [kpc] considered for projection
      //  par_light[7]     UNUSED
      for (int i = 0; i < 5; i++)
         par_light[i] = par[i];
      par_light[5] = kZHAO3D;
      par_light[6] = r_or_R;
      par_light[7] = 0.;

      // Cast integration in four pieces: [0,y_min] [y_min,y_inter1] [y_inter1,y_inter2] [y_inter2,y_max]
      // Find max value of function (for y_min), and then find y_inter1 and y_inter2 such that:
      //    light_i_integrand(y_inter1) = 0.99*light_i_integrand(y_min)
      //    light_i_integrand(y_inter2) = 0.8*light_i_integrand(y_min)
      double y_min = 1.e-6 * par[1], y_max = rmax_integr;
      double res_ymin = 0.;
      light_i_integrand(y_min, par_light, res_ymin);
      double res_ref1 = 0.99 * res_ymin;
      double res_ref2 = 0.8 * res_ymin;
      double y_inter1 = 0.;
      double eps1 = 0.005;
      find_x_logstep(res_ref1, y_inter1, y_min, y_max, light_i_integrand, par_light, eps1);
      double y_inter2 = 0.;
      double eps2 = 0.01;
      find_x_logstep(res_ref2, y_inter2, y_inter1, y_max, light_i_integrand, par_light, eps2);

      // Fill intermediate points (for integration)
      vector<double> yy;
      yy.push_back(0.);
      if (y_min > yy[yy.size() - 1] && y_min < y_max)
         yy.push_back(y_min);
      if (y_inter1 > yy[yy.size() - 1] && y_inter1 < y_max)
         yy.push_back(y_inter1);
      if (y_inter2 > yy[yy.size() - 1] && y_inter2 < y_max)
         yy.push_back(y_inter2);
      if (y_max > yy[yy.size() - 1] * 10.)
         yy.push_back(sqrt(y_inter2 * y_max));
      yy.push_back(y_max);
      double res = 0.;
      for (int i = 0; i < (int)yy.size() - 1; ++i) {
         double tmp_res = 0.;
         if (i == 0)
            // N.B.: very inner parts have very small contribution => not necessary to have good contrib.
            simpson_lin_adapt(light_i_integrand, yy[i], yy[i + 1], par_light, tmp_res, 0.1);
         else
            simpson_log_adapt(light_i_integrand, yy[i], yy[i + 1], par_light, tmp_res, eps);
         res += tmp_res;
      }
      return res;
   }

   // Density profile [3D]
   return rho_ZHAO(r_or_R, par);
}

//______________________________________________________________________________
double jeans_nuvr2(double &r, double par_jeans[20])
{
   //--- Returns solution of Jeans equation (3D 'un'-projected) in [Msol/kpc^4]
   //       [nu(r)*\bar{v_r^2}(r)]/G = 1/f(r) \int_r^\infty f(s) nu(s) M(s)/s^2 ds
   //    with f(r) calculated in beta_anisotropy_fr.
   //    N.B.: We do not return directly nu*vr2, but nu*vr2/G instead
   //    (where G=6.67384e-11 = NEWTON_CONSTANT_SI in CLUMPY, see inlines.h), because
   //    numerically it is better to handle numbers as close as possible to 1.
   //
   //  r             Radius from halo center [kpc]
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Max radius of integration
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)

   int card_profile_anisotropy = int(par_jeans[19]);
   double rmax = par_jeans[6];
   double r1 = par_jeans[17];
   if (card_profile_anisotropy == kCONSTANT)
      r1 = par_jeans[8];

   double res = 0.;
   if (r > r1)
      simpson_log_adapt(jeans_nuvr2_integrand, r, rmax, par_jeans, res, par_jeans[14]);
   else {
      simpson_log_adapt(jeans_nuvr2_integrand, r, r1, par_jeans, res, par_jeans[14]);
      double tmp_res = 0.;
      simpson_log_adapt(jeans_nuvr2_integrand, r1, rmax, par_jeans, tmp_res, par_jeans[14]);
      res += tmp_res;
   }

   // Returns [nu(r)*\bar{v_r^2}(r)]/G in [Msol/kpc^4]
   return res / beta_anisotropy_fr(r, &par_jeans[15], par_jeans[14]);
}

//______________________________________________________________________________
void jeans_nuvr2_integrand(double &s, double par_jeans[20], double &res)
{
   //--- Calculates f(s)*nu(s)*M(s)/s^2 in [Msol/kpc^5], the integrand to get
   //    the solution nu(r)*\bar{v_r^2}(r) of the Jeans equation: f(s) is
   //    calculated with beta_anisotropy_fr(), whereas M(s) is the mass of
   //    the dark matter halo.
   //
   // INPUTS:
   //  s             Radius from halo center [kpc]
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Max radius of integration
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   // OUTPUT:
   //  res           f(s)*nu(s)*M(s)/s^2


   // DM mass calculated for M(r<s), with s radius in [kpc]
   // N.B.: in mass_singlehalo(), par[6] is the radius of integration,
   // and we use temporarily here par_jeans[6] to store 's'.
   double rmax_ref = par_jeans[6];
   double R_ref = par_jeans[13];
   par_jeans[6] = s;
   double m_dm = mass_singlehalo(par_jeans, par_jeans[14]);
   // reset par_jeans[6] to rmax_ref
   par_jeans[6] = rmax_ref;
   par_jeans[13] = rmax_ref; // set maximum integration radius for computation of nu (needed for somes cases)

   // Calculates f(s)*nu(s)*M(s)/s^2 in [Msol/kpc^5]
   res = beta_anisotropy_fr(s, &par_jeans[15], par_jeans[14])
         * light_nu(s, &par_jeans[7]) * m_dm / (s * s);
   par_jeans[13] = R_ref; // set back par_jeans[13] to its initial value
}

//______________________________________________________________________________
void jeans_nuvr2_integrand_log(double &s, double par_jeans[20], double &res)
{
   //--- Calculates s*jeans_nuvr2_integrand in [Msol/kpc^4], with jeans_nuvr2_integrand()
   //    the integrand to get the solution nu(r)*\bar{v_r^2}(r) of the Jeans equation.
   //    This function is used to find radii where to cut the integrations in different
   //    sub-parts (because integrations are made in log-scale).
   //
   // INPUTS:
   //  s             Radius from halo center [kpc]
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Max radius of integration
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   // OUTPUT:
   //  res           s*jeans_nuvr2_integrand

   // Calculates s*[f(s)*nu(s)*M(s)/s^2] in [Msol/kpc^4]
   jeans_nuvr2_integrand(s, par_jeans, res);
   res *= s;
}

//______________________________________________________________________________
void jeans_isigmap2_integrand(double &y_or_r, double par_jeans[21], double &res)
{
   //--- Handler to deal with I*Sigma_p^2 integrand whether we use the Kernel
   //    solution of the general full integration solution: the choice is set
   //    in the 20-th (extra) parameter.
   //
   // INPUTS:
   //  y_or_r        y for par_jeans[21]=false, r otherwise [kpc]
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Max radius of integration
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  par_jeans[20] is_use_kernel: whether to use kernel solution (true) or full integration (false)
   // OUTPUT:
   //  res           jeans_isigmap2_integrand_numerical or jeans_isigmap2_integrand_withkernel in [Msol/kpc^4]

   bool is_use_kernel = (bool)par_jeans[20];
   if (is_use_kernel)
      jeans_isigmap2_integrand_withkernel(y_or_r, par_jeans, res);
   else
      jeans_isigmap2_integrand_numerical(y_or_r, par_jeans, res);
}

//______________________________________________________________________________
void jeans_isigmap2_integrand_log(double &y_or_r, double par_jeans[21], double &res)
{
   //--- Handler to deal with I*Sigma_p^2 integrand (log) whether we use the Kernel
   //    solution of the general full integration solution: the choice is set in
   //    the 20-th (extra) parameter.
   //
   // INPUTS:
   //  y_or_r        Equals y if par_jeans[21]=false, r otherwise [kpc]
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Max radius of integration
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  par_jeans[20] is_kernel: whether kernel solution (true) or full integration (false)
   // OUTPUT:
   //  res           r*jeans_isigmap2_integrand in [Msol/kpc^3]

   jeans_isigmap2_integrand(y_or_r, par_jeans, res);
   res *= y_or_r;
}

//______________________________________________________________________________
void jeans_isigmap2_integrand_numerical(double &y, double par_jeans[20], double &res)
{
   //--- Calculates the integrand 2 * (1-beta_ani(r)*R^2/r^2) * nuvr2(r)/G in [Msol/kpc^4]
   //    used to obtain the Jeans solution for projected quantities.
   //    N.B.1: we remind that the quantity to calculate will lead to I(R)*sigmap2(R)/G,
   //    because the function nuvr2() returns \nu(r)*\bar{v_r^2}(r)/G.
   //    N.B.2: this is the brute-force triple integration way to obtain [I(R)*sigmap2(R)/G]
   //    when no analytical shortcut can be taken (see jeans_isigmap2_integrand_withkernel()
   //    and beta_anisotropy_kernel() for an alternative for some anisotropy profiles).
   //    This full (and more time consumming) integration must be used, e.g., for kBAES
   //    anisotropy profile.
   //
   // INPUTS:
   //  y             Integration variable y=sqrt(r^2-R^2) for a fixed R projected radius [kpc]
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Max radius of integration
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   // OUTPUT:
   //  res           2 * (1-beta_ani(r)*R^2/r^2) * nuvr2(r)

   double R = par_jeans[13];
   double r = sqrt(y * y + R * R);

   // Integrand =  (1-beta(r)*R^2/r^2) * [nuvr2(r)/G] in [Msol/kpc^4]
   res = 2. * (1. - beta_anisotropy(r, &par_jeans[15]) * R * R / (r * r)) * jeans_nuvr2(r, par_jeans);
}

//______________________________________________________________________________
void jeans_isigmap2_integrand_withkernel(double &r, double par_jeans[20], double &res)
{
   //--- Calculates the integrand 2*K(r/R,r_a/R)*vu(r)*M(r)/r in [Msol/kpc^4]
   //    that is used to obtain the Jeans solution for projected quantities.
   //    N.B.1: the quantity to calculate will lead to I(R)*sigmap2(R)/G,
   //    N.B.2: this is the Kernel single integration way (fast) to obtain
   //    I(R)*sigmap2(R)/G when an analytical form exist (see jeans_isigmap2_integrand_numerical()
   //    for the triple integration when no alternative exist). The kernel
   //    solution available, coded in beta_anisotropy_kernel(), are taken
   //    from Appendix of Mamon & Lokas, MNRAS 363, 705 (2005).
   //
   // INPUTS:
   //  r             Radius from the halo centre [kpc]
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Max radius of integration
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   // OUTPUT:
   //  res           K(r/R,r_a/R)*vu(r)*M(r)/r

   // DM mass calculated within M(r)
   // N.B.: in mass_singlehalo(), par[6] is the radius of integration,
   // and we use temporarily here par_jeans[6] to store 'r'.
   double rmax_ref = par_jeans[6];
   par_jeans[6] = r;
   double m_dm = mass_singlehalo(par_jeans, par_jeans[14]);
   // reset par_jeans[6] to rmax_ref
   par_jeans[6] = rmax_ref;

   // nu(r)
   // N.B.: in light_nu(), the parameter corresponding to par_jeans[13] must be the maximum radius!
   double par_ref13 = par_jeans[13];
   par_jeans[13] = par_jeans[6];
   double nu = light_nu(r, &par_jeans[7]);
   par_jeans[13] = par_ref13;

   // Kernel
   // N.B.: for kCONSTANT, the kernel relies on 2F1 hypergeometric function
   // that fails for some argument. As the integrand is used only to calculate
   // the integration domain, and as 2F1 is as the argument for the kCONSTANT
   // kernel is close to 1, we set it to 1, for r<R. This approximation is not
   // used in the jeans_sigmap2 calculation.
   double kernel = beta_anisotropy_kernel(r, &par_jeans[15], par_jeans[13]);

   // Kernel = 2*K(r/R,r_a/R)*nu(r)*M(r)/r in [Msol/kpc^4]
   res = 2. * kernel * m_dm * nu / r;
}

//______________________________________________________________________________
double jeans_sigmap2(double &R, double par_jeans[20], bool is_use_kernel)
{
   //--- Returns the projected velocity dispersion sigmap2(R) in [km^2/s^2]
   //    calculated from the Jeans equation. If a kernel solution exists,
   //    the integration to get I(R)*sigma_p^2(R)/G is obtained calling
   //    jeans_isigmap2_integrand_withkernel(), otherwise by calling
   //    jeans_isigmap2_integrand_numerical(). The integrand for the former
   //    solution is analytical, whereas it involves a double integration
   //    for the latter. The parameter "is_use_kernel" forces the use of
   //    the kernel if it exists (otherwise use tripe integration).
   //    N.B.: R and par_jeans[13] must be equal.
   //
   //  R             Projected radius from halo centre [kpc]
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Max radius of integration
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  is_use_kernel [default=true] if true, use kernel integration if exists

   bool is_verbose = false;

   // We calculate sigma_p^2(R) = [ I(R)*sigma_p^2(R)/G ] * G / I(R) with
   //    [I*sigmap2/G] = [Msol/kpc^3]
   //    [I(R)] = [Lsol] = [kpc^{-2}]  (any norm. in I(R) is absorbed in the Jeans solution)
   //    [G] = [m^3/kg/s^2]
   // The above calculation corresponds to [Msol/kg m^3/kpc /s^2], which must
   // be converted into [km^2/s^2].
   // N.B.: in light_i(), the parameter corresponding to par_jeans[13] must
   // be the maximum radius!
   double par_ref = par_jeans[13];
   par_jeans[13] = par_jeans[6];
   double i_R = light_i(R, &par_jeans[7]);

   if (i_R < 1.e-60) { // If i_light is zero (e.g. for King profile above rlim)
      cout << "In jeans_sigmap2, the surface brightness profile I(" << R
           << ") is close to zero. sigmap2 is set to 0. " << endl;
      return 0.;
   }

   double prefactor = (NEWTON_CONSTANT_SI * M3perKG_to_KM2KPCperMSOL) / i_R;
   par_jeans[13] = par_ref;

   // N.B.: if no kernel exists, force numerical integration
   //   -> kBAES does not have a kernel
   if (int(par_jeans[19]) == kBAES)
      is_use_kernel = false;

   // Ensure par_jeans[13]=R
   par_jeans[13] = R;

   // To have the most generic calculation, we fill a par_jeans_opt[21] parameter,
   // where the last parameter decides whether to use the kernel solution
   // or full solution for the integrand
   const int n_new = 21;
   double par_jeans_opt[n_new];
   for (int i = 0; i < n_new - 1; ++i)
      par_jeans_opt[i] = par_jeans[i];
   par_jeans_opt[n_new - 1] = (int)is_use_kernel;

   // To optimise integration time, proceed in several steps:
   //    1. Find rmax which gives the maximum valmax of integrand
   //    2. Find rlow and rup for which integrand(rlow)=integrand(rup)=valmax*fraction
   //    3. Integrate on ranges [zero,rmin] + [rmin,rlow] + [rlow,rup] + [rup,rmax]
   // N.B.: there are slight differences whether the integration is performed on the kernel
   // solution (integrated on dr from 0 to Rmax), or on the general projected jeans solution
   // (integrated on dy from 0 to sqrt(Rmax^2-R^2), taken here to be Rmax)
   double xlow = 0.;
   double xup = 0.;

   if (is_verbose)
      cout << "   0. SQUEEZE AXES" << endl;

   // 0. Squeeze optimal range for integration
   // Start with large range, and adapt until range brackets reasonable values of integrand
   double ax = 1.e-3 * par_jeans[8];
   double cx = 1.e3 * par_jeans[8];
   double bx = 0.;
   if (is_use_kernel) {
      ax = R;
      //cx = min(1.e4*R,cx);
   }
   // N.B.: in case value is zero for ax or bx, adapt until it is non-zero
   //       (otherwise, goldenmin_logstep fails!).
   double par_ref_opt = par_jeans_opt[13];
   par_jeans_opt[13] = R ;
   par_jeans_opt[14] = 0.1; // set "bad" precision for this step

   int nstep = 10;
   double pas_log_r = pow(cx / ax, 1. / double(nstep - 1));
   double test = 0.;
   double y_R = 0.;
   double Rp = R * 1.5;
   jeans_isigmap2_integrand_log(Rp, par_jeans_opt, y_R);
   if (is_verbose)
      cout << "y_R before = " << y_R << endl;
   for (int j = 0; j < nstep; ++j) {
      double ytest = ax * pow(pas_log_r, j);
      jeans_isigmap2_integrand_log(ytest, par_jeans_opt, test);
      y_R = max(y_R, test);
   }

   if (y_R < 1.e-60) // the integrand is equal to 0 everywhere
      return 0.;
   if (is_verbose)
      cout << "y_R = " << y_R << endl;

   int N_it = 0;
   double tol;
   double size_step;
   if (is_use_kernel) {
      tol = 1.e-4;
      size_step = 1.2;
   } else {
      tol = 1.e-6;
      size_step = 1.5;
   }

   do {
      N_it = N_it + 1;
      double y_ax = 0., y_cx = 0.;
      jeans_isigmap2_integrand_log(ax, par_jeans_opt, y_ax);
      jeans_isigmap2_integrand_log(cx, par_jeans_opt, y_cx);
      if (is_verbose)
         cout << "  sigma_p loop: " << ax << " " << cx << "   "
              << y_ax << " " << y_cx << "  y(R)=" << y_R << endl;
      if (y_ax / y_R < tol || y_cx / y_R < tol) {
         if (y_ax / y_R < tol) ax *= size_step;
         if (y_cx / y_R < tol) cx /= size_step;
         if (ax > cx) {
            if (is_use_kernel) {
               ax = R;
               cx *= size_step;
               break;
            }
            printf("\n====> ERROR: jeans_sigmap2() in jeans_analysis.cc");
            printf("\n             Fail to find integration ranges");
            printf("\n             => abort()\n\n");
            abort();
         }
      } else {
         ax /= size_step;
         cx *= size_step;
         break;
      }
   } while (1);

   if (is_verbose)
      cout << "N iterations for finding integration range = " << N_it << endl;

   double ax_opt = ax;
   double cx_opt = cx;

   // 1. Search for maximum
   // Use integrand log to search for max (we integrate afterwards in log step).
   // We use goldenmin_logstep method in which y(bx) must be larger than y(ax) and y(cx).
   // N.B.: specific case if Kernel is used, as the integration starts at r=R
   if (is_verbose)
      cout << "   1. SEARCH MAX" << endl;
   bx = sqrt(ax * cx);
   double x_valmax = 0.;
   double valmax = 0.;
   par_jeans_opt[13] = R ;
   // Set minimum precision for computing of the integrand for
   // finding maximum and caracteristic radii
   par_jeans_opt[14] = 0.05;

   if (par_jeans[14] < par_jeans_opt[14])
      par_jeans_opt[14] = par_jeans[14];

   // minimum precision for finding the optimal range of integration
   double eps = 0.05;
   if (par_jeans[14] < eps)
      eps = par_jeans[14];

   valmax = goldenmin_logstep(ax, bx, cx, jeans_isigmap2_integrand_log, par_jeans_opt, eps, false, x_valmax);
   par_jeans_opt[13] = par_ref_opt;

   if (is_verbose)
      cout << "           R=" << R << " x_valmax=" << x_valmax << " valmax=" << valmax << endl;

   // 2. Find xlow and xup int_log(xlow)=int_log(xup)=int_log(x_valmax)*fraction
   // N.B.: specific case if Kernel is used, as the integration starts at r=R
   //      - if x_valmax>R => xlow=R
   //double fraction = 0.9;
   double fraction = 0.9;
   double y_ref = valmax * fraction;
   if (is_verbose)
      cout << "   2.a. XLOW" << endl;
   // xlow
   double x1 = ax_opt;
   double x2 = x_valmax;

   find_x_logstep(y_ref, xlow, x1, x2, jeans_isigmap2_integrand_log, par_jeans_opt, eps);
   if (is_verbose)
      cout << "   xlow = " << xlow << endl;
   if (is_verbose)
      cout << "   2.b. XUP" << endl;
   // xup
   x1 = x_valmax;
   x2 = cx_opt;
   find_x_logstep(y_ref, xup, x1, x2, jeans_isigmap2_integrand_log, par_jeans_opt, eps);

   if (is_verbose)
      cout << "   xup = " << xup << endl;


   //  3. Integrate on ranges:
   //     - If standard integration: [zero,xlow] + [xlow,x_valmax], [x_val_max,xup] + [xup,cx_opt]
   //     - If Kernel: [ax_opt,xlow] + [xlow,x_valmax] + [x_valmax,xup] + [xup,cx_opt]
   if (is_verbose)
      cout << "   3. INTEGRATE" << endl;
   if (par_jeans[6] < cx_opt) {
      printf("\n====> ERROR: jeans_sigmap2() in jeans_analysis.cc");
      printf("\n             For calculation at R=%.2le, the (upper) integration boundary", R);
      printf("\n             Rintegr (here %.2le)  must be larger than %.2le",  par_jeans[6], cx_opt);
      printf("\n             => abort()\n\n");
      abort();
   }

   par_jeans_opt[14] = par_jeans[14]; // reset precision to its initial value

   // Fill intermediate points (for integration)
   vector<double> rr;
   if (is_use_kernel) {
      rr.push_back(R);
      if (sqrt(R * sqrt(R * xlow)) > rr[rr.size() - 1] && sqrt(R * sqrt(R * xlow)) < cx_opt)
         rr.push_back(sqrt(R * sqrt(R * xlow)));
      if (sqrt(R * xlow) > rr[rr.size() - 1] && sqrt(R * xlow) < cx_opt)
         rr.push_back(sqrt(R * xlow));
      if (sqrt(sqrt(R * xlow) * xlow) > rr[rr.size() - 1] && sqrt(sqrt(R * xlow) * xlow) < cx_opt)
         rr.push_back(sqrt(sqrt(R * xlow) * xlow));
   } else {
      rr.push_back(0.);
      rr.push_back(ax_opt);
   }
   if (xlow > rr[rr.size() - 1] && xlow < cx_opt)
      rr.push_back(xlow);
   if (x_valmax > rr[rr.size() - 1] && x_valmax < cx_opt)
      rr.push_back(x_valmax);
   if (xup > rr[rr.size() - 1] && xup < cx_opt)
      rr.push_back(xup);
   if (10.* rr[rr.size() - 1] < cx_opt) {
      rr.push_back(sqrt(rr[rr.size() - 1]*cx_opt));
   }
   rr.push_back(cx_opt);


   double res = 0.;
   for (int i = 0; i < (int)rr.size() - 1; ++i) {
      double tmp_res = 0.;
      if (i == 0 && !is_use_kernel)
         simpson_lin_adapt(jeans_isigmap2_integrand, rr[i], rr[i + 1], par_jeans_opt, tmp_res, par_jeans[14]);
      else
         simpson_log_adapt(jeans_isigmap2_integrand, rr[i], rr[i + 1], par_jeans_opt, tmp_res, par_jeans[14]);
      res += tmp_res;
   }
   if (is_verbose)
      cout << "   R = " << R << " DONE!" << endl;

   return res * prefactor;
}

//______________________________________________________________________________
void load_jeansanalysis_parameters(string file_name, struct gStructJeansAnalysis &analysis)
{
   //--- Fills from a file the parameters to use in a Jeans analysis (free and
   //    fixed parameters, priors, etc.). An example file is ./data/params_jeans.
   //
   //  file_name     Name of the file frow which to load parameters
   //  analysis      Structure in which to store Jeans analysis parameters


   vector<string> light_param;
   int nfree_param = 9; // number of possible free parameters

   // Open parameter file
   ifstream file(file_name.c_str());
   if (!file) {
      printf("\n====> ERROR: load_jeansanalysis_parameters() in jeans_analysis.cc");
      printf("\n             Could not open file %s", file_name.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }
   cout << ">>>>> Load " << file_name << endl;

   string line;
   int line_number = 0; // which line is read?
   while (getline(file, line)) { // read parameters file
      line.erase(remove_if(line.begin(), line.end(), isNotASCII), line.end());
      if (line[0] == '#' || line.empty()) continue;
      line_number += 1;
      vector<string> params;
      replace_substring(line, "\t", " ");
      string2list(line, " ", params); // read line

      if (line_number <= nfree_param) { // read the free parameters information
         if (params.size() < 6) {
            printf("\n====> ERROR: load_jeansanalysis_parameters() in jeans_analysis.cc");
            printf("\n             Wrong format (les than 6 parameters) in line = %s", line.c_str());
            printf("\n             => abort()\n\n");
            abort();
         }
         analysis.IsFreeParam[line_number - 1] = atoi(params[2].c_str()); // read is parameter is free or not
         if (analysis.IsFreeParam[line_number - 1] == 0) { // parameter is fixed
            analysis.LowerRange[line_number - 1] = 0.;
            analysis.UpperRange[line_number - 1] = 100.;
         } else {   // parameter is free
            if (params[3] == "-" || params[4] == "-") { // No ranges given for the free parameter
               printf("\n====> ERROR: load_jeansanalysis_parameters() in jeans_analysis.cc");
               printf("\n             No range given for the prior on %s",  params[0].c_str());
               printf("\n             => abort()\n\n");
            }
            analysis.LowerRange[line_number - 1] = atof(params[3].c_str());
            analysis.UpperRange[line_number - 1] = atof(params[4].c_str());
         }
         analysis.Value[line_number - 1] = atof(params[5].c_str());
      } else if (line_number == nfree_param + 1) { // read profiles
         for (int n = 0; n < 3; n++)
            params[n].erase(remove(params[n].begin(), params[n].end(), '\t'), params[n].end());

         analysis.HaloProfile = string_to_enum("FLAG_PROFILE", params[0]);
         analysis.LightProfile = string_to_enum("FLAG_LIGHTPROFILE", params[1]);
         analysis.AnisoProfile = string_to_enum("FLAG_ANISOTROPYPROFILE", params[2]);
      } else if (line_number < nfree_param + 7) // read light profile parameters
         light_param.push_back(params[2]);
      else if (line_number == nfree_param + 7)
         analysis.Name = params[2];
      else if (line_number == nfree_param + 8)
         analysis.Distance = atof(params[2].c_str());
      else if (line_number == nfree_param + 9)
         analysis.Longitude = atof(params[2].c_str());
      else if (line_number == nfree_param + 10)
         analysis.Latitude = atof(params[2].c_str());
      else if (line_number == nfree_param + 11)
         analysis.HaloSize = atof(params[4].c_str());
      else if (line_number == nfree_param + 12)
         gSIM_JEANS_RMAX = atof(params[5].c_str());
   }

   // Set the light profile parameters in the gStructJeansAnalysis analysis
   for (int k = 0; k < 5; k++)
      analysis.LightParam[k] = atof(light_param[k].c_str());
}


//______________________________________________________________________________
double log_likelihood_jeans_binned(double *par_jeans, gStructHalo stat_halo, int type_analysis)
{
   //--- Returns log-likelihood function for a binned Jeans analysis for given
   //    DM halo with free parameters 'par_jeans' (see, e.g., Bonnivard et al., 2015).
   //
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Max radius of integration
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  stat_halo     DM halo and its Jeans data (against which the log-likelihood is calculated)
   //  type_analysis 0 if data=sigmap2, 1 if data=sigmap, 2 if data=vel (last choice unused here)

   double logL = 0.; // LogLikelihood
   int imax = stat_halo.JeansData.Radius.size(); // Number of kinematic data points

   // If many data points, use interpolation to speed up the analysis
   if (imax >= 15) {
      const int ni = 15; //number of points for interpolation
      double sigmap_inter[ni]; // sigmap2 values for interpolation
      double xmin = find_min_value(stat_halo.JeansData.Radius); // Minimum radius in kinematic data
      double xmax = find_max_value(stat_halo.JeansData.Radius) + 0.001; // Maximum radius in kinematic data (+ safety factor)

      double step_log_r = pow(xmax / xmin, 1. / double(ni - 1)); // set the log scale of interpolation radii
      double xi[ni]; // radius values for interpolation

      for (int j = 0; j < ni; j++) { // loop over the interpolation points
         xi[j] = xmin * pow(step_log_r, j); // radius where to compute sigmap2
         par_jeans[13] = xi[j]; // set the values of radius
         sigmap_inter[j] = jeans_sigmap2(xi[j], par_jeans);
         if (type_analysis == 1) // data = sigmap
            sigmap_inter[j] = sqrt(sigmap_inter[j]); // set the sigmap values for interpolation
      }

      for (int i = 0; i < imax; i++) { // loop over the entire kinematic data set
         int index = -1;
         // find the radius values that enclose the ith radius
         for (int j = 0; j < ni - 1; j++) {
            if (xi[j] <= stat_halo.JeansData.Radius[i] && xi[j + 1] > stat_halo.JeansData.Radius[i]) {
               index = j;
               break;
            }
         }
         if (index < 0) {
            printf("\n====> ERROR: log_likelihood_jeans_binned() in jeans_analysis.cc");
            printf("\n             Could not find interpolating radius");
            printf("\n             => abort()\n\n");
            abort();
         }
         // Compute log likelihood
         logL += pow((stat_halo.JeansData.VelocData[i]
                      - loglin_interp(stat_halo.JeansData.Radius[i], xi[index], xi[index + 1], sigmap_inter[index], sigmap_inter[index + 1])), 2)
                 / pow(stat_halo.JeansData.ErrVelocData[i], 2);
      }
   } else { // same without interpolation
      for (int i = 0; i < imax; i++) {
         par_jeans[13] = stat_halo.JeansData.Radius[i];
         double res = jeans_sigmap2(stat_halo.JeansData.Radius[i], par_jeans);
         if (type_analysis == 1) // data = sigmap
            res = sqrt(res);

         logL += pow((stat_halo.JeansData.VelocData[i] - res), 2)
                 / pow(stat_halo.JeansData.ErrVelocData[i], 2);
      }
   }
   return -0.5 * logL;

}
//______________________________________________________________________________
double log_likelihood_jeans_unbinned(double par_jeans[20], gStructHalo stat_halo, double vmean)
{
   //--- Returns log-likelihood function for an unbinned Jeans analysis for given
   //    DM halo with free parameters 'par_jeans' (see, e.g., Bonnivard et al., 2015).
   //
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Max radius of integration
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  stat_halo     DM halo and its Jeans data (against which the log-likelihood is calculated)
   //  vmean         Mean velocity for this set of velocity data

   double logL = 0.; // LogLikelihood
   int imax = stat_halo.JeansData.Radius.size(); // Number of kinematic data points

   // use interpolation to speed up the analysis
   const int ni = 15; //number of points for interpolation
   double sigmap_inter[ni]; // sigmap2 values for interpolation
   double xmin = find_min_value(stat_halo.JeansData.Radius); // Minimum radius in kinematic data
   double xmax = find_max_value(stat_halo.JeansData.Radius) + 0.001; // Maximum radius in kinematic data (+ safety factor)

   double step_log_r = pow(xmax / xmin, 1. / double(ni - 1)); // set the log scale of interpolation radii
   double xi[ni]; // radius values for interpolation

   for (int j = 0; j < ni; j++) { // loop over the interpolation points
      xi[j] = xmin * pow(step_log_r, j); // radius where to compute sigmap2
      par_jeans[13] = xi[j]; // set the value of radius
      sigmap_inter[j] = jeans_sigmap2(xi[j], par_jeans); // set the sigmap2 values for interpolation
   }

   for (int i = 0; i < imax; i++) { // loop over the entire kinematic data set
      int index = -1;
      // find the radius values that enclose the ith radius
      for (int j = 0; j < ni - 1; j++) {
         if (xi[j] <= stat_halo.JeansData.Radius[i] && xi[j + 1] > stat_halo.JeansData.Radius[i]) {
            index = j;
            break;
         }
      }
      if (index < 0) {
         printf("\n====> ERROR: log_likelihood_jeans_binned() in jeans_analysis.cc");
         printf("\n             Could not find interpolating radius");
         printf("\n             => abort()\n\n");
         abort();
      }
      // Compute log likelihood
      logL += stat_halo.JeansData.MembershipProb[i] * log(pow(stat_halo.JeansData.ErrVelocData[i], 2) + loglin_interp(stat_halo.JeansData.Radius[i], xi[index], xi[index + 1], sigmap_inter[index], sigmap_inter[index + 1])) +
              stat_halo.JeansData.MembershipProb[i] * pow((stat_halo.JeansData.VelocData[i] - vmean), 2) / (pow(stat_halo.JeansData.ErrVelocData[i], 2) + loglin_interp(stat_halo.JeansData.Radius[i], xi[index], xi[index + 1], sigmap_inter[index], sigmap_inter[index + 1]));
   }

   return -0.5 * logL;
}

//______________________________________________________________________________
void print_parjeans(double par_jeans[20])
{
   //--- Prints Jeans parameters.
   //
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Max radius of integration
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)

   printf("       ================= par_jeans[8] =================\n");
   printf("         [0] DM halo norm     = %le [Msol kpc^{-3}]\n", par_jeans[0]);
   printf("         [1] DM halo rs       = %le [kpc]\n", par_jeans[1]);
   printf("         [2] DM halo shape#1  = %le [-]\n", par_jeans[2]);
   printf("         [3] DM halo shape#2  = %le [-]\n", par_jeans[3]);
   printf("         [4] DM halo shape#3  = %le [-]\n", par_jeans[4]);
   printf("         [5] DM halo profile  = %d (%s)\n", (int)par_jeans[5], gNAMES_PROFILE[(int)par_jeans[5]]);
   printf("         [6] DM halo Rmax     = %le [kpc]\n", par_jeans[6]);
   printf("         [7] Light norm       = %le [Lsol kpc^{-2}] or [Lsol kpc^{-3}] (for I(R) or nu(r)\n", par_jeans[7]);
   printf("         [8] Light rs         = %le [kpc]\n", par_jeans[8]);
   printf("         [9] Light shape#1    = %le\n", par_jeans[9]);
   printf("         [10]Light shape#2    = %le\n", par_jeans[10]);
   printf("         [11]Light shape#3    = %le\n", par_jeans[11]);
   printf("         [12]Light profile    = %d (%s)\n", (int)par_jeans[12], gNAMES_LIGHTPROFILE[(int)par_jeans[12]]);
   printf("         [13]R (projected)    = %le [kpc]\n", par_jeans[13]);
   printf("         [14]eps              = %le\n", par_jeans[14]);
   printf("         [15]Anisotropy par#1 = %le\n", par_jeans[15]);
   printf("         [15]Anisotropy par#2 = %le\n", par_jeans[16]);
   printf("         [15]Anisotropy par#3 = %le\n", par_jeans[17]);
   printf("         [15]Anisotropy par#4 = %le\n", par_jeans[18]);
   printf("         [16]Anisotropy prof  = %d (%s)\n", (int)par_jeans[19], gNAMES_ANISOTROPYPROFILE[(int)par_jeans[19]]);
   printf("\n");
}

//______________________________________________________________________________
void print_parlight(double par_light[8])
{
   //--- Prints par_light parameters.
   //
   //  par_light[0]  Light profile #1 (usually normalisation)
   //  par_light[1]  Light profile #2 (usually scale radius [kpc])
   //  par_light[2]  Light profile #3 (if used)
   //  par_light[3]  Light profile #4 (if used)
   //  par_light[4]  Light profile #5 (if used)
   //  par_light[5]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_light[6]  If not analytical, maximum radius for integration [kpc]
   //  par_light[7]  If not analytical, relative precision for the integration

   printf("       ================= par_light[8] =================\n");
   printf("         [0] norm             = %le [Lsol kpc^{-2}] or [Lsol kpc^{-3}] (for I(R) or nu(r)\n", par_light[0]);
   printf("         [1] scale radius     = %le [kpc]\n", par_light[1]);
   printf("         [2] shape#1          = %le\n", par_light[2]);
   printf("         [3] shape#2          = %le\n", par_light[3]);
   printf("         [4] shape#3          = %le\n", par_light[4]);
   printf("         [5] Light profile    = %d (%s)\n", (int)par_light[5], gNAMES_LIGHTPROFILE[(int)par_light[5]]);
   printf("         [6] Rmax             = %le [kpc]\n", par_light[6]);
   printf("         [7] eps              = %le\n", par_light[7]);
   printf("\n");
}

