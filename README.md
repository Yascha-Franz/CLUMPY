[![pipeline status](https://gitlab.com/clumpy/CLUMPY/badges/release/pipeline.svg)](https://gitlab.com/clumpy/CLUMPY/commits/release) [![coverage report](https://gitlab.com/clumpy/CLUMPY/badges/release/coverage.svg)](https://gitlab.com/clumpy/CLUMPY/commits/release)

# For detailed information, see the [online documentation](http://lpsc.in2p3.fr/clumpy/) & [CLUMPY publications](http://adsabs.harvard.edu/abs/2012CoPhC.183..656C,2016CoPhC.200..336B,2018arXiv180608639H)

##  I. WHAT IS CLUMPY?


**Physics goal**   
CLUMPY addresses the following physics problems of dark matter annihilation/decay signals for all targets in the sky:   
   - *astrophysical J-factors* from spherical or triaxial DM distribution in the Galaxy (smooth, mean and/or drawn sub-haloes) and/or for nearby haloes;   
   - *&gamma;-ray and &nu; fluxes* for galactic and/or extragalactic contributions (combining the astrophysical factor and the particle physics factor);
   - *Jeans analyses* to reconstruct DM profiles from kinematic data (&chi;2 analysis or MCMC analysis) and calculate the associated J-factors.   

**History (of development)**  
- V1 or V11.09 [2011/09] -- *CLUMPY: A code for γ-ray signals from dark matter structures*; [Charbonnier, Combet, & Maurin, CPC 183, 656 (2012)](http://cdsads.u-strasbg.fr/abs//2012CoPhC.183..656C).  
- V2 or V15.06 [2015/06] -- *CLUMPY v2: Jeans analysis, &gamma;-ray and &nu; fluxes from dark matter (sub-)structures;* [Bonnivard et al., CPC 200, 336 (2016)](http://cdsads.u-strasbg.fr/abs/2016CoPhC.200..336B).  
- V3 or V18.06 [2018/06] -- *CLUMPY v3: &gamma;-ray and &nu; signals from dark matter at all scales;* [Hütten, Combet, & Maurin (2018)](http://adsabs.harvard.edu/abs/2018arXiv180608639H).   

**Developers/contacts**   
For any question, or if you believe that you have a module that would be a bonus for a future CLUMPY release, feel free to [contact us](mailto:clumpy@lpsc.in2p3.fr) to discuss the matter:
- [Céline Combet](mailto:celine.combet@lpsc.in2p3.fr) (DM distributions, clump drawing, extragalactic module)
- [Moritz Hütten](mailto:mhuetten@mpp.mpg.de) (HEALPix/FITS implementation, APS, I/O interfaces, extragalactic module)
- [David Maurin](mailto:dmaurin@lpsc.in2p3.fr) (project coordinator)

*Past contributors*: Vincent Bonnivard (Jeans analysis module in v2), Aldée Charbonnier (wrote unreleased v0), and Emmanuel Nezri (particle physics module in v2).


## II. INSTALLATION AND COMPILATION

The CLUMPY package is written in C/C++ (but no classes) and is interfaced with several third-party softwares. Some are mandatory (other optionals) and must be installed before proceeding to the CLUMPY installation.

**Language and third-party softwares**  
- *To install* (mandatory): 
    1. [GSL](http://www.gnu.org/software/gsl) (GNU scientific library): directly install the package for your distribution (or, if needed, install it locally). Do not forget to install the development files ``gsl-devel`` to also get the headers files to include!
    2. [CFITSIO](http://heasarc.gsfc.nasa.gov/fitsio/) for (FITS outputs): directly install the package for your distribution (or, if it does not exist for your system, [download](http://heasarc.gsfc.nasa.gov/fitsio/) it and install it).
- *Shipped with the code* (no need to install!):     
    1. [HEALPix](http://sourceforge.net/projects/healpix) (skymap pixelization scheme and APS):  a frozen version of this library (version 3.31) is shipped with the code, and internally built at compilation, so you no longer have to worry about how to install it.
- *Optional packages*: 
    1. [ROOT CERN library](https://root.cern.ch/): for displays and random drawing clumps from multivariate distributions. The easiest way to install it on your system is via your package manager (apt-get, dnf, or brew, etc.), and do not forget the development (``-devel``) packages. Alternatively, you may want to compile locally ROOT. To do so, [download a version](https://root.cern.ch/releases) and follow the configuration/compilation instructions to [build ROOT](https://root.cern.ch/building-root). In addition, in the latter case, do not forget to define the ``ROOTSYS`` environment variable in your ``~/.bashrc``).
    2. [GreAT](http://lpsc.in2p3.fr/great) (for MCMC Jeans analyses): if you want to run MCMC/Jeans analyses, follow the download/installation instruction from GreAT [here](http://lpsc.in2p3.fr/great) or directly from [GreAT on GitLab](https://gitlab.in2p3.fr/derome/GreAT). Before configuring :program:`CLUMPY` with :program:`cmake`, you have to set the environmental variable :envvar:`GREAT` and do (e.g., in your ~/.bashrc):
    ```
    $ export LD_LIBRARY_PATH=$GREAT/lib:$LD_LIBRARY_PATH
    $ export DYLD_LIBRARY_PATH=$GREAT/lib:$DYLD_LIBRARY_PATH  *[Mac OSX only]*
    ```
    3. [CLASS](http://www.class-code.net) (v3 onwards required): needed for the computation of the linear matter power spectrum.
    
For MacOS, we recommend to install gsl and optionally ROOT via homebrew. To define the environment variable ``ROOTSYS``, you need to source, e.g., in your configuration file ``~/.bashrc``,  ``thisroot.sh``/``thisroot.csh``.


**Install CLUMPY**   
1. *Clone from git repository* (if you seek for an old code release, see Release history)
    ```
    $ git clone https://gitlab.com/clumpy/CLUMPY.git --depth=1
    ```

2. The compilation relies on cmake (file CmakeLists.txt)
    ```
    $ cd CLUMPY
    $ mkdir build; cd build
    $ cmake ../
    $ make -jN   *[using N=2, 3,... cores]*
    ```
    *Note that CLUMPY requires cmake ≥ 3.2. If you need to install cmake itself and do not have access to a C++2011 compiler, [cmake 3.2.3](http://gitlab.kitware.com/cmake/cmake/tree/v3.2.3) does the job.*

3. *Define the CLUMPY environment variables* (e.g., in your ~/.bashrc)
    ```
    $ export CLUMPY=absolute_path_to_local_installation
    $ export PATH=$CLUMPY/bin:$PATH
    $ export LD_LIBRARY_PATH=$CLUMPY/lib:$LD_LIBRARY_PATH
    $ export DYLD_LIBRARY_PATH=$CLUMPY/lib:$DYLD_LIBRARY_PATH  *[Mac OS only]*
    ```
    
    *Do not forget to ``$ source ~/.bashrc`` (or whereever your variables are defined) to ensure that the environment variables are set in your current xterm (``$ echo $CLUMPY`` should point to the directory where you installed CLUMPY).*

    You should now be able to run the code from anywhere just typing:
    ```
    $ clumpy
    ```

4. *Test installation*
We now provide an automated test suite to check the proper output of all modules after the installation. After having set up correctly all environmental variables, just type:
    ```
    $ clumpy_tests
    ```
    *To pass all the tests, the environmental variable CLUMPY must be set.*

5. *CLUMPY execution*
    ```
    $ ./bin/clumpy              [to access all CLUMPY options]
    $ ./bin/clumpy_jeansChi2    [for Jeans analysis]
    $ ./bin/clumpy_jeansMCMC    [if GreAT is installed]
    ```
    *Run examples are provided in the documentation (see http://lpsc.in2p3.fr/clumpy/) so that you can check the various options available and the expected outputs.*


**Installed and tested on**  
- Ubuntu 14.04 (LTS, gcc 4.8.4) and 17.10 (c++ 7.2)
- MacOS X High Sierra (10.13.4, clang-902.0.39.2) and Capitan
- Scientific Linux 6.9 (Carbon, gcc 4.4.7)
- Fedora 25 (c++ 6.4.1)

## III. License

CLUMPY is licensed under the [GNU General Public License (GPLv2)](http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html).


